% Function to generate Gibbs sampler chain, called by main.m

% Init value are the evaluation values when the chain does not
% update theta or sigma_eta

function gen_chains_fun(chain_spec,model,priors,init,data,options)

   % Output folder name and options
    output_folder = [options.output_stub model.model_name];
    chain_base_file_name = [output_folder '/chain_' chain_spec.name '_' num2str(chain_spec.chain_no)];
    N_files = options.total_files - options.resumeAt;
    
    if ~isdir(output_folder)
        mkdir(output_folder);
        fprintf('Created folder %s\n', output_folder);
    end

    % Generate Eta mat
    if model.donor_uh
        data.eta_mat = create_dummies(data.donor_id,data.donor_list);
    end 
    
    % Generate patient mat
    if model.pt_uh
        data.pt_mat = create_dummies(data.patients,data.patient_list);
    end

    %% Initialize Chain
    if options.resumeAt == 0        
        % Default value for Y
        if ~isfield(init,'Y')
            init.Y = mvnrnd(0,1,data.N_offers);
        end
        % Default value for Y _beta_i
        if model.pt_uh & ~isfield(init,'beta_i')
            init.beta_i = model.K_types - sum(bsxfun(@lt, rand(data.N_patients,1), cumsum(init.beta)),2)+1;
        end
        if model.donor_uh & ~isfield(init,'eta')
            init.eta = randn(data.N_donors,1);
        end
        delete([chain_base_file_name '*'])
        save([chain_base_file_name '--init'],'model','priors','init','chain_spec');
    else
        last_file = [chain_base_file_name '_' num2str(options.resumeAt)];
        load(last_file);
        init = final_val;
    end
    
                    
    %% Run and save files
    for ff = 1:N_files
        display(['Generating ' chain_spec.name ' chain for ' model.model_name ' Chain: ' num2str(chain_spec.chain_no) ', File: ' num2str(options.resumeAt + ff)]);
                         
        rng(init.rng_state);
        tic;
        [chain,final_val] = gibbs_mixture(data, model, priors, init, chain_spec);
        toc     
        
        % Chain save file
        save_file = [chain_base_file_name '_' num2str(options.resumeAt + ff)];
        gen_date = datetime('today');
        save(save_file,'chain','final_val','gen_date');
     
        % New init_val is the previous final_val
        init = final_val;
    end

