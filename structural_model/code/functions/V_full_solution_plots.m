% Comparing to Vs based on observed cutoffs and those arising from the full-solution method.


% testing
testing = false;  % set to true for testing not through structural model code

if testing
    addpath('../../structural_model/code');
    addpath('../../structural_model/code/functions');
    addpath('../../structural_model/code/gibbsdiag');
    addpath('../../structural_model/code/mcmcdiag');
    addpath('../../structural_model/code/model_files');
    addpath('../../structural_model/code/cf_functions');
    addpath('../../structural_model/code/cf_files');
    addpath('../../structural_model/code/cf_optimal');
    model_data_folder = '~/organs-star/model_master_files/';
    model_name = 'nyrt_lim_time_uh';
    cf_options.sample_name = 'sample0'
    
end
if ~exist('cf_full_sol','var'), 
    cf_full_sol = 'score_binspoints_locl_ref'; 
    fprintf('cf_full_sol is not defined. \nSetting it to the default: %s\n',cf_full_sol);
end

 
% paths
path_results = ['/proj/organs/common_output/structural/output/results/' model_name '/' cf_options.sample_name];  % Name of folder / name of cf_sample
path_figs  = ['/proj/organs/common_output/structural/output/results/' model_name '/figs/' cf_options.sample_name];  % Name of folder / fig / name of cf_sample
path_master = [model_data_folder model_name];

fid = fopen([path_figs '_V_full_solution_plots.tex'],'w');
%% compare value functions

load([path_results '_' cf_full_sol '_results.mat']);  % counterfactual CCP
plotobj.ctf = output;
load([path_master '/' cf_options.sample_name]);  % observed cutoffs
plotobj.data = data_object(1).calc;
plotobj.postEntry = data_object(1).postEntry(2);
quarters = (1:size(plotobj.ctf.V,2))/4;

%% Added on 8/22/2019
%% Estimated CCPs and those implied by the steady-state equilibrium are similar
plotobj.data.p_accept = log(normcdf(bsxfun(@plus,plotobj.data.Gamma_eta, - plotobj.data.V)));
plotobj.ctf.p_accept = log(normcdf(bsxfun(@plus,plotobj.data.Gamma_eta, - plotobj.ctf.V)));

%% Weights
% Recall
    % pi_this: probability that patient i receives an offer for 
    % kidney k given that a donor arrives today.
w_donor_pat_pair =  bsxfun(@times,plotobj.ctf.pi_this,plotobj.ctf.pt_weights);
w_donor_pat_pair =  w_donor_pat_pair/sum(w_donor_pat_pair(:));

%% Difference between the two probabilities;
comp.diff = plotobj.ctf.p_accept - plotobj.data.p_accept;
%% Average accept probability
comp.avd = plotobj.data.p_accept(:)' * w_donor_pat_pair(:);
comp.avc = plotobj.ctf.p_accept(:)' * w_donor_pat_pair(:);
%% Variance of accept probability
comp.var = (plotobj.data.p_accept(:)'-comp.avd).^2  * w_donor_pat_pair(:);
%% MSE
comp.mse = (comp.diff(:)').^2 * w_donor_pat_pair(:);
%% MSE - BIAS
comp.res = (comp.diff(:)-comp.avc(:)+comp.avd(:))'.^2 * w_donor_pat_pair(:);


%% Weighted regression
[b,bint,r,rint,stats] = regress(plotobj.ctf.p_accept(:).*w_donor_pat_pair(:),[w_donor_pat_pair(:), plotobj.data.p_accept(:).*w_donor_pat_pair(:)]);

fprintf('Weighted Regression\n Rquared: \n');
stats(1)
fprintf('95% CI: \n');
bint

fprintf(fid,['\\begin{frame}                  \n',...
             '\\frametitle{Regression Results}\n',...
             'Regression of Log simulated CCP on a constant and Log estimated CCP\n',...
             'Weighted by the probability the offer is made in steady state equilibrium (SSeq)\n',...
             '\\begin{itemize}                \n',...
             '\\item Constant (95 CI): ' num2str(bint(1,1)) ' ' num2str(bint(1,2)) '\n',...
             '\\item Log estimated CCP  (95 CI): ' num2str(bint(2,1)) ' ' num2str(bint(2,2)) '\n',...
             '\\item R-square: ' num2str(stats(1)) '\n',...
             '\\end{itemize}                  \n',...
             'Note:\n',...
             '\\begin{itemize}                \n',...
             '\\item Log estimated CCP: directly estimated in our model\n',...
             '\\item Log simulated CCP: obtained in SSeq given our estimated preferences\n',...
             '\\end{itemize}                  \n',...
             '\\end{frame}                    \n']);


%% BinScatter -- High Weights
% Set boundaries of the plot
lb = -35;  % lower x and y;
in_scatter = w_donor_pat_pair > 1e-6  & plotobj.data.p_accept > lb & plotobj.ctf.p_accept > lb;
hh = binscatter(plotobj.data.p_accept(in_scatter),plotobj.ctf.p_accept(in_scatter));

%% BinScatter -- Weights
xcell = sum(bsxfun(@gt,plotobj.data.p_accept(:),hh.XBinEdges),2);
ycell = sum(bsxfun(@gt,plotobj.ctf.p_accept(:),hh.YBinEdges),2);

in_scatter = xcell > 0  & ycell > 0;
assert(sum(w_donor_pat_pair(in_scatter))>0.995);
% Compute area in a bin
area = (hh.XLimits(2)-hh.XLimits(1)) * (hh.YLimits(2)-hh.YLimits(1))/prod(hh.NumBins);
% Plot
imagesc(min(hh.XLimits,-0.5),min(hh.YLimits,-0.5),full(sparse(xcell(in_scatter),ycell(in_scatter),w_donor_pat_pair(in_scatter)))/area);
set(gca,'YDir','normal')  
blue_colormap = matlab.graphics.chart.internal.heatmap.blueColormap(64);
blue_colormap(1,:) = [1 1 1];  % White for v.low probability
colormap(gca,blue_colormap)
xlabel('Log estimated CCP')
ylabel('Log simulated CCP')
h = colorbar;
ylabel(h,'Density');
filename =[path_figs '_' cf_full_sol 'V_full_solution_weight.png'];
saveas(gcf,filename);
fprintf(fid,'\\begin{frame}\n    \\frametitle{Weighted}\n    \\includegraphics[scale=0.5]{%s}\n\\end{frame} \n',filename);

fclose(fid);

% Print Plots
[status,cmdout] = system(['pdflatex ','-output-directory ',options.results_folder,' ', pwd, '/code/functions/V_full_solution_plots.tex']);
fprintf('%d.\n',status)