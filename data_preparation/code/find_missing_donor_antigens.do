// This script finds non- A or B locus antigens for donor_id's in DECEASED_DONOR_DATA but not KIDPAN_DATA, using other organs
// The filepaths for sample donor ids are fixed, since we always want to use the full sample to recover these antigens
// Can be run on its own, or included in gen_primary_files.do


// get donor_id's not in KIDPAN
use ~/organs-star/primary_files/sample_donor_ids.dta, clear
merge 1:m donor_id using "`kidpan_data'KIDPAN_DATA.DTA", keepusing(ddr1) keep(master) nogen
drop ddr1
keep donor_id
isid donor_id

// merge sequentially with other files: INTESTINE_DATA, LIVER_DATA, THORACIC_DATA
// first we need to get trr_id_code for each donor we find in LIVER_DATA, then merge with ADDTL_HLA for antigens

*******************************
******* Liver transplant ******
*******************************
merge 1:m donor_id using "`liver_data'LIVER_DATA.DTA", keepusing(trr_id_code)
drop if _merge == 2
rename _merge liver_match
replace liver_match = 0 if liver_match == 1
replace liver_match = 1 if liver_match == 3

// now get antigens from liver transplants
merge m:1 trr_id_code using "`liver_data'HLA Additional/LIVER_ADDTL_HLA.DTA", keepusing(dbw* dc* ddr* ddq* ddp*)
drop if _merge == 2
drop _merge trr_id_code

// rename variables to distinguish sources
foreach var in `hla_vars'{
  rename `var' `var'_liver
}
duplicates drop
isid donor_id

********************************
********** Intestines **********
********************************
merge 1:m donor_id using "`intestine_data'INTESTINE_DATA.DTA", keepusing(trr_id_code)
drop if _merge == 2
rename _merge intestine_match
replace intestine_match = 0 if intestine_match == 1
replace intestine_match = 1 if intestine_match == 3

// now get antigens from intestine transplants
merge m:1 trr_id_code using "`intestine_data'HLA Additional/INTESTINE_ADDTL_HLA.DTA", keepusing(dbw* dc* ddr* ddq* ddp*)
drop if _merge == 2
drop _merge trr_id_code

// rename variables to distinguish sources
foreach var in `hla_vars'{
    rename `var' `var'_intestine
  }
duplicates drop
isid donor_id

*******************************
************ Lungs ************
*******************************
merge 1:m donor_id using "`lung_data'THORACIC_DATA.DTA", keepusing(trr_id_code)
drop if _merge == 2
rename _merge lung_match
replace lung_match = 0 if lung_match == 1
replace lung_match = 1 if lung_match == 3

merge m:1 trr_id_code using "`lung_data'HLA Additional/THORACIC_ADDTL_HLA.DTA", keepusing(dbw* dc* ddr* ddq* ddp*)
drop if _merge == 2
drop _merge trr_id_code

// rename variables to distinguish sources
foreach var in `hla_vars'{
	rename `var' `var'_lung
}
duplicates drop
isid donor_id

// create final hla vars from the merged variables
foreach var in `hla_vars'{
  gen `var' = `var'_liver
  replace `var' = `var'_lung if `var' == .
  replace `var' = `var'_intestine if `var' == .
  drop `var'_liver `var'_lung `var'_intestine
}
drop *_match

save "`auxiliary_files'donor_antigens_notin_kidpan.dta", replace
