// Match statistics at the patient level, by patient type
// Stats are annual flow rates of different types of offers


program match_patient_experience

args make_table match_variables_file sample_name results outcomes output_folder

// Similar to match outcomes table, but from a patient's POV
 

if `make_table'==1{

    foreach sample_cut in top_10_unscreened_offers screened_and_unscreened{

        use `match_variables_file', clear  
          
        // identify which offers count toward `sample_cut' match definition

        if "`sample_cut'" == "top_10_unscreened_offers"{
            gen match = top_10 == 1
        }
        if "`sample_cut'" == "screened_and_unscreened"{
            gen match = no_offers==0  // make sure not to count patients who received no offers
        }

        local collapse_string = "(count) wl_id_code"
            
        // count offers in each category, overall and by time-varying patient type
        sort wl_id_code
        foreach var in `outcomes'{    
            by wl_id_code: egen n_`var' = total(`var' * match)
            by wl_id_code: egen n_tx_`var' = total(`var' * willing_to_accept * match)
        }
            
        // reduce dataset to patient level
        keep wl_id_code n_* init_age_cat dialysis_str sample_time_active
        duplicates drop
        isid wl_id_code  // other vars should be unique at wl_id level
            
        // compute versions of the variables as annual rates
        foreach var in `outcomes'{
            gen rate_`var' = n_`var' * 365 / sample_time_active 
            local collapse_string = "`collapse_string'" + " (mean) rate_`var'_m = rate_`var' (sum) n_`var' n_tx_`var' "
        }


        // patient-level outcome statistics, by group

        // Patient blood group
        preserve
        collapse `collapse_string', by(init_age_cat)
        rename wl_id_code count
        rename init_age_cat group
        gen sample = "`sample_cut'"
        order sample group count
        if "`sample_cut'"!="top_10_unscreened_offers"{
            append using `results'
          }
        save `results', replace
        restore

        // Patient on dialysis at listing
        collapse_by_var "`collapse_string'" dialysis_str wl_id_code "`results'" "`sample_cut'"

        // Overall
        gen everyone = "All"
        collapse_by_var "`collapse_string'" everyone wl_id_code "`results'" "`sample_cut'"

    }
}

use `results', clear

gen group_cat = 1 if group == "All"
replace group_cat = 2 if group == "On Dialysis at Registration"
replace group_cat = 3 if group == "Not on Dialysis at Registration"
replace group_cat = 4 if strpos(group,"Age") > 0
sort sample group_cat group
drop group_cat

label var sample "Offer Type"
label var group "Patient Group"
label var count "Num Patients"
label var rate_match_m "Annual Match Rate"
label var n_match "Numerb of Matches"
label var n_tx_match "Numerb of Transplanted Matches"
label var rate_local_m "Annual Local Match Rate"
label var n_local "Numerb of Local Matches"
label var n_tx_local "Numerb of Transplanted Local Matches"
label var rate_zero_abdr_m "Annual 0 Mismatch Match Rate"
label var n_zero_abdr "Numerb of 0 Mismatch Matches"
label var n_tx_zero_abdr "Numerb of Transplanted 0 Mismatch Matches"

order sample group count *match* *local* *zero*

export excel using "`output_folder'summary_tables_`sample_name'.xls", sheet("Match Patient Experience") firstrow(varlabels) sheetreplace

end
