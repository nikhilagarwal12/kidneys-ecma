
* Load all files
tempfile kas
local listofvar "num geo abdr cpra pat_abo pld ped_list ped_alloc top20 ped_alloc_top20 don_abo oth "

drop _all
insheet `listofvar' using KDPI_20.csv
gen cat = 1
save `kas', replace

drop _all
insheet `listofvar' using KDPI_20_35.csv
gen cat = 2
append using `kas'
save `kas', replace

drop _all
insheet `listofvar' using KDPI_35_85.csv
gen cat = 3
append using `kas'
save `kas', replace

drop _all
insheet `listofvar' using KDPI_85.csv
gen cat = 4
append using `kas'
save `kas', replace

* Check number of bins (69,49,46,31)
table cat 

* Check each variable
table geo
table abdr
table cpra
table pat_abo	
table don_abo

bys pld: table oth
bys ped_list: table oth
bys ped_alloc: table oth
bys top20: table oth
bys ped_alloc_top20: table oth
