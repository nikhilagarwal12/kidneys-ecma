addpath('./functions');

display('Executing import all');

% Change variable names
load([data_dir 'simulated_offers_matlab']);
offers = st_dataset;
clear st_dataset;
save([data_dir 'simulated_offers_matlab'],'offers');

% Get list of donors and patients ever offered
wlreg_audit_id_code_list = unique(offers.wlreg_audit_id_code);
wl_id_code_list = unique(offers.wl_id_code);
donor_id_list = unique(offers.donor_id);

% Import parameters of hazard rate model
hazard_dir = '/proj/organs/common_output/reduced_form/hazard/';
hazard_files = {'trim_weibull','trim_gompertz'};
for hz = 1:numel(hazard_files)
    hazard.(hazard_files{hz})=importdata([hazard_dir hazard_files{hz} '.csv']);
end
save([data_dir 'hazard'],'-struct','hazard');

% Import time-till
time_till = import_wait_times([data_dir 'mech_']); 
% Assert that all donors and pt_histories were found
assert(all(ismember(donor_id_list,cat(1,time_till.donor_id))));
assert(all(ismember(wlreg_audit_id_code_list,cat(1,time_till.pt_hist_id))));

% Remove donors and patients that are not in the sample
for abo = 1:length(time_till)
    % Create a sparsedata
    this_data = sparsedata(time_till(abo),'pt_hist_id','donor_id','blood_type');
    this_data = lookup(this_data,'pt_hist_id',wlreg_audit_id_code_list);
    this_data = lookup(this_data,'donor_id',donor_id_list);
    savesd([data_dir 'don_pat_pairs_' this_data.blood_type],this_data);
end


% Change variable names - patients
load([data_dir 'patients']);
patients = st_dataset(ismember(st_dataset.wl_id_code,wl_id_code_list),:);
clear st_dataset;
save([data_dir 'patients'],'patients');

% Change variable names - donors
load([data_dir 'donors']);
donors = st_dataset(ismember(st_dataset.donor_id,donor_id_list),:);
clear st_dataset;
save([data_dir 'donors'],'donors');

% Change variable names  - pt_history
load([data_dir 'pt_history']);
pt_history = st_dataset(ismember(st_dataset.wlreg_audit_id_code,wlreg_audit_id_code_list),:);
clear st_dataset;
save([data_dir 'pt_history'],'pt_history');

% Assert some checks
assert(all(ismember(donor_id_list,donors.donor_id)));
assert(all(ismember(wlreg_audit_id_code_list,cat(1,time_till.pt_hist_id))));

display('Done!');


