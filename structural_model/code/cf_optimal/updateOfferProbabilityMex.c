#include "mex.h"
#include "matrix.h"

#include <math.h>
#include <stdint.h>

double btw(double a, double lb, double ub) {
  if (a < ub) {
    if (a > lb) {
      return a;
    } else {
      return lb;
    }
  } else {
    return ub;
  }
}

double poissonAproxAny(double r1, double r2) {
  if (r2 < 1e-8) {
    return 0.5*exp(-r1)*(exp(-r2)*(1.0+r1+r2)+(1.0+r1));
  }
  double ea, eb;
  eb = exp(-(r1+r2));
  ea = exp(-r1);
  const double r2i = 1.0/r2;
  const double t1 = r2*(1.0+r1);
  const double this = (t1*ea-(t1+r2*r2)*eb)*r2i*r2i; 
  const double val = this+(ea-eb)*r2i;
  eb *= (1.0+r2+r1);
  ea *= (1.0+r1);
  return btw(val, eb, ea);
}

double poissonAproxBoth(double r1, double r2) {
  if (r2 < 1e-8) {
    return 0.5*(exp(-r1)+exp(-(r1+r2)));
  }
  double ea, eb, val;
  eb = exp(-(r1+r2));
  ea = exp(-r1);
  val = (ea-eb)/r2;
  return btw(val, eb, ea);
}

void mexFunction(int nlhs, mxArray *plhs[], int rhs, const mxArray *prhs[]) {
  double *p = (double*)mxGetPr(prhs[0]);
  double *prTx = (double*)mxGetPr(prhs[1]);
  uint64_t *position = (uint64_t*)mxGetPr(prhs[3]);
  const uint64_t maxPosition = (uint64_t)mxGetScalar(prhs[4]);
  double *h = (double*)mxGetPr(mxGetField(prhs[5], 0, "h"));
  double *timeOutProb = (double*)mxGetPr(mxGetField(prhs[5], 0, "timeout_prob"));
  double *endowment = (double*)mxGetPr(mxGetField(prhs[5], 0, "endowment"));

  const mwSize *sizeArray = mxGetDimensions(prhs[3]);
  const unsigned long nPatients = sizeArray[0];
  const unsigned long nTime = sizeArray[1];
  const unsigned long nDonors = sizeArray[2];

  mwSize dims[3] = {nPatients, nTime, nDonors};
  plhs[0] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
  double *q = (double*)mxGetPr(plhs[0]);
  double *expRate, *cumExpRate, *pOffersAny;
  const unsigned long nCum = maxPosition*nDonors;
  expRate = (double*)calloc(nCum, sizeof(double));
  cumExpRate = (double*)malloc(nCum*sizeof(double));
  pOffersAny = (double*)malloc(nCum*sizeof(double));

  /* Create the expiration_rate_ss array */
  for (unsigned long i = 0, index2 = 0; i < nDonors; ++i) {
    for (unsigned long j = 0, index3 = 0; j < nTime; ++j) {
      for (unsigned long k = 0; k < nPatients; ++k, ++index2, ++index3) {
        if (position[index2]) { // skip zero values
          const unsigned long index = position[index2]-1;
          expRate[i+index*nDonors] += p[index3]*h[index3]*(prTx[index2]+timeOutProb[i]);
        }
      }
    }
  }

  /* Create cumulative from exp rate */
  for (unsigned long i = 0; i < nDonors; ++i) {
    cumExpRate[i] = 0.0;
  }
  for (unsigned long i = 1, index1 = nDonors, index2 = 0; i < maxPosition; ++i) {
    for (unsigned long j = 0; j < nDonors; ++j, ++index1, ++index2) {
      cumExpRate[index1] = cumExpRate[index2]+expRate[index2];
    }
  }
  for (unsigned long j = 0, index = 0; j < maxPosition; ++j) {
    for (unsigned long i = 0; i < nDonors; ++i, ++index) {
      if (endowment[i] == 1.0) {
        pOffersAny[index] = poissonAproxBoth(cumExpRate[index], expRate[index]);
      } else {
        pOffersAny[index] = poissonAproxAny(cumExpRate[index], expRate[index]);
      }
    }
  }
  /* assign values to q */
  for (unsigned long i = 0, index2 = 0; i < nDonors; ++i) {
    for (unsigned long j = 0; j < nTime; ++j) {
      for (unsigned long k = 0; k < nPatients; ++k, ++index2) {
        if (position[index2]) {
          const unsigned long index = position[index2]-1;
          q[index2] = pOffersAny[i+index*nDonors]; 
        }
      }
    }
  }
  free(expRate);
  free(cumExpRate);
  free(pOffersAny);
}
