% Generates a series of simple plots

function gamma_plots(data,model,donors,patients,pt_history,chain,options,donorstats,fun);
    
dir = [options.results_folder model.model_name '/figs/' options.cf_altname(model)];
fid = fopen([dir 'gamma_plots.tex'],'w');


% Reference patient;
this_pt_hist = pt_history(1,:);
this_patient = patients(patients.wl_id_code==this_pt_hist.wl_id_code,:);
% reference donor pick donor closest with the average donor
%0 -> ABO = "O", 1 -> ABO = "A", 3 -> ABO = "B", 4 -> ABO = "AB" (codes)
abo_fn = @(x) (x==0)+2*(x==1 | x == 11 | x ==12)+3*(x==2)+4*(x==3 | x==31 | x==32);

% Reference donor
this_donor = donorstats.donor;
this_donor.don_age_lt_18 = this_donor.age_don<19;
this_donor.don_age_18_35 = this_donor.age_don>17 & this_donor.age_don<36;
this_donor.don_age_ge_50 = this_donor.age_don>49;
donor_ref_abo = abo_fn(this_donor.abo_don);
this_patient  = donorstats.patient;
this_pt_hist  = donorstats.pt_history;
this_offer  = donorstats.offers;
reference = 'supply';

% Filename flag - Y labels in plots
add2filename = '';
ylab_str = 'Equivalent Increase in Donor Supply (%)';
if ~strcmp(reference,'supply'), 
    add2filename = ['_' reference]; 
    ylab_str = 'Equivalent Offers';
end
if ~options.av_don_local, add2filename= [add2filename '_nonlocdon']; end


% Set the set of donors of different age;
these_donors = repmat(this_donor,4,1);
these_donors.don_age_lt_18 = [1;0;0;0];
these_donors.don_age_18_35 = [0;1;0;0];
these_donors.don_age_ge_50 = [0;0;0;1];
donors_age_range = {'< 18', '18 to 34', '35 to 49','> 49' };
these_donors.age_don = sort([12;30;45;60]);
these_donors.abo_don = [1;1;1;1].*donor_ref_abo;


%%% Ex-Fig 3: Age Plot
plotname = 'don_pt_age';
for dialysis = [0 1]
    if dialysis, nodial = ''; else nodial = '_nodial'; end
    these_offers            = this_offer;
    evaluate_at_waittime    = 0;
    these_offers.match_date = this_patient.init_date + evaluate_at_waittime;
    plts=struct();
    ages = [20:10:70]';  % Do not go below 18! 
    % The tioli offer for a <18 year old patient will be mispecified 
    % (I need to simulate the experience of a pediatric patient with 
    % the same characteristics as the ref patient!)
    K = size(these_donors,1);
    B = K-1;
    for k = 1:K
        for a = 1:numel(ages)
            pat = this_patient; pat.init_age = ages(a);
            pat.dialysis_date = this_patient.init_date + 365 - 730 * dialysis;
            call_data = call_data_structure(model,these_offers,pat,these_donors(k,:),this_pt_hist,chain,fun,reference);
            plts(k).data.ei(a) = call_data.ei;
        end
        plts(k).legend = ['Donor Age ' donors_age_range{k}];
    end
     


    fram_str = [': AGE ' ylab_str];
    legend_loc = 'northwest';
    if ~strcmp(reference,'supply'),
        legend_loc = 'northeast';
    end
    fname = [dir plotname add2filename nodial '.png'];
    axlim = [20 70 0 6];
    plotit(plts,ages*365,'ei',fname,'Patient Age',ylab_str,legend_loc,axlim)
    write_tex(fid,[model.model_name fram_str],fname)
    writetable(plts2table(plts,ages,'ei','Age'),[dir 'gamma_plots' add2filename '.xls'],'Sheet',plotname);


end


%%% Ex-Fig 4: Age-dialysis x Age 
plotname = 'don_age_dial';
plts=struct();
ages = [25 55 25 55];
dial_date = this_patient.init_date + [365 365 -365 -365];
labels = {'Off Dialysis 0-49', 'Off Dialysis 50+','On Dialysis 0-49', 'On Dialysis 50+'};
K = size(these_donors,1);
B = K-1;
for k = 1:K
    for a = 1:numel(ages)
        pat = this_patient; pat.init_age = ages(a);
        pat.dialysis_date = dial_date(a);
        call_data = call_data_structure(model,this_offer,pat,these_donors(k,:),this_pt_hist,chain,fun,reference);
        plts(k).data.ei(a) = call_data.ei;
    end
    plts(k).legend = ['Donor Age ' donors_age_range{k}];
end




fram_str = [': AGE- DIAL ' ylab_str];
fname = [dir plotname add2filename '.png'];
barit(plts([2 4]),labels,'ei',fname,'Patient Group',ylab_str)
write_tex(fid,[model.model_name fram_str],fname)
writetable(plts2table(plts,labels,'ei','Patient Group'),[dir 'gamma_plots' add2filename '.xls'],'Sheet',plotname);



%%% Ex-Fig 5: Mismatches
plotname = 'don_age_mm';
% Age-mismatches
plts=struct();
these_offers_alt = repmat(this_offer,5,1);
these_offers_alt.a_mm =  [0 2 0 0 1]';
these_offers_alt.b_mm =  [0 0 2 0 1]';
these_offers_alt.dr_mm = [0 0 0 2 1]';
labels = {'Zero', '2 A', '2 B', '2 DR', 'Not 2 DR'};
%
B = 2;

for k = 1:size(these_donors,1)
    plts(k).data = call_data_structure(model,these_offers_alt,this_patient,these_donors(k,:),this_pt_hist,chain,fun,reference);
    plts(k).legend = ['Donor Age ' donors_age_range{k}];
end

fram_str = [': AGE- MM ' ylab_str];
fname = [dir plotname add2filename '.png'];
barit(plts([2 4]),labels,'ei',fname,'Mismatches',ylab_str)
write_tex(fid,[model.model_name fram_str],fname)
writetable(plts2table(plts,labels,'ei','Mismatches'),[dir 'gamma_plots' add2filename '.xls'],'Sheet',plotname);

fclose(fid);
fprintf('gamma_plots.m just finished!!\n')
    
%% Auxilliary functions
function out = call_data_structure(model,offers,patients,donors,pt_histories,chain,fun,reference)

    coefficient_names = [model.fixed.colheaders model.flex.colheaders];
    [data,model] = gen_data_structure(model,[],offers,patients,donors,pt_histories,'value_function');
    chi = chi_param(data,model); 
    out= dataset();
    out.mu = - chi*chain.mu_theta;   % Index;
    out.pr = normcdf(out.mu);        % Probability of acceptance
    
    vinit=fun.continuation_value(pt_histories,patients,0,[]);
    % Value of a tioli offer or cont offer
    vinit=fun.average_offer(pt_histories,patients,vinit);
    % Continuation value at t (if tioli)
    if strcmp(reference,'tioli'), 
        t = these_offers.match_date - this_patient.init_date;
vt=fun.continuation_value(pt_histories,patients,t,[]); 
    end
    switch reference
        case 'tioli', out.ei = ccp_inversion(out.mu + vt)./vinit.tioli;
        case 'cont',  out.ei = ccp_inversion(out.mu)./vinit.cont;
        case 'supply', out.ei = ccp_inversion(out.mu)./vinit.V*100;
    end



function plotit(series,x,what,saveItAs,xlab,ylab,legend_location,ax,fid)
    legnds = {};
    for m=1:numel(series)
        plot(x/365,series(m).data.(what),'LineWidth',3); hold on;
        legnds{m} = series(m).legend;
    end
    if nargin<5, xlab='Years Waited'; end
    if nargin>=6, ylabel(ylab); end
    if nargin<7, legend_location='northeast'; end
    if nargin>7, axis(ax); end
    xlabel(xlab);
    legend(legnds{:},'location',legend_location); 
    saveas(gcf,saveItAs);
    hold off
    

function barit(series,x,what,saveItAs,xlab,ylab,legend_location,fid)
    l = {}; y = [];
    for m=1:numel(series)
        y(:,m) = series(m).data.(what);
        l{m} = series(m).legend;
    end
    b= bar(y);
    shades_color = 0.025 : 0.95/(numel(b)-1) : 0.975;
    for k = 1:numel(b)
        b(k).FaceColor = [shades_color(k) shades_color(k) 1];  % Shades of Blue
    end
    set(gca,'xticklabel',x);
    if nargin<5, xlab='Years Waited'; end
    if nargin>=6, ylabel(ylab); end
    if nargin<7, legend_location='northeast'; end
    xlabel(xlab);
    legend(l{:},'location',legend_location); 
    saveas(gcf,saveItAs);
    hold off        
  
function write_tex(fid,frametitle,filename)   % Writes a slide with the figure.
    frametitle = regexprep(frametitle,'[^a-zA-Z0-9]',' '); % Only alpha-numeric (to avoid latex problems)
    fprintf(fid,'\\begin{frame}\n    \\frametitle{%s}\n    \\includegraphics[scale=0.5]{%s}\n\\end{frame} \n',frametitle,filename);

function tbl = plts2table(series,x,what,xlab)
    if nargin<4, xlab = 'ObsName'; end
    tbl = table(x(:),'VariableNames',{matlab.lang.makeValidName(xlab)});
    for m=1:numel(series)
        tbl.(matlab.lang.makeValidName(series(m).legend)) = series(m).data.(what)(:);
    end
    
