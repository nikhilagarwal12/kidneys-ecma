function object = gen_cf_data(model,don_pat_pairs,pt_history,patients,donors,projest,eta_draws_struct,beta_draws_struct,time_specs,options,all_donors)

    %% B. Calculate NPV of TX for pt-donor pairs for each time
    % Generate simulated offers
    if ~isfield(options,'method'),          options.method = 'increments';                          end
    if ~isfield(options,'increments'),      options.increments = time_specs.t;                         end      
    if ~isfield(options,'offers'),          options.offers = isfield(model,'pos_xmatch');           end
    if ~isfield(options,'store_scores'),    options.store_scores = isfield(don_pat_pairs{1}.Data,'points'); end
    debug.check_every = 100; 
    
    N_patients = size(pt_history,1);
    N_donors   = size(donors,1);
    N_time = size(time_specs.t,2);  
    
    % Inline functions
    place_donors_time = @(row,col,y) full(sparse(row,col,y,N_time,N_donors));
    flat = @(x) x(:);
    
    
    % Preallocate space 
    object = cf_data(); % Generate an empty instance of class cf_data 
    object.proj.Gamma_eta    = zeros(N_patients,N_time,N_donors);
    object.calc.Gamma_eta    = zeros(N_patients,N_time,N_donors);
    object.calc.Gamma        = zeros(N_patients,N_time,N_donors);
    object.opt_assignment.chi = zeros(N_patients,numel(options.chi_times),N_donors,model.fixed.K);
    object.lyft         = zeros(N_patients,N_time,N_donors);
    object.proj.V            = zeros(N_patients,N_time);
    object.calc.V            = zeros(N_patients,N_time);
    object.pr_compat    = zeros(N_patients,1,N_donors);  % pr_compat = compat .* (1-pos_xmatch);
    object.V_init       = zeros(N_patients,1);
    if options.store_scores    
        object.bins     = zeros(N_patients,N_time,N_donors);
        object.points   = zeros(N_patients,N_time,N_donors);
        object.kas_bins     = zeros(N_patients,N_time,N_donors);
        object.kas_points   = zeros(N_patients,N_time,N_donors);
    end


    % Iterate over pt_histories
    display('***********************************');
    display('Constructing the data structure ...');
    fprintf('Donors: %d; Patients: %d; Periods: %d\n', N_donors, N_patients, N_time)
    [istrue,where_patients] = ismember(pt_history.wl_id_code,patients.wl_id_code);
    % Find the init ages of pt_histories
    init_age_in_pd = double(max(patients.init_age(where_patients)*time_specs.pd_per_year,1));
    assert(all(istrue));
        
    voptions.edraws = 40; voptions.tdraws = 400; voptions.approach='numeric_integration';
    voptions.iterate_over = 'patients';  voptions.verbose = 0; 
        
    loop_timer =tic;
    for hh = 1:size(pt_history)
        this_history = pt_history(hh,:);
        off = offerset(don_pat_pairs,this_history,patients,donors,[]);
        % Exclude offers
        off = off.excludeBin0;
        if size(off.data,1)==0, continue; end
        % Add match dates
        these_increments = options.increments(init_age_in_pd(hh)*365.25/time_specs.pd_per_year+options.increments<=time_specs.max_T+1);
        off = matchDates(off,options.method,these_increments);
        % PreKas & KAS
        off = off.cPREKAS.cKAS;
        cf_data_hh = gen_data_structure(model,[],off.subsetvars,patients,donors,pt_history,'value_function');
        
        % Go on as usual;
        [istrue,where_donor] = ismember(cf_data_hh.donor_id,donors.donor_id); assert(all(istrue));
        % debugging: make sure there are no reps;
        if mod(hh,debug.check_every)==0, u = unique([off.indices.incr where_donor], 'rows', 'first'); assert(size(u,1) == numel(where_donor)); fprintf('.'); end

        assert(max(off.indices.incr+init_age_in_pd(hh)-1)==N_time);
        pdt = @(x) place_donors_time(off.indices.incr+init_age_in_pd(hh)-1,where_donor,x);

        % Assign V and Gamma;
        if exist('all_donors','var')   % Do not repeat for different splits of the data
	    if ~model.pt_uh
                V_temp = pdt(cf_data_hh.fixed.obs(:,projest.vf)*projest.V_coeff); 
                V_temp = V_temp(:,unique(where_donor));  % We carry a lot of zeros around.
		assert(~any(std(V_temp,0,2)>1e-10));
                object.proj.V(hh,:)           = mean(V_temp,2);
                object.proj.Gamma_eta(hh,:,:) = pdt(cf_data_hh.fixed.obs*projest.gamma_coeff);
	    end
            chain.mu_theta                = projest.theta;
	    [c_V,gamma,chi,V_init]        = value_function(don_pat_pairs,cf_data_hh,model,all_donors,patients,this_history,off.data,chain,eta_draws_struct,beta_draws_struct.beta_draws,voptions);
            object.calc.Gamma(hh,:,:) = pdt(gamma);
            object.calc.Gamma_eta(hh,:,:) = object.calc.Gamma(hh,:,:);
            V_temp                        = pdt(c_V);
            object.calc.V(hh,:)           = max(V_temp,[],2);
            object.V_init(hh)             = V_init.V;
            for tt = 1:size(V_temp,1)                    % Check V is constant across donors
                  v = V_temp(tt,:); v = round(v(v>0),7); % Non-zero v rounded up to the 7th decimal
                  if numel(unique(v)) >1, keyboard; end  % Debug if we have more than one value for V.
            end
        end   % if exist('all_donors','var')
        
        % Store subset of chi for opt_assignment
        for k =1:size(cf_data_hh.fixed.obs,2)
            all_chi = pdt(cf_data_hh.fixed.obs(:,k));
            object.opt_assignment.chi(hh,:,:,k) = all_chi(options.chi_times+init_age_in_pd(hh),:);
        end

        % Store subset of chi for opt_assignment
        for k =1:size(cf_data_hh.fixed.obs,2)
            all_chi = pdt(cf_data_hh.fixed.obs(:,k));
            object.opt_assignment.chi(hh,:,:,k) = all_chi(options.chi_times+init_age_in_pd(hh),:);
        end

        % Compatibility
        object.pr_compat(hh,1,where_donor) = 1 - pt_history.cpra(hh);

        % Storing scores from current mechanism, if requested
        if options.store_scores
            object.bins(hh,:,:)   = pdt(off.cbins(cf_data_hh.offer_index));
            object.points(hh,:,:) = pdt(off.cpoints(cf_data_hh.offer_index));
            object.kas_bins(hh,:,:)   = pdt(off.kas_cbins(cf_data_hh.offer_index));
            object.kas_points(hh,:,:) = pdt(off.kas_cpoints(cf_data_hh.offer_index));
        end
        % Positive x_match
        if isfield(model,'pos_xmatch'), 
            xmatch_temp=double(predict_pos_xmatch(off.data,this_history,patients, model.pos_xmatch.coefs));
            xmatch_temp=pdt(xmatch_temp);
            object.pr_compat(hh,1,:) = flat(object.pr_compat(hh,1,:)) .* flat(xmatch_temp(end,:));
            object.pr_compat(hh,1,where_donor) = max(object.pr_compat(hh,1,where_donor),1e-3);
        end
    end
    toc(loop_timer)
    %% (OOP)  This function should end here and call the data object.
        % Add on the eta piece if donor uh
    if model.donor_uh & exist('all_donors','var') % all_donors is passed only for the 'all' sample, not for the splits
        [istrue,locb] = ismember(donors.donor_id,eta_draws_struct.donor_id);
        assert(all(istrue));
        object.proj.Gamma_eta = bsxfun(@plus,object.proj.Gamma_eta,-reshape(eta_draws_struct.eta_draws(locb),[1,1,N_donors]));
        object.calc.Gamma_eta = bsxfun(@plus,object.calc.Gamma,-reshape(eta_draws_struct.eta_draws(locb),[1,1,N_donors]));
    end

    
    % Get the number of kidneys, and variables on the number of offers for each donor
    object.donor_options.N_ki_don              = donors.ki_available_nyrt;
    object.donor_options.offers.mean_log       = donors.mean_log_offers;
    object.donor_options.offers.sd_log         = donors.sd_log_offers;
    object.donor_options.offers.timeout_hazard = donors.timeout_hazard;
    object.donor_options.offers.p_discard      = donors.p_discard;
    object.donor_options.offers.local_donor    = donors.opo_dsa_id == model.local_opo;
 
    object.opt_assignment.offset_gamma = bsxfun(@plus,options.chi_times,init_age_in_pd);
    

    display('Data structure completed ...');    

    %% C. Patient Entry, Departure Rates and Compatibility   
    if ~all(sum(squeeze(object.pr_compat),2)>0)
        warning(['Error! Sampled a patient that is not compatible ' ...
                 'with any of the sampled donors!']);
    end

    % Generate pt_entry_weights
    if ~isfield(options,'pt_entry_weights')
        object.pt_entry_weights = zeros(N_patients,N_time);
   
        % Get the entry indices and set the entry weights
        entry_indices = sub2ind([N_patients,N_time],(1:N_patients)',init_age_in_pd);        
        object.pt_entry_weights(entry_indices) = 1/length(entry_indices);
    else
        object.pt_entry_weights = options.pt_entry_weights;
    end
    

    % Check that we expect the queue to be longer than 1 person
    if ischar(model.q_leave)
        % Obtain the hazard function
        haz_fn = hazard_fn(model.hazard.distribution,model.hazard.param);
        % Obtain the baseline parameters for the hazard function
        [istrue,where_patients] = ismember(pt_history.wl_id_code,patients.wl_id_code);
        assert(all(istrue));
        pt_dep_rate_params = patients.([model.q_leave '_dep_rate'])(where_patients);

        % Populate object.q_leave_exog for each patient and each time_specs.t
        object.q_leave_exog = zeros(N_patients,N_time);
        object.p_leave_in_pd = zeros(N_patients,N_time);
        init_age_in_d = init_age_in_pd*365.25/time_specs.pd_per_year;
        for tt = 1:N_time
            if tt<N_time
                % Compute the wait time
                wait_time = (time_specs.t(tt+1)-init_age_in_d).*((time_specs.t(tt+1)-init_age_in_d)>0);
            else
                wait_time = (time_specs.max_T-init_age_in_d).*((time_specs.max_T-init_age_in_d)>0);
            end

            % Compute hazard rates
            object.wait_time(:,tt) = wait_time;
            object.q_leave_exog(:,tt) = haz_fn.hazard(wait_time,pt_dep_rate_params);
            % Probability of leaving in this period (not used anymore)
            %object.p_leave_in_pd(:,tt) = 1-sum(object.p_leave_in_pd(:,1:tt-1),2)-haz_fn.survival(wait_time,pt_dep_rate_params);
        end
    else
        % q_leave based on a constant rate
        object.q_leave_exog = model.q_leave*ones(N_patients,N_time);
    end

    % Diagnose V, gamma and ccps
    compat=bsxfun(@times,object.pr_compat>0,ones(1,size(object.calc.Gamma_eta,2),1));

end
