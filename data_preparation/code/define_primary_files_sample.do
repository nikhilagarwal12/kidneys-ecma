// This script contains code to define the samples of patients, donor, and offers
// Key outputs are:
//  - sample_offers.dta
//  - sample_donor_ids.dta
//  - sample_wl_id_codes.dta

************************************
********* sample_donor_ids *********
************************************

// Donor sample: every donor_id in PTR_KI that is recorded in DECEASED_DONOR_DATA, within sample period
append_ptr_files `ptr_start_year' `sample_end_year' "`offer_data'"
keep if match_submit_dt >= `sample_start_date' & match_submit_dt <= `sample_end_date'

save "`primary_files'ptr_primary", replace  // erased at end of primary files script

keep donor_id match_submit_dt
bys donor_id (match_submit_dt): keep if _n==1 // earliest match date
merge 1:m donor_id using "`deceased_donor_data'DECEASED_DONOR_DATA.DTA"
keep if _merge == 3
drop _merge

if `test_sample'==1{
  preserve
  keep if match_submit_dt >= `test_sample_start_date' & match_submit_dt <= `test_sample_end_date'
  keep donor_id
  sort donor_id
  duplicates drop
  save "`test_sample_files'sample_donor_ids.dta", replace
  restore
}

keep donor_id
duplicates drop
codebook donor_id
save "`primary_files'sample_donor_ids.dta", replace

**************************************
********* sample_wl_id_codes *********
* Patient sample: every wl_id_code in 
* KIDPAN_WLHISTORY_DATA who was not 
* deleted before sample_start_date or 
* added after sample_end_date 
**************************************

// First store patients with certain removal codes where we don't think the patient was really in the market for a kidney
use "`kidpan_data'KIDPAN_DATA.DTA", clear
keep if rem_cd == 6 | rem_cd == 12 | rem_cd == 22 | rem_cd == 24
drop if wl_id_code == .
keep wl_id_code
duplicates drop
tempfile wl_ids_drop
save `wl_ids_drop', replace

// Patient history record filter
use "`kidpan_data'Waiting List History/KIDPAN_WLHISTORY_DATA.DTA", clear
keep if unos_cand_stat_cd > 4000 & unos_cand_stat_cd < 5000  // only KI candidate obs; drop KP and PA
gen deletion_before_start_date = chg_ty == "D" & chg_date < `sample_start_date' & `sample_start_date' != .
gen addition_after_end_date = chg_ty == "A" & chg_date > `sample_end_date' & `sample_end_date' != .
bys wl_id_code: egen deleted_before = total(deletion_before_start_date)
bys wl_id_code: egen added_after = total(addition_after_end_date)
keep if deleted_before == 0 & added_after == 0

// filter out patients with removal codes we are omitting
merge m:1 wl_id_code using `wl_ids_drop', keep(master match)
drop if _merge == 3
drop _merge

if `test_sample'==1{
  preserve
  drop deletion_before_start_date addition_after_end_date deleted_before added_after
  gen deletion_before_start_date = chg_ty == "D" & chg_date < `test_sample_start_date' & `test_sample_start_date' != .
  gen addition_after_end_date = chg_ty == "A" & chg_date > `test_sample_end_date' & `test_sample_end_date' != .
  bys wl_id_code: egen deleted_before = total(deletion_before_start_date)
  bys wl_id_code: egen added_after = total(addition_after_end_date)
  keep if deleted_before == 0 & added_after == 0
  keep wl_id_code
  duplicates drop
  save "`test_sample_files'sample_wl_id_codes.dta", replace
  restore
}

keep wl_id_code
sort wl_id_code
duplicates drop
codebook wl_id_code
save "`primary_files'sample_wl_id_codes.dta", replace


// create a table of kidpan variables to flag problem people
use "`kidpan_data'KIDPAN_DATA.DTA", clear
keep wl_id_code activate_date init_date dialysis_date end_date death_date tx_date
keep if wl_id_code ~= .
isid wl_id_code
tempfile tmp0000
save `tmp0000'
  


*************************************
*********** sample_offers ***********
* Offer sample: all offers from the 
* donor sample to patients in the 
* patient sample, including bypasses, 
* during sample period
* Also, correct for offers made to 
* dead patients. This info comes from 
* KIDPAN
*************************************

use "`primary_files'ptr_primary", clear
keep donor_id wl_id_code match_id_code ptr_row_order ptr_sequence_num pb_offer_seq match_submit_dt organ_placed prime_opo_refusal_id

// first, filter out donor_id's that aren't in sample (don't have a DECEASED_DONOR record)
merge m:1 donor_id using "`primary_files'sample_donor_ids.dta"
keep if _merge == 3
drop _merge
codebook donor_id

// second, filter out missing wl_id_code's and other patients not found in WLHISTORY
sort wl_id_code
merge m:1 wl_id_code using "`primary_files'sample_wl_id_codes.dta"
// keep if _merge == 3 | payback==1  // payback offers don't have wl_id_codes
keep if _merge == 3
drop _merge
codebook wl_id_code

// third, flag offers that were made to deceased or transplanted patients (before their WLHISTORY records were updated)
merge m:1 wl_id_code using `tmp0000'
keep if _merge == 3

gen time_since_death = match_submit_dt - death_date  // death filter
summ time_since_death, d
gen offer_to_dead_patient = (time_since_death > 0 & time_since_death != .)

gen time_since_tx = match_submit_dt - tx_date  // transplant filter
summ time_since_tx, d
gen offer_to_transplanted_patient = (time_since_tx > 0 & time_since_tx != .)

keep donor_id match_id_code ptr_row_order ptr_sequence_num pb_offer_seq offer_to_dead offer_to_transplanted match_submit_dt
sort donor_id match_id_code ptr_row_order pb_offer_seq
save "`primary_files'sample_offers.dta", replace

if `test_sample'==1{
  keep if match_submit_dt >= `test_sample_start_date' & match_submit_dt <= `test_sample_end_date'
  save "`test_sample_files'sample_offers.dta", replace
}
