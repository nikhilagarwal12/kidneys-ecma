% Function to calculate mean-squared prediction error of CCP model
% Written for out-of-sample validation exercise
% Should only be called without patient UH

% 'eta_object' should be a struct with fields 'eta0','eta1','eta2','index0','index1','index2'
% 0 <-> comparison sample (subset of full estimation sample)
% 1 <-> validation sample (not used for estimation)
% 2 <-> full estimation sample

function fit_table = compute_mse(data0,data1,data,eta_object,theta,filepath,model_name)

   % Estimation Sample
   mu = data.fixed.obs(eta_object.index2,:) * theta;
   p_hat = mean(normcdf(repmat(mu,1,size(eta_object.eta2,2)) + eta_object.eta2), 2);
   y = data.cont_choice(eta_object.index2);
   mean_est = mean(y);

   fit2 = fit_object(y,p_hat,mean_est);

   % Comparison Sample
   mu = data0.fixed.obs(eta_object.index0,:) * theta;
   p_hat = mean(normcdf(repmat(mu,1,size(eta_object.eta0,2)) + eta_object.eta0) , 2);
   y = data0.cont_choice(eta_object.index0);

   fit0 = fit_object(y,p_hat,mean_est);

   % Validation Sample
   mu = data1.fixed.obs(eta_object.index1,:) * theta;
   p_hat = mean(normcdf(repmat(mu,1,size(eta_object.eta1,2)) + eta_object.eta1), 2);
   y = data1.cont_choice(eta_object.index1);

   fit1 = fit_object(y,p_hat,mean_est);

    % make a comparison table
    results = [fit0.mse fit1.mse fit2.mse; ...
		   fit0.rmse fit1.rmse fit2.rmse; ...
		   fit0.abserr fit1.abserr fit2.abserr; ...
		   fit0.mse_mean fit1.mse_mean fit2.mse_mean; ...
		   fit0.rmse_mean fit1.rmse_mean fit2.rmse_mean; ...
		   fit0.abserr_mean fit1.abserr_mean fit2.abserr_mean; ...
		   fit0.y_bar fit1.y_bar fit2.y_bar; ...
		   fit0.p_hat_bar fit1.p_hat_bar fit2.p_hat_bar; ...
		   fit0.y_std fit1.y_std fit2.y_std; ...
		   fit0.p_std fit1.p_std fit2.p_std;...
	           fit0.N fit1.N fit2.N];
fit_table = array2table(results,'RowNames',{'MSE, CCP Estimator','RMSE, CCP Est.','Abs. Error, CCP Est.','MSE, Mean Estimator','RMSE, Mean Est.','Abs. Err., Mean Est.','Mean Dep. Var','Mean CCP','S.D. Dep. Var','S.D. CCP','# Observations'},'VariableNames',{'Comparison_Sample','Validation_Sample','Estimation_Sample'});
    writetable(fit_table,filepath,'Sheet',model_name,'WriteRowNames',true);


function fit = fit_object(y,p_hat,mu)

fit.y_bar = mean(y);
fit.p_hat_bar = mean(p_hat);
fit.y_std = std(y);
fit.p_std = std(p_hat);

fit.mse = mean((y-p_hat).*(y-p_hat));
fit.rmse = sqrt(fit.mse);
fit.abserr = mean(abs(y-p_hat));
fit.mse_mean = mean((y-mu).*(y-mu));
fit.rmse_mean = sqrt(fit.mse_mean);
fit.abserr_mean = mean(abs(y-mu));
fit.N = length(y);
