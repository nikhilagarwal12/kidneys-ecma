#include "common.h"

#include "mex.h"

#include <math.h>
#include <string.h>
#include <stdint.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[0], 0, "mu"));
  const long nPatients = sizeArray[0];
  const long nTime = sizeArray[1];
  const long nDonors = sizeArray[2];
  const uint64_t firstBest = *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "first_best")));
  double *includePattern = (double*)mxGetPr(mxGetField(prhs[0], 0, "include_pattern"));
  uint64_t isSmall = 0;
  if (includePattern != NULL) {
    isSmall = 1;
  }
  const long nQ = nPatients*nTime*nDonors;
  const long nP = nPatients*nTime;
  const long nTm1nP = nP-nPatients;

  // First create a sparse matrix in COO format and convert to matlab format
  long *iIndex, *jIndex;
  double *value;
  // Calculate maximum number of non-zeros and allocate memeory
  long nnz;
  if (isSmall) {
    const uint64_t qCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "qCount")));
    const uint64_t pCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "pCount")));
    const uint64_t donorCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "donorCount")));
    if (firstBest) {
      nnz = pCount*(donorCount*donorCount+2)+qCount*4;
    } else {
      nnz = pCount*(donorCount*donorCount+3)+qCount*4;
    }
  } else {
    if (firstBest) {
      nnz = nQ*(nDonors+4l)+2l*nP;
    } else {
      nnz = nP*(nDonors*(nDonors+4)+3);
    }
  }
  iIndex = (long*)malloc(nnz*sizeof(long));
  jIndex = (long*)malloc(nnz*sizeof(long));
  value = (double*)malloc(nnz*sizeof(double));
  unsigned long int currentIndex = 0;
  if (isSmall) {
    const uint64_t qCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "qCount")));
    const uint64_t pCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "pCount")));
    const uint64_t pM1Count =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "pM1Count")));
    const uint64_t *map =  (uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "map"));
    uint64_t *filterPattern = (uint64_t*)mxGetPr(prhs[1]);
    uint64_t *donorList1D =  (uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "donorList"));
    const unsigned long nDonorsP1 = nDonors + 1;
    const long matrixSize = qCount+2*pCount;
    if (firstBest) {
      uint64_t* dList = donorList1D;
      for (unsigned long k = 0; k < pM1Count; ++k, dList += nDonorsP1) {
        const unsigned long donorCount = dList[0];
        for (unsigned long i = 1; i < donorCount; ++i) {
          const unsigned long index1 = dList[i];
          for (unsigned long j = 1; j < donorCount; ++j) {
            const unsigned long index2 = dList[j];
            iIndex[currentIndex] = index1;
            jIndex[currentIndex++] = index2;
          }
        }
      }
      for (unsigned long k = pM1Count; k < pCount; ++k, dList += nDonorsP1) {
        const unsigned long donorCount = dList[0];
        for (unsigned long i = 1; i < donorCount; ++i) {
          const unsigned long index1 = dList[i];
          iIndex[currentIndex] = index1;
          jIndex[currentIndex++] = index1;
        }
      }
      // Add qp and pq Values
      dList = donorList1D;
      for (unsigned long j = 0; j < pCount; ++j, dList += nDonorsP1) {
        const uint64_t* dList = donorList1D+j*nDonorsP1;
        const unsigned long donorCount = dList[0];
        const unsigned long index2 = j+qCount;
        for (unsigned long i = 1; i < donorCount; ++i) {
          const unsigned long index1 = dList[i];
          iIndex[currentIndex] = index1;
          jIndex[currentIndex++] = index2;
          iIndex[currentIndex] = index2;
          jIndex[currentIndex++] = index1;
        }
      }
      // Add qv and vq values
      const unsigned long offset = nQ+nP;
      for (unsigned long i = 0, offset1 = 0; i < nDonors; ++i, offset1 += nP) {
        for (unsigned long j = 0; j < nP; ++j) {
          const unsigned long index1 = j+offset1;
          const unsigned long index2 = offset+j;
          if (filterPattern[index1] && filterPattern[index2]) {
            iIndex[currentIndex] = map[index1];
            jIndex[currentIndex++] = map[index2];
            iIndex[currentIndex] = map[index2];
            jIndex[currentIndex++] = map[index1];
          } 
        }
      }
      // Add pv and vp values
      const unsigned long offset1 = pCount+qCount;
      for (unsigned long i = 0; i < pCount; ++i) {
        const unsigned long index1 = i+qCount;
        const unsigned long index2 = i+offset1;
        iIndex[currentIndex] = index1;
        jIndex[currentIndex++] = index2;
        iIndex[currentIndex] = index2;
        jIndex[currentIndex++] = index1;
      }
    } else {
      // Add qq Values
      uint64_t* dList = donorList1D;
      for (unsigned long k = 0; k < pM1Count; ++k, dList += nDonorsP1) {
        const unsigned long donorCount = dList[0];
        for (unsigned long i = 1; i < donorCount; ++i) {
          const unsigned long index1 = dList[i];
          for (unsigned long j = 1; j < donorCount; ++j) {
            const unsigned long index2 = dList[j];
            iIndex[currentIndex] = index1;
            jIndex[currentIndex++] = index2;
          }
        }
      }
      // Add qp and pq Values
      dList = donorList1D;
      for (unsigned long j = 0; j < pCount; ++j, dList += nDonorsP1) {
        const unsigned long donorCount = dList[0];
        const unsigned long index2 = j+qCount;
        for (unsigned long i = 1; i < donorCount; ++i) {
          const unsigned long index1 = dList[i];
          iIndex[currentIndex] = index1;
          jIndex[currentIndex++] = index2;
          iIndex[currentIndex] = index2;
          jIndex[currentIndex++] = index1;
        }
      }
      // Add qv and vq values
      dList = donorList1D;
      for (unsigned long j = 0; j < pCount; ++j, dList += nDonorsP1) {
        const unsigned long donorCount = dList[0];
        const unsigned long index2 = j+qCount+pCount;
        for (unsigned long i = 1; i < donorCount; ++i) {
          const unsigned long index1 = dList[i];
          iIndex[currentIndex] = index1;
          jIndex[currentIndex++] = index2;
          iIndex[currentIndex] = index2;
          jIndex[currentIndex++] = index1;
        }
      }
      // Add pv and vp values
      const unsigned long offset = pCount+qCount;
      for (unsigned long i = 0; i < pCount; ++i) {
        const unsigned long index1 = i+qCount;
        const unsigned long index2 = i+offset;
        iIndex[currentIndex] = index1;
        jIndex[currentIndex++] = index2;
        iIndex[currentIndex] = index2;
        jIndex[currentIndex++] = index1;
      }
      // Add vv values
      for (unsigned long i = offset; i < matrixSize; ++i) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex++] = i;
      }
    }
    for (unsigned long i = 0; i < currentIndex; ++i) {
      value[i] = 1.0;
    }
    // Create output sparse matrix
    plhs[0] = mxCreateSparse(matrixSize, matrixSize, currentIndex, mxREAL);
    double *sr  = (double*)mxGetPr(plhs[0]);
    size_t* irs = (size_t*)mxGetIr(plhs[0]);
    size_t* jcs = (size_t*)mxGetJc(plhs[0]);
    COO_toCSC(matrixSize, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
  } else {
    const long matrixSize = nQ+2*nP;
    if (firstBest) {
      // Add qq Values
      for (unsigned long k = 0; k < nTm1nP; ++k) {
        for (unsigned long i = 0, offset1 = 0; i < nDonors; ++i, offset1 += nP) {
          for (unsigned long j = 0, offset2 = 0; j < nDonors; ++j, offset2 += nP) {
            iIndex[currentIndex] = offset1+k;
            jIndex[currentIndex++] = offset2+k;
          }
        }
      }
      for (unsigned long k = nTm1nP; k < nP; ++k) {
        for (unsigned long i = 0, offset1 = 0; i < nDonors; ++i, offset1 += nP) {
          iIndex[currentIndex] = offset1+k;
          jIndex[currentIndex++] = offset1+k;
        }
      }
      // Add qp and pq Values
      for (unsigned long i = 0, offset = 0; i < nDonors; ++i, offset += nP) {
        for (unsigned long j = 0; j < nP; ++j) {
          iIndex[currentIndex] = j+offset;
          jIndex[currentIndex++] = j+nQ;
          iIndex[currentIndex] = j+nQ;
          jIndex[currentIndex++] = j+offset;
        }
      }
      // Add qv and vq values
      const unsigned long offset1 = nQ+nP;
      for (unsigned long i = 0, offset = 0; i < nDonors; ++i, offset += nP) {
        for (unsigned long j = 0; j < nP; ++j) {
          iIndex[currentIndex] = j+offset;
          jIndex[currentIndex++] = j+offset1;
          iIndex[currentIndex] = j+offset1;
          jIndex[currentIndex++] = j+offset;
        }
      }
      // Add pv and vp values
      for (unsigned long i = nQ; i < offset1; ++i) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex++] = i+nP;
        iIndex[currentIndex] = i+nP;
        jIndex[currentIndex++] = i;
      }
    } else {
      // Add qq Values
      for (unsigned long k = 0; k < nTm1nP; ++k) {
        for (unsigned long i = 0, offset1 = 0; i < nDonors; ++i, offset1 += nP) {
          for (unsigned long j = 0, offset2 = 0; j < nDonors; ++j, offset2 += nP) {
            iIndex[currentIndex] = offset1+k;
            jIndex[currentIndex++] = offset2+k;
          }
        }
      }
      // Add qp and pq Values
      for (unsigned long i = 0, offset = 0; i < nDonors; ++i, offset += nP) {
        for (unsigned long j = 0; j < nP; ++j) {
          iIndex[currentIndex] = j+offset;
          jIndex[currentIndex++] = j+nQ;
          iIndex[currentIndex] = j+nQ;
          jIndex[currentIndex++] = j+offset;
        }
      }
      // Add qv and vq values
      unsigned long offset = nQ+nP;
      for (unsigned long i = 0, offset1 = 0; i < nDonors; ++i, offset1 += nP) {
        for (unsigned long j = 0; j < nP; ++j) {
          iIndex[currentIndex] = j+offset1;
          jIndex[currentIndex++] = j+offset;
          iIndex[currentIndex] = j+offset;
          jIndex[currentIndex++] = j+offset1;
        }
      }
      // Add pv and vp values
      for (unsigned long i = nQ; i < offset; ++i) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex++] = i+nP;
        iIndex[currentIndex] = i+nP;
        jIndex[currentIndex++] = i;
      }
      // Add vv values
      for (unsigned long i = offset; i < matrixSize; ++i) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex++] = i;
      }
    }
    for (unsigned long i = 0; i < currentIndex; ++i) {
      value[i] = 1.0;
    }
    // Create output sparse matrix
    plhs[0] = mxCreateSparse(matrixSize, matrixSize, currentIndex, mxREAL);
    double *sr  = (double*)mxGetPr(plhs[0]);
    size_t* irs = (size_t*)mxGetIr(plhs[0]);
    size_t* jcs = (size_t*)mxGetJc(plhs[0]);
    COO_toCSC(matrixSize, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
  }

  // Free all allocated memory
  free(iIndex);
  free(jIndex);
  free(value);
}
