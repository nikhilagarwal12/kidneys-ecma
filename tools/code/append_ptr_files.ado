// This function takes a range of years and creates a dataset of kidney offers from those years
// It omits some variables and compresses each file to save memory

program append_ptr_files

args start_year end_year offer_data

local start_append_year = `start_year' + 1
tempfile offer_stock

use "`offer_data'ki_ptr_`start_year'.DTA", clear
drop sec_opo_refusal_ostxt txctr_refusal_ostxt sec_txctr_refusal_ostxt
compress

if `end_year'>`start_year'{
  save `offer_stock', replace
  forvalues x = `start_append_year'/`end_year'{
    use "`offer_data'ki_ptr_`x'.DTA"
    drop sec_opo_refusal_ostxt txctr_refusal_ostxt sec_txctr_refusal_ostxt
    compress
    append using `offer_stock'
    if `x' < `end_year'{
      save `offer_stock', replace
    }
  }
}

end
