#include "common.h"

#include "mex.h"

#include <math.h>
#include <stdint.h>
#include <string.h>

// A bunch of static variables to remove reallocation of arrays
static int notInitialized = 1;
static long *iIndex, *jIndex;
static double *value;
static double *expTerm, *Fz, *eMax, *feas, *qFeas, *qfz, *pExpTerm;
static double *aK, *pExpTermLh;

void cleanup(void) {
  mexPrintf("MEX-file is terminating, destroying allocations in analytic small second Jacobian\n");
  free(iIndex);
  free(jIndex);
  free(value);
  free(expTerm);
  free(pExpTerm);
  free(pExpTermLh);
  free(qfz);
  free(Fz);
  free(feas);
  free(qFeas);
  free(eMax);
  free(aK);
  notInitialized = 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  double *discountV2 = (double*)mxGetPr(mxGetField(prhs[1], 0, "discount_V2"));
  double *arrivals = (double*)mxGetPr(mxGetField(prhs[1], 0, "arrivals"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[1], 0, "mu"));
  const long nPatients = sizeArray[0];
  const long nTime = sizeArray[1];
  const long nDonors = sizeArray[2];
  
  const long nQ = nPatients*nTime*nDonors;
  const long nP = nPatients*nTime;
  const unsigned long nTm1 = nTime-1;
  const unsigned long nTm1nP = nTm1*nPatients;
  const double *q = (double*)mxGetPr(prhs[0]);

  /* Part to filter for smaller problem */
  double *sigmaFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "sigmaFiltered"));
  double *deltaBy_hFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "delta_x_hFiltered"));
  double *muFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "muFiltered"));
  double *sigmaFilteredInv = (double*)mxGetPr(mxGetField(prhs[1], 0, "sigmaFilteredInv"));
  double *lambda_hOverKFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "lambdah_over_KFiltered"));
  double *endowmentFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "endowmentFiltered"));
  double *discountW_Filtered = (double*)mxGetPr(mxGetField(prhs[1], 0, "discount_wFiltered"));
  double *cArrayRepeatFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "pr_compat_repeatFiltered"));
  double *hFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "hFiltered"));
  double *timeOutProbFiltered =  (double*)mxGetPr(mxGetField(prhs[1], 0, "timeout_probFiltered"));
  const uint64_t qCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "qCount")));
  const uint64_t pCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "pCount")));
  const uint64_t pM1Count =  *((uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "pM1Count")));
  const uint64_t donorCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "donorCount")));
  const double nDonorsInv =  (double)mxGetScalar(mxGetField(prhs[1], 0, "nDonorsInv"));
  const uint64_t *npIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "npIndex"));
  const uint64_t *mapEq =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "mapEq"));
  const uint64_t *map =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "map"));
  const uint64_t *mapF =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "mapF"));
  const uint64_t *donorIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "donorIndex"));

  uint64_t *filterPattern = (uint64_t*)mxGetPr(prhs[2]);
  uint64_t *filterPaTi = filterPattern + nQ;
  uint64_t *filterDonors = (uint64_t*)mxGetPr(prhs[3]);

  const double *p = q+qCount;
  const double *v = p+pCount;

  if (notInitialized) {
    unsigned long sum;
    if (pCount > qCount/5.0) {
      sum = 5l*pCount;
    } else {
      sum = qCount;
    }
    const unsigned long nnz = 2l*qCount+sum;
    iIndex = (long*)malloc(nnz*sizeof(long));
    jIndex = (long*)malloc(nnz*sizeof(long));
    value = (double*)malloc(nnz*sizeof(double));
    expTerm = (double*)malloc(pM1Count*sizeof(double));
    pExpTerm = (double*)malloc(pM1Count*sizeof(double));
    pExpTermLh = (double*)malloc(pM1Count*sizeof(double));
    Fz = (double*)malloc(qCount*sizeof(double));
    feas = (double*)malloc(qCount*sizeof(double));
    qFeas = (double*)malloc(qCount*sizeof(double));
    qfz = (double*)malloc(qCount*sizeof(double));
    eMax = (double*)malloc(qCount*sizeof(double));
    aK = (double*)malloc(pCount*sizeof(double));
    mexAtExit(cleanup);
    notInitialized = 0;
  }
  memset(expTerm, 0, pM1Count*sizeof(double));
  for (unsigned long i = 0; i < qCount; ++i) {
    const double timeOutProbLocal = timeOutProbFiltered[donorIndex[i]];
    const unsigned long nIndex = npIndex[i];
    const double sigmaInv = sigmaFilteredInv[nIndex];
    const double zLocal = (muFiltered[i]-v[nIndex])*sigmaFilteredInv[nIndex];
    const double fzLocal = normPdf(zLocal)*cArrayRepeatFiltered[i];
    Fz[i] = normCdf(zLocal)*cArrayRepeatFiltered[i];
    feas[i] = hFiltered[nIndex]*(Fz[i]+timeOutProbLocal);
    qFeas[i] = feas[i]*q[i];
    if (nIndex < pM1Count) {
      expTerm[nIndex] += q[i]*Fz[i]; 
    }
    eMax[i] = sigmaFiltered[nIndex]*(Fz[i]*zLocal+fzLocal);
    qfz[i] = q[i]*fzLocal*sigmaInv;
  }
  for (unsigned long i = 0; i < pCount; ++i) {
    aK[i] = discountW_Filtered[i]*nDonorsInv; 
  }
  for (unsigned long i = 0; i < pM1Count; ++i) {
    expTerm[i] = exp(deltaBy_hFiltered[i]-lambda_hOverKFiltered[i]*expTerm[i]);
    pExpTerm[i] = p[i]*expTerm[i];
    pExpTermLh[i] = pExpTerm[i]*lambda_hOverKFiltered[i];
  }
  // matrix c
  plhs[0] = mxCreateDoubleMatrix(donorCount, 1, mxREAL);
  double *c = (double*)mxGetPr(plhs[0]);
  /* Contribution from feasibility */
  for (unsigned long i = 0; i < nDonors; ++i) {
    if (filterDonors[i]) {
      double sum = 0.0;
      for (unsigned long j = 0, index1 = i*nP; j < nP; ++j, ++index1) {
        if (filterPattern[index1]) {
          const unsigned long index2 = map[index1];
          sum += p[mapEq[j]]*qFeas[index2];
        }
      }
      const unsigned long index3 = mapF[i];
      c[index3] = sum - endowmentFiltered[index3];
    }
  }
  // matrix ceq
  plhs[1] = mxCreateDoubleMatrix(pCount+pM1Count, 1, mxREAL);
  double *ceq = (double*)mxGetPr(plhs[1]);
  /* Contribution from steady state */
  for (unsigned long i = 0; i < nPatients; ++i) {
    for (unsigned long j = 0, index = i; j < nTm1; ++j, index += nPatients) {
      if (filterPaTi[index]) {
        const unsigned long index1 = index + nPatients;
        const unsigned long index2 = mapEq[index];
        double pIndex1 = 0.0;
        if (filterPaTi[index1]) {
          pIndex1 = p[mapEq[index1]];
        }
        ceq[index2] = -pExpTerm[index2]+pIndex1-arrivals[index1];
      }
    }
  }
  ceq = ceq + pM1Count;
  for (unsigned long i = 0; i < nPatients; ++i) {
    for (unsigned long j = 0, index = i; j < nTm1; ++j, index += nPatients) {
      if (filterPaTi[index]) {
        const unsigned long index1 = nPatients+index;
        double sumW = 0.0;
        for (unsigned long k = 0, index2 = index; k < nDonors; ++k, index2 += nP) {
          if (filterPattern[index2]) {
            const unsigned long index3 = map[index2];
            sumW += q[index3]*eMax[index3];
          }
        }
        if (filterPaTi[index]) {
          double previous = 0.0;
          if (filterPaTi[index1]) {
            previous = v[mapEq[index1]];
          }
          const unsigned long index3 = mapEq[index];
          ceq[index3] = discountV2[index]*v[index3]-aK[index3]*sumW-previous;
        }
      }
    }
    unsigned long index = nTm1nP+i;
    if (filterPaTi[index]) {
      double sumW = 0.0;
      for (unsigned long k = 0, index2 = index; k < nDonors; ++k, index2 += nP) {
        if (filterPattern[index2]) {
          const unsigned long index3 = map[index2];
          sumW += q[index3]*eMax[index3];
        }
      }
      const unsigned long index3 = mapEq[index];
      ceq[index3] = discountV2[index]*v[index3]-aK[index3]*sumW;
    }
  }

  if (nlhs > 2) {
    // matrix gc
    unsigned long int currentIndex = 0;
    for (unsigned long i = 0; i < qCount; ++i) {
      const double localValue = p[npIndex[i]]*feas[i];
      if (localValue != 0.0) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex] = donorIndex[i];
        value[currentIndex++] = localValue;
      }
    }
    for (unsigned long i = 0; i < qCount; ++i) {
      const double localValue = qFeas[i];
      if (localValue != 0.0) {
        iIndex[currentIndex] = qCount+npIndex[i];
        jIndex[currentIndex] = donorIndex[i];
        value[currentIndex++] = localValue;
      }
    }
    const unsigned long offset = pCount+qCount;
    for (unsigned long i = 0; i < qCount; ++i) {
      const unsigned long pIndex = npIndex[i];
      const double localValue = -hFiltered[pIndex]*p[pIndex]*qfz[i];
      if (localValue != 0.0) {
        iIndex[currentIndex] = pIndex+offset;
        jIndex[currentIndex] = donorIndex[i];
        value[currentIndex++] = localValue;
      }
    }
    plhs[2] = mxCreateSparse(qCount+2*pCount, donorCount, currentIndex, mxREAL);
    double *sr  = (double*)mxGetPr(plhs[2]);
    size_t* irs = (size_t*)mxGetIr(plhs[2]);
    size_t* jcs = (size_t*)mxGetJc(plhs[2]);
    COO_toCSC(donorCount, currentIndex, iIndex, jIndex, value, jcs, irs, sr);

    // matrix gceq
    currentIndex = 0;
    // Part from steady state composition
    for (unsigned long i = 0; i < qCount; ++i) {
      const unsigned long pIndex = npIndex[i];
      if (pIndex < pM1Count) {
        const double localValue = pExpTermLh[pIndex]*Fz[i];
        if (localValue != 0.0) {
          iIndex[currentIndex] = i;
          jIndex[currentIndex] = pIndex;
          value[currentIndex++] = localValue;
        }
      }
    }
    for (unsigned long j = 0; j < pM1Count; ++j) {
      if (expTerm[j] != 0.0) {
        iIndex[currentIndex] = j+qCount;
        jIndex[currentIndex] = j;
        value[currentIndex++] = -expTerm[j];
      }
    }
    for (unsigned long j = 0; j < nTm1nP; ++j) {
      const unsigned long index = j+nPatients;
      if (filterPaTi[j] && filterPaTi[index]) {
        iIndex[currentIndex] = qCount+mapEq[index];
        jIndex[currentIndex] = mapEq[j];
        value[currentIndex++] = 1.0;
      }
    }
    for (unsigned long j = 0; j < nTm1nP; ++j) {
      if(filterPaTi[j]) {
        double sumQfz = 0.0;
        for (unsigned long i = 0, index = j; i < nDonors; ++i, index += nP) {
          if (filterPattern[index]) {
            sumQfz += qfz[map[index]];
          }
        }
        const unsigned long index = mapEq[j];
        double localValue = pExpTermLh[index]*sumQfz;
        if (localValue != 0.0) {
          iIndex[currentIndex] = index+offset;
          jIndex[currentIndex] = index;
          value[currentIndex++] = -localValue;
        }
      }
    }
    /* Part of value */
    for (unsigned long i = 0; i < qCount; ++i) {
      const uint64_t pIndex = npIndex[i];
      const double localValue = -eMax[i]*aK[pIndex];
      if (localValue != 0.0) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex] = pIndex+pM1Count;
        value[currentIndex++] = localValue;
      }
    }
    for (unsigned long i = 0; i < nP; ++i) {
      if(filterPaTi[i]) {
        double sumQ_Fz = 0.0;
        for (unsigned long j = 0, index = i; j < nDonors; ++j, index += nP) {
          if (filterPattern[index]) {
            const unsigned long index1 = map[index];
            sumQ_Fz += q[index1]*Fz[index1];
          }
        }
        const unsigned long index = mapEq[i];
        sumQ_Fz = sumQ_Fz*aK[index] + discountV2[i];
        if (sumQ_Fz != 0.0) {
          iIndex[currentIndex] = index+offset;
          jIndex[currentIndex] = index+pM1Count;
          value[currentIndex++] = sumQ_Fz;
        }
      }
    }
    for (unsigned long i = 0; i < nTm1nP; ++i) {
      const unsigned long index = i+nPatients;
      if (filterPaTi[i] && filterPaTi[index]) {
        iIndex[currentIndex] = offset+mapEq[index];
        jIndex[currentIndex] = mapEq[i]+pM1Count;
        value[currentIndex++] = -1.0;
      }
    }

    plhs[3] = mxCreateSparse(qCount+2*pCount, pM1Count+pCount, currentIndex, mxREAL);
    double *sr1  = (double*)mxGetPr(plhs[3]);
    size_t* irs1 = (size_t*)mxGetIr(plhs[3]);
    size_t* jcs1 = (size_t*)mxGetJc(plhs[3]);
    COO_toCSC(pM1Count+pCount, currentIndex, iIndex, jIndex, value, jcs1, irs1, sr1);
  }
}
