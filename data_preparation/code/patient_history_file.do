// This file creates the patient_history primary file
// DO NOT run on own; it should be called within gen_primary_files.do

  // Waiting list history records
  use "`wlhistory_data'/KIDPAN_WLHISTORY_DATA.DTA", clear
  keep if unos_cand_stat_cd > 4000 & unos_cand_stat_cd < 5000   // patients waiting for a kidney only
  merge m:1 wl_id_code using "`primary_files'sample_wl_id_codes.dta"
  keep if _merge == 3
  drop _merge
format on_expand_donor %6.0g
format on_iexpand_donor %6.0g

tempfile pt_history_base
save `pt_history_base', replace

// there are duplicate (wl_id,date,time) tuples. To decide which to keep, figure out which are in the PTR files
bys wl_id_code chg_date chg_time: gen count = _N
keep if count>1
tab count
egen group_tag = tag(wl_id_code chg_date chg_time)
gen group_id = sum(group_tag)   // id uniquely identifies (wl_id_code chg_date chg_time)
keep group_id wl_id_code wlreg_audit_id_code chg_date chg_time chg_ty
tempfile pt_history_dups
save `pt_history_dups', replace

// Merge with PTR universe
use donor_id match_submit_dt wl_id_code wlreg_audit_id_code offer_accept organ_placed using "`primary_files'ptr_primary", clear
merge m:1 wlreg_audit_id_code using `pt_history_dups', keep(match) nogen

keep wlreg_audit_id_code
duplicates drop
tempfile dups_to_keep
save `dups_to_keep'

// merge patient history file to the dups we should keep, then de-duplicate
use `pt_history_base', clear
merge 1:1 wlreg_audit_id_code using `dups_to_keep', keep(master match)
tab _merge, m
gen keep_dup = _merge == 3
tab keep_dup, m
drop _merge

// de-duplicate based on chg_ty, keep_dup
gen chg_ty_num = 1 if chg_ty == "D"
replace chg_ty_num = 2 if chg_ty == "M"
replace chg_ty_num = 3 if chg_ty == "A"
gsort wl_id_code chg_date chg_time -keep_dup chg_ty_num -wlreg_audit_id_code
by wl_id_code chg_date chg_time: keep if _n==1
drop keep_dup chg_ty_num

isid wl_id_code chg_date chg_time

// code active/inactive
gen active = unos_cand < 4099 & chg_ty != "D"

// merge history of unacceptable antigens
sort wlreg_audit_id_code
merge 1:1 wlreg_audit_id_code using "`antigen_data'ki_unacc_antigen.DTA"
drop if _merge == 2
drop _merge wl_org  // wl_org == "KI" always

// rename variables to match old schema
foreach x in a b c dq dr bw{
  local cap = upper("`x'")
  display "`cap'"
  rename locus`x' unacc_antigens_`cap'
  replace unacc_antigens_`cap' = subinstr(unacc_antigens_`cap',",","|",.)
}

compress
isid wl_id_code chg_date chg_time
sort wl_id_code chg_date chg_time chg_ty

// chg_till variable gives the time interval to which the record applies
// activity_marker marks a unique active/inactive status history (so an active status following another active status is given a zero)
by wl_id_code: gen chg_till = chg_date[_n+1]
gen activity_marker = 1 - (active == active[_n-1])

order wl_id_code - chg_time chg_till activity_marker
save "`primary_files'patient_history.dta", replace

if `test_sample'==1{
  merge m:1 wl_id_code using `test_sample_files'sample_wl_id_codes, keep(match) nogen
  save `test_sample_files'patient_history, replace
}
