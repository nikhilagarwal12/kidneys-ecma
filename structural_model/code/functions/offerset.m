%% Offerset Object: Creates an object that contains a set of offers in object.data that are linked to
%% a donor in object.donor, a patient in object.patients and a patient history in pt_histories.
%%      object.indices.pt_histories contains the location of each offer in pt_history. 
%%      object.indices.patients contains the location of each offer in patients. 
%%      off.indices.donors contains the location of each offer in donors.

classdef offerset
    properties
        donors = [];
        pt_history  = [];    % Arbitrary structure (can be anything)
        patients = [];
        data = dataset();
        indices = dataset();
    end
    
    methods
        
        %% Construct a offerset class
        
        function off = offerset(MatchChars,pt_history,patients,donors,offers)
            % Create the database
            for ii = 1:numel(MatchChars)
                mc = MatchChars{ii};
                mc.rowLabel = 'wlreg_audit_id_code';
                if nargin<5 || isempty(offers)  % Offers for pt in pt_history and donros in donors
                    mc = lookup(mc,'wlreg_audit_id_code',pt_history.wlreg_audit_id_code);
                    mc = lookup(mc,'donor_id',donors.donor_id);
                    mc = mc{:};
                else
                    mc = lookup(mc,offers.wlreg_audit_id_code,offers.donor_id);
                end
                data{ii}       = struct2dataset(mc);
            end  % Offers were provided
            off.data = vertcat(data{:});
            
            % Donor index
            [~,off.indices.donors]=ismember(off.data.donor_id,donors.donor_id);
            
            % Add wl_id_code
            [~,off.indices.pt_histories]=ismember(off.data.wlreg_audit_id_code,pt_history.wlreg_audit_id_code);
            if ~ismember('wl_id_code',off.data.Properties.VarNames)
                off.data.wl_id_code = pt_history.wl_id_code(off.indices.pt_histories);
            end
            
            % Add patient variables
            abo_fn = @(x) (x==0)+2*(x==1 | x == 11 | x ==12)+3*(x==2)+4*(x==3 | x==31 | x==32); 
            [~,off.indices.patients]=ismember(off.data.wl_id_code,patients.wl_id_code);
            if ~ismember('abo',off.data.Properties.VarNames)
                off.data.abo = abo_fn(patients.abo(off.indices.patients));
            end
            
            % Init date
            if ~ismember('init_date',off.data.Properties.VarNames)
                off.data.init_date = patients.init_date(off.indices.patients);
            end
            
            % Init age
            if ~ismember('init_age',off.data.Properties.VarNames)
                off.data.init_age = patients.init_age(off.indices.patients);
            end
            off.donors = donors;
            off.pt_history = pt_history;
            off.patients = patients;
            
       end
       
       %%%% METHODS %%%%%
       
       %% Exclude some offers
       
       % exclude unacceptables;
       function off = excludeUnacceptables(off)
            keep = off.data.unacc ==0;
            off = SubsetObs(off,keep);
       end
       
       % Exclude other incompatibilities
       function off = excludeBin0(off) 
            keep = ~(off.data.bins ==0);
            off = SubsetObs(off,keep);
       end       
       
       % Exclude if never_offered
       function off = excludeNever(off) 
            keep = ~(off.data.days1 ==0);
            off = SubsetObs(off,keep);
       end
       
       function off = onlyLocal(off)
            keep = off.data.geo==1;
            off = SubsetObs(off,keep);
       end
       
       % Add match dates
       function [off,extra] = matchDates(off,method,values)
            switch method
                case 'date_donor'
                    off.data.match_date = off.donors.don_date(off.indices.donors);
                case 'increments'
                    extra.offer_dates = bsxfun(@plus,off.data.init_date,values(:)');
                    off = repdat(off,numel(values),'incr');
                    off.data.match_date = extra.offer_dates(:);
                case 'exponential_distribution'
                    % values should be a structure with min_match_date, rho, wait_grid, T_age
                    t_iz = off.data.days1;
                    if ismember('days2',off.data.Properties.VarNames),
                        t_iz = [t_iz off.data.days2 off.data.days3];
                        if ismember('days4',off.data.Properties.VarNames),
                            t_iz = [t_iz off.data.days4 off.data.days5];
                        end
                    end
                    t_iz(t_iz==0)=inf;
                    min_t_iz = min(t_iz,[],2); 
                    off.data = off.data(~isinf(min_t_iz),:); % Keeping only feasible donors;
                    extra.date_iz = max(off.data.init_date+min_t_iz,values.min_match_date);
                    extra.days_till_death = off.data.init_date + (values.T_age-off.data.init_age)*365.25 - extra.date_iz; %People die at age T_age on an aniversary of their listing date.
                    this_quantile = bsxfun(@times,1-exp(-values.rho*extra.days_till_death), values.wait_grid); % This quantile of the unconditional distribution.
                    this_wait_grid = - log(1- double(this_quantile))/values.rho;
                    this_wait_grid=offerset.evaluation_point_exponential_grid(this_wait_grid,values.rho);
                    extra.offer_dates = bsxfun(@plus,extra.date_iz,round(this_wait_grid)); %(donors,time)
                    off = repdat(off,numel(values.wait_grid)-1);
                    off.data.match_date = extra.offer_dates(:);
                otherwise, error('I do not understand how to add match dates'); 
           end
      end
      
      
      %% Time functions
      
      function wt = waittime(off)
          wt = off.data.match_date-off.data.init_date;
      end
      
      function a = age(off)
          a = off.data.init_age + off.waittime/365.25;
      end
      
      function  off = cKAS(off,mech)
                bins = off.data.kas_bins;
                bins(off.age<18) = off.data.kas_bins12(off.age<18);
                bins(off.age<11) = off.data.kas_bins10(off.age<11);
                        fprintf('-')
                points = off.data.kas_points;
                points(off.age<18) = off.data.kas_points12(off.age<18);
                points(off.age<11) = off.data.kas_points10(off.age<11);
                off.data.kas_cbins  = bins;
                off.data.kas_cpoints = points  + 1/365* off.waittime;
      end
      
      function  off = cPREKAS(off,mech)
                bins = off.data.bins;
                bins(off.age<18) = off.data.bins12(off.age<18);
                bins(off.age<11) = off.data.bins10(off.age<11);
          
                points = off.data.points;
                points(off.age<18) = off.data.points12(off.age<18);
                points(off.age<11) = off.data.points10(off.age<11);
                off.data.cbins  = bins;
                off.data.cpoints = points + floor(1/365.25* off.waittime);
      end
      
      
      function in_gap = in_eligibility_gap(off)
          in_between = @(mat,lb,ub) (bsxfun(@lt,mat,ub) & bsxfun(@gt,mat,lb));
          in_gap=in_between(off.waittime,off.data.days3,off.data.days2);
          if ismember('days4',get(off.data,'VarNames'))
             in_gap = in_gap | in_between(off.data.waittime,off.data.days5,off.data.days4);
          end
      end
      
      function d = subsetvars(off,subset)
          if nargin == 1, 
             subset ={'wlreg_audit_id_code','match_date','wl_id_code','donor_id','a_mm','b_mm','dr_mm','geo','c_abo'};
          end
          d = off.data(:,subset);
      end
      
      %%%% SPECIAL RULES %%%%%%%%
      
      % Overload subreferencing.
        function varargout = subsref(obj,s)   % Overloads subreferencing
           switch s(1).type
               case '{}'     % Define what happens when {}
                    newobj = obj;
                    if iscellstr(s(1).subs) && all(ismember(s(1).subs,get(obj.data,'VarNames')))   % If referencing variable/field names
                       newobj.data = obj.data(:,s(1).subs);
                    else   % otherwise, apply the indexing matrix by matrix.
                        s(1).type = '()';
                        % Implement obj(indices)
                        % Apply the subindexing to each matrix
                        newobj.data    = builtin('subsref',obj.data,s(1));
                        s(1).subs{2} = ':';
                        newobj.indices = builtin('subsref',obj.indices,s(1));
                        % Apply subindexing to rowNames and colNames
                    end
               case '.'  % Augment the properties that can be accessed with .
                    if strcmp(s(1).subs,'Properties')   % Labels
                        newobj = builtin('subsref',obj.data,s(1));
                    elseif ismember(s(1).subs,obj.data.Properties.VarNames) % Direct access to variables
                        newobj = full(obj.data.(s(1).subs));
                    else
                        newobj = builtin('subsref',obj,s(1));
                    end
               otherwise
                        newobj = builtin('subsref',obj,s(1));
           end  % switch
           if numel(s)>1, newobj = subsref(newobj,s(2:end)); end  % Recursive
           varargout = {newobj};
       end  % subsref

       % Subset of observations;
       function off = SubsetObs(off,keep);
            off.data = off.data(keep,:);
            off.indices = off.indices(keep,:);
       end
        % Repeat data: like repmat but keeping track of the indices and adds an additional indexname (optional);
       function off = repdat(off,num,indexname);
            rows = size(off.data,1);
            off.data = repmat(off.data,num,1);
            off.indices = repmat(off.indices,num,1);
            if nargin>2,
                incr_index = repmat(1:num,rows,1);
                off.indices.(indexname) = incr_index(:);
            end
       end
       
       
       
       
      end %methods
      
      methods(Static)
      function points=evaluation_point_exponential_grid(value_grid,rho,options) 
          rhoxl = value_grid(:,1:end-1)*rho;
          rhoxh = value_grid(:,2:end)*rho;
          points = (exp(-rhoxl).*(1+rhoxl)-exp(-rhoxh).*(1+rhoxh))./(exp(-rhoxl)-exp(-rhoxh))/rho;
          n= isinf(rhoxh);
          points(n) = (exp(-rhoxl(n)).*(1+rhoxl(n)))./exp(-rhoxl(n))/rho;
      end
      
      end %methods static
      
end %classdef