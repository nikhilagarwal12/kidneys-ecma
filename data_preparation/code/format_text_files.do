// Takes .dta primary files, encodes the string variables we want to use in python/matlab, and writes new .dta files and .txt files
// DO NOT run on own; it should be called within gen_primary_files.do


********************
***    Offers    ***
********************   

use "`file_dir'offers.dta", clear

#delimit ;
local dropvars = "refusal_ostxt cod_cad_don non_hrt_don creat_don age_don ecd_donor add_dt notify_dt
first_tx_date *_pra add_dt notify_dt donor_opo match_opo payback_opo age_guess age_from_25 descrip listing_ctr";
#delimit cr

// keep variables that are dropped for .txt file, so we can merge back for .dta file
preserve
keep donor_id match_id_code ptr_row_order pb_offer_seq `dropvars'
tempfile offer_dropvars
save `offer_dropvars', replace
restore

// drop variables we don't need for .txt file
drop `dropvars'

// Format some missing values
replace payback = 0 if payback == .

// some of the _t variables have extra values that we don't want to deal with in the structural code
replace cpra_cat_t = cpra_cat if cpra_cat_t == " >= 80" | cpra_cat_t == " < 80"
replace abo_match_t = abo_match if abo_match_t != "Incompatible" & abo_match_t != "Compatible" & abo_match_t != "Identical" & abo_match_t != "O Donor B Candidate"
replace geo_t = geo if geo_t == "Region 1" | geo_t == "DCTC"
replace ped_list_t = "KAS Pediatric" if ped_list_t == "Pediatric"
replace ped_alloc_t = "KAS Pediatric" if ped_alloc_t == "Pediatric"

// Encode the variables
#delimit ;
local offers_stringvars "offer_accept organ_placed donor_cat abo abo_don opo_dsa_id 
run_opo_id listing_ctr_dsa_id listing_ctr_id epts_t zero_abdr zero_abdr_t abo_match
gender_don abo_match_t geo geo_t ped_list ped_list_t ped_alloc ped_alloc_t cpra_cat 
cpra_cat_t run_type run_loc_ctr variance c_accept c_accept_810";
#delimit cr

encode_vars "`offers_stringvars'"

// merge back dropvars for .dta version of file, then drop
merge 1:1 donor_id match_id_code ptr_row_order pb_offer_seq using `offer_dropvars', keep(3) nogen

compress
save "`file_dir'offers_encoded.dta", replace

if `test_sample'==1{
  preserve
  merge 1:1 donor_id match_id_code ptr_row_order pb_offer_seq using `test_sample_files'/sample_offers, keep(match) nogen
  save `test_sample_files'/offers_encoded, replace    
  restore
}


drop `dropvars'

sort donor_id match_date
by donor_id: gen earliest_match_date = match_date[1]
by donor_id: gen latest_match_date = match_date[_N]

gsort donor_id match_id_code ptr_row_order pb_offer_seq

matlab_format "*date* first_active_post"
order wl_id_code donor_id *date first_active_post date_turn_18

compress

// save .txt version first
outsheet using "`file_dir'offers.txt", delimiter(|) noquote nolabel replace

if `test_sample'==1{
  merge 1:1 donor_id match_id_code ptr_row_order pb_offer_seq using `test_sample_files'/sample_offers.dta, keep(match) nogen
  outsheet using `test_sample_files'/offers.txt, delimiter(|) noquote nolabel replace
}

**********************
***    Patients    ***
**********************   

use "`file_dir'patients.dta", clear

#delimit ;
local dropvars_pt = "perm_zip* malig_ostxt_trr pri_payment_ctry_trr_ki
pri_payment_ctry_tcr_ki prev_malig_ty_ostxt donation
diag_ostxt_ki grf_fail_cause_ostxt_ki cod_ostxt_ki cod2_ostxt_ki cod3_ostxt_ki pri_payment_ctry_trr_pa art_recon_ostxt
duct_mgmt_ostxt pri_payment_ctry_tcr_pa enteric_drain diag_ostxt_pa grf_fail_cause_ostxt_pa oth_grf_fail_cause_ostxt_pa 
infect_pa - trtrej6m_pa malig_ty_ostxt rejcnf_ki - rejtrt_pa trr_id_code oth_life_sup_ostxt exh_perit_access exh_vasc_access
compl_absc - oper_tech recov_out_us recov_country cod_ostxt_pa cod_ostxt_wl dgn_ostxt_tcr dgn2_ostxt_tcr tx_ctr tx_ctr_id";

local patient_stringvars = "prev_tx prev_ki_tx prev_pa_tx malig_trr work_income_trr abo gender resum_maint_dial 
txhrt txint txkid txliv txlng txpan work_income_tcr perm_state  drugtrt_copd perip_vasc malig_tcr_ki use_which_pra grf_stat_ki
multiorg malig_tcr_pa ped_list listing_ctr_dsa_id listing_ctr_id
organ cmv_igg cmv_igm ebv_serostatus hbv_core hbv_sur_antigen hcv_serostatus payback malig rej_biopsy on_dialysis
wlhl wlin wlki wlkp wlli wllu wlpa wlpi wlhr data_transplant data_waitlist first_wk_dial pre_tx_txfus perm_state_trr
grf_stat_pa trtrej6m_ki ven_ext_grf px_stat age_group tx_type tx_ctr_dsa_id wl_org grf_vasc_thromb_pa dial_trr";
#delimit cr

preserve
keep wl_id_code `dropvars_pt'
tempfile patient_dropvars
save `patient_dropvars', replace
restore  

drop `dropvars_pt'

// additional omissions
drop antibody_tested citizen_country dwfg_ki trtrej1y_ki cmv_status hbv_surf_total
replace listing_ctr_zip = substr(listing_ctr_zip,1,5)
replace tx_ctr_zip = substr(tx_ctr_zip,1,5)

// Some minor changes
replace grf_stat_pa = "U" if grf_stat_pa=="P"

drop listing_ctr listing_ctr_dsa tx_ctr_dsa

// Encode
encode_vars "`patient_stringvars'"

// Some format changes
rename payback tx_payback
destring abo_mat vasc_mgmt pk_d* listing_ctr_zip tx_ctr_zip, replace 

// .dta version, with all variables
merge 1:1 wl_id_code using `patient_dropvars', keep(match) nogen

// Generate Variables Often Used in KPSAM specs
// diabetes variable (assuming none = 0, any kind = 1 in KPSAM)
gen pt_diabetes = diab>=2 & diab<=5


save "`file_dir'patients_encoded.dta", replace
drop `dropvars_pt'

#delimit ;
matlab_format "incomp_live_donor_date dialysis_date dial_date composite_death_date first_ki_prev_tx_date 
end_date  retxdate_ki admission_date first_tx_date ssdmf_death_date gfr_date faildate_ki discharge_date 
activate_date init_date faildate_pa first_init_date creat_clear_date  wt_qual_date  
px_stat_date date_turn_18 death_date tx_date prev_ki_date first_ki_tx_date resum_maint_dial_dt enteric_drain_dt 
yr_entry_us_tcr dob";
#delimit cr



order wl_id_code *_date enteric_drain_dt resum_maint_dial_dt faildate_ki retxdate_ki faildate_pa yr_entry_us_tcr dob
compress

outsheet using "`file_dir'patients.txt", delimiter(|) noquote nolabel replace


*****************************
***    Deceased Donors    ***
*****************************

use "`file_dir'deceased_donors.dta", clear

#delimit ;
local dropvars_donor = "home_city_don home_zip_don recov_country_don cod_ostxt_don consent_doc_mech_ostxt 
consent_px_writ_doc citizen_country_don other_inf_ostxt_don pt_oth*_ostxt_don opo_ctr don_ty *ostxt 
cancer_oth_ostxt_don opo_id chagas_nat chagas_* chagus_history hbv_dna hcv_nat htlv_nat west_nile_* 
*perfusion* tb_history donor_hospital donor_hospital_zip"; 

local donor_stringvars = "abo_don gender_don recov_out_us_don ebv_igg_cad_don ebv_igm_cad_don hbv_core_don 
hbv_sur_antigen_don controlled_don core_cool_don non_hrt_don consent_writ_doc_intent_don antihype_don 
blood_inf_conf_don cardarrest_postneuro_don cmv_don ddavp_don ebna_don htlv_don inotrop_agents_don 
other_inf_conf_don protein_urine_don urine_inf_conf_don pt_diuretics_don heparin_don pt_steroids_don 
pt_t3_don pt_t4_don pulm_inf_conf_don vasodil_don opo_dsa_id donor_cat vdrl_don arginine_don insulin_don 
inotrop_support_don htlv1_old_don htlv2_old_don alcohol_heavy_don contin_cig_don contin_cocaine_don 
contin_oth_drug_don hist_oth_drug_don diet_don diuretics_don extracranial_cancer_don hist_cig_don 
hist_cocaine_don intracranial_cancer_don other_hypertens_med_don skin_cancer_don tattoos_don cdc_risk_hiv_don 
history_mi_don lt_ki_biopsy_don li_biopsy_don kil_pump rt_ki_biopsy_don kir_pump wall_abn_seg_don 
wall_abn_glob_don pulm_cath_don abn_valves_don abn_congen_don abn_lvh_don po2_done_don hist_cancer_don 
hist_hypertens_don med_exam_report med_exam_perm coronary_angio_don kib_pump hep_c_anti_don clin_infect_don 
hbsab_don home_state_don legally_brain_dead";
#delimit cr

preserve
keep donor_id `dropvars_donor'
tempfile donor_dropvars
save `donor_dropvars', replace
restore  

drop `dropvars_donor'  

encode_vars "`donor_stringvars'"

// .dta file with all vars
merge 1:1 donor_id using `donor_dropvars', keep(match) nogen

// Generate donor specific variables used in KPSAM
// donor age
gen don_age_lt_18 = (age_don < 18)
gen don_age_18_35 = (age_don >= 18 & age_don <= 35)
gen don_age_ge_50 = (age_don >= 50)

// donor gender
gen don_male = (gender_don == 1)

// Pancreas offered or transplanted
gen don_pa_off_or_tx = pa_disposition == 5 | pa_disposition == 6

//gen dcd = non_hrt_don == 1
gen dcd = non_hrt_don == 1

// donor COD (cause of death)
gen don_cod_stroke = (cod_cad_don == 2)
gen don_cod_cns = (cod_cad_don == 4)
gen don_cod_other = (cod_cad_don == 999 | cod_cad_don == .)
gen don_cod_anoxia = (cod_cad_don == 1)

// donor creatine
gen don_creat_lt_p5 = (creat_don < 0.5)
gen don_creat_p5_1p0 = (creat_don >= 0.5 & creat_don < 1.0)
gen don_creat_1p0_1p5 = (creat_don >= 1.0 & creat_don < 1.5)
gen don_creat_ge_1p5 = (creat_don >= 1.5)

// Final Formatting
compress
save "`file_dir'deceased_donors_encoded.dta", replace  

drop `dropvars_donor'

matlab_format "*date*"
compress

outsheet using "`file_dir'deceased_donors.txt", delimiter(|) noquote nolabel replace
  
*****************************
***    Patient History    ***
*****************************

use "`file_dir'patient_history.dta", clear
merge m:1 wl_id_code using "`file_dir'sample_wl_id_codes", keep(match) nogen

preserve
keep wlreg_audit_id_code chg_time unacc_antigen*
tempfile pt_hist_dropvars
save `pt_hist_dropvars', replace
restore

drop chg_time unacc_antigen*

#delimit ;
local pt_hist_stringvars = "chg_ty prelim_xmatch_req use_which_pra acpt_organ_proc_othteam 
doncrit_acpt_dcd doncrit_acpt_dcd_import doncrit_acpt_hist_diabetes doncrit_acpt_hist_hypertension 
doncrit_acpt_hbcorepos doncrit_acpt_hcvpos doncrit_acpt_htlv_pos";
#delimit cr
encode_vars "`pt_hist_stringvars'"

merge 1:1 wlreg_audit_id_code using `pt_hist_dropvars', keep(match) nogen

// Generate History Specific Variables Often Used in KPSAM
// Current pra Spline
gen pt_alloc_pra_0 = current_pra == 0
gen pt_alloc_pra_gt80 = current_pra > 80 & current_pra != .
gen pt_alloc_pra_gt98 = current_pra > 98 & current_pra != .
gen pt_alloc_pra_missing = current_pra == . | current_pra == -99

compress
save "`file_dir'patient_history_encoded.dta", replace

drop chg_time unacc_antigen* 

matlab_format "chg_date chg_till"
compress

outsheet using "`file_dir'pt_history.txt", delimiter(|) noquote nolabel replace


