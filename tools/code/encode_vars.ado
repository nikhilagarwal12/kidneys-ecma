// This encodes the variables according to label_dictionary

// Encode

program encode_vars

args stringvars

  do ../../data_preparation/code/label_dictionary.do

  foreach x in `stringvars' {
    encode `x', generate(`x'2) label(`x') noextend
    drop `x'
    rename `x'2 `x'
  }  
end
