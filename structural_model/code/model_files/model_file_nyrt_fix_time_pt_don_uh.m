% Model with both patient and donor UH, based on nyrt_lim_time_uh
% Patient UH is only through the intercept term
model = struct();
priors = struct();

% Load model that we will simplify 
model_file_nyrt_fix_time_uh

% Filepath
model.model_name = 'nyrt_fix_time_pt_don_uh';
model.gen_script = mfilename('fullpath'); % name of script that generated model file (i.e. name of this script)
model.gen_date = datetime('today'); % date model file was generated

% Model structure
model.pt_uh = true;
model.K_types = 2;

% Specify which variables are patient-UH-type specific
model.fixed.constant = false;
model.flex.constant = true;
assert(~model.fixed.constant | ~model.flex.constant)

% Add dirichlet prior over unobserved patient types
priors.beta = ones(1,model.K_types);

% store model in /structural_model/input/ (../input/)
save([models_folder model.model_name],'model','priors');
