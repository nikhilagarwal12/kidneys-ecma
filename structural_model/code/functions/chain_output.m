% Takes a model name and chain spec and reads/processes chain output
function chain_summary = chain_output(model,chain_spec,options)
    % Keep only statistics, unless draws are requested
    if ~isfield(options,'keep_draws')
        options.keep_draws = false;
    end
    if ~isfield(options,'eta_draws')
        options.eta_draws = false;
    end
	if ~isfield(chain_spec,'burn_in')
        chain_spec.burn_in = 0;
    end

   % Loop over chain no
    for ii= 1:options.chain_nos
        % Find the number of files that have been run for this chain
        available_files{ii} = dir([options.output_stub model.model_name ...
                            '/chain_' chain_spec.name '_' num2str(ii) '_*']);
        chain_spec.total_files = length(available_files{ii});

        % Read the chain chain summary
        chain_summary_ii(ii) = chain_output_ii(model,chain_spec,options.output_stub);
    end
    % Concatenate and save
    % Save the file information for what was used to construct the summary
    chain_summary.available_files = available_files;
    chain_summary.model = model;
    chain_summary.chain_spec = chain_spec;
    
    % Collect the variable statistics
    theta_draws_trim = squeeze(chain_summary_ii(:).theta_draws_trim);
    chain_summary.mu_theta = mean(theta_draws_trim,1)';
    chain_summary.cov_theta = cov(theta_draws_trim);

    % Summarize sigma_eta
    if model.donor_uh
        sigma_eta_draws_trim = squeeze(chain_summary_ii(:).sigma_eta_draws_trim);
        chain_summary.mu_sigma_eta = mean(sigma_eta_draws_trim,1);  
        chain_summary.se_sigma_eta = std(sigma_eta_draws_trim);
    end
    % Summarize Beta
    if model.pt_uh
        beta_draws_trim = squeeze(chain_summary_ii(:).beta_draws_trim);
        chain_summary.mu_beta = mean(beta_draws_trim,1)';
        chain_summary.V_beta = cov(beta_draws_trim);
    end

    % Store draws, if requested
    if options.keep_draws
        chain_summary.theta_draws_trim = theta_draws_trim;
        if model.donor_uh
            chain_summary.sigma_eta_draws_trim = sigma_eta_draws_trim;
        end
        if model.pt_uh
            chain_summary.beta_draws_trim = beta_draws_trim;
        end
    end

    % Store UH draws if relevant
    if model.donor_uh
        chain_summary.eta_indiv_draws_trim = squeeze(chain_summary_ii(:).eta_indiv_trim);
    end
    if model.pt_uh
        chain_summary.beta_i_draws_trim = squeeze(chain_summary_ii(:).beta_i_draws_trim);
    end

    % Date
    chain_summary.gen_date = datetime('today');
end

% output from the the particular chain
function chain_summary_ii = chain_output_ii(model,chain_spec,output_stub)
    output_folder = [output_stub model.model_name];

    % Define useful constants for shorthand
    L = chain_spec.NITER*chain_spec.total_files/chain_spec.thin;  % total number of chain draws
    Lf = chain_spec.NITER/chain_spec.thin;  % number of chain draws per file
    burn_start = chain_spec.burn_in/chain_spec.thin + 1; % draw # starting AFTER burn-in

    % Initialize vectors to hold theta and eta draws
    theta_draws = zeros(L,model.K_theta);
    if model.donor_uh
        sigma_eta_draws = zeros(L,1);
        load([output_folder '/chain_' chain_spec.name '_' num2str(chain_spec.chain_no) '--init'],'init');
        eta_indiv = zeros(size(init.eta,1),L);
    end
    
    % Summarize Beta
    if model.pt_uh
        beta_draws = zeros(L,model.K_types);
        beta_i_draws = zeros(length(init.beta_i),L);
    end

    % Load values from chain
    for ff = 1:chain_spec.total_files    
        load([output_folder '/chain_' chain_spec.name '_' num2str(chain_spec.chain_no) '_' num2str(ff)]);    
        for ii = 1:Lf
            theta_draws(((ff-1)*Lf + ii),:) = chain(ii).theta;
            if model.donor_uh
                sigma_eta_draws(((ff-1)*Lf + ii),:) = chain(ii).sigma_eta;
                eta_indiv(:,((ff-1)*Lf + ii)) = chain(ii).eta;
            end
            if model.pt_uh
                beta_draws(((ff-1)*Lf+ii),:) = chain(ii).beta';
                beta_i_draws(:,((ff-1)*Lf+ii)) = chain(ii).beta_i;
            end
        end
    end

    % drop first burn-in/THIN observations, take means of theta and sd(eta)
    chain_summary_ii.theta_draws_trim = theta_draws(burn_start:end,:);
    if model.donor_uh
        chain_summary_ii.sigma_eta_draws_trim = sigma_eta_draws(burn_start:end,:);
        chain_summary_ii.eta_indiv_trim = eta_indiv(:,burn_start:end);
    end
    if model.pt_uh
        chain_summary_ii.beta_draws_trim = beta_draws(burn_start:end,:);
        chain_summary_ii.beta_i_draws_trim = beta_i_draws(:,burn_start:end);
    end
end
