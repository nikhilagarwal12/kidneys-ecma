% This function summarizes the counterfactual results 
% The output goes into Table 6 in the main text

function [table_ds,table_ds_splits,pt_table_ds] = cf_table(cf_output,cf_output_data,cf_table_options,options,model)

% Load donor and patient datasets
load(model.filenames.donors);
donors.don_cod_headtrauma = ~(donors.don_cod_stroke | donors.don_cod_cns | donors.don_cod_other | donors.don_cod_anoxia);
load(model.filenames.pt_history);
load(model.filenames.patients);

% Get the patients that are in the sample
[ismem,ix]         = ismember(cf_output_data(1).pt_hist_id,pt_history.wlreg_audit_id_code);
assert(all(ismem));
pt_history_trim       = pt_history(ix,:);
[ismem,ix]         = ismember(pt_history_trim.wl_id_code,patients.wl_id_code);
assert(all(ismem));
patients_trim      = patients(ix,:);
non_dup_cols       = ~ismember(pt_history_trim.Properties.VarNames,patients_trim.Properties.VarNames);
pt_data_trim       = cat(2,patients_trim,pt_history_trim(:,non_dup_cols));

% Compare multiple mechanisms -- Overall Statistics

for ii = 1:size(cf_output,1)    
    which_patients             = true(size(cf_output(ii).pt_weights,1),1);
    table_ds(ii)               = table_ds_ii(cf_output(ii),cf_output_data(1),cf_table_options.don_char_list,donors,which_patients,model,'all');    

    % Split by Patient Type
    if isfield(cf_table_options,'pt_splits')
        for jj = 1:size(cf_table_options.pt_splits,2)
            which_patients         = cf_table_options.pt_splits{jj}(pt_data_trim);        
            table_ds_splits(jj,ii) = table_ds_ii(cf_output(ii),cf_output_data(1),cf_table_options.don_char_list,donors,which_patients,model,cf_table_options.pt_split_names{jj});
        end
    end

    % Summarize patient characteristics
    if isfield(cf_table_options,'pt_chars')
        which_patients         = true(size(cf_output(ii).pt_weights,1),1);
        pt_table_ds(ii)        = pt_ds_ii(cf_output(ii),cf_output_data(1),cf_table_options.pt_chars,pt_data_trim,model);
    else
        pt_table_ds = struct();
    end    
end

% Get the table_ds for each cf_output
function table_ds = table_ds_ii(cf_output,cf_output_data,don_char_list,donors,which_patients,model,sample)

    if cf_output.use_proj 
        cf_output_data.Gamma_eta = cf_output_data.proj.Gamma_eta ;
    else
        cf_output_data.Gamma_eta = cf_output_data.calc.Gamma_eta ;
    end
    
    % Check some assertions
    assert(abs(sum(cf_output.pt_weights(:))-1)<1e-6);
    assert(abs(sum(cf_output_data.options.donor_weight(:))-1)<1e-6);

    % Initialize
    table_ds               = struct();

    % Set weights to all patients not in the group to 0
    cf_output.pt_weights(~which_patients,:)            = 0;
    cf_output_data.pt_entry_weights(~which_patients,:) = 0;

    % A useful function
    wgt_pt_out             = @(x,w) sum(x.*w,2)./sum(w,2);
    
    % Name of the row
    table_ds.name          = cf_output.mech_name;
    % Sample identifier
    table_ds.sample        = sample;
    table_ds.num_patients = sum(which_patients);  % calculate sample size
    
    % Convergence
    table_ds.diff_norm     = cf_output.diff_norm;
    table_ds.iter          = cf_output.iter;
    % Primitives
    %table_ds.lambda        = cf_output_data.options.lambda;
    table_ds.pt_arrival    = cf_output_data.options.pt_arrival_rate*mean(which_patients);
        
    % Size and fraction of kidneys
    table_ds.size          = cf_output.pool_size;
    table_ds.frac_kid_tx   = cf_output.frac_transplanted;

    % Average time on list
    table_ds.time_on_list  = sum(sum(cumsum(cf_output.pt_weights>0,2).*cf_output.pt_weights,2))/(4*sum(cf_output.pt_weights(:)));
    
    % Average LYFT (no longer used), Gamma_eta, t_entry_weights
    table_ds.avlyft_ctx    = mean_offer_char(cf_output_data.lyft,cf_output.p_tx,cf_output.pt_weights);
    table_ds.avgamma_eta   = mean_offer_char(cf_output_data.Gamma_eta,cf_output.p_tx,cf_output.pt_weights);
    
    % Decomposition on donor fixed effects and residuals
    donor_fe               = sum(sum(bsxfun(@times,cf_output_data.Gamma_eta,cf_output_data.pt_entry_weights),1),2);
    table_ds.res_gamma     = mean_offer_char(bsxfun(@plus,cf_output_data.Gamma_eta,-donor_fe),cf_output.p_tx,cf_output.pt_weights);
    donor_fe               = repmat(donor_fe,[size(cf_output.V) 1]);
    table_ds.donor_fe      = mean_offer_char(donor_fe,cf_output.p_tx,cf_output.pt_weights);
    
    % TX rate
    table_ds.total_tx_rate = mean_p_tx(cf_output.p_tx,cf_output.pt_weights)*model.lambda*cf_output.pool_size*sum(sum(cf_output.pt_weights));
    table_ds.av_tx_rate    = mean_p_tx(cf_output.p_tx,cf_output.pt_weights)*model.lambda*365.25;
    
    % Discard rates
    table_ds.disc          = cf_output.disc;
    table_ds.disc_loc      = cf_output.disc_loc;
    
    
    % Exogeneous Departure Rate
    table_ds.exog_dep         = sum(cf_output_data.q_leave_exog(:).*cf_output.pt_weights(:))/sum(cf_output.pt_weights(:))*cf_output.pool_size;
    table_ds.av_exog_dep_rate = sum(cf_output_data.q_leave_exog(:).*cf_output.pt_weights(:))/sum(cf_output.pt_weights(:))*365.25;
    
    % Average of various transplanted donor characteristics
    for ii = 1:length(don_char_list)
        char               = don_char_list{ii};
        table_ds.(char)    = mean_don_char(donors,char,cf_output,cf_output_data,model);
    end
    
    % Welfare
    av_V_ii                = wgt_pt_out(cf_output.V,cf_output.pt_weights);
    av_V_ii                = av_V_ii(which_patients);

    av_V_ii_enter          = wgt_pt_out(cf_output.V,cf_output_data.pt_entry_weights);
    av_V_ii_enter          = av_V_ii_enter(which_patients);
    av_V_ii_flow           = sum(cf_output.V(which_patients,:).*(cf_output.pt_weights(which_patients,:)*cf_output.pool_size + ... 
                             cf_output_data.pt_entry_weights(which_patients,:)*cf_output_data.options.pt_arrival_rate/cf_output_data.options.rho),2);

    % Decomposition into observed and unobserved components
    % All future flow payoffs have already been aggregated -- just need to multiply by donor arrival rate and divide by rho and # patients
    av_V_obs_ii_flow       = cf_output.vf_decomp.V_obs(which_patients);
    av_V_unobs_ii_flow     = cf_output.vf_decomp.V_unobs(which_patients);
    av_V_fixed_ii_flow     = cf_output.vf_decomp.V_fixed(which_patients);
    av_V_full_ii_flow      = cf_output.vf_decomp.V_full(which_patients);

    % Average value function for those on the list
    percentiles = [0 1 10 25 50 75 90 99 100];

    table_ds.averageV_list = mean(av_V_ii);
    table_ds.averageV_enter= mean(av_V_ii_enter);
    table_ds.averageV      = wgt_pt_out([table_ds.averageV_list,table_ds.averageV_enter],[cf_output.pool_size*mean(which_patients),cf_output_data.options.pt_arrival_rate*mean(which_patients)/cf_output_data.options.rho]);
    table_ds.median        = median(av_V_ii);
    boundit = @(x) max(min(x,inf),-inf);  % current version: no bounds

    table_ds.av_V_enter = mean(av_V_ii_enter./boundit(cf_output_data.V_ref_entry(which_patients))); % Equivalent lambda prime/ lambda
    table_ds.av_V = mean( av_V_ii_flow ./boundit(cf_output_data.V_ref_flow(which_patients))); % waitlist weighted V / lambda
    table_ds.frac_Vinc_enter = mean(av_V_ii_enter > boundit(cf_output_data.V_ref_entry(which_patients)));
    table_ds.frac_Vinc_5p_enter = mean((av_V_ii_enter - boundit(cf_output_data.V_ref_entry(which_patients)))./boundit(cf_output_data.V_ref_entry(which_patients)) >= - 0.05);
    table_ds.frac_Vinc = mean( av_V_ii_flow > boundit(cf_output_data.V_ref_flow(which_patients))); % waitlist weighted V / lambda
    table_ds.av_V_enter_std = std(av_V_ii_enter./boundit(cf_output_data.V_ref_entry(which_patients))); % Equivalent lambda prime/ lambda
    table_ds.av_V_std = std( av_V_ii_flow ./boundit(cf_output_data.V_ref_flow(which_patients))); % waitlist weighted V / lambda
    for p = percentiles
      table_ds.(['av_V_p' num2str(p)]) = prctile( av_V_ii_flow ./boundit(cf_output_data.V_ref_flow(which_patients)), p); % waitlist weighted V / lambda      
    end
    for p = percentiles
      table_ds.(['av_V_enter_p' num2str(p)]) = prctile(av_V_ii_enter./boundit(cf_output_data.V_ref_entry(which_patients)), p); % Equivalent lambda prime/ lambda
    end

    % Welfare decomposition fields
    table_ds.av_V_obs   = mean( av_V_obs_ii_flow ./boundit(cf_output_data.V_ref_flow(which_patients)));  % contribution of observables to waitlist weighted V / lambda
    table_ds.av_V_unobs = mean( av_V_unobs_ii_flow ./boundit(cf_output_data.V_ref_flow(which_patients)));  % contribution of unobservables to waitlist weighted V / lambda
    table_ds.av_V_fixed = mean( av_V_fixed_ii_flow ./boundit(cf_output_data.V_ref_flow(which_patients)));  % contribution of observables to waitlist weighted V / lambda
    table_ds.av_V_full  = mean( av_V_full_ii_flow ./boundit(cf_output_data.V_ref_flow(which_patients)));  % checking decomposition formulas against directly aggregating the value functions

    % Without applying pareto weights
    table_ds.V_obs_unwghtd = mean(av_V_obs_ii_flow);
    table_ds.V_unobs_unwghtd = mean(av_V_unobs_ii_flow);
    table_ds.V_fixed_unwghtd = mean(av_V_fixed_ii_flow);
    table_ds.V_full_unwghtd = mean(av_V_full_ii_flow);
    

function table_ds = pt_ds_ii(cf_output,cf_output_data,pt_char_list,patients_trim,model)

    % Initialize
    table_ds               = struct();
    % Name of the row
    table_ds.name          = cf_output.mech_name;

    % Average of various characteristics
    for ii = 1:length(pt_char_list)
        char               = pt_char_list{ii};
        table_ds.(char)    = mean_pt_char(patients_trim,char,cf_output,cf_output_data,model);
    end
      
% Get the average characteristic of the offer
function avchar = mean_offer_char(offer_char,p_tx,pt_wt)
    % Get the tx weights and average
    offer_char_wt = bsxfun(@times,p_tx,pt_wt);
    avchar        = sum(offer_char(:).*offer_char_wt(:))/sum(offer_char_wt(:));

% Get the average transplant rate
function av_p_tx = mean_p_tx(p_tx,pt_wt)
    % Get the tx weights and average
    av_p_tx       = bsxfun(@times,p_tx,pt_wt);
    av_p_tx       = sum(av_p_tx(:))/sum(pt_wt(:));

% Get the average characteristic of the donor
function avchar = mean_don_char(donors,char,cf_output,cf_output_data,model);
    % Restrict to subset of donors
    [ismem,ix]    = ismember(cf_output_data.donor_id,donors.donor_id);
    assert(all(ismem));
    these_donors  = donors(ix,:);  

    % Get the tx weights
    don_char      = reshape(these_donors.(char),1,1,[]);
    offer_char_wt = bsxfun(@times,cf_output.p_tx,cf_output.pt_weights);
    if isequal(model.missing{1},'yes')     % Deal with missing -- set the donor weight to 0 for this mean
        offer_char_wt(:,:,don_char==model.missing{2}) = 0;        
    end        

    % Average
    avchar        = bsxfun(@times,offer_char_wt,don_char);
    avchar        = sum(avchar(:))/sum(offer_char_wt(:));

% Get the average characteristic of the donor
function avchar = mean_pt_char(patients,char,cf_output,cf_output_data,model)
    % Get the pt weights
    wt = cf_output.pt_weights;
    pt_char       = patients.(char);
    if isequal(model.missing{1},'yes')     % Deal with missing -- set the donor weight to 0 for this mean
        wt(:,:,pt_char==model.missing{2}) = 0;        
    end        

    % Average
    avchar        = bsxfun(@times,wt,pt_char);
    avchar        = sum(avchar(:))/sum(wt(:));
