#include "common.h"

#include "mex.h"

#include <math.h>
#include <stdint.h>
#include <string.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[0], 0, "mu"));
  const long nPatients = sizeArray[0];
  const long nTime = sizeArray[1];
  const long nDonors = sizeArray[2];
  const uint64_t firstBest = (uint64_t)mxGetScalar(mxGetField(prhs[0], 0, "first_best"));
  double *includePattern = (double*)mxGetPr(mxGetField(prhs[0], 0, "include_pattern"));
  uint64_t isSmall = 0;
  if (includePattern != NULL) {
    isSmall = 1;
  }
  const unsigned long nQ = nPatients*nTime*nDonors;
  const unsigned long nP = nPatients*nTime;
  const unsigned long nTm1nP =nP - nPatients;

  // matrix gc
  // First create a sparse matrix in COO format and convert to matlab format
  long *iIndex, *jIndex;
  double *value;
  long nnz;
  if (isSmall) {
    const uint64_t qCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "qCount")));
    const uint64_t pCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "pCount")));
    const uint64_t donorCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "donorCount")));
    if (firstBest) {
      nnz = 4l*pCount+3l*qCount+pCount*donorCount;
    } else {
      nnz = 5l*pCount+3l*qCount+2l*pCount*donorCount;
    }
  } else {
    if (firstBest) {
      nnz = nP*(4*nDonors+2+nTime)-nPatients;
    } else {
      nnz = 4l*nQ + nP + nTm1nP*(nDonors+4l);
    }
  }
  iIndex = (long*)malloc(nnz*sizeof(long));
  jIndex = (long*)malloc(nnz*sizeof(long));
  value = (double*)malloc(nnz*sizeof(double));
  unsigned long int currentIndex = 0;
  if (isSmall) {
    const uint64_t qCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "qCount")));
    const uint64_t pCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "pCount")));
    const uint64_t pM1Count =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "pM1Count")));
    const uint64_t donorCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "donorCount")));
    const uint64_t *map =  (uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "map"));
    const uint64_t *mapEq =  (uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "mapEq"));
    const uint64_t *npIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "npIndex"));
    const uint64_t *donorIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[0], 0, "donorIndex"));
    uint64_t *filterPattern = (uint64_t*)mxGetPr(prhs[1]);
    uint64_t *filterPaTi = filterPattern + nQ;
    const unsigned long offset1 = pM1Count+donorCount;
    const unsigned long offset = pCount+qCount;
    if (firstBest) {
      for (unsigned long i = 0; i < qCount; ++i) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex] = donorIndex[i];
        value[currentIndex++] = 1.0;
      }
      for (unsigned long i = 0; i < pCount; ++i) {
        for (unsigned long j = 0; j < donorCount; ++j) {
          iIndex[currentIndex] = i+qCount;
          jIndex[currentIndex] = j;
          value[currentIndex++] = 1.0;
        }
      }
      /* // Part from steady state composition */
      for (unsigned long i = 0; i < qCount; ++i) {
        if (npIndex[i] < pM1Count) {
          iIndex[currentIndex] = i;
          jIndex[currentIndex] = donorCount+npIndex[i];
          value[currentIndex++] = 1.0;
        }
      }
      for (unsigned long j = 0; j < pM1Count; ++j) {
        iIndex[currentIndex] = j+qCount;
        jIndex[currentIndex] = j+donorCount;
        value[currentIndex++] = 1.0;
      }
      for (unsigned long j = 0; j < nPatients*(nTime-1); ++j) {
        if (filterPaTi[j+nPatients] && filterPaTi[j]) {
          iIndex[currentIndex] = qCount+mapEq[j+nPatients];
          jIndex[currentIndex] = mapEq[j]+donorCount;
          value[currentIndex++] = 1.0;
        }
      }
      // Value part
      for (unsigned long i = 0; i < nDonors; ++i) {
        for (unsigned long j = 0; j < nP; ++j) {
          const unsigned long index = j+i*nP;
          if (filterPattern[index] && filterPaTi[j]) {
            const unsigned long index2 = mapEq[j];
            const unsigned long index1 = map[index];
            iIndex[currentIndex] = index1;
            jIndex[currentIndex] = index2+offset1;
            value[currentIndex++] = 1.0;
          }
        }
      }
      for (unsigned long j = 0; j < pCount; ++j) {
        iIndex[currentIndex] = j+offset;
        jIndex[currentIndex] = j+offset1;
        value[currentIndex++] = 1.0;
      }
      for (unsigned long i = 0; i < nPatients*(nTime-1); ++i) {
        if (filterPaTi[i] && filterPaTi[i+nPatients]) {
          iIndex[currentIndex] = mapEq[i+nPatients]+offset;
          jIndex[currentIndex] = mapEq[i]+offset1;
          value[currentIndex++] = 1.0;
        }
      }
    } else {
      for (unsigned long i = 0; i < qCount; ++i) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex] = donorIndex[i];
        value[currentIndex++] = 1.0;
      }
      for (unsigned long i = 0; i < pCount; ++i) {
        for (unsigned long j = 0; j < donorCount; ++j) {
          iIndex[currentIndex] = i+qCount;
          jIndex[currentIndex] = j;
          value[currentIndex++] = 1.0;
        }
      }
      for (unsigned long i = 0; i < pCount; ++i) {
        for (unsigned long j = 0; j < donorCount; ++j) {
          iIndex[currentIndex] = i+offset;
          jIndex[currentIndex] = j;
          value[currentIndex++] = 1.0;
        }
      }
      // Part from steady state composition
      for (unsigned long i = 0; i < qCount; ++i) {
        if (npIndex[i] < pM1Count) {
          iIndex[currentIndex] = i;
          jIndex[currentIndex] = donorCount+npIndex[i];
          value[currentIndex++] = 1.0;
        }
      }
      for (unsigned long j = 0; j < pM1Count; ++j) {
        iIndex[currentIndex] = j+qCount;
        jIndex[currentIndex] = j+donorCount;
        value[currentIndex++] = 1.0;
      }
      for (unsigned long j = 0; j < nPatients*(nTime-1); ++j) {
        const unsigned long index = j+nPatients;
        if (filterPaTi[j] && filterPaTi[index]) {
          iIndex[currentIndex] = qCount+mapEq[index];
          jIndex[currentIndex] = mapEq[j]+donorCount;
          value[currentIndex++] = 1.0;
        }
      }
      for (unsigned long j = 0; j < nPatients*(nTime-1); ++j) {
        if(filterPaTi[j]) {
          const unsigned long index = mapEq[j];
          iIndex[currentIndex] = index+offset;
          jIndex[currentIndex] = index+donorCount;
          value[currentIndex++] = 1.0;
        }
      }

      for (unsigned long i = 0; i < qCount; ++i) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex] = npIndex[i]+offset1;
        value[currentIndex++] = 1.0;
      }
      for (unsigned long i = 0; i < nP; ++i) {
        if(filterPaTi[i]) {
          const unsigned long index = mapEq[i];
          iIndex[currentIndex] = index+offset;
          jIndex[currentIndex] = index+offset1;
          value[currentIndex++] = 1.0;
        }
      }
      for (unsigned long i = 0; i < nPatients*(nTime-1); ++i) {
        if (filterPaTi[i] && filterPaTi[i+nPatients]) {
          iIndex[currentIndex] = offset+mapEq[i+nPatients];
          jIndex[currentIndex] = mapEq[i]+offset1;
          value[currentIndex++] = 1.0;
        }
      }
    }
    plhs[0] = mxCreateSparse(qCount+2*pCount, pCount+offset1, currentIndex, mxREAL);
    double *sr1  = (double*)mxGetPr(plhs[0]);
    size_t* irs1 = (size_t*)mxGetIr(plhs[0]);
    size_t* jcs1 = (size_t*)mxGetJc(plhs[0]);
    COO_toCSC(pCount+offset1, currentIndex, iIndex, jIndex, value, jcs1, irs1, sr1);
  } else {
    if (firstBest) {
      for (unsigned long j = 0; j < nDonors; ++j) {
        for (unsigned long i = 0; i < nP; ++i) {
          iIndex[currentIndex] = i+j*nP;
          jIndex[currentIndex++] = j;
        }
      }
      for (unsigned long j = 0; j < nDonors; ++j) {
        for (unsigned long i = 0; i < nP; ++i) {
          iIndex[currentIndex] = i+nQ;
          jIndex[currentIndex++] = j;
        }
      }
      // Part from steady state composition
      for (unsigned long j = 0; j < nDonors; ++j) {
        for (unsigned long i = 0; i < nTm1nP; ++i) {
          iIndex[currentIndex] = i+j*nP;
          jIndex[currentIndex++] = nDonors+i;
        }
      }
      for (unsigned long j = 0; j < nTm1nP; ++j) {
        iIndex[currentIndex] = j+nQ;
        jIndex[currentIndex++] = j+nDonors;
      }
      for (unsigned long j = 0; j < nTm1nP; ++j) {
        iIndex[currentIndex] = j+nQ+nPatients;
        jIndex[currentIndex++] = j+nDonors;
      }
      // Value Part
      for (unsigned long i = 0; i < nDonors; ++i) {
        for (unsigned long j = 0; j < nP; ++j) {
          iIndex[currentIndex] = j+i*nP;
          jIndex[currentIndex++] = j+nDonors+nTm1nP;
        }
      }
      for (unsigned long j = 0; j < nP; ++j) {
        iIndex[currentIndex] = j+nP+nQ;
        jIndex[currentIndex++] = j+nDonors+nTm1nP;
      }
      for (unsigned long i = 0; i < nTm1nP; ++i) {
        iIndex[currentIndex] = i+nQ+nP+nPatients;
        jIndex[currentIndex++] = i+nDonors+nTm1nP;
      }
    } else {
      for (unsigned long j = 0; j < nDonors; ++j) {
        for (unsigned long i = 0; i < nP; ++i) {
          iIndex[currentIndex] = i+j*nP;
          jIndex[currentIndex++] = j;
        }
      }
      for (unsigned long j = 0; j < nDonors; ++j) {
        for (unsigned long i = 0; i < nP; ++i) {
          iIndex[currentIndex] = i+nQ;
          jIndex[currentIndex++] = j;
        }
      }
      for (unsigned long j = 0; j < nDonors; ++j) {
        for (unsigned long i = 0; i < nP; ++i) {
          iIndex[currentIndex] = i+nQ+nP;
          jIndex[currentIndex++] = j;
        }
      }
      // Part from steady state composition
      for (unsigned long j = 0; j < nDonors; ++j) {
        for (unsigned long i = 0; i < nTm1nP; ++i) {
          iIndex[currentIndex] = i+j*nP;
          jIndex[currentIndex++] = nDonors+i;
        }
      }
      for (unsigned long j = 0; j < nTm1nP; ++j) {
        iIndex[currentIndex] = j+nQ;
        jIndex[currentIndex++] = j+nDonors;
      }
      for (unsigned long j = 0; j < nTm1nP; ++j) {
        iIndex[currentIndex] = j+nQ+nPatients;
        jIndex[currentIndex++] = j+nDonors;
      }
      for (unsigned long j = 0; j < nTm1nP; ++j) {
        iIndex[currentIndex] = j+nQ+nP;
        jIndex[currentIndex++] = j+nDonors;
      }
      // Part of value
      for (unsigned long i = 0; i < nDonors; ++i) {
        for (unsigned long j = 0; j < nP; ++j) {
          iIndex[currentIndex] = j+i*nP;
          jIndex[currentIndex++] = j+nDonors+nTm1nP;
        }
      }
      for (unsigned long i = 0; i < nP; ++i) {
        iIndex[currentIndex] = i+nP+nQ;
        jIndex[currentIndex++] = i+nDonors+nTm1nP;
      }
      for (unsigned long i = 0; i < nTm1nP; ++i) {
        iIndex[currentIndex] = i+nQ+nP+nPatients;
        jIndex[currentIndex++] = i+nDonors+nTm1nP;
      }
    }
    for (unsigned long i = 0; i < currentIndex; ++i) {
      value[i] = 1.0;
    }
    const unsigned long matrixSize = nDonors + nP + nTm1nP;
    plhs[0] = mxCreateSparse(nP*(nDonors+2l), matrixSize, currentIndex, mxREAL);
    double *sr1  = (double*)mxGetPr(plhs[0]);
    size_t* irs1 = (size_t*)mxGetIr(plhs[0]);
    size_t* jcs1 = (size_t*)mxGetJc(plhs[0]);
    COO_toCSC(matrixSize, currentIndex, iIndex, jIndex, value, jcs1, irs1, sr1);
  }
  // Free all allocated memory
  free(iIndex);
  free(jIndex);
  free(value);
}
