#Folder: reduced_form

This folder contains scripts that generate reduced form analysis 
mechanism.

code/summary_statistics.do generates Tables 1 through 4
code/positive_crossmatch_model.do generates Table A.1
code/evidence_of_dynamic_incentives.do generates Tables C.6, Table E.7, and Figure C.1
code/past_offers.do prepares dataset for Table E.8
code/autocorrelation_test.m generates Table E.8
code/patient_order.m generates Table E.9

scripts in code/functions should not be run independently
