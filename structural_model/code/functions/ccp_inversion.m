
%**********************************************************************
%*** Performs CCP inversion described in Equations 5-6 of main text ***
%**********************************************************************

% Pass parameter p and/or mu
%
% G = standard normal distribution
% \psi(p) = E[\max\{0,\varepsilon - G^{-1}(p)\}] when passed two arguments
% \psi(p) = E[\max\{0,X}] X~N(mu,1) when passed one argument
% so, \psi(p) equals 0 if X<0  (or equivalently when Z < -mu which occurs with 
% probability  (normcdf(-mu)) and with probability normcdf(mu) is drawn 
% from a truncated normal E(X|X>0)=mu+normpdf(0-mu)/(1-normcdf(0-mu))
% Thus, \psi(p) = normcdf(mu)*mu + normpdf(-mu). By symmetry, we can get
% rid of the minus in normpdf(-mu).

% mu = - chi*post.theta - data.eta_mat*post.eta;  


function [phi] = ccp_inversion(mu,p)

    % Original mu is ignored if p is passed instead of mu
    if nargin>1
        mu = - norminv(p);
    end 

    % E[\max_u\{0,\varepsilon + mu\}] = \phi(-mu) + mu(1-\Phi(-mu) =
    % \phi(-mu) + mu\Phi(mu)
    phi = normpdf(mu) + mu.*normcdf(mu);

end
