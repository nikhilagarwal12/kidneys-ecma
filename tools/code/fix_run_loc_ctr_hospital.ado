// Links hospital information with offer data and adds donor_hosp_area_c in deceased_donors_encoded.
// donor_hosp_area_c is our best guess of the area associated with the donor's hospital.(to be run during variances preKAS)

program fix_run_loc_ctr_hospital

args genfile data_folder

local gf = "`genfile'" == "genfile"

if `gf' {
preserve
keep if local_match & match_run_num==1 & strpos(descrip,"Local")>0 
collapse (count) offers=organ_sequence_num , by(donor_id listing_ctr_id)
merge m:1 donor_id using "`data_folder'deceased_donors.dta", keepusing(donor_hospital)
collapse (count) runs=offers (sum) offers, by(donor_hospital listing_ctr_id)
drop if listing_ctr_id==""
bysort donor_hospital (runs offers): gen candidate = _n== _N
keep if candidate
drop candidate
drop if donor_hospital==""
isid donor_hospital
keep donor_hospital listing_ctr_id
ren listing_ctr_id donor_hosp_area_c
merge 1:m donor_hospital using "`data_folder'deceased_donors.dta", nogen keepusing(donor_id)
keep donor_id donor_hosp_area_c
save "`data_folder'donor_hosp_area_c.dta",replace
restore 
}
else {
merge m:1 donor_id using "`data_folder'donor_hosp_area_c.dta", keep(master match) keepusing(donor_hosp_area_c)
replace run_loc_ctr = donor_hosp_area_c if _merge==3 & local_match
drop donor_hosp_area_c _merge
}
end


