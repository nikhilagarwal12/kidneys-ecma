% Computes the Value of a random offer.

function [V_init,h,pt_hist_max,patient_max]=average_offer(don_pat_pairs,model,donors,patients,pt_history,chain,V_init,onlyLocal);

verbose = 1;

%% A. Inline functions
% 1 "A" 11 "A1" 12 "A2" 2 "B" 0 "O" 3 "AB" 31 "A1B" 32 "A2B" -> {'O','A','B','AB'}
abo_fn = @(x) (x==0)+2*(x==1 | x == 11 | x ==12)+3*(x==2)+4*(x==3 | x==31 | x==32); 
flat = @(x) x(:);

%% Robust to different ways to enter V_init: Create the structure if V_init is the same size as pt_history
if ~isstruct(V_init) && size(pt_history,1)==size(V_init,1)
    V_init =struct('pt_history',pt_history.wlreg_audit_id_code,'V',V_init);
end
if nargin<8, onlyLocal = 0; end

%% B. Set up
[ismem,where_in_pt_history] = ismember(V_init.pt_history,pt_history.wlreg_audit_id_code); assert(all(ismem));
[ismem,where_in_patients]   = ismember(pt_history.wl_id_code(where_in_pt_history),patients.wl_id_code); assert(all(ismem));

%% C. Loop
N_iter = numel(V_init.pt_history);
V_init.tioli = zeros(N_iter,model.K_types);   % if model.pt_uh, this assumes weights calculated for each unobserved patient type
V_init.cont = zeros(N_iter,model.K_types); 
if model.donor_uh, s = sqrt(1+chain.mu_sigma_eta); else, s = 1; end  % sigma_eta is the VARIANCE of eta

if verbose
    tt = tic;
    fprintf('average_offer.m\n')
end
    
for nn = 1:length(where_in_patients)
    
    % Store data on the patient
    this_patient = patients(where_in_patients(nn),:);
    this_pt_hist = pt_history(where_in_pt_history(nn),:);
    pat_rho = model.rho;
    
    % Create offerset
    off = offerset(don_pat_pairs,this_pt_hist,this_patient,donors,[]);
    if onlyLocal, off = off.onlyLocal; end
    denom = numel(unique(off.indices.donors));
    % Exclude offers
    off = off.excludeUnacceptables;   % Do not do: .excludeBin0.excludeNever;
    frac_of_donors = numel(unique(off.indices.donors))/denom;
    [off,dates] = matchDates(off,'increments',0);
    dates.init_date = this_patient.init_date;
    
    % Generate a data structure with the same offers in off.
    this_data = gen_data_structure(model,[],off.subsetvars,patients,donors,pt_history,'value_function');

    pr_pos_xmatch = predict_pos_xmatch(off.data,this_pt_hist,patients,model.pos_xmatch.coefs);
    %% D.4 Compute \psi(p)
    for kk = 1:model.K_types
        if model.pt_uh
            chi = chi_param(this_data,model,kk);
        else
            chi = chi_param(this_data,model);
        end
        mu = - chi*chain.mu_theta;
        % normcdf(mu) is the probability of NOT accepting a kidney.
        % cpp_inversion takes mu See lines 87-91 in gibbs_mixture.  
        V_init.cont(nn,kk) = mean(ccp_inversion(mu/s))*s*frac_of_donors;
        V_init.tioli(nn,kk) = mean(ccp_inversion((V_init.V(nn,kk) + mu)/s))*s*frac_of_donors;
    end
    ts = toc(tt);
    if mod(nn,1000)==1 && verbose, fprintf('Patient %d of %d. Elapsed Time: %3.2f. Estimated Time: %3.2f\n',nn,N_iter,ts,ts/nn*N_iter); end
end

if nargout>1
    q = [0.01 0.025 0.05 0.5 0.95 0.975 0.99];
    h = figure(1);
    for kk = 1:model.K_types  % allowing for multiple patient types
        if model.pt_uh, typestr = [', Type ' num2str(kk)]; else typestr = ''; end
        subplot(2*model.K_types,2,(kk-1)*4+1), ksdensity(V_init.cont(:,kk));   xlabel(['CONT:  E(Emax(\Gamma_{ik} - V_0,0))' typestr]);
        subplot(2*model.K_types,2,(kk-1)*4+2), ksdensity(V_init.tioli(:,kk));  xlabel(['TIOLI: E(Emax(\Gamma_{ik},0))' typestr]); 
        subplot(2*model.K_types,2,(kk-1)*4+3), aug_ecdf(-log(V_init.cont(:,kk)),q); xlabel(['CONT: -Log(E(Emax(\Gamma_{ik}- V_0,0)))' typestr]);
        subplot(2*model.K_types,2,(kk-1)*4+4), aug_ecdf(-log(V_init.tioli(:,kk)),q); xlabel(['TIOLI: -Log(E(E(Emax(\Gamma_{ik},0))' typestr]);
        for type = {'tioli','cont'}
            fprintf('%s\n',char(type),typestr)
            for x = [0,0.01,0.025,0.05,0.1]
	        fprintf('q(%1.2f)/q(%1.2f) = %4.2f \n', (1-x),x,quantile(V_init.(char(type)),1-x)/quantile(V_init.(char(type)),x));
            end
        end
    end

    [maximum_w,ix] = max(-log(V_init.cont));
    [minimum_w,iy] = max(-log(V_init.cont));
    pt_hist_max = V_init.pt_history(ix);
    where = find(pt_history.wlreg_audit_id_code == pt_hist_max);
    pt_hist_max=pt_history(where,:);
    where = find(patients.wl_id_code== pt_hist_max.wl_id_code);
    patient_max= patients(where,:);
end

function aug_ecdf(x,q)
    ecdf(x);
    line(repmat(quantile(x,q),2,1),[0;1],'LineStyle',':','Color','black');
