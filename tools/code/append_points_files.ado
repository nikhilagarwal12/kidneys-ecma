// This function takes a range of years and creates a dataset of kidney points sources for those years
// Same as append_ptr_files.ado, just different file path

program append_points_files

args start_year end_year points_data

local start_append_year = `start_year' + 1

use "`points_data'ptr_ki`start_year'_score.DTA", clear

if `end_year'>`start_year'{
  forvalues x = `start_append_year'/`end_year'{
    append using "`points_data'ptr_ki`x'_score.DTA"
  }
}

end
