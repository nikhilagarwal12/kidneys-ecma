// File to create sim_offers_basic for matching summary statistics

program create_sim_offers_file

args sim_offers_basic primary_folder


// using file "simulated_offers_matlab", which already restricts to offers permitted by the mechanism and de-dupicates donor-patient pairs
use "`primary_folder'simulated_offers_matlab.dta", clear

  keep donor_id match_date match_run_num wl_id_code wlreg_audit_id screened accept_yn position_dat geo *_mm
  gen local = geo==1
  gen hlamis = a_mm + b_mm + dr_mm

  // determine merge in organ_sequence_num from actual match runs
  merge 1:m donor_id wl_id_code using "`primary_folder'offers.dta", keep(master match) keepusing(organ_sequence_num c_accept) nogen
  gen top_10 = organ_sequence_num <= 10
  gen willing_to_accept = c_accept == "Yes"
  isid donor_id wl_id_code

save "`sim_offers_basic'", replace

end
