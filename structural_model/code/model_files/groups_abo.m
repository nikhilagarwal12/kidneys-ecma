% Split patients and donors in groups for analysis.

clear

% Filepath
group_folder = '/proj/organs/common_output/structural/input/';
group.name = 'patient_abo';
group.gen_script = mfilename('fullpath'); % name of script that generated model file (i.e. name of this script)
group.gen_date = datetime('today'); % date model file was generated

pt_abo_groups = {'O','A','B','AB'};
abo_fn = @(x) (x==0)+2*(x==1 | x == 11 | x ==12)+3*(x==2)+4*(x==3 | x==31 | x==32); 

group.groups = cell(numel(pt_abo_groups),1);
for d = 1:numel(pt_abo_groups)
    group.groups{d}.label = [pt_abo_groups{d} 'Patients'];
    group.groups{d}.conditions(1).data = 'patients';
    group.groups{d}.conditions(1).variables= {'abo'};
    group.groups{d}.conditions(1).f = @(x) (abo_fn(x) == d);
end
d = d+1;
group.groups{d}.label = ['All'];
group.groups{d}.conditions(1).data = 'patients';
group.groups{d}.conditions(1).variables= {'abo'};
group.groups{d}.conditions(1).f = @(x) (abo_fn(x) > 0);

save([group_folder group.name],'group');

