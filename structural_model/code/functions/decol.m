function [y,index]=decol(x,varnames)

rk=rank(x'*x);
[V,D]=eig(x'*x);  %(x'x)*V = V*D
[~,ix]=sort(diag(D),'descend'); in=ix(1:rk);
cl=V; cl(:,in)=[]; %These linear combinations produce zero vectors.

% Display the linearly dependent variables
display('The following sets of variables are linearly dependent:')
for ii = 1:size(cl,2)
    this_col = cl(:,ii).^2;
    [weights,ix] = sort(this_col,'descend');
    varnames(ix(1:3))
    weights(1:3)'
end

[Q,R,E] = qr(cl'); %cl'X'=0; cl'E=QR where Q is unitary, R is a upper
%diagonal matrix and E is a permutation matrix. QRinv(E)X'=0 ->Rinv(E)X'=0
%Xinv(E)'R'=0
invEp=inv(E)'; 
index=sum(invEp(:,1:size(Q,1)),2)==0;
y=x(:,index);
