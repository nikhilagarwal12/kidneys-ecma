
program gen_c_accept

/*
c_accept will take on the following values:

Y: Accepted
S: Screened; The patient is eligible for the kidney, but the kidney does not meet minimum desirability criteria
T: Technological Constraint; The patient is eligible for the kidney, but for some medical reason that kidney cannot be transplanted to the patient 
M: Negligence; The patient is eligible, the kidney meets criteria, but the patient is not responsive.
N: Genuine Rejection. The patient is eligible, has not expressed preferences that rule it out ex-ante, but prefers to wait.
I: The patient is inactive or not available for transplantation for patient-specific reasons.
B: Bypass; The patient did not receive this offer for non-patient-specific reasons
U: Unacceptable antigens, expects a positive crossmatch.


 Refusal Codes and classification in c_accept
800  Patient's condition improved, transplant not needed  [I]
801  Patient ill, unavailable, refused, or temporarily unsuitable [I]
802  Multiple organ transplant or different laterality is required [T]
803  Patient transplanted [I]
810  Positive crossmatch [Y]
811  Number of HLA mismatches unacceptable [N]
812  No Serum [M]
813  Unacceptable Antigens [U]
814  High PRA [U]
815  High CPRA [U]
820  Heavy workload [M]
821  Operational – transplant center (inactive as of 4/18/2007) [M]
822  Exceeded one hour response time [M]
823  Surgeon unavailable [M]
824  Distance to travel or ship [N]
825  Operational – transplant center [N]
830  Donor age or quality [N]
831  Donor size/weight [N]
832  Donor ABO [N]
833  Donor social history [N]
834  Positive serological tests [N]
835  Organ Preservation [N]
836  Organ anatomical damage or defect [N]
837  Organ-specific donor issue [N]
850  Multi-organ placement [B]
851  Directed donation [B]
852  Military donor [B]
853  ALU, Sharing Agreement, Variance [B]
860  Medical urgency of another potential recipient [B]
861  Operational – OPO [B]
862  Donor medical urgency [B]
863  Offer not made due to expedited placement attempt [B]
880  Kidney placed with Extra-renal [B]
881  Maximum 0mm offer limits exceeded [B]
882  Not offered - Non facilitated pancreas center [B]
883  Not offered - Min acceptance criteria not met [S]
898  Other Specify [See below]
*/

// Deal with 898
gen c_ref = refusal_id
replace c_ref = 898 if c_ref == . & (offer_accept == "N" | offer_accept == "B")
replace c_ref = 799 if c_ref == . & offer_accept == "Y"  // for actual acceptance
replace c_ref = 851 if strpos(refusal_ostxt,"DIRECT DONATION")>0 | strpos(refusal_ostxt,"DIRECTED DONATION")>0
replace c_ref = 852 if strpos(refusal_ostxt,"MILITARY")>0 
replace c_ref = 830 if strpos(refusal_ostxt,"DECLINE")>0 | strpos(refusal_ostxt,"REFUSE")>0 
replace c_ref = 861 if strpos(refusal_ostxt,"BACKUP")>0 | strpos(refusal_ostxt,"BACK-UP")>0
replace c_ref = 883 if strpos(refusal_ostxt,"ACCEPTANCE CRITERIA")>0 & strpos(refusal_ostxt,"DOES NOT MEET")>0
replace c_ref = 863 if strpos(refusal_ostxt,"AGGRESSIVE PLACEMENT")>0 | strpos(refusal_ostxt,"EXPEDIATED PLACEMENT")>0 | strpos(refusal_ostxt,"EXPEDITED PLACEMENT")>0

gen is_bpss = ((offer_accept == "B" & c_ref == 898) | (c_ref >= 850 & c_ref <= 882))
gen is_scrn = c_ref == 883 
gen is_tech = c_ref == 802 
gen is_uncc = c_ref >= 813 & c_ref <= 815
gen is_accp = c_ref == 799
gen is_gnno = c_ref == 810 | c_ref == 811 | (c_ref >= 824 & c_ref <= 837) | (c_ref ==898 & (offer_accept=="N" | offer_accept == ""))
gen is_negl = c_ref == 812 | (c_ref>=820 & c_ref<= 823)
gen is_inac = c_ref<= 803 & c_ref~=802 & c_ref ~= 799
gen is_noans = c_ref == .  // For missing refusal id's with missing offer_accept, treat as no answer. Usually occurs after final acceptance, so not relevant

assert is_bpss +  is_scrn +  is_accp + is_gnno + is_negl + is_inac + is_tech + is_uncc + is_noans == 1

gen c_accept = ""
replace c_accept = "Bypass" if is_bpss
replace c_accept = "Screen" if is_scrn
replace c_accept = "Yes" if is_accp
replace c_accept = "No" if is_gnno
replace c_accept = "Negligence" if is_negl
replace c_accept = "Inactive" if is_inac
replace c_accept = "Technological" if is_tech
replace c_accept = "Unacceptable" if is_uncc
replace c_accept = "Non-Response" if is_noans

gen c_accept_810 = c_accept
replace c_accept_810 = "Yes" if refusal_id == 810  // this variable simply re-codes 810's as "Yes", since the patient would have accepted with a negative crossmatch
replace c_accept = c_accept_810
    
drop c_ref-is_inac

end
