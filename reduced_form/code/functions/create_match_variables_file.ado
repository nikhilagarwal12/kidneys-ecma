// Create a file at the patient-donor match level with the variables we need for summary stats


program create_match_variables_file

args kidpan_w_vars deceased_donors_w_vars transplants sim_offers_basic match_variables_file primary_folder sample_name

// basic offer data from our simulation of the mechanism
use `sim_offers_basic', clear

// merge some additional patient and donor characteristics
merge m:1 donor_id using `deceased_donors_w_vars', keep(match) keepusing(kdri age_don da1 da2 db1 db2 ddr1 ddr2 opo_dsa_id opo_region_id recovery_date_don ecd_donor abo_don abo_don_* race_don_* kdri kdpi hep_c_anti_don hep_c_antibody hist_diabetes_don hist_hypertens_don diabetes_don hypertension_don non_hrt_don non_heart_beating creat_don) nogen

merge m:1 wl_id_code using `kidpan_w_vars', keep(match using) keepusing(abo ethcat init_age init_date listing_ctr_dsa_id listing_ctr_region_id male_pt diab diab*pt abo_pt abo_pt_* race_pt race_pt_* func_stat_listing prior_transplant dialysis_date a1 a2 b1 b2 dr1 dr2 pdiag_* payment_* age_cat cpra_cat peak_cpra_cat epts_cat sample_time_active*)
  gen no_offers = _merge == 2  // mark patients who never received an offer during the sample period
  drop _merge

merge m:1 wlreg_audit_id_code using "`primary_folder'patient_history.dta", keep(master match) keepusing(cpra) nogen

merge 1:1 donor_id wl_id_code using `transplants'  // who was transplanted
keep if _merge != 2
gen transplanted = _merge==3
drop _merge

// construct EPTS at time of offer
sysdir set PERSONAL ../../tools/code/   // to call compute_epts .ado file

gen first_tx_date = match_date - 1 if prior_transplant == 1  // for epts function
compute_epts  // computing EPTS at the time the patient entered our sample, either at sample_start or when you joined the waiting list if after
rename epts_raw epts
replace years_on_dialysis = . if years_on_dialysis < 0   // only those on dialysis should contribute to the average time
gen on_dialysis = years_on_dialysis > 0 & years_on_dialysis != .
gen on_dialysis_at_init_date = dialysis_date < init_date

gen years_on_waitlist = floor((recovery_date_don - init_date) / 365)
replace years_on_dialysis = floor((recovery_date_don - dialysis_date) / 365)
replace years_on_dialysis = 0 if years_on_dialysis == .

sysdir set PERSONAL ../../reduced_form/code/functions/

// Geography of offer
gen regional = local == 0 & listing_ctr_region_id == opo_region_id
gen national = (1-local)*(1-regional)

// categorize age, epts, cpra, kdri variables
gen age_pt_0_17 = age_guess <= 17
gen age_pt_18_34 = age_guess > 17 & age_guess <= 34
gen age_pt_35_49 = age_guess > 35 & age_guess <= 49
gen age_pt_50_64 = age_guess > 50 & age_guess <= 64
gen age_pt_65_plus = age_guess >= 65

// Binary age category variable
gen init_age_cat = "Age 0-49 at Listing" if init_age < 50  
replace init_age_cat = "Age 50+ at Listing" if init_age >= 50

gen diabetes_str = "Has Diabetes"
replace diabetes_str = "No Diabetes" if diabetes == 0

gen dialysis_str = "On Dialysis at Registration"
replace dialysis_str = "Not on Dialysis at Registration" if on_dialysis_at_init_date==0

gen dial_age = "Not on Dialysis and 50+ at Registration" if init_age >= 50 & on_dialysis_at_init_date==0
replace dial_age = "Not on Dialysis and 0-49 at Registration" if init_age < 50 & on_dialysis_at_init_date==0
replace dial_age = "On Dialysis and 50+ at Registration" if init_age >= 50 & on_dialysis_at_init_date==1
replace dial_age = "On Dialysis and 0-49 at Registration" if init_age < 50 & on_dialysis_at_init_date==1


replace cpra = round(cpra,.01)
gen cpra_0 = cpra == 0 | cpra == .
gen cpra_1_79 = cpra > .01 & cpra <= .79
gen cpra_80_94 = cpra >= .80 & cpra < .95
gen cpra_95_98 = cpra >= .95 & cpra < .99
gen cpra_99_100 = cpra == .99 | cpra == .100

gen epts_0_20 = epts <= 1.48933264887064
gen epts_20_40 = epts > 1.48933264887064 & epts <= 2.018842573579740
gen epts_40_60 = epts > 2.018842573579740 & epts <= 2.374824807193870
gen epts_60_80 = epts > 2.374824807193870 & epts <= 2.708639466580470
gen epts_80_100 = epts > 2.708639466580470

gen kdpi_0_20 = kdpi <= .20
gen kdpi_20_40 = kdpi > .20 & kdpi <= .40
gen kdpi_40_60 = kdpi > .40 & kdpi <= .60
gen kdpi_60_80 = kdpi > .60 & kdpi <= .80
gen kdpi_80_100 = kdpi > .80

// some match characteristics: hlamis, a_mm, b_mm, dr_mm available from mech_offers_processed
gen abo_identical = abo == abo_don
gen zero_abdr_mismatch = hlamis == 0
gen zero_dr_mismatch = dr_mm == 0

gen epts_0_35 = epts <= 1.908700889801510
gen kdpi_0_35 = kdpi <= .35
gen epts_85_100 = epts > 2.800840099563680
gen kdpi_85_100 = kdpi > .85

gen epts_0_20_kdpi_0_20 = epts_0_20*kdpi_0_20  // matching top 20% patients and donors
gen epts_0_35_kdpi_0_35 = epts_0_35*kdpi_0_35  // matching top 35% patients and donors
gen epts_85_100_kdpi_85_100 = epts_85_100*kdpi_85_100  // matching bottom 15% patients and donors
gen epts_60_100_kdpi_60_100 = (epts_60_80 + epts_80_100)*(kdpi_60_80 + kdpi_80_100) // matching bottom 40% patients and donors

gen age_diff = abs(age_guess - age_don)
gen age_diff_gt_15 = age_diff > 15

gen kdpi_85_100_local = local * kdpi_85_100
gen kdpi_85_100_regional = regional * kdpi_85_100
gen kdpi_85_100_national = national * kdpi_85_100
gen recip_65plus_kdpi_0_35 = age_pt_65_plus * kdpi_0_35

gen cpra_99_100_local = local * cpra_99_100
gen cpra_99_100_regional = regional * cpra_99_100
gen cpra_99_100_national = national * cpra_99_100

rename age_guess age_guess_pt


save `match_variables_file', replace


end
