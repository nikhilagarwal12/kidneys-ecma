#!/use/bin/python

# Library of functions to calculate offer variances

import csv
import re
import datetime
import math
import pdb
from calculate_offers_function_library import min_wait_fun as lib_wait_fun

# Define Functions

def local_areas(donor,pt,match):  # Replaces geo with OPO if donor/pt are in the same OPO but different local areas
    tulsa =set("OKSJ OKSF".split())
    oklah =set("OKBC OKCM OKMD OKSA".split())
    padv1 =set("PAGM PAHE PALV PAWV PAHH".split())
    padv2 =set("PAAE PACP PAHM PAUP PASC PATU PATJ PALH DEAI DECC".split())
    patf1 =set("WVCA WVWU".split())
    patf2 =set("PACH PAAG PAPT PAVA".split())
    souea =set("TXHH TXHI TXMH TXTC".split())
    north =set("TXFW TXCF TXGV".split())
    west =set("TXLG TXLM".split())
    dallas =set("TXTX TXCM TXMC TXPM TXHD TXSP".split())
    elpaso =set("TXSM TXLP TXCF TXGV".split())
    galves =set("TXJS TXDC".split())
    tyler =set("TXTY TXSW".split())
    all_areas = [tulsa,oklah,padv1,padv2,patf1,patf2,souea,north,west,dallas,elpaso,galves,tyler]
    for ar in all_areas:
        if match['geo'] == "Local" and pt['listing_ctr'][0:4] in ar and donor['run_loc_ctr'][0:4] not in ar:
            match['geo']="OPO"
    
    return match
    
def time_vars_fun(donor,pt,match):
    # Adjust Dialisis time in CAOP-OP1 OneLegacy, IAOP-OP1 Iowa Donor Network and MIOP-OP1 Gift of Life Michigan
    if match['geo'] == "Local" and donor['run_opo_id']  in ["CAOP-OP1","MIOP-OP1","IAOP-OP1"]:
        pt['dialysis_date']=match['match_date'] if pt['dialysis_date']=='' else datetime.datetime.strptime(pt['dialysis_date'], "%d%b%Y")
        dial_date =  pt['dialysis_date'].timetuple()
        match_date = match['match_date'].timetuple()
        match['p_wait_wl'] = match_date.tm_year - dial_date.tm_year - 1*(match_date.tm_yday < dial_date.tm_yday)
        match['c_tie_break'] = (match['match_date'] - pt['dialysis_date']).days
    elif match['geo'] == "Local" and donor['run_opo_id'] in["MAOB-OP1","CTOP-OP1"]:
        pt['dialysis_date']=match['match_date'] if pt['dialysis_date']=='' else datetime.datetime.strptime(pt['dialysis_date'], "%d%b%Y")
        wait_dialysis = (match['match_date'] - pt['dialysis_date']).days
        # Points
        match['p_wait_wl'] = (1- (1-(min(wait_dialysis,180)/180)**2)**(0.5))*8 if match['c_age'] <6 else 0
        match['p_wait_wl'] = (1- (1-(min(wait_dialysis,365)/365)**2)**(0.5))*8 if match['c_age'] >=6 and match['c_age'] <11 else match['p_wait_wl']
        match['p_wait_wl'] = (1- (1-(min(wait_dialysis,545)/545)**2)**(0.5))*8 if match['c_age'] >=11 and match['c_age'] <18 else match['p_wait_wl']
        match['p_wait_wl'] = min(wait_dialysis,545)/545*8  if match['c_age'] >=18 else match['p_wait_wl']
        match['c_tie_break'] = (match['match_date'] - pt['dialysis_date']).days
    return match
    
    
#def variances_bin(bin,geo,don_opo,lis_opo,donor_cat):
def points_and_bins_fun(donor,pt,match):
    bin = match['c_bin']
    amis = match['a_mm']
    bmis =  match['b_mm']
    drmis = match['dr_mm']
    pt['cpra']=float(pt['cpra'])
    match['p_urgency'] = 0
    match['highest_scoring'] = False # Need to fix this!!
    Ped_List = match['Ped_List']=="Ped At Listing"
    Ped_Alloc = match['Ped_Alloc']=="Ped At Allocation"
    prior_living_donor = pt['prior_living_donor']=="1"
    incompatible_paired_recipient = 0 #pt['incompatible_paired_recipient']=="1" 
    
    if donor['run_opo_id'][0:2] in ('TX','PA','OK') and match['geo']=="Local":
        match = local_areas(donor,pt,match)
    # Variance FL
    if donor['run_opo_id'][0:2]=="FL":
        bin = 1.9 if match['hlamis'] == "Zero" and match['c_abo']=="Identical" and match['geo'] =="Statewide" else bin
        bin = 14.9 if match['hlamis'] == "Zero" and match['c_abo']=="O Donor B Candidate" and match['geo'] =="Statewide" and bin>14 else bin
        bin = 27.9 if match['hlamis'] == "Zero" and match['c_abo']=="Compatible" and match['geo'] =="Statewide" and bin>27 else bin
        bin = 40.9 if prior_living_donor and not (match['c_abo']=="Incompatible") and match['geo'] =="Statewide" and bin>40 else bin
        bin = bin*10
        if match['geo']=="Regional" and pt['listing_ctr_dsa_id'][0:2] =="FL":
            match['geo'] ="Statewide"
            bin = bin -5
            bin = 441 if bin == 445 and donor['donor_cat'] == "young_healthy" else bin  # The Statewide block should be run before the Regional block
            bin = 442 if bin == 455 and donor['donor_cat'] == "young_healthy" else bin 
            bin = 443 if bin == 465 and donor['donor_cat'] == "young_healthy" else bin
            bin = 81 if bin == 85 and donor['donor_cat'] == "young_dcd" and (not match['hlamis'] == "Zero") else bin 
            bin = 82 if bin == 95 and donor['donor_cat'] == "young_dcd" and (not match['hlamis'] == "Zero") else bin 
            bin = 83 if bin == 105 and donor['donor_cat'] == "young_dcd" and (not match['hlamis'] == "Zero") else bin 
            bin = 31 if bin in set([85,95,105]) and donor['donor_cat'] == "young_dcd" and match['hlamis'] == "Zero" else bin 
            bin = 21 if bin == 31 and donor['donor_cat'] == "young_dcd" and  match['c_abo']=="O Donor B Candidate" else bin 
            bin = 11 if bin == 31 and donor['donor_cat'] == "young_dcd" and match['c_abo']=="Identical" else bin 
            bin = 31 if bin in set([65]) and donor['donor_cat'] == "old_dcd" and match['hlamis'] == "Zero" else bin 
            bin = 21 if bin == 31 and donor['donor_cat'] == "old_dcd" and  match['c_abo']=="O Donor B Candidate" else bin 
            bin = 11 if bin == 31 and donor['donor_cat'] == "old_dcd" and match['c_abo']=="Identical" else bin 
    
    # Variance TN
    elif  donor['run_opo_id'][0:2]=="TN":
        
        match['geo'] ="Statewide" if  pt['listing_ctr_dsa_id'][0:2] == donor['run_opo_id'][0:2] and match['geo'] =="Regional" else match['geo']
        mandatory = (bmis+drmis<2 or pt['cpra']>0.395 or match['cmis']+drmis==0 or pt['unos_cand_stat_cd']==4060) and (match['geo'] =="Statewide" or match['geo'] =="Local")
        if donor['donor_cat'] == "young_healthy" or donor['donor_cat'] == "old_healthy" or donor['donor_cat']=="ecd":
            bin = 1 if match['hlamis'] == "Zero" and match['c_abo']=="Identical" and match['geo'] =="Statewide" else bin
            bin = 14 if match['hlamis'] == "Zero" and match['c_abo']=="O Donor B Candidate" and match['geo'] =="Statewide" and bin>14 else bin
            bin = 27 if match['hlamis'] == "Zero" and match['c_abo']=="Compatible" and match['geo'] =="Statewide" and bin>27 else bin
            bin = 40 if prior_living_donor and not (match['c_abo']=="Incompatible") and match['geo'] =="Statewide" and bin>40 else bin
            bin = bin * 10 if bin>40 else bin
            if donor['donor_cat'] == "young_healthy":
                bin = 410 if mandatory and match['highest_scoring'] and bin> 410 else bin
                bin = 411 if mandatory and (Ped_List or Ped_Alloc) and match['geo'] =="Statewide" and bin>411 else bin
                bin = 412 if (not mandatory) and match['highest_scoring'] and match['geo'] =="Local" and bin>412 else bin
                bin = 412 if (not mandatory) and match['highest_scoring'] and match['geo'] =="Statewide" and bin>413 else bin
                bin = 420 if (not mandatory) and (Ped_List or Ped_Alloc) and match['geo'] =="Local" and bin>420 else bin
                bin = 421 if (not mandatory) and (Ped_List or Ped_Alloc) and match['geo'] =="Statewide" and bin>421 else bin
                bin = 439 if mandatory and bin>439 else bin
                bin = 441 if (not mandatory) and match['geo']== "Statewide" and bin> 441 else bin
                bin = 441 if (not mandatory) and match['geo']== "Statewide" and bin> 441 else bin
            elif donor['donor_cat'] == "old_healthy":
                bin = 419 if mandatory and bin> 419 else bin
                bin = 421 if (not mandatory) and match['geo']== "Statewide" and bin> 422 else bin
            elif donor['donor_cat'] == "ecd":
                bin = 410 if match['geo']== "Statewide" and bin> 410 else bin
        elif donor['donor_cat']== "old_dcd" or donor['donor_cat']== "young_dcd":
            bin = 4 if prior_living_donor and match['geo'] =="Statewide" and bin>4 else bin
            bin = bin * 10 if bin>4 else bin
            if donor['donor_cat']== "old_dcd":
                bin = 59 if mandatory  and bin> 59 else bin
                bin = 61 if (not mandatory) and match['geo']== "Statewide" and bin> 61 else bin
            if donor['donor_cat']== "young_dcd":
                bin = 48 if mandatory and match['highest_scoring'] and bin> 48 else bin
                bin = 49 if mandatory and (Ped_Alloc or Ped_List)  and bin> 49 else bin
                bin = 51 if (not mandatory) and match['highest_scoring'] and match['geo']== "Statewide" and bin> 51 else bin
                bin = 61 if (not mandatory) and (Ped_Alloc or Ped_List) and match['geo']== "Statewide" and bin> 61 else bin
                bin = 79 if mandatory and bin> 79 else bin
                bin = 81 if (not mandatory) and match['geo']== "Statewide" and bin> 81 else bin
        elif donor['donor_cat'] == "ecd_dcd":
                bin = 1 if match['hlamis']=="Zero" and match['c_abo']== "Identical" and (match['geo']== "Statewide" or match['geo']== "Local") else bin
                bin = 2 if match['hlamis']=="Zero" and match['c_abo']== "O Donor B Candidate" and (match['geo']== "Statewide" or match['geo']== "Local") and bin>2 else bin
                bin = 3 if match['hlamis']=="Zero" and match['c_abo']== "Compatible" and (match['geo']== "Statewide" or match['geo']== "Local") and bin>3 else bin
                bin = bin*10 if bin > 3 else bin
                bin = 65 if match['geo']== "Statewide"  and bin> 65 else bin
    
        # Points
        if match['geo']== "Local" or match['geo']== "Statewide":
            match['p_hla'] = 2 if bmis+drmis == 2 else 0
            match['p_hla'] = 2 if match['cmis']  ==0  and drmis<3 else match['p_hla']#? Two points are assigned for 0 CREG, a 1 or 2 DR mismatch;
            match['p_hla'] = 5 if bmis+drmis ==1 else match['p_hla']
            match['p_hla'] = 7 if bmis+drmis ==0 else match['p_hla']
            match['p_hla'] = 10 if drmis ==0 and match['cmis']  ==0 else match['p_hla']
            match['p_pra'] = 2 * ((pt['cpra'] >= 0.395) + (pt['cpra']>=0.795)) 
            match['p_urgency'] = 5 if pt['unos_cand_stat_cd'] == 4060 else 0
    # Variance CADN  
    elif donor['run_opo_id'] == "CADN-OP1":
        if donor['donor_cat'] == "young_healthy":
            bin = bin*10 if bin> 40 else bin
            bin = 421 if pt['wlreg_in_master']=='1' and pt['cpra']<0.295 and bin == 420  else bin
            bin = 422 if pt['wlreg_in_master']=='1' and bin == 420  else bin
            bin = 423 if (not pt['wlreg_in_master']=='1') and bin == 420  else bin
            bin = 441 if pt['wlreg_in_master']=='1' and pt['cpra']<0.295 and bin == 420  else bin
            bin = 442 if pt['wlreg_in_master']=='1' and bin == 440  else bin
            bin = 443 if (not pt['wlreg_in_master']=='1') and bin == 440  else bin
        elif donor['donor_cat'] == "old_healthy":
            bin = bin*10 if bin> 40 else bin
            bin = 421 if pt['wlreg_in_master']=='1' and pt['cpra']<0.295 and bin == 420 else bin
            bin = 422 if pt['wlreg_in_master']=='1' and bin == 420 else bin
            bin = 423 if (not pt['wlreg_in_master']=='1') and bin == 420 else bin
        elif donor['donor_cat'] == "young_dcd":
            bin = bin*10 if bin> 4 else bin    
            bin = 61 if pt['wlreg_in_master']=='1' and pt['cpra']<0.295 and bin == 60 else bin
            bin = 62 if pt['wlreg_in_master']=='1' and bin == 60 else bin
            bin = 63 if (not pt['wlreg_in_master']=='1') and bin == 60 else bin
            bin = 81 if pt['wlreg_in_master']=='1' and pt['cpra']<0.295 and bin == 80 else bin
            bin = 82 if pt['wlreg_in_master']=='1' and bin == 80 else bin
            bin = 83 if (not pt['wlreg_in_master']=='1') and bin == 80 else bin
        elif donor['donor_cat'] == "old_dcd":    
            bin = bin*10 if bin> 4 else bin    
            bin = 61 if pt['wlreg_in_master']=='1' and pt['cpra']<0.295 and bin == 60 else bin    
            bin = 62 if pt['wlreg_in_master']=='1' and bin == 60 else bin    
            bin = 63 if (not pt['wlreg_in_master']=='1') and bin == 60 else bin    
        if match['geo'] =="Local" and not (donor['donor_cat'] == "ecd_dcd" or "ecd"):
            match['p_wait_wl'] = match['p_wait_wl']/2
            match['p_hla'] = 2 if pt['awaits_PA'] == '1' else 0
            
    # Variance Region 1
    # Incompatible Living Donor Paired
    
    elif (donor['run_opo_id'] == "MAOB-OP1" or  donor['run_opo_id'] ==  "CTOP-OP1"):
        match['c_bin']=match['c_bin']*10
        if incompatible_paired_recipient:
            match['c_bin']=405 if match['donor_cat'] in set(['young_healthy','old_healthy','ecd']) else 45 
        if match['geo']=="Local":
            population_points=0
            # Points
            match['p_hla'] = 7 if drmis+bmis==0 and not (donor['donor_cat']== "ecd" or donor['donor_cat']== "dcd_ecd") else 0
            match['p_hla'] = match['p_hla'] + population_points
        
        
    # Variance DCTC-OP1
    # Incompatible Living Donor Paired
    elif donor['run_opo_id'] == "DCTC-OP1":
        bin = bin*10
        bin = 1 if match['c_abo'] == "Identical" and match['geo'] =="Local" and incompatible_paired_recipient else bin
    
    
    # Variance LAOP-OP1
    elif donor['run_opo_id'] ==  "LAOP-OP1":
        if match['geo'] =="Local":
            match['p_wait_wl'] = match['p_wait_wl']/2 
            
        
    # Variance MWOB-OP1
    elif donor['run_opo_id'] ==   "MWOB-OP1":
        bin = bin*10
        if donor['donor_cat']== "young_healthy" and (donor['abo_don'] =="A2" or donor['abo_don']=="A2B"):            
            bin = 401 if (bin >= 410 and bin <=  440) and  pt['cpra']>=0.795 and (pt['abo']=="A2" or pt['abo']=="A2B") else bin
            bin = 402 if (bin >= 410 and bin <=  440) and  pt['cpra']>=0.795 and (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
            bin = 403 if (bin >= 410 and bin <=  440) and  (pt['abo']=="A2" or pt['abo']=="A2B") and (Ped_Alloc or Ped_List) else bin
            bin = 404 if (bin >= 410 and bin <=  440) and  (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") and (Ped_Alloc or Ped_List) else bin
            bin = 405 if (bin >= 410 and bin <=  440) and  pt['abo']=="B" and match['highest_scoring'] else bin
            bin = 406 if (bin >= 410 and bin <=  440) and  (pt['abo']=="B" ) and (Ped_Alloc or Ped_List) else bin
            bin = 407 if (bin >= 410 and bin <=  440) and  pt['abo']=="O" and match['highest_scoring'] else bin
            bin = 408 if (bin >= 410 and bin <=  440) and  (pt['abo']=="O" ) and (Ped_Alloc or Ped_List) else bin
            bin = 441 if (bin == 440) and  (pt['abo']=="O" ) else bin
            bin = 442 if (bin == 440) and  pt['cpra']<0.795 and  (pt['abo']=="A2" or pt['abo']=="A2B") else bin
            bin = 443 if (bin == 440) and  pt['cpra']<0.795 and  (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
        elif  (donor['donor_cat']== "young_dcd") and (donor['abo_don'] =="A2" or donor['abo_don'] =="A2B"):
            bin = 41 if (bin >= 50 and bin <=  80) and  pt['cpra']>=0.795 and (pt['abo']=="A2" or pt['abo']=="A2B") else bin
            bin = 42 if (bin >= 50 and bin <=  80) and  pt['cpra']>=0.795 and (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
            bin = 43 if (bin >= 50 and bin <=  80) and  (pt['abo']=="A2" or pt['abo']=="A2B") and (Ped_Alloc or Ped_List) else bin
            bin = 44 if (bin >= 50 and bin <=  80) and  (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") and (Ped_Alloc or Ped_List) else bin
            bin = 45 if (bin >= 50 and bin <=  80) and  pt['abo']=="B" and match['highest_scoring'] else bin
            bin = 46 if (bin >= 50 and bin <=  80) and  (pt['abo']=="B" ) and (Ped_Alloc or Ped_List) else bin
            bin = 47 if (bin >= 50 and bin <=  80) and  pt['abo']=="O" and match['highest_scoring'] else bin
            bin = 48 if (bin >= 50 and bin <=  80) and  (pt['abo']=="O" ) and (Ped_Alloc or Ped_List) else bin
            bin = 81 if (bin == 80) and  (pt['abo']=="O") else bin
            bin = 82 if (bin == 80) and  pt['cpra']<0.795 and  (pt['abo']=="A2" or pt['abo']=="A2B") else bin
            bin = 83 if (bin == 80) and  pt['cpra']<0.795 and  (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
        elif (donor['donor_cat']== "old_healthy") and (donor['abo_don'] =="A2" or donor['abo_don'] =="A2B"):
            if bin == 420:
                bin = 401 if pt['cpra']>=0.795 and (pt['abo']=="A2" or pt['abo']=="A2B") else bin
                bin = 402 if pt['cpra']>=0.795 and (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
                bin = 421 if pt['abo']=="O"  else bin
                bin = 422 if pt['cpra']<0.795 and  (pt['abo']=="A2" or pt['abo']=="A2B") else bin
                bin = 423 if pt['cpra']<0.795 and  (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
        elif (donor['donor_cat']== "old_dcd") and (donor['abo_don'] =="A2" or donor['abo_don'] =="A2B"):
            if bin == 60:
                bin = 51 if pt['cpra']>=0.795 and (pt['abo']=="A2" or pt['abo']=="A2B") else bin
                bin = 52 if pt['cpra']>=0.795 and (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
                bin = 56 if  (pt['abo']=="B" ) else bin
                bin = 58 if  (pt['abo']=="O" ) else bin
                bin = 59 if  pt['cpra']<0.795 and  (pt['abo']=="A2" or pt['abo']=="A2B") else bin
        elif (donor['donor_cat']== "ecd_dcd") and (donor['abo_don'] =="A2" or donor['abo_don'] =="A2B"):
            if bin == 50:
                bin = 41 if pt['cpra']>=0.795 and (pt['abo']=="A2" or pt['abo']=="A2B") else bin
                bin = 42 if pt['cpra']>=0.795 and (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
                bin = 46 if  (pt['abo']=="B" ) else bin
                bin = 48 if  (pt['abo']=="O" ) else bin
                bin = 49 if  pt['cpra']<0.795 and  (pt['abo']=="A2" or pt['abo']=="A2B") else bin
        elif (donor['donor_cat']== "ecd") and (donor['abo_don'] =="A2" or donor['abo_don'] =="A2B"):
            if bin == 410:
                bin = 401 if pt['cpra']>=0.795 and (pt['abo']=="A2" or pt['abo']=="A2B") else bin
                bin = 402 if pt['cpra']>=0.795 and (pt['abo']=="A1" or pt['abo']=="A1B" or pt['abo']=="A"  or pt['abo']=="AB") else bin
                bin = 406 if  (pt['abo']=="B" ) else bin
                bin = 408 if  (pt['abo']=="O" ) else bin
                bin = 409 if  pt['cpra']<0.795 and  (pt['abo']=="A2" or pt['abo']=="A2B") else bin                

        # Points
        if match['geo'] =="Local":
            match['p_wait_wl'] = match['p_wait_wl']/2 
            
    # Variance VATB
    elif donor['run_opo_id'] == "VATB-OP1":
        bin = bin*10
        if incompatible_paired_recipient and pt['listing_ctr'][0:4]==donor['run_loc_ctr'][0:4]:
            bin = 38 if match['donor_cat'] in set(["young_dcd" , "ecd_dcd", "old_dcd"]) else 398
            bin = bin - 1 if match['c_abo']=="Identical" else bin
            
    # Variance NCCM
    elif donor['run_opo_id'] ==  "NCCM-IO1":
        #OPO KI, Incompatible Living Donor Paired
        pass
        
    # Variance ORUO
    elif donor['run_opo_id'] ==  "ORUO-IO1":
        #IMPORTANT FOR PYTHON CODE:
        #Blood group A2 donor kidneys are transplanted into blood group A, B, or AB tranasplant candidates
        #and blood group A2B donor kidneys are transplantedinto blood group B or AB transplant candidates
        pass
    
    # Variance OKOP
    elif donor['run_opo_id'] ==  "OKOP-OP1":
        if  (donor['donor_cat']== "young_healthy" or donor['donor_cat']== "old_healthy"):
            bin = bin*10 if bin>40 else bin
            bin = 421 if bin == 420 and match['geo']== "OPO" and ~(pt['cpra']>0.595 or pt['awaits_PA'] == '1' or Ped_Alloc) else bin
            bin = 441 if bin == 440 and match['geo']== "OPO" and ~(pt['cpra']>0.595 or pt['awaits_PA'] == '1' or Ped_Alloc) else bin
        elif (donor['donor_cat']== "ecd"):
            bin = bin*10 if bin>40 else bin
            bin = 411 if bin == 410 and match['geo']== "OPO" and ~(pt['cpra']>0.595 or pt['awaits_PA'] == '1' or Ped_Alloc) else bin
        elif (donor['donor_cat']== "old_dcd" or donor['donor_cat']== "young_dcd"):
            bin = bin*10 if bin>4 else bin
            bin = 61 if bin == 60 and match['geo']== "OPO" and ~(pt['cpra']>0.595 or pt['awaits_PA'] == '1' or Ped_Alloc) else bin
            bin = 81 if bin == 80 and match['geo']== "OPO" and ~(pt['cpra']>0.595 or pt['awaits_PA'] == '1' or Ped_Alloc) else bin
        elif (donor['donor_cat']== "ecd_dcd"):
            bin = bin*10 if bin > 3 else bin
            bin = 41 if bin == 40 and match['geo']== "OPO" and ~(pt['cpra']>0.595 or pt['awaits_PA'] == '1' or Ped_Alloc) else bin
            bin = 41 if bin == 40 and match['geo']== "OPO" and ~(pt['cpra']>0.595 or pt['awaits_PA'] == '1' or Ped_Alloc) else bin

            
    # Variance PA
    elif donor['run_opo_id'] ==  "PATF-OP1" or donor['run_opo_id'] ==  "PADV-OP1":
        if (donor['donor_cat']== "young_healthy" or donor['donor_cat']== "old_healthy" or donor['donor_cat']=="ecd"):
            bin = bin*10 if bin>39 else bin
            bin = bin + 2 if bin>390 and match['geo']== "OPO" else bin
            bin = bin + 9 if bin==412 and match['geo']== "OPO" and donor['donor_cat']== "young_healthy" else bin
            # First kidney is allocated Locally, the second OPO-wide. People are bypassed with 853'd. 
        elif (donor['donor_cat']== "young_dcd" or donor['donor_cat']== "old_dcd" or donor['donor_cat']=="ecd_dcd"):
            bin = bin*10 if bin>3 else bin
            bin = bin + 2 if bin>30 and match['geo']== "OPO" else bin
            bin = bin + 9 if bin==52 and match['geo']== "OPO" and donor['donor_cat']== "young_dcd" else bin
            
    # Variance TXGC
    elif donor['run_opo_id'] ==  "TXGC-OP1":
        if (donor['donor_cat']== "young_healthy" or donor['donor_cat']== "old_healthy" or donor['donor_cat']=="ecd"):
            bin = bin*10 if bin>39 else bin
            bin = bin + 2 if bin>390 and match['geo']== "OPO" else bin
            bin = bin + 9 if bin==412 and match['geo']== "OPO" and donor['donor_cat']== "young_healthy" else bin
            # First kidney is allocated Locally, the second OPO-wide. People are bypassed with 853'd. 
        elif (donor['donor_cat']== "young_dcd" or donor['donor_cat']== "old_dcd" or donor['donor_cat']=="ecd_dcd"):
            bin = bin*10 if bin>3 else bin
            bin = bin + 2 if bin>30 and match['geo']== "OPO" else bin
            bin = bin + 9 if bin==52 and match['geo']== "OPO" and donor['donor_cat']== "young_dcd" else bin
        #Points
        if match['geo']== "OPO" and match['geo']== "Local" and (donor['donor_cat']== "young_healthy" or donor['donor_cat']== "old_healthy" or donor['donor_cat']=="young_dcd" or donor['donor_cat']=="old_dcd"):
            match['p_hla'] = 0
            match['p_wait_wl'] = match['p_wait_wl']/2 
            
    
   
 
    # Variance TXSA
    elif donor['run_opo_id'] ==  "TXSA-OP1":
        bin = bin*10 if bin>43 and donor['donor_cat']=="young_healthy" else bin
        bin = 435 if donor['donor_cat']=="young_healthy" and bin == 440 and donor['abo_don'] =="A2" and (pt['abo']=="B" or pt['abo']=="O") else bin  
        bin = 435 if donor['donor_cat']=="young_healthy" and bin == 440 and donor['abo_don'] =="A2B" and pt['abo']=="B"  else bin
        # For standard criteria donors 35 years or older, potential recipients are listed in the  OPO KI, Highest Scoring High CPRA 
        #classification only on deceased donor matches where the donor ABO is A2 or  A2B, in order to preserve the priority for
        #the most highly sensitized candidates.     
           
            

    # Variance TXSB
    if donor['run_opo_id'] ==  "TXSB-OP1":
        if (donor['donor_cat']== "young_healthy" or donor['donor_cat']== "old_healthy" or donor['donor_cat']=="ecd"):
            bin = bin*10 if bin>39 else bin
            bin = bin + 2 if bin>390 and match['geo']== "OPO" else bin
            bin = bin + 9 if bin==412 and match['geo']== "OPO" and donor['donor_cat']== "young_healthy" else bin
            # First kidney is allocated Locally, the second OPO-wide. People are bypassed with 853'd. 
        elif (donor['donor_cat']== "young_dcd" or donor['donor_cat']== "old_dcd" or donor['donor_cat']=="ecd_dcd"):
            bin = bin*10 if bin>3 else bin
            bin = bin + 2 if bin>30 and match['geo']== "OPO" else bin
            bin = bin + 9 if bin==52 and match['geo']== "OPO" and donor['donor_cat']== "young_dcd" else bin
      
    match['c_bin']= bin
    
    
    # Add points
    if donor['donor_cat'] in ('young_healthy','old_healthy','old_dcd','young_dcd'):
        match['c_addtl_pts'] = match['p_hla'] + match['p_pra'] + match['p_ped11'] + match['p_ped35'] + match['p_urgency'] + match['p_pld']  
    else: 
        match['c_addtl_pts'] = 0
        
    match['c_kidney_points']= match['p_wait_wl'] + match['c_addtl_pts']
           
    return match
    
def min_wait_fun(donor,pt,match):
        
    if donor['run_opo_id'] in [ "TXGC-OP1","MWOB-OP1","LAOP-OP1","CADN-OP1"] and match['geo']=="Local":
        lib_wait_fun(donor,pt,match,365*2)
    elif match['geo']=="Local" and (donor['run_opo_id'] == "MAOB-OP1" or  donor['run_opo_id'] ==  "CTOP-OP1"):
          # Initialize   
        needed_points = 0
        needed_tiebreak = 0
        needed_wait_time = 0
        
        threshold= 180 if match['c_age'] <6 else 0
        threshold= 365 if match['c_age'] >=6 and match['c_age'] <11 else threshold
        threshold= 545 if match['c_age'] >=11 else threshold
        
        last_bin = int(donor['last_bin'])
        last_kidney_points=float(donor['last_kidney_points'])
        last_tiebreak = float(donor['last_tie_break'])
        
        ever_offered = last_bin>= match['c_bin'] and int(pt['unos_cand_stat_cd']) in [4010,4050,4060]
        always_offered = ever_offered and (last_bin> match['c_bin'] or last_kidney_points<match['c_addtl_pts'])
        if last_bin == match['c_bin'] and last_kidney_points>=match['c_addtl_pts']:
            needed_points = last_kidney_points-match['c_addtl_pts']
            needed_tiebreak = last_tiebreak
        if needed_points > 8:
            ever_offered = False
        elif needed_points == 8:
            needed_wait_time = max(needed_tiebreak,threshold)
        else:
            if match['c_age'] <18:
                needed_wait_time = threshold*(1.0- (1.0 - (1.0*needed_points/8.0) )**2)**0.5
            else:
                needed_wait_time = threshold * needed_points // 8
            needed_wait_time = needed_wait_time + (needed_wait_time < needed_tiebreak)
            # The last line is a correction a tie in points.     
        
        if ever_offered and needed_wait_time == 0:
            needed_wait_time = -1
        if always_offered:
            needed_wait_time = -1

        match['ever_offered'] = ever_offered
        match['always_offered'] = always_offered
        match['needed_wait_time'] = int(needed_wait_time)
    
    if donor['run_opo_id'] == "PADV-OP1" and False:
        # ALU Sharing Agreement (need to create variables first_bin and first_kidney_points)
        # First Kidney
        donor2 = donor
        donor2['last_bin'] = donor['first_bin']
        donor2['last_kidney_points'] = donor['first_kidney_points']
        match2 = match
        lib_wait_fun(donor2,pt,match2,365)
        # Fix Stuff
        donor3 = donor
        match3 = match
        # Change Bins (Undo bins, how?)
        lib_wait_fun(donor3,pt,match3,365)
        # Assign 
        match['ever_offered'] = max(match2['ever_offered'],match3['ever_offered'])
        match['always_offered'] = max(match2['always_offered'],match3['always_offered'])
        match['needed_wait_time'] = min(match2['needed_wait_time'],match3['needed_wait_time'])
       
        
        
    
    return match    
    
    
def abo_dictionary(pt,type_dict):
    A2_B = set("FLWC-OP1  GALL-OP1  ILIP-OP1  MNOP-OP1  MWOB-OP1  OHLB-OP1  ORUO-IO1  TXSA-OP1  WALC-OP1".split())
    A2_O = set("MWOB-OP1  TXSA-OP1".split())
    A2B_B = set("GALL-OP1  MNOP-OP1  MWOB-OP1  TXSA-OP1  WALC-OP1".split())
    if pt['abo'] == 'B' and pt['listing_ctr_dsa_id'] in A2_B:
        type_dict['A2']="Compatible when Local"
    if pt['abo'] == 'B' and pt['listing_ctr_dsa_id'] in A2B_B:
        type_dict['A2B']="Compatible when Local"
    if pt['abo'] == 'O' and pt['listing_ctr_dsa_id'] in A2_O:   
        type_dict['A2']="Compatible when Local"
    return type_dict
    
def abo_compatibility(donor,pt,match):
    
    if pt['listing_ctr_dsa_id']==donor['run_opo_id']:
        compatibleset = set([])
        if (donor['abo_don'] == 'A2') and pt['abo'] == 'B':
            compatibleset = set("FLWC-OP1  GALL-OP1  ILIP-OP1  MNOP-OP1  MWOB-OP1  OHLB-OP1  ORUO-IO1  TXSA-OP1  WALC-OP1".split())
        elif (donor['abo_don'] == 'A2') and pt['abo'] == 'O':
            compatibleset = set("MWOB-OP1  TXSA-OP1".split())
        elif (donor['abo_don'] == 'A2B') and pt['abo'] == 'B':
            compatibleset = set("GALL-OP1  MNOP-OP1  MWOB-OP1  TXSA-OP1  WALC-OP1".split())
            
        if donor['run_opo_id'] in compatibleset:
                match['c_abo']= "Compatible"
    return match
    

def screening_fun(donor,pt,match):
    amis = match['a_mm']
    bmis = match['b_mm']
    drmis =  match['dr_mm']
    if pt['listing_ctr_dsa_id']=="LAOP-OP1" and (not match['screened']) and pt['cpra']>0.495 and amis+bmis+drmis>2 and (not match['geo']=="Local"):
        match['reason_screen']= match['reason_screen']+'|50'
        match['screened']= match['reason_screen'] > 0  
    return match
    
    
