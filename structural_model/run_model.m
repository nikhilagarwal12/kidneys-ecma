%%% Estimation

%%% Clear workspace, add paths and define target folders;
clearvars; add_paths;

model_names = {'nyrt_fix', 'nyrt_fix_uh','nyrt_fix_time_uh', 'nyrt_fix_time_pt_don_uh'};

%% Tasks
tasks.estimation            = true;
tasks.init_model            = tasks.estimation;
tasks.gen_data              = tasks.estimation;
tasks.gen_gibbs_chain       = tasks.estimation;
tasks.summarize_base_chains = tasks.estimation;
tasks.convergence           = tasks.estimation;
tasks.results               = tasks.estimation;
tasks.avg_ccp               = tasks.estimation;
tasks.gen_redraw_chain      = tasks.estimation;
tasks.ccp_fit               = tasks.estimation;
tasks.value_function        = tasks.estimation;
tasks.average_offer         = tasks.estimation;
tasks.projection_table      = tasks.estimation;
tasks.validate_ccps         = false;  % use run_model_validation.m to run validation exercise
tasks.print_tables          = true;
tasks.print_fitplots        = true;

% Specify Options here (see main_options.m for available options)
options = main_options();

%%% Print Tasks
tasks                                      

%%% Pass to main scripts
% Loop
for ii = 1:length(model_names)
    model_name = model_names{ii};

    % do not run tasks.ccp_fit if model includes patient UH
    if ~isempty(strcmp(model_name,'pt')) & ~isempty(strcmp(model_name,'uh'))
        tasks.ccp_fit = false;
    end

    main
end

% Format tables, plots and output to pdfs
if tasks.print_tables
    fprintf('Tables ')
    [status,cmdout] = system(['R CMD BATCH ', pwd, '/code/functions/make_tables_V_Gamma_Estimates.R']);
end

if tasks.print_fitplots
    multi_spec_plots(options,model_names);
end
