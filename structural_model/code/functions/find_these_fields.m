% Function to find fields in design matrix and populate a new matrix
% Also transforms

function Z = find_these_fields(data,fields)
    
    N = size(data.fixed.obs,1);
    K = length(fields);
    Z = zeros(N,K);
    for k=1:K
        col_index=0; j=0;
        while col_index==0
            j = j+1; if strcmp(fields{k},data.fixed.colheaders{j})==1; col_index = j; end
        end 
        Z(:,k) = data.fixed.sigma_dm(col_index) * data.fixed.obs(:,col_index) + data.fixed.mu_dm(col_index);
    end