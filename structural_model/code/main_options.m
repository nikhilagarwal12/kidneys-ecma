classdef main_options
    properties
        ccp_fit        = struct('first_accept',true,'n_pos',20,'n_draws',500,'thin',10,'burn_in',5000)
                                        % Default Options for ccp_fit
        output_stub    = '/proj/organs/common_output/structural/output/';                % Where we store the outputs
        results_folder = '/proj/organs/common_output/structural/output/results/';        % Where we store the results
        groups         = '/proj/organs/common_output/structural/input/patient_donor_age';% Define subgroups for model fit
        resumeAt       = 0;             % MCMC estimation. Which file to resume (0 if from start, if > 1 file should exist)
        total_files    = 20;            % MCMC estimation. Files to generate
        burn_in        = 100000;        % MCMC estimation. Draws to burn in
        chain_nos      = [1]            % Read MCMC chains to be read
        keep_draws     = false;         % Read MCMC (chain_output): output draws
        diagnostics    = false;         % Read MCMC (chain_output): output objects for diagnostics
        override_rho   = [];            % Value function: Computes the value function with a different rho
        subset_pt      = [];            % Value function: (vector) Only runs the value_function code and the projection for pt_histories (wlreg_audit_idcode) that end in these digits.
        eta_draws      = false;         % Whether or not to calculate/summarize the draws of eta
        edraws         = 40;            % Value function: number of draws of eta
        tdraws         = 40;            % Value function: number of draws of time to the future
        approach='numeric_integration'; % Value function: how to perform the integration
        iterate_over   = 'patients';    % Value function: iterate over offers or patients
        verbose        = 1000;          % Value function: how often do you want to print progress
        rng_seed       = 954637;        % CCPFIT: Randomly generated seed on 11-17-16
        ref_pt_history = 21709962;      % AVERAGE_OFFER: Patient to use as reference: 20925071 or 21709962 -> find(patients.pt_diabetes ==0 & patients.init_age == 53 & patients.init_cpra == 0 & patients.abo == 0 & patients.end_bmi_calc < 27 & patients.end_bmi_calc > 26)
        av_don_local   = true;          % AVERAGE_OFFER: Interpersonal utility comparisons equating utility for the average (local?) donor
        resume_cfs     = false;         % CF: Resumes a cf simulation that has been started
        run_splits     = 1;             % CF: enter split number 1 (whole sample, 2-end subgroups) 
        overwrite_soln = false;         % CF: Are we willing to overwrite a solution?
        profile        = '';            % CF: Name of the folder where the profile results will be stored ('' if no profile)
   end
   
   methods
        
        function options = main_options(structure)
            if nargin==1
                for fname = fieldnames(structure)'
                    options.(char(fname)) = structure.(char(fname));
                end
            end
        end
        
        function nameend = cf_suffix(options,model)  % Different assumptions to run the VF.
            nameend = cf_altname(options,model);
            if numel(nameend)>0
                nameend = ['_' nameend];
            end
        end
        
        function nameend = cf_altname(options,model) 
            nameend = '';
            if ~isempty(options.subset_pt)
                nameend = ['pt_' cat(2,num2str(options.subset_pt))];
            end
            if ~isempty(options.override_rho),
                nameend = [nameend 'rho_' num2str(round(100*exp(-options.override_rho*365)))];
            end
        end
        
        function nameend = vfsavename(options,model,str,ext,results) % generates a name to save in output.
            nameend = cf_suffix(options,model); % Suffix we use in cf.
            if isfield(model,'q_leave') && ischar(model.q_leave)
                nameend = [nameend '_' model.q_leave];
            end
            if nargin<3, str='value_function'; end
            if nargin<4, ext= 'mat'; end
            if nargin<5, results = 0; end
            if results,
                fileroot = [options.results_folder model.model_name];
            else 
                fileroot = [options.output_stub model.model_name];
            end
            nameend = [fileroot '/' str nameend '.' ext];
        end
        
        function nameend = results_savename(options,model,str,ext)  % Generate a name to save in /results
            nameend = vfsavename(options,model,str,ext,1);
        end
        
        
        function pt = pt_include(options,data)  % Include only a fraction of patients.
            if ~isempty(options.subset_pt)
                k = ceil(log10(max(options.subset_pt)+1));  % Number of dignits options.subset has
                pt = ismember(mod(data.pt_history_id,10^k),options.subset_pt); % Pick a few pt.
            else
                pt = true(size(data.pt_history_id,1),1);
            end
        end
        
        
        function bool = isfield(obj,str)  % Overload isfield for structure
            bool = isprop(obj,str);
        end
        
   
   end
   
end


