// Code to impute missing DR antigens when missing for the donor
// Uses a scoring rule based on DR mismatches of offers and patient unacceptable antigens
// DO NOT run on own; it should be called within gen_primary_files.do

set more off

local dr_antigen_list = "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 103 1403 1404"


// Import HLA mismatch equivalences
import delimited using ../../mechanism/input/split_equiv_pre.csv, clear
keep if pat_dr != .
save `primary_files'auxiliary_files/split_equiv_pre, replace


// First, get DR antigen frequencies among donor population
use `primary_files'deceased_donors, clear
keep donor_id ddr1 ddr2
foreach x in `dr_antigen_list'{
    gen freq_`x' = ddr1==`x' | ddr2==`x'
}
collapse (mean) freq_*
gen joinvar = 1  // to do a "joinby" with the donor antigen scores later
save `primary_files'auxiliary_files/dr_antigen_frequencies, replace


// Second, get offers from donors without DR antigen records. Merge in offer data (including dr mismatches), patient DR antibodies and unacceptables, and equivalences
  
// donors with missing antigens
use `primary_files'deceased_donors, clear
keep if ddr1==.
keep donor_id ddr1 ddr2

// offers from those donors
merge 1:m donor_id using `primary_files'offers, keep(match) keepusing(wl_id_code wlreg_audit_id_code organ_sequence_num refusal_id drmis) nogen

// unacceptable antigens for patients who received offers
merge m:1 wlreg_audit_id_code using `primary_files'patient_history, assert(match using) keep(match) keepusing(unacc_antigens_DR) nogen
replace unacc_antigens_DR = "|" + unacc_antigens_DR + "|"  // ensure each antigen number is preceded and followed by a bar (for parsing)
sort donor_id

// antibodies of the patients
merge m:1 wl_id_code using `primary_files'patients, assert(match using) keep(match) keepusing(dr1 dr2) nogen
replace dr1 = dr2 if dr1==0 & dr2 != 0
replace dr2 = dr1 if dr2==0  // homozygous patients
  
// Merge equivalent antigens
rename dr1 pat_dr
merge m:1 pat_dr using ../../mechanism/input/split_dr_equiv_pre, keep(master match) keepusing(don_dr) nogen
replace don_dr = "," + don_dr + ","
rename don_dr equiv_dr1
rename pat_dr dr1

rename dr2 pat_dr
merge m:1 pat_dr using ../../mechanism/input/split_dr_equiv_pre, keep(master match) keepusing(don_dr) nogen
replace don_dr = "," + don_dr + ","
rename don_dr equiv_dr2
rename pat_dr dr2


// Score antigens based on contradictions with offer data. Dock one point for each of the following:
    // In a 0 DR-mm offer, the antigen is not equivalent to one of the patient's DR antigens
    // In a 2 DR-mm offer, the antigen is equivalent to one of the patient's DR antigens
    // The antigen was unacceptable to the patient

foreach x in `dr_antigen_list'{

    display `x'
    gen score_`x' = 0

    // 0 DR-mm offers
    replace score_`x' = score_`x' - 1 if drmis==0 & ~(strpos(equiv_dr1,",`x',")>0 | strpos(equiv_dr2,",`x',")>0)

    // 2 DR-mm offers
    replace score_`x' = score_`x' - 1 if drmis==2 & (strpos(equiv_dr1,",`x',")>0 | strpos(equiv_dr2,",`x',")>0)

    // Unacceptable antigens
    replace score_`x' = score_`x' - 1 if strpos(unacc_antigens_DR,"|`x'|") > 0

    // sum score by donor
    bys donor_id: egen tot_score_`x' = total(score_`x')
}

keep donor_id tot_score_*
duplicates drop
isid donor_id
gen joinvar = 1

// to break ties, use antigen frequencies among donor population where DR antigens are present
joinby joinvar using `primary_files'auxiliary_files/dr_antigen_frequencies
foreach x in `dr_antigen_list'{
    replace tot_score_`x' = tot_score_`x' + freq_`x'
}
drop freq_* joinvar


// turn dataset into a long one (one row per donor X antigen)
reshape long tot_score_, i(donor_id) j(dr)
gsort donor_id -tot_score_
by donor_id: keep if _n <= 2   // keep top 2 scoring antigens, since that rule works the best on test data
by donor_id: gen ddr1 = dr[1]
by donor_id: gen ddr2 = dr[2]
replace ddr2 = 98 if ddr2==.|ddr2==ddr1  // coding homozygous donors

save `primary_files'auxiliary_files/imputed_dr_antigens, replace

// merge back with deceased_donors file and save
keep donor_id ddr1 ddr2
duplicates drop
isid donor_id
rename ddr1 ddr1_imputed
rename ddr2 ddr2_imputed
merge 1:1 donor_id using `primary_files'deceased_donors, assert(match using) nogen
replace ddr1 = ddr1_imputed if ddr1==. & ddr1_imputed != .
replace ddr2 = ddr2_imputed if ddr2==. & ddr2_imputed != .
drop ddr1_imputed ddr2_imputed
replace ddr1 = 0 if ddr1==.
replace ddr2 = 98 if ddr2==.

save `primary_files'deceased_donors, replace
