// Creates a subsample of primary files for the NYRT sample
// Sample definition: A patient belongs to this sample if:
// 		i. She is registered at NYRT
// 		ii. Was registered prior to Dec 4, 2014
// DO NOT run on own; it should be called within gen_primary_files.do

* Pull the sample of patients in NYRT prior to 12/04/2014
use wl_id_code listing_ctr_dsa* init_date end_date using "`primary_files'/patients.dta", clear
keep if listing_ctr_dsa_id == "NYRT-OP1"
keep if init_date <= `nyrt_end_date' & end_date >= `nyrt_start_date'  // NYRT sample may have a different sample period
keep wl_id_code

tempfile wl_id_code_list
isid wl_id_code
save `wl_id_code_list', replace


* Determine which patients were active during the NYRT sample period (since some didn't receive offers)
use "`primary_files'patient_history", clear
merge m:1 wl_id_code using `wl_id_code_list', assert(master match) keep(match) nogen
sort wl_id_code chg_date chg_time
by wl_id_code: gen wl_mark = _n==1

gen record_in_sample = chg_date <= `nyrt_end_date' & chg_till >= `nyrt_start_date'
gen active_in_sample = active * record_in_sample
by wl_id_code: egen ever_active_in_sample = max(active_in_sample)  // marks whether patient was ever active (could have received offers)
tab ever_active_in_sample if wl_mark
keep if wl_mark==1
keep wl_id_code ever_active_in_sample
isid wl_id_code

* Merge with offers to create the subset of patients that didn't receive a transplant from a "special" donor
merge 1:m wl_id_code using "`primary_files'/offers", generate(_nyrt_patient)
label define _nyrt_patient 1 "1 NYRT Patient, no offers" 2 "2 Not NYRT Patient" 3 "3 NYRT Patient in offers"
label values _nyrt_patient _nyrt_patient

drop if _nyrt_patient==1 & ever_active_in_sample==0   // omit patients that were never active and never received an offer
tab _nyrt_patient ever_active_in_sample
drop ever_active_in_sample

* Tag offers made after 12/04/2014
gen _tag_post_12042014 = match_date>=date("12/04/2014","MDY") & match_date ~= .
drop if _tag_post_12042014 == 1


* Log the coverage
drop _tag*


* Keep a subset of variables
keep donor_id match_id_code match_date c_accept organ_sequence_num ptr_sequence_num pb_offer_seq _nyrt_patient pt_code wl_id_code
drop if match_date < `nyrt_start_date' | (match_date > `nyrt_end_date' & match_date ~= .)


/* Save as a tempfile */
tempfile offers_and_nyrt_patients
save `offers_and_nyrt_patients', replace
    
/* Save the sample_offer file */
* offers
use `offers_and_nyrt_patients', clear
keep if _nyrt_patient==3

// de-duplicate offers at the pt_code level
gen said_yes = c_accept == "Yes"
gsort donor_id pt_code -said_yes organ_sequence_num
by donor_id pt_code: keep if _n==1
sort donor_id match_id_code ptr_sequence_num pb_offer_seq

isid donor_id match_id_code ptr_sequence_num pb_offer_seq, m
keep donor_id match_id_code ptr_sequence_num pb_offer_seq
save "`file_dir'/sample_offers", replace

/* Save lists of donor_ids and wl_id_codes */
use wl_id_code _nyrt_patient using `offers_and_nyrt_patients', clear
keep if _nyrt_patient~=2
keep wl_id_code
duplicates drop
drop if wl_id_code == .
isid wl_id_code
save "`file_dir'/sample_wl_id_codes", replace

// Need to keep all donors because a 0-mm donor could be offered to a NYRT patient
// even if no NYRT patient got an offer in the sample (e.g. a patient who entered the
// waitlist afterwards).
use donor_id using `offers_and_nyrt_patients', clear
duplicates drop
drop if donor_id == .
isid donor_id
save "`file_dir'/sample_donor_ids", replace
