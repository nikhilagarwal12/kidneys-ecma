* Script to look at whether cutoffs "bunch" over time
* Prepares dataset for Table E.8: Tests for Rank Autocorrelation in Cutoff Scores
* This script has to be run before autocorrelation_test.m

clear
set more off

* ------------------------------
* Data location
* ------------------------------
local data = "~/organs-star/primary_files/"
local output = "~/organs-star/processed_files/summary_statistics/nyrt/"

* ------------------------------
* Data setup
* ------------------------------
* First, use offers.dta to determine which donors were discarded
* baseline assumption is that any discarded donor would have been offered to any compatible patient
use `data'offers_encoded, clear
bys donor_id match_run_num (organ_sequence_num): gen last_offer = _n==_N
keep if last_offer
* small number of bypass last offers. Assuming that's an error and organ was placed
gen discarded = offer_accept == 0  
keep donor_id discarded match_run_num run_opo_id donor_opo match_opo opo_dsa_id listing_ctr_dsa_id
isid donor_id match_run_num
tempfile last_accept
save `last_accept', replace

* Try taking discards directly from deceased_donors_w_vars, see if lines up
use "`data'nyrt/deceased_donors", clear
keep donor_id num_ki_disc
isid donor_id
merge 1:m donor_id using `last_accept', assert(match using) keep(match) nogen

drop opo_dsa_id run_opo_id
save `last_accept', replace

* de-duplicate mech_donor_extract, save tempfile
use "`data'nyrt/mech_donor_extract", clear
gen last_bin_standard = last_bin
* re-code last bin in case of variances (so can compare across DSAs) and round points
replace last_bin_standard = floor(last_bin/10) if last_bin > 51   
gen last_kidney_points_floor = floor(last_kidney_points)
gsort donor_id last_bin -last_kidney_points -last_tie_break
by donor_id: keep if _n==_N
drop last_bin last_kidney_points
* replace bin/points with standardized (non-variance, rounded) versions
rename last_bin_standard last_bin  
rename last_kidney_points_floor last_kidney_points
isid donor_id
codebook donor_id
tempfile mech_donors
save `mech_donors', replace


* Take cutoffs from mechanism, combine with discard to determine cutoff for being offered a kidney from each donor
use `last_accept', clear
merge 1:1 donor_id match_run_num using `mech_donors', assert(master match) keep(match) nogen

    * code discarded donors as one common cutoff. For each category, last_bin = max(last_bin) + 1, last points and tiebreak = 0 
    tab donor_cat discarded
    tab donor_cat discarded if opo_dsa_id == "NYRT-OP1"
    replace last_kidney_points = 0 if discarded==1
    replace last_tie_break = 0 if discarded==1
    replace last_bin = 51 if discarded==1 & donor_cat == "young_healthy"
    replace last_bin = 45 if discarded==1 & donor_cat == "old_healthy"
    replace last_bin = 15 if discarded==1 & donor_cat == "young_dcd"
    replace last_bin = 9 if discarded==1 & donor_cat == "old_dcd"
    replace last_bin = 44 if discarded==1 & donor_cat == "ecd"
    replace last_bin = 8 if discarded==1 & donor_cat == "ecd_dcd"

    * classify whether donor is from NYRT, and whether SCD
    * donor originated in NYRT    
    gen nyrt = opo_dsa_id == "NYRT-OP1"                                                  
    gen scd = donor_cat == "young_healthy" | donor_cat == "old_healthy"
    
    * sort cutoffs within each donor category, break ties with random numbers, compute rank statistics
    set seed 245827365
    gen randnum = runiform()

    gsort donor_cat nyrt last_bin -last_kidney_points -last_tie_break randnum
    * rank within donor category and NYRT/non-NYRT group
    by donor_cat nyrt: gen ranknum = invnormal((_n-0.5)/_N)    

    sort match_date donor_id
    gen date = _n

    * Create a dataset just containing numeric variables representing everything
    gen abo_A = abo_don == "A" | abo_don == "A1" | abo_don == "A2"
    gen abo_B = abo_don == "B"
    gen abo_AB = abo_don == "AB" | abo_don == "A1B" | abo_don == "A2B"
    gen abo_O = abo_don == "O"
    gen abo = (abo_A==1) + 2*(abo_AB==1) + 3*(abo_B==1) + 4*(abo_O==1)
    gen cat = (donor_cat=="young_healthy") + 2*(donor_cat=="old_healthy") + 3*(donor_cat=="young_dcd") + 4*(donor_cat=="old_dcd") + 5*(donor_cat=="ecd") + 6*(donor_cat=="ecd_dcd")
    keep donor_id match_date nyrt abo scd age_don kdpi cat last_bin last_kidney_points last_tie_break
    gen year = year(match_date)
    format match_date %12.0g

    save `output'donors_for_rank_test, replace
    outsheet using `output'donors_for_rank_test.txt, delimiter("|") noquote nolabel replace

