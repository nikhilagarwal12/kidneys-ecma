// Subroutines the section where we substitute for wlreg_audit_ids in the offer data that aren't matched back to the wlhistory file
// Meant to be called by the "offers" section of gen_primary_files.do, and other scripts with offer and wlhistory data
// Requires relevant offer dataset to be in memory

    // preliminary: construct string of "true" variables
    local true_var_str = ""
    foreach var in `vars_to_replace'{
        local true_var_str = "`true_var_str' true_`var'"
    }

    // create a set of "true" wlreg_audit_ids, match back to offer data
    preserve
    keep if _merge == 1  // wlreg_audit_id's we didn't find in wlhistory
    codebook wl_id_code  // how many offers (and patients) have unmatched wlreg_audit_ids
    codebook wlreg_audit_id_code
    keep wl_id_code match_date donor_id organ_sequence_num
    duplicates drop

    tempfile temp1001
    save `temp1001'  // this is the set of missing wlreg_audit_ids

    // For a given patient (wl_id), take the wlreg_audit_id from patient history that we THINK should have applied
    use "`primary_files'patient_history", clear
    keep wl_id_code chg_date chg_time chg_ty `vars_to_replace'

    joinby wl_id_code using `temp1001'
    keep wl_id_code chg_date chg_time match_date chg_ty donor_id organ_sequence_num `vars_to_replace'
    keep if chg_date <= match_date
    sort donor_id organ_sequence_num chg_date chg_time
    by donor_id organ_sequence_num: keep if _n == _N  // selects most recent update before match_date, takes that wlreg_audit_id as the true one

    codebook wlreg_audit_id_code
    assert ~missing(wlreg_audit_id_code)

    foreach var in `vars_to_replace'{
      rename `var' true_`var'
    }
    keep donor_id organ_sequence_num true_*
    sort donor_id organ_sequence_num

    save "`auxiliary_files'true_wlreg_audit_ids.dta", replace

    restore

    // merge offer dataset back with "fixed" wlreg_audit_ids

    cap drop _merge

    merge 1:1 donor_id organ_sequence_num using "`auxiliary_files'true_wlreg_audit_ids.dta", keepusing(`true_var_str')

    // replace wlhistory variables for offers where the wlreg_audit_id was replaced
    foreach var in `vars_to_replace'{
      replace `var' = true_`var' if _merge == 3
      drop true_`var'
    }
    drop _merge

