#include "common.h"

#include "mex.h"

#include <math.h>
#include <string.h>

// A bunch of static variables to remove reallocation of arrays
static int notInitialized = 1;
static long *iIndex, *jIndex;
static double *value;
static double *expTerm, *Fz, *eMax, *feas, *qFeas, *qfz, *pExpTerm;
static double *aK, *pExpTermLh;

void cleanup(void) {
  mexPrintf("MEX-file is terminating, destroying allocations in analytic second Jacobian\n");
  free(iIndex);
  free(jIndex);
  free(value);
  free(expTerm);
  free(pExpTerm);
  free(pExpTermLh);
  free(qfz);
  free(Fz);
  free(feas);
  free(qFeas);
  free(eMax);
  free(aK);
  notInitialized = 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  double *mu = (double*)mxGetPr(mxGetField(prhs[1], 0, "mu"));
  double *sigma = (double*)mxGetPr(mxGetField(prhs[1], 0, "sigma"));
  double *sigmaInvA = (double*)mxGetPr(mxGetField(prhs[1], 0, "sigma_inverse"));
  double *deltaBy_h = (double*)mxGetPr(mxGetField(prhs[1], 0, "delta_x_h"));
  double *endowment = (double*)mxGetPr(mxGetField(prhs[1], 0, "endowment"));
  double *lambda_hOverK = (double*)mxGetPr(mxGetField(prhs[1], 0, "lambdah_over_K"));
  double *discountW = (double*)mxGetPr(mxGetField(prhs[1], 0, "discount_w"));
  double *discountV2 = (double*)mxGetPr(mxGetField(prhs[1], 0, "discount_V2"));
  double *arrivals = (double*)mxGetPr(mxGetField(prhs[1], 0, "arrivals"));
  double *cArrayRepeat = (double*)mxGetPr(mxGetField(prhs[1], 0, "pr_compat_repeat"));
  double *timeOutProb =  (double*)mxGetPr(mxGetField(prhs[1], 0, "timeout_prob"));
  double *h = (double*)mxGetPr(mxGetField(prhs[1], 0, "h"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[1], 0, "mu"));
  const unsigned long nPatients = sizeArray[0];
  const unsigned long nTime = sizeArray[1];
  const unsigned long nDonors = sizeArray[2];
  const double nDonorsInv = (1.0/(double)nDonors);
  
  const unsigned long nQ = nPatients*nTime*nDonors;
  const unsigned long nP = nPatients*nTime;
  const unsigned long nTm1 = nTime-1l;
  const unsigned long nTm1nP = nP-nPatients;

  const double *q = (double*)mxGetPr(prhs[0]);
  const double *p = q+nQ;
  const double *v = p+nP;

  if (notInitialized) {
    unsigned long nnz;
    if (nQ > (5l*nP-nPatients*(nDonors+4l))) {
      nnz = nQ*3l;
    } else {
      nnz = nTm1nP*(nDonors+4l)+nQ+nP;
    }
    iIndex = (long*)malloc(nnz*sizeof(long));
    jIndex = (long*)malloc(nnz*sizeof(long));
    value = (double*)malloc(nnz*sizeof(double));
    expTerm = (double*)malloc(nTm1nP*sizeof(double));
    pExpTerm = (double*)malloc(nTm1nP*sizeof(double));
    pExpTermLh = (double*)malloc(nTm1nP*sizeof(double));
    Fz = (double*)malloc(nQ*sizeof(double));
    feas = (double*)malloc(nQ*sizeof(double));
    qFeas = (double*)malloc(nQ*sizeof(double));
    qfz = (double*)malloc(nQ*sizeof(double));
    eMax = (double*)malloc(nQ*sizeof(double));
    aK = (double*)malloc(nP*sizeof(double));
    mexAtExit(cleanup);
    notInitialized = 0;
  }
  memset(expTerm, 0, nTm1nP*sizeof(double));

  // Precompute common sub expressions
  for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
    const double timeOutProbLocal = timeOutProb[j];
    for (unsigned long i = 0l; i < nP; ++i, ++index) {
      const double sigmaInv = sigmaInvA[i];
      const double zLocal = (mu[index]-v[i])*sigmaInv;
      const double fzLocal = normPdf(zLocal)*cArrayRepeat[index];
      Fz[index] = normCdf(zLocal)*cArrayRepeat[index];
      feas[index] = h[i]*(Fz[index]+timeOutProbLocal);
      qFeas[index] = feas[index]*q[index];
      eMax[index] = sigma[i]*(Fz[index]*zLocal+fzLocal);
      qfz[index] = q[index]*fzLocal*sigmaInv;
    }
  }
  for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
    for (unsigned long i = 0l; i < nTm1nP; ++i, ++index) {
      expTerm[i] += q[index]*Fz[index];
    }
    index += nPatients;
  }
  for (unsigned long i = 0l; i < nP; ++i) {
    aK[i] = discountW[i]*nDonorsInv; 
  } 
  for (unsigned long i = 0l; i < nTm1nP; ++i) {
    expTerm[i] = exp(-deltaBy_h[i]-lambda_hOverK[i]*expTerm[i]);
    pExpTerm[i] = p[i]*expTerm[i];
    pExpTermLh[i] = pExpTerm[i]*lambda_hOverK[i];
  } 
  // matrix c
  plhs[0] = mxCreateDoubleMatrix(nDonors, 1, mxREAL);
  double *c = (double*)mxGetPr(plhs[0]);
  /* Contribution from feasibility */
  for (unsigned long i = 0l, index = 0l; i < nDonors; ++i) {
    double sum = 0.0;
    for (unsigned long j = 0l; j < nP; ++j, ++index) {
      sum += p[j]*qFeas[index];
    }
    c[i] = sum - endowment[i];
  }
  // matrix ceq
  plhs[1] = mxCreateDoubleMatrix(nP+nTm1nP, 1, mxREAL);
  double *ceq = (double*)mxGetPr(plhs[1]);
  /* Contribution from steady state */
  for (unsigned long j = 0l, index = 0l, index1 = nPatients; j < nTm1; ++j) {
    for (unsigned long i = 0l; i < nPatients; ++i, ++index, ++index1) {
      ceq[index] = p[index1]-arrivals[index1]-pExpTerm[index];
    }
  }
  ceq = ceq + nTm1nP;
  /* Contribution from value */
  for (unsigned long j = 0l, index = 0l; j < nTm1; ++j) {
    for (unsigned long i = 0l; i < nPatients; ++i, ++index) {
      double sumW = 0.0;
      for (unsigned long k = 0l, index1 = index; k < nDonors; ++k, index1 += nP) {
        sumW += q[index1]*eMax[index1];
      }
      ceq[index] = discountV2[index]*v[index]-aK[index]*sumW-v[index+nPatients];
    }
  }
  for (unsigned long i = nTm1nP; i < nP; ++i) {
    double sumW = 0.0;
    for (unsigned long k = 0l, index3 = i; k < nDonors; ++k, index3 += nP) {
      sumW += q[index3]*eMax[index3];
    }
    ceq[i] = discountV2[i]*v[i]-aK[i]*sumW;
  }
  if (nlhs > 2) {
    // matrix gc
    // First create a sparse matrix in COO format and convert to matlab format
    unsigned long int currentIndex = 0l;
    for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
      for (unsigned long i = 0l; i < nP; ++i, ++index) {
        const double localValue = p[i]*feas[index];
        if (localValue != 0.0) {
          iIndex[currentIndex] = index;
          jIndex[currentIndex] = j;
          value[currentIndex++] = localValue;
        }
      }
    }
    for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
      for (unsigned long i = 0l; i < nP; ++i, ++index) {
        const double localValue = qFeas[index];
        if (localValue != 0.0) {
          iIndex[currentIndex] = i+nQ;
          jIndex[currentIndex] = j;
          value[currentIndex++] = localValue;
        }
      }
    }
    const unsigned long offset = nQ + nP;
    for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
      for (unsigned long i = 0l; i < nP; ++i, ++index) {
        const double localValue = -p[i]*h[i]*qfz[index];
        if (localValue != 0.0) {
          iIndex[currentIndex] = i+offset;
          jIndex[currentIndex] = j;
          value[currentIndex++] = localValue;
        }
      }
    }
    plhs[2] = mxCreateSparse(nP*(nDonors+2l), nDonors, currentIndex, mxREAL);
    double *sr  = (double*)mxGetPr(plhs[2]);
    size_t* irs = (size_t*)mxGetIr(plhs[2]);
    size_t* jcs = (size_t*)mxGetJc(plhs[2]);
    COO_toCSC(nDonors, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
    // matrix gceq
    currentIndex = 0l;
    // Part from steady state composition
    for (unsigned long j = 0l, index = 0l; j < nDonors; ++j, index += nPatients) {
      for (unsigned long i = 0l; i < nTm1nP; ++i, ++index) {
        const double localValue = pExpTermLh[i]*Fz[index];
        if (localValue != 0.0) {
          iIndex[currentIndex] = index;
          jIndex[currentIndex] = i;
          value[currentIndex++] = localValue;
        }
      }
    }
    for (unsigned long j = 0l; j < nTm1nP; ++j) {
      if (expTerm[j] != 0.0) {
        iIndex[currentIndex] = j+nQ;
        jIndex[currentIndex] = j;
        value[currentIndex++] = -expTerm[j];
      }
    }
    const unsigned long offset1 = nQ + nPatients;
    for (unsigned long j = 0l; j < nTm1nP; ++j) {
      iIndex[currentIndex] = j+offset1;
      jIndex[currentIndex] = j;
      value[currentIndex++] = 1.0;
    }
    for (unsigned long j = 0l; j < nTm1nP; ++j) {
      double sumQfz = 0.0;
      for (unsigned long i = 0l, index = j; i < nDonors; ++i, index += nP) {
        sumQfz += qfz[index];
      }
      double localValue = pExpTermLh[j]*sumQfz;
      if (localValue != 0.0) {
        iIndex[currentIndex] = j+offset;
        jIndex[currentIndex] = j;
        value[currentIndex++] = -localValue;
      }
    }
    /* Value function */
    for (unsigned long i = 0l, index = 0l; i < nDonors; ++i) {
      for (unsigned long j = 0l; j < nP; ++j, ++index) {
        const double localValue = -eMax[index]*aK[j];
        if (localValue != 0.0) {
          iIndex[currentIndex] = index;
          jIndex[currentIndex] = j + nTm1nP;
          value[currentIndex++] = localValue;
        }
      }
    }
    for (unsigned long i = 0l; i < nP; ++i) {
      double sumQ_Fz = 0.0;
      for (unsigned long j = 0l, index = i; j < nDonors; ++j, index += nP) {
        sumQ_Fz += q[index]*Fz[index];
      }
      sumQ_Fz = sumQ_Fz*aK[i] + discountV2[i];
      if (sumQ_Fz != 0.0) {
        iIndex[currentIndex] = i+offset;
        jIndex[currentIndex] = i + nTm1nP;
        value[currentIndex++] = sumQ_Fz;
      }
    }
    const unsigned long offset2 = offset + nPatients;
    for (unsigned long i = 0l; i < nTm1nP; ++i) {
      iIndex[currentIndex] = i+offset2;
      jIndex[currentIndex] = i + nTm1nP;
      value[currentIndex++] = -1.0;
    }

    plhs[3] = mxCreateSparse(nP*(nDonors+2l), nP+nTm1nP, currentIndex, mxREAL);
    double *sr1  = (double*)mxGetPr(plhs[3]);
    size_t* irs1 = (size_t*)mxGetIr(plhs[3]);
    size_t* jcs1 = (size_t*)mxGetJc(plhs[3]);
    COO_toCSC(nP+nTm1nP, currentIndex, iIndex, jIndex, value, jcs1, irs1, sr1);
  }
}
