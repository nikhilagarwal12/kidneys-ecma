// Generate KDRI given donor characteristics

program compute_kdri

args pr_hypertens pr_diabetes

  #delimit ;
  gen kdri = exp(0.0128*(age_don-40)
               - 0.0194*(age_don-18)*(age_don<18)
               + 0.0107*(age_don-50)*(age_don>50)
               - 0.0464*(hgt_cm_don_calc-170)/10
               - 0.0199*(wgt_kg_don_calc<80)*(wgt_kg_don_calc-80)/5
               + 0.1790*(ethcat==2)
               + 0.1260*(hist_hypertens_don=="Y") + 0.1260*`pr_hypertens'*(hist_hypertens_don=="U")  // case where hypertensive status unknown
               + 0.1300*(hist_diabetes_don>1 & hist_diabetes_don<6)  + 0.1300*`pr_diabetes'*(hist_diabetes_don==998) // case where diabetic status unknown
               + 0.0881*(cod_cad_don==2)
               + 0.2200*(min(creat_don,8)-1)
               - 0.2090*(min(creat_don,8)-1.5)*(creat_don>1.5)
               + 0.2400*(hep_c_anti_don=="P")
               + 0.1330*(non_hrt_don=="Y"))
        if ~missing(age_don) & ~missing(hgt_cm_don_calc) & ~missing(wgt_kg_don_calc) & hist_hypertens_don~=""
        &  ~missing(hist_diabetes_don) & hist_diabetes_don~=998 & ~missing(cod_cad_don) & cod_cad_don~=999
        & ~missing(creat_don) & non_hrt_don~=""; // Hep C is taken as negative if not known
  #delimit cr

end
