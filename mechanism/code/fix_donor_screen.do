// Fixes a few issues with the screening criteria. These issues were found comparing the
// results of our simulation of the mechanism and the actual run.
// This file produces: ~/organs-star/processed_files/fix_donor_screen.dta
// which will be used in simulate_offers.do

// The idea is to look at the screening criteria of the patients we observe in the data to
// infer some unobserved donor characteristic that may be triggering screens or some
// other data collection problem in donor characteristics. 
// In practice, I look at each donor_id match_id_code and find the min and max of each 
// screening criteria among observed offers. For example, 
// by donor_id match_id_code: egen cd_peak_amylase = min(doncrit_max_peak_amylase)
// cd_peak_amylase is the minimum "maximum peak amylase" among all observed offers.
// I can infer that peak_amylase is less than that cd_peak_amylase because it was offered
// to someone with  doncrit_max_peak_amylase==cd_peak_amylase.

local offers_file = "~/organs-star/primary_files/offers"
local history_file = "~/organs-star/primary_files/patient_history"



//  Load offers & merge it with patient history
use donor_id wlreg_audit_id_code match_id_code match_run_num geo_t zero_abdr_t if zero_abdr_t ~="Zero" using `offers_file',clear
merge m:1 wlreg_audit_id_code using `history_file', keep(master match)

//  Split offers in ZeroMM, NonLocal, Local 
gen non_local_non_zero = ~(geo_t =="OPO" | geo_t =="Local" | zero_abdr_t =="Zero")
bysort donor_id match_id_code: egen non_local_off = max(non_local_non_zero)

replace non_local_non_zero = 2 if (geo_t =="OPO" | geo_t =="Local" )
label define typeoff 0 ZeroMM 1 NonLocal 2 Local
label values non_local_non_zero typeoff 
drop if non_local_non_zero==0
bysort donor_id match_id_code (non_local_non_zero): gen num_offers = _N
bysort donor_id match_id_code (non_local_non_zero): gen offer_num = _n

// By donor match_id_code find the min and max of each criteria among observed offers.
gen relevant = on_iexpand_donor if non_local_non_zero==1
replace relevant =  on_expand_donor if non_local_non_zero==2
by donor_id match_id_code: egen cd_ecd = min(relevant)
drop relevant
// on_expand_donor double  %1.0g                 Will accept Local Expanded Criteria Donor Kidney(s)

// max_expanded_~a double  %4.0g                 Maximum Number for local ECD of Acceptable A Mismatches
// max_expanded~ab double  %4.0g                 Maximum Number for local ECD of Acceptable AB Mismatches
// max_expand~abdr double  %4.0g                 Maximum Number for local ECD of Acceptable ABDR Mismatches
// max_expande~adr double  %4.0g                 Maximum Number for local ECD of Acceptable ADR Mismatches
// max_expanded~_b double  %4.0g                 Maximum Number for local ECD of Acceptable B Mismatches
// max_expand~_bdr double  %4.0g                 Maximum Number for local ECD of Acceptable BDR Mismatches
// max_expande~_dr double  %4.0g                 Maximum Number for local ECD of Acceptable DR Mismatches
// on_expand_donor double  %1.0g                 Will accept Local Expanded Criteria Donor Kidney(s)

// max_iexpanded~a double  %4.0g                 Maximum Number for imported ECD of Acceptable A Mismatches
// max_iexpande~ab double  %4.0g                 Maximum Number for imported ECD of Acceptable AB Mismatches
// max_iexpan~abdr double  %4.0g                 Maximum Number for imported ECD of Acceptable ABDR Mismatches
// max_iexpand~adr double  %4.0g                 Maximum Number for imported ECD of Acceptable ADR Mismatches
// max_iexpande~_b double  %4.0g                 Maximum Number for imported ECD of Acceptable B Mismatches
// max_iexpan~_bdr double  %4.0g                 Maximum Number for imported ECD of Acceptable BDR Mismatches
// max_iexpand~_dr double  %4.0g                 Maximum Number for imported ECD of Acceptable DR Mismatches
// on_iexpand_do~r double  %1.0g                 Will accept Imported Expanded Criteria Donor Kidney(s)

//doncrit_min_age double  %6.0g                 Minimal acceptable donor age(months) - local
//doncrit_min_age_import double  %6.0g          Minimal acceptable donor age(months) - import
gen relevant =  doncrit_min_age_import if non_local_non_zero==1
replace relevant =  doncrit_min_age if non_local_non_zero==2
by donor_id match_id_code: egen cd_age_months_min = max(relevant)
drop relevant

//doncrit_max_age double  %6.0g                 Maximal acceptable donor age(months) - local
//doncrit_max_age_import double  %6.0g          Maximal acceptable donor age(months) - import
gen relevant =  doncrit_max_age_import if non_local_non_zero==1
replace relevant =  doncrit_max_age if non_local_non_zero==2
by donor_id match_id_code: egen cd_age_months_max = min(relevant)
drop relevant

//doncrit_min_wgt double  %9.0g                 Minimum acceptable donor weight
//doncrit_max_wgt double  %9.0g                 Maximum acceptable donor weight
//doncrit_max_bmi double  %9.0g                 Maximum acceptable donor BMI
by donor_id match_id_code: egen cd_weight_min = max(doncrit_min_wgt)
by donor_id match_id_code: egen cd_weight_max = min(doncrit_max_wgt)
by donor_id match_id_code: egen cd_bmi_max = min(doncrit_max_bmi)

//doncrit_acpt_dcd str2    %2s                   Accept DCD donor - Local
//doncrit_acpt_dcd_import str2    %2s                   Accept DCD donor - Import
gen relevant = doncrit_acpt_dcd_import=="Y" if non_local_non_zero==1
replace relevant =  doncrit_acpt_dcd=="Y" if non_local_non_zero==2
by donor_id match_id_code: egen cd_dcd = min(relevant)
drop relevant

//doncrit_acpt_hist_diabetes str2    %2s                   History of Diabetes
//doncrit_acpt_hist_hypertension str2    %2s               History of Hypertension
//doncrit_acpt_hbcorepos str2    %2s                   	   Accept a Hepatitis B Core antibody positive donor
//doncrit_acpt_hcvpos str2    %2s                          Accept an HCV antibody positive donor
//doncrit_acpt_htlv_pos str2    %2s                        Accept an HTLV I/II antibody positive donor
by donor_id match_id_code: egen cd_don_diabetes = min(doncrit_acpt_hist_diabetes=="Y")
by donor_id match_id_code: egen cd_don_hypertension = min(doncrit_acpt_hist_hypertension=="Y")
by donor_id match_id_code: egen cd_don_hbcorepos = min(doncrit_acpt_hbcorepos=="Y")
by donor_id match_id_code: egen cd_don_hcvpos = min(doncrit_acpt_hcvpos=="Y")
by donor_id match_id_code: egen cd_don_htlv_pos = min(doncrit_acpt_htlv_pos=="Y")

//doncrit_max_warm_time double  %6.0g                      Max Acceptable Warm Ischemic Time (mins)
//doncrit_max_cold_time double  %6.0g                      Maximum acceptable cold ischemic time
by donor_id match_id_code: egen cd_warm_time = min(doncrit_max_warm_time)
by donor_id match_id_code: egen cd_cold_time = min(doncrit_max_cold_time)

//doncrit_max_pct_scler_lt10gl double  %6.0g                Max Accept % Glomeruli < 10 Observed - Local
//doncrit_max_pct_scler_lt10gl_imp double  %6.0g            Max Accept % Glomeruli < 10 Observed - Import
gen relevant =  doncrit_max_pct_scler_lt10gl_imp if non_local_non_zero==1
replace relevant =  doncrit_max_pct_scler_lt10gl if non_local_non_zero==2
by donor_id match_id_code: egen cd_glomeruli_lt10 = min(relevant)
drop relevant

gen relevant =  doncrit_max_pct_scler_10plgl_imp if non_local_non_zero==1
replace relevant =  doncrit_max_pct_scler_10plgl if non_local_non_zero==2
by donor_id match_id_code: egen cd_glomeruli_10pl = min(relevant)
drop relevant
//doncrit_max~lgl double  %6.0g                 Max Accept % Glomeruli > 10 Observed - Local
//doncrit~lgl_imp double  %6.0g                 Max Accept % Glomeruli > 10 Observed - Import

//doncrit_peak_creat double  %9.0g              Max Acceptable Peak Creatinine (mg/dl) - Local
//doncrit_peak_creat_import double  %9.0g       Max Acceptable Peak Creatinine (mg/dl) - Import
gen relevant =  doncrit_peak_creat_import if non_local_non_zero==1
replace relevant =  doncrit_peak_creat if non_local_non_zero==2
by donor_id match_id_code: egen cd_peak_creat = min(relevant)
drop relevant

//doncrit_final_creat double  %9.0g              Max Acceptable Final Creatinine (mg/dl) - Local
//doncrit_final_creat_import double  %9.0g       Max Acceptable Final Creatinine (mg/dl) - Import
gen relevant =  doncrit_final_creat_import if non_local_non_zero==1
replace relevant =  doncrit_final_creat if non_local_non_zero==2
by donor_id match_id_code: egen cd_final_creat = min(relevant)
drop relevant

//doncrit_max_peak_amylase double  %11.0g       Maximum Acceptable Peak Serum Amylase (u/L)
//doncrit_max_peak_lipase double  %11.0g        Maximum Acceptable Peak Serum Lipase (u/L)
by donor_id match_id_code: egen cd_peak_amylase = min(doncrit_max_peak_amylase)
by donor_id match_id_code: egen cd_peak_lipase = min(doncrit_max_peak_lipase)


summ cd_* if offer_num==1

keep if offer_num==1
drop wlreg_audit_id_code
save `auxiliary_files'fix_donor_screen.dta, replace

