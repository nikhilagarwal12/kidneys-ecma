To Compile the mex functions : run compileAllMex

To check Hessian :
  In knitroOptions.opt uncomment the line with 'derivcheck 3'

To run multistart :
  In knitroOptions.opt set 'ms_enable yes'
  Set number of threads to n where n is the cores to be used on the machine : 'par_numthreads n'

To get best results from knitro run on the production machine once with tuner
set to 1 in the options file. Then include the options suggested by knitro in
the  knitroOptions.opt file. Make sure line with tuner 1 is commented after the
tuning is done.

Outputs from multi-start will be written to file in the folder specified by
'outdir' in knitroOptions.opt. Change the folder when running multiple cases in
same code folder so that the results are not overwritten. Make sure that the
folder already exists. ms_num_to_save sets the number of local optimals knitro
will trite to file

'ms_maxsolves' should be set to a number greater than n*NumberOfVariables. n of
10 would be good, if not possible at least 3 is recommended. 
