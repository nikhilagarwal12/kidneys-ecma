
% Clear workspace, add paths and define target folders;
addpath('./code');
addpath('./code/functions');
addpath('./code/gibbsdiag');
addpath('./code/mcmcdiag');
addpath('./code/model_files');
addpath('./code/cf_functions');
addpath('./code/cf_files');
addpath('./code/cf_optimal');
addpath('../diagnostics/code');

% Point to the right folders
model_data_folder = '~/organs-star/model_master_files/';
models_folder = '~/organs-proj/common_output/structural/input/';
