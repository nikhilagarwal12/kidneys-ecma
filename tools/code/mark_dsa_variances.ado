// Marks DSA's with variances during 2007-2013
// The dataset needs to have the field "dsa_id_var"

program mark_dsa_variances

args dsa_id_var output_var

gen `output_var' = (`dsa_id_var' == "CADN-OP1" | `dsa_id_var' == "CTOP-OP1" | `dsa_id_var' == "DCTC-OP1" | `dsa_id_var' == "FLFH-IO1" | `dsa_id_var' == "FLMP-OP1" | `dsa_id_var' == "FLUF-IO1" | `dsa_id_var' == "FLWC-OP1" | `dsa_id_var' == "LAOP-OP1" | `dsa_id_var' == "MAOB-OP1" | `dsa_id_var' == "MIOP-OP1" | `dsa_id_var' == "MWOB-OP1" | `dsa_id_var' == "NCCM-IO1" | `dsa_id_var' == "OHOV-OP1" | `dsa_id_var' == "OKOP-OP1" | `dsa_id_var' == "ORUO-IO1" | `dsa_id_var' == "PADV-OP1" | `dsa_id_var' == "PATF-OP1" | `dsa_id_var' == "TNDS-OP1" | `dsa_id_var' == "TNMS-OP1" | `dsa_id_var' == "TXGC-OP1" | `dsa_id_var' == "TXSA-OP1" | `dsa_id_var' == "TXSB-OP1" | `dsa_id_var' == "VATB-OP1")

end
