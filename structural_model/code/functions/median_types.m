

classdef median_types
    properties
        donor = [];
        pt_history  = [];    % Arbitrary structure (can be anything)
        patient = [];
        vf = @(x) 0;
        model = [];
        mu_theta = [];
        data = [];
    end  % end properties
    
    methods
    
    
        function obj = median_types(don_pat_pairs,model,all_donors,patients,pt_history,chain,eta_draws_struct,options)
            % Compute the representative donor
            subset_donors  = all_donors.opo_dsa_id==model.local_opo & all_donors.age_don>18 & all_donors.age_don<35;
            obj.donor      = median_object(all_donors(subset_donors,:));   obj.donor.donor_id = 0; 
            obj.pt_history = median_object(pt_history);                    obj.pt_history.wlreg_audit_id_code = 0;
                                                                           obj.pt_history.wl_id_code          = 0;
            obj.patient    = median_object(patients);                      obj.patient.wl_id_code             = 0;
            
            obj.vf         = @(pt,pt_hist) vf_utils(obj.donor,don_pat_pairs,model,all_donors,pt,pt_hist,chain,eta_draws_struct,options);
            obj.model      = model;
            obj.mu_theta   = chain.mu_theta;
            obj.data       = @(pt,pt_hist) gen_data_structure(model,[],representative_offer(pt_hist,pt),pt,obj.donor,pt_hist,'value_function');
        end
        
        function rep_offer= offers(obj)
            rep_offer = representative_offer(obj.pt_history,obj.patient);
        end   
        
        
        function [V,gamma,chi,V_init,Gamma_med_0] = computeV(obj)
            [V,gamma,chi,V_init,Gamma_med_0] =obj.vf(obj.patient,obj.pt_history);
        end

        
    end  % end methods

end % end classdef


% Auxiliary Functions
function med = median_object(obj)
    med = obj(1,:);
    for kk = 1:size(obj,2)
        % Matlab function median does not work with datasets. So I need to take a somewhat indirect approach
        x = zeros(size(obj,1),1);  
        x(:) = obj(:,kk);    % To ensure that x is not a dataset
        med(:,kk) = dataset(median(x));% To ensure that rep_donor is a dataset and variable kk has the right variable type
    end
end
    
    
function rep_offer = representative_offer(pt_history,patients)
    rep_offer.donor_id            = 0;
    rep_offer.geo                 = 2; % Local
    rep_offer.a_mm                = 2;
    rep_offer.b_mm                = 2;
    rep_offer.dr_mm               = 2; 
    rep_offer.c_abo               = 2; % Identical
    rep_offer = struct2dataset(rep_offer);
    rep_offer                     = repmat(rep_offer,numel(pt_history.wlreg_audit_id_code),1);
    rep_offer.wlreg_audit_id_code = pt_history.wlreg_audit_id_code;
    rep_offer.wl_id_code          = pt_history.wl_id_code;
    [~,where]=ismember(rep_offer.wl_id_code,patients.wl_id_code);
    rep_offer.match_date          = patients.init_date(where);
end
    
    
function [V,gamma,chi,V_init,Gamma_med_0] = vf_utils(rep_donor,don_pat_pairs,model,all_donors,patients,pt_hist,chain,eta_draws_struct,options);
    rep_offer = representative_offer(pt_hist,patients); 
    cf_data_hh = gen_data_structure(model,[],rep_offer,patients,rep_donor,pt_hist,'value_function');
    [V,gamma,chi,V_init] = value_function(don_pat_pairs,cf_data_hh,model,all_donors,patients,pt_hist,rep_offer,chain,eta_draws_struct,options);
    Gamma_med_0 = V - cf_data_hh.fixed.obs*chain.mu_theta;
    %Emax_med = ccp_inversion(Gamma_rep_0);
end