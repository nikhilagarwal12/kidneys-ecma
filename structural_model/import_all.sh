#! /bin/sh

# Echo commands to the terminal
  set -x

# Directory for import
DATADIR='~/organs-star/primary_files/nyrt/'
#DATADIR='~/organs-star/primary_files/cross_validation/nyrt/'

# Delete previously created mat files
rm ${DATADIR}*.mat

# Convert txt to mat
/opt/shared_sw/stattransfer/14/st ${DATADIR}patients.txt ${DATADIR}patients.mat -y -of
/opt/shared_sw/stattransfer/14/st ${DATADIR}deceased_donors.txt ${DATADIR}donors.mat -y -of
/opt/shared_sw/stattransfer/14/st ${DATADIR}pt_history.txt ${DATADIR}pt_history.mat -y -of
/opt/shared_sw/stattransfer/14/st ${DATADIR}simulated_offers_matlab.txt ${DATADIR}simulated_offers_matlab.mat -y -of 

chmod 774 ${DATADIR}*.mat

# run matlab
matlab -nodisplay -nosplash -r "data_dir = '${DATADIR}'; cd code; import_all; exit;"
