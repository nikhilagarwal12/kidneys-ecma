function score = score_kas_fun(data_object)

% Creates a score for each patient based on cpra;

% Retrieve information

% Load files
load(data_object.filenames.donors,'donors');
load(data_object.filenames.patients,'patients');
load(data_object.filenames.pt_history,'pt_history');

% Find patient histories
[ismem,where_pt_hist]= ismember(data_object.pt_hist_id,pt_history.wlreg_audit_id_code); 
assert(all(ismem));

% Find patient wl_id_codes
[ismem,where_wl_id]= ismember(pt_history.wl_id_code(where_pt_hist),patients.wl_id_code);
assert(all(ismem));

pra = patients.end_cpra(where_wl_id);


% CPRA function

keyboard;

score = floor(pra/20); % 0-19, 20-39, 40-59, 60 - 79, 80-99, 100
score = bsxfun(@times,score,ones(size(data_object.Gamma_eta)));

% I need:

% Patient age 
% Mismaches
% KDPI
% priorliving
% CPRA
% Waittime
% EPTS
% Different blood type



