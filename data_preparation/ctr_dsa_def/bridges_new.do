// A few OPO/DSA names have changed in the new data (the 8-character codes are the same), so this script modifies "opo_ctr_id_bridge.dta" and "dsa_region_bridge.dta" accordingly

use `ctr_dsa_def'opo_ctr_id_bridge, clear
replace opo_ctr = "CADN-OP1 Donor Network West" if opo_ctr == "CADN-OP1 CA Transplant Donor Network"
replace opo_ctr = "INOP-OP1 Indiana Donor Network" if opo_ctr == "INOP-OP1 Indiana OPO"
replace opo_ctr = "NYRT-OP1 LiveOnNY" if opo_ctr == "NYRT-OP1 New York Organ Donor Network"
replace opo_ctr = "WISE-IO1 Wisconsin Donor Network" if opo_ctr == "WIDN-OP1 Wisconsin Donor Network"
save `ctr_dsa_def'opo_ctr_id_bridge.dta, replace

use `ctr_dsa_def'dsa_region_bridge, clear
replace listing_ctr_dsa = "CADN-OP1 Donor Network West" if listing_ctr_dsa == "CADN-OP1 CA Transplant Donor Network"
replace listing_ctr_dsa = "INOP-OP1 Indiana Donor Network" if listing_ctr_dsa == "INOP-OP1 Indiana OPO"
replace listing_ctr_dsa = "NYRT-OP1 LiveOnNY" if listing_ctr_dsa == "NYRT-OP1 New York Organ Donor Network"
save `ctr_dsa_def'dsa_region_bridge.dta, replace
