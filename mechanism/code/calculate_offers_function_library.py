# This is the main function library for ../../mechanism/code/calculate_offers.py

import csv
import re
import datetime
import math
import pdb
from bitstring import BitArray, Bits

def abo_dictionary():
    O_type ={'O': "Identical",'A2': "Variance"}
    B_type ={'B': "Identical", 'O':"O Donor B Candidate",'A2': "Variance",'A2B': "Variance"}
    A_type ={'A': "Identical", 'A1': "Identical",'A2': "Identical",'O': "Compatible"}
    AB_type={'AB':"Identical", "A1B":"Identical","A2B":"Identical",'O':"Compatible",'A':"Compatible",'A1':"Compatible",'A2':"Compatible",'B': "Compatible"}
    abo_dict={'O':O_type,'B':B_type,'A':A_type,'A1':A_type,'A2':A_type,'AB':AB_type,'A1B':AB_type,'A2B':AB_type}
    return abo_dict


def abo_compatibility(donor,pt,match):

    abo = "Compatible"
    if donor['abo_don'] == 'O' and pt['abo'] == 'B':
        abo = "O Donor B Candidate"
    if re.sub("\d+", "",donor['abo_don']) == re.sub("\d+", "", pt['abo']):
        abo = "Identical"
    if re.sub("\d+", "",donor['abo_don']) == 'AB' and re.sub("\d+", "",pt['abo']) != 'AB':
        abo = "Incompatible"
    
    if re.sub("\d+", "",donor['abo_don']) == 'A' and re.sub("\d+", "",pt['abo']) == 'B':
        abo = "Incompatible"
    elif re.sub("\d+", "",donor['abo_don']) == 'B' and re.sub("\d+", "",pt['abo']) == 'A':
        abo = "Incompatible"
    elif re.sub("\d+", "",donor['abo_don']) != 'O' and re.sub("\d+", "",pt['abo']) == 'O':
        abo = "Incompatible" 

    match['c_abo'] = abo
    return match

# This function identifies HLA Cross-Reactive Groups and determines number of CREG mismatches for a patient-donor pair
def creg_match(a1,a2,b1,b2,da1,da2,db1,db2,creg_groups,match):  # (string,...,string -> int)

    # determine whether patient and donor match in terms of each CREG group
    match_array = [];
    cmis = 0
    for group in creg_groups:
        pt = 1 if "|"+a1+"|" in group['hla_a'] or "|"+a2+"|" in group['hla_a'] or "|"+b1+"|" in group['hla_b'] or "|"+b2+"|" in group['hla_b'] else 0
        don = 1 if "|"+da1+"|" in group['hla_a'] or "|"+da2+"|" in group['hla_a'] or "|"+db1+"|" in group['hla_b'] or "|"+db2+"|" in group['hla_b'] else 0
        compat = 1 if pt==don else 0
        match_array.append(compat)
        cmis = cmis + (1 - compat)

    match['cmis'] = cmis
    return match


def creg_match_flat(a1,a2,b1,b2,da1,da2,db1,db2,creg_groups): 

    # determine whether patient and donor match in terms of each CREG group
    match_array = [];
    cmis = 0
    for group in creg_groups:
        pt = 1 if "|"+a1+"|" in group['hla_a'] or "|"+a2+"|" in group['hla_a'] or "|"+b1+"|" in group['hla_b'] or "|"+b2+"|" in group['hla_b'] else 0
        don = 1 if "|"+da1+"|" in group['hla_a'] or "|"+da2+"|" in group['hla_a'] or "|"+db1+"|" in group['hla_b'] or "|"+db2+"|" in group['hla_b'] else 0
        compat = 1 if pt==don else 0
        match_array.append(compat)
        cmis = cmis + (1 - compat)

    return cmis



def geo_fun(donor,pt,match):  # (dict,dict -> string)

    geo = "National"
    if donor['run_opo_id']==pt['listing_ctr_dsa_id']:
        geo = "Local"
    elif donor['run_opo_region_id']==pt['listing_ctr_region_id']:
        geo = "Regional"
    
    match['geo'] = geo
    return match

 
    
def time_vars_fun(donor,pt,match):

    # Tie-break
    init_date = datetime.datetime.strptime(pt['init_date'],"%d%b%Y");
    # Age
    age_distance = (((match['match_date'] - init_date).days)/365.0)
    age = (int(pt['init_age']) + age_distance)
    # Pediatric at allocation
    if  age >= 18:
        Ped_Alloc = "Non-Ped At Allocation"
    else:
        Ped_Alloc = "Ped At Allocation"
    # Standard points for waiting:
    match['Ped_Alloc'] = Ped_Alloc
    match['init_date'] = init_date
    match['c_age'] = age
    
    p_wait_wl = (match['match_date'].timetuple().tm_year - init_date.timetuple().tm_year 
            - 1*(match['match_date'].timetuple().tm_yday < init_date.timetuple().tm_yday))
    match['p_wait_wl'] = p_wait_wl
    match['c_tie_break'] = (match['match_date'] - match['init_date']).days
    
    return match
    

def cpra_fun(pt,match):
    
    if float(pt['cpra']) <=0.195:
        cpra = "CPRA<=20"
    elif float(pt['cpra']>0.195) and float(pt['cpra'])<0.795:
        cpra = "21<=CPRA<=79"
    elif float(pt['cpra'])>=0.795:
        cpra = "CPRA>=80"    
                    
    match['c_cpra'] = cpra
    return match



def hla_fun(donor,pt,creg_groups,match):

    # patient
    a = set([int(pt['a1']),int(pt['a2'])])-set([0])
    b = set([int(pt['b1']),int(pt['b2'])])-set([0])
    dr = set([int(pt['dr1']),int(pt['dr2'])])-set([0])
    
    # donor
    da = set([int(donor['da1']),int(donor['da2'])]) - set([98])
    db = set([int(donor['db1']),int(donor['db2'])]) - set([98])
    ddr = set([int(donor['ddr1']),int(donor['ddr2'])]) - set([98])
    
    # HLA mismatches
    match['a_mm'] = len(da - a)
    match['b_mm'] = len(db - b)
    match['dr_mm'] = len(ddr - dr)
    # Fixes
    match['a_mm'] = len(da - a) if len(da)>0 and len(a)>0 else 2
    match['b_mm'] = len(db - b) if len(da)>0 and len(a)>0 else 2
    match['dr_mm'] = len(ddr - dr) if len(da)>0 and len(a)>0 else 2
    
    match['hlamis'] = "Zero" if match['a_mm']+match['b_mm']+match['dr_mm']==0 else "Non-Zero"
    
    # CREG MISmatches
    creg_match(pt['a1'],pt['a2'],pt['b1'],pt['b2'],donor['da1'],donor['da2'],donor['db1'],donor['db2'],creg_groups,match)

    return match


def hla_fun_with_eq_core(donor,pt,creg_groups,match,split_equiv):
    # patient
    a = split_equiv['a'].get(pt['a1'],set([])) | split_equiv['a'].get(pt['a2'],set([]))
    b = split_equiv['b'].get(pt['b1'],set([])) | split_equiv['b'].get(pt['b2'],set([]))
    dr = split_equiv['dr'].get(pt['dr1'],set([])) | split_equiv['dr'].get(pt['dr2'],set([]))
    
    # donor
    da = set([int(donor['da1']),int(donor['da2'])]) - set([98])
    db = set([int(donor['db1']),int(donor['db2'])]) - set([98])
    ddr = set([int(donor['ddr1']),int(donor['ddr2'])]) - set([98])
    
    match['a_mm'] = len(da - a) if len(da)>0 and len(a)>0 else 2
    match['b_mm'] = len(db - b) if len(da)>0 and len(a)>0 else 2
    match['dr_mm'] = len(ddr - dr) if len(da)>0 and len(a)>0 else 2
    match['hlamis'] = "Zero" if match['a_mm']+match['b_mm']+match['dr_mm']==0 else "Non-Zero"
    
    return match

def hla_fun_with_eq(donor,pt,creg_groups,match,split_equiv):
    hla_fun_with_eq_core(donor,pt,creg_groups,match,split_equiv)
    creg_match(pt['a1'],pt['a2'],pt['b1'],pt['b2'],donor['da1'],donor['da2'],donor['db1'],donor['db2'],creg_groups,match)
    return match

def hla_fun_with_eq_light(donor,pt,creg_groups,match,split_equiv):
    hla_fun_with_eq_core(donor,pt,creg_groups,match,split_equiv)
    # CREG MISmatches
    if donor['run_opo_id'][0:2]=="TN":
        creg_match(pt['a1'],pt['a2'],pt['b1'],pt['b2'],donor['da1'],donor['da2'],donor['db1'],donor['db2'],creg_groups,match)
    return match
    

# Keeping points and bins separate functions for now

def point_vars_fun(donor,pt,match):
    
    zmm = match['hlamis'] == 'Zero'
    dcd = donor['non_hrt_don']== 'Y'
    natreg = (match['geo'] == 'National' or match['geo'] == 'Regional')
    age_match = match['c_age']
    
    match['p_ped11'] = 3*(zmm and age_match< 18) + 1*(zmm and age_match < 11)
    match['p_ped35']  = (int(donor['age_don'])<35)*(1*(age_match<11)+3*(zmm and dcd and age_match<18 and natreg))
    match['p_hla'] = 4 if float(pt['cpra'])>=0.795 else 0
    match['p_pra'] = 2 - match['dr_mm']
    match['p_pld'] = 4 if pt['prior_living_donor']=='1' else 0

    match['Ped_List'] = "Ped At Listing" if int(pt['init_age']) < 18 else "Non-Ped At Listing"
    
    
    return match


    
def min_wait_fun(donor,pt,match,daysp=365):

    # Initialize
    needed_points = 0
    needed_tiebreak = 0
    needed_wait_time = 0

    if match['geo'] == 'Payback':
        last_bin = int(donor['last_bin_payback'])
        last_kidney_points=float(donor['last_kidney_points_payback'])
        last_tiebreak = float(donor['last_tie_break_payback'])
    else:
        last_bin = int(donor['last_bin'])
        last_kidney_points=float(donor['last_kidney_points'])
        last_tiebreak = float(donor['last_tie_break'])
        
    ever_offered = last_bin>= match['c_bin']
    always_offered =ever_offered and (last_bin> match['c_bin'] or last_kidney_points<match['c_addtl_pts'])
    if last_bin == match['c_bin'] and last_kidney_points>match['c_addtl_pts']:
        needed_points = last_kidney_points-match['c_addtl_pts']
        needed_tiebreak = last_tiebreak
        if needed_points*daysp> needed_tiebreak:
            needed_wait_time = needed_points*daysp
        else:
            needed_wait_time = needed_tiebreak
    if last_bin == match['c_bin'] and last_kidney_points==match['c_addtl_pts']:
        needed_tiebreak = last_tiebreak
        # In all variances patients get points at their anniversary.
        needed_wait_time = min(needed_tiebreak,365)
        
    # modify based on offer type; if it's a "within" DSA offer don't consider patients from other DSAs
    # in final version of the code, will want to filter these guys out earlier
    
    if ever_offered and needed_wait_time == 0:
        needed_wait_time = -1
    if always_offered:
        needed_wait_time = -1

    match['ever_offered'] = ever_offered
    match['always_offered'] = always_offered
    match['needed_wait_time'] = int(needed_wait_time)
    
    return match


def points_and_bins_fun(donor,pt,match,young_healthy,old_healthy,young_dcd,old_dcd,ecd,ecd_dcd):
    match['c_addtl_pts'] =  0 
    if donor['donor_cat'] in ('young_healthy','old_healthy','old_dcd','young_dcd'):
        match['c_addtl_pts'] = match['p_hla'] + match['p_pra'] + max(match['p_ped11'],match['p_ped35']) + match['p_pld'] 
        # Standard Criteria Donor (SCD), Age < 35
        if donor['donor_cat']=='young_healthy':
            for row in young_healthy:
                if row[0] == match['hlamis'] and row[1] == match['c_cpra'] and row[2] == match['c_abo'] and row[3] == match['Ped_List'] and row[4] == match['Ped_Alloc'] and row[5] == match['geo']:
                    bin = row[6]
            try:
                bin = "40" if int(bin) > 40 and match['geo'] == "Local" and match['prior_living_donor'] == "1" else bin
            except:
                print match['hlamis']
                print match['c_cpra']
                print match['c_abo']
                print match['Ped_List']
                print match['Ped_Alloc']
                print match['geo']
                for row in young_healthy:
                    print row

        # Standard Criteria Donor (SCD), Age >= 35
        elif donor['donor_cat']=='old_healthy':
            for row in old_healthy:
                if row[0] == match['hlamis'] and row[1] == match['c_cpra'] and row[2] == match['c_abo'] and row[3] == match['Ped_List'] and row[4] == match['Ped_Alloc'] and row[5] == match['geo']:
                    bin = row[6]
            bin = "40" if int(bin) > 40 and match['geo'] == "Local" and match['prior_living_donor'] == "1" else bin

        # Donation after Circulatory Death (DCD), Age < 35
        elif donor['donor_cat']=='young_dcd':
            for row in young_dcd:
                if row[0] == match['hlamis'] and row[1] == match['c_abo'] and row[2] == match['Ped_List'] and row[3] == match['geo']:
                    bin = row[4]
            bin = "4" if int(bin) > 4 and match['geo'] == "Local" and match['prior_living_donor'] == "1" else bin

        # Donation after Circulatory Death (DCD), Age >= 35
        elif donor['donor_cat']=='old_dcd':
            for row in old_dcd:
                if row[0] == match['hlamis'] and row[1] == match['c_abo'] and row[2] == match['geo']:
                    bin = row[3]
            bin = "4" if int(bin) > 4 and match['geo'] == "Local" and match['prior_living_donor'] == "1" else bin

    # Expanded Criteria Donor (ECD)
    elif donor['donor_cat']=='ecd':
        for row in ecd:
            if row[0] == match['hlamis'] and row[1] == match['c_cpra'] and row[2] == match['c_abo'] and row[3] == match['Ped_List'] and row[4] == match['Ped_Alloc'] and row[5] == match['geo']:
                bin = row[6]

    # ECD and DCD
    elif donor['donor_cat']=='ecd_dcd':
        for row in ecd_dcd:
            if row[0] == match['hlamis'] and row[1] == match['c_abo'] and row[2] == match['geo']:
                bin = row[3]


    match['c_bin'] = int(bin)
    match['c_kidney_points'] = match['c_addtl_pts'] + match['p_wait_wl']
    
    return match

    
def abo_abdr_ok(donor,pt,match):

    match['abo_abdr_ok'] = (donor['abo_don'] != 'O' and donor['abo_don'] != 'B') or match['c_abo']=="Identical" or match['hlamis'] =="Zero"
    return match


def offer_fun(donor,pt,match):

    match['offered_points'] = match['c_tie_break'] >=  match['needed_wait_time'] and match['ever_offered']
    match['offered_runtype'] = False if donor['run_type']=="within" and pt['listing_ctr_dsa_id'] != donor['run_opo_id'] else True
    match['offered_status'] = int(pt['unos_cand_stat_cd']) in [4010,4050,4060]
    match['predicted_mechanism'] = (match['abo_abdr_ok'] and (not match['unacceptable']) and (not match['unacceptable_eq']) and match['offered_runtype'] and match['offered_status'] and match['offered_points'])
    match['predicted_offer'] = match['predicted_mechanism'] and (match['screened']==0 or match['screened']==50)
    return match



def unacc_antigen_fun(donor,pt,match):

    unacceptable_a = pt['unacc_antigens_A'].split("|")
    unacceptable_b = pt['unacc_antigens_B'].split("|")
    unacceptable_bw = pt['unacc_antigens_BW'].split("|")
    unacceptable_c = pt['unacc_antigens_C'].split("|")
    unacceptable_dr = pt['unacc_antigens_DR'].split("|")
    unacceptable_dq = pt['unacc_antigens_DQ'].split("|")
    
    unaccept  = ""
    if donor['da1'] in unacceptable_a or donor['da2'] in unacceptable_a:
        unaccept  = unaccept + "A"
    if donor['db1'] in unacceptable_b or donor['db2'] in unacceptable_b:
        unaccept  = unaccept + "B"
    if donor['dbw4']=='95' and '4' in unacceptable_bw:
        unaccept  = unaccept + "Bw4"
    if donor['dbw6']=='95' and '6' in unacceptable_bw:
        unaccept  = unaccept + "Bw6"
    if donor['dc1'] in unacceptable_c or donor['dc2'] in unacceptable_c:
        unaccept  = unaccept + "C"
    if donor['ddr1'] in unacceptable_dr or donor['ddr2'] in unacceptable_dr:
        unaccept  = unaccept + "DR"
    if donor['ddr51']=='95' and '51' in unacceptable_dr:
        unaccept  = unaccept + "DR51"
    if donor['ddr52']=='95' and '52' in unacceptable_dr:
        unaccept  = unaccept + "DR52"
    if donor['ddr53']=='95' and '53' in unacceptable_dr:
        unaccept  = unaccept + "DR53"
    if donor['ddq1'] in unacceptable_dq or donor['ddq2'] in unacceptable_dq:
        unaccept  = unaccept + "DQ"

    match['reason_unacc'] = unaccept
    match['unacceptable'] = len(unaccept)>0
    
    return match
    
def unacc_antigen_fun_with_eq(donor,pt,match,unacc_equiv):
    antigens = ['A','B','C','DR','DQ']
    unaccept = ""
    unacceptable_bw = pt['unacc_antigens_BW'].split("|")    
    unacceptable_dr = pt['unacc_antigens_DR'].split("|")
    
    for a in antigens:
        donor_antigen_set = set([int (x) for x in [donor['d'+a.lower()+'1'],donor['d'+a.lower()+'2']] if len(x)>0 ])
        for s in pt['unacc_antigens_'+a].split("|"):
            wrong_antigens = donor_antigen_set  & unacc_equiv[a.lower()].get(s,set([]))
            unaccept = unaccept + ''.join([s+str(x) for x in wrong_antigens])
        if a=='B':
            for s in unacceptable_bw:
                wrong_antigens = donor_antigen_set  & unacc_equiv['b'].get('Bw'+s,set([]))
                unaccept = unaccept + ''.join([s+str(x) for x in wrong_antigens])
                
    unaccept = unaccept + 'dr51' if (donor['ddr51']=='95' and '51' in unacceptable_dr) else unaccept
    unaccept = unaccept + 'dr52' if (donor['ddr52']=='95' and '52' in unacceptable_dr) else unaccept
    unaccept = unaccept + 'dr53' if (donor['ddr53']=='95' and '53' in unacceptable_dr) else unaccept
    unaccept = unaccept + 'bw4' if (donor['dbw4']=='95' and '4' in unacceptable_bw) else unaccept
    unaccept = unaccept + 'bw6' if (donor['dbw6']=='95' and '6' in unacceptable_bw) else unaccept
     
    match['unacceptable_eq'] = len(unaccept)>0
    match['reason_unacc_eq'] = unaccept
    return match


def eq_unacc_antigen_fun_expand(pt,unacc_equiv):
    unacceptable = dict()
    unacceptable['bw'] = pt['unacc_antigens_BW'].split("|")
    unacceptable['a'] = set.union(*[unacc_equiv['a'].get(a,set([])) for a in pt['unacc_antigens_A'].split("|")])
    unacceptable['b'] = set.union(*[unacc_equiv['b'].get(a,set([])) for a in pt['unacc_antigens_B'].split("|")] + [unacc_equiv['b'].get('Bw'+a,set([])) for a in pt['unacc_antigens_BW'].split("|")])
    unacceptable['c'] = set.union(*[unacc_equiv['c'].get(a,set([])) for a in pt['unacc_antigens_C'].split("|")])
    unacceptable['dr'] = set.union(*[unacc_equiv['dr'].get(a,set([])) for a in pt['unacc_antigens_DR'].split("|")])
    unacceptable['dq'] = set.union(*[unacc_equiv['dq'].get(a,set([])) for a in pt['unacc_antigens_DQ'].split("|")])
    pt['eq_unacc'] =  unacceptable
    return pt

def unacc_antigen_fun_with_eq_light(donor,pt,match):
    antigens = ['A','B','C','DR','DQ']
    unaccept = False
    for a in antigens:
        if unaccept:
            break
            donor_antigen_set = set([int (x) for x in [donor['d'+a.lower()+'1'],donor['d'+a.lower()+'2']] if len(x)>0 ])
            unaccept = len(pt['eq_unacc'][a.lower()] & donor_antigen_set)>0
    
    if not unaccept:
        unaccept = donor['ddr51']=='95' and '51' in pt['eq_unacc']['dr']
        unaccept = (donor['ddr52']=='95' and '52' in pt['eq_unacc']['dr']) or unaccept
        unaccept = (donor['ddr53']=='95' and '53' in pt['eq_unacc']['dr']) or unaccept
        unaccept = (donor['dbw4']=='95' and '4' in pt['eq_unacc']['bw']) or unaccept
        unaccept = (donor['dbw6']=='95' and '6' in pt['eq_unacc']['bw']) or unaccept
        
    match['unacceptable_eq'] = unaccept
    match['unacceptable'] = unaccept
    
    return match
    


def screening_fun(donor,pt,match):

    screened =''
    if pt['doncrit_min_age']!="" and donor['age_don']!="" and match['imported']==False:
        screened = screened + '|1' if int(pt['doncrit_min_age'])/12 > int(donor['age_don']) else screened
    if pt['doncrit_min_age_import']!="" and donor['age_don']!="" and match['imported']==True:
        screened = screened + '|2' if int(pt['doncrit_min_age_import'])/12 > int(donor['age_don']) else screened
    if pt['doncrit_max_age']!="" and donor['age_don']!="" and match['imported']==False:
        screened = screened + '|3' if int(pt['doncrit_max_age'])/12 < int(donor['age_don']) else screened
    if pt['doncrit_max_age_import']!="" and donor['age_don']!="" and match['imported']==True:
        screened = screened + '|4' if int(pt['doncrit_max_age_import'])/12 < int(donor['age_don']) else screened
    if pt['doncrit_min_wgt']!="" and donor['wgt_kg_don_calc']!="":
        screened = screened + '|5' if float(pt['doncrit_min_wgt']) > float(donor['wgt_kg_don_calc']) else screened
    if pt['doncrit_max_wgt']!="" and donor['wgt_kg_don_calc']!="":
        screened = screened + '|6' if float(pt['doncrit_max_wgt']) < float(donor['wgt_kg_don_calc']) else screened                        
    if pt['doncrit_acpt_dcd']!="" and match['imported']==False:
        screened = screened + '|7' if pt['doncrit_acpt_dcd']=='N' and (donor['donor_cat']=='young_dcd' or donor['donor_cat']=='old_dcd' or donor['donor_cat']=='ecd_dcd') else screened
    if pt['doncrit_acpt_dcd_import']!="" and match['imported']==True:
        screened = screened + '|8' if pt['doncrit_acpt_dcd_import']=='N' and (donor['donor_cat']=='young_dcd' or donor['donor_cat']=='old_dcd' or donor['donor_cat']=='ecd_dcd') else screened
    if pt['doncrit_acpt_hcvpos']!="":
        screened = screened + '|9' if pt['doncrit_acpt_hcvpos']=='N' and donor['hep_c_anti_don']=='P' else screened
    if pt['max_mismatch_a']!="":
        screened = screened + '|10' if int(pt['max_mismatch_a']) < match['a_mm'] else screened
    if pt['max_mismatch_b']!="":
        screened = screened + '|11' if int(pt['max_mismatch_b']) < match['b_mm'] else screened
    if pt['max_mismatch_dr']!="":
        screened = screened + '|12' if int(pt['max_mismatch_dr']) < match['dr_mm'] else screened
    if pt['max_mismatch_ab']!="":
        screened = screened + '|13' if int(pt['max_mismatch_ab']) < match['a_mm'] + match['b_mm'] else screened
    if pt['max_mismatch_adr']!="":
        screened = screened + '|14' if int(pt['max_mismatch_adr']) < match['a_mm'] + match['dr_mm'] else screened
    if pt['max_mismatch_bdr']!="":
        screened = screened + '|15' if int(pt['max_mismatch_bdr']) < match['b_mm'] + match['dr_mm'] else screened
    if pt['max_mismatch_abdr']!="":
        screened = screened + '|16' if int(pt['max_mismatch_abdr']) < match['a_mm'] + match['b_mm'] + match['dr_mm'] else screened
    if pt['on_expand_donor']!="" and match['imported']==False and (donor['donor_cat']=='ecd' or donor['donor_cat']=='ecd_dcd'):
        screened = screened + '|18' if int(pt['on_expand_donor'])==0 else screened
        if int(pt['on_expand_donor']) == 1 and screened != True:
            if pt['max_expanded_mismatch_a'] != "":
                screened = screened + '|19' if int(pt['max_expanded_mismatch_a']) < match['a_mm'] else screened
            if pt['max_expanded_mismatch_b'] != "": 
                screened = screened + '|20' if int(pt['max_expanded_mismatch_b']) < match['b_mm'] else screened
            if pt['max_expanded_mismatch_dr'] != "": 
                screened = screened + '|21' if int(pt['max_expanded_mismatch_dr']) < match['dr_mm'] else screened
            if pt['max_expanded_mismatch_ab'] != "": 
                screened = screened + '|22' if int(pt['max_expanded_mismatch_ab']) < match['a_mm'] + match['b_mm'] else screened
            if pt['max_expanded_mismatch_adr'] != "": 
                screened = screened + '|23' if int(pt['max_expanded_mismatch_adr']) < match['a_mm'] + match['dr_mm'] else screened
            if pt['max_expanded_mismatch_bdr'] != "": 
                screened = screened + '|24' if int(pt['max_expanded_mismatch_bdr']) < match['b_mm'] + match['dr_mm'] else screened
            if pt['max_expanded_mismatch_abdr'] != "": 
                screened = screened + '|25' if int(pt['max_expanded_mismatch_abdr']) < match['a_mm'] + match['b_mm'] + match['dr_mm'] else screened
    if pt['on_iexpand_donor']!="" and match['imported']==True and (donor['donor_cat']=='ecd' or donor['donor_cat']=='ecd_dcd'):
        screened = screened + '|26' if int(pt['on_iexpand_donor'])==0 else screened
        if int(pt['on_iexpand_donor']) == 1 and screened == 0:
            if pt['max_iexpanded_mismatch_a'] != "":
                screened = screened + '|27' if int(pt['max_iexpanded_mismatch_a']) < match['a_mm'] else screened
            if pt['max_iexpanded_mismatch_b'] != "": 
                screened = screened + '|28' if int(pt['max_iexpanded_mismatch_b']) < match['b_mm'] else screened
            if pt['max_iexpanded_mismatch_dr'] != "": 
                screened = screened + '|29' if int(pt['max_iexpanded_mismatch_dr']) < match['dr_mm'] else screened
            if pt['max_iexpanded_mismatch_ab'] != "": 
                screened = screened + '|30' if int(pt['max_iexpanded_mismatch_ab']) < match['a_mm'] + match['b_mm'] else screened
            if pt['max_iexpanded_mismatch_adr'] != "": 
                screened = screened + '|31' if int(pt['max_iexpanded_mismatch_adr']) < match['a_mm'] + match['dr_mm'] else screened
            if pt['max_iexpanded_mismatch_bdr'] != "": 
                screened = screened + '|32' if int(pt['max_iexpanded_mismatch_bdr']) < match['b_mm'] + match['dr_mm'] else screened
            if pt['max_iexpanded_mismatch_abdr'] != "": 
                screened = screened + '|33' if int(pt['max_iexpanded_mismatch_abdr']) < match['a_mm'] + match['b_mm'] + match['dr_mm'] else screened                        
    if pt['doncrit_acpt_hbcorepos'] != "" and donor['hbv_core_don'] != "":
        screened = screened + '|34' if pt['doncrit_acpt_hbcorepos'] == "N" and donor['hbv_core_don'] != "N" and donor['hbv_core_don'] != "ND" else screened
    if pt['doncrit_acpt_hcvpos'] != "" and donor['hep_c_anti_don'] != "":
        screened = screened + '|35' if pt['doncrit_acpt_hcvpos'] == "N" and donor['hep_c_anti_don'] != "N" and donor['hep_c_anti_don'] != "ND" else screened
    if pt['doncrit_acpt_htlv_pos'] != "" and donor['htlv_don'] != "":
        screened = screened + '|36' if pt['doncrit_acpt_htlv_pos'] == "N" and donor['htlv_don'] != "N" and donor['htlv_don'] != "ND" else screened
    if pt['doncrit_max_bmi']!="" and donor['bmi_don_calc']!="":
        screened = screened + '|37' if float(donor['bmi_don_calc']) > float(pt['doncrit_max_bmi']) else screened
    if pt['doncrit_acpt_hist_diabetes']!= "" and donor['hist_diabetes_don'] != "":
        screened = screened + '|38' if pt['doncrit_acpt_hist_diabetes']=="N" and int(donor['hist_diabetes_don']) > 1 and int(donor['hist_diabetes_don']) < 6 else screened
    if pt['doncrit_acpt_hist_hypertension']!= "" and donor['hist_hypertens_don'] != "":
        screened = screened + '|39' if pt['doncrit_acpt_hist_hypertension']=="N" and donor['hist_hypertens_don'] == "Y" else screened
    if pt['doncrit_max_warm_time'] != "" and donor['warm_isch_tm_don'] != "":
        screened = screened + '|40' if int(pt['doncrit_max_warm_time']) < int(donor['warm_isch_tm_don']) else screened
    if pt['doncrit_max_peak_amylase'] != "" and donor['amylase_don'] != "":
        screened = screened + '|41' if float(pt['doncrit_max_peak_amylase']) < float(donor['amylase_don']) else screened
    if pt['doncrit_max_peak_lipase'] != "" and donor['lipase_don'] != "":
        screened = screened + '|42' if float(pt['doncrit_max_peak_lipase']) < float(donor['lipase_don']) else screened
    if pt['doncrit_final_creat'] != "" and donor['creat_don'] != "" and match['imported']==False:
        screened = screened + '|43' if float(pt['doncrit_final_creat']) < float(donor['creat_don']) else screened
    if pt['doncrit_final_creat_import'] != "" and donor['creat_don'] != "" and match['imported']==True:
        screened = screened + '|44' if float(pt['doncrit_final_creat_import']) < float(donor['creat_don']) else screened
    if pt['doncrit_peak_creat'] != "" and donor['creat_peak_don'] != "" and match['imported']==False:
        screened = screened + '|45' if float(pt['doncrit_peak_creat']) < float(donor['creat_peak_don']) else screened
    if pt['doncrit_peak_creat_import'] != "" and donor['creat_peak_don'] != "" and match['imported']==True:
        screened = screened + '|46' if float(pt['doncrit_peak_creat_import']) < float(donor['creat_peak_don']) else screened

    match['screened'] = len(screened)>0
    match['reason_screen'] = screened
    return match
    
    
def fixboolean(match,listofvars = None):
    if isinstance(listofvars,type(None)):
        listofvars = match.keys()
    for var in [v for v in listofvars if isinstance(match[v],bool)]:
        match[var] = 1*match[var]
    return match
        

def label_decode(dictin,label_dict):
    for key in [k for k in dictin.keys() if k in label_dict.keys()]:
        dictin[key] = label_dict[key][dictin[key]]
    return dictin

def label_encode(dictin,label_dict):
    for key in [k for k in dictin.keys() if k in label_dict.keys()]:
        code=[k for k in label_dict[key].keys() if label_dict[key][k]== dictin[key]] 
        dictin[key] = code[0] if len(code)==1 else ''
    return dictin        
        
def time_till_intervals(x):
    smpl_interval = []
    x = [j for j in x if not (len(j)==1 and j[0]==0)] # Drop one-sided intervals with 0 lower bound
    x = [j for j in x if len(j)==1 or (j[0]<j[1] and not j[0]==0)] # Drop intervals with upper bound < lower bound
    x = [[0]] if len(x)==0 else x
    mx = max([j for i in x for j in i])  # Maximum element in x
    for interval in x:
        if len(interval)==1:
            interval.append(mx+1) # If interval has length 1 it has no upper limit
        if len(smpl_interval)>1 and smpl_interval[-1]>=interval[0]:
            smpl_interval[-1] = interval[1] #the second interval extends the validity of the first one.
        else:
            smpl_interval = smpl_interval + interval #disjoint intervals              
    smpl_interval = [s for s in smpl_interval if s<=mx] if len(smpl_interval)>0 else [0]
    return smpl_interval
                    
            
class binwriter(object):
    def __init__(self,file,length):
        self.file = file
        self.length = length   # At initialization, set the length (in bits) of the data
        self.buffer = []       # an empty buffer
        self.file.write(Bits(uint = length,length=8).bytes)    # Write in the file the length in it's first byte (256 max)
        self.flush_at_closing = True    # Set default. Will flush at closing
    def bytewrite(self,bits):
        self.file.write(Bits(uint = bits.uint,length=len(bits)).bytes)   # Writes bytes
    def binwrite(self,integer):
        self.buffer = self.buffer + Bits(uint = integer,length=self.length)   # Accumulates bits in the buffer + between Bits is just concatenation
        if len(self.buffer)>=8:     # If the buffer can be stored in a byte
            x = (len(self.buffer)/8)*8
            self.bytewrite(self.buffer[0:x]) # write it
            self.buffer = self.buffer[x:]         # keep the remainder
    def close(self):
        if self.flush_at_closing and len(self.buffer)>0:
            self.buffer.tofile(self.file)         # Saves the remainder, will pad with zeros at the end to make it byte aligned
        self.file.close()                         # close file  
    

            
        
