// Creates auxiliary table with list of transplants that actually occurred


program create_transplants_table

args transplants sample_start_date sample_end_date primary_folder


  // prior to main data work: create a set of transplanted patient-donor pairs from KIDPAN
  use "`primary_folder'kidpan", clear
  keep if don_ty == "C" & tx_date >= `sample_start_date' & tx_date <= `sample_end_date'
  keep wl_id_code donor_id
  save `transplants', replace


end
