% Computes the value function for a patient at a point in time
% Prepares data to call continuation_value.m, returns V and Gamma

function [V,gamma,chi,V_init]=value_function(don_pat_pairs,data,model,donors,patients,pt_history,offers,chain,eta,beta_i,options,include_pt)

%% Check that all patients are in don_pat_pairs:
foundit=false(size(data.pt_history_id));
for k=1:numel(don_pat_pairs), foundit=foundit | ismember(data.pt_history_id,don_pat_pairs{k}.pt_hist_id); end
if ~all(foundit), missing_pt=unique(~foundit), keyboard; end

%% A. Inline functions
% 1 "A" 11 "A1" 12 "A2" 2 "B" 0 "O" 3 "AB" 31 "A1B" 32 "A2B" -> {'O','A','B','AB'}
flat = @(x) x(:);
%% B. Defaults
if isfield(model,'T_age'), T_age = model.T_age; else T_age=100; end
if ~exist('options','var'), options =struct(); end
if model.donor_uh
    if ~isfield(options, 'edraws'), options.edraws=size(eta.draws,2); end
    eta.draws = eta.draws(:,round([1:options.edraws]*size(eta.draws,2)/options.edraws));
end
if isfield(options,'V'), V = options.V; else V=[]; end
if ~isfield(options, 'tdraws'), options.tdraws=40; end
if ~isfield(options, 'iterate_over'), options.iterate_over='offers'; end

ref_data = @(x) x;   % Identity function; overwritten if we want to exclude some patients;

%% C. Set up
if ~isfield(data,'match_date'), data.match_date = offers(data.offer_index,:).match_date; end % get match date
assert(all(size(data.match_date)==[data.N_offers,1]));   % checking that data.match_date has the right size

switch options.iterate_over
  case 'offers', 
    [unique_offer,where_first_offer,offer_map] = unique([data.pt_history_id, data.match_date],'rows');
    N_iter = size(unique_offer,1);
  case 'patients'
    N_iter = data.N_histories; 
    [unique_pt_history_id,where_first_pt_hist,pt_hist_map] = unique(data.pt_history_id);
    if exist('include_pt','var') && ~isempty(include_pt) && ...
            (numel(include_pt)<numel(data.pt_history_id) || ~all(include_pt))  % We mean to exclude some patients
        ref_data = unique(pt_hist_map(include_pt));
        N_iter = numel(ref_data); % ref_data contains the position in data of the patients that we are going to compute
    end
end

if ~isfield(options, 'verbose'), options.verbose=0; end

%% D. Loop
if numel(V)~=data.N_offers
    V = zeros(data.N_offers,1); 
    match_init_date_problem = 0;
    V_init.pt_history = zeros(N_iter,1);
    V_init.V          = zeros(N_iter,1);
end
if options.verbose, tic; fprintf('Computing V\n'); end


for mm = 1:N_iter
    if options.verbose && mod(mm,options.verbose)==0, fprintf('%d of %d:%3.2f\n',mm,N_iter,toc); end
    nn = ref_data(mm);
    
    switch options.iterate_over
      case 'offers', 
        this_pt_hist_id = unique_offer(nn,1); %offer's wlreg_audit_id_code
        match_date = unique_offer(nn,2);
        pt  = data.patients(where_first_offer(nn)); % wl_id_code we're looking at
      case 'patients', 
        this_pt_hist_id = unique_pt_history_id(nn);
        pt  = data.patients(where_first_pt_hist(nn));
        match_date = data.match_date(pt_hist_map==nn);
    end
    
    this_pt_hist = pt_history(pt_history.wlreg_audit_id_code==this_pt_hist_id,:);
    this_patient = patients(patients.wl_id_code==pt,:);
    
    waittime = [0; match_date-this_patient.init_date];
    
    [this_V, errors] = continuation_value(waittime,don_pat_pairs,model,donors,patients,this_pt_hist,chain,eta,beta_i,options);
    
    match_init_date_problem = match_init_date_problem + errors;
    V_init.pt_history(mm,:) = this_pt_hist_id;
    V_init.V(mm)            = this_V(1);
    switch options.iterate_over
      case 'offers'
        V(offer_map==nn) = this_V(2:end)';
      case 'patients'
        V(pt_hist_map==nn) = this_V(2:end)';
    end
end

%% E. Output
isnanV=isnan(V); 
if options.verbose
    fprintf('Number of NaNs in V: %d\n',sum(isnanV));
    fprintf('Instances of match_date < init_date: %d\n', match_init_date_problem);
end
V(isnanV)=0;    % Replacing NaNs by zero. There is a NaN when no kidney is available for this patient.
if model.pt_uh     % design matrix
  if size(pt_history,1)==1
    pt = pt_history.wl_id_code;
    if model.pt_uh, beta_i_nn = beta_i(patients.wl_id_code==pt); else beta_i_nn = []; end
    chi = chi_param(data,model,beta_i_nn); 
  else
    where_ii= cell(size(data.patient_list));
    for ii=1:data.N_patients
        where_ii{ii} = find(data.patients == data.patient_list(ii));           
    end
    chi = zeros(data.N_offers,model.K_theta);
    for kk = 1:model.K_types
        where_kk = cat(1,where_ii{beta_i==kk});  % offer indices for each type
        chi(where_kk,:) = chi_param(data,model,kk,where_kk); % populate chi matrix for this type
    end
  end
else
    chi = chi_param(data,model);
end
mu = -chi*chain.mu_theta;
gamma = V + mu;
