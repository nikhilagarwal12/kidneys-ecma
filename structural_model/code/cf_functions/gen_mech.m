function [cf_mech,init_cond] = gen_mech(cf_mech,data,model,options)
   % options common to all alternative mechanisms:
   options              = defaultvalue(options,'verbose',true);
   % Save options
   cf_mech.options      = catstruct(cf_mech.options,options);

   def_pool_size       = max((model.pt_arrival_rate - model.lambda/5)/(1-mean(data.q_leave_exog(:))),1000);
   options             = defaultvalue(options,'pool_size',def_pool_size);

   % Optimization defaults;
   init_cond           = struct();
   init_cond.state     = struct();
   if isfield(cf_mech,'heuristic'), init_cond.heuristic = cf_mech.heuristic; else init_cond.heuristic = struct(); end
   if isfield(options,'heuristic'), init_cond.heuristic = options.heuristic; end

   if strcmp(cf_mech.type,'score')
        
        if isstruct(data), data=cf_data(data); end;
        % Construct the score
        if cf_mech.use_proj 
            data.Gamma_eta = data.proj.Gamma_eta ;
            data.V_fact    = data.proj.V;
        else
            data.Gamma_eta = data.calc.Gamma_eta ;
            data.V_fact    = data.calc.V; 
        end
        cf_mech.score = cf_mech.score(data);
        if isfield(cf_mech,'qualy')
            cf_mech.qualy = cf_mech.qualy(data);
        end

        % Define the function that computes the ss
        cf_mech.compute_ss = @(obj,soln_file) wrap_ss_allocation(obj.data,obj.mech,...
                catstruct(obj.data.options,obj.mech.options),obj.soln.state,soln_file); 
                % see subfunction below
        % initial conditions
        init_cond.state.pool_size  = options.pool_size;
        
        options = defaultvalue(options,'pt_weights',data.pt_entry_weights);
        init_cond.state.pt_weights = options.pt_weights;
        
        options = defaultvalue(options,'p_tx',ones(size(data.Gamma_eta))/2);
        init_cond.state.p_tx = options.p_tx;
        
   elseif strcmp(cf_mech.type,'optim')
       init_cond = struct();
       init_cond.opt_prob = optimal_ss_problem(data).mechanism(cf_mech);    % optimal_ss_problem object to be saved for supply/knitro (see subfunction alt2 below)
       init_cond.first_best = cf_mech.first_best; 
       init_cond.alloc      = [];
       cf_mech.compute_ss = @loadoptim;       
   else
       error('Error! Unrecognized counterfactual type');
   end
   
   function output   = wrap_ss_allocation(data,cf_mech,options,alloc_init,soln_file)
       % builds an optimal_ss_problem instance, run score_ss_allocation and return output compatible
       % with the rest of the workflow.
       if ~isfield(cf_mech,'randstart')
           cf_mech.randstart = false;
       end
       % Use default or random starting value
       if cf_mech.randstart
           opt_prob = optimal_ss_problem(data,cf_mech).mechanism(cf_mech,alloc_init).set_rand_alloc;
       else
           opt_prob = optimal_ss_problem(data,cf_mech).mechanism(cf_mech,alloc_init).set_default_alloc;
       end

       [state,heuristic] = opt_prob.find_ss_allocation;
       opt_prob.alloc = state;
       output = opt_prob.fill_vf_decomp.full_times(size(data.proj.V,2)).export2main(options);
       save(soln_file,'state','heuristic');
       
   function output  = loadoptim(obj,file)
       % Loads the solution to the optimal allocation problem;
       opt_prob = optimal_ss_problem(obj.data).mechanism(obj.mech);
       output = [];
       if ~exist(file,'file')
           fprintf('%s\n does not exist!\n',file)
       else
           soln     = load(file);
           if ~isfield(soln,'alloc')
                fprintf('%s\n does not contain an alloc field!\n',file)
           else
                opt_prob.alloc = soln.alloc;
                opt_prob_full = opt_prob.fill_vf_decomp.full_times(size(obj.data.calc.V,2));
                output   = opt_prob_full.export2main(obj.data.options);
                fprintf('%s\n contains an alloc field with exitflag %d\n',file,soln.alloc.exitflag)
           end
       end
