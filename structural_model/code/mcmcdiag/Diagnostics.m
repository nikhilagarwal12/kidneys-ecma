%% Diagnostics


clear

addpath '/proj/pagarwal/parag/demand/Bayesian/mcmcdiag'
addpath '/proj/pagarwal/parag/demand/Bayesian/results'
addpath '/proj/pagarwal/parag/demand/Common'

load('/proj/pagarwal/parag/demand/Specifications/spec6.mat');

cd '/proj/pagarwal/parag/demand/Bayesian/results'
vars=whos('spec*');

%Define here chains to perform Diagnostics
chains = [1 2 3];
histo = 0;
trace=0;
diag=1;
all=1;

for iter=17:17


clear coeff1 coeff2 coeff results result

spec_name=vars(iter).name;
fprintf('Spec name: %12s \n',spec_name)

max_outputfile =  250.*ones(1,3); % This is the total number of outputfiles
%Parameters for burn-in and thinning

nthin = []; %no thinning
nperout = 5000; %number of draws per file.
nburn = 150*5000; %min(max_outputfile)*nperout/2 ; % 405000;

for jj=chains   
    % Load result files
    results(jj).B_vec=[];
    results(jj).Delta_vec=[];
    results(jj).sigmae=[];
    results(jj).sigmaxi=[];
    results(jj).sigmaGamma=[];

    filestem =['./Gibbs_' spec_name '/chain_' num2str(jj) '_'];
for ii=1:max_outputfile(1, jj)        
        currentFile = [filestem num2str(ii)];    
        load(currentFile, 'all_coeff');
if ~isempty(all_coeff.B_vec)
        results(jj).B_vec = [results(jj).B_vec all_coeff.B_vec];
end
if ~isempty(all_coeff.Delta_vec)
results(jj).Delta_vec = [results(jj).Delta_vec all_coeff.Delta_vec];       
end
        results(jj).sigmae = [results(jj).sigmae all_coeff.sigmae];
        results(jj).sigmaxi = [results(jj).sigmaxi all_coeff.sigmaxi];
        results(jj).sigmaGamma = [results(jj).sigmaGamma all_coeff.sigmaGamma];        
end
    
if ~isempty(all_coeff.Delta_vec) && ~isempty(all_coeff.B_vec)
result(:,:,jj) = [results(jj).B_vec(:,nburn+1:end)' results(jj).Delta_vec(:,nburn+1:end)' results(jj).sigmae(:,nburn+1:end)' results(jj).sigmaxi(:,nburn+1:end)' results(jj).sigmaGamma(:,nburn+1:end)'];
elseif ~isempty(all_coeff.Delta_vec) && isempty(all_coeff.B_vec)
result(:,:,jj) = [ results(jj).Delta_vec(:,nburn+1:end)' results(jj).sigmae(:,nburn+1:end)' results(jj).sigmaxi(:,nburn+1:end)' results(jj).sigmaGamma(:,nburn+1:end)'];
elseif isempty(all_coeff.Delta_vec) && ~isempty(all_coeff.B_vec)
result(:,:,jj) = [results(jj).B_vec(:,nburn+1:end)'  results(jj).sigmae(:,nburn+1:end)' results(jj).sigmaxi(:,nburn+1:end)' results(jj).sigmaGamma(:,nburn+1:end)'];
elseif isempty(all_coeff.Delta_vec) && isempty(all_coeff.B_vec)
result(:,:,jj) = [results(jj).sigmae(:,nburn+1:end)' results(jj).sigmaxi(:,nburn+1:end)' results(jj).sigmaGamma(:,nburn+1:end)'];
end
    end

if all==1
  results_all=[]; %zeros(size(result,1)*size(result,3),size(result,2));  
for jj3=chains
  results_all=[results_all; result(:,:,jj3)];
end
result=results_all;
chains=1;
end

fprintf('Finished loading files\n')

%%
% Defining diagnostics to perform
%This can implement any of the following diagnostics: psrf, cpsrf, mpsrf,
%cmpsrf, ipsrf, cipsrf, gbiter (this would consider the first chain only),
%ksstat.

% Results from the respective diagnostic are saved in the structure Diag
% with a field name equal to the name of the function selected.

if diag==1
type_diag{1} = 'psrf';
type_diag{2} = 'ipsrf';
type_diag{3} = 'mpsrf';

nout = [1 1 1];
% Perform diagnostics
%Diag(1) = diagnostics_gibbs(result, type_diag, nout);

for jj=chains;
aux = result(:, :, jj);
Diag(jj) = diagnostics_gibbs(aux, type_diag, nout);  
end

fprintf('Done running diagnostics, next it will write formatted tables\n')
% % Data arranged for trace plots
% 
% %Rearranging results: each column is one parameter.
% 
%if trace==1 
    % results_trace = [];
% for ii=1:max_outputfile
% results_trace = [results_trace; results(:, :, ii)];
% end
% 
% k_betas = size(all_coeff.B_vec, 1); %number of betas
% k_Delta = size(all_coeff.Delta_vec, 1); %number of Deltas
% k_sigma_gamma = size(all_coeff.sigmaGamma, 1); %number of sigmaGamma
% 
% 
% 
% % Perform correlations
% 
% % In this part we get different autocorrelation diagnostics. This can be
% % implemented for: acorr, acorrtime, geyer_icse, geyer_imse.
% 
% % Results from the respective diagnostic are saved in the structure Corr
% % with a field name equal to the name of the function selected.
% 
% corr_diag{1} = 'acorr';
% maxlag = 20;
% 
% Corr = corr_gibbs(results(:, :, 1), corr_diag, maxlag);
% 
% 


%% Add names and export diagnostics

if any(find(ismember(type_diag,'psrf'))) || any(find(ismember(type_diag,'ipsrf')))
    
stud_header = importdata('/proj/pagarwal/parag/demand/Common/students_headers.csv');
prog_header = importdata('/proj/pagarwal/parag/demand/Common/programs_headers.csv');

stud_header.colheaders = [stud_header.colheaders 'Constant'];
Z_name = stud_header.colheaders(1, 1 + eval([spec_name '.z_indices'])); 
X_name_aux = prog_header.colheaders(1,eval([spec_name '.x_indices'])+2);
X_dum = {};
if ~isempty([spec_name '.x_dum_ind'])
   for ii=1:eval(['length(' spec_name '.x_dum_ind)'])
       X_dum = [X_dum (['Dum_' cell2mat(X_name_aux(1,eval([spec_name '.x_dum_ind(1,' num2str(ii) ')'])))]) ];
   end
end
X_name = [X_name_aux X_dum];

X_name_d = X_name_aux(1,eval([spec_name, '.x_delta']));
X_name_d = [X_name_d X_dum];


Sigma_name={};
if ~isempty(eval([spec_name '.x_types']))
kk=1;
jj2=1;
Sigma_name_aux = prog_header.colheaders(1,eval([spec_name '.x_types'])+2);
for ii=1:length(eval([spec_name '.x_types']))
         for ii2=1:kk
		   Sigma_name(jj2,1) =  Sigma_name_aux(ii2);
Sigma_name(jj2,2) =  Sigma_name_aux(kk);
jj2=jj2+1;
    end
    kk=kk+1;
end
end

indexes_Delta = [[ eval([spec_name '.x_delta']) (length(X_name_aux) + [1:size(eval([spec_name '.x_dum_ind']),2)])]' zeros(size(results(jj).Delta_vec,1),1)];

% Put together indices for names and result
k_beta = size(results(jj).B_vec,1);
k_delta = size(results(jj).Delta_vec,1);

for jj=chains

if k_beta>0 
if type_diag{1} == 'psrf' & nout(1,1)==2;
coeff1(:,:,jj) = [eval([spec_name,'.interact'])' Diag(jj).psrf{1,1}(1:k_beta)' Diag(jj).ipsrf{1,1}(1:k_beta)' Diag(jj).psrf{1,2}(1:k_beta)'];
else
coeff1(:,:, jj) = [eval([spec_name,'.interact'])' Diag(jj).psrf{1,1}(1:k_beta)' Diag(jj).ipsrf{1,1}(1:k_beta)'];
end
 else
   coeff1(:,:,jj)=zeros(0,0);
end

if type_diag{1} == 'psrf' & nout(1,1)==2;
coeff2(:, :, jj) = [indexes_Delta Diag(jj).psrf{1,1}(1+k_beta:k_beta+k_delta)' Diag(jj).ipsrf{1,1}(1+k_beta:k_beta+k_delta)' Diag(jj).psrf{1,2}(1+k_beta:k_beta+k_delta)'];
else
coeff2(:, :, jj) = [indexes_Delta Diag(jj).psrf{1,1}(1+k_beta:k_beta+k_delta)' Diag(jj).ipsrf{1,1}(1+k_beta:k_beta+k_delta)'];
end
end


for jj=chains
  if ~isempty(coeff1)
  coeff(:,:,jj) = [coeff1(:,:,jj); coeff2(:,:,jj)];
elseif isempty(coeff1)
coeff(:,:,jj) = coeff2(:,:,jj);
end
coeff(:,:,jj) = sortrows(coeff(:,:,jj) ,[1 2]);
end

fid  =  fopen( ['./tables/Diag_burn' num2str(nburn/1000) 'k_' spec_name 'no_neff.csv'], 'w');


fprintf(fid, ' , , %s, ','All' );
for jj=chains
  fprintf(fid, ' , %s, ', ['Chain ' num2str(jj)]);
end

  fprintf(fid, '\n');
fprintf(fid, '%s, %s, ', 'Variable 1', 'Variable 2');
for jj= chains
  if type_diag{1} == 'psrf' & nout(1,1)==2;
fprintf(fid, '%s, %s, %s, ', 'psrf', 'ipsrf', 'neff');
else
fprintf(fid, '%s, %s, ', 'psrf', 'ipsrf');
end
end
fprintf(fid, '\n');



for ii=1:k_beta+k_delta
        
	 fprintf(fid, '%s, ', cell2mat(X_name(1,coeff(ii,1,1))));
    if coeff(ii,2)~=0
      fprintf(fid, '%s, ', cell2mat(Z_name(1,coeff(ii,2,1))));
    else
    fprintf(fid, '%s, ', 'Main Effect');
    end
    for jj=chains
      fprintf(fid, '%1.4f, ', coeff(ii,3,jj));
fprintf(fid, '%1.4f, ', coeff(ii,4,jj));
if type_diag{1} == 'psrf' & nout(1,1)==2;
fprintf(fid, '%1.0f, ', coeff(ii,5,jj));
end
    end
    
    fprintf(fid, '\n');
end


ii= ii +1;
fprintf(fid, '%s, ', 'Sigmae');
fprintf(fid, '%s, ', '');
for jj=chains
  fprintf(fid, '%1.4f, ', Diag(jj).psrf{1,1}(ii));
fprintf(fid, '%1.4f, ', Diag(jj).ipsrf{1,1}(ii));
if type_diag{1} == 'psrf' & nout(1,1)==2;
fprintf(fid, '%1.0f, ', Diag(jj).psrf{1,2}(ii));
end
end
fprintf(fid, '\n');

ii= ii +1;
fprintf(fid, '%s, ', 'Sigmaxi');
fprintf(fid, '%s, ', '');    
for jj=chains
  fprintf(fid, '%1.4f, ', Diag(jj).psrf{1,1}(ii));
fprintf(fid, '%1.4f, ', Diag(jj).ipsrf{1,1}(ii));
if type_diag{1} == 'psrf' & nout(1,1)==2;
fprintf(fid, '%1.0f, ', Diag(jj).psrf{1,2}(ii));
end
end
fprintf(fid, '\n');

if ~isempty(Sigma_name)
fprintf(fid, '%s, ', 'Sigma Gamma');
fprintf(fid, '\n');
end



for ii=k_beta+k_delta+3:k_beta+k_delta+2+size(results(1).sigmaGamma,1)
    
    ii2=ii-k_beta-k_delta-2;
    fprintf(fid, '%s, ', cell2mat(Sigma_name(ii2,1)));
    fprintf(fid, '%s, ', cell2mat(Sigma_name(ii2,2)));

for jj=chains
  fprintf(fid, '%1.4f, ', Diag(jj).psrf{1,1}(ii));
fprintf(fid, '%1.4f, ', Diag(jj).ipsrf{1,1}(ii));
if type_diag{1} == 'psrf' & nout(1,1)==2;
fprintf(fid, '%1.4f, ', Diag(jj).psrf{1,2}(ii))
end

end
    fprintf(fid, '\n');
end


fprintf(fid, '%s, ,', 'MPSRF');
for jj=chains
fprintf(fid, '%1.4f, , ', Diag(jj).mpsrf{1,1}(1,1));
end  
fprintf(fid, '\n');


N=max_outputfile(1,1)*nperout-nburn;
fprintf(fid, '%s, ', 'N');
fprintf(fid, '%1.0f, ', N);
fprintf(fid, '\n');
fprintf(fid, '%s, ', 'Burn-in');
fprintf(fid, '%1.0f, ', nburn);  
fprintf(fid, '\n');

fclose(fid);



end

end
end




%Histogram

if histo==1
stud_header = importdata('students_headers.csv');
prog_header = importdata('programs_headers.csv');

stud_header.colheaders = [stud_header.colheaders 'Constant'];
Z_name = stud_header.colheaders(1, 1 + eval([spec_name '.z_indices']));
X_name_aux = prog_header.colheaders(1,eval([spec_name '.x_indices'])+2);

for jj=1:size(result,2)
fig=figure;
set(fig,'Visible','off');
histogram(result(:,jj));

print(fig,'-depsc' , ['./figures/hist/spec6_' num2str(iter) '_coef' num2str(jj)]);

end
end

%end


%end


%Trace

if trace==1
stud_header = importdata('students_headers.csv');
prog_header = importdata('programs_headers.csv');

stud_header.colheaders = [stud_header.colheaders 'Constant'];
Z_name = stud_header.colheaders(1, 1 + eval([spec_name '.z_indices']));
X_name_aux = prog_header.colheaders(1,eval([spec_name '.x_indices'])+2);

X_dum = {};
if ~isempty([spec_name '.x_dum_ind'])
for ii=1:eval(['length(' spec_name '.x_dum_ind)'])
	 X_dum = [X_dum (['Dum_' cell2mat(X_name_aux(1,eval([spec_name '.x_dum_ind(1,' num2str(ii) ')'])))]) ];
end
end
         %end
   X_name = [X_name_aux X_dum];

X_name_d = X_name_aux(1,eval([spec_name, '.x_delta']));
X_name_d = [X_name_d X_dum];

 
for ii2=chains
  for jj=1:size(results(ii2).Delta_vec,1)
	      fig=figure;
set(fig, 'Visible', 'off');
plot(results(ii2).Delta_vec(jj,1+nburn:end));
print(fig, '-dpdf', ['./figures/' cell2mat(X_name_d(jj)) '_chain_' ...
                    num2str(ii2) '_' spec_name ]);
end

for jj=1:size(results(ii2).B_vec,1)
	 fig=figure;
set(fig, 'Visible', 'off');
plot(results(ii2).B_vec(jj,1+nburn:end));
print(fig, '-dpdf', ['./figures/int_' cell2mat(X_name(eval([spec_name ...
                    '.interact(1,' num2str(jj) ')']))) '_' ...
                    cell2mat(Z_name(eval([spec_name '.interact(2,' num2str(jj) ')']))) '_chain_' num2str(ii2)]);
end

fig=figure;
set(fig, 'Visible', 'off');
    plot(results(ii2).sigmae(1+nburn:end));
    print(fig, '-dpdf', ['./figures/sigmae_chain_' num2str(ii2), '_' spec_name]);
 
        
fig=figure;
set(fig, 'Visible', 'off');
plot(results(ii2).sigmaxi(1+nburn:end));
print(fig, '-dpdf', ['./figures/sigmaxi_chain_' num2str(ii2), '_' spec_name]);
    end
end

%problem=find(Diag.psrf{1}>1.2);
%for jj=[problem]
%fig=figure;
%set(fig,'Visible','off');
%plot(result(:,jj));
%print(fig,'-depsc' , ['./figures/' cell2mat(X_name(eval([spec_name '.interact(1,' num2str(jj) ')']))) cell2mat(Z_name(eval([spec_name '.interact(2,' num2str(jj) ')'])) )  ]);
%end
