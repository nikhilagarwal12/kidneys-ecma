% This patient and donor sample generates the main counterfactual results
% (Tables 6, 7, and Panels A,B,D of C.5)

% Sample
cf_sample               = struct();
cf_sample.donors        = 500;
cf_sample.histories     = 300;
cf_sample.rng           = 0;
cf_sample.single_hist   = false;                          % Only allow one history per patient
cf_sample.max_T         = 100*365.25;% Death date
cf_sample.pd_per_year   = 4;         % Periods per year;
cf_sample.t             = 1:365.25/cf_sample.pd_per_year:cf_sample.max_T; % Once a quarter, 100 years forward

% splits
cf_sample.pt_split_by{1}    = {'init_age',@(x) x<median(x)};
cf_sample.pt_split_by{2}    = {'init_age',@(x) true(size(x))};          % of the reminder;    
cf_sample.don_split_by{1}   = {'age_don',@(x) x<median(x)};
cf_sample.don_split_by{2}   = {'age_don',@(x) true(size(x))};      % of the reminder;
cf_sample.pool_name{1}      = 'Young Donor Pool';
cf_sample.pool_name{2}      = 'Old Donor Pool';

% Options
cf_sample.options = struct(); % This options will be passed to gen_cf_data (default is empty)
cf_sample.options.chi_times    = (0:4)*cf_sample.pd_per_year+1; 
% Other optional fields in cf_sample (default to model values in gen_sample_cfs):
% cf_sample.rho;
% cf_sample.lambda;
% cf_sample.pt_arrival_rate;
