********************************************************************************************
* This script takes the raw data and generates primary data files we use for future analysis
* It is a wrapper script that calls a lot of sub-scripts and sub-function. 
* 
* Run this script from within the gen_primary_files/code/ subdirectory so the relative paths 
* work out
********************************************************************************************

clear all
set more off
 
local mechanism = "../../mechanism/input/"
local ctr_dsa_def = "../../data_preparation/ctr_dsa_def/"

// data folders
local offer_data = "~/organs-raw/raw-extract-3/ki_ptr/"
local kidpan_data = "~/organs-raw/raw-extract-3/STAR/Kidney_ Pancreas_ Kidney-Pancreas/"
local liver_data = "~/organs-raw/raw-extract-3/STAR/Liver/"
local intestine_data = "~/organs-raw/raw-extract-3/STAR/Intestine/"
local lung_data = "~/organs-raw/raw-extract-3/STAR/Thoracic/"
local wlhistory_data = "`kidpan_data'Waiting List History/"
local antigen_data = "~/organs-raw/raw-extract-4/Non-Std/"
local non_std_4_data = "~/organs-raw/raw-extract-4/Non-Std/"
local points_data = "~/organs-raw/raw-extract-4/ki_ptr/"
local deceased_donor_data = "~/organs-raw/raw-extract-3/STAR/Deceased Donor/"
local location_data = "~/organs-raw/raw-extract-2/"  // using raw-extract-2, not -3, because -3 is missing listing centers for some patients

// output paths
local primary_files = "~/organs-star/primary_files/"  // if we're just generating a test sample, this must be changed to the test sample folder
local test_sample_files = "`primary_files'test_sample/"
local auxiliary_files = "~/organs-star/primary_files/auxiliary_files/"
local hazard_output = "/proj/organs/common_output/reduced_form/hazard"    // output from hazard model for waitlist departure
    
// Control which parts of the script to run
local create_ctr_dsa_def = 1
local preliminary = 1
local define_sample = 1
local find_addtl_hla = 1
local kidpan = 1
local patient = 1
local kidpan_donor = 1
local patient_history = 1
local deceased_donor = 1
local points = 1
local offers = 1
local impute_missing_dr = 1
local matlab = 1   // up to here all files are created for the nationwide sample (the NYRT files are not created)

local nyrt = 1      // creates NYRT subsamples and writes to .txt files
local append_nyrt_dep_rates_nyrt = 1
local simulate_mechanism_nyrt = 1
local append_nyrt_n_offers_nyrt = 1
local document_sample_selection = 1

local cross_validation = 1     // cross-validation exercise reported in the online appendix
local simulate_mechanism_xval = 1


    
if `impute_missing_dr'==1{ // regenerate deceased donors file if imputing missing DR antigens
    local deceased_donor = 1
}
if `deceased_donor'==1{
    local impute_missing_dr = 1
}

// Whether to output full version, test sample, or both
//  5. To generate BOTH full and test samples, set `full_sample' and `test_sample' to 1, and set the `..._date' locals appropriately
//     To generate ONLY a full sample, set `full_sample' = 1 and `test_sample' = 0
//     To generate ONLY a test sample:
//       (a) Change the `primary_files' local to the test_sample subfolder
//       (b) Change `sample_start_date' and `sample_end_date' to the test sample dates
//       (c) Set `full_sample' = 1 and `test_sample' = 0
local full_sample = 1
local test_sample = 0

// sample dates
local sample_start_date = mdy(1,1,2010)  // dates for full (nationwide) sample
local sample_end_date = .  // set to "." if no end
local nyrt_start_date = mdy(1,1,2010)    // dates for NYRT sample used in main analysis
local nyrt_end_date = mdy(12,31,2013)
local test_sample_start_date = mdy(1,8,2010)
local test_sample_end_date = mdy(5,7,2010)
local cross_start_date = mdy(1,1,2014)   // dates for cross-validation sample
local cross_end_date = mdy(6,30,2014)

  // special case for only generating test sample: act as though we're generating the full sample but change sample dates and primary files folder
  if `test_sample'==1 & `full_sample'==0{

      local primary_files = "`test_sample_files'"
      local test_sample = 0
      local full_sample = 1
      local sample_start_date = `test_sample_start_date'
      local sample_end_date = `test_sample_end_date'
  }

sysdir set PERSONAL ../../tools/code/    // look for .ado files here


**********************************************************
* GENERATE PRIMARY FILES AT NATIONAL LEVEL
**********************************************************
// (-1) Define sample variables and date range
include ../../data_preparation/code/define_sample_period_and_vars.do // Change parameters of data definitions here


// (0) Define sample of donors, patients, and offers, and import variables relevant to mechanism
if `create_ctr_dsa_def'==1{
  include ../ctr_dsa_def/extract_ctr_dsa_def.do
  include ../ctr_dsa_def/ctr_dsa_def.do
  include ../ctr_dsa_def/bridges_new.do
}
    
if `preliminary'==1{
  include ../../data_preparation/code/import_mechanism_vars.do
}

if `define_sample' == 1{
  include ../../data_preparation/code/define_primary_files_sample.do  
}

* Some deceased donors are not in KIDPAN, so we get their HLA variables from transplants of other organ types
if `find_addtl_hla'==1{
  include ../../data_preparation/code/find_missing_donor_antigens.do
}


// (1) Files based on KIDPAN: KIDPAN, patient, and donor files
if `kidpan'==1 | `patient'==1 | `kidpan_donor'==1{
  include ../../data_preparation/code/kidpan_based_files.do
}

// (2) Patient history file: sample wlreg_audit_id records
if `patient_history'==1{
  include ../../data_preparation/code/patient_history_file.do
}


// (3) Deceased donors file: donor characteristics from DECEASED_DONOR_DATA
if `deceased_donor'==1{
  include ../../data_preparation/code/deceased_donor_file.do
}


// (4) Points and Offer files
* Points given to each offer, by source
if `points'==1{
  include ../../data_preparation/code/points_file.do
}

* Offers with key patient, donor, and match characteristics
if `offers'==1{
  include ../../data_preparation/code/offer_file.do
}


// (4') If needed, impute missing donor DR antigens using offer data
if `impute_missing_dr'==1{
  include ../../tools/code/impute_dr_antigens_when_missing.do
}

// (5) Format Files for MATLAB
if `matlab'==1{
  local file_dir = "`primary_files'"
  local just_patients = 0
  include ../../data_preparation/code/format_text_files.do
}

**********************************************************
* SUBSET TO PRIMARY FILES AT NYRT LEVEL
**********************************************************

// (6) Subsample files - NYRT
if `nyrt'==1{
  local file_dir = "`primary_files'nyrt/"
  local data_files = "`primary_files'"
  local test_sample = 0
  include ../../data_preparation/code/count_ki_available_to_nyrt.do
  include ../../data_preparation/code/nyrt_subsample.do
  include ../../data_preparation/code/gen_subsample_files.do
}

// (7) NYRT Departure Rates
if `append_nyrt_dep_rates_nyrt' == 1{
    * Calculate departure hazard rates
    local file_dir = "`primary_files'nyrt/"
    local test_sample = 0
    include ../../data_preparation/code/calc_nyrt_dep_rates.do
    include ../../data_preparation/code/format_text_files.do
}


// （9） Simulate mechanism to determine offers refused by pre-set screening criteria
if `simulate_mechanism_nyrt'==1{
    local filter = "NYRT"
    include ../../mechanism/code/fix_init_date.do
    include ../../mechanism/code/fix_donor_screen.do
    include ../../mechanism/code/simulate_offers.do
}


// (8) NYRT Offer Rates
if `append_nyrt_n_offers_nyrt' == 1{
    * Calculate n_offers
    local file_dir = "`primary_files'nyrt/"
    local test_sample = 0
    include ../../data_preparation/code/n_offer_censor.do
    include ../../data_preparation/code/format_text_files.do
}

// (10) document_sample_selection
if `document_sample_selection' == 1{
    local filter = "NYRT"
    include ../../data_preparation/code/document_sample_selection.do
}

***************************************************************
* GENERATE PRIMARY FILES AT NYRT LEVEL FOR CROSS VALIDATION
***************************************************************
// (11) Subsample files - cross validation period
if `cross_validation'==1{   // save post- data for cross-validation exercise
  local file_dir = "`primary_files'cross_validation/nyrt/"
  local data_files = "`primary_files'"
  local test_sample = 0
  local orig_nyrt_start = `nyrt_start_date'
  local orig_nyrt_end = `nyrt_end_date'
  local nyrt_start_date = `cross_start_date'
  local nyrt_end_date = `cross_end_date'

  include ../../data_preparation/code/count_ki_available_to_nyrt.do
  include ../../data_preparation/code/nyrt_subsample.do
  include ../../data_preparation/code/gen_subsample_files.do
  
  local nyrt_start_date = `orig_nyrt_start'
  local nyrt_end_date = `orig_nyrt_end'
}

// (12) Simulate mechanism to determine offers refused by pre-set screening criteria in cross validation period
if `simulate_mechanism_xval'==1{
    local filter = "NYRT-Validate"   
    include ../../mechanism/code/simulate_offers.do
}

// update permissions on written files
shell chmod -R 774 `primary_files'
