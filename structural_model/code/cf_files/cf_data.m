classdef cf_data
    properties
        proj = [];
        calc  = [];    
        opt_assignment = [];
        pr_compat = [];
        V_init = [];
        bins = [];
        points = [];
        kas_bins = [];
        kas_points =[];
        donor_options =[];
        pt_entry_weights =[];
        q_leave_exog = [];
        p_leave_in_pd = []
        options = [];
        filenames =[];
        lyft = [];
        donor_id =[];
        pt_hist_id =[];
        pt_split_by = [];
        don_split_by = [];
        pool_name = [];
        wait_time = [];
        V_ref_flow  = [];
        V_ref_entry = [];
        V_ref_dec = [];
        % Properties to keep previous functionality (not added to subset) 
        Gamma_eta = [];
        V_fact  = []; 
        optimal_ss_allocation = [];
        % When adding properties: remember to add it also to the subset function.
        
    end  % properties
    
    
    methods 
        % A. General Methods
            % cf_data: constructor
            % Gamma_eta: Allow for obj.Gamma_eta 
            % postEntry: is this patient/time/[donor] after patient entry
            % feasible: postEntry * pr_compat>0
            
        % B. Generate Mechanisms
        
        % C. Generate splits
        
        % D. Generate social planner problems
            % gen_optimal_ss_allocation
        
        % A. General Methods
        function obj = cf_data(arg1)   % Constructor Function
            if nargin == 1 && isa(arg1,'struct')
                % If argument is a structure convert all fields to properties
                 for fn = fieldnames(arg1)'    %enumerat fields
                    try
                        obj.(fn{1}) = arg1.(fn{1});   %and copy
                    catch
                        warning('Could not copy field %s', fn{1});
                    end
                end
            end
        end   % end cf_data (Constructor)
        
        function s = saveobj(obj)   % Saving behavior
            s = struct(obj);
        end

        % Logical function if this cell is after entry (default 3 dimensional array)
        function mat = postEntry(obj,dimensions),
            mat = cumsum(obj.pt_entry_weights>0,2);
            if nargin == 1 || dimensions == 3
                mat = repmat(mat,1,1,numel(obj.donor_id));
            end
        end
        
        % Is this donor available for this patient at this time?
        function mat = feasible(obj)
            mat = bsxfun(@times,obj.postEntry,obj.pr_compat>0);
        end
        
        function mat = blackout(obj,per)
            mat = (cumsum(obj.postEntry,2)>per)*1;
        end

        % Mechanisms aliases and functions
        function score = gamma(obj,bins,blackout)
            score = bsxfun(@times,obj.calc.Gamma,1./obj.V_ref_flow);
            score = score +log(obj.feasible);
            if nargin>2, score = score + log(blackout(obj,blackout)); end
            if bins<size(score,1)
                score = score_xtile(score,bins);
            end
        end
        
        function score = gammaeta(obj,bins,blackout)
            score = bsxfun(@times,obj.calc.Gamma_eta,1./obj.V_ref_flow);
            score = score +log(obj.feasible);
            if nargin>2, score = score + log(blackout(obj,blackout)); end
            if bins<size(score,1)
                score = score_xtile(score,bins);
            end
        end
        
        % Few compatible donors
        function obj = delete_histories_with_few_compatible_donors(object,max_num_histories)
            include = squeeze(sum(object.pr_compat>0.01,3))>0;
            obj = object.subset(include & cumsum(include)<=max_num_histories,[],[]);
        end

        % Splits

        function obj = subset(object,where_pt_hist,where_T,where_don)
                [N_pat,N_time,N_don] = size(object.proj.Gamma_eta);
                if nargin < 4 || isempty(where_don), where_don = true(N_don,1); end
                if nargin < 3 || isempty(where_T), where_T = true(N_time,1); end
                if nargin < 2 || isempty(where_pt_hist), where_pt_hist = true(N_pat,1); end
                % Donors and patients
                obj.proj.Gamma_eta = object.proj.Gamma_eta(where_pt_hist,where_T,where_don);
                obj.proj.V         = object.proj.V(where_pt_hist,where_T);
                obj.calc.Gamma_eta = object.calc.Gamma_eta(where_pt_hist,where_T,where_don);
                obj.calc.Gamma     = object.calc.Gamma(where_pt_hist,where_T,where_don);
                obj.calc.V         = object.calc.V(where_pt_hist,where_T);
                obj.opt_assignment.chi = object.opt_assignment.chi(where_pt_hist,:,where_don,:);
                obj.opt_assignment.offset_gamma = object.opt_assignment.offset_gamma(where_pt_hist,:);
                obj.pr_compat      = object.pr_compat(where_pt_hist,1,where_don);
                obj.V_init         = object.V_init(where_pt_hist);
                if ~isempty(object.V_ref_entry)
                    obj.V_ref_entry = object.V_ref_entry(where_pt_hist);  % It can be empty it V_ref depends on cf outcomes
                end
                if ~isempty(object.V_ref_flow)
                    obj.V_ref_flow = object.V_ref_flow(where_pt_hist,:);  % It can be empty it Vp_ref depends on cf outcomes
                end
                obj.bins           = object.bins(where_pt_hist,where_T,where_don);
                obj.points         = object.points(where_pt_hist,where_T,where_don);
                obj.kas_bins       = object.kas_bins(where_pt_hist,where_T,where_don);
                obj.kas_points     = object.kas_points(where_pt_hist,where_T,where_don);
                obj.lyft           = object.lyft(where_pt_hist,where_T,where_don);
                % Just Patients
                obj.pt_hist_id     = object.pt_hist_id(where_pt_hist);
                obj.pt_entry_weights  = object.pt_entry_weights(where_pt_hist,where_T);
                obj.q_leave_exog      = object.q_leave_exog(where_pt_hist,where_T);
                obj.wait_time         = object.wait_time(where_pt_hist,where_T);
                % Just donors
                obj.donor_id       = object.donor_id(where_don);
                obj.donor_options.N_ki_don  = object.donor_options.N_ki_don(where_don);
                obj.donor_options.offers.mean_log  = object.donor_options.offers.mean_log(where_don);
                obj.donor_options.offers.sd_log    = object.donor_options.offers.sd_log(where_don);
                obj.donor_options.offers.timeout_hazard = object.donor_options.offers.timeout_hazard(where_don);
                obj.donor_options.offers.p_discard = object.donor_options.offers.p_discard(where_don);
                obj.donor_options.offers.local_donor = object.donor_options.offers.local_donor(where_don);
                obj.filenames      = object.filenames;
                obj.options        = object.options;
                obj.options.pt_arrival_rate = object.options.pt_arrival_rate*(numel(obj.pt_hist_id)/numel(object.pt_hist_id));
                obj.options.lambda          = object.options.lambda*(numel(obj.donor_id)/numel(object.donor_id));
                obj.options.t               = object.options.t(where_T);
                obj = cf_data(obj);
        end
    
        function obj = update_V_ref(obj,V_ref,V_ref_dec)  % Updates obj.V_ref_entry and obj_V_ref_flow;
            [~,where] = ismember(obj.pt_hist_id,V_ref.pt_history);
            obj.V_ref_entry = V_ref.V(where);
            obj.V_ref_flow = V_ref.flow(where,:);
            if any(isnan(obj.V_ref_flow))
                fprintf('Split %d has %d NaNs in V_ref_flow.\n',ii,sum(isnan(obj.V_ref)));
                fprintf('Next line: obj.V_ref_flow(isnan(obj.V_ref_flow)) = max(V_ref_flow.V)\n');
                keyboard;
                obj.V_ref_flow(isnan(obj.V_ref_flow)) = max(V_ref_flow.V);
            end
            if nargin>2
                obj.V_ref_dec.eps_ref = V_ref_dec.eps_ref(where,:);
                obj.V_ref_dec.mu_ref = V_ref_dec.mu_ref(where,:);
            end
        end
        
        function offset = compatible_offerset(obj,match_characteristics,pt_history,patients,donors)
            % match_characteristics is a sparsedata object: time_till
            
            % Define the set of offers
            [where_patient,where_donors]= find(squeeze(obj.pr_compat>0));
            offers.wlreg_audit_id_code = obj.pt_hist_id(where_patient);
            offers.donor_id = obj.donor_id(where_donors);
            % Get offer characteristics
            offset = offerset(match_characteristics,pt_history,patients,donors,offers);
            % Do not Exclude offers:  off = off.excludeBin0;
        end
        
   end  % methods
        
   methods(Static)
       function obj = loadobj(s)   % Loading behavior
          if isstruct(s)
            for old = {'V_rep_offer','V_ref'}
             % Load old versions of cf_data
             if isfield(s,old{1}) && ~isfield(s,'V_ref_entry')
                fprintf('You are loading an old version of cf_data.\n')
                fprintf('The loading version has a field called %s.\n',old{1})
                fprintf('The current version of cf_data requires V_ref_entry.\n')
                fprintf('Verify that it is OK to rename the current %s\n',old{1})
                fprintf('by V_ref_entry and type dbcont.\n')
                keyboard;  % This piece of code will be deprecated later 
                s.V_ref_entry = s.(old{1});
             end
            end
            obj = cf_data(s);
          else
             obj = s;
          end
       end
    end % static methods
end

