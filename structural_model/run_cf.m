%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% READ ME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%:
% This script needs to be run multiple times, each with different settings. 

% -------------------
% Main specification
% -------------------
% Step 1 (current status of the script puts you here): 
%         set cf_tasks.gen_data and cf_tasks.compute_ss to true;
%         set other cf_tasks to false.
%         This run will run all score_* counterfactuals, and set up the optim_* ones
% Step 2: 
%         set cf_tasks.compute_ss and cf_tasks.optimize to true;
%         set other cf_tasks to false.
%         This run will run all optim_* counterfactuals
% Step 3: 
%         set cf_tasks.compute_ss and cf_tasks.counterfactual_table to true;
%         set other cf_tasks to false.
%         This run will run all greedy_* counterfactuals
%
% ---------------------------------------------------------
% Robustness Check 1:  Patient Unobserved Heterogeneity
% ---------------------------------------------------------
% See "Robustness Check 1 -  Patient Unobserved Heterogeneity" below for settings
% Then repeat Steps 1-3 from above
%
% ---------------------------------------------------------
% Robustness Check 2: Larger Patient and Donor Type Space
% ---------------------------------------------------------
% See "Robustness Check 2 - Annual Discount Factor of 10 Percent" below for settings
% Then repeat Steps 1-3 from above
%
% ---------------------------------------------------------
% Robustness Check 3: Annual Discount Factor of 10 Percent
% ---------------------------------------------------------
% See "Robustness Check 3 - Larger Patient and Donor Type Space" below for settings
% Then repeat Steps 1-3 from above
%
% ---------------------------------------------------------
% Robustness Check 4 - No Limit on Maximum Number of Offers
% ---------------------------------------------------------
% See "Robustness Check 4 - No Limit on Maximum Number of Offers" below for settings 
% Then repeat Steps 1-3 from above
%
% end of READ ME
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Clear workspace, add paths and define target folders;
clearvars; add_paths;
%% Specify Options here (see main_options.m for available options)
options = main_options();

%% Specification
% Empirical model (check run_model.m for model names)
model_name = 'nyrt_fix_time_uh'
% Sample selection for the counterfactuals (check *sample_* files in cf_files)
cf_options.sample_name             = 'sample0';

%% Collections of counterfactual types (check mech_aliases.m for
%% acceptable inputs)

base_cf_mech_list                    = {'score_binspoints_locl_ref',...
                                        'score_kas',...
                                        'score_fifo',...
                                        'score_lifo',...
                                        'optim_second',...
                                        'greedy_second',...
                                        'greedy_secondC',...
                                        'optim_first',...
                                        'score_gammaeta',...
                                        'score_binspoints_fact_locl',...
                                        'score_kas_fact',...
                                        'score_fifo_fact',...
                                        'score_lifo_fact', ...
                                        'score_gammaeta_fact',...
                                        'optim_secondC'};

cf_options.add2tablename = '';        % Empty for baseline, 


% Robustness Check 1 -  Patient Unobserved Heterogeneity
% Uncomment the next line: 
%model_name = 'nyrt_fix_time_pt_don_uh'

% Robustness Check 2 - Annual Discount Factor of 10 Percent
% Uncomment the next line: 
%options.override_rho = -log(0.9)/365;

% Robustness Check 3 - Larger Patient and Donor Type Space
% Uncomment the next line: 
%cf_options.sample_name = 'sample0big';

% Robustness Check 4 - No Limit on Maximum Number of Offers
%cf_options.add2tablename           = '_notimeout'
%base_cf_mech_list = strcat(base_cf_mech_list, '_notimeout');


%% Table and cf lists
cf_mech_list                        = base_cf_mech_list;
cf_mech_list_table                  = base_cf_mech_list(1:(end-1));


%% Basic Computation
cf_tasks.gen_data                  = true;    % true in run # 1 and then set it to false
cf_tasks.compute_ss                = true;    % true in all runs
cf_tasks.optimize                  = false;   % true in run #2 (false in other runs)
cf_tasks.counterfactual_table      = false;   % true in run # 3 (table will be incomplete unless all runs are complete)

%% Tools for debugging and developing the code
cf_options.add_run_name            = '';  % Save modification separately with this suffix

cf_tasks.export2anil               = false; % Only relevant in supply.mit.edu; 
                                            % Instead of running the problem, it copies to Anil's folder;

options.resume_cfs                 = false; % Resume from last run
options.run_splits                 = 1;   % enter split number (It's always 1)
options.overwrite_soln             = 1;   % Are we willing to overwrite the old solution?
options.profile                    = '';  % Name of the folder where the profile results will be stored ('' if no profile)

% Detect host machine
[~, hostname] = system('hostname');

% Define the run_name and file paths
cf_options.run_name = [options.cf_suffix([]) cf_options.add_run_name];

data_filepath = [model_data_folder      model_name '/' cf_options.sample_name cf_options.run_name];
rslt_filepath = [options.results_folder model_name '/' cf_options.sample_name cf_options.run_name];
if isempty(strfind(hostname,'optimal')), 
  % modify path of proj folder to run on knitro server (this issue is specific to our environment)
    rslt_filepath = strrep(rslt_filepath,'proj/organs','proj/pagarwal/organs'); 
end
% If only running the "optim" counterfactuals (run #2)
if cf_tasks.optimize
    cf_mech_list  = {'optim_second','optim_first','optim_secondC'};
end
 
% Run main_cf
main_cf

