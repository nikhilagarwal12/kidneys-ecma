// corrects existing bins and points (pre-KAS) for DSA variances
// should be run on a file in the format of offers.dta (~/organs-star/primary_files/)


program variances_preKAS
syntax [,Binsonly Descrip]

local do_points = ~("`binsonly'" == "binsonly")
if `do_points' di "Do Points" 
else di "Do not do Points"
local use_descript = "`descrip'" == "descrip"
if `use_descript' di "Use Descrip" 
else di "Do not use Descrip"


tempvar thisblock highest_scoring mandatory in_master_file unsensitized incompatible_paired_recipient wait_dialysis
gen flag_bin = 0


replace cpra = -1 if cpra==.
replace dialysis_date = init_date if dialysis_date ==. // check this!

// Highest Scoring CPRA?

gen variance = "None"
// Create Statewide Variable
if `use_descript' {
	replace Geo = "Statewide" if Geo_detail == "Statewide"
	gen `highest_scoring' = highest_pra
	gen `mandatory' = mandatory
	gen `in_master_file' = masterfile
	gen `incompatible_paired_recipient' = incom_liv_don_pair
	gen `unsensitized' = unsensitized
	}
else {
	replace Geo = "Statewide" if Geo == "Regional" & substr(run_opo_id,1,2) == substr(listing_ctr_dsa_id,1,2) & (substr(run_opo_id,1,2) == "FL" | substr(run_opo_id,1,2) == "TN") 
	gen `highest_scoring' = strpos(descrip,"Highest")>0
	gen `mandatory' = (bmis+drmis < 2 | (cpra >0.395 & cpra~=.) | (cmis == 0 & drmis ==0) | unos_cand_sta== 4060) & (Geo == "Statewide" | Geo == "Local")
	gen `in_master_file' = wlreg_in_master == 1
	gen `incompatible_paired_recipient' = 0
	gen `unsensitized' = cpra<0.295
}

di "Variance: State of Florida"
replace variance = "FL" if substr(run_opo_id,1,2)=="FL"
// Statewide 0 ABDR mismatch are processed right after local irrespective of CPRA
gen `thisblock' = substr(run_opo_id,1,2)=="FL" &  (donor_cat == "young_healthy" | donor_cat == "old_healthy" | donor_cat=="ecd") 
replace Bin = 1.9 if `thisblock' & ABDR=="Zero" & ABO == "Identical" & Geo == "Statewide" //There is only a statewide 0 ABRD mismatch sharing.
replace Bin = 14.9 if `thisblock' & ABDR=="Zero" & ABO == "O Donor B Candidate"  & Geo == "Statewide" & Bin> 14
replace Bin = 27.9 if `thisblock' & ABDR=="Zero" & ABO == "Compatible"  & Geo == "Statewide" & Bin> 27
replace Bin = 40.9 if `thisblock' & prior_living_donor & Geo == "Statewide" & Bin> 40

replace Bin = Bin*10 if  substr(run_opo_id,1,2)=="FL"
replace Bin = Bin - 5 if substr(run_opo_id,1,2)=="FL" & Geo == "Statewide" // Put Statewide before Regional

replace `thisblock' = substr(run_opo_id,1,2)=="FL" 
replace Bin = 441 if `thisblock' & Bin == 445 & donor_cat == "young_healthy" // The Statewide block should be run before the Regional block
replace Bin = 442 if `thisblock' & Bin == 455 & donor_cat == "young_healthy"
replace Bin = 443 if `thisblock' & Bin == 465 & donor_cat == "young_healthy" 
replace Bin = 81 if `thisblock' & Bin == 85 & ABDR~="Zero" & donor_cat == "young_dcd"
replace Bin = 82 if `thisblock' & Bin == 95 & ABDR~="Zero" & donor_cat == "young_dcd"
replace Bin = 83 if `thisblock' & Bin == 105 & ABDR~="Zero" & donor_cat == "young_dcd"
replace Bin = 31 if `thisblock' & (Bin == 85 | Bin ==95 | Bin ==105) & ABDR=="Zero" & donor_cat == "young_dcd"
replace Bin = 21 if `thisblock' & Bin == 31 & ABO == "O Donor B Candidate" & donor_cat == "young_dcd"
replace Bin = 11 if `thisblock' & Bin == 31 & ABO == "Identical" & donor_cat == "young_dcd"

replace Bin = 31 if `thisblock' & Bin == 65 & ABDR=="Zero" & donor_cat == "old_dcd"
replace Bin = 21 if `thisblock' & Bin == 31 & ABO == "O Donor B Candidate" & donor_cat == "old_dcd"
replace Bin = 11 if `thisblock' & Bin == 31 & ABO == "Identical" & donor_cat == "old_dcd"


di "Variance: State of Tennessee"
if `do_points' gen p_urgency = 0

replace variance="TN" if  substr(run_opo_id,1,2) == "TN"


// Bins
//Young_healthy, Old_healthy and ecd
replace `thisblock' = substr(run_opo_id,1,2) == "TN" &  (donor_cat == "young_healthy" | donor_cat == "old_healthy" | donor_cat=="ecd") 
replace Bin = 1 if `thisblock' & ABDR=="Zero" & ABO == "Identical" & Geo == "Statewide" //There is only a statewide 0 ABRD mismatch sharing.
replace Bin = 14 if `thisblock' & ABDR=="Zero" & ABO == "O Donor B Candidate"  & Geo == "Statewide" & Bin> 14
replace Bin = 27 if `thisblock' & ABDR=="Zero" & ABO == "Compatible"  & Geo == "Statewide" & Bin> 27
replace Bin = 40 if `thisblock' & prior_living_donor & Geo == "Statewide" & Bin> 40
replace Bin = Bin*10 if `thisblock' & Bin > 40 
replace `thisblock' =  substr(run_opo_id,1,2) == "TN" & (donor_cat == "young_healthy")
replace Bin = 410 if `thisblock' & `mandatory' & `highest_scoring' & Bin> 410
replace Bin = 411 if `thisblock' & `mandatory' & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")  & (Geo == "Statewide" | Geo == "Local") & Bin> 411
replace Bin = 412 if `thisblock' & ~`mandatory' & `highest_scoring' & Geo == "Local" & Bin> 412
replace Bin = 413 if `thisblock' & ~`mandatory' & `highest_scoring' & Geo == "Statewide" & Bin> 413
replace Bin = 420 if `thisblock' & ~`mandatory' & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing") & Geo == "Local" & Bin> 420
replace Bin = 421 if `thisblock' & ~`mandatory' & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing") & Geo == "Statewide" & Bin> 421
replace Bin = 439 if `thisblock' & `mandatory' & Bin> 439
replace Bin = 441 if `thisblock' & ~`mandatory' & Geo == "Statewide" & Bin> 441
replace `thisblock' = substr(run_opo_id,1,2) == "TN" &  (donor_cat == "old_healthy")
replace Bin = 419 if `thisblock' & `mandatory' & Bin> 419
replace Bin = 421 if `thisblock' & ~`mandatory' & Geo == "Statewide" & Bin> 422
replace `thisblock' = substr(run_opo_id,1,2) == "TN" & (donor_cat == "ecd")
replace Bin = 410 if `thisblock' & Geo== "Statewide" & Bin> 410

//Young_dcd, Old_dcd
replace `thisblock' = substr(run_opo_id,1,2) == "TN"  &  (donor_cat == "old_dcd" | donor_cat == "young_dcd")
replace Bin = 1 if `thisblock' & ABDR=="Zero" & ABO == "Identical" & Geo == "Statewide"  //There is only a statewide 0 ABRD mismatch sharing.
replace Bin = 2 if `thisblock' & ABDR=="Zero" & ABO == "O Donor B Candidate"  & Geo == "Statewide" & Bin>2
replace Bin = 3 if `thisblock' & ABDR=="Zero" & ABO == "Compatible"  & Geo == "Statewide" & Bin>3
replace Bin = 4 if `thisblock' & prior_living_donor & Geo == "Statewide" & Bin>4
replace Bin = Bin*10 if `thisblock' & Bin > 4
replace `thisblock' = substr(run_opo_id,1,2) == "TN" &  (donor_cat == "old_dcd")
replace Bin = 59 if `thisblock' & `mandatory'  & Bin> 59 
replace Bin = 61 if `thisblock' & ~`mandatory' & Geo == "Statewide" & Bin> 61
replace `thisblock' = substr(run_opo_id,1,2) == "TN" &  (donor_cat == "young_dcd")
replace Bin = 48 if `thisblock' & `mandatory' & `highest_scoring' & Bin> 48
replace Bin = 49 if `thisblock' & `mandatory' & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")  & Bin> 49
replace Bin = 51 if `thisblock' & ~`mandatory' & `highest_scoring' & Geo == "Statewide" & Bin> 51
replace Bin = 61 if `thisblock' & ~`mandatory' & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing") & Geo == "Statewide" & Bin> 61
replace Bin = 79 if `thisblock' & `mandatory' & Bin> 79
replace Bin = 81 if `thisblock' & ~`mandatory' & Geo == "Statewide" & Bin> 81
// Ecd_dcd
replace `thisblock' = substr(run_opo_id,1,2) == "TN" & (donor_cat == "ecd_dcd")
replace Bin = 1 if `thisblock' & ABDR=="Zero" & ABO == "Identical" & (Geo == "Statewide" | Geo == "Local") //There is only a statewide 0 ABRD mismatch sharing.
replace Bin = 2 if `thisblock' & ABDR=="Zero" & ABO == "O Donor B Candidate" & (Geo == "Statewide" | Geo == "Local") & Bin>2
replace Bin = 3 if `thisblock' & ABDR=="Zero" & ABO == "Compatible" & (Geo == "Statewide" | Geo == "Local") & Bin>3
replace Bin = Bin*10 if `thisblock' & Bin > 3
replace Bin = 65 if `thisblock' & Geo == "Statewide"  & Bin> 65
// Points
replace `thisblock' = substr(run_opo_id,1,2)=="TN"  & (Geo == "Local" | Geo == "Statewide") 
if `do_points' {
replace p_hla = 0 if `thisblock' 
replace p_hla = 2 if `thisblock' & bmis+drmis == 2
replace p_hla = 2 if `thisblock' & cmis ==0  & drmis<3 //? Two points are assigned for 0 CREG, a 1 or 2 DR mismatch;
replace p_hla = 5 if `thisblock' & bmis+drmis ==1  
replace p_hla = 7 if `thisblock' & bmis+drmis ==0
replace p_hla = 10 if `thisblock' & drmis ==0 & cmis ==0
replace p_pra = 0 if `thisblock'
replace p_pra = 2* ((cpra >= 0.395) + (cpra>=0.795)) if `thisblock'
replace p_urgency = 5*(unos_cand_sta == 4060) if `thisblock' 
}
replace flag_bin = 1 if variance=="TN" & (donor_cat == "ecd" | donor_cat == "ecd_dcd") & (strpos(descrip,"Mandatory")>0 | strpos(descrip,"Voluntary")>0)
replace flag_bin = 1 if variance=="TN" & ~(donor_cat == "ecd" | donor_cat == "ecd_dcd") & ~(strpos(descrip,"Mandatory")>0 | strpos(descrip,"Voluntary")>0 | strpos(descrip,"ABRD")>0) & (strpos(descrip,"OPO")>0 | strpos(descrip,"Local")>0 | strpos(descrip,"Statewide")>0)

// Add up the points




  
di "Variance CADN"
replace variance = "CADN" if run_opo_id ==  "CADN-OP1"
// CADN offers the first kidney to unsensitized patients (cpra>=0.295); once it is allocated, it jumps all unsensitized patients (code 853) and offers it to sensitized patients
// Order: OPO KI, Master File, Unsensitized Pediatric ;  OPO KI, Master File, Unsensitized and Sensitized Pediatric;  OPO KI, Not in Master file, Pediatric;
// Then same thing, not pediatric;
// No upgrade for Highest scoring High CPRA Locally!
// Some provision to give additional waiting time to incompatible paired recipient listed at CADN.
replace `thisblock' = (variance == "CADN") & (donor_cat == "young_healthy")
replace Bin = Bin*10 if `thisblock' & Bin > 40 
replace Bin = 421 if `thisblock' & `in_master_file' & `unsensitized' & Bin == 420
replace Bin = 422 if `thisblock' & `in_master_file' & Bin == 420
replace Bin = 423 if `thisblock' & ~`in_master_file' & Bin == 420
replace Bin = 441 if `thisblock' & `in_master_file' & `unsensitized' & Bin == 420
replace Bin = 442 if `thisblock' & `in_master_file' & Bin == 440
replace Bin = 443 if `thisblock' & ~`in_master_file' & Bin == 440
replace `thisblock' = (variance == "CADN") & (donor_cat == "old_healthy")
replace Bin = Bin*10 if `thisblock' & Bin > 40 
replace Bin = 421 if `thisblock' & `in_master_file' & `unsensitized' & Bin == 420
replace Bin = 422 if `thisblock' & `in_master_file' & Bin == 420
replace Bin = 423 if `thisblock' & ~`in_master_file' & Bin == 420
replace flag_bin = 1 if (variance == "CADN") & (donor_cat == "ecd" | donor_cat == "ecd_dcd") & strpos(descrip,"Master")> 0
replace `thisblock' = (variance == "CADN") & (donor_cat == "young_dcd")
replace Bin = Bin*10 if `thisblock' & Bin > 4 
replace Bin = 61 if `thisblock' & `in_master_file' & `unsensitized' & Bin == 60
replace Bin = 62 if `thisblock' & `in_master_file' & Bin == 60
replace Bin = 63 if `thisblock' & ~`in_master_file' & Bin == 60
replace Bin = 81 if `thisblock' & `in_master_file' & `unsensitized' & Bin == 80
replace Bin = 82 if `thisblock' & `in_master_file' & Bin == 80
replace Bin = 83 if `thisblock' & ~`in_master_file' & Bin == 80
replace `thisblock' = (variance == "CADN") & (donor_cat == "old_dcd")
replace Bin = Bin*10 if `thisblock' & Bin > 4 
replace Bin = 61 if `thisblock' & `in_master_file' & `unsensitized' & Bin == 60
replace Bin = 62 if `thisblock' & `in_master_file' & Bin == 60
replace Bin = 63 if `thisblock' & ~`in_master_file' & Bin == 60
// Points
replace `thisblock' = (variance == "CADN")  & Geo=="Local"
if `do_points' {
replace p_wait_wl = p_wait_wl/2 if  `thisblock'
replace p_wait_wl_deep = p_wait_wl_deep/2 if  `thisblock'
replace p_wait_wl_act = p_wait_wl_act/2 if  `thisblock'
replace p_hla = awaits_PA*2 if  `thisblock' 
// No points for HLA but 2 points for patients who awaiy PA.
// Add up points
di "Add up points!"
}
           
di "Variance Region 1"
gen `wait_dialysis' = match_date - dialysis_date
replace `wait_dialysis' = 0 if `wait_dialysis'==. | `wait_dialysis'< 0 
replace variance = "R1" if run_opo_id ==  "MAOB-OP1" |  run_opo_id ==  "CTOP-OP1"

if `do_points' {
// Points
replace p_wait_wl = 0 if variance =="R1"
replace p_wait_wl = (1- sqrt(1-(min(`wait_dialysis',180)/180)^2))*8 if age_guess <6 & variance =="R1"
replace p_wait_wl = (1- sqrt(1-(min(`wait_dialysis',365)/365)^2))*8 if age_guess >=6 & age_guess <11 & variance =="R1"
replace p_wait_wl = (1- sqrt(1-(min(`wait_dialysis',545)/545)^2))*8 if age_guess >=11 & age_guess <18 & variance =="R1"
replace p_wait_wl = min(`wait_dialysis',545)/545*8  if age_guess >=18 & variance =="R1"
replace p_hla = 0 if variance=="R1"
replace p_hla = 7 if variance=="R1" & drmis+bmis==0 & ~(donor_cat == "ecd" | donor_cat == "dcd_ecd")
//replace p_hla = p_hla + population_points
}

//Region 1 KI, Incompatible Living Donor Paired
replace Bin = Bin*10 if variance =="R1"
replace `thisblock' =  variance =="R1" & `incompatible_paired_recipient' & Geo =="Local"
replace Bin = 405 if `thisblock' & (donor_cat =="young_healthy" | donor_cat=="old_healthy" | donor_cat == "ecd")
replace Bin = 45 if `thisblock' & ~(donor_cat =="young_healthy" | donor_cat=="old_healthy" | donor_cat == "ecd")


/*
table wl_id_co if strpos(descrip,"Region 1")>0

----------------------
ENCRYPTED |
WL_ID     |      Freq.
----------+-----------
    18396 |          2
   532586 |          6
   674395 |          1
   925676 |          6
   945263 |          2
----------------------
These candidates receive the right of first refusal for the next ABO identical (T-cell crossmatch negative)
deceased donor kidney available in the region. This priority continues until the candidate is transplanted.
If there are multiple potential recipients with clasification "Region 1 KI, Incompatible Living Donor Paired"
they are sorted by transplant date of paired donor, then Region 1 waiting time (dialysis), and finally, the earlier
listing date.

The rules pertaining to prior living organ donors and incompatible living donor pairs are not applicable these classifications do not appear on ECD match runs.
*/



di "Variance DCTC"
replace variance = "DCTC" if run_opo_id ==  "DCTC-OP1"
replace Bin = Bin*10 if variance=="DCTC"
replace Bin = 1 if `incompatible_paired_recipient' & (`use_descript' | (ABO=="Identical" & Geo=="Local")) & variance=="DCTC"
/* 
DCTC KI, Incompatible Living Donor Paired
----------------------
ENCRYPTED |
WL_ID     |      Freq.
----------+-----------
   490852 |          3
----------------------
Same rules as in Region 1
*/

di "Variance LAOP"
replace variance = "LAOP" if run_opo_id ==  "LAOP-OP1"
if `do_points' {
replace `thisblock' = (variance == "LAOP")  & Geo=="Local"
replace p_wait_wl = p_wait_wl/2 if  `thisblock'
replace p_wait_wl_deep = p_wait_wl_deep/2 if  `thisblock'
replace p_wait_wl_act = p_wait_wl_act/2 if  `thisblock'
}
// IMPORTANT FOR PYTHON CODE:
// Patients with current CPRA>50 and greater than 2 abdr mismatch are not offered kidneys recovered outside the OPO (imported)
// We need to screen out patients listed at LAOP-OP1 when offered at 	the regional or national level.

di "Variance MWOB" // (see this, I may run a different piece for _t and _c)
replace variance = "MWOB" if run_opo_id ==  "MWOB-OP1"
// Different treatment depending on donor-patient blood types.
replace Bin = Bin*10 if (variance == "MWOB")
replace `thisblock' = (variance == "MWOB") & (donor_cat == "young_healthy")  & (abo_don =="A2" | abo_don =="A2B")
replace Bin = 401 if `thisblock' & (Bin >= 410 & Bin <=  440) &  cpra>=0.795 & (abo =="A2" | abo =="A2B") 
replace Bin = 402 if `thisblock' & (Bin >= 410 & Bin <=  440) &  cpra>=0.795 & (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
replace Bin = 403 if `thisblock' & (Bin >= 410 & Bin <=  440) &  (abo =="A2" | abo =="A2B") & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")
replace Bin = 404 if `thisblock' & (Bin >= 410 & Bin <=  440) &  (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" ) & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")
replace Bin = 405 if `thisblock' & (Bin >= 410 & Bin <=  440) &  (abo =="B" ) & `highest_scoring'
replace Bin = 406 if `thisblock' & (Bin >= 410 & Bin <=  440) &  (abo =="B" ) & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")
replace Bin = 407 if `thisblock' & (Bin >= 410 & Bin <=  440) &  (abo =="O" ) & `highest_scoring'
replace Bin = 408 if `thisblock' & (Bin >= 410 & Bin <=  440) &  (abo =="O" ) & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")
replace Bin = 441 if `thisblock' & (Bin == 440) &  (abo =="O" ) 
replace Bin = 442 if `thisblock' & (Bin == 440) &  cpra<0.795 &  (abo =="A2" | abo =="A2B")
replace Bin = 443 if `thisblock' & (Bin == 440) &  cpra<0.795 &  (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
replace `thisblock' = (variance == "MWOB") & (donor_cat == "young_dcd")  & (abo_don =="A2" | abo_don =="A2B")
replace Bin = 41 if `thisblock' & (Bin >= 50 & Bin <=  80) &  cpra>=0.795 & (abo =="A2" | abo =="A2B")
replace Bin = 42 if `thisblock' & (Bin >= 50 & Bin <=  80) &  cpra>=0.795 & (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
replace Bin = 43 if `thisblock' & (Bin >= 50 & Bin <=  80) &  (abo =="A2" | abo =="A2B") & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")
replace Bin = 44 if `thisblock' & (Bin >= 50 & Bin <=  80) &  (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" ) & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")
replace Bin = 45 if `thisblock' & (Bin >= 50 & Bin <=  80) &  (abo =="B" ) & `highest_scoring'
replace Bin = 46 if `thisblock' & (Bin >= 50 & Bin <=  80) &  (abo =="B" ) & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")
replace Bin = 47 if `thisblock' & (Bin >= 50 & Bin <=  80) &  (abo =="O" ) & `highest_scoring'
replace Bin = 48 if `thisblock' & (Bin >= 50 & Bin <=  80) &  (abo =="O" ) & (Ped_Alloc=="Ped At Allocation" | Ped_List=="Ped At Listing")
replace Bin = 81 if `thisblock' & (Bin == 80) &  (abo =="O" ) 
replace Bin = 82 if `thisblock' & (Bin == 80) &  cpra<0.795 &  (abo =="A2" | abo =="A2B")
replace Bin = 83 if `thisblock' & (Bin == 80) &  cpra<0.795 &  (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
// Older donors
// When the donor is 35yo or more and blood type A2 or A2B , there is no Highest Schoring High CPRA classification for candidates with ABO = A,A1,A2,AB,A1B,A2B.
// these candidates are classified by CPRA and blood type only. For the same donors,  inclusion in the "OPO KI, Highest Scoring High CPRA, O Candidates" (or B) is based 
//upon the scores of local blood type B candidates only... (weird, there is no such classification for old donors in the data).
replace `thisblock' = (variance == "MWOB") & (donor_cat == "old_healthy")  & (abo_don =="A2" | abo_don =="A2B")
replace Bin = 401 if `thisblock' & (Bin == 420) &  cpra>=0.795 & (abo =="A2" | abo =="A2B") 
replace Bin = 402 if `thisblock' & (Bin == 420) &  cpra>=0.795 & (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
replace Bin = 421 if `thisblock' & (Bin == 420) &  (abo =="O" ) 
replace Bin = 422 if `thisblock' & (Bin == 420) &  cpra<0.795 &  (abo =="A2" | abo =="A2B")
replace Bin = 423 if `thisblock' & (Bin == 420) &  cpra<0.795 &  (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
replace `thisblock' = (variance == "MWOB") & (donor_cat == "old_dcd")  & (abo_don =="A2" | abo_don =="A2B")
replace Bin = 51 if `thisblock' & Bin == 60 &  cpra>=0.795 & (abo =="A2" | abo =="A2B")
replace Bin = 52 if `thisblock' & Bin == 60 &  cpra>=0.795 & (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
replace Bin = 56 if `thisblock' & Bin == 60 &  (abo =="B" ) 
replace Bin = 58 if `thisblock' & Bin == 60 &  (abo =="O" ) 
replace Bin = 59 if `thisblock' & Bin == 60 &  cpra<0.795 &  (abo =="A2" | abo =="A2B")
replace `thisblock' = (variance == "MWOB") & (donor_cat == "ecd_dcd")  & (abo_don =="A2" | abo_don =="A2B")
replace Bin = 41 if `thisblock' & Bin == 50 &  cpra>=0.795 & (abo =="A2" | abo =="A2B")
replace Bin = 42 if `thisblock' & Bin == 50 &  cpra>=0.795 & (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
replace Bin = 46 if `thisblock' & Bin == 50 &  (abo =="B" ) 
replace Bin = 48 if `thisblock' & Bin == 50 &  (abo =="O" ) 
replace Bin = 49 if `thisblock' & Bin == 50 &  cpra<0.795 &  (abo =="A2" | abo =="A2B")
replace `thisblock' = (variance == "MWOB") & (donor_cat == "ecd")  & (abo_don =="A2" | abo_don =="A2B")
replace Bin = 401 if `thisblock' & Bin == 410 &  cpra>=0.795 & (abo =="A2" | abo =="A2B")
replace Bin = 402 if `thisblock' & Bin == 410 &  cpra>=0.795 & (abo =="A1" | abo =="A1B" | abo=="A" | abo=="AB" )
replace Bin = 406 if `thisblock' & Bin == 410 &  (abo =="B" ) 
replace Bin = 408 if `thisblock' & Bin == 410 &  (abo =="O" ) 
replace Bin = 409 if `thisblock' & Bin == 410 &  cpra<0.795 &  (abo =="A2" | abo =="A2B")



// old_dcd & ecd_dcd & ecd?
// Points
replace `thisblock' = (variance == "MWOB")   & Geo=="Local" & (abo_don =="A2" | abo_don =="A2B")
if `do_points' {
replace p_wait_wl = p_wait_wl/2 if  `thisblock'
replace p_wait_wl_deep = p_wait_wl_deep/2 if  `thisblock'
replace p_wait_wl_act = p_wait_wl_act/2 if  `thisblock'
}
di "Variance VATB"
replace variance = "VATB" if run_opo_id ==  "VATB-OP1"
//  OPO KI, Incompatible Living Donor Paired. This is problematic
replace Bin = Bin*10 if  variance == "VATB"
if `use_descript' {
	replace `thisblock' = `incompatible_paired_recipient' & variance == "VATB" & (ABO == "Identical" | ABO == "Compatible")
    }
else {
    replace `thisblock' = `incompatible_paired_recipient' & variance == "VATB" & substr(run_loc_ctr,1,4)==substr(listing_ctr,1,4)  
}
replace Bin = 38 if  `thisblock' & (donor_cat =="young_dcd" | donor_cat =="ecd_dcd" | donor_cat =="old_dcd")
replace Bin = 398 if  `thisblock' & (donor_cat =="ecd" | donor_cat =="young_healthy" | donor_cat =="old_healthy")
replace Bin = Bin - 1  if `thisblock' & ABO == "Identical"

di "Variance NCCM"
replace variance = "NCCM" if run_opo_id ==  "NCCM-IO1"
//  OPO KI, Incompatible Living Donor Paired

di "Variance ORUO"
replace variance = "ORUO" if run_opo_id ==  "ORUO-IO1"
// IMPORTANT FOR PYTHON CODE:
// Blood group A2 donor kidneys are transplanted into blood group A, B, or AB tranasplant candidates
// and blood group A2B donor kidneys are transplantedinto blood group B or AB transplant candidates

di "Variance OKOP"
replace variance = "OKOP" if run_opo_id ==  "OKOP-OP1"

tempvar loc_area lis_area
local tulsa = "OKSJ OKSF"
local oklahoma = "OKBC OKCM OKMD OKSA"
gen `loc_area' =  0
gen `lis_area' =  0
foreach tx_ctr of local tulsa {
replace `loc_area' = 1 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="OKOP"
replace `lis_area' = 1 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="OKOP"
} 
foreach tx_ctr of local oklahoma {
replace `loc_area' = 2 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="OKOP"
replace `lis_area' = 2 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="OKOP"
}

if `use_descript' {
	replace Geo = "OPO" if Geo_detail == "OPO" & variance =="OKOP"
	}
else {
	replace Geo = "OPO" if Geo == "Local" & variance =="OKOP" & `loc_area' ~= `lis_area' & ~(cpra>0.595 | awaits_PA | Ped_Alloc=="Ped At Allocation")
}

table Geo if variance =="OKOP"

replace `thisblock' = (variance == "OKOP")  & (donor_cat == "young_healthy" | donor_cat == "old_healthy")
replace Bin = Bin*10 if Bin>40 & `thisblock'
replace Bin = 421 if `thisblock' & Bin == 420 & Geo == "OPO" 
replace Bin = 441 if `thisblock' & Bin == 440 & Geo == "OPO" 
replace `thisblock' = (variance == "OKOP")  & (donor_cat == "ecd")
replace Bin = Bin*10 if Bin>40 & `thisblock'
replace Bin = 411 if `thisblock' & Bin == 410 & Geo == "OPO" 
replace `thisblock' = (variance == "OKOP")  & (donor_cat == "old_dcd" | donor_cat == "young_dcd")
replace Bin = Bin*10 if Bin>4 & `thisblock'
replace Bin = 61 if `thisblock' & Bin == 60 & Geo == "OPO" 
replace Bin = 81 if `thisblock' & Bin == 80 & Geo == "OPO" 
replace `thisblock' = (variance == "OKOP")  & (donor_cat == "ecd_dcd")
replace Bin = Bin*10 if `thisblock' & Bin > 3
replace Bin = 41 if `thisblock' & Bin == 40 & Geo == "OPO" 


di "Variance PADV"
replace variance = "PADV" if run_opo_id ==  "PADV-OP1" 

local padv1 = "PAGM PAHE PALV PAWV PAHH"
local padv2 = "PAAE PACP PAHM PAUP PASC PATU PATJ PALH DEAI DECC"

foreach tx_ctr of local padv1 {
replace `loc_area' = 1 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="PADV"
replace `lis_area' = 1 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="PADV"
} 
foreach tx_ctr of local padv2 {
replace `loc_area' = 2 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="PADV"
replace `lis_area' = 2 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="PADV"
}

if `use_descript' {
	replace Geo = "OPO" if Geo_detail == "OPO" & variance == "PADV"
	}
else {
	replace Geo = "OPO" if Geo == "Local" & variance == "PADV" & `loc_area' ~= `lis_area' 
}
replace `thisblock' = (variance == "PADV")  & (donor_cat == "young_healthy" | donor_cat == "old_healthy" | donor_cat =="ecd")
replace Bin = Bin*10 if `thisblock' & Bin>39
replace Bin = Bin + 2 if `thisblock' & Bin>390 & Geo == "OPO"
replace Bin = Bin + 9 if `thisblock' & Bin==412 & Geo == "OPO" & donor_cat == "young_healthy" // Local: Highest Scoring, Pediatric; OPO: Highest Scoring, Pediatric;

// First kidney is allocated Locally, the second OPO-wide. People are bypassed with 853'd. 
replace `thisblock' = (variance == "PADV")  & (donor_cat == "young_dcd" | donor_cat == "old_dcd" | donor_cat =="ecd_dcd")
replace Bin = Bin*10 if `thisblock' & Bin>3
replace Bin = Bin + 2 if `thisblock' & Bin>30 & Geo == "OPO"
replace Bin = Bin + 9 if `thisblock' & Bin==52 & Geo == "OPO" & donor_cat == "young_dcd"


di "Variance PATF"
replace variance = "PATF" if run_opo_id ==  "PATF-OP1" 

local patf1 ="WVCA WVWU"
local patf2 ="PACH PAAG PAPT PAVA"

foreach tx_ctr of local patf1 {
replace `loc_area' = 1 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="PATF"
replace `lis_area' = 1 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="PATF"
} 
foreach tx_ctr of local patf2 {
replace `loc_area' = 2 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="PATF"
replace `lis_area' = 2 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="PATF"
}

if `use_descript' {
	replace Geo = "OPO" if Geo_detail == "OPO" & variance == "PATF"
	}
else {
	replace Geo = "OPO" if Geo == "Local" & variance == "PATF" & `loc_area' ~= `lis_area'
}


replace `thisblock' = (variance == "PATF")  & (donor_cat == "young_healthy" | donor_cat == "old_healthy" | donor_cat =="ecd")
replace Bin = Bin*10 if `thisblock' & Bin>39
replace Bin = Bin + 2 if `thisblock' & Bin>390 & Geo == "OPO"
replace Bin = Bin + 9 if `thisblock' & Bin==412 & Geo == "OPO" & donor_cat == "young_healthy" // Local: Highest Scoring, Pediatric; OPO: Highest Scoring, Pediatric;
// First kidney is allocated Locally, the second OPO-wide. People are bypassed with 853'd. 
replace `thisblock' = (variance == "PATF")  & (donor_cat == "young_dcd" | donor_cat == "old_dcd" | donor_cat =="ecd_dcd")
replace Bin = Bin*10 if `thisblock' & Bin>3
replace Bin = Bin + 2 if `thisblock' & Bin>30 & Geo == "OPO"
replace Bin = Bin + 9 if `thisblock' & Bin==52 & Geo == "OPO" & donor_cat == "young_dcd"


di "Variance TXGC"
replace variance = "TXGC" if run_opo_id ==  "TXGC-OP1" 

local southeast = "TXHH TXHI TXMH TXTC"
local north = "TXFW TXCF TXGV"
local west = "TXLG TXLM"


foreach tx_ctr of local southeast {
replace `loc_area' = 1 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="TXGC"
replace `lis_area' = 1 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="TXGC"
} 
foreach tx_ctr of local north {
replace `loc_area' = 2 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="TXGC"
replace `lis_area' = 2 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="TXGC"
}
foreach tx_ctr of local west {
replace `loc_area' = 3 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="TXGC"
replace `lis_area' = 3 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="TXGC"
}
if `use_descript' {
	replace Geo = "OPO" if Geo_detail == "OPO" & variance =="TXGC"
	}
else {
	replace Geo = "OPO" if Geo == "Local" & variance =="TXGC" & `loc_area' ~= `lis_area' 
}


replace `thisblock' = (variance == "TXGC")  & (donor_cat == "young_healthy" | donor_cat == "old_healthy" | donor_cat =="ecd")
replace Bin = Bin*10 if `thisblock' & Bin>39
replace Bin = Bin + 2 if `thisblock' & Bin>390 & Geo == "OPO"
replace Bin = Bin + 9 if `thisblock' & Bin==412 & Geo == "OPO" & donor_cat == "young_healthy" // Local: Highest Scoring, Pediatric; OPO: Highest Scoring, Pediatric;
// First kidney is allocated Locally, the second OPO-wide. People are bypassed with 853'd. 
replace `thisblock' = (variance == "TXGC")  & (donor_cat == "young_dcd" | donor_cat == "old_dcd" | donor_cat =="ecd_dcd")
replace Bin = Bin*10 if `thisblock' & Bin>3
replace Bin = Bin + 2 if `thisblock' & Bin>30 & Geo == "OPO"
replace Bin = Bin + 9 if `thisblock' & Bin==52 & Geo == "OPO" & donor_cat == "young_dcd"
//Points
replace `thisblock' = (variance == "TXGC")  & Geo == "OPO" & Geo == "Local" & (donor_cat == "young_healthy" | donor_cat == "old_healthy" | donor_cat =="young_dcd" | donor_cat =="old_dcd")
if `do_points' {
replace p_hla = 0 if `thisblock'
replace p_wait_wl = p_wait_wl/2 if  `thisblock'
replace p_wait_wl_deep = p_wait_wl_deep/2 if  `thisblock'
replace p_wait_wl_act = p_wait_wl_act/2 if  `thisblock'
}


di "Variance TXSA" //(specialize for _t _c)
replace variance = "TXSA" if run_opo_id ==  "TXSA-OP1" 
replace `thisblock' = (variance == "TXSA")  
replace Bin = Bin*10 if Bin>43 & `thisblock' & donor_cat =="young_healthy"
replace Bin = 435 if `thisblock' & donor_cat =="young_healthy" & Bin == 440 & abo_don =="A2" & (abo=="B" | abo =="O")  
replace Bin = 435 if `thisblock' & donor_cat =="young_healthy" & Bin == 440 & abo_don =="A2B" & abo=="B"  
//for standard criteria donors 35 years or older, potential recipients are listed in the  “OPO KI, Highest Scoring High  CPRA” 
//classification only on deceased donor matches where the donor ABO is A2 or  A2B, in order to preserve the priority for
//the most highly sensitized candidates.     


di "Variance TXSB"
replace variance = "TXSB" if run_opo_id ==  "TXSB-OP1" 

local dallas = "TXTX TXCM TXMC TXPM TXHD TXSP"
local elpaso = "TXSM TXLP TXCF TXGV"
local galveston = "TXJS TXDC"
local tyler = "TXTY TXSW"


foreach tx_ctr of local dallas {
replace `loc_area' = 1 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="TXSB"
replace `lis_area' = 1 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="TXSB"
} 
foreach tx_ctr of local elpaso {
replace `loc_area' = 2 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="TXSB"
replace `lis_area' = 2 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="TXSB"
}
foreach tx_ctr of local galveston {
replace `loc_area' = 3 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="TXSB"
replace `lis_area' = 3 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="TXSB"
}
foreach tx_ctr of local tyler {
replace `loc_area' = 4 if substr(run_loc_ctr,1,4)=="`tx_ctr'" & variance =="TXSB"
replace `lis_area' = 4 if substr(listing_ctr,1,4)=="`tx_ctr'" & variance =="TXSB"
}


if `use_descript' {
	replace Geo = "OPO" if Geo_detail == "OPO" & variance =="TXSB"
	}
else {
	replace Geo = "OPO" if Geo == "Local" & variance =="TXSB" & `loc_area' ~= `lis_area' 

}

replace `thisblock' = (variance == "TXSB")  & (donor_cat == "young_healthy" | donor_cat == "old_healthy" | donor_cat =="ecd")
replace Bin = Bin*10 if `thisblock' & Bin>39
replace Bin = Bin + 2 if `thisblock' & Bin>390 & Geo == "OPO"
replace Bin = Bin + 9 if `thisblock' & Bin==412 & Geo == "OPO" & donor_cat == "young_healthy" // Local: Highest Scoring, Pediatric; OPO: Highest Scoring, Pediatric;
// First kidney is allocated Locally, the second OPO-wide. People are bypassed with 853'd. 
replace `thisblock' = (variance == "TXSB")  & (donor_cat == "young_dcd" | donor_cat == "old_dcd" | donor_cat =="ecd_dcd")
replace Bin = Bin*10 if `thisblock' & Bin>3
replace Bin = Bin + 2 if `thisblock' & Bin>30 & Geo == "OPO"
replace Bin = Bin + 9 if `thisblock' & Bin==52 & Geo == "OPO" & donor_cat == "young_dcd"

replace cpra =. if cpra==-1
//In November 2004, the OPTN Board of Directors approved a voluntary pilot study regarding alternative kidney waiting time calculations. The study assessed the impact on kidney allocation from permitting kidney waiting time accrual to commence from the time of initiation of chronic maintenance dialysis, even if this time pre-dated the date of listing. The study did not change current policy allowing waiting time (1) for adult candidates who have not yet initiated chronic maintenance dialysis to accrue upon attaining a minimum creatinine clearance level or calculated GFR, with no time accrued based upon these criteria prior to the date of the candidate’s listing, and (2) for pediatric candidates who have not yet initiated chronic maintenance dialysis to accrue upon date of wait listing. The intent of the study was to test the effect of a change in the definition of waiting time on access to transplantation within participating DSAs. Since implementation in 2006, three OPOs have elected to participate in the study: OneLegacy in California, Iowa Donor Network, and Gift of Life Michigan.
// CAOP-OP1 OneLegacy
// IAOP-OP1 Iowa Donor Network
// MIOP-OP1 Gift of Life Michigan

if `do_points' {
di "Alternative Points in CAOP-OP1, IAOP-OP1 and MIOP-OP1"
replace p_wait_wl = year(match_date) - year(dialysis_date) -1*(month(match_date)< month(dialysis_date) | (month(match_date)== month(dialysis_date) &  day(match_date)< day(dialysis_date))) if Geo == "Local" & (run_opo_id == "CAOP-OP1" | run_opo_id == "MIOP-OP1" | run_opo_id == "IAOP-OP1")


replace kidney_points=  p_wait_wl
replace kidney_points = kidney_points + p_hla + p_pra + p_ped11 + p_ped35 + p_urgency if ecd_donor==0
replace kidney_points_deep = p_wait_wl_deep
replace kidney_points_deep = kidney_points_deep + p_hla + p_pra + p_ped11 + p_ped35 + p_urgency if ecd_donor==0
replace kidney_points_activate = p_wait_wl_activate
replace kidney_points_activate = kidney_points_activate + p_hla + p_pra + p_ped11 + p_ped35 + p_urgency if ecd_donor==0

replace kidney_points = kidney_points+4 if prior_living_donor==1 &  ecd_donor==0
replace kidney_points_deep = kidney_points_deep+4 if prior_living_donor==1 &  ecd_donor==0
replace kidney_points_activate = kidney_points_activate+4 if prior_living_donor==1 &  ecd_donor==0
}


end
