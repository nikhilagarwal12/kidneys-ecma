// This script combines several stages in the offer simulation process. Python steps are called from this script.
// Steps are:
//  - (STATA) Prepare inputs for match run imputation
//  - (python) Do match run imputation (../../mechanism/code/python_test/impute_match_runs.py)
//  - (STATA) Process match runs, prepare data for offer simulation
//  - (python) Do offer simulation (../../mechanism/code/calculate_offers.py) 
//  - (STATA) Post-processing of simulated offers

// This script should be called within gen_primary_files.do, which specifies the relevant sample ('filter')

cap log close

***********************************
**  Control which parts of the script to run  
***********************************
// All these locals have to be set to one for a full re-run
local prepare_for_simulation = 1
local do_simulation = 1
local process_simulation = 1
local matlab_inputs = 1
local relevant_histories = 1
local unique_histories = 1
local do_till_time_file = 1

***********************************
**  Decisions 
***********************************
// these locals should be 1 by default.
local manual_fixes = 1
local fix_init_date = 1
local fix_donor_screen = 1


local usefiles = "~/organs-star/primary_files/test_sample/"
if "`filter'" == "NYRT"{
	local usefiles = "~/organs-star/primary_files/nyrt/"
	}
if "`filter'" == "NYRT-Validate"{
  local usefiles = "~/organs-star/primary_files/cross_validation/nyrt/"
}
local mechfiles = "`usefiles'mech_"

// (1) prepare files for offer simulation
if `prepare_for_simulation'==1{
  // 1. Load offers.dta
  // 2. Create variable position
  // 3. Filter match_runs 
  // 4. Drop offers with missing info (wlreg_audit_id_code and match_run_num)
  // 5. Selects a subset of variables (vars_offers_subset) and save `mechfiles'offer_subset
  // 6. Get  last bin, points and tie-breaker also first and last day in sample
  // 7. Overwrite last bin, points and tie-breaker according to fix_last_points by donor_id match_run_num
  // 8. Fix last bin for payback offers
  // 9. Merge it with donor characteristics (see missing antigen hack)
  // 10. Save `mechfiles'donor_extract
  // 11. See patient history below.
  
  // Now prepare donor extract

  di "`filter'"

  use `usefiles'offers.dta, clear
  sort donor_id  
  bys donor_id (match_date match_id_code  ptr_row_order  pb_offer_seq): gen position = _n
  if "`filter'" == "NYRT"{
    keep if pre_KAS
  }
  if "`filter'" == "NYRT-Validate"{
    keep if yofd(match_date)==2014 & month(match_date)<=6
  }
  
  // Drop paybacks (should I drop missing match_run_num?)
  drop if wl_id_code==.
  drop if match_run_num==.
    
  
  // output files to be used in our analysis
  local vars_offers_subset = "donor_id wl_id_code abo_don donor_cat match_date init_date kidney_points bin organ_sequence_num offer_accept refusal_id ecd_donor payback local_match listing_ctr_dsa_id match_run_num match_id_code run_type run_opo_id run_loc_ctr run_opo_region_id payback wlreg_audit_id_code descrip position amis bmis drmis geo abo_match_t"
  keep `vars_offers_subset'
  
  isid donor_id match_run_num wl_id_code
  save `mechfiles'offer_subset, replace
  outsheet using `mechfiles'offer_subset.csv, comma replace
  // `mechfiles'offer_subset should be sorted by donor_id  match_date  match_id_code
  
  summ match_date
  local first_day  = r(min)
  local last_day = r(max) 
    
  // create bin/points/tiebreak cutoffs
  gen tie_break = match_date - init_date
  sort donor_id match_run_num organ_sequence_num

  // account for paybacks when setting cutoffs
  by donor_id: gen payback_flip_marker = match_run_num==1 & payback==. & payback[_n+1]==1

  //preserve
  drop if  kidney_points ==. | tie_break==. | bin==.
  by donor_id match_run_num: keep if _n==_N  // keep last offer for each match run. This generates the cutoff for that match run
  gen last_bin = bin
  gen last_kidney_points = kidney_points
  gen last_tie_break = tie_break

  if `fix_init_date' {
		merge 1:1 donor_id match_run_num using `auxiliary_files'fix_last_points.dta, keepusing(bin_t ub_date lb_date c_ptr_points) keep(match master)
		
		replace last_bin = bin_t if _merge==3
		replace last_kidney_points = c_ptr_points if _merge==3
		replace last_tie_break = match_date - ub_date if _merge==3
		drop _merge
  }
    
  // set cutoffs differently when paybacks follow
  gen payback_bin = 43 if donor_cat == "young_healthy"
  replace payback_bin = 41 if donor_cat == "old_healthy"
  replace payback_bin = 7 if donor_cat == "young_dcd"
  replace payback_bin = 5 if donor_cat == "old_dcd"
  replace payback_bin = 40 if donor_cat == "ecd"
  replace last_bin = payback_bin if payback_flip_marker==1 & payback_bin > last_bin & payback_bin~=. // only replace if the payback-based cutoff is less stringent
  replace last_kidney_points = 100 if payback_flip_marker==1 & payback_bin > last_bin & payback_bin~=.
  
  // save tempfile
  local vars_donors_off = "donor_id match_run_num match_id_code last_bin last_kidney_points last_tie_break run_opo_id local_match run_loc_ctr match_date run_type run_opo_region_id payback"
  keep `vars_donors_off'

  // Merge with donor characteristics, save file
  local vars_donors_don = "abo_don da1 da2 db1 db2 ddr1 ddr2 dbw* dc1 dc2 ddr5* ddp1 ddp2 ddq1 ddq2 opo_id opo_dsa_id opo_region_id donor_cat non_hrt_don age_don wgt_kg_don_calc hep_c_anti_don hist_diabetes_don hist_hypertens_don warm_isch_tm_don bmi_don_calc creat_don amylase_don lipase_don hbv_core_don htlv_don kdri_cat kdpi"
  merge m:1 donor_id using `usefiles'deceased_donors, keep(match) keepusing(`vars_donors_don') nogenerate

  keep donor_id `vars_donors_don' `vars_donors_off'
  gen creat_peak_don = .
  
  if `manual_fixes'== 1 {
    replace hbv_core_don = "Y" if donor_id == 344292
    replace donor_cat = "ecd_dcd" if donor_id == 345097
    replace hist_diabetes_don = 1 if donor_id == 345587    
    replace creat_peak_don = 3.2 if donor_id == 345587
    
  }
  
  if `fix_donor_screen' {
		merge m:1 donor_id match_id_code using `auxiliary_files'fix_donor_screen.dta, keep(master match) keepusing(cd*)
		gen creat_don_old = creat_don 
		replace cd_peak_creat =1000 if cd_peak_creat==.
		replace cd_final_creat =1000 if cd_final_creat==.		
		replace creat_peak_don = cd_peak_creat if cd_peak_creat~=.
		replace creat_don = cd_final_creat if cd_final_creat~=.
		replace creat_don = cd_final_creat if cd_final_creat~=.
		replace warm_isch_tm_don = cd_warm_time if cd_warm_time ~=.
  }
  
  local counter = 1
  foreach var in da1 da2 db1 db2 ddr1 ddr2 ddr51 ddr52 ddr53 dbw4 dbw6 dc1 dc2 ddq1 ddq2 {
      // ensure that donors with missing antigens always result in mismatches (assign them negative values) 
      replace `var' = - `counter' if `var'==.
      local counter = `counter' + 1
  }

  drop if last_bin ==. | last_kidney_points ==. | last_tie_break ==.
  
  sort match_date donor_id match_run_num
  save `mechfiles'donor_extract, replace
  outsheet using `mechfiles'donor_extract.csv, comma replace  // really a "match run extract"

  use `mechfiles'offer_subset

  *************** Patient History **********************
  ** Keep "relevant patient histories". Active during the period, in the right time, or observed in the data
  keep wlreg_audit_id match_date
  bysort wlreg_audit_id: egen min_off_dat = min(match_date)
  bysort wlreg_audit_id: egen max_off_dat = max(match_date)
  bysort wlreg_audit_id: gen obsno = _n
  keep if obsno == 1 
  keep wlreg_audit_id min_off_dat max_off_dat
  tempfile relevant 
  save `relevant'  
  
  use `usefiles'patient_history, clear
  merge 1:1 wlreg_audit_id using `relevant', gen(relevant)
  local addkeeplist=""
  * Keep only relevant updates for relevant patients
  gen tag_for_del = ~(unos_cand_stat > 4000 & unos_cand_stat < 5000)
  local sortby "chg_date chg_time chg_till wlreg_audit_id"
  sort wl_id_code `sortby'
  by wl_id_code: gen long til_date = chg_date[_n+1]
  by wl_id_code: replace chg_ty = "A" if _n==1
  replace til_date = `last_day' + 1 if til_date ==.
  replace chg_date = chg_date - 1 if chg_ty =="A"  // So that the patient is included from the day it's added
  format til_date %d
  * Relevant histories
  di `first_day'
  di `last_day'
  replace tag_for_del = 1 if chg_date> `last_day' | til_date < `first_day' | chg_ty=="D"
  table tag_for_del relevant
  format min_off_dat max_off_dat %td
  list chg_ty min_off_dat max_off_dat chg_date chg_time if relevant==3 & tag_==1
  drop if tag_for_del & relevant<3
  if `fix_init_date' {
    local sortby "chg_date chg_time chg_till wlreg_audit_id num_jump"
    merge 1:m wlreg_audit_id using `auxiliary_files'fix_init_date.dta, generate(source) keepusing(num_jump c_init_date c_date_from wl_id_code wlreg_audit_id_code)
    //Remove observations that are irrelevant
    bysort wl_id_code (`sortby'): egen num_master = total(source==3 | source==1)
    drop if num_master==0
	  // Update chg_date for jumps & for observations from the using sample (source == 2)
	  replace chg_date = c_date_from if (source>1 & num_jump > 1 & c_date_from ~=.) | chg_date==.
    drop if chg_date == .
    replace chg_time = "23:59:59" if chg_time ==""
    replace chg_till = `last_day' + 1 if chg_till ==.
    format chg_till %td
    replace num_jump = 0 if num_jump ==.
    // Fix til_date and drop irrelevant histories
	  sort wl_id_code `sortby'
    isid wl_id_code `sortby'
    replace til_date = chg_date[_n+1] if wl_id_code==wl_id_code[_n+1] & _n<_N
    replace til_date = `last_day' + 1 if til_date ==.
    drop if chg_date> `last_day' | til_date < `first_day' | chg_ty=="D"
    local addkeeplist="c_init_date c_date_from num_jump source"
  }
  
  * Keep only match relevant variables
  format on_expand_donor %6.0g
  format on_iexpand_donor %6.0g
  duplicates drop
  
  * Sort wl_id_codes
  local sortby "chg_date chg_time til_date wlreg_audit_id"
  sort wl_id_code `sortby'
  isid  wl_id_code `sortby'
  keep `addkeeplist' wlreg_audit_id wl_id_code unos_cand_stat_cd cpra chg_time chg_date til_date on_expand_donor on_iexpand_donor unacc* max* doncrit* wlreg_in_master
  * Merge with patient variables
  merge m:1 wl_id_code using `usefiles'patients, keepusing(init_age init_date a1 a2 b1 b2 dr1 dr2 abo listing_ctr listing_ctr_dsa_id listing_ctr_region_id prior_living_donor dialysis_date awaits_PA first_tx_date diab) assert(match using) keep(match) nogenerate
  * Use c_init_date if local fix_init_date is active
  if `fix_init_date' {
    // create new_init_date: 1) if we have info in the using data, use it; otherwise, default to init_date.
    // 2) When we have info, use the date corresponding to c_init_date for num_jump==1 for all entries of that wl_id_code until I see a jump
    replace c_init_date=0 if c_init_date==.
	  bysort wl_id_code (`sortby'): egen new_init_date = max(c_init_date * (num_jump==1))
    replace new_init_date = new_init_date[_n-1] *(source==1) + c_init_date *(1-(source==1)) if _n>1 & wl_id_code==wl_id_code[_n-1]
    replace new_init_date = init_date if new_init_date==0
    // Replace init_date
    replace init_date =new_init_date 
    drop if source == 2
    drop num_jump c_init_date c_date_from new_init_date source
       
   }
   
  * Format
  format init_age %9.0g
  format first_tx_date  %td
  sort `sortby'
  save `mechfiles'pt_hist_extract, replace
  outsheet using `mechfiles'pt_hist_extract.csv, comma replace 
}

// (4) Run python script to calculate offers (whether each patient should get an offer, based on points, screening criteria, unacc antigens, and waiting list status abo etc)

if `do_simulation'==1{

  shell python ../../mechanism/code/calculate_offers.py 0 0 `mechfiles'
  insheet using `mechfiles'offer_write.csv, clear
  foreach var of varlist *_date {
    gen double eventtime = clock(`var', "YMDhms")
    drop `var'
    gen `var' = dofc(eventtime)
    drop eventtime
  }

  save  `mechfiles'offer_write.dta, replace
  
}


// (5) Process simulated offers, see how they match actual offer data
if `process_simulation' ==1 {
  * Merge offers we THINK should have been made with the actual offer dataset
  use `mechfiles'offer_write.dta, clear
  
  if `manual_fixes'==1 {
    replace reason_screen = "" if reason_screen == "|50" 
  }

        
            
  merge 1:1 donor_id match_run_num wl_id_code using `mechfiles'offer_subset.dta, keepusing(organ_sequence_num offer_accept bin kidney_points refusal_id wlreg_audit_id init_date match_date position)
  gen written_from_python = _merge == 1 |  _merge == 3
  di _N
  drop _merge
  
  di _N
  gen byte offered_sim = offered_points & offered_status & (1-unacceptable)>0 & (1-unacceptable_eq)>0 & offered_runtype & abo_abdr_ok & reason_screen=="" & written_from_python
  gen offered_dat = organ_sequence_num~=.
  tempfile offers_temp
  save `offers_temp', replace

      ****************************************
      * output to mechanism_code_fit_NYRT
      ****************************************
      // interlude to save tabulation of predicted vs actual offers meeting screening criteria
      tab offered_sim offered_dat, matcell(x)
      matrix list x
      clear
      svmat x
      l if _n==1
      export excel using ../../../../common_output/reduced_form/mechanism_code_fit_`filter'.xls, sheet("raw", replace)
  
  use `offers_temp', clear  // back to our regularly scheduled program
  di _N
  
  // Code reasons more concisely as a string. "1"/"0" for whether you were FILTERED based on each of the five criteria
  // P - points S - screening U - unacceptables W - written by python R - run type
  // So, e.g. "P0S1U0W0R1" means you were not offered in our simulation both because of the screening variables and because it was a within-OPO run, but you had enough points and there were no unacceptable antigens
    gen filter_string = "P0"
    replace filter_string = "P1" if ~offered_points
    replace filter_string = filter_string + "I0" if offered_status 
    replace filter_string = filter_string + "I1" if ~offered_status
    replace filter_string = filter_string + "S0" if reason_screen== "" 
    replace filter_string = filter_string + "S1" if reason_screen~= ""
    replace filter_string = filter_string + "U0" if ~unacceptable 
    replace filter_string = filter_string + "U1" if unacceptable
    replace filter_string = filter_string + "W0" if written_from_python  // note that this reverses the coding of "written_from_python"
    replace filter_string = filter_string + "W1" if ~written_from_python
    replace filter_string = filter_string + "R0" if offered_runtype
    replace filter_string = filter_string + "R1" if ~offered_runtype
    replace filter_string = filter_string + "K0" if abo_abdr_ok
    replace filter_string = filter_string + "K1" if ~abo_abdr_ok
    replace filter_string = filter_string + "Q0" if ~unacceptable_eq
    replace filter_string = filter_string + "Q1" if unacceptable_eq
    
    tab filter_string offered_dat
  
  save `mechfiles'offers_processed.dta, replace
 }
  
if `matlab_inputs'==1 {
  
  use `mechfiles'offers_processed.dta, clear
  isid donor_id  match_run_num  wl_id_code
  cap drop match_date
 
  * Merge match_date
  merge m:1 donor_id match_run_num using `mechfiles'donor_extract, keepusing(match_date) assert(match)
   
  // drop duplicate offers (same patient-donor pair, different match runs) -- Modified according to fix #2
  // Keep the "best" offer a patient receives: unscreened offers (or screened offers if all offers where screened).
  gen int neg_match_run = - match_run_num
  bys donor_id wl_id_code (screened  neg_match_run): gen tag_for_del = _n > 1
  drop neg_match_run   
  
  gen accept_yn = offer_accept == "Y" 
  
  format match_date %tdNN/DD/YY
  local keep_in_order = "tag_for_del wl_id_code wlreg_audit_id donor_id offered_dat offered_sim c_bin c_kidney_points c_tie_break screened position accept_yn match_date refusal_id a_mm b_mm dr_mm c_abo geo match_run_num"

  replace tag_for_del=1 if ~(offered_dat | filter_string == "P0I0S1U0W0R0K0Q0")    // Keep only relevant offers for estimation.
  keep `keep_in_order'
  order `keep_in_order'
  
  // Reset position as consecutive numbers.
  replace screened = position==.
  gsort donor_id screened position -offered_sim c_bin -c_kidney_points -c_tie_break match_run_num wlreg_audit_id_code wlreg_audit_id_code
  drop position
  by donor_id: gen position = _n
  ren position position_dat
  gsort donor_id match_run_num -offered_sim c_bin -c_kidney_points -c_tie_break wlreg_audit_id_code
  by donor_id: gen position_sim = _n
  
  // offers observed in the data and not in the simulation have missing mismatches. Fix
  merge m:1 donor_id match_run_num wlreg_audit_id using `mechfiles'offer_subset, update keep(master match match_update match_conflict) keepusing(amis bmis drmis geo abo_match_t)
  replace a_mm = amis if a_mm==.
  replace b_mm = bmis if b_mm==.
  replace dr_mm = drmis if dr_mm==.
  replace c_abo = abo_match_t if c_abo ==""
  drop amis bmis drmis abo_match_t
  // encode variables
  encode_vars "c_abo geo" 
  replace refusal_id = -99 if refusal_id==.
  gen accept_810_yn = (refusal_id==810 | accept_yn==1)
  drop if tag_for_del
  drop c_bin c_kidney_points c_tie_break tag_for_del _merge
  
  isid donor_id wl_id_code // Verify if there are duplicates.

  // de-duplicate offers by pt_code
  merge m:1 wl_id_code using `usefiles'patients, assert(match using) keep(match) keepusing(pt_code) nogenerate
  gsort donor_id pt_code -accept_yn position_sim
  isid donor_id pt_code accept_yn position_sim
  by donor_id pt_code: keep if _n==1
  isid donor_id pt_code
  
  outsheet using `usefiles'simulated_offers_matlab.txt, delimiter("|") nolabel names noquote replace 
  save `usefiles'simulated_offers_matlab.dta, replace

  // create a file called deceased_donors_in_sample that only contains donors offered to patients in the sample
  keep donor_id
  duplicates drop
  merge 1:1 donor_id using `usefiles'deceased_donors_encoded, assert(match using) keep(match) nogen
  compress
  save `usefiles'deceased_donors_in_sample, replace
  
  // format variables for matlab
  #delimit ;
  local dropvars_donor = "home_city_don home_zip_don recov_country_don cod_ostxt_don consent_doc_mech_ostxt 
  consent_px_writ_doc citizen_country_don other_inf_ostxt_don pt_oth*_ostxt_don 
  opo_ctr don_ty *ostxt cancer_oth_ostxt_don opo_id chagas_nat chagas_* chagus_history 
  hbv_dna hcv_nat htlv_nat west_nile_* *perfusion* tb_history donor_hospital donor_hospital_zip"; 
  #delimit cr

  drop `dropvars_donor'
  matlab_format "*_date"
  compress

  outsheet using `usefiles'deceased_donors_in_sample.txt, delimiter("|") nolabel names noquote replace
}


if `relevant_histories'==1 {
  tempfile relevant
  use `usefiles'simulated_offers_matlab.dta, clear
  drop if wlreg_audit_id==.
  bysort wlreg_audit_id: egen ever_off_dat = max(offered_dat)
  bysort wlreg_audit_id: egen ever_off_sim = max(offered_sim)
  bysort wlreg_audit_id: gen obsno = _n
  keep if obsno == 1
  keep wlreg_audit_id ever_off_dat ever_off_sim
  capture drop _merge
  save `relevant', replace
  use `mechfiles'pt_hist_extract.dta
  gen original_order = _n
  keep wlreg_audit_id original_order chg_date til_date
  merge m:1 wlreg_audit_id using `relevant', assert(master match)
  gen relevant = _merge==3

  drop _merge
  sort original_order
  save `mechfiles'patient_relevant_history.dta, replace
  outsheet wlreg_audit_id relevant using `mechfiles'patient_relevant_history.csv, comma names noquote replace
}

if `unique_histories'==1 {
  use `mechfiles'pt_hist_extract.dta,clear
  order wlreg*  chg_date  til_date dialysis_date init_date wl_id_code
  qui describe, short varlist
  local lastvar : word `r(k)' of `r(varlist)'
  merge 1:1 wlreg_audit_id chg_date til_date using `mechfiles'patient_relevant_history.dta, assert(match) nogen
  ren relevant irrelevant
  replace irrelevant=1-irrelevant
  bysort wlreg_audit_id_code wl_id_code-`lastvar' (irrelevant): gen copy_n = _n
  gen first_occ = copy_n ==1
  gen id = sum(first_occ)
  preserve
  keep wlreg_audit_id copy_n id
  save `mechfiles'unique_hist_map.dta, replace
  restore 
  keep if first_occ & ~irrelevant
  drop irrelevant-id
  save `mechfiles'pt_unique_hist.dta, replace
  outsheet using `mechfiles'pt_unique_hist.csv, comma names noquote replace
}


if `do_till_time_file'==1{
  cap shell python ../../mechanism/input/kas/txt2csv.py
  shell python ../../mechanism/code/calculate_pair_bin_points.py `mechfiles'
}
