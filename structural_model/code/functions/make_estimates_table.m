function make_estimates_table(cs,data,file) 

%% cs: chain_summary structure
fid = fopen(file, 'w');
w = cs.model.model_name;
%%% CCPs
%fprintf(fid, '%s\n',cs.model.model_name);
%fprintf(fid, 'Conditional Choice Probabilities\n');
%% Headers
fprintf(fid, 'X,Offset%s,',w);
fprintf(fid, 'Coefficient%s, SE%s,%s',w,w,w);
fprintf(fid, '\n');

%% Coefficients 
coefficient_names = cs.model.fixed.colheaders;
mu_dm = [cs.model.fixed.mu_dm repmat(cs.model.flex.mu_dm,1,cs.model.K_types)];
sigma_dm = [cs.model.fixed.sigma_dm repmat(cs.model.flex.sigma_dm,1,cs.model.K_types)];

assert(cs.model.K_theta==numel(cs.mu_theta));
b = cs.mu_theta;
se = sqrt(diag(cs.cov_theta));

if cs.model.pt_uh  % add column names for type-specific coefficients
    for kk = 1:cs.model.K_types
        for mm = 1:length(cs.model.flex.colheaders)
            coefficient_names{end+1} = [cs.model.flex.colheaders{mm} ' Type ' num2str(kk)];
        end
    end
end

if cs.model.donor_uh
    coefficient_names{end+1} = 'Donor Unobservable SD';
    mu_dm(end+1)=0;
    sigma_dm(end+1)=1;
    if isfield(cs,'V_sigma_eta'), cs.se_sigma_eta = cs.V_sigma_eta; end   % Delete in next rebuild
    b(end+1)=sqrt(cs.mu_sigma_eta);
    se(end+1) = 1/sqrt(2 * cs.mu_sigma_eta) * cs.se_sigma_eta;    % Delta Method
end

if cs.model.pt_uh  % patient UH type probabilities
    for kk = 1:cs.model.K_types
        coefficient_names{end+1} = ['Type ' num2str(kk) ' Probability'];
        mu_dm(end+1) = 0; 
        sigma_dm(end+1) = 1;
        b(end+1) = cs.mu_beta(kk); 
        se(end+1) = sqrt(cs.V_beta(kk,kk));
    end
end

coefficient_names{end+1} = 'acceptance_rate';
mu_dm(end+1)=0;
sigma_dm(end+1)=1;
b(end+1)=mean(1-data.cont_choice);
se(end+1) = sqrt(b(end)*(1-b(end))/data.N_offers);

coefficient_names{end+1} = 'N_offers';
mu_dm(end+1)=0;
sigma_dm(end+1)=1;
b(end+1)= data.N_offers;
se(end+1) = 0;

coefficient_names{end+1} = 'N_donors';
mu_dm(end+1)=0;
sigma_dm(end+1)=1;
b(end+1)= data.N_donors;
se(end+1) = 0;

coefficient_names{end+1} = 'N_patients';
mu_dm(end+1)=0;
sigma_dm(end+1)=1;
b(end+1)= data.N_patients;
se(end+1) = 0;


newline = true;
for coeff = 1:numel(b)
    if newline, fprintf(fid, '%s', coefficient_names{coeff}); end
    fprintf(fid, ',%4.4f, %4.4f, [%4.2f],', mu_dm(coeff), b(coeff)./sigma_dm(coeff), se(coeff)./sigma_dm(coeff));
    t = abs(b(coeff))/se(coeff);
    if t>1.644, fprintf(fid, '*');  % 0.1 significance
        if t>1.96, fprintf(fid, '*'); % 0.05 significance
            if  t>2.575, fprintf(fid, '*'); % 0.01 significance
            end
        end
    end
    if newline, fprintf(fid, '\n'); end
end

fclose(fid);

