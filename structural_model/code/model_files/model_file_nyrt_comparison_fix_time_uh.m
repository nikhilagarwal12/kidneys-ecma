% This is the zero mismatch model
model = struct();
priors = struct();

% Load model that we will simplify 
model_file_nyrt_validation_fix_time_uh

% Filepaths for input data
primary_data_folder = '~/organs-star/primary_files/nyrt/';
model.filenames.offers = strcat(primary_data_folder,'simulated_offers_matlab');
model.filenames.pt_history = strcat(primary_data_folder,'pt_history');
model.filenames.donors = strcat(primary_data_folder,'donors');
model.filenames.patients = strcat(primary_data_folder,'patients');
model.filenames.don_pat_pairs = strcat(primary_data_folder,'don_pat_pairs_');

% Filepath
model.model_name = 'nyrt_comparison_fix_time_uh';
model.match_date_range = {'07/01/2013', '12/31/2013'}; % sample time period

model.gen_script = mfilename('fullpath'); % name of script that generated model file (i.e. name of this script)
model.gen_date = datetime('today'); % date model file was generated

% store model in /structural_model/input/ (../input/)
save([models_folder model.model_name],'model','priors');
