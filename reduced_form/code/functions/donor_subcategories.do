// Creating a sub-function to aggregate characteristics across subsets of donors

*** All years
preserve
collapse (count) donor_id `string_mean' `string_sd' `string_median' `string_p10' `string_p90'
rename donor_id num_donors

gen sample = "All"
assert sample ~=""
order sample num_donors
save `table_temp', replace
restore


*** By Whether Both Kidneys were Transplanted
gen no_discards = num_ki_disc == 0
collapse (count) donor_id  `string_mean' `string_sd' `string_median' `string_p10' `string_p90', by(no_discards)
rename donor_id num_donors

gen sample = "Both Kidneys Transplanted Y/N:" + string(no_discards)
assert sample ~=""
order sample num_donors

drop no_discards 

append using `table_temp'
save `table_temp', replace



