% This is the zero mismatch model
model = struct();
priors = struct();

% Load model that we will simplify 
model_file_nyrt_fix_time_uh_sparse

% Filepath
model.model_name = 'nyrt_validation_fix_time_uh_sparse';
model.orig_name = 'nyrt_fix_time_uh_sparse';
model.comparison_name = 'nyrt_comparison_fix_time_uh_sparse';

% Filpaths for input data
primary_data_folder = '~/organs-star/primary_files/cross_validation/nyrt/';
model.filenames.offers = strcat(primary_data_folder,'simulated_offers_matlab');
model.filenames.pt_history = strcat(primary_data_folder,'pt_history');
model.filenames.donors = strcat(primary_data_folder,'donors');
model.filenames.patients = strcat(primary_data_folder,'patients');
model.filenames.don_pat_pairs = strcat(primary_data_folder,'don_pat_pairs_');

model.match_date_range = {'01/01/2014', '06/30/2014'}; % sample time period

model.gen_script = mfilename('fullpath'); % name of script that generated model file (i.e. name of this script)
model.gen_date = datetime('today'); % date model file was generated

% store model in /structural_model/input/ (../input/)
save([models_folder model.model_name],'model','priors');
