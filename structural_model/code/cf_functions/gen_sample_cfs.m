function object = gen_sample_cfs(model,don_pat_pairs,pt_history,donors,patients,projest,eta_draws_struct,beta_draws_struct,cf_sample)

% This is a wraper for gen_cf_data
% Remove history if init_date is prior to 2010
wl_id_code_list = patients.wl_id_code(patients.init_date>=734139);

% Construct a weight for each wlreg_audit_id_code in proportion to duplication
pt_history.weights = zeros(length(pt_history),1);
for ii = 1:length(pt_history)
    if ~ismember(pt_history.wl_id_code(ii),wl_id_code_list)
        pt_history.weights(ii) = 0;
    else
        pt_history.weights(ii) = 1/sum(pt_history.wl_id_code == pt_history.wl_id_code(ii));
    end
end


% Get a SUBSET of pt_histories and donors (random or according to some criteria)
donor_samp       = donors;
pt_hist_samp     = pt_history;
if ~isfield(cf_sample,'histories_repl'), cf_sample.histories_repl = 0; end
if ~isempty(cf_sample) 
    % Select a subsample
    if isfield(cf_sample,'donors') 
        is_just_a_number = @(x) isreal(x) && numel(x)==1;
        if is_just_a_number(cf_sample.donors) && is_just_a_number(cf_sample.histories)  
            % Random Sample
            rng(cf_sample.rng);
            [donor_samp,  ~] = datasample(donors,cf_sample.donors,'Replace',false);        
            [pt_hist_samp,~] = datasample(pt_history,cf_sample.histories+cf_sample.histories_repl,...
                'Replace',false,'Weights',pt_history.weights);
        else   
            % User specified criteria (functions for donors, patients and histories)
            donor_samp = donors(cf_sample.donors(donors),:);
            if isfield(cf_sample,'patients')
                pt_sample    = patients.wl_id_code(cf_sample.patients(patients));
                pt_hist_samp = pt_hist_samp(ismember(pt_hist_samp.wl_id_code,pt_sample),:);
            end
            if isfield(cf_sample,'histories')
                pt_hist_samp = pt_hist_samp(cf_sample.histories(pt_hist_samp),:);
            end
        end
    end
    if isfield(cf_sample,'single_hist') && cf_sample.single_hist
        % Keep only one history per patient
        perm_order = randperm(size(pt_hist_samp,1)); % random permutation
        [~,ix,~] = unique(pt_hist_samp.wl_id_code(perm_order)); % pick unique values (ix is the first time where a unique entry appears);
        pt_hist_samp = pt_hist_samp(sort(perm_order(ix)),:);   % sort is just to keep the original ordering of pt_histories, not necessary.
        assert(numel(unique(pt_hist_samp.wl_id_code))== numel(ix));
    end    
end


% Call gen_cf_data
time_specs = flatten_structure(cf_sample,{'max_T','t','pd_per_year'});
object =  gen_cf_data(model,don_pat_pairs,pt_hist_samp,patients,donor_samp,projest,eta_draws_struct,beta_draws_struct,time_specs,cf_sample.options,donors);

object.options = time_specs;
object.filenames = model.filenames;

% Set cf_sample options
cf_sample = defaultvalue(cf_sample,'pt_arrival_rate',model.pt_arrival_rate);
cf_sample = defaultvalue(cf_sample,'rho',model.rho);
cf_sample = defaultvalue(cf_sample,'lambda',model.lambda);

object.options.rho = cf_sample.rho;
object.options.lambda = cf_sample.lambda;
object.options.pt_arrival_rate = cf_sample.pt_arrival_rate;

object.donor_id    = donor_samp.donor_id;
object.pt_hist_id  = pt_hist_samp.wlreg_audit_id_code;

% Split and output object:
object.pt_split_by  = 'Combined';
object.don_split_by = 'Combined';
object.pool_name    = 'Combined';

object = object.delete_histories_with_few_compatible_donors(cf_sample.histories);
pt_hist_samp = pt_hist_samp(ismember(pt_hist_samp.wlreg_audit_id_code,object.pt_hist_id),:);
if numel(pt_hist_samp.wlreg_audit_id_code)<cf_sample.histories
    disp('Not enough histories with enough compatible Donors');
    disp('Increase cf_sample.histories_repl.');
    keyboard;
end

if isfield(cf_sample,'pt_split_by')
    ii = 1;
    while size(donor_samp,1)>0 && size(pt_hist_samp,1)>0
        if ii <= numel(cf_sample.pt_split_by)
            % Get a subset of patients/donors
            which_patients  = cf_sample.pt_split_by{ii}{2}(patients.(cf_sample.pt_split_by{ii}{1}));
            which_donors    = cf_sample.don_split_by{ii}{2}(donor_samp.(cf_sample.don_split_by{ii}{1}));
            
        else
            % Get all other patients/donors
            which_patients              = true(size(pt_hist_samp,1));
            which_donors                = true(size(donor_samp,1));
            % Tag the object
            cf_sample.pt_split_by{ii}   = 'All others';
            cf_sample.don_split_by{ii}  = 'All others';
        end
        % Process subset patients        
        which_wlcodes   = patients.wl_id_code(which_patients);
        which_histories = ismember(pt_hist_samp.wl_id_code,which_wlcodes);
        pt_hist_this    = pt_hist_samp(which_histories,:);
        % Process subset of donors
        which_donors    = cf_sample.don_split_by{ii}{2}(donor_samp.(cf_sample.don_split_by{ii}{1}));
        don_this        = donor_samp(which_donors,:);
        % Find them in object.
        where_pt_hist = ismember(object(1).pt_hist_id,pt_hist_this.wlreg_audit_id_code);
        where_don     = ismember(object(1).donor_id,don_this.donor_id);
        temp_object   = object(1).subset(where_pt_hist,[],where_don);
        % Tag the object
        temp_object.pt_split_by  = cf_sample.pt_split_by{ii};
        temp_object.don_split_by = cf_sample.don_split_by{ii};
        temp_object.pool_name    = cf_sample.pool_name{ii};

        % Remove used patients:
        donor_samp              = donor_samp(~which_donors,:);
        pt_hist_samp            = pt_hist_samp(~which_histories,:);
        ii = ii + 1;
        object(ii)              = temp_object;
        
    end
end

