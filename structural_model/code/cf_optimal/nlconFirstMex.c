#include "common.h"

#include "mex.h"

#include <math.h>
#include <string.h>

static int notInitialized = 1;
static long *iIndex, *jIndex;
static double *value;
static double *expTerm, *invCdf, *q, *bac, *pc, *aK;

void cleanup(void) {
  mexPrintf("MEX-file is terminating, destroying allocations in analytic first Jacobian\n");
  free(expTerm);
  free(q);
  free(bac);
  free(invCdf);
  free(pc);
  free(aK);
  free(iIndex);
  free(jIndex);
  free(value);
  notInitialized = 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  double *mu = (double*)mxGetPr(mxGetField(prhs[1], 0, "mu"));
  double *sigma = (double*)mxGetPr(mxGetField(prhs[1], 0, "sigma"));
  double *deltaBy_h = (double*)mxGetPr(mxGetField(prhs[1], 0, "delta_x_h"));
  double *endowment = (double*)mxGetPr(mxGetField(prhs[1], 0, "endowment"));
  double *lambda_hOverK = (double*)mxGetPr(mxGetField(prhs[1], 0, "lambdah_over_K"));
  double *discountW = (double*)mxGetPr(mxGetField(prhs[1], 0, "discount_w"));
  double *discountV1 = (double*)mxGetPr(mxGetField(prhs[1], 0, "discount_V1"));
  double *arrivals = (double*)mxGetPr(mxGetField(prhs[1], 0, "arrivals"));
  double *cArrayRepeat = (double*)mxGetPr(mxGetField(prhs[1], 0, "pr_compat_repeat"));
  double *h = (double*)mxGetPr(mxGetField(prhs[1], 0, "h"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[1], 0, "mu"));
  const unsigned long nPatients = sizeArray[0];
  const unsigned long nTime = sizeArray[1];
  const unsigned long nDonors = sizeArray[2];
  const double nDonorsInv = (1.0/(double)nDonors);
  
  const unsigned long nQ = nPatients*nTime*nDonors;
  const unsigned long nP = nPatients*nTime;
  const unsigned long nTm1 = nTime-1l;
  const unsigned long nTm1nP = nP-nPatients;
  const unsigned long nCeq = nP+nTm1nP;

  const double *qTemp = (double*)mxGetPr(prhs[0]);
  const double *p = qTemp+nQ;
  const double *v = p+nP;

  if (notInitialized) {
    unsigned long nnz;
    if (4*nTime > (nDonors+3)) {
      nnz = nTm1nP*(nDonors+3l)+nQ+nP;
    } else {
      nnz = 2l*nQ;
    }
    iIndex = (long*)malloc(nnz*sizeof(long));
    jIndex = (long*)malloc(nnz*sizeof(long));
    value = (double*)malloc(nnz*sizeof(double));
    expTerm = (double*)malloc(nP*sizeof(double));
    bac = (double*)malloc(nP*sizeof(double));
    invCdf = (double*)malloc(nQ*sizeof(double));
    q = (double*)malloc(nQ*sizeof(double));
    aK = (double*)malloc(nP*sizeof(double));
    pc = (double*)malloc(nQ*sizeof(double));
    mexAtExit(cleanup);
    notInitialized = 0;
  }
  memset(expTerm, 0, nP*sizeof(double));

  // Precompute common sub expressions
  for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
    for (unsigned long i = 0l; i < nP; ++i, ++index) {
      const double local = qTemp[index];
      const double cLocal = cArrayRepeat[index];
      q[index] = local*cLocal;
      expTerm[i] += q[index];
      invCdf[index] = invCDF(1.0-local);
      pc[index] = cLocal*p[i]; 
    }
  } 
  for (unsigned long i = 0l; i < nP; ++i) {
    bac[i] = discountW[i]*(expTerm[i]*nDonorsInv)+discountV1[i];
  }
  for (unsigned long i = 0l; i < nP; ++i) {
    aK[i] = discountW[i]*nDonorsInv;
  } 
  for (unsigned long i = 0l; i < nTm1nP; ++i) {
    expTerm[i] = exp(-deltaBy_h[i]-lambda_hOverK[i]*expTerm[i]);
  } 
  // matrix c
  plhs[0] = mxCreateDoubleMatrix(nDonors, 1, mxREAL);
  double *c = (double*)mxGetPr(plhs[0]);
  /* Contribution from feasibility */
  for (unsigned long i = 0l, index = 0l; i < nDonors; ++i) {
    double sum = 0.0;
    for (unsigned long j = 0l; j < nP; ++j, ++index) {
      sum += p[j]*h[j]*q[index];
    }
    c[i] = sum - endowment[i];
  }
  // matrix ceq
  plhs[1] = mxCreateDoubleMatrix(nCeq, 1, mxREAL);
  double *ceq = (double*)mxGetPr(plhs[1]);
  /* Contribution from steady state */
  for (unsigned long j = 0l, index = 0l, index1 = nPatients; j < nTm1; ++j) {
    for (unsigned long i = 0l; i < nPatients; ++i, ++index, ++index1) {
      ceq[index] = -p[index]*expTerm[index]+p[index1]-arrivals[index1];
    }
  }
  ceq = ceq + nTm1nP;
  for (unsigned long i = 0l, index3 = nTm1nP; i < nPatients; ++i, ++index3) {
    for (unsigned long j = 0l, index = i; j < nTm1; ++j, index += nPatients) {
      double sumW = 0.0;
      for (unsigned long k = 0l, index2 = index; k < nDonors; ++k, index2 += nP) {
        sumW += q[index2]*mu[index2]+normPdf(invCdf[index2])*cArrayRepeat[index2]*sigma[index];
      }
      ceq[index] = bac[index]*v[index]-aK[index]*sumW-v[index+nPatients];
    }
    double sumW = 0.0;
    for (unsigned long k = 0l, index2 = index3; k < nDonors; ++k, index2 += nP) {
      sumW += q[index2]*mu[index2]+normPdf(invCdf[index2])*cArrayRepeat[index2]*sigma[index3];
    }
    ceq[index3] = bac[index3]*v[index3]-aK[index3]*sumW;
  }
  if (nlhs > 2) {
    // matrix gc
    unsigned long int currentIndex = 0l;
    for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
      for (unsigned long i = 0l; i < nP; ++i, ++index) {
        const double localValue = pc[index]*h[i];
        if (localValue != 0.0) {
          iIndex[currentIndex] = index;
          jIndex[currentIndex] = j;
          value[currentIndex++] = localValue;
        }
      }
    }
    for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
      for (unsigned long i = 0l; i < nP; ++i, ++index) {
        const double localValue = q[index]*h[i];
        if (localValue != 0.0) {
          iIndex[currentIndex] = i+nQ;
          jIndex[currentIndex] = j;
          value[currentIndex++] = localValue;
        }
      }
    }
    plhs[2] = mxCreateSparse(nP*(nDonors+2l), nDonors, currentIndex, mxREAL);
    double *sr  = (double*)mxGetPr(plhs[2]);
    size_t* irs = (size_t*)mxGetIr(plhs[2]);
    size_t* jcs = (size_t*)mxGetJc(plhs[2]);
    COO_toCSC(nDonors, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
    // matrix gceq
    currentIndex = 0l;
    // Part from steady state composition
    for (unsigned long j = 0l, index = 0l; j < nDonors; ++j, index += nPatients) {
      for (unsigned long i = 0l; i < nTm1nP; ++i, ++index) {
        const double localValue = expTerm[i]*lambda_hOverK[i]*pc[index];
        if (localValue != 0.0) {
          iIndex[currentIndex] = index;
          jIndex[currentIndex] = i;
          value[currentIndex++] = localValue;
        }
      }
    }
    for (unsigned long j = 0l; j < nTm1nP; ++j) {
      if (expTerm[j] != 0.0) {
        iIndex[currentIndex] = j+nQ;
        jIndex[currentIndex] = j;
        value[currentIndex++] = -expTerm[j];
      }
    }
    const unsigned long offset = nQ + nPatients;
    for (unsigned long j = 0l; j < nTm1nP; ++j) {
      iIndex[currentIndex] = j + offset;
      jIndex[currentIndex] = j;
      value[currentIndex++] = 1.0;
    }
    // Part from value function
    for (unsigned long i = 0l, index = 0l; i < nDonors; ++i) {
      for (unsigned long j = 0l; j < nP; ++j, ++index) {
        double localValue = v[j]-(mu[index]+sigma[j]*invCdf[index]);
        localValue *= aK[j]*cArrayRepeat[index];
        if (localValue != 0.0) {
          iIndex[currentIndex] = index;
          jIndex[currentIndex] = j + nTm1nP;
          value[currentIndex++] = localValue;
        }
      }
    }
    const unsigned long offset1 = nP + nQ;
    const unsigned long offset2 = offset1 + nPatients;
    for (unsigned long j = 0l; j < nP; ++j) {
      const double localValue = bac[j];
      if (localValue != 0.0) {
        iIndex[currentIndex] = j+offset1;
        jIndex[currentIndex] = j + nTm1nP;
        value[currentIndex++] = localValue;
      }
    }
    for (unsigned long i = 0l; i < nTm1nP; ++i) {
      iIndex[currentIndex] = i + offset2;
      jIndex[currentIndex] = i + nTm1nP;
      value[currentIndex++] = -1.0;
    }

    plhs[3] = mxCreateSparse(nP*(nDonors+2l), nP+nTm1nP, currentIndex, mxREAL);
    double *sr1  = (double*)mxGetPr(plhs[3]);
    size_t* irs1 = (size_t*)mxGetIr(plhs[3]);
    size_t* jcs1 = (size_t*)mxGetJc(plhs[3]);
    COO_toCSC(nP+nTm1nP, currentIndex, iIndex, jIndex, value, jcs1, irs1, sr1);
  }
}
