// pre-KAS (2009-10-01 - 2014-12-03) ranking categories
// the relevant variables are created by generate_rank_category_vars.ado

program rank_categories_preKAS

local keys = "../../mechanism/input/"
local temp = "../../mechanism/temp/"

local matchvars ABDR CPRA ABO Ped_List Ped_Alloc Geo
sort donor_cat `matchvars'

// (A) Uses all vars. Need to add prior living donor and "highest scoring high CPRA" categories manually
preserve

keep if donor_cat == "young_healthy"
merge m:1 `matchvars' using `keys'rank_categories_preKAS_young_healthy.dta, keep(master match) nogen
replace Bin = 40 if Bin > 40 & Geo == "Local" & prior_living_donor == 1  // Prior Living Organ Donor -- by patient, using descrip
replace Bin = 41 if Bin > 41 & Geo == "Local" & strpos(descrip, "Highest Scoring") > 0
replace Bin = 45 if Bin > 45 & Geo == "Regional" & strpos(descrip, "Highest Scoring") > 0
replace Bin = 48 if Bin > 48 & Geo == "National" & strpos(descrip, "Highest Scoring") > 0
tempfile temp_young_healthy
save `temp_young_healthy', replace

restore

// (B) Also uses all vars
preserve

keep if donor_cat == "old_healthy"
merge m:1 `matchvars' using `keys'rank_categories_preKAS_old_healthy.dta, keep(master match) nogen
replace Bin = 40 if Bin > 40 & Geo == "Local" &  prior_living_donor == 1  // Prior Living Organ Donor -- by patient, using descrip
tempfile temp_old_healthy
save `temp_old_healthy', replace

restore

// (C) No CPRA or Ped_Alloc, same Prior Living Donor and Highest Scoring High CPRA hard-coding as (A)
preserve

keep if donor_cat == "young_dcd"
sort ABDR ABO Ped_List Geo
merge m:1 ABDR ABO Ped_List Geo using `keys'rank_categories_preKAS_young_dcd.dta, keep(master match) nogen
replace Bin = 4 if Bin > 4 & Geo == "Local" &  prior_living_donor == 1  // Prior Living Organ Donor -- by patient, using descrip
replace Bin = 5 if Bin > 5 & Geo == "Local" & strpos(descrip, "Highest Scoring") > 0
replace Bin = 9 if Bin > 9 & Geo == "Regional" & strpos(descrip, "Highest Scoring") > 0
replace Bin = 12 if Bin > 12 & Geo == "National" & strpos(descrip, "Highest Scoring") > 0
tempfile temp_young_dcd
save `temp_young_dcd', replace

restore

// (D) Analogous to (B)
preserve

keep if donor_cat == "old_dcd"
sort ABDR ABO Geo
merge m:1 ABDR ABO Geo using `keys'rank_categories_preKAS_old_dcd.dta, keep(master match) nogen
replace Bin = 4 if Bin > 4 & Geo == "Local" &  prior_living_donor == 1  // Prior Living Organ Donor -- by patient, using descrip 
tempfile temp_old_dcd
save `temp_old_dcd', replace

restore

// (E) No special categories
preserve

keep if donor_cat == "ecd"
merge m:1 `matchvars' using `keys'rank_categories_preKAS_ecd.dta, keep(master match) nogen
tempfile temp_ecd
save `temp_ecd', replace

restore

// (F) Same. Since this is the last category, don't preserve

keep if donor_cat == "ecd_dcd"
sort ABDR ABO Geo

merge m:1 ABDR ABO Geo using `keys'rank_categories_preKAS_ecd_dcd.dta, keep(master match) nogen

append using `temp_young_healthy' `temp_old_healthy' `temp_young_dcd' `temp_old_dcd' `temp_ecd'

end
