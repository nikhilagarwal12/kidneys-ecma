function chi = chi_param_update(data,model,kk,where_ii)

    % This function simply updates the flexible (UH-dependent) part of the
    % chi matrix
    if nargin==3
        chi = zeros(data.N_offers,model.K_types*model.flex.K);
        chi(:,(kk-1)*model.flex.K+(1:model.flex.K)) = data.flex.obs;
    else    
        chi = zeros(length(where_ii),model.K_types*model.flex.K);
        chi(:,(kk-1)*model.flex.K+(1:model.flex.K)) = data.flex.obs(where_ii,:);
    end

end