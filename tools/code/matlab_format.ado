// Format for matlab

  program matlab_format
    args date_vars

      format * %12.0g
   
      foreach x of varlist * {
        replace `x'=-99 if `x'==.
      } 

     // format date vars so MATLAB can read them
     foreach x of varlist `date_vars' {
       di "`x'"
       replace `x'=. if `x'==-99
       format `x' %tdNN/DD/YY
     }
  end

