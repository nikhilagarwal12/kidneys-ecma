// DO NOT run on own; it should be called within gen_primary_files.do
// Estimates "timeout" model for maximum number of offers
// Timing: 
//        1. There are ki_available_nyrt kidneys available. The offer pointer is at the first patient on the list. 
//        2. Make an offer to this patient 
//        3. Agent decides whether to accept the offer. If she accepts, the number of kidneys goes down by one.
//        4. Nature decides if one kidney timed-out.  If there is a timeout, the number of available kidneys goes down by one.
//        5. Stop if there are no more kidneys available; otherwise, move the pointer to the next patient and go to 2.

clear

// Count the total number of screened and unscreened offers, by kidney number
use "`file_dir'simulated_offers_matlab.dta", clear
keep offer* position* screened donor_id accept_yn

* Keep offers and sort by position
drop if offered_dat == 0 & screened == 0
sort donor_id position_sim

* Count offers
by donor_id: gen offer_count    = _N
by donor_id: gen last_offer     = _n==_N
by donor_id: egen n_accept      = total(accept_yn)

keep if last_offer

* Merge with deceased_donors
merge m:1 donor_id using "`file_dir'deceased_donors.dta", keepusing(ki_available_nyrt num_ki_disc) keep(match) nogenerate

keep donor_id offer_count n_accept num_ki_disc ki_available_nyrt accept_yn

* The number of offers is censored iff ki_available_nyrt =< n_accept (no discards)
gen censored_donor = ki_available_nyrt <= n_accept 
* Cases dealt with so far:
*       ki_available_nyrt==1 & n_accept==0  -> Not censored (the only kidney available timed-out; case 1)
*       ki_available_nyrt==1 & n_accept==1  -> Censored  (the only kidney available was accepted; there was no timeout; case 2)
*       ki_available_nyrt==2 & n_accept==1 & accept_yn ==0  -> Censored  (one kidneys was accepted; the other timed-out; case 5)
*       ki_available_nyrt==2 & n_accept==2  -> Censored  (both kidneys were accepted; there was no timeout; case 6)

* Now deal with cases with discards (ki_available_nyrt > n_accept  &  no censoring)
* ki_available_nyrt==2 & n_accept==0, there were two timeouts. The first one happened at or before offer_count (Case 3: uniform)
* ki_available_nyrt==2 & n_accept==1 & last offer accepted. There was one timeout. The first one happened at or before offer_count (Case 4: uniform)
set seed 1
replace offer_count = ceil(offer_count * uniform()) if ki_available_nyrt==2 & (n_accept==0  | (n_accept==1 & accept_yn))

* Keep donor_id, offer_count, ki_available_nyrt and censored_donor
keep donor_id ki_available_nyrt censored_donor offer_count num_ki_disc

tempfile n_offers_donor
save `n_offers_donor', replace

* Figure out donor characteristics we need
use "`file_dir'offers", clear

* Sample cuts
keep if match_date>=`nyrt_start_date' & match_date<=`nyrt_end_date'
keep if real_offer == 1
isid donor_id wl_id_code

* Sort
sort donor_id match_run_num organ_sequence_num

* Other variables
gen nyrt_donor = opo_dsa_id == "NYRT-OP1"
gen regional_donor = opo_dsa_id ~="NYRT-OP1" & opo_region_id == 9
gen non_nyrt_local_match = opo_dsa_id~="NYRT-OP1" & local_match
gen dcd = non_hrt_don == "Y"

* Collapse and merge with offers
collapse (max) nyrt_donor regional_donor non_nyrt_local_match dcd ecd_donor, by(donor_id)
merge 1:m donor_id using `n_offers_donor',  keep(match) nogenerate assert(match using)

* Generate which donors were discarded after censoring
gen discarded_donor = num_ki_disc~=0 if ~censored_donor

* Set up the Y variables for the interval regression
replace offer_count = 1 if offer_count == 0 
gen log_offers = log(offer_count)
gen left_log_offers = log_offers
gen right_log_offers = log_offers if censored_donor==0

* Set up the X variables
gen nyrt_dcd_donor = nyrt_donor*dcd
gen regional_dcd_donor = regional_donor*dcd
gen non_nyrt_local_match_dcd_donor = non_nyrt_local_match*dcd
gen nyrt_ecd_donor = nyrt_donor*ecd_donor
gen regional_ecd_donor = regional_donor*ecd_donor
gen non_nyrt_local_match_ecd_donor = non_nyrt_local_match*ecd_donor

* Regresion output
label variable nyrt_donor "Donor Recovered in NYRT"
label variable nyrt_dcd_donor "DCD Donor Recovered in NYRT"
label variable nyrt_ecd_donor "ECD Donor Recovered in NYRT"
label variable regional_donor "Donor Recovered in Region"
label variable regional_dcd_donor "Regional DCD Donor"
label variable regional_ecd_donor "Regional ECD Donor"
label variable non_nyrt_local_match "Non-NYRT Local Match Run"
label variable non_nyrt_local_match_dcd_donor "DCD Non-NYRT Local Match Run"
label variable non_nyrt_local_match_ecd_donor "ECD Non-NYRT Local Match Run"
label variable dcd "DCD Donor"
label variable ecd_donor "ECD Donor"

* Interval regression
eststo, title("Interval Regression"): intreg left_log_offers right_log_offers dcd ecd_donor nyrt* regional* non_nyrt*, het(dcd nyrt* regional* non_nyrt*)

* Predict
predict mean_log_offers, xb
predict sd_log_offers, equation(lnsigma)

* Hazard Model
stset offer_count, failure(censored_donor == 0)
eststo, title("Exponential Hazard"): streg dcd ecd_donor nyrt* regional* non_nyrt* ki_available_nyrt, d(exponential)

predict timeout_hazard, hazard
gen e_offers = 1/timeout_hazard

* Remove the duplicates on donors
duplicates drop donor_id, force

* Probability of discard
probit discarded_donor dcd ecd_donor nyrt* regional* non_nyrt*
predict p_discard

* Merge with donor file
keep donor_id mean_log_offers sd_log_offers timeout_hazard p_discard 
tempfile offer_censor
save `offer_censor', replace

* Append to deceased_donors.dta
use "`file_dir'deceased_donors", clear
capture drop mean_log_offers sd_log_offers timeout_hazard p_discard 
merge 1:1 donor_id using `offer_censor', nogenerate
save "`file_dir'deceased_donors", replace
