
% Definition of the class sparsedata
classdef sparsedata
    % a sparsedata contains Data, rowNames, and colNames 
    properties
        Data = []; % A structure containing equal-sized (N x M) possibly sparse matrices 
        rowNames = [];  % a (N x 1) vector of row names
        colNames = [];  % a (1 x M) vector of column names
        Details  = struct();    % Arbitrary structure (can be anything)
        rowLabel = 'default_SD_rowNames';
        colLabel = 'default_SD_colNames';
        detLabel = 'default_SD_Details';
    end   % properties block
    
    methods
        function obj = sparsedata(Data,rowNames,colNames,Details,keepfields)   % constructor: it builds an instance of this class
            
            if ~isa(Data,'struct'), error('The first input should be a structure'); end
            if nargin == 2 && isa(rowNames,'cell')
                colNames = rowNames{1};
                rowNames = rowNames{2};
            end

            if isa(rowNames,'char')   % if the first argument is a char, it is a field of the structure;
                obj.rowNames = Data.(rowNames);
                obj.rowLabel = rowNames;
                Data         = rmfield(Data,rowNames);
            else
                obj.rowNames = rowNames;
            end
            
            if isa(colNames,'char')   % if the second argument is a char, it is also a field;
                obj.colNames = Data.(colNames);
                obj.colLabel = colNames;
                Data         = rmfield(Data,colNames);
            else
                obj.colNames = colNames;
            end
            
            if nargin >= 4,        
                if isa(Details,'char'), 
                    obj.Details  = Data.(Details);
                    obj.detLabel = Details;
                    Data         = rmfield(Data,Details);
                else
                    obj.Details = Details;
                end
            end
            obj.Data = Data; 
            if nargin >=5, bj = keepfields(obj,keepfields);  end
            if nargin >=6, warning('Ignoring extra inputs');  end
               
        end  % sparsedata (constructor)
        
        function savesd(a_file,obj)    % Saves a file with all the components for this class
            struct_to_save                 = obj.Data;
            struct_to_save.(obj.rowLabel)  = obj.rowNames;
            struct_to_save.(obj.colLabel)  = obj.colNames;
            struct_to_save.(obj.detLabel)  = obj.Details;
            save(a_file,'-struct','struct_to_save')
        end  % savesd
                
        function obj=rmfield(obj,field)         % Removes a variable/field;
            % https://www.mathworks.com/matlabcentral/answers/1904-whats-a-fast-method-for-keeping-only-a-subset-of-fields-from-a-structure
            obj.Data = rmfield(obj.Data,field);
        end  % rmfield
        
        function obj = keepfields(obj,keepfields);  % keeps only keepfields;
            for f = fieldnames(obj.Data)'
                f = f{1};
                if ~ismember(f,keepfields), obj = rmfield(obj,f); end
            end
        end  
        
        
        ;
        function varargout = subsref(obj,s)   % Overloads subreferencing
           switch s(1).type
               case '{}'     % Define what happens when {}
                    if iscellstr(s.subs) && all(ismember(s.subs,fieldnames(obj.Data)))   % If referencing variable/field names
                       varargout = {keepfields(obj,s.subs)};
                    else   % otherwise, apply the indexing matrix by matrix.
                        s.type = '()';
                        % Implement obj(indices)
                        % Apply subindexing to rowNames and colNames
                        for f = fieldnames(obj.Data)' % Apply the subindexing to each matrix
                            f = f{1};
                            obj.Data.(f) = builtin('subsref',obj.Data.(f),s); 
                            if numel(s.subs)==1, obj.Data.(f) = full(obj.Data.(f)); end
                        end
                        if numel(s.subs)>1
                            srows = s; srows.subs = srows.subs(1);
                            obj.rowNames = builtin('subsref',obj.rowNames,srows);
                            scols = s; scols.subs = scols.subs(2);
                            obj.colNames = builtin('subsref',obj.colNames,scols);
                            varargout = {obj};
                        else
                            aux = 1:[numel(obj.rowNames)*numel(obj.colNames)];
                            aux = builtin('subsref',aux,s);
                            [rows,cols] = ind2sub([numel(obj.rowNames),numel(obj.colNames)],aux);
                            obj.Data.(obj.rowLabel) = obj.rowNames(rows);
                            obj.Data.(obj.colLabel) = obj.colNames(cols);
                            varargout = {obj.Data};
                        end
                    end
               case '.'  % Augment the properties that can be accessed with .
                    if strcmp(s(1).subs,obj.rowLabel)   % Labels
                        s.subs = 'rowNames';
                        varargout = {builtin('subsref',obj,s)};
                    elseif strcmp(s(1).subs,obj.colLabel)    
                        s.subs = 'colNames';
                        varargout = {builtin('subsref',obj,s)};
                    elseif strcmp(s(1).subs,obj.detLabel)
                        s.subs = 'Details';
                        varargout = {builtin('subsref',obj,s)};
                    elseif ismember(s(1).subs,fieldnames(obj.Data))  % Variables: implement obj{index} and return just the data!
                        s.type = '{}';
                        obj = subsref(obj,s);  % runs obj{index}
                        varargout = {obj.Data.(s.subs)}; % gets just the data
                    else
                        varargout = {builtin('subsref',obj,s)};
                    end     
               otherwise
                        varargout = {builtin('subsref',obj,s)};
           end  % switch
       end  % subsref
           
       function obj = lookup(obj,what,indexing)
           % Different types of indexing
           if isa(what,'char')   % smart indexing, only rows or only columns
                if strcmp(what,'rows') || strcmp(what,obj.rowLabel)   % get only some rows
                    found = ismember(obj.rowNames,indexing);
                    s.subs = {found,':'};
                elseif strcmp(what,'cols') || strcmp(what,obj.colLabel)   % get only some cols
                    found = ismember(obj.colNames,indexing); 
                    s.subs = {':',found};
                else
                    error('I could not recognize the indexing dimension %s',what)
                end
                skip = all(found);   % do not call subref if it will be inconsequential.
           else % get some specific cells
                [found_row,where_row] = ismember(what,obj.rowNames);
                [found_col,where_col] = ismember(indexing,obj.colNames);
                found = found_row & found_col;
                linearIndex = sub2ind([numel(obj.rowNames),numel(obj.colNames)], where_row(found), where_col(found));
                s.subs = {linearIndex};
                skip = numel(unique(where_row(found_col))) == numel(obj.colNames) && numel(unique(where_row(found_col))) == numel(obj.colNames);
           end
           if ~skip  % subindexing will have an effect.
               s.type = '{}';
               obj = subsref(obj,s); % because subsref(obj(where(found),:), or similar calls built-in; 
           end
       end  % lookup
       
       function obj = or(obj1,obj2) % overload or: union of two sets
           
           [ismemRow,whereRow] = ismember(obj2.rowNames,obj1.rowNames);
           [ismemCol,whereCol] = ismember(obj2.rowNames,obj1.rowNames);
           rowNames = [obj1.rowNames(:); obj2.rowNames(~ismemRow)];
           colNames = [obj1.colNames(:); obj2.colNames(~ismemRow)];
           for f = fieldnames(obj1)'
                f=f{1};
                Data.(f) = [obj1.Data.(f),                    obj2.Data.(f)(whereRow(ismemRow),~ismemCol)
                            obj2.Data.(f)(~ismemRow,whereCol(ismemCol)),obj2.Data.(f)(~ismemRow,~ismemCol) ];
           end
           obj = sparsedata(Data,rowNames,colNames,{obj1.Details,'or',obj2.Details});           
       end
    
    end   % methods block
end