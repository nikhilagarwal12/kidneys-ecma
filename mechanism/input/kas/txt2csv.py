# -*- coding: utf-8 -*-

# Organize Raw KAS files from txt to formatted csv

import csv 
import re
list_of_files = ['KDPI_20','KDPI_20_35','KDPI_35_85','KDPI_85']
# Fields in the csv file
fnames    = ['num','geo','hlamis','cpra','abo_pat','prior_living_donor','Ped_List','Ped_Alloc','Top20epts','Top20epts_or_Ped','abo_don','unmatched']

for f in list_of_files:
    csv_file = file('../../mechanism/input/kas/'+f+'.csv','wb')  
    csv_writer = csv.DictWriter(csv_file,delimiter = ',', dialect='excel', fieldnames = fnames, extrasaction='ignore')
    csv_writer.writeheader()
    with open('../../mechanism/input/kas/'+f+'.txt','rb') as tx:
        bins = dict()
        for line in tx:
            bins['num']        = int(line.strip())
            geo                = re.sub('OPO.?.?.?s','OPOs',tx.next()).strip()
            bin_crt            = tx.next().strip()
            bins['abo_don']    = tx.next().strip().title()             
            
            # Process fields
            bin_crt = bin_crt.split(',')
            unmatched = ''
            
            pat_abo,pat_cpra,hlamis = ['','','']
            for crt in bin_crt:
                # Patient ABO
                found_pat_abo = re.search(',?( and)? ?[bB]lood type',crt)
                found_cpra    = re.search(',?CPRA',crt)
                found_hlamis    = re.search(',?ABDR',crt)
                if found_pat_abo:
                    pat_abo = crt[found_pat_abo.end()+1:].strip()
                elif found_cpra:
                    pat_cpra =  crt[found_cpra.end()+1:].strip()
                elif found_hlamis:
                    hlamis  =  crt.strip()
                else:
                    unmatched = '' + crt
            
            # Re-name fields
            if geo.find('Nation')>=0:
                bins['geo'] = 'National'
            elif geo.find('local')>=0:
                bins['geo'] = 'Local'
            elif geo.find('region')>=0:
                bins['geo'] = 'Regional'                    
            else:
                bins['geo'] = 'Unknown'    
            
            bins['hlamis']  = "Zero" if hlamis == '0-ABDR mismatch' else 'Any'
                   
            if pat_abo.find('permissible')>=0:
                bins['abo_pat'] = 'Compatible'
            else:
                bins['abo_pat'] = pat_abo.title()

            unmatched = unmatched.replace('years at','years old at')
            unmatched = unmatched.replace('time of the match','time of match')
            unmatched = unmatched.replace('time of match run','time of match')
            unmatched = unmatched.replace('at time of','at the time of')
            unmatched = unmatched.replace('than 18 at','than 18 years old at')

            if pat_cpra in ['equal 100%','equal to 100%'] :
                bins['cpra'] = '100%'
            elif pat_cpra.find('99%')>=0:
                bins['cpra'] = '99%'
            elif pat_cpra.find('98%')>=0:
                bins['cpra'] = '98%'
            else:
                bins['cpra'] = re.sub('[^\d%]','',pat_cpra)
            bins['unmatched'] = unmatched

            bins['Ped_List']  = 'Ped At Listing' if unmatched.find('Registered prior to 18 years')>=0 else 'Any'
            bins['prior_living_donor']  = '1    ' if unmatched.find('Prior living donor')>=0 else 'Any'
            Top20epts = unmatched.find('Top 20% EPTS')>=0
            ped_at_alloc = unmatched.find('less than 18')>=0
            bins['Top20epts']  = 'Top 20% EPTS' if Top20epts and not ped_at_alloc else 'Any'
            bins['Ped_Alloc']  = 'Ped At Allocation' if ped_at_alloc and not Top20epts else 'Any'
            bins['Top20epts_or_Ped']  = '1' if ped_at_alloc and Top20epts else 'Any'           

            # Write dict in file    
            csv_writer.writerow(bins)
    csv_file.close()


            

        