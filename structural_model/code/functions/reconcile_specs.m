% should be run during tasks.validate_ccps in structural model code

   % reconcile specs if needed
   N0 = size(data0.fixed.obs,1); N1 = size(data1.fixed.obs,1);

   dim = length(data.fixed.colheaders); dim0 = length(data0.fixed.colheaders); dim1 = length(data1.fixed.colheaders);
   missing = false(dim,1); missing0 = false(dim0,1); missing1 = false(dim1,1);
   for ii = 1:dim0
       missing0(ii) = ~any(strcmp(data0.fixed.colheaders{ii},data.fixed.colheaders));
   end
   find(missing0)
   data0.fixed.obs = data0.fixed.obs(:,~missing0);
   data0.fixed.colheaders = data0.fixed.colheaders(~missing0);
   data0.fixed.mu_dm = data0.fixed.mu_dm(~missing0);
   data0.fixed.sigma_dm = data0.fixed.sigma_dm(~missing0);
   model0.fixed.colheaders = model0.fixed.colheaders(~missing0);
   model0.fixed.mu_dm = model0.fixed.mu_dm(~missing0);
   model0.fixed.sigma_dm = model0.fixed.sigma_dm(~missing0);

   for ii = 1:dim1
       missing1(ii) = ~any(strcmp(data1.fixed.colheaders{ii},data.fixed.colheaders));
   end
   find(missing1)
   data1.fixed.obs = data1.fixed.obs(:,~missing1);
   data1.fixed.colheaders = data1.fixed.colheaders(~missing1);
   data1.fixed.mu_dm = data1.fixed.mu_dm(~missing1);
   data1.fixed.sigma_dm = data1.fixed.sigma_dm(~missing1);
   model1.fixed.colheaders = model1.fixed.colheaders(~missing1);
   model1.fixed.mu_dm = model1.fixed.mu_dm(~missing1);
   model1.fixed.sigma_dm = model1.fixed.sigma_dm(~missing1);

   for ii = 1:dim
       if ~any(strcmp(data.fixed.colheaders{ii},data0.fixed.colheaders));
       assert(ii <= size(data0.fixed.obs,2));
           if ii==1 
		  data0.fixed.obs = [zeros(N0,1) data0.fixed.obs]; 
		  data0.fixed.colheaders = [data.fixed.colheaders{ii} data0.fixed.colheaders];
                  data0.fixed.mu_dm = [0 data0.fixed.mu_dm];
                  data0.fixed.sigma_dm = [0 data0.fixed.sigma_dm];
		  model0.fixed.colheaders = [model.fixed.colheaders{ii} model0.fixed.colheaders];
                  model0.fixed.mu_dm = [0 model0.fixed.mu_dm];
                  model0.fixed.sigma_dm = [0 model0.fixed.sigma_dm];
	   end
	   if ii==size(data0.fixed.obs,2)
		  data0.fixed.obs = [data0.fixed.obs zeros(N0,1)]; 
		  data0.fixed.colheaders = [data0.fixed.colheaders data.fixed.colheaders{ii}];
                  data0.fixed.mu_dm = [data0.fixed.mu_dm 0];
                  data0.fixed.sigma_dm = [data0.fixed.sigma_dm 0];
		  model0.fixed.colheaders = [model0.fixed.colheaders model.fixed.colheaders{ii}];
                  model0.fixed.mu_dm = [model0.fixed.mu_dm 0];
                  model0.fixed.sigma_dm = [model0.fixed.sigma_dm 0];
	   end
	   if ii>1 & ii < size(data0.fixed.obs,2)
		  data0.fixed.obs = [data0.fixed.obs(:,1:(ii-1)) zeros(N0,1) data0.fixed.obs(:,(ii:end))];
		  data0.fixed.colheaders = [data0.fixed.colheaders(1:(ii-1)) data.fixed.colheaders{ii} data0.fixed.colheaders(ii:end)];
                  data0.fixed.mu_dm = [data0.fixed.mu_dm(1:(ii-1)) 0 data0.fixed.mu_dm(ii:end)];
                  data0.fixed.sigma_dm = [data0.fixed.sigma_dm(1:(ii-1)) 0 data0.fixed.sigma_dm(ii:end)];
		  model0.fixed.colheaders = [model0.fixed.colheaders(1:(ii-1)) model.fixed.colheaders{ii} model0.fixed.colheaders(ii:end)];
                  model0.fixed.mu_dm = [model0.fixed.mu_dm(1:(ii-1)) 0 model0.fixed.mu_dm(ii:end)];
                  model0.fixed.sigma_dm = [model0.fixed.sigma_dm(1:(ii-1)) 0 model0.fixed.sigma_dm(ii:end)];
	   end           
       end
   end

   for ii = 1:dim
       if ~any(strcmp(data.fixed.colheaders{ii},data1.fixed.colheaders));
           assert(ii <= size(data1.fixed.obs,2));
           if ii==1 
		  data1.fixed.obs = [zeros(N1,1) data1.fixed.obs]; 
		  data1.fixed.colheaders = [data.fixed.colheaders{ii} data1.fixed.colheaders];
                  data1.fixed.mu_dm = [0 data1.fixed.mu_dm];
                  data1.fixed.sigma_dm = [0 data1.fixed.sigma_dm];
		  model1.fixed.colheaders = [model.fixed.colheaders{ii} model1.fixed.colheaders];
                  model1.fixed.mu_dm = [0 model1.fixed.mu_dm];
                  model1.fixed.sigma_dm = [0 model1.fixed.sigma_dm];
	   end
	   if ii==size(data1.fixed.obs,2)
		  data1.fixed.obs = [data1.fixed.obs zeros(N1,1)]; 
		  data1.fixed.colheaders = [data1.fixed.colheaders data.fixed.colheaders{ii}];
                  data1.fixed.mu_dm = [data1.fixed.mu_dm 0];
                  data1.fixed.sigma_dm = [data1.fixed.sigma_dm 0];
		  model1.fixed.colheaders = [model1.fixed.colheaders model.fixed.colheaders{ii}];
                  model1.fixed.mu_dm = [model1.fixed.mu_dm 0];
                  model1.fixed.sigma_dm = [model1.fixed.sigma_dm 0];
	   end
	   if ii>1 & ii < size(data1.fixed.obs,2)
		  data1.fixed.obs = [data1.fixed.obs(:,1:(ii-1)) zeros(N1,1) data1.fixed.obs(:,(ii:end))];
		  data1.fixed.colheaders = [data1.fixed.colheaders(1:(ii-1)) data.fixed.colheaders{ii} data1.fixed.colheaders(ii:end)];
                  data1.fixed.mu_dm = [data1.fixed.mu_dm(1:(ii-1)) 0 data1.fixed.mu_dm(ii:end)];
                  data1.fixed.sigma_dm = [data1.fixed.sigma_dm(1:(ii-1)) 0 data1.fixed.sigma_dm(ii:end)];
		  model1.fixed.colheaders = [model1.fixed.colheaders(1:(ii-1)) model.fixed.colheaders{ii} model1.fixed.colheaders(ii:end)];
                  model1.fixed.mu_dm = [model1.fixed.mu_dm(1:(ii-1)) 0 model1.fixed.mu_dm(ii:end)];
                  model1.fixed.sigma_dm = [model1.fixed.sigma_dm(1:(ii-1)) 0 model1.fixed.sigma_dm(ii:end)];
	   end           
       end
   end

   model0.fixed.K = model.fixed.K; model1.fixed.K = model.fixed.K;
   model0.K_theta = model.K_theta; model1.K_theta = model.K_theta;
