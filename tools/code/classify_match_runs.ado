// Classify match runs as "full", "within", or "payback" (so not to individual patients)
// rule/assumption: all runs done by the recovering OPO are "full". All runs done by non-local OPOs that don't get offered to other OPOs are "within"
// Also generates match_run_num, which doesn't necessarily go in order


program classify_match_runs

  // find local offers to impute donor sub-unit
  gen local_offer = strpos(lower(descrip),"local")>0
  gen run_loc_ctr = substr(listing_ctr_dsa_id,1,8) if local_offer==1
  bys donor_id match_id_code: replace run_loc_ctr = run_loc_ctr[_n-1] if run_loc_ctr == ""  // fill in backwards
  bys donor_id match_id_code: replace run_loc_ctr = run_loc_ctr[_n+1] if run_loc_ctr == ""  // fill in forwards

  // classify match runs by type
  gen run_type = "payback" if payback==1
  replace run_type = "full" if local==1
  replace run_type = "within" if local==0 & payback!=1
  gen non_local_offer = match_opo_dsa_id != listing_ctr_dsa_id
  bys match_id_code: egen n_non_local_offers = total(non_local_offer)
  replace run_type = "full" if n_non_local_offers > 0 & run_type == "within"
  drop n_non_local_offers

  rename local local_match

  // create match_run_num variable
  tempfile holdon
  save `holdon', replace

  keep donor_id match_id_code local_match match_date
  gsort donor_id -local match_date match_id_code // ordering local runs before non-local runs, and also ordering non-local runs by match date. Partial order.
  keep donor_id match_id_code
  duplicates drop
  by donor_id: gen match_run_num = _n

  merge 1:m match_id_code using `holdon', nogen

end
