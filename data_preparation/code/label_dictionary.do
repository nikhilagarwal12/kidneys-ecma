// called by gen_primary_files.do to convert string to numeric variables for MATLAB
// WARNING: Changing the encoding in this file has downstream effects in MATLAB

// Create a dictionary
  label drop _all
  // Offers
  label define offer_accept 2 "Z" 1 "Y" 0 "N" -1 "B"
  label define organ_placed -1 "L" 1 "R" 2 "B" 0 ""
  label define donor_cat 100 "young_healthy" 110 "young_dcd" 000 "old_healthy" 010 "old_dcd" 001 "ecd" 011 "ecd_dcd"
  label define abo 1 "A" 11 "A1" 12 "A2" 2 "B" 0 "O" 3 "AB" 31 "A1B" 32 "A2B"
  label copy abo abo_don

  label define variance 0 "None" 1 "CADN" 2 "DCTC" 3 "FL" 4 "LAOP" 5 "MWOB" 6 "NCCM" 7 "OKOP" 8 "ORUO" 9 "PADV" 10 "PATF" 11 "R1"  12 "TN" 13 "TXGC" 14 "TXSA" 15 "TXSB" 16 "VATB"
  label define zero_abdr 0 "Zero" 9 "Non-Zero"
  #delimit ;
  label define abo_match 0 "Incompatible" 1 "Compatible" 2 "Identical" 3 "O Donor B Candidate" 4 "Eligible B Candidates/Deceased A2/A2B Donors"
                         5 "Eligible B Candidates/Deceased A2/A2B Donors & O Candidates/Deceased A2 Donors" 6 "A1 or A1B" 7 "A2 or A2B"
                         8 "B Candidates" 9 "B Potential Recipients" 10 "O Potential Recipients" 11 "O Candidates";
  #delimit cr 
  label define geo 0 "OPO" 1 "Local" 2 "Payback" 3 "Regional" 4 "Statewide" 5 "National" 6 "UNOS" 7 "SEOPF" 8 "Region 1" 9 "DCTC" 10 "KAS Local" 11 "KAS Regional" 12 "KAS National"
  label define ped_list 0 "Non-Ped At Listing" 1 "Ped At Listing" 2 "KAS Pediatric"
  label define ped_alloc 0 "Non-Ped At Allocation" 1 "Ped At Allocation" 2 "KAS Pediatric"
  label define cpra_cat 20 "CPRA<=20" 79 "21<=CPRA<=79" 100 "CPRA>=80" 80 " < 80" 81 " >= 80"
  label define epts 1 "0%-20% EPTS or less than 18 years old, " 2 "0%-20% EPTS, " 3 "21%-100% EPTS, "

  label copy zero_abdr zero_abdr_t
  label copy abo_match abo_match_t 
  label copy abo_match c_abo
  label copy geo geo_t
  label copy ped_list ped_list_t
  label copy ped_alloc ped_alloc_t
  label copy cpra_cat cpra_cat_t
  label copy epts epts_t

  label define run_type 1 "full" 2 "within" 3 "payback"
  label define gender 1 "M" 2 "F"
  label copy gender gender_don

  // Yes-No variables
  label define yes_no 1 "Y" 0 "N" -1 "U"

  label copy yes_no prev_tx
  label copy yes_no prev_ki_tx
  label copy yes_no prev_pa_tx
  label copy yes_no malig_trr
  label copy yes_no work_income_trr
  label copy yes_no first_wk_dial
  label copy yes_no pre_tx_txfus 
  label copy yes_no resum_maint_dial
  label copy yes_no work_income_tcr
  label copy yes_no drugtrt_copd
  label copy yes_no perip_vasc
  label copy yes_no malig_tcr_ki
  label copy yes_no pulm_embol
  label copy yes_no dial_trr
  label copy yes_no grf_stat_ki
  label copy yes_no grf_stat_pa
  label copy yes_no rej_chronic_ki
  label copy yes_no bk_virus_ki
  label copy yes_no dwfg_ki
  label copy yes_no mrcreatg
  label copy yes_no px_non_compl_ki
  label copy yes_no trtrej1y_ki
  label copy yes_no multiorg
  label copy yes_no ven_ext_grf
  label copy yes_no malig_tcr_pa
  label copy yes_no malig
  label copy yes_no rej_biopsy
  label copy yes_no exh_perit_access
  label copy yes_no exh_vasc_access
  label copy yes_no on_dialysis
  label copy yes_no wlhl
  label copy yes_no wlin
  label copy yes_no wlki
  label copy yes_no wlkp
  label copy yes_no wlli
  label copy yes_no wllu
  label copy yes_no wlpa
  label copy yes_no wlpi
  label copy yes_no wlhr
  label copy yes_no data_transplant
  label copy yes_no data_waitlist
  label copy yes_no prev_pi_tx_tcr_new
  label copy yes_no trtrej6m_ki
  label copy yes_no grf_vasc_thromb_pa
  label copy yes_no recov_out_us_don
  label copy yes_no controlled_don
  label copy yes_no core_cool_don
  label copy yes_no non_hrt_don
  label copy yes_no consent_writ_doc_intent_don
  label copy yes_no anticonv_don
  label copy yes_no antihype_don
  label copy yes_no blood_inf_conf_don
  label copy yes_no cardarrest_postneuro_don
  label copy yes_no clin_infect_don
  label copy yes_no ddavp_don
  label copy yes_no inotrop_agents_don
  label copy yes_no other_inf_conf_don
  label copy yes_no protein_urine_don
  label copy yes_no pt_diuretics_don
  label copy yes_no heparin_don
  label copy yes_no pt_steroids_don
  label copy yes_no pt_t3_don
  label copy yes_no pt_t4_don
  label copy yes_no pulm_inf_conf_don
  label copy yes_no urine_inf_conf_don
  label copy yes_no vasodil_don
  label copy yes_no arginine_don
  label copy yes_no insulin_don
  label copy yes_no inotrop_support_don
  label copy yes_no alcohol_heavy_don
  label copy yes_no contin_cig_don
  label copy yes_no contin_cocaine_don
  label copy yes_no contin_oth_drug_don
  label copy yes_no diet_don
  label copy yes_no diuretics_don
  label copy yes_no extracranial_cancer_don
  label copy yes_no hist_cig_don
  label copy yes_no hist_cocaine_don
  label copy yes_no hist_oth_drug_don
  label copy yes_no intracranial_cancer_don
  label copy yes_no other_hypertens_med_don
  label copy yes_no skin_cancer_don
  label copy yes_no tattoos_don
  label copy yes_no cdc_risk_hiv_don
  label copy yes_no history_mi_don
  label copy yes_no lt_ki_biopsy_don
  label copy yes_no li_biopsy_don
  label copy yes_no kil_pump
  label copy yes_no rt_ki_biopsy_don
  label copy yes_no kir_pump
  label copy yes_no wall_abn_seg_don
  label copy yes_no wall_abn_glob_don
  label copy yes_no pulm_cath_don
  label copy yes_no abn_valves_don
  label copy yes_no abn_congen_don
  label copy yes_no abn_lvh_don
  label copy yes_no po2_done_don
  label copy yes_no hist_cancer_don
  label copy yes_no hist_hypertens_don
  label copy yes_no med_exam_report
  label copy yes_no med_exam_perm
  label copy yes_no coronary_angio_don
  label copy yes_no kib_pump
  label copy yes_no prelim_xmatch_req  

  label define yes_no_unc 0 "N" 1 "Y" -99 ""
  label copy yes_no_unc legally_brain_dead
  label copy yes_no_unc acpt_organ_proc_othteam
  label copy yes_no_unc doncrit_acpt_dcd
  label copy yes_no_unc doncrit_acpt_dcd_import
  label copy yes_no_unc doncrit_acpt_hist_diabetes
  label copy yes_no_unc doncrit_acpt_hist_hypertension
  label copy yes_no_unc doncrit_acpt_hbcorepos
  label copy yes_no_unc doncrit_acpt_hcvpos
  label copy yes_no_unc doncrit_acpt_htlv_pos

  label define c_accept 1 "Yes" 2 "Bypass" 3 "Inactive" 4 "Technological" 5 "Unacceptable" 6 "Negligence" 7 "Screen" 8 "No" 9 "Non-Response"
  label copy c_accept c_accept_810

  // Patient File
  label define txhrt 1 "Y" 2 "S" 3 "W" 4 "E" 5 "L" 6 "R" 7"D"
  label copy txhrt txint
  label copy txhrt txkid
  label copy txhrt txliv
  label copy txhrt txlng
  label copy txhrt txpan

  label define use_which_pra 1 "C" 2 "P"
  
  label define organ 1 "KI" 2 "KP" 3 "PA"
  label copy organ wl_org
  
  label define cmv_igg 1 "C" 2 "I" 3 "N" 4 "ND" 5 "P" 6 "U" 7 "PD"
  label copy cmv_igg cmv_igm
  label copy cmv_igg ebv_serostatus
  label copy cmv_igg hbv_core
  label copy cmv_igg hbv_sur_antigen
  label copy cmv_igg hcv_serostatus
  label copy cmv_igg cmv_don

  label define tx_type 1 "PAK" 2 "PTA" 3 "PWK" 4 "SKP"
  
  label define payback 1 "Y" 0 "N"
  
  label define age_group 1 "A" 2 "P"
  
  label define px_stat 1 "A" 2 "L" 3 "D" 4 "R"

  // Donor File
  label copy cmv_igg hbsab_don
  label copy cmv_igg hbv_sur_antigen_don
  label copy cmv_igg ebv_igg_cad_don
  label copy cmv_igg ebv_igm_cad_don
  label copy cmv_igg hbv_core_don
  label copy cmv_igg hep_c_anti_don
  label copy cmv_igg ebna_don
  label copy cmv_igg htlv_don
  label copy cmv_igg vdrl_don
  label copy cmv_igg htlv1_old_don
  label copy cmv_igg htlv2_old_don

  // Patient History
  label define chg_ty 1 "A" 2 "D" 3 "M"

  // Geography and centers/OPOs
#delimit ;
label define state  1 "AK" 2 "AL" 3 "AR" 4 "AS" 5 "AZ" 6 "CA" 7 "CO" 8 "CT" 9 "DC"
10 "DE" 11 "FL" 12 "GA" 13 "GU"	14 "HI" 15 "IA" 16 "ID" 17 "IL"
18 "IN" 19 "KS" 20 "KY" 21 "LA" 22 "MA" 23 "MD" 24 "ME" 25 "MI"
26 "MN"	27 "MO" 28 "MS" 29 "MT" 30 "NA" 31 "NC" 32 "ND" 33 "NE"
34 "NH" 35 "NJ" 36 "NM"	37 "NV" 38 "NY" 39 "OH" 40 "OK" 41 "OR"
42 "PA" 43 "PR" 44 "RI" 45 "SC" 46 "SD" 47 "TN" 48 "TX" 49 "UT"
50 "VA" 51 "VI" 52 "VT" 53 "WA" 54 "WI" 55 "WV"
56 "WY" 57 "ZZ" 58 "MP";
#delimit cr
  
  label copy state perm_state_trr
  label copy state perm_state
  label copy state home_state_don

#delimit ;
label define listing_ctr_dsa_id 1 "OHOV-OP1" 2 "MAOB-OP1" 4 "WIUW-IO1"
  5 "NYSB-IO1"  6 "PATF-OP1"  7 "TNDS-OP1"  8 "PADV-OP1"  9 "OHLP-OP1"
 10 "CAOP-OP1" 11 "TXSB-OP1" 12 "NYRT-OP1" 13 "CARO-OP1" 14 "SCOP-OP1"
 15 "CADN-OP1" 16 "ILIP-OP1" 17 "TXSA-OP1" 18 "GAMC-IO1" 19 "AZOB-OP1"
 20 "KYDA-OP1" 21 "CTOP-OP1" 22 "NMOP-OP1" 23 "OKHM-IO1" 24 "INOP-OP1"
 25 "CORS-OP1" 26 "MWOB-OP1" 27 "MOMA-OP1" 28 "FLFH-IO1" 29 "CAGS-OP1"
 30 "OHLC-OP1" 31 "FLMP-OP1" 32 "VAOP-OP1" 33 "MIOP-OP1" 34 "FLUF-IO1"
 35 "MNOP-OP1" 36 "WVMS-OP1" 37 "VATB-OP1" 38 "NCNC-OP1" 39 "NYAP-OP1"
 40 "GALL-OP1" 41 "TXGC-OP1" 42 "WIDN-OP1" 43 "WANW-OP1" 44 "ILCL-OP1"
 45 "CASD-IO1" 46 "OKOP-OP1" 47 "NEOR-OP1" 48 "OHLB-OP1" 49 "ALOB-OP1"
 50 "DCTC-OP1" 51 "WASH-IO1" 52 "TXFW-IO1" 53 "IAIV-IO1" 54 "NJTO-OP1"
 55 "ORUO-IO1" 56 "NVLV-OP1" 57 "TXLG-IO1" 58 "MDPC-OP1" 59 "MSOP-OP1"
 60 "UTOP-OP1" 61 "AROR-OP1" 62 "FLSW-OP1" 63 "NYFL-IO1" 64 "TXAD-IO1"
 65 "FLWC-OP1" 66 "TNMS-OP1" 67 "LAOP-OP1" 68 "MNRC-OP1" 69 "WISL-IO1"
 70 "WALC-OP1" 71 "NCCM-IO1" 72 "PRLL-OP1" 73 "TXBC-IO1" 74 "IAOP-OP1"
 75 "NCBG-IO1" 76 "OHMV-IO1" 77 "ZCAN-FO1" 78 "UNKN-OP1" 79 "NYDP-OP1"
 80 "NYWN-OP1" 81 "HIOP-OP1" 82 "TNET-OP1" 83 "LAOC-OP1" 84 "LAOS-OP1"
 85 "ZFOR-FO1" 86 "NYRC-OP1" 87 "ILCR-OP1" 88 "VAFH-IO1" 89 "ZGER-FO1"
 90 "LASU-IO1" 91 "FL" 92 "TN";
#delimit cr

  label copy listing_ctr_dsa_id opo_dsa_id
  label copy listing_ctr_dsa_id run_opo_id
  label copy listing_ctr_dsa_id tx_ctr_dsa_id

#delimit ;
label define listing_ctr_id  1 "NVUM-TX1" 2 "NCBG-TX1" 3 "CASV-TX1" 4 "MNUM-TX1" 5 "VAUV-TX1"
 6 "CALL-TX1" 7 "CASD-TX1" 8 "CASH-TX1" 9 "MISM-TX1"
 10 "LAOF-TX1" 11 "TXHS-TX1" 12 "NJBI-TX1" 13 "NJSB-TX1"
 14 "MIUM-TX1" 15 "OHOU-TX1" 16 "MDUM-TX1" 17 "NJRW-TX1"
 18 "CAPM-TX1" 19 "CASU-TX1" 20 "TXTY-TX1" 21 "ILUC-TX1"
 22 "MISJ-TX1" 23 "DCGU-TX1" 24 "TXTX-TX1" 25 "MAMG-TX1"
 26 "PAHH-TX1" 27 "NJHK-TX1" 28 "MOUM-TX1" 29 "TXHH-TX1"
 30 "MDJH-TX1" 31 "MABI-TX1" 32 "NYUM-TX1" 33 "MAPB-TX1"
 34 "VAMC-TX1" 35 "TNUT-TX1" 36 "KSUK-TX1" 37 "AZMC-TX1"
 38 "MIHH-TX1" 39 "ILPL-TX1" 40 "OHUH-TX1" 41 "CTYN-TX1"
 42 "TXHI-TX1" 43 "NYMA-TX1" 44 "ARBH-TX1" 45 "CAUC-TX1"
 46 "MIBH-TX1" 47 "CASF-TX1" 48 "ALUA-TX1" 49 "NCCM-TX1"
 50 "TNVU-TX1" 51 "PAAG-TX1" 52 "NYMS-TX1" 53 "MIHF-TX1"
 54 "FLFH-TX1" 55 "RIRH-TX1" 56 "MAUM-TX1" 57 "PATJ-TX1"
 58 "NYDS-TX1" 59 "WAVM-TX1" 60 "NYUC-TX1" 61 "AZGS-TX1"
 62 "GAEM-TX1" 63 "OHTC-TX1" 64 "WAUW-TX1" 65 "NYNY-TX1"
 66 "COSL-TX1" 67 "NCDU-TX1" 68 "MOBH-TX1" 69 "IAIV-TX1"
 70 "ILNM-TX1" 71 "MOSL-TX1" 72 "PAUP-TX1" 73 "GAMC-TX1"
 74 "NYSL-TX1" 75 "AZUA-TX1" 76 "FLJM-TX1" 77 "CAUH-TX1"
 78 "PAPT-TX1" 79 "CASG-TX1" 80 "SCMU-TX1" 81 "VANG-TX1"
 82 "IAIV-VA1" 83 "TNMH-TX1" 84 "MORH-TX1" 85 "DCWH-TX1"
 86 "TNJC-TX1" 87 "WIUW-TX1" 88 "MIMC-TX1" 89 "INIM-TX1"
 90 "OHAC-TX1" 91 "OHCC-TX1" 92 "CASJ-TX1" 93 "TXAS-TX1"
 94 "NYBU-TX1" 95 "ILMM-TX1" 96 "MOLH-TX1" 97 "TXDM-TX1"
 98 "NYEC-TX1" 99 "PAAE-TX1" 100 "VAHD-TX1" 101 "NMAQ-TX1"
 102 "PAHE-TX1" 103 "PAHM-TX1" 104 "PASC-TX1" 105 "NJLL-TX1"
 106 "MABS-TX1" 107 "KYJH-TX1" 108 "NYWC-TX1" 109 "CASM-TX1"
 110 "WISL-TX1" 111 "MANM-TX1" 112 "LAWK-TX1" 113 "VAFH-TX1"
 114 "PALV-TX1" 115 "TXBC-TX1" 116 "OHCO-TX1" 117 "FLUF-TX1"
 118 "GAPH-TX1" 119 "CAOP-OP1" 120 "FLTG-TX1" 121 "MACH-TX1"
 122 "ILUI-TX1" 123 "ILLU-TX1" 124 "KSFW-TX1" 125 "KYUK-TX1"
 126 "INIU-TX1" 127 "NYSB-TX1" 128 "TXMH-TX1" 129 "CASB-TX1"
 130 "OKSJ-TX1" 131 "CAWM-TX1" 132 "WASM-TX1" 133 "TXMC-TX1"
 134 "PAGM-TX1" 135 "NECM-TX1" 136 "HISF-TX1" 137 "TXLG-TX1"
 138 "MSUM-TX1" 139 "MIKZ-TX1" 140 "WISE-TX1" 141 "TXHD-TX1"
 142 "NYFL-TX1" 143 "CAGH-TX1" 144 "WANW-OP1" 145 "LATU-TX1"
 146 "ILSF-TX1" 147 "FLJT-TX1" 148 "TXAD-TX1" 149 "DCHU-TX1"
 150 "TNUK-TX1" 151 "NYCP-TX1" 152 "FLSF-OP1" 153 "NMPH-TX1"
 154 "FLFR-TX1" 155 "UTLD-TX1" 156 "TXSW-TX1" 157 "PRSJ-TX1"
 158 "OHSE-TX1" 159 "MALC-TX1" 160 "CAKP-TX1" 161 "OHUC-TX1"
 162 "CACL-TX1" 163 "MNMC-TX1" 164 "NCMH-TX1" 165 "CTHH-TX1"
 166 "NDSL-TX1" 167 "TXSM-TX1" 168 "UTMC-TX1" 169 "MEMC-TX1"
 170 "OHMV-TX1" 171 "MOCG-TX1" 172 "NCEC-TX1" 173 "COUC-TX1"
 174 "NYAM-TX1" 175 "CALB-TX1" 176 "NHDH-TX1" 177 "MNAN-TX1"
 178 "MOSJ-TX1" 179 "ORUO-TX1" 180 "OHCA-TX1" 181 "WASH-TX1"
 182 "ARUA-TX1" 183 "SDMK-TX1" 184 "TXJS-TX1" 185 "OKBC-TX1"
 186 "IAMH-TX1" 187 "MABV-TX1" 188 "ALOB-OP1" 189 "LASB-TX1"
 190 "TNVU-VA1" 191 "NEUN-TX1" 192 "FLSL-TX1" 193 "TNPV-TX1"
 194 "COPM-TX1" 195 "PAWV-TX1" 196 "TNEM-TX1" 197 "CARC-TX1"
 198 "TXFW-TX1" 199 "TNLB-TX1" 200 "OKSA-TX1" 201 "IAIM-TX1"
 202 "INSV-TX1" 203 "PALH-TX1" 204 "CALA-TX1" 205 "CAMH-TX1"
 206 "MICH-TX1" 207 "TXPM-TX1" 208 "MDSG-TX1" 209 "CAIM-TX1"
 210 "TXRM-TX1" 211 "MAHS-TX1" 212 "CABH-TX1" 213 "WVWU-TX1"
 214 "VTMC-TX1" 215 "CAPC-TX1" 216 "MNHC-TX1" 217 "CACS-TX1"
 218 "TXCF-TX1" 219 "PAVA-TX1" 220 "FLBF-TX1" 221 "PACP-TX1"
 222 "OHZA-TX1" 223 "ALAM-TX1" 224 "MABU-TX1" 225 "NCDU-VA1"
 226 "PATU-TX1" 227 "AZHH-TX1" 228 "MODU-TX1" 229 "OKMD-TX1"
 230 "MDBC-TX1" 231 "DCWR-TX1" 232 "CABE-TX1" 233 "TXLM-TX1"
 234 "ILCM-TX1" 235 "DECC-TX1" 236 "OHCM-TX1" 237 "WVCA-TX1"
 238 "GAEH-TX1" 239 "OKHM-TX1" 240 "NESJ-TX1" 241 "FLCC-TX1"
 242 "TXGV-TX1" 243 "AZSJ-TX1" 244 "WVCC-TX1" 245 "MIHM-TX1"
 246 "TXMA-TX1" 247 "MOCM-TX1" 248 "ORUO-VA1" 249 "NDMC-TX1"
 250 "OHMN-TX1" 251 "PACH-TX1" 252 "LANO-TX1" 253 "TNST-TX1"
 254 "TXSP-TX1" 255 "INLH-TX1" 256 "NVHH-TX1" 257 "WICH-TX1"
 258 "MNSM-TX1" 259 "OKCM-TX1" 260 "MOCH-TX1" 261 "TXTC-TX1"
 262 "ORGS-TX1" 263 "SCRM-TX1" 264 "LAMC-TX1" 265 "TXWH-TX1"
 266 "DEAI-TX1" 267 "LACH-TX1" 268 "INFW-TX1" 269 "MDNI-TX1"
 270 "OHRH-TX1" 271 "NYBC-TX1" 272 "HIQM-TX1" 273 "AZSM-TX1"
 274 "AZTV-TX1" 275 "ARCH-TX1" 276 "NYNS-TX1" 277 "ILLU-VA1"
 278 "CACH-TX1" 279 "TXCM-TX1" 280 "WACH-TX1" 281 "TXDC-TX1"
 282 "COCH-TX1" 283 "DCCH-TX1" 284 "MOJM-TX1" 285 "VACH-TX1"
 286 "FLAC-TX1" 287 "DCGW-TX1" 288 "VARM-TX1" 289 "NDDH-TX1"
 290 "CASC-TX1" 291 "OKOP-OP1" 292 "MIWS-TX1" 293 "NEOR-OP1"
 294 "LASU-TX1" 295 "OHDC-TX1" 296 "OHMC-OP1" 297 "TNDC-TX1"
 298 "ILVA-TX1" 299 "NYWN-OP1" 300 "LASM-TX1" 301 "TXCH-TX1"
 302 "OHLI-TX1" 303 "OKSF-TX1" 304 "NJUH-TX1" 305 "OHPM-TX1"
 306 "CAHP-TX1" 307 "GASJ-TX1" 308 "UTMC-VA1" 309 "OHCD-TX1"
 310 "TNNV-TX1" 311 "MAOB-OP1" 312 "MNOP-OP1" 313 "KYKC-TX1"
 314 "INEV-TX1" 315 "AZCH-TX1" 316 "OHCW-TX1" 317 "INSB-TX1"
 318 "PAPT-VA1" 319 "ILCR-TX1" 320 "SDSV-TX1" 321 "TNDS-OP1"
 322 "NVLV-TX1" 323 "MIDV-TX1" 324 "FLHM-TX1" 325 "OHCH-TX1"
 326 "OHSP-TX1" 327 "INMU-TX1" 328 "TXSB-OP1" 329 "MNMV-TX1"
 330 "INLA-TX1" 331 "NCBH-TX1" 332 "TNET-TX1" 333 "INLF-TX1"
 334 "INHA-TX1" 335 "UTPC-TX1" 336 "CORS-OP1" 337 "MNMM-TX1"
 338 "OHPH-TX1" 339 "PATF-OP1" 340 "DCTC-OP1" 341 "INTE-TX1"
 342 "ILIP-OP1" 343 "TXST-TX1" 344 "OHMS-TX1" 345 "TXLP-TX1"
 346 "ILCH-TX1" 347 "PACC-TX1" 348 "TXPL-TX1" 349 "ALCH-TX1"
 350 "ALVA-TX1" 351 "FLBC-TX1" 352 "MNAC-TX1" 353 "TXVA-TX1"
 354 "CAMB-TX1" 355 "PADV-OP1" 356 "TXGC-OP1" 357 "AROR-OP1"
 358 "AZOB-OP1" 359 "CADN-OP1" 360 "CAGS-OP1" 361 "CASD-IO1"
 362 "CTOP-OP1" 363 "FLFH-IO1" 364 "FLMP-OP1" 365 "FLUF-IO1"
 366 "FLWC-OP1" 367 "GALL-OP1" 368 "HIOP-OP1" 369 "IAOP-OP1"
 370 "INOP-OP1" 371 "KYDA-OP1" 372 "LAOP-OP1" 373 "MDPC-OP1"
 374 "MIOP-OP1" 375 "MOMA-OP1" 376 "MSOP-OP1" 377 "MWOB-OP1"
 378 "NCCM-IO1" 379 "NCNC-OP1" 380 "NJTO-OP1" 381 "NMOP-OP1"
 382 "NVLV-OP1" 383 "NYAP-OP1" 384 "NYFL-IO1" 385 "NYRT-OP1"
 386 "OHLB-OP1" 387 "OHLC-OP1" 388 "OHLP-OP1" 389 "OHOV-OP1"
 390 "ORUO-IO1" 391 "PRLL-OP1" 392 "SCOP-OP1" 393 "TNMS-OP1"
 394 "TXSA-OP1" 395 "UTOP-OP1" 396 "VATB-OP1" 397 "WALC-OP1"
 398 "WIDN-OP1" 399 "WIUW-IO1";
 
#delimit cr

label copy listing_ctr_id run_loc_ctr
