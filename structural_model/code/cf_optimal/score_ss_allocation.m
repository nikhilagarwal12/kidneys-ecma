function [output,heu] = score_ss_allocation(params,score,alloc,use_fact_V,score_list,heuristic)
    % Computes the steady state allocation for a score
    % params: problem parameters
    % score (IMPORTANT!): priorities, if score_list is omitted high values will be offered kidneys first.
    % Vold: initial conditions for V;
    % use_fact_V: 1 V is fixed at Vold; 
    % score_list: allows us to reshuffle the queueing order of scores.
    
    % Heuristics for convergence
    if nargin<6 || isempty(heuristic), heuristic = struct(); end
    heu = get_heuristic(heuristic);
    
    if nargin<2 || isempty(score) % Default score
        score = repmat(cumsum(cumsum(params.arrivals>0,2),2), [1, 1, size(params.mu,3)] );
        disp('Assuming FCFS');
    end
    if nargin<3 || isempty(alloc) % Default initial conditions
        qold = zeros(size(params.mu));
        Vold = zeros(size(params.arrivals));
    else
        qold = alloc.q;
        Vold = alloc.V;
    end
    
    if nargin<4 || isempty(use_fact_V), use_fact_V = 0; end
    if nargin<5 || isempty(score_list),
        score_list = -unique(-score); % Unique scores sorted in decreasing order
        score_list(isinf(score_list))=[];  % Infinite scores will not be included;
    end
    % Replace infeasible allocations with inf: they will be set q=0;
    score(params.include_pattern==0) = inf;
    
    % Prepare for Mex code
    position = score2position(score,score_list);
    params.use_fact_V = use_fact_V;
    new_values = @(q,V,inner_heu) update_values(q,V,params,inner_heu); % For some reason the matlab version is faster.
    % new_values = @(q,V,inner_heu) updateValuesMex(q,V,params,inner_heu);
    % new_composition = @(q,V) update_composition(q,V,params);
    new_composition = @(q,V) updateCompositionMex(q,V,params);
    % new_offerprob   = @(p,pr_tx) update_offer_probability(p,pr_tx,[],position,max(position(:)),params);
    new_offerprob   = @(p,pr_tx) updateOfferProbabilityMex(p,pr_tx,[], uint64(position), uint64(max(position(:))), params);

    % Initialize
    ii = 0; Vdiff=1; qdiff=1; chatters = 0;
    while Vdiff+qdiff > heu.tol && ii<heu.maxiter
        if use_fact_V == 1  % fact_V mode
            if ii == 0  
                [~, pr_tx] = new_values(qold,Vold,[]); % fix V and pr_tx
                V = Vold;
            end
        else 
            inner_heu.tol = min(Vdiff/20/(chatters+1),1e-3/ii); inner_heu.maxiter = ii+chatters+1;
            [V,pr_tx] = new_values(qold,Vold,inner_heu); % Now update_values does a complete backwards induction.
        end
        p         = new_composition(qold,V);  
        q         = new_offerprob(p, pr_tx);

        % Wrap up this iteration;
        Vdiff=max(abs(V(:)-Vold(:)));
        qdiff=max(abs(q(:)-qold(:)));
        
        % Chatter control
        if ii>0, 
            Vdiff2=max(abs(V(:)-Vold2(:)));
            qdiff2=max(abs(q(:)-qold2(:)));
            chatters = max(chatters + heu.K*(Vdiff2<Vdiff && qdiff2<qdiff)-1,0);
        end
        w = heu.k^chatters; % if chatters = 0 w = 1, and there is full update.    
        q = w * q + (1-w) * qold;
        V = w * V + (1-w) * Vold;
        
        fprintf('%d) Norm differences V: %e; q: %e. Pool size: %e; Chatters: %d\n',ii,Vdiff,qdiff,sum(p(:)'*params.h(:)),chatters);
        
        Vold2 = Vold;
        qold2 = qold;
        Vold = V;
        qold = q;
        ii = ii + 1;
        
       
    end
    
    if use_fact_V   % Find continuation values of playing the naive strategy.
        [V,pr_tx] = new_values(q,Vold,[]); % Right now update_values uses previous V 
    end

    % Populate output
    output= struct('V',V,'q',q,'p',p,'pr_tx',pr_tx,'diff_norm',Vdiff+qdiff,'iter',ii);
end


function position = score2position(score,list)
    % Scores should be prepared as a list of consecutive positive and 
    % finite integers sorted in decreasing order 
    [~,position] = ismember(score,list);  % Get order in the list
end

function out = get_heuristic(heu)
    
    % k < 1: dampening rate when chatters>0
    % K >=2: When chattering is detected, increase the chatter level 
    %by K. Set K larger than the lenght of the cycles.
    % tol: tolerance level
    % maxiter: maximum number of iterations
    
    if ~isfield(heu,'standard'), heu.standard = 1; end  % Default: follow standard heuristics
    
    switch heu.standard % standard heuristics
        case 2,    out.k = 0.25; out.K = 3;  out.tol = 1e-7;  out.maxiter = 1000;
        case 3,    out.k = 0.66; out.K = 6;  out.tol = 1e-7;  out.maxiter = 1000;
        otherwise, out.k = 0.87; out.K = 4;  out.tol = 1e-7;  out.maxiter = 1000;
    end
    
    if heu.standard == 0  % Follow original heuristics
        if isfield(heu,'k'),       out.k=heu.k; end
        if isfield(heu,'K'),       out.K=heu.K; end
        if isfield(heu,'tol'),     out.tol=heu.tol; end
        if isfield(heu,'maxiter'), out.maxiter=heu.maxiter; end
    else
        heu.standard = heu.standard + 1; % If standard heuristic; set it up so next heuristics are different;
    end
end
    
