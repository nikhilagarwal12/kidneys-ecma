// Deceased donor characteristics, overall and by year

*** Code to create the donor table 
program define donor_table	

args make_table donor_table_file deceased_donors_w_vars don_chars sample_name subsample_donor_code subsample_tx_code subsample_offer_code output_folder sample_start_date sample_end_date

if `make_table' == 1 {
use `deceased_donors_w_vars', clear
keep if donor_id != . & don_ty == "C" // only select deceased donors, and only during our sample period

if "`sample_name'" != ""{  // to hold results for specific types of donors
    tempfile donor_temp
    save `donor_temp', replace
}

// full sample
local string_mean = ""
local string_sd = ""
local string_p10 = ""
local string_median = ""
local string_p90 = ""

// construct collapse string
foreach var of varlist `don_chars'{
  display "`var'"
  local string_mean = "`string_mean'" + " (mean) " + "`var'_mean = `var'"
  local string_sd = "`string_sd'" + " (sd) " + "`var'" + "_sd = `var'"
  local string_median = "`string_median'" + " (median) " + "`var'" + "_median = `var'"
  local string_p10 = "`string_p10'" + " (p10) " + "`var'" + "_p10 = `var'"
  local string_p90 = "`string_p90'" + " (p90) " + "`var'" + "_p90 = `var'"
}
display "collapse (count) donor_id `donor_chars'"


tempfile table_temp    // Holds stats broken out by group
tempfile sample_table  // Holds stats for different universes of donors (if this is for a subsample)


if "`sample_name'"==""{
    include ../code/functions/donor_subcategories.do
    gen donor_subset = "All Donors"
    order donor_subset
}

if "`sample_name'"!=""{
   
    local sample_upper = upper("`sample_name'")
    
    // donors recovered in subsample

    if `"`subsample_donor_code'"' != ""{
    
        use `donor_temp', clear
        keep if `subsample_donor_code'
        include ../code/functions/donor_subcategories.do
        gen donor_subset = "Donor Recovered in `sample_upper'"
        order donor_subset
        save `sample_table', replace
    }

    // donors corresponding to offers contained in subsample

    if `"`subsample_offer_code'"' != ""{

        use `donor_temp', clear
        keep if `subsample_offer_code'
        include ../code/functions/donor_subcategories.do
        gen donor_subset = "Donors Offered to `sample_upper' Patients"
        order donor_subset
        if `"`subsample_donor_code'"' != "" | `"`subsample_tx_code'"' != ""{
            append using `sample_table'
        }
    }
    
    isid donor_subset sample
}

reshape long `don_chars', i(donor_subset sample) j(statistic) string
sort sample
save `donor_table_file', replace

}

*** Code to format the donor table
use `donor_table_file', clear

** Generate a sample and statistics order
gen sam_order = 1 if sample == "All"
replace sam_order = 2 if sample == "Both Kidneys Transplanted Y/N:0"
replace sam_order = 3 if sample == "Both Kidneys Transplanted Y/N:1"


gen stat_order = 1 if statistic == "_mean"
replace stat_order = 2 if statistic == "_sd"
replace stat_order = 3 if statistic == "_median"
replace stat_order = 4 if statistic == "_p10"
replace stat_order = 5 if statistic == "_p90"

sort donor_subset sam_order sample stat_order

drop sam_order stat_order 

*** Label variables
label variable donor_subset "Donor Subset"
label variable num_donors "Number of Donors"
label variable n_offers "Number of Offers per Donor"
label variable age_don "Donor Age"
label variable cause_head_trauma "Cause of Death -- Head Trauma"
label variable cause_stroke "Cause of Death -- Stroke"
label variable diabetes_don "Diabetic Donor"
label variable hypertension_don "Hypertensive Donor"
label variable creat_don "Donor Creatinine"
label variable ecd_donor "Expanded Criteria Donor (ECD)"
label variable non_heart_beating "Non-Heart Beating Donor (DCD)"
label variable num_ki_tx "Number of Kidneys Transplanted per Donor"

** Rename Statistic Field
replace statistic = "Mean" if statistic == "_mean"
replace statistic = "S.D." if statistic == "_sd"
replace statistic = "Median" if statistic == "_median"
replace statistic = "10th Percentile" if statistic == "_p10"
replace statistic = "90th Percentile" if statistic == "_p90"


** Rename Certain Sample Names
replace sample = "Donors With No Discards" if sample == "Both Kidneys Transplanted Y/N:1"
replace sample = "Donors With Discards" if sample == "Both Kidneys Transplanted Y/N:0"

order donor_subset sample statistic num_donors n_offers num_ki_tx age_don cause_head_trauma cause_stroke diabetes_don hypertension_don ecd_donor non_heart_beating creat_don

export excel using "`output_folder'summary_tables_`sample_name'.xls", sheet("Donors") firstrow(varlabels) sheetreplace

end
