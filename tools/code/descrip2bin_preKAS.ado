
program descrip2bin_preKAS

local tvars "ABDR CPRA ABO Ped_List Ped_Alloc Geo Bin prior_living_donor abo cpra abo_don"


// ABDR_t
gen ABDR_t = "Non-Zero" // Default
replace ABDR_t = "Zero" if strpos(descrip,"0 ABDR Mismatch:")>0
gen remaining = subinstr(descrip,"0 ABDR Mismatch: ","",.)

// Geo
gen Geo_detail = substr(remaining,1,strpos(remaining,"KI")-2)
replace Geo_detail = trim(subinstr(Geo_detail,",","",.))
gen Geo_t = Geo_detail
replace Geo_t = "Local" if Geo_detail == "OPO" | ((Geo_detail=="Regional" | Geo_detail == "Region 1") & variance =="R1") | (Geo_detail=="DCTC" & variance =="DCTC")
replace Geo_t = "Regional" if Geo_detail == "Statewide"
replace remaining = substr(remaining,strpos(remaining,"KI")+3,.)

// ABO 
gen ABO_t= "Identical" if strpos(remaining,"ABO Identical")>0
replace remaining = subinstr(remaining,"ABO Identical","",.)
replace ABO_t= "Compatible" if strpos(remaining,"ABO Compatible")>0
replace remaining = subinstr(remaining,"ABO Compatible","",.)
replace ABO_t= "O Donor B Candidate" if strpos(remaining,"B Candidates/O Donors")>0
replace remaining = subinstr(remaining,"B Candidates/O Donors","",.)
replace ABO_t= "O Donor B Candidate" if strpos(remaining,"B Candidates/O Donors")>0
replace remaining = subinstr(remaining,"ABO Compatible","",.)

//save ../../diagnostics/temp/descrip_text2.dta, replace

//use ../../diagnostics/temp/descrip_text2.dta, clear
local alist `" "Eligible B Candidates/Deceased A2/A2B Donors & O Candidates/Deceased A2 Donors" "Eligible B Candidates/Deceased A2/A2B Donors" "A1 or A1B" "A2 or A2B" "B Candidates" "B Potential Recipients" "O Potential Recipients" "O Candidates" "'
foreach avar of local alist {
    di "`avar'"
    replace ABO_t= "`avar'" if strpos(remaining, "`avar'")>0
    replace remaining = subinstr(remaining,"`avar'","",.)
}
replace ABO_t = "Compatible" if (ABO_t=="") //Default

// Petiatric At Allocation
gen pediatric = strpos(remaining,"Pediatric")>0
replace remaining = subinstr(remaining,"Pediatric","",.)
gen Ped_Alloc_t = "Ped At Allocation"
gen Ped_List_t = "Ped At Listing"
replace Ped_Alloc_t = "Non-"+Ped_Alloc_t if ~pediatric
replace Ped_List_t = "Non-"+Ped_List_t if ~pediatric


local alist `"  "Highest Scoring High PRA"  "Highest Scoring High CPRA" "'
gen highest_pra = 0
foreach avar of local alist {
    replace highest_pra = highest_pra | strpos(remaining, "`avar'")>0
    replace remaining = subinstr(remaining,"`avar'","",.)
}


gen CPRA_t = ""
replace CPRA_t ="CPRA<=20" if strpos(remaining, "0%-20%")>0 | strpos(remaining, "0% - 20%")>0
replace remaining = subinstr(remaining,"0%-20%","",.)
replace remaining = subinstr(remaining,"0% - 20%","",.)

replace CPRA_t ="21<=CPRA<=79" if strpos(remaining, "21%-79%")>0 | strpos(remaining, "21% - 79%")>0
replace remaining = subinstr(remaining,"21%-79%","",.)
replace remaining = subinstr(remaining,"21% - 79%","",.)
replace CPRA_t ="CPRA>=80" if strpos(remaining, "80%-100%")>0 | strpos(remaining, "80% - 100%")>0
replace remaining = subinstr(remaining,"80%-100%","",.)
replace remaining = subinstr(remaining,"80% - 100%","",.)

local alist `" "Peak PRA < 80" "Peak PRA >= 80"  " < 80" " >= 80" "'
foreach avar of local alist {
    di "`avar'"
    replace CPRA_t = "`avar'" if strpos(remaining, "`avar'")>0
    replace remaining = subinstr(remaining,"`avar'","",.)
}
replace CPRA_t ="CPRA<=20" if CPRA_t ==""


gen mandatory = strpos(remaining, "Mandatory")>0
replace remaining = subinstr(remaining,"Mandatory","",.)
//replace mandatory = 0 if strpos(remaining, "Voluntary")>0   // I don't need Voluntary.
replace remaining = subinstr(remaining,"Voluntary","",.)

gen masterfile = 0 if strpos(remaining, "Not in Master file")>0
replace remaining = subinstr(remaining,"Not in Master file","",.)
replace masterfile = 1 if strpos(remaining, "Master File")>0
replace remaining = subinstr(remaining,"Master File","",.)

gen incom_liv_don_pair =  strpos(remaining,"Incompatible Living Donor Paired")>0
replace remaining = subinstr(remaining,"Incompatible Living Donor Paired","",.)
gen unsensitized = 0 if strpos(remaining,"Unsensitized and Sensitized")>0
replace remaining = subinstr(remaining,"Unsensitized and Sensitized","",.)
replace unsensitized = 1 if strpos(remaining,"Unsensitized")>0

gen prior_living_donor_t =  strpos(remaining,"Prior Living Organ Donors")>0
replace remaining = subinstr(remaining,"Prior Living Organ Donors","",.)

// Do abo abo_don cpra for variances...
gen cpra_t =.
replace cpra_t =0.79 if trim(CPRA_t)=="< 80" | trim(CPRA_t) =="21<=CPRA<=79"
replace cpra_t =0.81 if trim(CPRA_t)==">= 80" | trim(CPRA_t) =="CPRA>=80"
replace cpra_t =0.19 if trim(CPRA_t)=="CPRA<=20" 
gen CPRA_true = CPRA_t
replace CPRA_true = "21<=CPRA<=79" if cpra_t >0.195 & cpra_t <=0.795
replace CPRA_true = "CPRA>=80" if cpra_t >0.805
replace CPRA_true = "CPRA<=20" if cpra_t <=0.195 | cpra_t ==.
drop CPRA_t
ren CPRA_true CPRA_t

tab CPRA_t,m
	

gen abo_t = ""
replace abo_t = "B" if ABO_t=="B Candidates" | ABO_t =="B Potential Recipients" | ABO_t =="B Candidate" | ABO_t=="O Donor B Candidate" | strpos(ABO_t,"Eligible B Candidates")>0
replace abo_t = "O" if ABO_t=="O Candidates" | ABO_t =="O Potential Recipients" 
replace abo_t = "A1B" if ABO_t=="A1 or A1B" 
replace abo_t = "A2B" if ABO_t=="A2 or A2B" | strpos(ABO_t,"A2/A2B")>0
gen abo_don_t = ""
replace abo_don_t = "A2" if ABO_t=="A2 or A2B" | ABO_t =="A1 or A1B" | ABO_t=="B Candidates" | ABO_t=="B Potential Recipients" | ABO_t=="O Candidates" | ABO_t =="O Potential Recipients" | strpos(ABO_t,"A2/A2B")>0
replace abo_don_t ="O" if ABO_t=="O Donor B Candidate" 

gen ABO_true = "Compatible"
replace ABO_true = "Identical" if ABO_t =="Identical"
replace ABO_true = "O Donor B Candidate" if abo_don_t =="O" & abo_t == "B"


* Rename variables 
drop ABO_t
ren ABO_true ABO_t
gen Bin_t = 0
foreach v of local tvars {
	ren `v' `v'_c
	ren `v'_t `v'
}





drop Bin

// Run the categories code
rank_categories_preKAS
summ Bin
drop flag_bin variance
variances_preKAS, binsonly descrip
summ Bin
* Rename variables back
foreach v of local tvars {
	ren `v' `v'_t
	ren `v'_c `v'
}

end
