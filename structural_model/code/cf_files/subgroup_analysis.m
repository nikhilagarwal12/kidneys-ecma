%% Analyze patterns by donor and patient subgroups in equilibria of counterfactual mechanisms


cf_mech_list                       = {'score_binspoints_locl_ref','score_gammaeta','greedy_second','greedy_secondC'};
cf_tree_varlist                    = {'amis_2','bmis_2','cpra','dcd','age_don','abo_compatible',...
                                      'don_cod_anoxia','don_cod_cns','don_cod_stroke',...
                                      'don_creat_1p0_1p5','don_creat_ge_1p5','don_creat_p5_1p0',...
                                      'don_male','donor_hypertension','drmis_2','ecd_donor',...
                                      'end_bmi_calc','init_age','log_dialysis_time_at_init',...
                                      'log_wait_time','national_donor','on_dialysis_at_init_date',...
                                      'prior_transplant','pt_diabetes','regional_donor','tot_serum_album','zero_mm','pt_abo_O'};
cf_tree_don_cat                    = @(pt) 1 * all([pt.national_donor>0 | pt.regional_donor>0],2) + ...
                                           3 * all([pt.national_donor<=0 pt.regional_donor<=0 pt.dcd<=0 pt.ecd_donor<=0 pt.age_don<35],2)+ ...
                                           4 * all([pt.national_donor<=0 pt.regional_donor<=0 (pt.dcd>0 | pt.ecd_donor>0 | pt.age_don>=35)],2);
cf_tree_pat_cat                    = @(pt) 1+(pt.init_age>=50)+2*(pt.on_dialysis_at_init_date<=0);
                                      
                                      
                                      

load([data_filepath '_offers'],'offers');
load(model.filenames.donors,'donors');
load(model.filenames.patients,'patients');
load(model.filenames.pt_history,'pt_history');
orig_offers = offers;  % since this gets overwritten each time

% Loop over mechanisms, generating subgroup stats for each
for kk = 1:numel(cf_mech_list)

    cf_mech_alias = cf_mech_list{kk}
    display('Loading Stuff')
    alias_filepath = [rslt_filepath '_' cf_mech_alias];

    soln_file    = load([alias_filepath '_soln']);
    mech_aliases;
    if strcmp(cf_mech.type,'optim'), 
        prob_file    = load([alias_filepath '_prob']);       
    else 
        soln_file.alloc = soln_file.state;
        prob_file.opt_prob = optimal_ss_problem(data_object(1)).simplify_times(cf_mech.simplify_times);
        prob_file.opt_prob.periods_from_arrival = cf_mech.simplify_times;
    end

    % Create an 'offers' object to hold relevant information from CF output in long format
    display('Reshaping CF Output')

    offers = matchDates(orig_offers,'increments',floor(prob_file.opt_prob.periods_from_arrival*365.25/data_object(1).options.pd_per_year));
    
    shift = sum(1-cumsum(prob_file.opt_prob.arrivals>0,2),2);  % How many places I need to move shift the index
    [~,index1] = ismember(offers.data.wlreg_audit_id_code,data_object(1).pt_hist_id); % Get pt_hist_id locations
    index2 = offers.indices.incr + shift(index1);                            % Get time_period locations 
    offers = SubsetObs(offers,index2<=numel(prob_file.opt_prob.periods_from_arrival)); % Delete extra time_periods

    [N_patient,N_time,N_donor] = size(prob_file.opt_prob.mu);
    N_donor_real = N_donor*4/size(donors,1);
    
    [~,index1] = ismember(offers.data.wlreg_audit_id_code,data_object(1).pt_hist_id); % Get mew pt_hist_id locations
    index2 = offers.indices.incr + shift(index1);                            % Get new time_period locations
    [~,index3] = ismember(offers.data.donor_id,data_object(1).donor_id);  % Get new donor_id locations
    linearIndq = sub2ind(size(soln_file.alloc.q), index1, index2, index3);  % get linear index for q
    offers.data.q = soln_file.alloc.q(linearIndq);                 % recover q: prob_offer
    linearIndp = sub2ind(size(soln_file.alloc.p), index1, index2);          % get linear index for p
    offers.data.p = soln_file.alloc.p(linearIndp);                          % get linear index for p
    offers.data.V = soln_file.alloc.V(linearIndp);                          % get linear index for V
    offers.data.pr_tx = soln_file.alloc.pr_tx(linearIndq);                  % get linear index for pr_tx
    offers.data.days_per_pd = prob_file.opt_prob.days_per_pd(linearIndp);
    post_arrival = cumsum(prob_file.opt_prob.arrivals,2)>0;
    wait_time = cumsum(post_arrival .* prob_file.opt_prob.days_per_pd,2);
    offers.data.time = wait_time(linearIndp)/365.25;
    this_model = model; 
    if model.donor_uh
        this_model.donor_chars = [model.donor_chars {'eta'}];
    end

    % remove interactions from data structure
    display('Generating Data Structure')
    vars_in_pw_lin = cat(1,this_model.fixed.pwlin{:});
    this_model.fixed.linear =unique([this_model.fixed.linear vars_in_pw_lin(:,1)']);
    this_model.fixed.linear = cf_tree_varlist;
    this_model.fixed.pwlin  = {};
    this_model.fixed.interactions = {};
    this_model.fixed.colheaders = this_model.fixed.linear;
    this_model.renormalize = false;
    this_model.fixed = rmfield(this_model.fixed,{'K','mu_dm','sigma_dm'});
    dset = gen_data_structure(this_model,[],offers.subsetvars,offers.patients,offers.donors,offers.pt_history,'value_function');
    dtable = array2table(dset.fixed.obs,'VariableNames',dset.fixed.colheaders);
    don_cat=cf_tree_don_cat(dtable);
    pat_cat=cf_tree_pat_cat(dtable); %'

    type_O_match = dtable.pt_abo_O .* ~dtable.abo_compatible;   % the abo compatible condition is actually unnecessary for Type O


%****************************%
%      Generate Tables       %
% display('Generate Tables') %
%****************************%

    % Initialize table arrays
    pat_cat_list = unique(pat_cat); % for selecting relevant categories
    pi_mat = zeros(length(pat_cat_list),max(don_cat));
    avg_time_off = zeros(length(pat_cat_list),max(don_cat));
    tx_rates_don = zeros(length(pat_cat_list),max(don_cat));
    
% loop over donor and patient categories
for dd = 1:max(don_cat)
    
    don_cat_ii = don_cat==dd;
    N_dd = sum(don_cat_ii)/(N_patient*N_time); % # donors in group
    
    for pp_i = 1:length(pat_cat_list)

	pp = pat_cat_list(pp_i);				   
        include_this = (don_cat ==dd & pat_cat == pp);
        include_type_O = (type_O_match & include_this);

        offer_weights = offers.data.q(include_this).*max(offers.data.p(include_this),0).*offers.data.days_per_pd(include_this)/sum(offers.data.p(include_this).*offers.data.days_per_pd(include_this).*offers.data.q(include_this));
                       
	offer_weights_O = offers.data.q(include_type_O).*max(offers.data.p(include_type_O),0).*offers.data.days_per_pd(include_type_O)/sum(offers.data.p(include_type_O).*offers.data.days_per_pd(include_type_O).*offers.data.q(include_type_O));

        patient_weights = max(offers.data.p(include_this),0).*offers.data.days_per_pd(include_this)/sum(offers.data.p(include_this).*offers.data.days_per_pd(include_this));

	% Prob. of OFFER from an arriving donor, from perspective of an individual patient
        pi_mat(pp_i,dd) = sum(offers.data.q(include_this).*patient_weights);

        % Mean waiting time at offer. Offer-weighted (for a given offer, how long has the patient waited?)
        avg_time_off(pp_i,dd) = sum(offers.data.time(include_type_O).*offer_weights_O);

        % transplant rates to each patient type, by donor type. Sum across patients and donors
        tx_rates_don(pp_i,dd) = sum(offers.data.q(include_this).*max(offers.data.p(include_this),0).*offers.data.days_per_pd(include_this).*offers.data.pr_tx(include_this))/N_donor_real;

    end
end

%**************************%
%       Output tables      %
% display('Output Tables') %
%**************************%

    % Structure of each output table: columns are donor types, rows patient types
    var_names = {'NonLocal','ECDDCD','Young','Old'}';
    row_names = {'On Dial, Young','On Dial, Old',...
                 'Off Dial, Young','Off Dial, Old'};
    row_names = row_names(unique(pat_cat))';
    
    % Convert arrays to MATLAB table structures for outputting
    pi_mat = array2table(pi_mat,'VariableNames',var_names,'RowNames',row_names);
    avg_time_off = array2table(avg_time_off,'VariableNames',var_names,'RowNames',row_names);
    tx_rates_don = array2table(tx_rates_don,'VariableNames',var_names,'RowNames',row_names);

    % Where to write the table
    descrip_filepath = [rslt_filepath '_mech_compare_formatted.xls'];  % filepath
    cf_mech_short = cf_mech_alias(6:end);  % mechanism identifer in sheet name within excel workbook

    writetable(pi_mat,descrip_filepath,'WriteRowNames',true,'Sheet',['OffProbs' cf_mech_short])
    writetable(avg_time_off,descrip_filepath,'WriteRowNames',true,'Sheet',['TimeOff' cf_mech_short])
    writetable(tx_rates_don,descrip_filepath,'WriteRowNames',true,'Sheet',['TXRates' cf_mech_short])

end

