function [output] = optimal_ss_allocation(params,runtype,knitro_opt_file,alloc)
    % Computes the optimal steady state allocation assuming that values are:
    % Normal(mu,sigma).
    % params should contain:
    % mu: [N_patients,N_time,N_donors] array 
    % sigma: array, matrix, or vector (bsxfun w/ mu)
    % arrivals:  [N_patients,N_time] matrix of mass of patient arrivals (per 
    % unit of time)
    % hazard: [N_patients,N_time] matrix of donor exogenous departure
    % hazard rate
    % endowment: [N_donors,1] vector of kidneys available if donor arrives
    % timeout_prob: [N_donors x 1] probability that a KIDNEY times out (see hack)
    % pr_compat: [N_patients x 1 x N_donors] probability that a patient and donor pair are compatible
    % lambda: donor arrival rate (donors per day)
    % rho: daily discount_rate
    % h: [N_patients,N_time] days per period
    % include_pattern: boolean [N_patients,N_time,N_donors] feasible assignments (optional)
    % pareto_weights: (optional) pareto weights (empty -> arrivals).
    % status_quo_V (optional) if we want to ensure that each agent type get a minimum value;
    % runtype: (optional) boolean (0 or empty -> second best), if it is a vector and 
    % the second element is true, values will be constrained from below. If the third element is true, the objective function maximizes transplants.
    
    % Global Tunnable Parameters

    % Debugging tools:
    debug_mode = false;
    useKnitro = true;
    useMex = true;
    useRealq = false;
    run_feasible_initial = false;
    if debug_mode
      useMex = false; % Test all mex functions inside matlab functions
    end
    
    % Default Second Best
    if nargin<2 || isempty(runtype), runtype = 0; end  
        
    % Extract problem scalars
    [N_patients, N_time, N_donors] = size(params.mu);
    
    % Some useful functions
    useful.flat  = @(x) x(:);
    useful.X2matQ = @(x) reshape(x(1:N_patients*N_time*N_donors),N_patients,N_time,N_donors);
    useful.X2matP = @(x) reshape(x(N_patients*N_time*N_donors+1:N_patients*N_time*(N_donors+1)),N_patients,N_time);
    useful.X2matV = @(x) reshape(x(N_patients*N_time*(N_donors+1)+1:end),N_patients,N_time);
    useful.spdiag  = @(x) sparse(1:numel(x),1:numel(x),x(:),numel(x),numel(x),numel(x));
    useful.offdiag = @(x) sparse(1:numel(x),repmat(1:N_patients*N_time,1,N_donors),x(:),N_patients*N_time*N_donors,N_patients*N_time,N_patients*N_time*N_donors);
    
    % Enter default pareto weights if they are not provided
    if ~isfield(params,'pareto_weights')
        params.pareto_weights = sum(params.arrivals,2)./sum(params.arrivals(:)); % Default: arrivals
    end
    
    % Verify that pareto weights make sense
    assert(isequal(size(params.pareto_weights),[N_patients,1]));  % Check the size of pareto_weights
    if max(params.pareto_weights)>1,
        fprintf('There is a pareto_weight greater than 1: %3.2f\n',max(params.pareto_weights));
        this_bound = input(['Enter bound (inf: no bound): ']);
        fprintf('You are bounding the pareto_weights by %3.2f\n',this_bound);
        keyboard;
        params.pareto_weights = min(params.pareto_weights,this_bound);
    end
    % Constrained Value Functions
    if ~isfield(params,'status_quo_V') || numel(runtype)==1 || ~runtype(2)
       lbV = -inf(N_patients*N_time,1);   % No constraints;
    else
       lbV = params.status_quo_V(:);         % Pass the constraints;
       assert(all(size(lbV)==[N_patients*N_time,1]));
       if any(~isinf(lbV)), fprintf('Adding constraints on Values\n'); end
    end
    % Transplant Maximizing
    maximize_transplants = numel(runtype)>=3  && runtype(3);
    if maximize_transplants, 
        fprintf('Maximize transplants\n'); 
        if isfield(params,'slack_cost'),
            assert(numel(params.slack_cost)==N_donors);
            params.slack_cost = params.slack_cost(:)';   % Making sure it is a row vector;
        else
            params.slack_cost = ones(1,N_donors);
            fprintf('Using default slack_cost\n'); 
        end
    end
    
    
    % If include pattern is not provided all variables will be included
    if ~isfield(params,'timeout_prob'), params.timeout_prob = zeros(N_donors, 1); end
    if ~isfield(params,'pr_compat'), params.pr_compat = ones(N_patients, 1, N_donors); end
    if ~isfield(params,'include_pattern'), params.include_pattern = []; end
    if nargin<3 || isempty(knitro_opt_file), knitro_opt_file = 'knitroOptions.opt'; end
    solveSmallerProblem = ~isempty(params.include_pattern) || useRealq;
    
    %% Set up the optimization problem
    % Initial values 
    eps  = 1e-12;
    if nargin<4 || isempty(alloc) % Default initial conditions
        q0   = 0.5*ones(N_patients*N_time*N_donors,1);  % First Best: Fraction of patients in a bin who RECEIVE a kidney (between 0 and 1)
                                                       % Second Best: Fraction of patients in a bin who will be OFFERED a kidney (between 0 and 1)
        p0   = zeros(N_patients*N_time,1);           % Mass of patients in a bin
        v0   = zeros(N_patients*N_time,1);           % Value Function of patients in a bin
        run_feasible_initial = true;
    else
        q0   = max(min(alloc.q(:),1-eps),eps);
        p0   = alloc.p(:);
        v0   = alloc.V(:);
    end
    
    opt.x0   = [q0;p0;v0];                           % Initial conditions   
    
    % Bounds
    arriv1st = (params.arrivals~=cumsum(params.arrivals,2));
    lbp      = params.arrivals; lbp(arriv1st) = -inf;          % Fix p(:,1) = params.arrivals(:,1);             
    ubp      = params.arrivals; ubp(arriv1st) = inf;                        
    opt.lb   = [zeros(size(q0)); lbp(:); lbV];     % q is between 0 and 1 
    opt.ub   = [ones(size(q0)); ubp(:); inf(size(v0))];
         
    % Linear Restrictions
    opt.A    = [];
    opt.b    = [];
    opt.Aeq  = [];                                   % No linear restrictions
    opt.beq  = [];

    if isequal(size(params.sigma),[N_patients,1])
        params.sigma = repmat(params.sigma,1,N_time);
    end   % Hessian computation assumes [N_patients, N_time] matrix;

    % Performing input variable checks before executing Mex codes
    if useMex
      validateattributes(params.mu, {'double'}, {'3d'});
      nP = N_patients*N_time;
      nQ = numel(params.mu);
      validateattributes(params.sigma, {'double'}, {'2d', 'numel', nP});
      if solveSmallerProblem 
        validateattributes(params.include_pattern, {'double'}, {'3d', 'numel', nQ});
      end
      validateattributes(params.arrivals, {'double'}, {'2d', 'numel', nP});
      validateattributes(params.hazard, {'double'}, {'2d', 'numel', nP});
      validateattributes(params.endowment, {'double'}, {'numel', N_donors});
      validateattributes(params.timeout_prob, {'double'}, {'numel', N_donors});
      validateattributes(params.pr_compat, {'double'}, {'3d', 'numel', N_patients*N_donors});
      validateattributes(params.discount_w, {'double'}, {'2d','numel',nP});
      validateattributes(params.discount_V1, {'double'}, {'2d','numel',nP});
      validateattributes(params.discount_V2, {'double'}, {'2d','numel',nP});
      validateattributes(params.delta_x_h, {'double'}, {'2d','numel',nP});
      validateattributes(params.lambdah_over_K, {'double'}, {'2d','numel',nP});
    end
    
    % Add to the parameters of the problem (passed to subfunctions)
    
    add_fields = {'first_best','g','sigma_inverse','pr_compat_repeat','useMex','debug','pareto_weights_repeat'};
    for f = add_fields, assert(~isfield(params,f{1})); end
    params.first_best   = uint64(runtype(1));
    params.g            = [zeros(size(q0));zeros(size(p0)); useful.flat(bsxfun(@times,params.pareto_weights./sum(params.arrivals,2),params.arrivals))]; % Only used in Objective_Enter
    params.sigma_inverse = 1.0./params.sigma;
    params.pr_compat_repeat = repmat(params.pr_compat, [1, N_time, 1]);
    params.pareto_weights_repeat = repmat(params.pareto_weights, [1, N_time, 1]);
    % Passing useMex and useKnitro
    params.useMex = useMex;
    params.debug = debug_mode;
    
    % Function handles for Knitro: objective function, Hessian and non-linear constraints
    if maximize_transplants
        params.timeout_prob = params.timeout_prob*0;  % We only count transplants, not timeouts
        opt.obj   = @(x) Transplants(x, params, useful);
        params.maximize_transplants = int32(1);
    else
        opt.obj   = @(x) Objective(x, params, useful);
        params.maximize_transplants = int32(0);
    end
    
    % Feasible initial guess (execute before defining the small problem)
    if run_feasible_initial
        opt.x0 = feasible_initial(opt,params,useful);
        [c_nlcon_init,ceq_nlcon_init] = nlcon_base(opt.x0,params,useful);  
    end
    
    if params.first_best
      if useMex
        opt.analyticHessian_base = @(x, lambda) analyticHessianFirstMex(x, lambda, params, useful);
        opt.nlcon_base = @(x) nlconFirstMex(x, params);
      else
        opt.analyticHessian_base = @(x, lambda) analyticHessianFirst(x, lambda, params, useful);
        opt.nlcon_base = @(x) nlcon_base(x, params, useful);
      end
    else
      if useMex
        opt.analyticHessian_base = @(x, lambda) analyticHessianSecondMex(x, lambda, params);
        opt.nlcon_base = @(x) nlconSecondMex(x, params, useful);
      else
        opt.analyticHessian_base = @(x, lambda) analyticHessianSecond(x, lambda, params, useful);
        opt.nlcon_base = @(x) nlcon_base(x, params, useful);
      end
    end

    % Rewrite opt if we want to make the problem smaller
    if solveSmallerProblem
        [opt, v_sel, params] = smaller_problem(opt,params,useful, useMex, useRealq);
        opt.nlcon = opt.nlcon_small;
        opt.analyticHessian = opt.analyticHessian_small;
    else
        opt.nlcon = opt.nlcon_base;
        opt.analyticHessian = opt.analyticHessian_base;
    end
    % monitor_memory_whos();
   
    % Jacobian and Hessian Patterns
    if useMex
      if solveSmallerProblem
        Hpattern = analyticHessianPatternMex(params, uint64(v_sel.included_vars));
        JacPatt = nlconPatternMex(params, uint64(v_sel.included_vars));
      else
        Hpattern = analyticHessianPatternMex(params);
        JacPatt = nlconPatternMex(params);
      end
    else
      lambda1.ineqnonlin = ones(N_donors, 1);
      lambda1.eqnonlin = ones(N_patients*(N_time-1)+N_patients*N_time, 1);
      x1 = ones(N_patients*N_time*N_donors+2*N_patients*N_time, 1);
      if params.first_best
        [~, Hpattern] = analyticHessianFirst(x1, lambda1, params, useful);
      else
        [~, Hpattern] = analyticHessianSecond(x1, lambda1, params, useful);
      end
      [~, ~, ~, ~, JacPatt] = nlcon_base(x1, params, useful);
      if solveSmallerProblem
        Hpattern = Hpattern(v_sel.included_vars,v_sel.included_vars);
        JacPatt = JacPatt(v_sel.included_vars,[v_sel.included_ineqnonlin; v_sel.included_eqnonlin]);
      end
      if debug_mode
        % Testing Mex Pattern
        if solveSmallerProblem
          Hpattern1 = analyticHessianPatternMex(params, uint64(v_sel.included_vars));
          JacPatt1 = nlconPatternMex(params, uint64(v_sel.included_vars));
        else
          Hpattern1 = analyticHessianPatternMex(params);
          JacPatt1 = nlconPatternMex(params);
        end
      
        diffPattern = sum(Hpattern(:)-Hpattern1(:));
        if diffPattern
          fprintf('ERORR in mex HPattern\n');
          keyboard;
        end
        diffPattern = sum(JacPatt(:)-JacPatt1(:));
        if diffPattern
          fprintf('ERORR in mex JacPatt\n');
          keyboard;
        end
      end
    end
    % fprintf('Hessian Pattern set\n');
    % monitor_memory_whos();
    % output = struct();
    % return;

    if useKnitro
      % Set Knitro Options
      opt.knitro_opt   = optimset('Display','iter-detailed','GradObj','on', ...
                                'GradConstr','on', 'Hessian','user-supplied', ...
                                'HessFcn',@opt.analyticHessian, ...
                                'HessPattern', Hpattern, 'JacobPattern', JacPatt',...
                                'MaxIter',5000);
    else
      % Set fmincon options
      opt.fmincon = optimoptions(@fmincon, 'MaxFunEvals',Inf,'MaxIter',5000,...
        'Algorithm','interior-point','Display','iter', ...
        'GradObj', 'on','GradConstr', 'on', 'Hessian', 'on', ...
        'HessFcn', @opt.analyticHessian, 'HessPattern', Hpattern);
      % Set fmincon properties for 2015+ versions of matlab
%       opt.fmincon = optimoptions(@fmincon, 'MaxFunEvals',Inf,'MaxIter',5000,...
%         'Algorithm','interior-point','Display','iter', ...
%         'SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true, ...
%         'Hessian', 'on', 'HessianFcn', @opt.analyticHessian, ...
%         'HessPattern', Hpattern, 'CheckGradients', true);
    end
    
    % Check derivatives in debug mode
    if debug_mode && numel(size(opt.x0))< 1000 % Otherwise it may take too long.
      check_derivatives2(opt,params);
      if useKnitro
        % opt.knitro_opt.derivcheck = 3;
        opt.knitro_opt = optimset(opt.knitro_opt, 'DerivativeCheck', 'on');
      else
        % opt.fmincon = optimoptions(opt.fmincon, 'CheckGradients', true, 'PlotFcn', {@optimplotfval});
        opt.fmincon = optimoptions(opt.fmincon, 'DerivativeCheck', 'on');
      end
    end
    
    % Verify Initial guess is feasible
    [c_nlcon_init,ceq_nlcon_init] = opt.nlcon(opt.x0);
    assert(all(opt.x0<=opt.ub+eps & opt.x0>=opt.lb-eps))
    fprintf('Just before running knitro\n');
    monitor_memory_whos();
    %% Run KNITRO
    tic;
    if useKnitro
      [x1,fval,exitflag,output,lambda]  = knitromatlab(@opt.obj,opt.x0,opt.A,opt.b,opt.Aeq,opt.beq,opt.lb,opt.ub,opt.nlcon,[],opt.knitro_opt,knitro_opt_file);
    else
      % Settings for global solution in matlab
      % gs = GlobalSearch;
      % problem = createOptimProblem('fmincon', 'objective', @opt.obj, ...
      %    'x0', opt.x0, 'lb', opt.lb, 'ub', opt.ub, 'nonlcon', opt.nlcon, ...
      %    'options', opt.fmincon);    
      % [x1,fval,exitflag,output,solution] = run(gs,problem);
      [x1,fval,exitflag,output,lambda]  = fmincon(@opt.obj,opt.x0,opt.A,opt.b,opt.Aeq,opt.beq,opt.lb,opt.ub,opt.nlcon,opt.fmincon);
    end
    tDelta = toc;
    %% Organize output
    if ~isempty(params.include_pattern)
        x1 = v_sel.expand(x1); 
        lambda.ineqnonlin = v_sel.expand_ineqnonlin(lambda.ineqnonlin);
        lambda.eqnonlin = v_sel.expand_eqnonlin(lambda.eqnonlin);
        output.v_sel = v_sel;
    end
    
    output.q   = useful.X2matQ(x1);
    output.p   = useful.X2matP(x1);
    output.V   = useful.X2matV(x1);
    output.x   = x1;
    output.fval= fval;
    output.exitflag=exitflag;
    output.first_best = runtype(1);
    output.lambda = lambda;
    output.elapsed_time = tDelta;
    [c_nlcon,ceq_nlcon] =  nlcon_base(output.x,params,useful);
    output.feasible_slack = c_nlcon(1:N_donors);
    output.dynamic_slack = reshape(ceq_nlcon(1:N_patients*(N_time-1)), N_patients, N_time-1);
    output.value_slack = reshape(ceq_nlcon(N_patients*(N_time-1)+1:end),size(output.V));
    if params.first_best
        output.pr_tx   = bsxfun(@times,ones(size(params.mu)),params.pr_compat); % Prob tx conditional on offer
    else
        mli  = bsxfun(@plus,params.mu,-output.V);         % Mean of the Latent Index
        z    = bsxfun(@times,-mli,1./params.sigma);       % Cutoff for standard normal
        Fz  = normcdf(-z);                                % 
        output.pr_tx   = bsxfun(@times,Fz,params.pr_compat);      % Prob tx conditional on offer
    end
    clear mex;
end

%% Subfunctions

% Objective Function
function [f, g] = Objective_Enter(x,params,~)
    f = -x(:)'*params.g(:);  % Linear objective;
    if nargout>1, g = -params.g; end
end

function [f, g] = Objective(x,params,useful)
    V   = useful.X2matV(x);
    p   = useful.X2matP(x);
    f = - params.pareto_weights'*sum(V.*(p.*params.h + params.arrivals/params.rho),2);
    
    if nargout>1, 
        grad_wrt_p = -bsxfun(@times,params.pareto_weights,V.*params.h);
        grad_wrt_V = -bsxfun(@times,params.pareto_weights,(p.*params.h + params.arrivals/params.rho));
        g = [sparse(numel(params.mu),1,0); grad_wrt_p(:); grad_wrt_V(:)];
    end
end

function [f, g] = Transplants(x,params,useful)
    if nargout > 1
      [c,gc] = feasibility(x,params,useful);
      f = -params.slack_cost*(params.endowment(:)+c);
      g = -gc*params.slack_cost(:);
    else
      c = feasibility(x,params,useful);
      f = -params.slack_cost*(params.endowment(:)+c);
    end
end

% Constraints:
%    feasibility
%    steady_state_composition
%    value function

% Feasibility Constraint
function [c,gc,gcpatt] = feasibility(x,params,useful)

    % N_donors constraints.
    % c is a vector with N_donors violations.
    % gc gcpatt are  matrix with as many rows as numel(x) and 
    % N_donors columns
    q   = useful.X2matQ(x);
    p   = useful.X2matP(x);
    [N_patients, N_time, N_donors] = size(q);
    if params.first_best
        c = useful.flat(sum(sum(bsxfun(@times,params.h.*p,q),2).*params.pr_compat,1)) - params.endowment(:); % <=0
    else
        V = useful.X2matV(x);
        mli  = bsxfun(@plus,params.mu,-V);         % Mean of the Latent Index
        z   = bsxfun(@times,-mli,1./params.sigma); % cutoff for standard normal
        Fz  = normcdf(-z);
        cp_t = bsxfun(@plus, bsxfun(@times, Fz,params.pr_compat),reshape(params.timeout_prob,1,1,[]));
        qcz_p_t = q.*cp_t;
        c = useful.flat(sum(sum(bsxfun(@times,params.h.*p,qcz_p_t),2),1)) - params.endowment(:); % <=0
    end
    if nargout > 1
        if params.first_best
            qc = bsxfun(@times,bsxfun(@times, q, params.pr_compat),params.h);
            pc = bsxfun(@times, repmat(params.h.*p, [1, 1, N_donors]), params.pr_compat);
            jj = kron(1:N_donors, ones(1, N_patients*N_time)); jj = jj(:);
            pcm = sparse(1:numel(q), jj, pc(:), N_patients*N_time*N_donors, N_donors);
            gc = [pcm 
                reshape(qc,size(q,1)*size(q,2),size(q,3))
                sparse(size(q,1)*size(q,2),size(q,3))];
        else
            fzs  = bsxfun(@times, bsxfun(@times,normpdf(z),1./params.sigma), params.pr_compat);
            
            gc = [bsxfun(@times,kron(speye(size(q,3)),useful.flat(params.h.*p)),cp_t(:)); 
               reshape(bsxfun(@times,cp_t,params.h).*q,size(q,1)*size(q,2),size(q,3));
               -reshape(bsxfun(@times,params.h.*p,q.*fzs),size(q,1)*size(q,2),size(q,3))];
        end
    end
    if nargout > 2
        gcpatt = [kron(speye(size(q,3)),ones(numel(p),1))
                  reshape(ones(size(q)),size(q,1)*size(q,2),size(q,3))];
        if params.first_best
            gcpatt = [gcpatt; sparse(size(q,1)*size(q,2),size(q,3))];
        else
            gcpatt = [gcpatt; reshape(ones(size(q)),size(q,1)*size(q,2),size(q,3))];
        end
    end   
end

function q = feasible_feasibility(x,params,useful)
    % N_donors constraints.
    % c is a vector with N_donors violations.
    % gc gcpatt are  matrix with as many rows as numel(x) and 
    % N_donors columns
    q   = useful.X2matQ(x);
    p   = useful.X2matP(x);
    c = 1.1;
    while c>1
        if params.first_best
            c = max(useful.flat(sum(sum(bsxfun(@times,params.h.*p,q),2).*params.pr_compat,1)) ./ params.endowment(:)); % <=0
        else
            V = useful.X2matV(x);
            mli  = bsxfun(@plus,params.mu,-V);         % Mean of the Latent Index
            z   = bsxfun(@times,-mli,1./params.sigma); % cutoff for standard normal
            Fz  = normcdf(-z);
            cp_t = bsxfun(@plus, bsxfun(@times, Fz,params.pr_compat),reshape(params.timeout_prob,1,1,[]));
            qcz_p_t = q.*cp_t;
            c = max(useful.flat(sum(sum(bsxfun(@times,params.h.*p,qcz_p_t),2),1)) ./ params.endowment(:)); % <=0
        end
        % Adjust quantities
        if c>1
            q = q/c/2;
        end
        fprintf('Max (Allocation/Endowment): %e\n',c)
    end
end

% Steady state composition
function  [c,gc,gcpatt] = steady_state_composition(x,params,useful)
    % N_patients * N_times constraints.
    % c is a vector with N_patients * N_times violations.
    % gc gcpatt are  matrix with as many rows as numel(x) and 
    % N_patients * N_times columns
    q   = useful.X2matQ(x);
    p   = useful.X2matP(x);
    [N_patients, N_time, N_donors] = size(q);
    if params.first_best
        out = exp(-(params.delta_x_h+params.lambdah_over_K.*sum(bsxfun(@times,q,params.pr_compat),3))); 
    else
        V = useful.X2matV(x);
        mli  = bsxfun(@plus,params.mu,-V);         % Mean of the Latent Index
        z   = bsxfun(@times,-mli,1./params.sigma); % cutoff for standard normal
        Fz  = normcdf(-z);
        qcz = bsxfun(@times,q.*Fz,params.pr_compat);
        out = exp(-(params.delta_x_h+params.lambdah_over_K.*sum(qcz,3)));
    end
    lead = @(x) x(:,2:end);  % lead any N_patients*N_time matrix;
    c = lead(p - params.arrivals) - p(:,1:end-1).*out(:,1:end-1);   %==0
    c = c(:); 
    if nargout > 1
       g  = [sparse(numel(q)+N_patients,numel(p)); speye(N_patients*(N_time-1)) sparse(N_patients*(N_time-1),N_patients)];
       if params.first_best
           pc = bsxfun(@times, repmat(p.*out.*params.lambdah_over_K, [1, 1, N_donors]), params.pr_compat);
           der_wrt_q = useful.offdiag(pc);
           der_wrt_q = der_wrt_q(:,1:end-N_patients);
           der_wrt_V =  sparse(N_patients*N_time, N_patients*(N_time-1));
       else
           Fzc = bsxfun(@times, Fz, params.pr_compat);
           der_wrt_q = repmat(useful.spdiag(p.*out.*params.lambdah_over_K), N_donors, 1);
           der_wrt_q = bsxfun(@times,der_wrt_q,Fzc(:));
           der_wrt_q = der_wrt_q(:,1:end-N_patients);
           fzs  = bsxfun(@times, bsxfun(@times,normpdf(z),1./params.sigma), params.pr_compat);
           der_wrt_V = -useful.spdiag(p.*out.*params.lambdah_over_K.*sum(q.*fzs,3));
           der_wrt_V = der_wrt_V(:,1:end-N_patients);
       end
       pitDer = -useful.spdiag(out);
       gc = g(:,1:end-N_patients) + [ der_wrt_q ; pitDer(:,1:end-N_patients)];
       gc = [gc; der_wrt_V];
       if nargout > 2
         pitDer = speye(numel(out));
         if params.first_best
           der_wrt_V_patt = der_wrt_V;
         else
           der_wrt_V_patt = pitDer(:, 1:end-N_patients);
         end
         qDerPat = repmat(speye(numel(p)),size(q,3),1);
         gcpatt  = [qDerPat(:,1:end-N_patients); pitDer(:,1:end-N_patients)] + g(:,1:end-N_patients);
         gcpatt = [gcpatt; der_wrt_V_patt];
       end 
    end
end


% Value Function
function [c,gc,gcpatt] = value_function(x,params,useful)
    % N_patients * N_times constraints.
    % c is a vector with N_patients * N_times violations.
    % gc amd gcpatt are matrices with as many rows as numel(x) and
    % N_patients * N_times columns
    
    q   = useful.X2matQ(x);
    V   = useful.X2matV(x);
    [N_patients, N_time, N_donors] = size(q); 
    a = params.discount_w;
    
    if params.first_best
        z   = norminv(1-q); % cutoff for standard normal
        fz  = normpdf(z);
        w_kit = bsxfun(@times,q.*params.mu + bsxfun(@times,params.sigma,fz),params.pr_compat); % Emax(0,mu-cutoff+eps)
        w   = mean(w_kit,3); 
        b   = params.discount_V1 + a.*mean(bsxfun(@times,q,params.pr_compat),3); 
    else
        z   = bsxfun(@times,bsxfun(@plus,params.mu,-V),1./params.sigma); % cutoff for standard normal
        fz  = normpdf(z);
        Fz  = normcdf(z);                        % Prob of accepting the offer  
        Emax = bsxfun(@times,params.sigma,Fz .* z + fz); % Emax(0,mu-V+eps)
        qc  = bsxfun(@times,q,params.pr_compat);
        w   = mean(bsxfun(@times,qc,Emax),3);
        b   = params.discount_V2;
    end
    
    lead = @(x) [x(:,2:end) zeros(size(V,1),1)];
    c = b .* V - a .* w - lead(V);
    c = c(:);
    
    if nargout > 1
        g  = [sparse(numel(q)+N_patients*(N_time+1),numel(V)); -useful.spdiag(ones(N_patients, N_time-1)) sparse(N_patients*(N_time-1),N_patients)];
        if params.first_best
          amod = bsxfun(@times, repmat(a, [1, 1, N_donors]), params.pr_compat)/N_donors;
          grad_wrt_q = useful.offdiag(-amod.*(params.mu+bsxfun(@times, params.sigma, z))+bsxfun(@times, amod, V));
        else
            grad_wrt_V  = [sparse(numel(q)+N_patients*N_time,numel(V)); useful.spdiag(mean(qc.*Fz,3).*a)];
            grad_wrt_q = -useful.offdiag(bsxfun(@times,a/N_donors, bsxfun(@times,Emax,params.pr_compat))) ;
            g = g + grad_wrt_V;
        end        
        gc = g + [grad_wrt_q; sparse(numel(V),numel(V)); useful.spdiag(b(:))];
        if nargout > 2
            gcpatt  = [sparse(numel(q)+N_patients*(N_time+1),numel(V)); speye(numel(b(:,1:end-1))) sparse(N_patients*(N_time-1),N_patients)];
            gcpatt  = gcpatt + [useful.offdiag(ones(size(q))); sparse(numel(V),numel(V)); speye(N_patients*N_time)];
        end
    end
end

% Feasible initial
function x = feasible_initial(opt,params,useful)
    [c,ceq,gc,gceq,gcpatt] = nlcon_base(opt.x0,params,useful);
    v = max([c(:);ceq(:)]);
    x = opt.x0;
    slack = 0.01;
    ii = 0;
    while ii==0 || (v > 1e-10 && ii<100)
        q = feasible_feasibility(x,params,useful);
        if ~isempty(params.include_pattern)
            q(params.include_pattern==0)=0;
        end
        V = update_values(q,useful.X2matV(x),params); % Right now update_values uses previous V instead of doing a complete backwards induction.
        p = update_composition(q,V,params,slack);
        x = [q(:);p(:);V(:)];
        [c,ceq,gc,gceq,gcpatt] = nlcon_base(x,params,useful);
        v = max([c(:);ceq(:)]);
        fprintf('Constraint Violation %e\n',v)
        ii = ii + 1;
    end
end

% Wraps up  non-linear constraints: Stack inequality and equality
% constraints.
% Inequality: feasibility, steady state,
% Equality:  value function
function [c,ceq,gc,gceq,gcpatt] = nlcon_base(x,params,useful)
    if nargout>2
        if nargout > 4
            [c_fea,gc_fea,gcpatt_fea] = feasibility(x,params,useful);
            [c_dyn,gc_dyn,gcpatt_dyn] = steady_state_composition(x,params,useful);
            [c_vfn,gc_vfn,gcpatt_vfn] = value_function(x,params,useful);
            gcpatt =[gcpatt_fea gcpatt_dyn gcpatt_vfn]; % Ineq, Eq
        else
            [c_fea,gc_fea] = feasibility(x,params,useful);
            [c_dyn,gc_dyn] = steady_state_composition(x,params,useful);
            [c_vfn,gc_vfn] = value_function(x,params,useful);
        end
        gc =gc_fea;
        gceq = [gc_dyn gc_vfn];
    else
        c_fea = feasibility(x,params,useful);
        c_dyn = steady_state_composition(x,params,useful);
        c_vfn = value_function(x,params,useful);
    end
    c =c_fea;
    ceq = [c_dyn;c_vfn];
    if params.debug
      if params.first_best
        [c1, ceq1, gc1, gceq1]=nlconFirstMex(x,params);
      else
        [c1, ceq1, gc1, gceq1]=nlconSecondMex(x,params);
      end
      % Check nlcon Mex
      checkError(c, c1, 'inequality Constraint', 1e-7);
      checkError(ceq, ceq1, 'equality Constraint', 1e-7);
      if nargout>2
        checkError(gc, gc1, 'inequality Constraint Gradient', 1e-10);
        checkError(gceq, gceq1, 'equality Constraint Gradient', 1e-10);
      end
    end
end

% Computation of hessian for second best
function [h, hPatt] = analyticHessianSecond(x, lambda, params, useful)
  % Output Hessian matrix of size numel(x) by numel(x)
  q   = useful.X2matQ(x);
  p   = useful.X2matP(x);
  V = useful.X2matV(x);
  [N_patients, N_time, N_donors] = size(q);
  Nq = N_patients*N_time*N_donors;
  Np = N_patients*N_time;
  lambdaSteadyState = reshape(lambda.eqnonlin(1:N_patients*(N_time-1)), N_patients, N_time-1);
  lambdaFeasibility =  full(lambda.ineqnonlin);
  lambdaValue = full(lambda.eqnonlin(N_patients*(N_time-1)+1:end));
  lambdaSteadyState = full([lambdaSteadyState zeros(N_patients,1)]);
  if params.maximize_transplants
    lambdaFeasibility = lambdaFeasibility-params.slack_cost(:);
  end
  mli  = bsxfun(@plus, params.mu, -V);         % Mean of the Latent Index
  z   = bsxfun(@times, mli, 1./params.sigma); % cutoff for standard normal
  Fz  = normcdf(z);
  fz  = normpdf(z);
  out = -params.lambdah_over_K.*params.lambdah_over_K.*lambdaSteadyState.*p.*exp(-(params.delta_x_h+params.lambdah_over_K.*sum(bsxfun(@times, q.*Fz, params.pr_compat),3)));
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Creating qq sub matrix
  NpM1 = Np-N_patients;
  I = zeros(1, NpM1*N_donors*N_donors);
  J = zeros(1, NpM1*N_donors*N_donors);
  V = zeros(1, NpM1*N_donors*N_donors);
  for i = 1:N_donors
    for j = 1:N_donors
      Fz1 = bsxfun(@times, Fz(:, :, i), params.pr_compat(:, i));
      Fz2 = bsxfun(@times, Fz(:, :, j), params.pr_compat(:, j));
      colFz = Fz1(:).*Fz2(:);
      diagVal = out(:).*colFz;
      diagVal = diagVal(1:NpM1);
      indexRange = (((i-1)*N_donors+(j-1))*NpM1+1):((i-1)*N_donors+j)*NpM1;
      iStart = (i-1)*Np;
      jStart = (j-1)*Np;
      I(indexRange) = iStart+(1:NpM1);
      J(indexRange) = jStart+(1:NpM1);
      V(indexRange) = diagVal;
    end
  end
  qq = sparse(I, J, V, Nq, Nq);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  cp_t = bsxfun(@plus, bsxfun(@times, Fz, params.pr_compat),reshape(params.timeout_prob,1,1,[]));
  mat1 = reshape(bsxfun(@times, cp_t, params.h), Np, N_donors);
  mat2 = bsxfun(@times, mat1, lambdaFeasibility');
  out = params.lambdah_over_K.*lambdaSteadyState.*exp(-(params.delta_x_h+params.lambdah_over_K.*sum(bsxfun(@times, q.*Fz, params.pr_compat), 3)));
  mat4 = reshape(bsxfun(@times, Fz,params.pr_compat), Np, N_donors);
  mat3 = mat2+bsxfun(@times, out(:), mat4);
  qp = spdiags(mat3,0:-Np:-Nq+1,Nq,Np);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  mat1 = reshape(bsxfun(@times, fz,params.pr_compat), Np, N_donors);
  mat1 = bsxfun(@times, mat1, -lambdaFeasibility');
  mat1 = bsxfun(@times, mat1, params.h(:).*p(:)./params.sigma(:));
  mat2 = reshape(fz, Np, N_donors);
  t1 = params.lambdah_over_K.*sum(bsxfun(@times, q.*fz, params.pr_compat), 3);
  t2 = -reshape(Fz, Np, N_donors);
  t2 = bsxfun(@times, t2, t1(:));
  out = -params.lambdah_over_K.*lambdaSteadyState.*exp(-(params.delta_x_h+params.lambdah_over_K.*sum(bsxfun(@times, q.*Fz, params.pr_compat),3)))./params.sigma;
  p1 = bsxfun(@times, repmat(p.*out, [1, 1, N_donors]), params.pr_compat);
  p1 = reshape(p1, Np, N_donors);
  mat2 = (mat2+t2).*p1;
  mat3 = reshape(bsxfun(@times, Fz, params.pr_compat), Np, N_donors);
  t1 = params.discount_w(:).*lambdaValue/N_donors;
  mat3 = bsxfun(@times, mat3, t1);
  qv = spdiags(mat1+mat2+mat3,0:-Np:-Nq+1,Nq,Np);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  pp = sparse(Np, Np);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  mat1 = bsxfun(@times, q.*fz, params.pr_compat);
  mat1 = bsxfun(@times, mat1, params.h);
  mat1 = reshape(mat1, Np, N_donors);
  mat1 = bsxfun(@times, mat1, 1./params.sigma(:));
  mat1 = bsxfun(@times, mat1, lambdaFeasibility');
  mat1 = -sum(mat1,2);
  t1 = sum(bsxfun(@times, q.*fz, params.pr_compat), 3);
  mat2 = t1.*out;
  if params.maximize_transplants 
    pv = useful.spdiag(mat1+mat2(:));
  else
    mat3 = -bsxfun(@times,params.pareto_weights,params.h);
    pv = useful.spdiag(mat1+mat2(:)+mat3(:));
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  mat1 = reshape(bsxfun(@times, -fz.*z.*q, params.pr_compat), Np, N_donors);
  mat1 = params.h(:).*p(:).*sum(bsxfun(@times, mat1, lambdaFeasibility'),2)./(params.sigma(:).^2);
  mat3 = sum(bsxfun(@times, q.*fz, params.pr_compat), 3);
  mat2 = mat3.*mat3.*params.lambdah_over_K+sum(bsxfun(@times, q.*fz.*z, params.pr_compat), 3);
  mat2 = mat2.*p.*out./params.sigma;
  mat3 = -mat3(:).*lambdaValue.*params.discount_w(:)./params.sigma(:)/N_donors;
  vv = useful.spdiag(mat1+mat3+mat2(:));
  h = [qq qp qv; qp' pp pv; qv' pv' vv];
  if params.debug
    % Check Mex
    h1 = analyticHessianSecondMex(x, lambda, params);
    checkError(h, h1, 'Hessian (second best)', 1e-10);
  end
  if nargout>1
    qqP = sparse(I, J, ones(1, NpM1*N_donors*N_donors), Nq, Nq);
    mat1 = ones(Np, N_donors);
    qpP = spdiags(mat1, 0:-Np:-Nq+1, Nq, Np);
    % qvP has the same pattern as qpP
    % pp is all zeros
    pvP = speye(Np);
    % vvP has the same pattern as pvP
    hPatt = [qqP qpP qpP; qpP' pp pvP; qpP' pvP' pvP];
  end
end

% Computation of hessian for first best
function [h, hPatt] = analyticHessianFirst(x, lambda, params, useful)
  q   = useful.X2matQ(x);
  p   = useful.X2matP(x);
  [N_patients, N_time, N_donors] = size(q);
  Nq = N_patients*N_time*N_donors;
  Np = N_patients*N_time;
  lambdaSteadyState = reshape(lambda.eqnonlin(1:N_patients*(N_time-1)), N_patients, N_time-1);
  lambdaFeasibility =  lambda.ineqnonlin;
  lambdaValue = full(lambda.eqnonlin(N_patients*(N_time-1)+1:end));
  lambdaSteadyState = [lambdaSteadyState zeros(N_patients,1)];
  if params.maximize_transplants
    lambdaFeasibility = lambdaFeasibility-params.slack_cost(:);
  end
  out = -params.lambdah_over_K.*params.lambdah_over_K.*lambdaSteadyState.*p.*exp(-(params.delta_x_h+params.lambdah_over_K.*sum(bsxfun(@times, q, params.pr_compat),3)));
  out1 = bsxfun(@times, 1./normpdf(norminv(1-q)), params.pr_compat);
  out1 = bsxfun(@times, reshape(lambdaValue(:), N_patients, N_time).*params.discount_w.*params.sigma, out1)/N_donors;
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Creating qq sub matrix
  I = zeros(1, Np*N_donors*N_donors);
  J = zeros(1, Np*N_donors*N_donors);
  V = zeros(1, Np*N_donors*N_donors);
  for i = 1:N_donors
    for j = 1:N_donors
      Fz1 = bsxfun(@times, out, params.pr_compat(:, i));
      Fz2 = repmat(params.pr_compat(:, j), [1, N_time]);
      diagVal = Fz1(:).*Fz2(:);
      indexRange = (((i-1)*N_donors+(j-1))*Np+1):((i-1)*N_donors+j)*Np;
      iStart = (i-1)*Np;
      jStart = (j-1)*Np;
      I(indexRange) = iStart+(1:Np);
      J(indexRange) = jStart+(1:Np);
      V(indexRange) = diagVal;
    end
  end
  qq = sparse(I, J, V, Nq, Nq);
  qq = qq + useful.spdiag(out1(:));
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  I = zeros(1, Nq);
  J = zeros(1, Nq);
  V = zeros(1, Nq);
  out1 = params.lambdah_over_K.*lambdaSteadyState.*exp(-(params.delta_x_h+params.lambdah_over_K.*sum(bsxfun(@times, q, params.pr_compat),3)));
  for i = 1:N_donors
    Fz1 = repmat(params.pr_compat(:, i), [1, N_time]);
    diagVal = params.h(:).*Fz1(:).*lambdaFeasibility(i);
    diagVal1 = Fz1(:).*out1(:);
    indexRange = (i-1)*Np+1:i*Np;
    iStart = (i-1)*Np;
    I(indexRange) = iStart+(1:Np);
    J(indexRange) = (1:Np);
    V(indexRange) = diagVal+diagVal1;
  end
  qp = sparse(I, J, V, Nq, Np);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  qv = [];
  out1 = lambdaValue.*params.discount_w(:)/N_donors;
  for i = 1:N_donors
    Fz1 = repmat(params.pr_compat(:, i), [1, N_time]);
    diagVal = Fz1(:).*out1(:);
    qv = [qv; spdiags(diagVal, 0, Np, Np)'];
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  pp = sparse(Np, Np);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  % pv = sparse(Np, Np);
  if params.maximize_transplants 
    pv = sparse(Np, Np);
  else
    pv = useful.spdiag(-bsxfun(@times,params.pareto_weights,params.h));
  end
  % pv = useful.spdiag(-bsxfun(@times,params.pareto_weights,params.h));
  % pv = pv + sp;
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  vv = sparse(Np, Np);
  h = [qq qp qv; qp' pp pv; qv' pv' vv];
  if params.debug
    % Check Mex
    h1 = analyticHessianFirstMex(x, lambda, params);
    checkError(h, h1, 'Hessian (first best)', 1e-10);
  end
  if nargout>1
    NpM1 = Np-N_patients;
    qqP = kron(ones(N_donors), sparse(1:NpM1, 1:NpM1, ones(NpM1, 1), Np, Np))+speye(Nq);
    qqP(qqP==2) = 1;
    qpP = kron(ones(N_donors, 1), speye(Np));
    qvP = kron(ones(N_donors, 1), spdiags(ones(Np, 1), 0, Np, Np)');
    % pp are vv are all zeros
    pvP = speye(Np);
    hPatt = [qqP qpP qvP; qpP' pp pvP; qvP' pvP' vv];
  end
end

% % Hessian of the objective function (For reference)
% function [hess,hesspatt] = WelfareHess(x,lambda,params,useful)
%     [f,g,hess] = Welfare(x,params,useful);
%     [N_patients,N_time,N_donors] = size(params.mu);
%     lambda_dyn = lambda.ineqnonlin(1:N_patients*N_time);
%     lambda_fea = lambda.ineqnonlin(N_patients*N_time+1:end);
%     assert(numel(lambda_fea)==N_donors);
%     off_diag_block = repmat(useful.spdiag(lambda_dyn),N_donors,1) +kron(lambda_fea(:),speye(N_patients*N_time));
%     lambda_part = [sparse(numel(params.mu),numel(params.mu)) off_diag_block; 
%                   off_diag_block' sparse(N_patients*N_time,N_patients*N_time)]; 
%     hess = hess + lambda_part;
%     if nargout>1
%         off_diag_block = useful.offdiag(ones(numel(params.mu),1)); 
%         hesspatt = [speye(numel(params.mu)) off_diag_block
%                     off_diag_block'        sparse(N_patients*N_time,N_patients*N_time) ];
%         off_diag_block = repmat(speye(numel(lambda_dyn)),N_donors,1) +kron(ones(numel(lambda_fea),1),speye(N_patients*N_time));
%         lambda_part = [sparse(numel(params.mu),numel(params.mu)) off_diag_block; 
%                   off_diag_block' sparse(N_patients*N_time,N_patients*N_time)]; 
%         assert(isequal(spones(hess),hesspatt));
%     end
% end


% Functions to make the problem smaller

% makes the problem smaller
function [opt, v_sel, params] = smaller_problem(opt, params, useful, useMex,useRealq)
    pattern = params.include_pattern>0;
    [N_patients,N_time,N_donors] = size(params.mu);
    
    % Create v_sel structure with functions to go from the smaller to the
    % bigger problem and vice versa.
    v_sel = struct();
    
    aux_patientstime    = squeeze(any(pattern,3));        aux_patientstime = aux_patientstime(:);
    aux_patientstimem1    = squeeze(any(pattern(:,1:end-1,:), 3));        aux_patientstimem1 = aux_patientstimem1(:);
    aux_donors          = squeeze(any(any(pattern,1),2)); aux_donors       = aux_donors(:);

    v_sel.included_vars     = [pattern(:); aux_patientstime; aux_patientstime];  % Selects included matches: q,p,V
    v_sel.included_ineqnonlin  = aux_donors;  % Selects included in eq constraints: feasibility
    v_sel.included_eqnonlin    = [aux_patientstimem1; aux_patientstime;]; % Selects included eq constraints: steady_state value_function
    v_sel.aux_donors = aux_donors;
    
    v_sel.expand        = @(x) full(sparse(find(v_sel.included_vars),1,x(:),N_patients*N_time*(N_donors+2),1)); 
    v_sel.expand_ineqnonlin = @(x) full(sparse(find(v_sel.included_ineqnonlin),1,x(:), N_donors,1));
    v_sel.expand_eqnonlin  = @(x) full(sparse(find(v_sel.included_eqnonlin),1,x(:), N_patients*(2*N_time-1),1));
    v_sel.included_fun = @(x) x(v_sel.included_vars);
    
    if useRealq
        v_sel.expand        = @(x) full(sparse(find(v_sel.included_vars),1,tRto01(x(:),sum(pattern(:))),N_patients*N_time*(N_donors+2),1)); 
        [~,~,~,opt.x0]      = tRto01(opt.x0,N_patients*N_time*N_donors);
        [~,~,~,opt.lb]      = tRto01(opt.lb,N_patients*N_time*N_donors);
        [~,~,~,opt.ub]      = tRto01(opt.ub,N_patients*N_time*N_donors);
        
    end
    

    % Adjust opt
    opt.x0 = opt.x0(v_sel.included_vars);
    opt.lb = opt.lb(v_sel.included_vars);  
    opt.ub = opt.ub(v_sel.included_vars);
    opt.obj = @(x) ObjectiveSmall(x,params,useful,v_sel);
    if useMex
      params = smallProblemPrecomputeParamsMex(params, uint64(v_sel.included_vars), uint64(v_sel.aux_donors));
      if useRealq, funt = @(x) tRto01(x,sum(pattern(:))); else funt = @(x) x; end 
      if params.first_best
        opt.analyticHessian_sm = @(x, lambda) analyticHessianSmallFirstMex(funt(x), lambda, params, uint64(v_sel.included_vars));
        opt.nlcon_sm = @(x) nlconSmallFirstMex(funt(x), params, uint64(v_sel.included_vars), uint64(v_sel.aux_donors));
      else
        opt.analyticHessian_sm = @(x, lambda) analyticHessianSmallSecondMex(funt(x), lambda, params);
        opt.nlcon_sm = @(x) nlconSmallSecondMex(funt(x), params, uint64(v_sel.included_vars), uint64(v_sel.aux_donors));
      end
    else
      if params.debug
        params = smallProblemPrecomputeParamsMex(params, uint64(v_sel.included_vars), uint64(v_sel.aux_donors));
      end
      opt.nlcon_sm = @(x) nlconSmall(x, v_sel, params, opt.nlcon_base);
      opt.analyticHessian_sm = @(x,lambda) analyticHessianSmall(x, lambda, v_sel, params, opt.analyticHessian_base);
    end
    
    if useRealq
      opt.analyticHessian_small = @(x,lambda) analyticHessianRto01(x,lambda,sum(pattern(:)),opt.analyticHessian_sm,opt.nlcon_sm,opt.obj);
      opt.nlcon_small = @(x) nlconRto01(x,sum(pattern(:)),opt.nlcon_sm);
      % opt.obj   = @(x) ... No need!
    else
      opt.analyticHessian_small = opt.analyticHessian_sm;
      opt.nlcon_small           = opt.nlcon_sm;
    end
end

function varargout = ObjectiveSmall(x,params,useful,v_sel)
  if params.maximize_transplants
    [varargout{1:nargout}] = Transplants(v_sel.expand(x),params,useful);
  else
    [varargout{1:nargout}] = Objective(v_sel.expand(x),params,useful);
  end
  if nargout > 1, varargout{2} = varargout{2}(v_sel.included_vars);  end
  if nargout > 2, varargout{3} = varargout{3}(v_sel.included_vars,v_sel.included_vars); end
end

% Smaller set of included constraints
function varargout  = nlconSmall(x, v_sel, params, big_nlcon)
    [varargout{1:nargout}] = big_nlcon(v_sel.expand(x));
    
    varargout{1} = varargout{1}(v_sel.included_ineqnonlin);  % c
    varargout{2} = varargout{2}(v_sel.included_eqnonlin);  % ceq
    if nargout > 2
        varargout{3} = varargout{3}(v_sel.included_vars,v_sel.included_ineqnonlin); % gc
        varargout{4} = varargout{4}(v_sel.included_vars,v_sel.included_eqnonlin); % gceq
    end
    if nargout > 4
        varargout{5} = varargout{5}(v_sel.included_vars,[v_sel.included_ineqnonlin; v_sel.included_eqnonlin]); % gcpatt
    end
    % if nargout>4, assert(isequal(spones(varargout{3}),varargout{4})); end (it may fail if it is evaluated in a non-generic point)
    if params.debug
      y = v_sel.expand(x);
      y = y(v_sel.included_vars);
      if params.first_best
        [c1, ceq1, gc1, gceq1] = nlconSmallFirstMex(y, params, uint64(v_sel.included_vars), uint64(v_sel.aux_donors));
      else
        [c1, ceq1, gc1, gceq1] = nlconSmallSecondMex(y, params, uint64(v_sel.included_vars), uint64(v_sel.aux_donors));
      end
      checkError(varargout{1}, c1, 'Small inequality constraint', 1e-7);
      checkError(varargout{2}, ceq1, 'Small equality constraint', 1e-7);
      if nargout > 2
        checkError(varargout{3}, gc1, 'Small inequality constraint gradient', 1e-10);
        checkError(varargout{4}, gceq1, 'Small equality constraint gradient', 1e-10);
      end
    end
end

function varargout = analyticHessianSmall(x, lambda, v_sel, params, bigHessian)
    if params.debug
      % To test mex
      if params.first_best
        hessianTest = analyticHessianSmallFirstMex(x, lambda, params, uint64(v_sel.included_vars));
      else
        hessianTest = analyticHessianSmallSecondMex(x, lambda, params);
      end
    end
    % Test mex ends
    lambda.ineqnonlin = v_sel.expand_ineqnonlin(lambda.ineqnonlin);
    lambda.eqnonlin = v_sel.expand_eqnonlin(lambda.eqnonlin);
    [varargout{1:nargout}] = bigHessian(v_sel.expand(x),lambda);
    for ii = 1:nargout
        varargout{ii} = varargout{ii}(v_sel.included_vars,v_sel.included_vars);
    end
    if params.debug
      % To test mex
      % Check for nan
      checkError(varargout{1}, hessianTest, 'Small Hessian', 1e-10);
    end
end

%% Rto01
function [q,dq,ddq,R] = tRto01(R,sizeq);   % Executes Rto01 passing only the relevant variables.
    [varargout{1:nargout}] = Rto01(R(1:sizeq)); % Avoid unnecesary computations 
    q = R; q(1:sizeq) = varargout{1};  % Change of variables           
    if nargout > 1,   % First derivative
        dq          = ones(size(R));     
        dq(1:sizeq) = varargout{2};
        if nargout > 2, % Second derivative
            ddq          = zeros(size(R));
            ddq(1:sizeq) = varargout{3};
            if nargout>3  % inverse function
                R(1:sizeq) = varargout{4};
            end
        end
        
    end
end

function varargout  = nlconRto01(x, sizeq, orig_nlcon)
    [varargout{1:nargout}] = orig_nlcon(x);

    if nargout > 2
        [~,dq] = tRto01(x,sizeq);
        varargout{3} = bsxfun(@times,varargout{3},dq); % gc   (chain rule)
        varargout{4} = bsxfun(@times,varargout{4},dq); % gceq (chain rule) 
    end
end

function varargout = analyticHessianRto01(x,lambda, sizeq, orig_analyticHessian,orig_nlcon,orig_obj)
    [~,dq,ddq]  = tRto01(x,sizeq);
    [varargout{1:nargout}] = orig_analyticHessian(x,lambda);
    spdiag  = @(x) sparse(1:numel(x),1:numel(x),x(:),numel(x),numel(x),numel(x));
    dq = spdiag(dq);
    [~,~,gc,gceq] = orig_nlcon(x);
    [~,g] = orig_obj(x);
    % (chain rule)^2
    varargout{1} = dq*varargout{1}*dq + spdiag((g+gceq*lambda.eqnonlin+gc*lambda.ineqnonlin).*ddq);
end

function [q,dq,ddq,R] = Rto01(R);
    index = 'logit';
    switch index
        case 'logit'
            % Logit
            ex  = exp(-R);
            q   = (1+ex).^(-1);
            if nargout>1  % first derivative
                dq  = ex.*q.^2;
                if nargout>2  % second derivative
                    ddq = - ex.*(1-ex).*q.^3;
                    if nargout>3  % inverse function
                        R = - log(1./R - 1);
                    end
                end
            end
    end
end

function [] = check_derivatives(opt,params,useful)
    % Auxiliary piece of code that check derivatives and obtain Hessian and Jacobian Patterns
    [N_patients,N_time,N_donors]= size(params.mu);
    aux.ineqnonlin = rand(N_patients*N_time+N_donors,1)*4; 
    aux.eqnonlin = rand(N_patients*N_time,1)*4;
    aux.x0         = rand(size(opt.x0))*0.9999 + 1e-6;       % Initial conditions: keep them away from 0 or 1.
    % Check derivatives uses a toolkit called derivest that contains 
    % functions gradest,jacobianest, and hessian
    %https://www.mathworks.com/matlabcentral/fileexchange/13490-adaptive-robust-numerical-differentiation

    % Jacobian: Value Function Constraints (First best)
    params.first_best = 1;
    this_cons = @(x) value_function(x,params,useful);
    [~,gc] = this_cons(aux.x0);
    [jac,err] = jacobianest(this_cons,aux.x0); jac = jac';
    fprintf('1st Best: Jacobian Value Function Norm Diff: %e. Approx error %e\n',norm(gc(:)-jac(:)),max(err(:)));
    % Jacobian: Welfare Constraints (Second best)
    params.first_best = 0;
    this_cons = @(x) value_function(x,params,useful);
    [~,gc] = this_cons(aux.x0);
    [jac,err] = jacobianest(this_cons,aux.x0); jac = jac';
    fprintf('2nd Best: Jacobian Value Function Norm Diff: %e. Approx error %e\n',norm(gc(:)-jac(:)),max(err(:)));

    % Jacobian: Feasibility Constraints (First best)
    params.first_best = 1;
    this_cons = @(x) feasibility(x,params,useful);
    [~,gc] = this_cons(aux.x0);
    [jac,err] = jacobianest(this_cons,aux.x0); jac = jac';
    fprintf('1st Best: Jacobian Feasibility Norm Diff: %e. Approx error %e\n',norm(gc(:)-jac(:)),max(err(:)));
    % Jacobian: Welfare Constraints (Second best)
    params.first_best = 0;
    this_cons = @(x) feasibility(x,params,useful);
    [~,gc] = this_cons(aux.x0);
    [jac,err] = jacobianest(this_cons,aux.x0); jac = jac';
    fprintf('2nd Best: Jacobian Feasibility Norm Diff: %e. Approx error %e\n',norm(gc(:)-jac(:)),max(err(:)));

    % Jacobian: Steady State Constraints (First best)
    params.first_best = 1;
    this_cons = @(x) steady_state_composition(x,params,useful);
    [~,gc] = this_cons(aux.x0);
    [jac,err] = jacobianest(this_cons,aux.x0); jac = jac';
    fprintf('1st Best: Jacobian Steady State Norm Diff: %e. Approx error %e\n',norm(gc(:)-jac(:)),max(err(:)));
    % Jacobian: Steady State Constraints (Second best)
    params.first_best = 0;
    this_cons = @(x) steady_state_composition(x,params,useful);
    [~,gc] = this_cons(aux.x0);
    [jac,err] = jacobianest(this_cons,aux.x0); jac = jac';
    fprintf('2nd Best: Jacobian Steady State Norm Diff: %e. Approx error %e\n',norm(gc(:)-jac(:)),max(err(:)));

    % Gradient and Hessian
    obj = @(x) Objective(x,params,useful);
    [~,ga,ha]= obj(aux.x0);
    [grad,err] = gradest(obj,aux.x0);
    [hess] = hessian(obj,aux.x0);
    fprintf('Gradient Norm Diff: %e;\nHessian Norm Diff %e. Approx error %e\n',norm(ga(:)-grad(:)),norm(ha(:)-hess(:)),max(err(:)));

    % Hessian of the Lagrangian
    params.first_best = 1;
    obj_cons = @(x) opt.obj(x) + aux.ineqnonlin'*[feasibility(x,params,useful); steady_state_composition(x,params,useful)] + aux.eqnonlin'*value_function(x,params,useful);
    [hess,err] = hessian(obj_cons,aux.x0);
    opt.analyticHessian = @(x, lambda) analyticHessianFirst(x, lambda, params, useful);
    ha =opt.analyticHessian(aux.x0,aux);
    fprintf('1st Best: Hessian Lagrangian Norm Diff %e. Approx error %e\n',norm(ha(:)-hess(:)),max(err(:)));
    
    params.first_best = 0;
    obj_cons = @(x) opt.obj(x) + aux.ineqnonlin'*[feasibility(x,params,useful); steady_state_composition(x,params,useful)] + aux.eqnonlin'*value_function(x,params,useful);
    [hess,err] = hessian(obj_cons,aux.x0);
    opt.analyticHessian = @(x, lambda) analyticHessianSecond(x, lambda, params, useful);
    ha =opt.analyticHessian(aux.x0,aux);
    fprintf('2nd Best:  Hessian Lagrangian Norm Diff %e. Approx error %e\n',norm(ha(:)-hess(:)),max(err(:)));
end

function [L,g,H] = lagrangean(opt,x,lambda)
    [f,g]           = opt.obj(x);
    [c,ceq,gc,gceq] = opt.nlcon(x);
    L               = f + c'*lambda.ineqnonlin + ceq'*lambda.eqnonlin;
    g               = g + gc*lambda.ineqnonlin + gceq*lambda.eqnonlin;
    if nargout>2
        H           = opt.analyticHessian(x,lambda);
    end
    fprintf('.');
end

function [] = check_derivatives2(opt,params)
    % Auxiliary piece of code that check derivatives and obtain Hessian and Jacobian Patterns
    [N_patients,N_time,N_donors]= size(params.mu);
    [c,ceq] = opt.nlcon(opt.x0);
    aux.ineqnonlin = rand(size(c))*4; 
    aux.eqnonlin   = rand(size(ceq))*4;
    
    % Check derivatives uses a toolkit called derivest that contains 
    % functions gradest,jacobianest, and hessian
    %https://www.mathworks.com/matlabcentral/fileexchange/13490-adaptive-robust-numerical-differentiation

    % Hessian of the Lagrangian
    lagrangeanfun = @(x) lagrangean(opt,x,aux);
    [L,gc,ha] = lagrangeanfun(opt.x0);
    [jac,err] = jacobianest(lagrangeanfun,opt.x0); jac = jac';
    fprintf('Jacobian Norm Difference %e. Approx error %e\n',norm(gc(:)-jac(:)),max(err(:)));
    [hess,Herr] = hessian(lagrangeanfun,opt.x0);
    fprintf('Hessian Norm Diff %e. Approx error %e\n',norm(ha(:)-hess(:),'inf'),max(err(:)));
    hessoff =hess-diag(diag(hess));  
    haoff  = ha-diag(diag(ha));
    erroff = err - diag(diag(err));
    fprintf('Hessian OffDiag Norm Diff %e. Approx error %e\n',norm(haoff(:)-hessoff(:)),max(erroff(:)));
    
    
    % Check Rto1
    nRto01 = @(x) tRto01(x,sum(params.include_pattern(:)));
    [q,dq,ddq] = nRto01(opt.x0);
    [Rjac,err] = jacobianest(nRto01,opt.x0);
    fprintf('Rto01 Jacobian Norm Difference %e. Approx error %e\n',norm(dq(:)-diag(Rjac)),max(err(:)));
    sumnRto01 = @(x) sum(nRto01(x));
    [Rhess,err] = hessian(sumnRto01,opt.x0);
    
    fprintf('Rto01 Hessian Norm Diff %e. Approx error %e\n',norm(ddq(:)-diag(Rhess)),max(err(:)));
end

function [] = monitor_memory_whos()
%MONITOR_MEMORY_WHOS uses the WHOS command and evaluates inside the BASE
%workspace and sums up the bytes.  The output is displayed in GB.
  mem_elements = evalin('caller','whos');
  if size(mem_elements,1) > 0
    for i = 1:size(mem_elements,1)
      memory_array(i) = mem_elements(i).bytes;
    end

    memory_in_use = sum(memory_array);
    memory_in_use = memory_in_use/1048576/1024;
  else
    memory_in_use = 0;
  end
  fprintf('Current workspace memory usage : %f GB\n', memory_in_use);
end

function [] = checkError(c1, c2, s1, tol)
  c1 = c1(:);
  c2 = c2(:);
  rE = (c2-c1)./c1;
  filter = abs(c1) < 1e-6;
  rE(filter) = c2(filter)-c1(filter); % Use absolute error when close to zero
  if sum(find(isinf(c2))-find(isinf(c1)))
    fprintf(['ERROR! Infinity in arrays do not match in ', s1, '\n']);
    keyboard;
  end
  filter = isinf(c1);
  rE(filter) = 0; % Zero out infinity
  if sum(find(isnan(c2))-find(isnan(c1)))
    fprintf(['ERROR! NaN in arrays do not match in ', s1, '\n']);
    keyboard;
  end
  filter = isnan(c1);
  rE(filter) = 0;
  if sum(find(isnan(rE)))
    fprintf(['ERROR! NaN Found in ', s1, '\n']);
    keyboard;
  end
  d1 = norm(rE);
  if d1 > tol
    fprintf(['Relative difference in ', s1, ' is %e\n'], d1);
    fprintf(['ERROR! in mex ', s1, '\n']);
    keyboard;
  end
end
