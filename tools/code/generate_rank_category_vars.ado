// This file generates the categorical variables needed to determine the ranking category an offer falls into

program generate_rank_category_vars
  
// ABDR - # antigen mismatches
gen ABDR = "Non-Zero"
replace ABDR = "Zero" if hlamis == 0

// ABO - blood type match. This doesn't completely classify blood types because it exploits that all offers are compatible
gen ABO = "Compatible"
replace ABO = "O Donor B Candidate" if abo_don == "O" & abo == "B"
foreach var in abo abo_don{
gen `var'_base = `var'
replace `var'_base = subinstr(`var'_base,"1","",1)
replace `var'_base = subinstr(`var'_base,"2","",1)
}
replace ABO = "Identical" if abo_base == abo_don_base
drop abo_base abo_don_base

// Geo - local, regional, national, payback. Leveraging the "descrip" field in the offer dataset rather than using DSA names directly
gen Geo = "Local" if match_opo_dsa_id == listing_ctr_dsa_id
replace Geo = "Local" if (match_opo_dsa_id == "CTOP-OP1" & listing_ctr_dsa_id== "MAOB-OP1") | (match_opo_dsa_id=="MAOB-OP1" & listing_ctr_dsa_id=="CTOP-OP1")
replace Geo = "Regional" if Geo == "" & match_opo_region_id==listing_ctr_region_id
replace Geo = "National" if Geo == ""

// Ped_list - whether under 18 at listing
gen Ped_List = "Non-Ped At Listing"
replace Ped_List = "Ped At Listing" if init_age < 18

// Ped_alloc - whether under 18 at allocation
gen Ped_Alloc = "Non-Ped At Allocation"
replace Ped_Alloc = "Ped At Allocation" if init_age < 18 & date_turn_18 > match_date

// CPRA - whether highly, moderately, or mildly sensitized.
gen CPRA = "CPRA<=20"
replace CPRA = "21<=CPRA<=79" if cpra>.205 & cpra<.795
replace CPRA = "CPRA>=80" if cpra>=.795 & cpra != .

// These categories are only valid from 2009-10-01 - 2014-12-03
gen valid_rank_categories = (match_date >= 18171 & match_date <= 20060 )


end
