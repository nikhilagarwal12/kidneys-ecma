# Library to compute bins and points in KAS

import csv
import re
import datetime
import math
import pdb

# We should classify donors using the following categories
# CAT 1: Donors with KDPI <= 20%
# CAT 2: Donors with KDPI > 20% & KDPI<=35%
# CAT 3: Donors with KDPI > 35% & KDPI<=85%
# CAT 4: Donors with KDPI > 85%

def compute_epts(pt,match):
    # Prior organ transplant receipient
    
    prior_organ_transplant = match['match_date'] > pt['first_tx_date']
    
    # Years on dialysis
    dial_date =  pt['dialysis_date'].timetuple()
    match_date = match['match_date'].timetuple()
    years_on_dialysis = max(match_date.tm_year - dial_date.tm_year - 1*(match_date.tm_yday < dial_date.tm_yday),0)
    # Diabetes
    diabetes = pt['diab'].strip() in set(['2','3','4','5'])
    # Age
    age_from_25 = max(match['c_age'] - 25,0)
    # EPTS
    epts_raw = 0.047*age_from_25 - 0.015*diabetes*age_from_25 + 0.398*prior_organ_transplant - 0.237*diabetes*prior_organ_transplant + 0.315*math.log(years_on_dialysis + 1) - 0.099*diabetes*math.log(years_on_dialysis + 1) + 0.130*(years_on_dialysis==0) - 0.348*diabetes*(years_on_dialysis==0) + 1.262*diabetes
    match['Top20epts'] = epts_raw <= 1.48933264887064
    return match
    
def greedy_kdri_cat(donor):
    if donor['kdri_cat'] == '0-20%':
        kdri_cat = 1 
    elif donor['kdri_cat'] == '21-34%':
        kdri_cat = 2
    elif donor['kdri_cat'] == '35-85%':
        kdri_cat = 3
    elif donor['kdri_cat'] == '86-100%':
        kdri_cat = 4
    else:
        kdri_cat = 0
         
    # Greedy kdri_cat
    if kdri_cat == 0 and not donor['kdpi']=='':
        if float(donor['kdpi']) < 0.205:
            kdri_cat = 1
        elif float(donor['kdpi'])< 0.355:
            kdri_cat = 2
        elif float(donor['kdpi'])< 0.855:
            kdri_cat = 3
        else:
            kdri_cat = 4
    
    donor['kdri_cat'] = kdri_cat
    
    return donor

def bin(donor,pt,match,rules):
    compute_epts(pt,match)
    greedy_kdri_cat(donor)
    pt['cpra_kas'] = round(pt['cpra']*100)
    these_rules = rules[donor['kdri_cat']-1]
    match['Top20epts_or_Ped'] = match['Top20epts'] or match['Ped_Alloc'] == 'Ped At Allocation'
    
    for rule in these_rules:
        # Geo
        rule_applies = rule['geo'] == 'National' or match['geo'] == 'Local' or rule['geo'] == match['geo'] 
        
        # Pediatric Criteria and hlamis
        for crit in ['Ped_List','Ped_Alloc','hlamis']:
            rule_applies = rule_applies and (rule[crit] == match[crit] or rule[crit]== "Any")
            
        # Age and Top 20% Criteria
        rule_applies = rule_applies and (rule['Top20epts']=='Any' or match['Top20epts'])
        rule_applies = rule_applies and (rule['Top20epts_or_Ped']=='Any' or match['Top20epts'] or match['Ped_Alloc'] == 'Ped At Allocation')
        
        # Patient criteria
        rule_cpra = rule['cpra']== ''  or (rule['cpra']== "100%" and pt['cpra_kas']==100) or (rule['cpra']== "99%" and pt['cpra_kas']==99)
        rule_cpra = rule_cpra or (rule['cpra']== "98%" and pt['cpra_kas']==98) or (rule['cpra']== "80%100%" and pt['cpra_kas']>=80)
        rule_cpra = rule_cpra or (rule['cpra']== "21%79%" and pt['cpra_kas']>20 and pt['cpra_kas']<80) or (rule['cpra']== "0%20%" and pt['cpra_kas']<=20)
        rule_applies = rule_applies and rule_cpra
        
        rule_applies = rule_applies and (rule['prior_living_donor']=='Any' or (rule['prior_living_donor']==pt['prior_living_donor']))
        
        # ABO
        if rule['abo_pat'] == 'Identical':
            rule_applies = rule_applies and (match['c_abo'] == 'Identical')
        elif rule['abo_pat'] == 'B':
            rule_applies = rule_applies and (pt['abo'] == 'B')
        elif rule['abo_pat'] == 'Compatible':
            rule_applies = rule_applies and not (match['c_abo'] == 'Incompatible')
        
        if rule['abo_don'] == 'O':
            rule_applies = rule_applies and (donor['abo_don'] == 'O')
        elif rule['abo_don'] == 'A2 Or A2B':
            rule_applies = rule_applies and (donor['abo_don'] in ['A2','A2B'])
            
        
        if rule_applies:
            match['kas_bin'] = int(rule['num'])
            break
    
    if not rule_applies:        
        print pt['wlreg_audit_id_code']
        print donor['donor_id']
    return match    
    
def cpra_points(pt):
    # CPRA values will be rounded to the nearest one hundredth percentage. (Policy 3.5.1)
    pt['cpra_kas'] = round(pt['cpra']*100)
    
    # Points following Policy 3.5.3, Table 3.5-2:
    if   pt['cpra_kas'] <= 19:
        pt['kas_cpra_points'] =   0.00
    elif pt['cpra_kas'] <= 29:
        pt['kas_cpra_points'] =   0.08 
    elif pt['cpra_kas'] <= 39:
        pt['kas_cpra_points'] =   0.21 
    elif pt['cpra_kas'] <= 49:
        pt['kas_cpra_points'] =   0.34
    elif pt['cpra_kas'] <= 59:
        pt['kas_cpra_points'] =   0.48
    elif pt['cpra_kas'] <= 69:
        pt['kas_cpra_points'] =   0.81 
    elif pt['cpra_kas'] <= 74:
        pt['kas_cpra_points'] =   1.09
    elif pt['cpra_kas'] <= 79:
        pt['kas_cpra_points'] =   1.58
    elif pt['cpra_kas'] <= 84:
        pt['kas_cpra_points'] =   2.46
    elif pt['cpra_kas'] <= 89:
        pt['kas_cpra_points'] =   4.05 
    elif pt['cpra_kas'] <= 94:
        pt['kas_cpra_points'] =   6.71 
    elif pt['cpra_kas'] <= 95:
        pt['kas_cpra_points'] =  10.82 
    elif pt['cpra_kas'] <= 96:
        pt['kas_cpra_points'] =  12.17 
    elif pt['cpra_kas'] <= 97:
        pt['kas_cpra_points'] =  17.30
    elif pt['cpra_kas'] <= 98:
        pt['kas_cpra_points'] =  24.40
    elif pt['cpra_kas'] <= 99:
        pt['kas_cpra_points'] =  50.09
    elif pt['cpra_kas'] <= 100:
        pt['kas_cpra_points'] = 202.10
    return pt

    
def points(donor,pt,match):
    # Listed for transplant and meets the qualifying criteria described in Policy 3.5.4 Waiting Time
    # 3.5.6.1, 3.5.6.2, 3.5.6.3, or 3.5.6.4
    # 1/365 points for each day since the qualifying criteria in Policy 3.5.4 Waiting Time
    #init_date = datetime.datetime.strptime(pt['init_date'],"%d%b%Y");
    #match['c_tie_break'] = (match['match_date'] - match['init_date']).days
    #match['kas_addtl_pts'] = 1.0*(match['c_tie_break']/365)
    # See how we deal with waittime points in preKAS
    match['kas_addtl_pts'] = 0
    #Aged 0-10 at time of match and a 0-ABDR mismatch with the donor
    # 3.5.6.1, 3.5.6.2, or 3.5.6.3
    #4 points
    
    if match['c_age'] <= 10 and match['hlamis'] == 'Zero' and donor['kdri_cat']<4:
        match['kas_addtl_pts'] = match['kas_addtl_pts'] + 4
        
    #Aged 11-17 at time of match and a 0-ABDR mismatch with the donor
    # 3.5.6.1, 3.5.6.2, or 3.5.6.3
    # 3 points
    
    if match['c_age'] > 10 and match['c_age'] <= 17 and match['hlamis'] == 'Zero' and donor['kdri_cat']<4:
        match['kas_addtl_pts'] = match['kas_addtl_pts'] + 3
        
    #Aged 0-10 at time of match and donor has a KDPI score <35%
    # 3.5.6.1 or 3.5.6.2
    # 1 point

    if match['c_age'] > 10 and match['c_age'] <= 17 and donor['kdri_cat']<3: # KDPI score<35 implied by kdri_cat<2
        match['kas_addtl_pts'] = match['kas_addtl_pts'] + 1
   
    # A prior living donor
    # 3.5.6.1, 3.5.6.2, or 3.5.6.3
    # 4 points 
    if match['prior_living_donor'] == "1"  and donor['kdri_cat']<4:
        match['kas_addtl_pts'] = match['kas_addtl_pts'] + 4
    
    # Sensitized (CPRA at least 20%)
    # 3.5.6.1, 3.5.6.2, or 3.5.6.3
    # See table
    if donor['kdri_cat']<4:
        cpra_points(pt)
        match['kas_addtl_pts'] = match['kas_addtl_pts'] + pt['kas_cpra_points'] 
    
    # Sharing a single HLA-DR mismatch with the donor*
    # 3.5.6.1, 3.5.6.2, or 3.5.6.3
    # 1 point
    if match['dr_mm']==1 and donor['kdri_cat']<4:
        match['kas_addtl_pts'] = match['kas_addtl_pts'] + 1
    
    # Sharing a zero HLA-DR mismatch with the donor*
    # 3.5.6.1, 3.5.6.2, or 3.5.6.3    
    # 2 points
    if match['dr_mm']==0 and donor['kdri_cat']<4:
        match['kas_addtl_pts'] = match['kas_addtl_pts'] + 2
    
    # Round points: we'll use coarse points anyways.
    match['kas_addtl_pts'] = int(round(match['kas_addtl_pts']))
    
    return match
    