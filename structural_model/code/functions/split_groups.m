% This function splits patients and donor groups according to our definition of groups;        
        
function [pt_include,k_include,group_label] = split_groups(donors,pt_history,patients,groups,gr);
    pt_include = true(size(pt_history,1),1);
	k_include = true(size(donors,1),1);
	this_group = groups{gr};
	[found_pth,where] = ismember(pt_history.wl_id_code,patients.wl_id_code);
	assert(all(found_pth));
	group_label = this_group.label;
	for k = 1:numel(this_group.conditions)
		switch this_group.conditions(k).data
			case 'pt_history', d = pt_history;
			case 'patients', d = patients;
			case 'donors', d = donors;
		end
		vars = {};
		for v = 1: numel(this_group.conditions(k).variables)
			vars{v} = d.(this_group.conditions(k).variables{v});
		end
		outcome=this_group.conditions(k).f(vars{:});
		switch this_group.conditions(k).data
			case 'pt_history',
				pt_include = pt_include & outcome;
			case 'patients', d = patients;
			    pt_include = pt_include & outcome(where);
			case 'donors', d = donors;
				k_include = k_include & outcome;
		end
	end
		