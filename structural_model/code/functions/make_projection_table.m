function [out]=make_projection_table(V,theta,chi,model,options) %%,print_map_names,varUnits,extrarows)

%% Check that we have as many coefficients as names
assert(model.K_types*model.flex.K + model.fixed.K==size(chi,2));

%%% Value function
vf_all = false(model.K_types,model.K_theta);  % holds type-specific indices in chi for VF projection
vf_fixed = ismember(model.fixed.colheaders,model.vf_projection);
if model.pt_uh  % columns to use in projection
    for kk = 1:model.K_types
        vf_flex = false(1,model.K_types*model.flex.K);
        vf_flex(((kk-1)*model.flex.K+1):(kk*model.flex.K)) = ismember(model.flex.colheaders,model.vf_projection);
        vf_all(kk,:) = [vf_fixed vf_flex];
        mu_dm(:,kk) = [model.fixed.mu_dm(vf_fixed)';model.flex.mu_dm(ismember(model.flex.colheaders,model.vf_projection))'];        
        sigma_dm(:,kk) = [model.fixed.sigma_dm(vf_fixed)';model.flex.sigma_dm(ismember(model.flex.colheaders,model.vf_projection))'];
    end
else
    vf_all = vf_fixed; mu_dm = model.fixed.mu_dm(vf_fixed)'; sigma_dm = model.fixed.sigma_dm(vf_fixed)';
end
if model.pt_uh                % variable names used in projection
    coefficient_names = [model.fixed.colheaders(ismember(model.fixed.colheaders,model.vf_projection)) ...  % put 
        model.flex.colheaders(ismember(model.flex.colheaders,model.vf_projection))];
else
    coefficient_names = model.fixed.colheaders(ismember(model.fixed.colheaders,model.vf_projection));
end
assert(size(vf_all,2)==size(chi,2))
b = zeros(sum(vf_all(1,:)),model.K_types); 
se = zeros(sum(vf_all(1,:)),model.K_types);
r2 = zeros(1,model.K_types); 
for kk = 1:model.K_types
    vf = vf_all(kk,:);
    b(:,kk) = (chi(:,vf,kk)\V(:,kk));
    r2(kk) = var(chi(:,vf,kk)*b(:,kk))/var(V(:,kk));
    se(:,kk) = sqrt(diag(inv(chi(:,vf,kk)'*chi(:,vf,kk)))*std(V(:,kk)-chi(:,vf,kk)*b(:,kk)));
end
fid = fopen(options.results_savename(model,'V','csv'), 'w');
write_header(['_' model.model_name],fid,model.K_types);
write_table(coefficient_names,b,se,mu_dm,sigma_dm,r2,fid,model.K_types);
fclose(fid);
out.V_coeff = b;
out.vf = vf_all;

%%% Gamma
out.method = 'Project Gamma';
gamma = zeros(size(V)); K_coef = model.fixed.K+model.flex.K;
b = zeros(K_coef,model.K_types); se = zeros(K_coef,model.K_types); r2 = zeros(1,model.K_types);
switch out.method
    case 'Project Gamma'
        %% Project V+mu on chi
        if model.pt_uh
            for kk = 1:model.K_types
                gamma(:,kk) = V(:,kk) - chi(:,:,kk)*theta;
                kk_ind = [1:model.fixed.K (model.fixed.K + (((kk-1)*model.flex.K+1):(kk*model.flex.K)))];
                b(:,kk) = (chi(:,kk_ind,kk)\gamma(:,kk));
                se(:,kk) = sqrt(diag(inv(chi(:,kk_ind,kk)'*chi(:,kk_ind,kk)))*std(gamma(:,kk)-chi(:,kk_ind,kk)*b(:,kk)));
                r2(kk) = var(chi(:,kk_ind,kk)*b(:,kk))/var(gamma(:,kk));
            end
        else
            gamma = V-chi*theta;
            b = (chi\gamma);
        end
    case 'Add coefficients'
        %% Subtract theta from the fitted V
        if model.pt_uh
            poo  % not coded for patient UH
        end
        b = -theta;
        b(vf) = b(vf) + out.V_coeff;
        gamma = chi*b;
end
if ~model.pt_uh
    se = sqrt(diag(inv(chi'*chi))*std(gamma-chi*b));
    r2 = var(chi*b)/var(gamma);
end
mu_dm = repmat([model.fixed.mu_dm model.flex.mu_dm]',1,model.K_types);
sigma_dm = repmat([model.fixed.sigma_dm model.flex.sigma_dm]',1,model.K_types);
fid = fopen(options.results_savename(model,'Gamma','csv'), 'w');
write_header(['_' model.model_name],fid,model.K_types);
write_table([model.fixed.colheaders model.flex.colheaders],b,se,mu_dm,sigma_dm,r2,fid,model.K_types);
fclose(fid);
out.gamma_coeff=b;
out.coefficient_names = coefficient_names;
out.theta = theta;



function write_header(w,fid,K)
    %% Headers
    fprintf(fid, 'X,Offset%s',w);
    for k = 1:K
        fprintf(fid, ',Coefficient%s, SE%s,%s',w,w,w);
    end
    fprintf(fid, '\n');


function write_table(coefficient_names,b,se,mu_dm,sigma_dm,r2,fid,K)
    newline = true;
    for coeff = 1:size(b,1)
        if newline, fprintf(fid, '%s,%4.2f', coefficient_names{coeff},mu_dm(coeff,1)); end
        for kk = 1:K
            fprintf(fid, ', %4.2f, [%4.2f],', b(coeff,kk)./sigma_dm(coeff,kk), se(coeff,kk)./sigma_dm(coeff,kk));
            t = abs(b(coeff,kk))/se(coeff,kk);
            if t>1.644, fprintf(fid, '*');  % 0.1 significance
                if t>1.96, fprintf(fid, '*'); % 0.05 significance
                    if 	t>2.575, fprintf(fid, '*'); % 0.01 significance
                    end
                end
            end
        end
        newline = true;
        if newline, fprintf(fid, '\n'); end
    end
    % %3.4f
    fprintf(fid,'R^2');
    for k=1:K
        fprintf(fid,',,%3.4f,',r2(k));
    end

