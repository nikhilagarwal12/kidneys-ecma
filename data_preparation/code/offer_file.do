// Generates offers primary file
// Does NOT run on its own; included in gen_primary_files.do

  use "`primary_files'ptr_primary.dta", clear
 
  // Create an 'organ_sequence_num' variable to uniquely order offers
  sort donor_id match_id_code ptr_row_order pb_offer_seq
  by donor_id: gen organ_sequence_num = _n
  
  // sample offers
  merge 1:1 donor_id match_id_code ptr_row_order pb_offer_seq using "`primary_files'sample_offers"
  keep if _merge == 3
  drop _merge
  
  // donor info
  merge m:1 donor_id using "`primary_files'deceased_donors.dta", keepusing(ecd_donor age_don non_hrt_don cod_cad_don gender_don creat_don donor_cat da1 da2 db1 db2 kdri kdri_cat kil_disposition kir_disposition kib_disposition)
  keep if _merge == 3
  drop _merge

  // Patient info
  merge m:1 wl_id_code using "`primary_files'patients.dta", keepusing(init_age init_date activate_date dialysis_date first_tx_date diab listing_ctr listing_ctr_id listing_ctr_dsa_id listing_ctr_region n_listings prior_living_donor incomp_live_donor_date awaits_PA a1 a2 b1 b2 wt_qual_date date_turn_18 pt_code)

  keep if _merge == 3 | payback==1  // otherwise payback offers will be dropped b/c they don't match to a patient record
  drop _merge

  gen total_pre_wait_time = 0
  gen first_active_post = wt_qual_date

  compress

  // current PRA/CPRA: merge with patient history file on wlreg_audit_id_code
  sort wlreg_audit_id_code

  merge m:1 wlreg_audit_id_code using "`primary_files'patient_history.dta", keepusing(cpra current_pra peak_pra use_which_pra unos_cand_stat_cd)
  keep if _merge != 2 | payback==1

  // Wait time start date: for now, default is wt_qual_date from KIDPAN
  gen wait_time_date = min(wt_qual_date,match_submit_dt)

  // rename variables
  rename match_submit_dt match_date
  rename abo abo_don
  rename abo_cand abo
  rename prime_opo_refusal_id refusal_id
  rename second_opo_refusal_id second_refusal_id
  rename classification descrip
  rename prim_opo_refusal_ostxt refusal_ostxt
  rename ptr_tot_score kidney_points
  drop wl_org

  // replace wlreg_audit_id_code for offers where there's no matching wlhistory record
  local vars_to_replace = "wlreg_audit_id_code cpra unos_cand_stat_cd peak_pra current_pra use_which_pra"
  include ../../tools/code/fix_wlreg_audit_ids.do

  // get donor and match opo dsa ids
  donor_and_match_opo_dsa_ids

  // merge in points broken down by source
  merge m:1 match_id_code ptr_sequence_num using "`primary_files'points_wide.dta"
  drop if _merge == 2
  drop _merge

  // code missing values as zero points
  foreach code in 1 2 3 4 5 7 8 13 16 23 34 35{
    cap replace ptr_sub_score_points_`code' = 0 if ptr_sub_score_points_`code' == .
  }
  
  // classify match runs as "full", "within", or "payback" (payback offers are not to individual patients)
  // Also impute local center for sub-unit variances, and create a 'match_run_num' variable
  classify_match_runs

  // Compute CREG mismatches (Tennessee variance)
  creg_match_stata 1 "`auxiliary_files'"

  // for CADN: impute "in master file", also update patient history file
  master_file_cadn "`primary_files'"

  // generate bins
  generate_rank_category_vars  // an .ado file generates the actual categories 
  rank_categories_preKAS  // generates the "pre-KAS" rank categories, which apply to non-variance DSA's between 2009-10-01 and 2014-12-03

  rename match_opo_dsa_id run_opo_id
  rename donor_opo_dsa_id opo_dsa_id
  rename match_opo_region_id run_opo_region_id
  rename donor_opo_region_id opo_region_id
  
  // Calculate EPTS score
  compute_epts

  // Indicate DSAs with variances
  mark_dsa_variances run_opo_id dsa_has_variance

  compress

  // fix donor hospital, useful for variances
  local genfile = "genfile"  // if =="genfile", generates the hospital file
  fix_run_loc_ctr_hospital "`genfile'" "`primary_files'"

  // modify bins and points in variance DSAs
  variances_preKAS,binsonly

  // Alternative version of bins based on the `descrip' field
  descrip2bin
  
  // Mark when pre-KAS rules applied. Bins only relevant if pre-KAS==1
  gen pre_KAS = match_date <= mdy(12,3,2014)
  

  // Create c_accept: a new definition of offer_accept.
  gen_c_accept

  // drop offers in non-standard match runs
  cap drop _merge
  include ../../tools/code/flag_bypass.do


  // Create a real_offer variable, indicating whether there was still an organ available and c_accept was "Yes" or "No"
  // Only store "real" offers; ignore other offers that would not have led to a transplant if accepted
  include ../../tools/code/define_offer_sample.do
  keep if real_offer == 1

  drop a1 a2 b1 b2 da1 da2 db1 db2 Geo_detail abo_t abo_don_t remaining ro_*

  // Change some variable names
  rename ABDR zero_abdr
  rename ABDR_t zero_abdr_t
  rename ABO abo_match
  rename ABO_t abo_match_t
  rename Geo geo
  rename Geo_t geo_t
  rename Ped_List ped_list
  rename Ped_List_t ped_list_t
  rename Ped_Alloc ped_alloc
  rename Ped_Alloc_t ped_alloc_t
  rename CPRA cpra_cat
  rename CPRA_t cpra_cat_t
  rename Bin bin
  rename Bin_t bin_t
  rename EPTS_t epts_t

  create_offer_cat  // broad offer classification

  // determine who actually received a transplant, among those offered
  merge m:1 donor_id wl_id_code using `primary_files'kidpan, keep(master match) keepusing(wl_org)
  gen transplanted = _merge==3 & wl_org == "KI"
  drop wl_org _merge

  compress

  if `full_sample'==1{
      save "`primary_files'offers.dta", replace
  }

  if `test_sample'==1{
    merge 1:1 donor_id match_id_code ptr_row_order pb_offer_seq using `test_sample_files'/sample_offers, keep(match) nogen
    save `test_sample_files'offers, replace
  }
