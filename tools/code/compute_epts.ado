// Calculates EPTS (Expected Post-Transplant Survival) from KIDPAN data

program compute_epts

gen prior_organ_transplant = match_date > first_tx_date 

gen age_guess = ((init_age + 0.5)*365.25 + match_date - init_date) / 365.25  // this is a "guess", taking the average
gen age_from_25 = max(age_guess - 25, 0)

gen years_on_dialysis = 0
replace years_on_dialysis = (match_date - dialysis_date) / 365.25 if dialysis_date != . & match_date > dialysis_date

gen diabetes = diab > 1 & diab < 998  // once a diabetic, always a diabetic — for now

gen epts_raw = 0.047*age_from_25 - 0.015*diabetes*age_from_25 + 0.398*prior_organ_transplant - 0.237*diabetes*prior_organ_transplant + 0.315*log(years_on_dialysis + 1) - 0.099*diabetes*log(years_on_dialysis + 1) + 0.130*(years_on_dialysis==0) - 0.348*diabetes*(years_on_dialysis==0) + 1.262*diabetes

gen epts_bottom_20 = epts_raw <= 1.48933264887064

end
