function score = score_opt_fun(data_object,N_sims,bins,fitted_value)
        
        postEntry = repmat(cumsum(data_object.pt_entry_weights,2),1,1,size(data_object.Gamma_eta,3))>0;
        Prcompat  = bsxfun(@times,data_object.pr_compat,ones(size(data_object.Gamma_eta)));
        ok        = postEntry.*(Prcompat>0);
    
        % Get some basic variables
        [N_patients,N_time,N_donors] = size(data_object.Gamma_eta);
        N_years = size(data_object.opt_assignment.offset_gamma,2);

        %% Generate the basic model structure
        model = lp_model(N_patients*N_years,N_donors); 

        % Gamma_eta
        Gamma_eta_mat = zeros(N_patients,N_years,N_donors);
        X = data_object.Gamma_eta + log(ok);
        for ii = 1:N_patients
            Gamma_eta_mat(ii,:,:) = X(ii,data_object.opt_assignment.offset_gamma(ii,:),:);
        end
        %% Solve the LP model B times and get the average assignment probabilities
        P = zeros(N_patients*N_years,N_donors);
        for bb = 1:N_sims
            if mod(bb,10) == 1
                display(['Solving LP iteration: ' num2str(bb)]);
            end
            rng(213215+bb);
            x = solve_lp_model(model,Gamma_eta_mat,1./data_object.V_rep_offer);
            P = P + x/N_sims;
        end

        % Store the output
        output.P = P;

        % Recover chi
        K = size(data_object.opt_assignment.chi,4);
        chi = reshape(data_object.opt_assignment.chi,N_patients*N_years*N_donors,K);
        beta = chi\P(:);

        % Compute P_hat and score
        P_hat = chi*beta;

        % Compute scores -- get average score for each donor and
        % extrapolate
        P_hat = mean(reshape(P_hat,N_patients,N_years,N_donors),2);
        P     = mean(reshape(P,N_patients,N_years,N_donors),2);
        if fitted_value
            score = score_xtile(P_hat,bins);
        else
            score = score_xtile(P,bins);
        end
        score = repmat(score,1,N_time,1);

end

function x = solve_lp_model(model,Gamma_eta_mat,w)
    
    % Set up objective function

    obj = bsxfun(@times,Gamma_eta_mat + randn(size(Gamma_eta_mat)),w); % add \varepsilon_{ijt} and multiply by w
    model.obj = -obj(:); % Maximize

    % Solve the model
    params.outputflag = 0;
    result = gurobi(model,params);

    % Check results
    assert(isequal(result.status,'OPTIMAL'));
    x=reshape(result.x,size(Gamma_eta_mat,1)*size(Gamma_eta_mat,2),size(Gamma_eta_mat,3));
end


function model = lp_model(N,J)    
    % Sum of assignments to N     
    N_feas=repmat(speye(N),1,J);

    % Sum of assignments to J
    index=zeros(N*J,2);
    index(:,1)=reshape(repmat(1:J,N,1),N*J,1);
    index(:,2)=repmat((1:N)',J,1)+(index(:,1)-1)*N;    
    J_feas=sparse(index(:,1),index(:,2),ones(N*J,1),J,N*J,N*J);

    % Create the model structure
    % Feasibility constraints
    %    model.rhs = ones(N+J,1);
    model.rhs = [1/N*ones(N,1);1/J*ones(J,1)];
    model.A = [N_feas;J_feas];
    model.sense = '<';

    % Upper and lower bounds
    model.lb = zeros(N*J,1);
    model.ub = ones(N*J,1);
end
