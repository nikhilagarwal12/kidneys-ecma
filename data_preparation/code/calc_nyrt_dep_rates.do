// Structural Model requires a rate of living donor arrival
// Use data on patients on the list, and living donor transplants to compute this
// DO NOT run on own; it should be called within gen_primary_files.do

// Need to install survci
* type findit survci in xstata and follow instructions


use "`file_dir'kidpan.dta", clear

**********************************************************
* Generate basic variables for survival analysis
**********************************************************
* Calculate the total number of days on the waiting list
gen days_on_list = end_date - init_date
replace days_on_list = 1 if days_on_list <= 0

* Generate an entry variable for the survival model
gen entry_day = max(date("01/01/2010","MDY") - init_date,0)

count if entry_day>=days_on_list
assert `r(N)'<100
replace entry_day = 0 if entry_day>=days_on_list // Small number of pre-init-date end_dates

* Generate variable with departure reason
gen living_donors = don_ty == "L"
gen non_deceased_dep = rem_cd~=. & rem_cd~=4
gen non_tx_dep = rem_cd~=. & rem_cd~=4 & rem_cd~=15

*** Generate variables used in the structural model ***
gen pt_diabetes = diab>=2 & diab<=5
label variable pt_diabetes "Diabetic Patient"

gen abo_A = abo == "A" | abo == "A1"
gen abo_O = abo == "O"
label variable abo_A "Bloodtype A Patient"
label variable abo_O "Bloodtype O Patient"

local cpra_var = "end_cpra"

replace `cpra_var' = 0 if `cpra_var' == .
gen cpra_0 = `cpra_var' == 0
gen cpra_gt80 = `cpra_var' > 80
gen cpra_g_80 = (`cpra_var' - 80)*(`cpra_var' >= 80)
gen cpra_g_20 = (`cpra_var' - 20)*(`cpra_var' >= 20)
label variable cpra_0 "CPRA = 0"
label variable `cpra_var' "Calculated Panel Reactive Antibodies (CPRA)"
label variable cpra_gt80 "CPRA >= 80"
label variable cpra_g_80 "CPRA - 80 if CPRA>=80"
label variable cpra_g_20 "CPRA - 20 if CPRA>=20"

local cpra_old = "cpra_0 cpra_gt80"
local cpra_vars = "`cpra_var' cpra_0 cpra_gt80"
local cpra_sparse = "`cpra_var' cpra_0 cpra_g_80"

gen init_age_g_18 = (init_age-18)*(init_age>=18)
gen init_age_g_35 = (init_age-35)*(init_age>=35)
gen init_age_g_50 = (init_age-50)*(init_age>=50)
gen init_age_g_65 = (init_age-65)*(init_age>=65)
label variable init_age "Age (at Registration)"
label variable init_age_g_18 "Age - 18 if Age>=18"
label variable init_age_g_35 "Age - 35 if Age>=35"
label variable init_age_g_50 "Age - 50 if Age>=50"
label variable init_age_g_65 "Age - 65 if Age>=65"

local age_vars = "init_age init_age_g_18 init_age_g_35 init_age_g_50 init_age_g_65"
local age_sparse = "`age_vars'"

gen prior_transplant = num_prev_tx>0 & ~mi(num_prev_tx)
label variable prior_transplant "Prior Transplant"

gen end_bmi_calc_dum        = mi(end_bmi_calc)
replace end_bmi_calc        = 0 if mi(end_bmi_calc)
gen end_bmi_calc_g_18_5     = end_bmi_calc>=18.5
replace end_bmi_calc_g_18_5 = 0 if mi(end_bmi_calc)
gen end_bmi_calc_g_25       = end_bmi_calc>=25
replace end_bmi_calc_g_25   = 0 if mi(end_bmi_calc)
gen end_bmi_calc_g_30       = end_bmi_calc>=30
replace end_bmi_calc_g_30   = 0 if mi(end_bmi_calc)
label variable end_bmi_calc_dum "Missing BMI"
label variable end_bmi_calc "Body Mass Index (BMI)"
label variable end_bmi_calc_g_18_5 "BMI >= 18.5"
label variable end_bmi_calc_g_25 "BMI >= 25"
label variable end_bmi_calc_g_30 "BMI >= 30"

local bmi_vars = "end_bmi_calc end_bmi_calc_dum end_bmi_calc_g_18_5 end_bmi_calc_g_25 end_bmi_calc_g_30"
local bmi_sparse = "`bmi_vars'"

gen tot_serum_album_dum        = mi(tot_serum_album)
replace tot_serum_album        = 0 if mi(tot_serum_album)
gen tot_serum_album_g_3_7      = tot_serum_album>=3.7
replace tot_serum_album_g_3_7  = 0 if mi(tot_serum_album)
gen tot_serum_album_g_4_4      = tot_serum_album>=4.4
replace tot_serum_album_g_4_4  = 0 if mi(tot_serum_album)
label variable tot_serum_album_dum "Missing Total Serum Albumin"
label variable tot_serum_album "Total Serum Albumin"
label variable tot_serum_album_g_3_7 "Total Serum Albumin >= 3.7"
label variable tot_serum_album_g_4_4 "Total Serum Albumin >= 4.4"

local albu_vars = "tot_serum_album tot_serum_album_dum tot_serum_album_g_3_7 tot_serum_album_g_4_4"

gen on_dialysis_at_init_date = init_date >= dialysis_date
gen log_dialysis_time_at_init = log(min(max((init_date-dialysis_date)/365,1/365),5000))
replace log_dialysis_time_at_init = log(1/365) if on_dialysis_at_init_date == 0
gen log_dialysis_time_at_init_gt5 = (log_dialysis_time_at_init - log(5)) * (log_dialysis_time_at_init > log(5))
label variable on_dialysis_at_init_date "On Dialysis at Registration"
label variable log_dialysis_time_at_init "Log Years on Dialysis at Registration"
label variable log_dialysis_time_at_init_gt5 "Log Years on Dialysis at Registration * Over 5 Years"

local dial_old = "on_dialysis_at_init_date log_dialysis_time_at_init"
local dial_vars = "`dial_old' log_dialysis_time_at_init_gt5"

// specify covariates
local trim_spec = "pt_diabetes abo_A abo_O `cpra_sparse' `age_sparse' prior_transplant `bmi_sparse' `albu_vars' `dial_vars'"
local trim_curve = "pt_diabetes=0.00 abo_A=0.00 abo_O=0.00 init_age=51.46 init_age_g_18=33.59 init_age_g_35=17.72 init_age_g_50=6.52 init_age_g_65=0.84 tot_serum_album_g_3_7=0.00 tot_serum_album_g_4_4=0.00 on_dialysis_at_init_date=0.00 log_dialysis_time_at_init=0.00"

************************************************
* Hazard Analysis
************************************************
stset days_on_list non_deceased_dep, entry(entry_day)

* Model with all characteristics in the structural model
* Generate the variables used in the Structural  model
* Estimate Trim Gompertz
eststo, title("Gompertz Trim"): streg `trim_spec', d(gompertz)
estimates store trim_gompertz

* Estimate Trim Weibull
eststo, title("Weibull"): streg `trim_spec', d(weibull)
estimates store trim_weibull

* Estimate Trim Cox
eststo, title("Cox"): stcox `trim_spec'

* Predict Trim Gompertz 
estimates restore trim_gompertz
predict trim_gompertz_dep_rate, hazard
replace trim_gompertz_dep_rate = trim_gompertz_dep_rate/exp(`e(gamma)'*days_on_list)
* Output
!echo `e(gamma)' > `hazard_output'/trim_gompertz.csv

* Predict Trim Weibull
estimates restore trim_weibull
predict trim_weibull_dep_rate, hazard
replace trim_weibull_dep_rate = (trim_weibull_dep_rate/(`e(aux_p)'*days_on_list^(`e(aux_p)'-1)))^(1/`e(aux_p)')
* Output
!echo `e(aux_p)' > `hazard_output'/trim_weibull.csv

* Output the hazards 
ssc install savesome
savesome wl_id_code *dep_rate using "`hazard_output'/hazard_rates", replace 
outsheet wl_id_code *dep_rate using "`hazard_output'/hazard_rates.csv", replace comma


* -------------------------------------
* Table A.2: Survival Model Estimates
* Output estimates to excel
* -------------------------------------
esttab using "`hazard_output'/hazard_regs.csv", label se nostar mtitles("Gompertz" "Weibull" "Cox") replace
preserve
import delimited using "`hazard_output'/hazard_regs.csv", delimiter(",") clear
replace v1 = subinstr(v1,"_","",.)
replace v1 = subinstr(v1,`"""',"",.)
replace v1 = subinstr(v1,"=","",.)
replace v2 = subinstr(v2,`"""',"",.)
replace v2 = subinstr(v2,"=","",.)
replace v3 = subinstr(v3,`"""',"",.)
replace v3 = subinstr(v3,"=","",.)
replace v4 = subinstr(v4,`"""',"",.)
replace v4 = subinstr(v4,"=","",.)
export excel using "`hazard_output'/hazard_regs.xlsx", sheet("hazards, raw") sheetreplace
cap shell rm "`hazard_output'/hazard_regs.csv"
restore

//*** Average harzard based only on totals
* Compute Totals
gen years_on_list = floor(days_on_list/365)
collapse (sum) days_on_list living_donors non_deceased_dep non_tx_dep, by(years_on_list)

* Compute Rates
gen rate = living_donors/days_on_list
gen non_deceased_rate = non_deceased_dep/days_on_list
gen non_tx_rate = non_tx_dep/days_on_list

gen discount_factor = exp(-rate*365)
gen non_deceased_discount_factor = exp(-non_deceased_rate*365)
gen non_tx_discount_factor = exp(-non_tx_rate*365)

* List
list
 
**********************************************************
* Append the calculated hazard rates to patient.dta
**********************************************************
use "`file_dir'patients.dta", clear
capture drop *dep_rate
merge 1:1 wl_id_code using "`hazard_output'/hazard_rates", assert(match) nogenerate
save "`file_dir'patients.dta", replace
