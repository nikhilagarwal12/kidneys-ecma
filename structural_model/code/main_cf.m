
% Run the counterfactuals code

% Load basic data 
if ~cf_tasks.optimize && (cf_tasks.gen_data || cf_tasks.compute_ss || cf_tasks.counterfactual_table)
    % Load the model
    load([model_data_folder model_name '/master'],'model','data');
    % Define the run_name to run subspecs (rho robustness and debug)
    if ~isempty(options.override_rho), model.rho = options.override_rho; end
    % Model name and definition of the data_filepath that will be saved or loaded.
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end
    if ~exist('manual','var'), manual = struct(); end
    warning off MATLAB:structOnObject
end

% Generate data file
if ~cf_tasks.optimize && ~options.resume_cfs && cf_tasks.gen_data
    load([options.output_stub model_name '/chain_base--summary.mat'],'chain_summary'); 
    % Load model files
    load(model.filenames.donors,'donors');
    load(model.filenames.patients,'patients');
    load(model.filenames.pt_history,'pt_history');
    % Data about donor and patient pairs
    selective_load = {'pt_hist_id','donor_id','blood_type',...
                        'a_mm','b_mm','dr_mm','geo',...
                        'pediatric','unacc','c_abo',...
                        'points','points10','points12','bins','bins10','bins12',...
                        'days1','days2','days3',...
                        'kas_points','kas_points10','kas_points12','kas_bins','kas_bins10','kas_bins12'};
    spload = @(x) sparsedata(load(x,selective_load{:}),'pt_hist_id','donor_id','blood_type');
    abo_list = {'O','A','B','AB'};
    for abo = 1:4
        don_pat_pairs{abo}=spload([model.filenames.don_pat_pairs abo_list{abo}]);
    end
    % Pare down to the sample of patients, donors and pt_histories
    % in the estimation dataset
    donors = donors(ismember(donors.donor_id,data.donor_id),:);
    pt_history = pt_history(ismember(pt_history.wlreg_audit_id_code,data.pt_history_id),:);
    
    % Load hazard parameters: 
    if isfield(model,'q_leave') && ischar(model.q_leave)
        model.hazard.param = load([fileparts(model.filenames.donors) '/hazard']);
        model.hazard.param = model.hazard.param.(model.q_leave);
        model.hazard.distribution = strsplit(model.q_leave,'_');
        model.hazard.distribution = model.hazard.distribution{2};
    end
    
    % Load eta_draws
    if model.donor_uh
        load([options.output_stub model_name '/redraw_eta--summary.mat'],'eta_indiv_draws_trim','chain_spec');        
        % Create a structure with the eta draws, and the indices of the donors
        eta_draws_struct.eta_draws = eta_indiv_draws_trim(:,end); % Code can only work with one draw right now
        eta_draws_struct.draws = eta_indiv_draws_trim; % Code can only work with one draw right now
        eta_draws_struct.donor_id = data.donor_list;
    else
        eta_draws_struct = struct();
    end   
    
    % Load beta_draws into separate object
    if model.pt_uh
        load([options.output_stub model_name '/redraw_eta--summary.mat'],'beta_i_draws_trim','chain_spec');  
        beta_draws_struct.beta_draws = beta_i_draws_trim(:,end);
        beta_draws_struct.draws = beta_i_draws_trim;
        beta_draws_struct.wl_id_code = data.patient_list;
    else
        beta_draws_struct.beta_draws = [];
        beta_draws_struct.draws = [];
        beta_draws_struct.wl_id_code = [];
    end

    % Load Projection Estimates
    projfile = [options.output_stub model_name '/projection_table_' model.q_leave '.mat'];
    projest = load(projfile);

        % for pt_uh spec, reassign projest.theta to equal chain.mu_theta
	if model.pt_uh
	    projest.theta = chain_summary.mu_theta;
        end

    % Construct the dataset and save it.
    display(['Generating the Data File For: ' cf_options.sample_name cf_options.run_name]);
    eval(cf_options.sample_name);
    data_object = gen_sample_cfs(model,don_pat_pairs,pt_history,donors,patients,projest,eta_draws_struct,beta_draws_struct,cf_sample); 
    save([data_filepath],'data_object','-v7.3');

    % Save offers and eta_draws_struct for future use in creating the trees
    offers = data_object(1).compatible_offerset(don_pat_pairs,pt_history,patients,donors);
    if model.donor_uh 
        [istrue,locb] = ismember(offers.donors.donor_id,eta_draws_struct.donor_id);
        assert(all(istrue));
        offers.donors.eta = eta_draws_struct.eta_draws(locb);
    end
    save([data_filepath '_offers'],'offers');
    fprintf('Saved %s_offers\n',data_filepath)
end

%   Load data_object
if ~cf_tasks.gen_data && ~cf_tasks.optimize && (cf_tasks.counterfactual_table || cf_tasks.compute_ss),
    data_object=load(data_filepath);
    if isfield(data_object,'data_object'), data_object = data_object.data_object; end  % To allow legacy saves
end

% Generate mech file and initial condition
for kk = 1:numel(cf_mech_list)
    if ~cf_tasks.compute_ss, continue; end
    cf_mech_alias = cf_mech_list{kk}
    mech_aliases;
    if isfield(cf_mech,'score') && isempty(cf_mech.score),  fprintf('Score is empty... continuing\n'); continue; end
    % Define filenames depending on whether we are running optim or score
    alias_filepath = [rslt_filepath '_' cf_mech_alias];  % Add cf_mech_alias to the results path (proj/results)
    mech_filename = [data_filepath '_' cf_mech_alias];   % mech should be stored within roth because of sensitive info.
    soln_filename = [alias_filepath '_soln'];            % soln will be in proj so I can use alternative server with knitro
    if strcmp(cf_mech.type,'optim'), 
        prob_filename = [alias_filepath '_prob'];        % save prob file so that other server can read it
    else 
        prob_filename= soln_filename;                    % 'score' does not need two different files
    end
    % Run steady state allocation in server with knitro
    if  cf_tasks.optimize
        % Exit if not 'optim' or if the solution file exist and should not be overwritten
        if ~strcmp(cf_mech.type,'optim'), continue; end
        if exist([soln_filename '.mat'], 'file') == 2 && ~options.overwrite_soln, continue; end
        pp=load(prob_filename);  % Load problem file
        if pp.first_best(1),   % Define options file
            opt_file = 'code/cf_optimal/knitroOptionsb1f0.opt';
            secb     = load([strrep(alias_filepath,'optim_first','optim_second') '_soln']);
            pp.alloc = secb.alloc;
            pp.params = pp.opt_prob.export2solver;
            pp.params.first_best(1) = 1;
            mli  = bsxfun(@plus,pp.params.mu,-pp.alloc.V);         % Mean of the Latent Index
            z    = bsxfun(@times,-mli,1./pp.params.sigma);       % Cutoff for standard normal
            if numel(pp.first_best)==1 || ~pp.first_best(2)
                pp.alloc.q  = pp.alloc.q.*normcdf(-z);
                pp.alloc.q  = pp.alloc.q*0.99 + 1e-3; % Get away from boundaries
                pp.alloc.V = update_values(pp.alloc.q,[],pp.params);
                pp.alloc.p = update_composition(pp.alloc.q,pp.alloc.V,pp.params);
            end
        else
            opt_file = 'code/cf_optimal/knitroOptionsb0f0.opt';
            %fcfs     = load([strrep(alias_filepath,'optim_second','score_fifo') '_soln']);
            %pp.alloc = fcfs.state;  % feed fcfs solution as initial value;
            bnpt     = load([strrep(strrep(alias_filepath,'optim_secondC','optim_second'),'optim_second','score_binspoints_locl_ref') '_soln']);
            pp.alloc = bnpt.state;  % feed binspoints solution as initial value;
        end
        if numel(options.profile)>0, profile on; end
        if isfield(cf_tasks,'export2anil') && cf_tasks.export2anil
            optimal_ss_allocation_export(pp.opt_prob.export2solver,pp.first_best,opt_file,pp.alloc,alias_filepath);
            continue
        end
        alloc = optimal_ss_allocation(pp.opt_prob.export2solver,pp.first_best,opt_file,pp.alloc);
        if numel(options.profile)>0, 
            profile off
            profsave(profile('info'),[options.profile '_' cf_mech_alias])
        end
        save(soln_filename,'alloc');
        continue
    end

    % Run steady state allocation
    reasons_to_continue = [];
    for ii = options.run_splits
        if ii>1, split_string = num2str(ii); else split_string =''; end % First object is always the full data
        if exist([soln_filename split_string '.mat'], 'file') ~= 2 || options.overwrite_soln || strcmp(cf_mech.type,'optim')
            reasons_to_continue = [reasons_to_continue ii];
        end
    end
    if isempty(reasons_to_continue), continue; end
    % Load data
    if ~isfield(options,'run_splits') || ~any(options.run_splits), options.run_splits=1:numel(data_object); end
    
    for ii = reasons_to_continue
        if strcmp(cf_mech.type,'optim') && ii>1, continue; end % optim does not work with splits.
        % split string: to save and load files
        if ii>1, split_string = num2str(ii); else split_string =''; end % First object is always the full data
        
        if ~options.resume_cfs
            % Generate the mechanism file;
            display(['Generating the CF File For: ' [cf_mech_alias split_string]]);
            manual = defaultvalue(manual,'gen_mech',struct());
            [cf_mech,init_cond] =  gen_mech(cf_mech,data_object(ii),model,manual.gen_mech); 
            % Make sure never to append at this stage -- will break some code
            save([mech_filename split_string],'-struct','cf_mech','-v7.3'); 
            save([prob_filename split_string],'-struct','init_cond','-v7.3');
        end
        
        % Running the Counterfactuals: obtain steady state
        object       = struct();
        object.data  = data_object(ii);   % cf_data
        object.mech  = load([mech_filename split_string]);   % Mechanism
        if strcmp(cf_mech.type,'score'),
            object.soln  = load([soln_filename split_string]);   % solution
            manual       = defaultvalue(manual,'compute_ss',struct());
            object.manu  = manual.compute_ss; 
        end
        cf_init_time = tic; 
        if numel(options.profile)>0, profile on; end
        output = object.mech.compute_ss(object,[soln_filename split_string '.mat']);
        % Should I profile the code?
        if numel(options.profile)>0, 
            profile off
            profsave(profile('info'),[options.profile '_' cf_mech_alias])
        end
        if isempty(output), continue; end
        display(['Total Time For Solving Cf (s) = ' num2str(toc(cf_init_time))]);
        save(strrep([soln_filename split_string],'_soln','_results'),'output','model','-v7.3');
        
        % Update V_ref
        if cf_mech.update_V_ref && ii==1
            prob = optimal_ss_problem(data_object(1)); 
            V_ref.pt_history = data_object(1).pt_hist_id;
            V_ref.V          = sum(output.V.*(prob.arrivals>0),2);
            V_ref.flow       = sum(output.V.*(output.pt_weights.*output.pool_size + prob.arrivals/model.rho),2);
            % If V_ref changes we can use this code to update the data structure instead of creating a new one
            if any(isnan(V_ref.V)), 
                fprintf('There were %d NaNs in V_ref.V.\n',sum(isnan(V_ref.V)));
            end
    
            for jj = 1:numel(data_object)
                data_object(jj) = update_V_ref(data_object(jj),V_ref,output.V_ref_dec);
            end
            save([data_filepath],'data_object','-v7.3');
            load([data_filepath '_offers'],'offers');
            [ismem,where]     = ismember(offers.pt_history.wlreg_audit_id_code,V_ref.pt_history);
            V_ref_flow        = zeros(size(offers.pt_history.wlreg_audit_id_code));
            V_ref_flow(ismem) = V_ref.flow(where(ismem));
            offers.pt_history.V_ref_flow = V_ref_flow;
            save([data_filepath '_offers'],'offers');
        end
        
    end
end

% Collect the results and create the counterfactual tables
if ~cf_tasks.optimize && cf_tasks.counterfactual_table
    cf_table_options;  % Creates cf_table_opt
    if ~isfield(options,'run_splits') || ~any(options.run_splits), options.run_splits=1:numel(data); end
    options.run_splits = unique([1 options.run_splits]); 

    % Collect the output from the various mechanisms
    [cf_output,cf_output_data] =  collect_cf_output(data_object,data_filepath,rslt_filepath,cf_mech_list_table,options);
    
    % Generate an overall table
    [table_ds,table_ds_splits,pt_table_ds]  = cf_table(cf_output,cf_output_data,cf_table_opt,options,model);

    % Tables -- Transplants
    table_ds_out    = struct2table(table_ds);
    for jj = 1:size(cf_table_opt.pt_splits,2)
        table_ds_out = cat(1,table_ds_out,struct2table(table_ds_splits(jj,:)));
    end 

    rslt_filepath = [options.results_folder model_name '/' cf_options.sample_name cf_options.run_name] 

    if isfield(cf_options,'add2tablename')
        rslt_filepath = [rslt_filepath cf_options.add2tablename]
    end

    writetable(table_ds_out,[rslt_filepath '.csv']);
    % collect all the robustness checks into one excel file
    if strcmp(cf_options.run_name,'_rho_90')
        writetable(table_ds_out,[strrep(rslt_filepath,cf_options.run_name,'') '_mech_compare_formatted_robustness.xls'],'Sheet',['raw_summary' cf_options.run_name]);
    elseif  strcmp(cf_options.sample_name,'sample0big') 
        writetable(table_ds_out,[strrep(rslt_filepath,cf_options.sample_name,'sample0') '_mech_compare_formatted_robustness.xls'],'Sheet',['raw_summary_' cf_options.sample_name]);
    elseif strcmp(cf_options.add2tablename,'_notimeout') 
        writetable(table_ds_out,[strrep(rslt_filepath,cf_options.add2tablename,'') '_mech_compare_formatted_robustness.xls'],'Sheet',['raw_summary' cf_options.add2tablename]);
    elseif strcmp(model_name,'nyrt_fix_time_pt_don_uh')
        writetable(table_ds_out,[rslt_filepath '_mech_compare_formatted.xls'],'Sheet','raw_summary');
        writetable(table_ds_out,[strrep(rslt_filepath,model_name,'nyrt_fix_time_uh') '_mech_compare_formatted_robustness.xls'],'Sheet','raw_summary_pt_don_uh');
    else
        writetable(table_ds_out,[rslt_filepath '_mech_compare_formatted.xls'],'Sheet','raw_summary');
    end
    % Write the patients as well
    if ~isequal(pt_table_ds,struct())
        pt_table_ds_out = struct2table(pt_table_ds);
        writetable(pt_table_ds_out,[rslt_filepath '_pt.csv']);
        if strcmp(cf_options.run_name,'_rho_90')
            writetable(table_ds_out,[strrep(rslt_filepath,cf_options.run_name,'') '_mech_compare_formatted_robustness.xls'],'Sheet',['raw_patient' cf_options.run_name]);
        elseif strcmp(cf_options.sample_name,'sample0big') 
            writetable(table_ds_out,[strrep(rslt_filepath,cf_options.sample_name,'sample0') '_mech_compare_formatted_robustness.xls'],'Sheet',['raw_patient_' cf_options.sample_name]);
        elseif strcmp(cf_options.add2tablename,'_notimeout') 
            writetable(table_ds_out,[strrep(rslt_filepath,cf_options.add2tablename,'') '_mech_compare_formatted_robustness.xls'],'Sheet',['raw_patient' cf_options.add2tablename]);
        elseif strcmp(model_name,'nyrt_fix_time_pt_don_uh')
            writetable(pt_table_ds_out,[rslt_filepath '_mech_compare_formatted.xls'],'Sheet','raw_patient');
            writetable(table_ds_out,[strrep(rslt_filepath,model_name,'nyrt_fix_time_uh') '_mech_compare_formatted_robustness.xls'],'Sheet','raw_patient_pt_don_uh');
        else
            writetable(pt_table_ds_out,[rslt_filepath '_mech_compare_formatted.xls'],'Sheet','raw_patient');
        end
    end

    subgroup_analysis  % patterns in counterfactual equilibrium allocations (by patient and donor subgroup)
end

