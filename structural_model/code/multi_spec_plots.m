function []=multi_spec_plots(options,specs)

leg_position = 1;
X  = 8;   % Number of years;
for spec = specs
    spec = spec{1};
    load([options.output_stub spec '/chain_base--summary.mat']);

    firstone = leg_position==1;
    leg_position = leg_position + 1;
    % Generate the plot by position
    figure(1)
    if firstone, cla; plot(1:size(ccp_fits.model,1),ccp_fits.data,'k--'); hold on; leg1 = {'Data'}; end
    plot(1:size(ccp_fits.model,1),ccp_fits.model,marker(spec),'MarkerEdgeColor',tint(spec),'Color',tint(spec),'MarkerFaceColor','none'); 
    leg1{leg_position} = beautify(spec);
    
    % Generate the plot by waittime
    figure(2)
    for m = 1:numel(ccp_pat_fit.data), 
        if strcmp(ccp_pat_fit.data{m}.label,'All'), continue; end; 
    end
    if firstone, cla;
        errorbar(1:X,log(ccp_pat_fit.data{m}.mean(1:X)),...
                     -log(ccp_pat_fit.data{m}.lb(1:X))+log(ccp_pat_fit.data{m}.mean(1:X)),...
                      log(ccp_pat_fit.data{m}.ub(1:X))-log(ccp_pat_fit.data{m}.mean(1:X)),'k--')
        hold on;
        leg2 = {'Data'};
    end
    plot(1:X,log(ccp_pat_fit.pred(1:X)),marker(spec),'MarkerEdgeColor',tint(spec),'Color',tint(spec),'MarkerFaceColor','none')
    leg2{leg_position} = beautify(spec);
    axis([0.5 X+0.5    ylim])
end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Output figure 1 (a): Model Fit
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(1)
    
    xlabel('Offer Number');
    ylabel('Acceptance Probability');
    legend(leg1);
    saveas(gcf,[options.output_stub 'results/ccp_position_fit.png']);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Output figure 1 (b): Model Fit
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(2)
    xlabel('Years Waited');
    ylabel('Log Acceptance Probability');
    legend(leg2,'Location','northwest');
    saveas(gcf,[options.output_stub 'results/ccp_waittime_fit.png']);



function beautiful = beautify(ugly)
    if      strfind(ugly,'time_uh'), beautiful = 'Donor UH and Time Controls';
    elseif  strfind(ugly,'uh'),      beautiful = 'Donor UH';
    else                             beautiful = 'Base Specification';
    end
    

function marker = marker(ugly)
    if      strfind(ugly,'time_uh'), marker = 'x';
    elseif  strfind(ugly,'uh'),      marker = 's';
    else                             marker = 'o';
    end
    
function tint = tint(ugly)
    if      strfind(ugly,'time_uh'), tint = [0.0 0.0 1];  % dark blue
    elseif  strfind(ugly,'uh'),      tint = [0.4 0.4 1];
    else                             tint = [0.6 0.6 1];  % light blue
    end
    
function write_tex(fid,frametitle,filename)   % Writes a slide with the figure.
    frametitle = regexprep(frametitle,'[^a-zA-Z0-9]',' '); % Only alpha-numeric (to avoid latex problems)
    fprintf(fid,'\\begin{frame}\n    \\frametitle{%s}\n    \\includegraphics[scale=0.5]{%s}\n\\end{frame} \n',frametitle,filename);
