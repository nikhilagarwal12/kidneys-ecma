#include "common.h"

#include "mex.h"

#include <math.h>
#include <stdint.h>
#include <string.h>

// A bunch of static variables to remove reallocation of arrays
static int notInitialized = 1;
static double *lambdaFeasibilityMod;
static long *iIndex, *jIndex;
static double *value;
static double *expTerm, *akc, *cLh;

void cleanup(void) {
  mexPrintf("MEX-file is terminating, destroying allocations in analytic small first Hessian\n");
  free(iIndex);
  free(jIndex);
  free(value);
  free(lambdaFeasibilityMod);
  free(expTerm);
  free(akc);
  free(cLh);
  notInitialized = 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[2], 0, "mu"));
  const long nPatients = sizeArray[0];
  const long nTime = sizeArray[1];
  const long nDonors = sizeArray[2];

  /* Input parameters constructed for smaller problem */
  double *sigmaFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "sigmaFiltered"));
  double *deltaBy_hFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "delta_x_hFiltered"));
  double *lambda_hOverKFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "lambdah_over_KFiltered"));
  double *discountWFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "discount_wFiltered"));
  const double nDonorsInv =  (double)mxGetScalar(mxGetField(prhs[2], 0, "nDonorsInv"));
  double *cArrayRepeatFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "pr_compat_repeatFiltered"));
  double *paretoWeightsRepeatFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "pareto_weights_repeatFiltered"));
  double *hFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "hFiltered"));
  const uint32_t maxTransplant = *((uint32_t*)mxGetPr(mxGetField(prhs[2], 0, "maximize_transplants")));
  
  const long nQ = nPatients*nTime*nDonors;
  const long nP = nPatients*nTime;
  const double *q = (double*)mxGetPr(prhs[0]);
  const double *lambdaFeasibility = (double*)mxGetPr(mxGetField(prhs[1], 0, "ineqnonlin"));
  const double *lambdaSteadyState = (double*)mxGetPr(mxGetField(prhs[1], 0, "eqnonlin"));

  /* Part to filter for smaller problem */
  uint64_t *filterPattern = (uint64_t*)mxGetPr(prhs[3]);
  const uint64_t qCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "qCount")));
  const uint64_t pCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "pCount")));
  const uint64_t pM1Count =  *((uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "pM1Count")));
  const uint64_t donorCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "donorCount")));
  const uint64_t *npIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "npIndex"));
  const uint64_t *donorIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "donorIndex"));
  uint64_t *donorList1D =  (uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "donorList"));
  uint64_t *map =  (uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "map"));

  const double *p = q+qCount;
  const double *lambdaValue = lambdaSteadyState + pM1Count;
  const unsigned long nDonorsP1 = nDonors+1;
  if (notInitialized) {
    lambdaFeasibilityMod = (double*)malloc(donorCount*sizeof(double));
    const long nnz = pCount*(donorCount*donorCount+2)+qCount*4;
    iIndex = (long*)malloc(nnz*sizeof(long));
    jIndex = (long*)malloc(nnz*sizeof(long));
    value = (double*)malloc(nnz*sizeof(double));
    akc = (double*)malloc(qCount*sizeof(double));
    cLh = (double*)malloc(qCount*sizeof(double));
    expTerm = (double*)malloc(pCount*sizeof(double));
    mexAtExit(cleanup);
    notInitialized = 0;
  }
  memcpy(lambdaFeasibilityMod, lambdaFeasibility, donorCount*sizeof(double));
  if (maxTransplant) {
    double *slackCostFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "slack_costFiltered"));
    for (unsigned long j = 0; j < donorCount; ++j) {
      lambdaFeasibilityMod[j] -= slackCostFiltered[j];
    }
  }
  memset(expTerm, 0, pCount*sizeof(double));

  unsigned long int currentIndex = 0;
  // Precompute common sub expressions
  uint64_t* dList = donorList1D;
  for (unsigned long i = 0; i < qCount; ++i) {
    const unsigned long nIndex = npIndex[i];
    if (nIndex < pM1Count) {
      expTerm[nIndex] += q[i]*cArrayRepeatFiltered[i]; 
    }
    akc[i] = lambdaValue[nIndex]*discountWFiltered[nIndex]*cArrayRepeatFiltered[i]*nDonorsInv;
    cLh[i] = cArrayRepeatFiltered[i]*lambda_hOverKFiltered[nIndex];
  }
  for (unsigned long i = 0; i < pM1Count; ++i) {
    expTerm[i] = -lambdaSteadyState[i]*exp(deltaBy_hFiltered[i]-lambda_hOverKFiltered[i]*expTerm[i]);
  }
  // Add qq Values
  for (unsigned long k = 0; k < pCount; ++k, dList += nDonorsP1) {
    const double localValue = p[k]*expTerm[k];
    const unsigned long donorCount = dList[0];
    for (unsigned long i = 1; i < donorCount; ++i) {
      const unsigned long index1 = dList[i];
      const double cArrayLocal = localValue*cLh[index1];
      for (unsigned long j = 1; j < donorCount; ++j) {
        const unsigned long index2 = dList[j];
        double localValue1 = cArrayLocal*cLh[index2];
        if (i == j) {
          localValue1 += akc[index2]*sigmaFiltered[k]/normPdf(invCDF(1.0-q[index2]));
        }
        if (localValue1 != 0.0) {
          iIndex[currentIndex] = index1;
          jIndex[currentIndex] = index2;
          value[currentIndex++] = localValue1;
        }
      }
    }
  }
  // Add qp and pq Values
  dList = donorList1D;
  for (unsigned long j = 0; j < pCount; ++j, dList += nDonorsP1) {
    const unsigned long donorCount = dList[0];
    const unsigned long index2 = j+qCount;
    for (unsigned long i = 1; i < donorCount; ++i) {
      const unsigned long index1 = dList[i];
      const double localValue = cArrayRepeatFiltered[index1]*(hFiltered[j]*lambdaFeasibilityMod[donorIndex[index1]]-lambda_hOverKFiltered[j]*expTerm[j]);
      if (localValue != 0.0) {
        iIndex[currentIndex] = index1;
        jIndex[currentIndex] = index2;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = index2;
        jIndex[currentIndex] = index1;
        value[currentIndex++] = localValue;
      }
    }
  }
  /* const unsigned long nPmnPa = nP - nPatients; */
  const unsigned long offset = nQ+nP;
  // Add qv and vq values
  for (unsigned long i = 0, offset1 = 0; i < nDonors; ++i, offset1 += nP) {
    for (unsigned long j = 0; j < nP; ++j) {
      const unsigned long index1 = j+offset1;
      const unsigned long index2 = j+offset;
      if (filterPattern[index1] && filterPattern[index2]) {
        const unsigned long i1 = map[index1];
        const double localValue = akc[i1];
        if (localValue != 0.0) {
          const unsigned long i2 = map[index2];
          iIndex[currentIndex] = i1;
          jIndex[currentIndex] = i2;
          value[currentIndex++] = localValue;
          iIndex[currentIndex] = i2;
          jIndex[currentIndex] = i1;
          value[currentIndex++] = localValue;
        }
      } 
    }
  }
  // Add pv and vp contribution
  if (!maxTransplant) {
    const unsigned long offset1 = qCount+pCount;
    for (unsigned long i = 0; i < pCount; ++i) {
      const unsigned long index1 = i+qCount;
      const unsigned long index2 = i+offset1;
      const double localValue = -paretoWeightsRepeatFiltered[i]*hFiltered[i]; // Contribution from objective
      if (localValue != 0.0) {
        iIndex[currentIndex] = index1;
        jIndex[currentIndex] = index2;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = index2;
        jIndex[currentIndex] = index1;
        value[currentIndex++] = localValue;
      }
    }
  }
  // Create output sparse matrix
  const long matrixSize = qCount+2*pCount;
  plhs[0] = mxCreateSparse(matrixSize, matrixSize, currentIndex, mxREAL);
  double *sr  = (double*)mxGetPr(plhs[0]);
  size_t* irs = (size_t*)mxGetIr(plhs[0]);
  size_t* jcs = (size_t*)mxGetJc(plhs[0]);
  COO_toCSC(matrixSize, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
}
