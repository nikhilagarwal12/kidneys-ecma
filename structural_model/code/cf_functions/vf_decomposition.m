% Decompose value function into observed and unobserved components

function [V_obj,pr_tx] = vf_decomposition(q,V,params,heu)
    % V: value function (N_patients x N_time)
    % pr_tx: probability of transplant (yes and compat) conditional on offer (N_patients x N_time x N_donors).
    % Not exactly backwards induction because I use previous V instead.
    % It will perform similarly, maybe reduces oscilations.
    [N_patients, N_time, N_donors] = size(q);
    a = params.discount_w;
    
    if (isfield(params,'first_best') && params.first_best)
        run_type = 'first_best';
    elseif isfield(params,'use_fact_V') && params.use_fact_V
        run_type = 'second_best_use_fact';
    elseif nargin==4 && ~isempty(heu)
        run_type = 'second_best_backwards_induction';
    else
        run_type = 'second_best';
    end

    qc  = bsxfun(@times,q,params.pr_compat);
    
    if strcmp(run_type,'first_best')
        z   = norminv(1-q); % cutoff for standard normal
        fz  = normpdf(z);
        w_kit_obs   = bsxfun(@times,qc.*params.mu,params.pr_compat);% E(mu)P(mu+eps>cutoff)
        w_kit_unobs = bsxfun(@times,bsxfun(@times,params.sigma,fz),params.pr_compat);% E(eps|mu+eps>cutoff)P(mu+eps>cutoff)
        w_obs   = mean(w_obs,3);
        w_unobs = mean(w_unobs,3);
        w       = w_obs + w_unobs;
        w_kit_check = bsxfun(@times,qc.*params.mu + bsxfun(@times,params.sigma,fz),params.pr_compat);
        w_check     = mean(w_check,3);
        b       = a.*mean(qc,3) + params.discount_V1; 
        if nargout>1, pr_tx = bsxfun(@times,ones(size(params.mu)),params.pr_compat); end
    else
        z   = bsxfun(@times,bsxfun(@plus,params.mu,-V),1./params.sigma); % cutoff for standard normal
        fz  = normpdf(z);
        Fz  = normcdf(z);                        % Prob of accepting the offer
        if strcmp(run_type,'second_best')
            Emax_obs   = bsxfun(@times,params.sigma, Fz.*z);      % 'observed' part
            Emax_unobs = bsxfun(@times,params.sigma, fz);         % 'unobserved' part
            w_obs      = mean(qc.*Emax_obs,3);
            w_unobs    = mean(qc.*Emax_unobs,3);
            w          = w_obs + w_unobs;
            Emax_check = bsxfun(@times,params.sigma,Fz .* z + fz); % Emax(0,mu-V+eps)
            w_check    = mean(qc.*Emax_check,3);
            b          = params.discount_V2;
        else   % use_V_fact
            % Emax = Fz .* params.mu +bsxfun(@times,params.sigma,fz); % E(mu+eps|mu+eps>cutoff)P(mu+eps>cutoff)
            Emax_obs   = Fz .* params.mu;      % 'observed' part
            Emax_unobs = bsxfun(@times,params.sigma, fz);         % 'unobserved' part
            w_obs      = mean(qc.*Emax_obs,3);
            w_unobs    = mean(qc.*Emax_unobs,3);
            w   = w_obs + w_unobs;
            Emax_check = Fz .* params.mu +bsxfun(@times,params.sigma,fz); % E(mu+eps|mu+eps>cutoff)P(mu+eps>cutoff)
            w_check    = mean(qc.*Emax_check,3);
            b   = a.*mean(Fz.*qc,3) + params.discount_V1;
        end
        if nargout>1, pr_tx = bsxfun(@times,Fz,params.pr_compat); end
    end
    
    % Update V
        V_full         = zeros(size(a));
        V_obs          = zeros(size(a));
        V_unobs        = zeros(size(a));
        V_check        = zeros(size(a));
        V_full(:,end)  = a(:,end) .* w(:,end) ./ b(:,end);
        V_obs(:,end)   = a(:,end) .* w_obs(:,end) ./ b(:,end);
        V_unobs(:,end) = a(:,end) .* w_unobs(:,end) ./ b(:,end);
        V_check(:,end) = a(:,end) .* w_check(:,end) ./ b(:,end);
        for k=2:size(V,2)
            V_full(:,end+1-k)  = (a(:,end+1-k) .* w(:,end+1-k)       + V_full(:,end+2-k)) ./b(:,end+1-k);
            V_obs(:,end+1-k)   = (a(:,end+1-k) .* w_obs(:,end+1-k)   + V_obs(:,end+2-k))  ./b(:,end+1-k);
            V_unobs(:,end+1-k) = (a(:,end+1-k) .* w_unobs(:,end+1-k) + V_unobs(:,end+2-k))./b(:,end+1-k);
            V_check(:,end+1-k) = (a(:,end+1-k) .* w_check(:,end+1-k) + V_check(:,end+2-k))./b(:,end+1-k);
        end
    
    V_full(V==0)=0;V_obs(V==0)=0;V_unobs(V==0)=0;
        V_obj.V = V_full; V_obj.V_obs = V_obs; V_obj.V_unobs = V_unobs;
end
