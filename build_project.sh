#!/bin/bash

# Get the current path
  SCRIPTPATH=$(pwd)

# Time the script
  start=$(date +%s)

# Mount datasets -- encfs-mount is a local alias
#  encfs-mount organs
#  encfs-mount organs-raw

# Step 0: Extract data on STAR CDs 
  cd  ${SCRIPTPATH}
  extract_script.sh
  st ~/organs-raw/raw-extract-4/match_run_num_nagarwal.sas7bdat ~/organs-star/primary_files/match_run_num_optn.dta 

# Step 1: Generate Primary Files
  cd ${SCRIPTPATH}/data_preparation/code 
  stata-mp -b do gen_primary_files.do

# Step 2: Summary Statistics
  cd  ${SCRIPTPATH}/reduced_form/code 
  stata-mp -b do summary_statistics.do 
  stata-mp -b do positive_crossmatch_model.do
  stata-mp -b do evidence_of_dynamic_incentives.do 
  stata-mp -b do past_offers.do
  matlab -r "autocorrelation_test; exit;"
  matlab -r "patient_order; exit;"

# Step 3: Transfer files to MATLAB and run structural model
  cd ${SCRIPTPATH}/structural_model/
  sh import_all.sh

  cd ${SCRIPTPATH}/structural_model/code/cf_optimal/
  matlab -r "compileAllMex; exit;"

  cd ${SCRIPTPATH}/structural_model/code/model_files/
  matlab -r groups_abo
  matlab -r groups_age 
  
  cd ${SCRIPTPATH}/structural_model/
  matlab -r "run_model; exit;"

  # The next line needs to be run multiple times, each with different settings.
  # See README at the top of run_model_validation.m for instruction 
  matlab -r "run_model_validation; exit;"

  # The next line needs to be run multiple times, each with different settings.
  # See README at the top of run_cf.m for instruction
  matlab -r "run_cf; exit;"

# Step 4: Pull tables and figures into assets folder
  cd ${SCRIPTPATH}/
  sh pull_tables_and_figures.sh
