function [D,group_ids] = create_dummies(group,group_ids)
%   Returns a matrix X containing zeros and ones, whose
%   columns are dummy variables for the grouping variable GROUP.  GROUP can be
%   a categorical or numeric column vector.  The I'th dummy variable column in
%   X contains ones in elements where values in GROUP specify the I'th group.
    
D = sparse(size(group,1),size(group_ids,1));
for gg = 1:size(group_ids,1)
    D(:,gg) = sparse(group == group_ids(gg));    
end

D = sparse(D);