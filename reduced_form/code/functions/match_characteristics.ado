// Create table showing age and health match of transplants
// Also show fraction/number of each patient age group not transplanted, among those still waiting end 2013

program match_characteristics

args kidpan_data donor_data output_folder start_date end_date sample_name

    local variables = "age_don_g1 age_don_g2 age_don_g3 age_don_g4 age_don_g5 "

    local outcome_vars = "transplanted"

    use `kidpan_data', clear

    // code what happened to the patient
    gen transplanted = tx_date >= `start_date' & tx_date <= `end_date'
    replace transplanted = (don_ty=="C")*transplanted   // patients who received DECEASED donor transplants

    // age groups for donors and patients
    gen age_don_g1 = age_don <= 17
    gen age_don_g2 = age_don <= 34 & age_don > 17
    gen age_don_g3 = age_don <= 49 & age_don > 34
    gen age_don_g4 = age_don <= 64 & age_don > 49
    gen age_don_g5 = age_don >= 65

    gen age_pt = "Patient Age 0-17" if init_age <= 17    // by INITIAL age for patients
    replace age_pt = "Patient Age 18-34" if init_age > 17 & init_age <= 34
    replace age_pt = "Patient Age 35-49" if init_age > 34 & init_age <= 49
    replace age_pt = "Patient Age 50-64" if init_age > 49 & init_age <= 64
    replace age_pt = "Patient Age 65+" if init_age > 64

    gen dialysis_str = "Not on Dialysis at Registration"
    replace dialysis_str = "On Dialysis at Registration" if dialysis_date < init_date

    gen count = 1

    // patient by age
    preserve
    collapse (sum) count if other_center_nyrt==0, by(age_pt)
    rename age_pt group
    egen total = total(count)
    gen share = count / total
    keep group share 
    order group share 
    tempfile outcomes
    save `outcomes', replace
    restore

    // patient by dialysis
    preserve
    collapse (sum) count if other_center_nyrt==0, by(dialysis_str)
    rename dialysis_str group
    egen total = total(count)
    gen share = count / total
    keep group share 
    order group share 
    append using `outcomes'
    save `outcomes', replace
    restore


    // second, restrict to DDR tx recipients and see what kind of donors they got
    keep if transplanted==1

    preserve
    collapse (sum) count (mean) age_don_g1-age_don_g5, by(age_pt)
    rename age_pt group
    egen total = total(count)
    gen share_don_pt = count / total
    drop total count
    tempfile all
    save `all'
    restore

    preserve
    collapse (sum) count (mean) age_don_g1-age_don_g5, by(dialysis_str)
    rename dialysis_str group
    egen total = total(count)
    gen share_don_pt = count / total
    drop total count
    append using `all'
    save `all', replace
    restore

    // merge everything together
    use `outcomes', clear
    merge 1:1 group using `all', assert(match) nogen

    gen group_cat = .
    replace group_cat = 1 if group=="Patient Age 0-17"
    replace group_cat = 2 if group=="Patient Age 18-34"
    replace group_cat = 3 if group=="Patient Age 35-49"
    replace group_cat = 4 if group=="Patient Age 50-64"
    replace group_cat = 5 if group=="Patient Age 65+"
    replace group_cat = 6 if group=="On Dialysis at Registration"
    replace group_cat = 7 if group=="Not on Dialysis at Registration"

    sort group_cat
    drop group_cat

    // label variables for excel
    label var group "Patient Group"
    label var share "Share of Patient Population"
    label var share_don_pt "Share of Patient Population (By Donor Age)"
    label var age_don_g1 "Donor Age 0-17"
    label var age_don_g2 "Donor Age 18-35"
    label var age_don_g3 "Donor Age 35-49"
    label var age_don_g4 "Donor Age 50-64"
    label var age_don_g5 "Donor Age >= 65"


    order group share share_don_pt age_don*
    export excel using "`output_folder'summary_tables_`sample_name'.xls", sheet("Match Characteristics Table") firstrow(varlabels) sheetreplace

end
