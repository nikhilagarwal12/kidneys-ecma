function [q,pr_expires] = update_offer_probability(p,pr_tx,V,position,max_position,params)
    % p composition of the list: density of agents
    % pr_tx probability of a transplant conditional on offer
    % V value function
    % position: will offer organs from position 1 to position max_position
    % params: parameters of the model
    % max_position: position at which the procedure stops offering the organ.
    
    % pr_expires = 1 - pr_no_expires; pr_no_expires = (1-pr_tx).*(1-pr_time_out);
    % pr_expires = pr_tx + reshape(params.timeout_prob,1,1,[]).*(1 - pr_tx);
    % pr_expires = pr_tx + reshape(params.timeout_prob,1,1,[]); % Dropped (1 - pr_tx) to make it consistent with constraints (HACK);
    pr_expires = bsxfun(@plus,pr_tx,reshape(params.timeout_prob,1,1,[]));
    
    % Expiration rate: Expected number of expirations for a patient-time-donor triplet
    expiration_rate      = bsxfun(@times,p.*params.h,pr_expires);
    % Cumulative expiration rate for a donor
    cum_expiration_rate  = zeros(size(position,3),1);
    % Initialize q
    q = zeros(size(position));
    
    for ss = 1:max_position % Iterate over position levels
        % Find patient-time-donor triplets in the current position
        where_position = position==ss; 
        % Add up their expected number of expirations
        expiration_rate_ss  = squeeze(sum(sum(expiration_rate.*(where_position),1),2));
        % Run the Poisson approximation
        p_offers            = poisson_approx(cum_expiration_rate,expiration_rate_ss);
        % Fix cases with 1 kidney and update q
        p_offers.any(params.endowment == 1) = p_offers.both(params.endowment == 1);
        
        for dd = 1:size(q,3)
            q(:,:,dd) = q(:,:,dd) + where_position(:,:,dd)*p_offers.any(dd);
            % maybe there is a better way to make this assignment. I don't 
            % need to use a sum. I just need to assign p_offers.any(dd) to all
            % the cells in q(:,:,dd) such that where_position(:,:,dd)==1.
        end
        % Update cum_expiration_rate
        cum_expiration_rate = cum_expiration_rate + expiration_rate_ss;
        
    end
    % debuging_tools(q,p,V,params)
    
    
function [appx,lb,ub] = poisson_approx(r1,r2)
    % r1 = Expected acceptances/timeouts by higher positions;
    % r2 = Expected acceptances/timeouts by the same positions;Expiration rate for the same positions
    % Using the Poisson approximation of the binomial
    a = r1;     % 
    b = r2 + a; % Expected acceptances by same or higher position;
    c = r1;
    d = r2 + c; % I know this is redundant but I want to keep the rest of the code until we figure out how to fix the hack of timeouts
    elambda = 1;
    
    % lower bound: probability of zero and one acceptance by higher positions; 
    lb.both = exp(-b).*elambda;
    lb.any  = lb.both + d .* exp(-b).*elambda;
    
    % upper bound: probability of zero and one acceptance by higher or same position;
    % Using property of Poisson: X~P(L1), Y~P(L2) X+Y~P(L1+L2);
    ub.both = exp(-a).*elambda;
    ub.any  = ub.both  + c .* exp(-a).*elambda;
    
    % Integrating the poisson cdf over all possible positions in the pool
    % From mathematica
    %that = (a+(-1).*b).^(-2).*(((-1)+(-1).*a+b).*c+d).*exp(1).^((-1).*a)+(a+( ...
    %1        -1).*b).^(-2).*(c+((-1)+a+(-1).*b).*d).*exp(1).^((-1).*b);
    % Simpler:
    this = ( ( (b-1-a).*c+d).* exp(-a) + (c+ (a-1-b).*d).* exp(-b) ) ./ (b-a).^2;
    %if max(abs(that-this))>1e-10, keyboard; end
    
    appx.both     = ((exp(-a) - exp(-b))./(b-a)).*elambda;
    appx.any      = appx.both + this.*elambda;
    na            = (b - a) < 1e-8;
    appx.both(na) = (lb.both(na)+ ub.both(na))/2;  % numerical adjustment;
    appx.any(na)  = (lb.any(na) + ub.any(na))/2;
    btw           = @(x,lb,ub) max(min(x,ub),lb);
    appx.both     = btw(appx.both,lb.both,ub.both);
    appx.any      = btw(appx.any ,lb.any ,ub.any );


function [expected_discards,expected_discards2] = debuging_tools(q,p,V,params)
    % Verify feasibility condition:
    
    % Using current V
    useful.flat  = @(x) x(:);
    expected_discards = params.endowment(:) - squeeze(sum(sum(expiration_rate .* q,1),2)); 
    
    % Using last V
    if params.first_best
        poo
    else
        mli  = bsxfun(@plus,params.mu,-V);         % Mean of the Latent Index
        z   = bsxfun(@times,-mli,1./params.sigma); % cutoff for standard normal
        Fz  = normcdf(-z);
        cp_t = bsxfun(@plus, bsxfun(@times, Fz,params.pr_compat),reshape(params.timeout_prob,1,1,[]));
        qcz_p_t = q.*cp_t;
        expected_discards2 = params.endowment(:) - useful.flat(sum(sum(bsxfun(@times,params.h.*p,qcz_p_t),2),1)) ; % >=0
    end
    
    norm(expected_discards-expected_discards2)