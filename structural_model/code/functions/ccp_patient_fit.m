function ccps = ccp_patient_fit(data,model,chain,donors,offers,pt_history,patients,groups,eta_draws,beta_draws,options)

% initialize the random number generator
if ~isfield(options,'rng_seed')
    options.rng_seed = 954637;  % Randomly generated seed on 11-17-16
end
rng(options.rng_seed);
if model.donor_uh
    if ~isfield(options, 'edraws'), options.edraws=size(eta_draws,2); end
    eta_draws = eta_draws(:,round([1:options.edraws]*size(eta_draws,2)/options.edraws));
end
if model.pt_uh
    if ~isfield(options, 'edraws'), options.edraws=size(eta_draws,2); end
    beta_draws = beta_draws(:,round([1:options.edraws]*size(beta_draws,2)/options.edraws));
end

% predicted and actual ccp vectors
ccps.data = cell(numel(groups),1);

% Convention accept is zero
assert(isequal(model.choice_def{2},'accept'));   
   
% contribution of UH to CCP
mu_fixed = data.fixed.obs * chain.mu_theta(1:model.fixed.K);  % contribution of fixed part
if model.pt_uh
    where_ii= cell(size(data.patient_list));
    for ii=1:data.N_patients
        where_ii{ii} = find(data.patients == data.patient_list(ii));           
    end
    mu = zeros(model.N_offers,size(beta_draws,2));
    for draw = 1:size(beta_draws,2)  % contribution of flexible part
        beta_i = beta_draws(:,draw);
        for kk = 1:model.K_types
            where_kk = cat(1,where_ii{beta_i==kk});  % offers to patients of this type
            ind_kk = model.fixed.K + (kk-1)*model.flex.K + (1:model.flex.K);  % select coefficients
            mu(where_kk,draw) = mu_fixed(where_kk) + data.flex.obs(where_kk,:)*chain.mu_theta(ind_kk);
        end
    end
else
    if model.donor_uh
        mu = repmat(mu_fixed,1,size(eta_draws,2));
    else
        mu = mu_fixed;
    end
end

if model.donor_uh
    % Compute the CCPs
    [~,where_don] = ismember(data.donor_id,data.donor_list);
    ccps.model = 1 - mean(normcdf(mu + eta_draws(where_don,:) ),2);
    % if Out of memory: try reducing edraws in options. 
else
    ccps.model = 1 - mean(normcdf(mu),2);
end 

% Prepare to save the output as plots
model_results_folder = [options.results_folder model.model_name];
makedir2(model_results_folder)
makedir2([model_results_folder '/figs'])
% TeX code
fid = fopen([model_results_folder '/figs/ccp_patient_fit.tex'],'w');
    
    
% Get waittime
[~,where_pat] = ismember(data.patients,patients.wl_id_code);
[~,where_don] = ismember(data.donor_id,offers.donor_id);
waittime = offers(where_don,:).match_date - patients(where_pat,:).init_date;
K=ceil(max(waittime/365.25));
% Fit by position
write_tex(fid,[model.model_name ' Fit by position'],[model_results_folder '/figs/ccp_fit.png']);
% Loop Over Groups
for m=1:numel(groups)
    % Which donors and patients are in the group
    [pt_include,k_include,label] = split_groups(donors,pt_history,patients,groups,m);
    incl = ismember(data.pt_history_id,pt_history(pt_include,:).wlreg_audit_id_code);
    incl = incl & ismember(data.donor_id,donors(k_include,:).donor_id);
    plotm  = zeros(size(incl,1),2);
    ccps.data{m}.label = groups{m}.label;
    
    for k = 1:K
        inclk = incl & max(waittime,0)>=365.25*(k-1) & waittime<365.25*k;
        plotm(inclk,1) = ccps.model(inclk);
        plotm(inclk,2) = k;
        ccps.data{m}.mean(k) = 1-mean(data.cont_choice(inclk));
        ccps.data{m}.n(k) = sum(inclk);   
    end
    assert(all(plotm(incl,2)>0));
    % Generate the plot
    cla
    ccps.pred = zeros(K,1); for k=1:K, ccps.pred(k)=mean(plotm(plotm(:,2)==k,1)); end
    plot(1:K,ccps.pred)
    hold on 
    plot(1:K,ccps.data{m}.mean)
    title({'Conditional Choice Probability Fits'; label});
    xlabel('Years Waited');
    ylabel('Acceptance Probability');
    legend('CCPs','Data')
    saveas(gcf,[model_results_folder '/figs/ccp_fit_' label '.png']);
        
    hold off
    
    cla
    if m<numel(groups) % I don't want the boxplot for the whole sample
        boxplot(log(plotm(incl,1)),plotm(incl,2),'Symbol','');
        hold on
    end
     
    [ub,lb]=test_inversion(ccps.data{m}.mean,ccps.data{m}.n);
    
    plot(1:sum(ccps.data{m}.n>0),log(ub(ccps.data{m}.n>0)),'k--'),hold on;
    plot(1:sum(ccps.data{m}.n>0),log(lb(ccps.data{m}.n>0)),'k--')
    plot(1:sum(ccps.data{m}.n>0),log(ccps.pred(ccps.data{m}.n>0)),'rx')
    title({'Conditional Choice Probability Fits'; label});
    xlabel('Years Waited');
    ylabel('Log Acceptance Probability');
    fname = [model_results_folder '/figs/ccp_fit_' label '_log.png'];
    ccps.data{m}.ub = ub;
    ccps.data{m}.lb = lb;
    
    if m==numel(groups), legend('Data 95CI LB','Data 95CI UB','CCPs'); end
    saveas(gcf,fname);  
    write_tex(fid,[model.model_name ' ' label],fname)    
    hold off
  
    
end

cla
labels = {};
for m=1:numel(groups)
    plot(1:K,log(ccps.data{m}.n))
    hold on
    labels{m} = groups{m}.label;
end
legend(labels{:})
xlabel('Years Waited');
ylabel('Log Number of Offers in the data');
saveas(gcf,[model_results_folder '/figs/ccp_fit_numoffers_log.png']);
hold off
cla

fclose(fid);


function makedir2(dir)
    if ~isdir(dir)
        mkdir(dir);
        fprintf('Created folder %s\n', dir);
    else
        fprintf('Folder %s already exists.\n', dir);
    end
    
function [ub,lb]=test_inversion(p,n)
    % p0 such that p0 - 1.96 sqrt(p0(1-p0)/n) = p;
    for i = 1:numel(p)
        if n(i)==0
            lb(i) = nan; ub(i) = nan;
        else        
            objlb = @(x) (p(i) - x - 1.96*sqrt(x*(1-x)/n(i))).^2;
            objub = @(x) (p(i) - x + 1.96*sqrt(x*(1-x)/n(i))).^2;
            lb(i) = fminbnd(objlb,0,1);
            ub(i) = fminbnd(objub,0,1);
        end
    end
    
function write_tex(fid,frametitle,filename)   % Writes a slide with the figure.
    frametitle = regexprep(frametitle,'[^a-zA-Z0-9]',' '); % Only alpha-numeric (to avoid latex problems)
    fprintf(fid,'\\begin{frame}\n    \\frametitle{%s}\n    \\includegraphics[scale=0.5]{%s}\n\\end{frame} \n',frametitle,filename);

    
    
    
