% Mechanism Aliases:

% basic structure
cf_mech                         = struct();
cf_mech.options                 = struct();
% cf_mech.options : default empty   
%           optional: 'verbose' (default: 1, 0 ,-1 is
%           suppress) 
%           initial conditions:
%           'pool_size','pt_weights','p_tx' and 'V'
% Can over-write defaults

% Splits ch_mech_alias ='score_binspoints_fact in {type,score,modif}={'score','binspoints','fact'}
C = strsplit(cf_mech_alias,'_');

cf_mech.type  = C{1};       % Counterfactual type

if strcmp(cf_mech.type,'score')
    score     = C{2};
    
    switch score  
      case 'kas',               cf_mech.score = @(x) -(x.kas_bins)*100  + round(x.kas_points/2);
      case 'binspoints',        cf_mech.score = @(x) -(x.bins)*1000   + (x.points);
      case 'gammaeta',          cf_mech.score = @(x) x.gammaeta(inf);
      case 'fifo',              cf_mech.score = @(x) cumsum(x.postEntry,2)+log(x.feasible);
      case 'lifo',              cf_mech.score = @(x) (size(x.Gamma_eta,2)-cumsum(x.postEntry,2))+log(x.feasible);
      otherwise
        error(['Custom mechanism: ' cf_mech_alias ' is not a recognized mechanism alias.']);
    end
elseif strcmp(cf_mech.type,'optim')
    type = C{2};
    switch type
        case 'first',           cf_mech.first_best = true;
        case 'second',          cf_mech.first_best = false;
        case 'firstC',          cf_mech.first_best = [true true];
        case 'secondC',         cf_mech.first_best = [false true];
      otherwise
        error(['Unrecognized optim type in ' cf_mech_alias]);
    end
    cf_mech.simplify_times  = [];
elseif strcmp(cf_mech.type,'greedy')
    the_solution = [rslt_filepath '_optim_' C{2} '_soln.mat'];
    the_problem = [rslt_filepath '_optim_' C{2} '_prob.mat'];
    if exist(the_solution,'file')
        prev_pr  = load(the_problem);
        prev_cf  = load(the_solution); 
        prev_pr.opt_prob.alloc = prev_cf.alloc;
        cf_mech.score = @(x) prev_pr.opt_prob.full_times.alloc.q;
        cf_mech.type = 'score';
    else
        fprintf('Cannot find: %s\n',the_solution)
        cf_mech.score = [];
    end
end

cf_mech.use_fact_V = false;
cf_mech.use_proj   = false;
cf_mech.simplify_times  = [0:60 68:8:100 200 300 399];  % 69 time periods (default)
cf_mech.update_V_ref      = false;
for modif = C(3:end)
    switch modif{1}
      case 'fact',              cf_mech.use_fact_V = true; % uses estimated choice probabilities
      case 'locl',              non_local_score    = @(x) -(x.bins)*1000   + (x.points); % new rules only for NYRT DONORS
                                cf_mech.score      = @(x) score_local_fun(x,cf_mech.score,non_local_score);
      case 'notimeout',         cf_mech.timeout = false;
      otherwise
        warning(['Unrecognized modifier in ' cf_mech_alias]);
    end
end


% As one sees above, cf_mech may include a field 'heuristic'. e.g.:
% cf_mech.heuristic.tol = 1e-6;
% cf_mech.heuristic.Max_iter = 50;
% If not included gen_cfs.m chooses default values
