% Runs all versions of structural model (CCP estimation + value function calculations)
% - Model definition files must have already been created
% - Loads model_name
%    - Model files must be stored in the same folder, with different 'model_name' suffixes
% - Builds data structures based on each model
% - Loads chain specs and runs chains for different models
% - Computes functions of model output

% Generates the baseline master-file
if tasks.init_model && ~  tasks.gen_data
   fprintf('Ignoring tasks.init_model because tasks.gen_data is off\n');
end
if tasks.gen_data
    display(['Data for model:' model_name]);
    if tasks.init_model
        display(['Generating the Model File For:' model_name]);
        eval(['model_file_' model_name]);
    else 
        load(strcat(models_folder,model_name),'model','priors');
    end
    

    load(model.filenames.donors,'donors');
    load(model.filenames.patients,'patients');
    load(model.filenames.pt_history,'pt_history');
    load(model.filenames.offers,'offers');

    type = 'master';

    % Fields are added to the model structure
    % Priors are expanded from single dimensional to multi-dimensional
    [data,model,priors] = gen_data_structure(model,priors,offers,patients,donors,pt_history,type,model_data_folder,options.output_stub);

    % Add a placeholder for LYFT
    data.lyft = zeros(data.N_offers,1);

    % Save master file
    % verify model name
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end
    save([model_data_folder model.model_name '/master'],'data','model','priors','-v7.3');
end


% Run gibbs sampler chains for each model
if tasks.gen_gibbs_chain
    % Define chain
    chain_spec=struct();
    chain_spec.name = 'base';
    chain_spec.draw_params = true;    
    chain_spec.thin = 10;  % Thinning constant -- add every thin-th draw to the chain
    chain_spec.NITER = 10000;
    if ~isfield(chain_spec,'chain_no')
        chain_spec.chain_no = 1;
    end

    load([model_data_folder model_name '/master']);
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end
        
    % Define initial values
    init = struct();
    init.rng_state = chain_spec.chain_no*100;
    
    rng(671173); % Randomly generated seed on 11-17-16
    init.theta = mvnrnd(priors.mu_theta*chain_spec.chain_no,priors.V_theta*chain_spec.chain_no);
    if model.donor_uh
        init.sigma_eta = rand*chain_spec.chain_no;
    end
    if model.pt_uh
        init.beta = dirichlet_sample(priors.beta);  % draw multinomial probability vector governing patient types
    end
    
    % Run the chain
    gen_chains_fun(chain_spec,model,priors,init,data,options);    

    clear chain_spec;
end


% Read available chains and the model structure for post-estimation
tasks.post_estimation = any([tasks.summarize_base_chains,tasks.convergence,tasks.results, tasks.avg_ccp, tasks.ccp_fit,tasks.gen_redraw_chain]);
if tasks.post_estimation
    % Load the model structural 
    load([model_data_folder model_name '/master'], 'model');
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end

    if ~isempty(strfind(model.model_name,'validation')), model_name = model.orig_name; end
    
    % Find the chain numbers that have been run * chains should be
    % numbered 1,2,...
    if ~isfield(options,'chain_nos')
        available_chains = dir([options.output_stub model_name '/chain_base*init.mat']);
        options.chain_nos=1:length(available_chains);
    end

    % Collect the variable statistics
    load([options.output_stub model_name '/chain_base_1--init.mat'],'chain_spec');
    % Set a default burn_in option
    chain_spec.burn_in = options.burn_in;

    if ~isempty(strfind(model.model_name,'validation')), model_name = model.model_name; end
end

% Generate a chain summary file
if tasks.summarize_base_chains | tasks.results 
    % Options for results
    options.keep_draws = false;
    options.diagnostics = false;
    
    % Append the chain information and save the chain summary file
    chain_summary = chain_output(model,chain_spec,options);
    filename = [options.output_stub model_name '/chain_base--summary.mat'];
    if exist(filename,'file')
        save(filename,'chain_summary','-append');
    else
        save(filename,'chain_summary');    
    end

    % Generate the results table  
    load([model_data_folder model_name '/master'], 'data','model'); % Load the data
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end
    conv_and_results(chain_summary,data,model,options);
end

% Convergence and results
if tasks.convergence | tasks.avg_ccp
    % Convergence
    options.keep_draws = true;
    options.diagnostics = true;
    % Get eta draws if needed
    options.eta_draws = all([tasks.avg_ccp model.donor_uh]); 

    % Load the data
    load([model_data_folder model_name '/master'], 'data','model');
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end
    
    % Append the chain information and save the chain summary file
    chains_appended = chain_output(model,chain_spec,options);
    if tasks.convergence
        conv_and_results(chains_appended,data,model,options);
    end
    if tasks.avg_ccp
        avg_ccp_base = est_avg_ccp(chains_appended,data,model);
        save([options.output_stub model_name '/chain_base--summary.mat'],'avg_ccp_base','-append');
    end
end 

% Draw eta conditional on the observed decisions
if tasks.gen_redraw_chain
    % Load data and model specification
    load([model_data_folder model_name '/master'],'data','model');
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end
    
    if ~model.donor_uh
        disp(['Skipping! tasks asks for redraw chains, but the ' ...
              'specification ' model_name ' does not have donor UH.']);
    else
        % Define chain single chain for each model
        chain_spec=struct();
        chain_spec.name = 'redraw_eta';
        chain_spec.draw_params = false;    
        chain_spec.thin = 10;  % Thinning constant -- add every thin-th draw to the chain
        chain_spec.NITER = 10000;  % # iterations per FILE
        if ~isfield(chain_spec,'chain_no')
            chain_spec.chain_no = 1;
        end

        % Options
        if ~isfield(options,'resumeAt')        
            options.resumeAt = 0;
        end
        if ~isfield(options,'total_files')        
            options.total_files = 10;
        end
    
        % Read the chain summary
    	if ~isempty(strfind(model.model_name,'validation')), model_name = model.orig_name; end
	load([options.output_stub model_name '/chain_base--summary.mat'],'chain_summary');
    	if ~isempty(strfind(model.model_name,'validation')), model_name = model.model_name; end
        
        % Define initial values
        init = struct();
        init.rng_state = 1000;
        init.theta = chain_summary.mu_theta;
        if model.donor_uh
            init.sigma_eta = chain_summary.mu_sigma_eta;  % sigma_eta is the VARIANCE of eta
        end
        if model.pt_uh
            init.beta = chain_summary.mu_beta';
        end

        priors = struct(); % Priors should not be specified    
                           % Run the chain
        gen_chains_fun(chain_spec,model,priors,init,data,options);
        eta_options = options; 
        eta_options.eta_draws = true;
        eta_chain = chain_output(model,chain_spec,eta_options);
        eta_chain.donor_id = data.donor_list;
        save([options.output_stub model_name '/' chain_spec.name '--summary.mat'],'-struct','eta_chain');   
        clear chain_spec;
    end
end


% Draw acceptance hazard pictures for model fit
if tasks.ccp_fit
    display(['Acceptance Rates:' model_name]);
    load([model_data_folder model_name '/master'],'data','model');
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end
    if model.pt_uh
      poo  % have not yet modified to accommodate patient UH
    end
    
    load([options.output_stub model_name '/chain_base--summary.mat'],'chain_summary');
    % Compute CCP fits by offer number
    ccp_fits = ccp_position_fit(data,model,chain_summary,options);
    % Store the options and date that generated the fit
    ccp_fits.gen_date = datetime('today');
    save([options.output_stub model_name '/chain_base--summary.mat'],'ccp_fits','-append');   
end

% CCP validation exercise -- should be called by run_model_validation.m
% Generates Table A.4. 'comparison' sample was used for development but is not reported in the paper
% Instead, the validation sample is compared to the full estimation sample
if tasks.validate_ccps

    display('Calculate MSE in and out of sample')

    % Validation dataset
    load([model_data_folder model_name '/master'],'data','model');
    data1 = data; model1 = model;

    % Comparison dataset
    load([model_data_folder model1.comparison_name '/master'],'data','model');
    data0 = data; model0 = model;
  
    % Estimation dataset
    load([model_data_folder model1.orig_name '/master'],'data','model');
  
    % Estimation results
    load([options.output_stub model1.orig_name '/chain_base--summary.mat'],'chain_summary');

    % reconcile specs if needed
    reconcile_specs

    if model.pt_uh, poo; end

    % Redraw eta for offers up to first acceptance (restricted to first 20 positions)
    options.ccp_fit.n_pos = 20;
    ccps0 = ccp_position_fit(data0,model0,chain_summary,options);
    ccps1 = ccp_position_fit(data1,model1,chain_summary,options); % validation sample
    ccps2 = ccp_position_fit(data,model,chain_summary,options);   % estimation sample
    eta_object.index0 = ccps0.index; eta_object.index1 = ccps1.index; eta_object.index2 = ccps2.index;
    eta_object.eta0 = ccps0.eta_redraws; eta_object.eta1 = ccps1.eta_redraws; eta_object.eta2 = ccps2.eta_redraws

    save([options.output_stub model_name '/out_of_sample_fit_objects.mat'],'eta_object','data0','data1','model0','model1');

    % Predicted CCPs and MSE
    fit_filepath = [options.results_folder '/ccp_model_validation.xls'];
    fit_table = compute_mse(data0,data1,data,eta_object,chain_summary.mu_theta,fit_filepath,model1.orig_name);
    fit_table
end

if tasks.value_function | tasks.projection_table | tasks.average_offer
    
    load([model_data_folder model_name '/master'],'data','model');
    if ~strcmp(model.model_name,model_name), fprintf('Check model_name \n'), keyboard; end
    
    % Load eta_draws
    if model.donor_uh || model.pt_uh
        load([options.output_stub model_name '/redraw_eta--summary.mat']);
    end
    
    if model.donor_uh
        eta.draws = eta_indiv_draws_trim;
        eta.donor_id = data.donor_list;
    else
        eta.draws = [];
    end
        
    if model.pt_uh
        beta.draws = beta_i_draws_trim;
        beta.wl_id_code = data.patient_list;
    else    
        beta.draws = [];
    end

% Load chain results
    load([options.output_stub model_name '/chain_base--summary.mat'],'chain_summary'); 
    
    % Load model files
    load(model.filenames.donors,'donors');
    load(model.filenames.patients,'patients');
    load(model.filenames.pt_history,'pt_history');
    load(model.filenames.offers,'offers');
    load([model_data_folder model_name '/master'],'data','model');
    if tasks.value_function || tasks.projection_table || tasks.average_offer, 
        selective_load = {'pt_hist_id','donor_id','blood_type',...
                          'a_mm','b_mm','dr_mm','geo','c_abo',...
                          'unacc','bins','days1','days2','days3'};
        spload = @(x) sparsedata(load(x,selective_load{:}),'pt_hist_id','donor_id','blood_type');
        abo_list = {'O','A','B','AB'};
        for abo = 1:4
            don_pat_pairs{abo}=spload([model.filenames.don_pat_pairs abo_list{abo}]);
        end
        % Override rho;
        if ~isempty(options.override_rho), model.rho = options.override_rho; end
        fun.continuation_value = @(pt_hist,pat,waittime,beta_i) continuation_value(waittime,don_pat_pairs,model,donors,pat,pt_hist,chain_summary,eta,beta_i,options);
        fun.average_offer      = @(pt_hist,pat,V_init) average_offer(don_pat_pairs,model,donors,pat,pt_hist,chain_summary,V_init,options.av_don_local);
    end
    

    % Override rho or select a subset of patients;
    pt_include = options.pt_include(data);
    if ~isempty(options.override_rho), model.rho = options.override_rho; end
    
    
    vname = options.vfsavename(model);
    tname = options.vfsavename(model,'typical_donor');
    
    if tasks.value_function
        % Run value function 
        if model.pt_uh
            for kk = 1:model.K_types  % calculate VF's for each patient, for each possible type
                beta_i = repmat(kk,data.N_patients,1);  % assign all patients one type
                [V(:,kk),gamma(:,kk),chi(:,:,kk),V_init_k] = value_function(don_pat_pairs,data,model,donors,patients,pt_history,offers,chain_summary,eta,beta_i,options,pt_include);
                V_init.pt_history = V_init_k.pt_history;  % same patient histories used for each type
                V_init.V(:,kk) = V_init_k.V;              % type-specific V's
            end
        else
            beta_i = [];  % no patient UH
            [V,gamma,chi,V_init]=value_function(don_pat_pairs,data,model,donors,patients,pt_history,offers,chain_summary,eta,beta_i,options,pt_include);
        end
        save(vname,'V','gamma','chi','V_init','pt_include','-v7.3');
    else
        load(vname,'V','gamma','chi','pt_include','V_init');
    end
    
    if tasks.average_offer
        [V_init, h]=fun.average_offer(pt_history,patients,V_init);
        if options.av_don_local, desc = 'distr_average_loc_don'; else desc = 'distr_average_don'; end
        saveas(h,[options.results_folder model_name '/figs/' desc '.png']); hold off; close(gcf);
        save(vname,'V','gamma','chi','V_init','pt_include','-v7.3');
    end
    
    if tasks.projection_table
        % Set options for the projection and run it. nameend prevents overwriting files when we include only a subset
        % of patients or override rho or when we use a different hazard function. 
        %make_projection_table runs the projection and writes the V and Gamma tables.     
        % Write the estimates table.
        make_estimates_table(chain_summary,data,[options.results_folder model_name '/Estimates.csv']);
        [proj_out]=make_projection_table(V(pt_include,:),chain_summary.mu_theta,chi(pt_include,:,:),model,options);
        save(options.vfsavename(model,'projection_table'),'-struct','proj_out');
        % Model fit by patient groups
        load('/proj/organs/common_output/structural/input/patient_abo','group') 
        ccp_pat_fit = ccp_patient_fit(data,model,chain_summary,donors,offers,pt_history,patients,group.groups,eta.draws,beta.draws,options);
        save([options.output_stub model_name '/chain_base--summary.mat'],'ccp_pat_fit','-append');
        % Gamma plots (Gamma: NPV of a transplant)
        load(vname); 
        % Pick representative donor, patient and offer (synthetic donor, patient offer with median characteristics)
        rep = median_types(don_pat_pairs,model,donors,patients,pt_history,chain_summary,eta,options);
        % Replace patient by the one that we picked for the plots in options.ref_pt_history
        rep.pt_history = pt_history(pt_history.wlreg_audit_id_code == options.ref_pt_history,:);
        rep.patient    = patients(patients.wl_id_code == rep.pt_history.wl_id_code,:);
        if ~model.pt_uh
            gamma_plots(data,model,donors,patients,pt_history,chain_summary,options,rep,fun);
        end
    end
    

end
