# Main Analysis for Organs


##A. Setup Instructions

   	a. Analysis requires installation of 
   		1. STATA version 14.2
   		2. MATLAB version R2019a
   		3. KNITRO version 12.0.0
   		4. Python version 2.7.5
   		5. GCC version 4.8.5

   	b. The code was run and tested on a 64-bit linux based machine.

	c. Create three directories and symbolic links in home, ~/
	        1. ~/organs-raw/raw-extract-4 
		   should point to the raw data obtained from the OPTN.		   
		2. ~/organs-star is a directory for processed data
		3. ~/organs-proj contains both code and output from the analysis. 
		   
	d. Clone the project repo into ~/organs-proj/<user>/
	   You need to enable Git Large File Storage (LFS). Download and 
	   installation instructions for Git LFS are here: git-lfs.github.com	   	    

	e. Copy common_output.zip from ~/organs-proj/<user>/ into ~/organs-proj/
	   	and unzip it to generate the output directory structure and 
		formatted excel files. 
	

------------------------------------------------------------------------

##B. Data

   	a. Obtaining original data files:

		The data are taken from the proprietary sources described in
		the Data Appendix. We will retain copies of the data until
		permitted by our Data Use Agreement with the Organ Procurement
		and Transplantation Network (OPTN). We will send OPTN a copy of 
		our replication archive if and when we are required to destroy 
		our dataset. Researchers interested in using our dataset should
		directly contact OPTN to obtain permission:

		https://optn.transplant.hrsa.gov/data/request-data/

		We will be happy to provide copies of our data to researchers
		with permission and a data use agreement with OPTN.




------------------------------------------------------------------------

##C. Directories

	a. Files in patient directory
		1. build_project.sh
			Runs all the main scripts in this repository and generates
			all analyses in the article. See below for a more detailed 
			walkthrough of this script.
		2. extract_script.sh
			Extract data from STAR CDs to raw-extract
		3. pull_tables_and_figures.sh
			Collect tables and figures 

	b. Folders (there is a README_[folder name].md file in each folder):

		1. data_preparation
		Clean raw data files and generate primary files for further 
		analysis. 

		2. mechanism 
		Scripts for simulating the mechanism

		3. reduced_form
		Generate reduced form analysis 

		4. structural_model
		Estimate structural model and generate counterfactual analysis

		5. tools
		Contains helper funtions used in data_preparation and reduced_form



------------------------------------------------------------------------

##D. Build Project

	a. To build the project, run build_project.sh. 

		1. build_project.sh calls separate scripts in the order 
		they should be run.

		2. The following two scripts needs to be run multiple times, each
		with different settings:
			i. structural_model/run_model_validation.m
			ii. structural_model/run_cf.m
		build_project.sh only calls them once. For replication, one should 
		follow the instruction in the beginning of these two scripts for a
		complete run

