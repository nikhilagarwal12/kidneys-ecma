function score = score_locl_fun(data_object,fun_local,fun_non_local)

% Load files
load(data_object.filenames.donors,'donors');

% Retrieve information
[ismem,where]= ismember(data_object.donor_id,donors.donor_id);
assert(all(ismem));
local_donor      = donors.opo_dsa_id(where) == 12;  % 12 is NYRT!
local_score      = fun_local(data_object);
score            = fun_non_local(data_object);

score(:,:,local_donor) = local_score(:,:,local_donor);


