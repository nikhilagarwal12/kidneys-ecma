// This file creates the current version of Table 1, Panel A
// Snapshots of the waitlist on supplied dates (`snapshot_dates'). Currently set up for January 1st, 2010 and January 1st, 2013 as in the paper.
// The only arguments are the values of local variables

program waitlist_snapshot_table

args make_waitlist_snapshot_table waitlist_snapshot_file kidpan_w_vars snapshot_dates snapshot_vars primary_folder sample_name output_folder

// Rows: means of variables (usually dummies) computed at column date for patients on the waiting list
// Columns: specific date

// Some pre-processing: take patient_history.dta and extract records that are relevant to some `snapshot_dates'

if `make_waitlist_snapshot_table' == 1 {
    use wl_id_code wlreg_audit_id cpra unos_cand_stat chg_date chg_time chg_till chg_ty on_expand* on_iexpand* using "`primary_folder'patient_history.dta", clear

    sort wl_id_code chg_date chg_time
    gen relevant_date = .
    gen ever_relevant = 0
    foreach date in `snapshot_dates'{
      gen relevant_`date' = chg_date < `date' & chg_till >= `date' & chg_ty != "D"  // define a variable for each snapshot date
      replace ever_relevant = ever_relevant + relevant_`date'
    }
    keep if ever_relevant > 0
    tempfile wlhistory
    save `wlhistory'

    // compute variables at specific dates
    tempfile snapshot_temp
    local append = 0

    foreach date in `snapshot_dates'{
        use `kidpan_w_vars', clear
        display `date'
        keep if init_date < `date' & (end_date >= `date' | end_date == .) // filter sample to those on the waiting list at `date'
        
        gen curr_days_on_list = `date' - init_date
        gen curr_years_on_list = curr_days_on_list / 365
        gen curr_age = init_age + 0.5 + curr_days_on_list/365  // guess of current age

        sysdir set PERSONAL ../../tools/code/   // to call compute_epts .ado file
        gen match_date = max(`date',init_date)
        compute_epts  // computing EPTS at the time the patient entered our sample, either at sample_start or when you joined the waiting list if after
        
        sysdir set PERSONAL ../../reduced_form/code/functions/
        rename epts_raw curr_epts
        drop diabetes epts_bottom_20 age_guess age_from_25
        replace years_on_dialysis = 0 if years_on_dialysis < 0  // could be negative if not on dialysis at init_date

        drop on_dialysis  // KIDPAN gives most recent update, which is not necessarily current for `date'
        gen on_dialysis = years_on_dialysis > 0

        // merge in wl_history for cpra, status, and accept ecd/iecd
        gen relevant_`date' = 1
        merge 1:m wl_id_code relevant_`date' using `wlhistory', keep(match master) keepusing(cpra unos_cand_stat_cd on_expand* on_iexpand*) nogenerate
        isid wl_id_code
        rename cpra curr_cpra
        gen curr_active_ki = unos_cand_stat < 4070 & unos_cand_stat >= 4000
        gen curr_inactive_ki = unos_cand_stat == 4099 | unos_cand_stat == 4999
        gen curr_active_kp = unos_cand_stat == 5010
        gen curr_inactive_kp = unos_cand_stat == 5099 | unos_cand_stat == 5999
        gen accepts_local_ecd = on_expand_donor
        gen accepts_import_ecd = on_iexpand_donor

      // construct collapse string -- only the first time
        if `append'~=1 {
          foreach var of varlist `snapshot_vars'{
            local string_mean = "`string_mean'" + " (mean) " + "`var'__mean = `var'"
            local string_sd = "`string_sd'" + " (sd) " + "`var'" + "__sd = `var'"
          }
        }
          
        // Construct means and standard deviations
        keep `snapshot_vars' wl_id_code
       
        collapse (count) wl_id_code `string_mean' `string_sd' `string_median'
        gen snapshot_date = `date'
        rename wl_id_code count
        order snapshot_date count
        if `append'==1{
          append using `snapshot_temp'
        }
        save `snapshot_temp', replace
        local append = 1  // append from now on, just not the first time
    }
    save `waitlist_snapshot_file', replace
}

use `waitlist_snapshot_file', clear

sort snapshot_date
xpose, clear varname

*** Get Snapshot Date
rename v1 Jan01_2010
rename v2 Jan01_2013

label var Jan01_2010 "January 1st, 2010"
label var Jan01_2013 "January 1st, 2013"


** Parse _varname
gen variable_name = substr(_varname,1,strpos(_varname,"__")-1)
gen statistic = substr(_varname,strpos(_varname,"__")+2,strlen(_varname)-strpos(_varname,"__"))
replace variable_name = _varname if variable_name == ""
replace statistic = _varname if statistic == "napshot_date"
replace statistic = "mean" if statistic == "ount"

** Sample Order
gen sam_order = 1 if variable_name == "count"
replace sam_order = 2 if variable_name == "snapshot_date"
replace sam_order = 3 if variable_name == "curr_years_on_list"
replace sam_order = 4 if variable_name == "init_on_dialysis"
replace sam_order = 5 if variable_name == "years_on_dialysis"
replace sam_order = 6 if variable_name == "prior_transplant"
replace sam_order = 7 if variable_name == "curr_age"
replace sam_order = 8 if variable_name == "curr_cpra"
replace sam_order = 9 if variable_name == "diab_pt"
replace sam_order = 10 if variable_name == "init_bmi_calc"
replace sam_order = 11 if variable_name == "tot_serum_album"


*** Name Variable
replace variable_name="Current Age" if variable_name == "curr_age"
replace variable_name="Years on List" if variable_name == "curr_years_on_list"
replace variable_name="Calculated Panel Reactive Antibodies (CPRA)" if variable_name == "curr_cpra" 
replace variable_name="Diabetic Patient" if variable_name == "diab_pt"
replace variable_name="On Dialysis at Listing" if variable_name == "init_on_dialysis"
replace variable_name="Years on Dialysis" if variable_name == "years_on_dialysis"
replace variable_name="Prior Transplant" if variable_name == "prior_transplant"
replace variable_name="Number of Patients" if variable_name == "count"
replace variable_name="Snapshot Date" if variable_name == "snapshot_date"
replace variable_name="Body Mass Index (BMI) at Arrival" if variable_name == "init_bmi_calc"
replace variable_name="Total Serum Albumin" if variable_name == "tot_serum_album"

drop _varname
reshape wide Jan*, i(variable_name) j(statistic) string

order variable_name *mean *sd 
order variable_name *2010* *2013*
sort sam_order

drop *snapshot_date sam_order
drop if variable_name == "Snapshot Date"
export excel using "`output_folder'summary_tables_`sample_name'.xls", sheet("Waitlist Snapshots") firstrow(varlabels) sheetreplace

end
