// Predict positive crossmatch rate among positive crossmatch and accepted offers
// This script generates Table A.1: Positive Crossmatch Model


****************************************
* Table A.1: Positive Crossmatch Model
****************************************

clear
set more off
eststo clear

*-----------------------------------
* DATA LOCATION
*-----------------------------------
local data = "~/organs-star/primary_files/nyrt/"
local output = "~/organs-proj/common_output/reduced_form/"

*-----------------------------------
* GENERATE COVARIATE
*-----------------------------------
use `data'offers, clear
keep if c_accept == "Yes"

// dependent variables
gen positive_crossmatch = refusal_id==810

// mismatches
gen hla01 = hlamis==0 | hlamis==1
gen hla23 = hlamis==2 | hlamis==3
gen dr0 = drmis==0
gen cpra_hla01 = cpra*hla01
gen cpra_hla23 = cpra*hla23
local basic_vars = "cpra hla01 hla23 dr0 cpra_hla01 cpra_hla23"

// CPRA vars
gen cpra_0 = cpra==0
gen cpra_gt_p8 = (cpra - .795)*(cpra > .795)
local cpra_ccp = "cpra_0 cpra_gt_p8"

// dialysis time controls
gen log_dialysis_time_at_init = log(min(max(init_date - dialysis_date,1)/365,5000))
gen log_dialysis_time_at_init_gt5 = (log_dialysis_time_at_init - log(5))*(log_dialysis_time_at_init > log(5))
local dialysis_aug = "log_dialysis_time_at_init*"

// age controls
gen age_gt_35 = (init_age - 35)*(init_age > 35)
local age_ccp = "init_age age_gt_35"

// variable labels
label var hla01 "0 or 1 HLA Mismatches"
label var hla23 "2 or 3 HLA Mismatches"
label var dr0 "0 DR Mismatches"
label var cpra_hla01 "CPRA * (0 or 1 HLA Mismatches)"
label var cpra_hla23 "CPRA * (2 or 3 HLA Mismatches)"
label var cpra_0 "CPRA = 0"
label var cpra_gt_p8 "CPRA - 0.8 if CPRA > 0.8"
label var log_dialysis_time_at_init "Log Dialysis Time at Registration (Years)"
label var log_dialysis_time_at_init_gt5 "Log Dialysis Time at Registration * Over 5 Years"
label var init_age "Patient Age at Registration (Years)"
label var age_gt_35 "Age at Registration - 35 if Age > 35"
label var cpra "CPRA"

*-----------------------------------
* REGRESSION
*-----------------------------------
probit positive_crossmatch `basic_vars' `cpra_ccp' `dialysis_aug' `age_ccp'

*-----------------------------------
* EXPORT TO EXCEL
*-----------------------------------
local suffix = "_rich"

// export coefficient only version
// need for structural model later
esttab using `output'positive_crossmatch_coefs_esttab`suffix'.csv, not noobs nostar nomtitles replace
import delimited using `output'positive_crossmatch_coefs_esttab`suffix'.csv, delimiter(",") clear
drop if _n<=2
rename v1 varname
rename v2 coef
replace varname = subinstr(varname,"_","",.)
replace varname = subinstr(varname,`"""',"",.)
replace coef = subinstr(coef,`"""',"",.)
replace varname = subinstr(varname,"=","",.)
replace coef = subinstr(coef,"=","",.)
export delimited using `output'positive_crossmatch_coefs`suffix'.csv, delimiter(",") noquote replace
cap shell rm `output'positive_crossmatch_coefs_esttab`suffix'.csv

// format and restore version with both coeffs and se to xls
esttab using `output'positive_crossmatch_coefs_se_esttab`suffix'.csv, se r2 label nostar replace
import delimited using `output'positive_crossmatch_coefs_se_esttab`suffix'.csv, delimiter(",") clear
rename v1 varname
rename v2 est
replace varname = subinstr(varname,"_","",.)
replace varname = subinstr(varname,`"""',"",.)
replace est = subinstr(est,`"""',"",.)
replace varname = subinstr(varname,"=","",.)
replace est = subinstr(est,"=","",.)
export excel using `output'positive_crossmatch_coefs.xls, sheet("raw, `suffix'") sheetreplace
cap shell rm `output'positive_crossmatch_coefs_se_esttab`suffix'.csv


