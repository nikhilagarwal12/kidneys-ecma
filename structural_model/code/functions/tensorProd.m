function [new_struc,col_no]=tensorProd(struc,Pn,name,mode)

if nargin<4, mode='tensor'; end

if strcmp(mode,'tensor')
col_no = 0;
for ii = 1:size(struc.Pn,2)
	for jj = 1:size(Pn,2)
		this_vector = struc.Pn(:,ii).*Pn(:,jj);
		col_no = col_no + 1;
		new_struc.Pn(:,col_no) = this_vector;
		new_struc.colheaders{col_no} = [struc.colheaders{ii} name num2str(jj)];
	end
end
end

if strcmp(mode,'poly')
col_no = 1;
for ii = 1:size(struc.Pn,2)
    max_order = size(Pn,2) - struc.order(ii);
	for jj = 1:max_order
        this_vector = struc.Pn(:,ii).*Pn(:,jj);
		col_no = col_no + 1;
		new_struc.Pn(:,col_no) = this_vector;
		new_struc.colheaders{col_no} = [struc.colheaders{ii} name num2str(jj)];
        new_struc.order(col_no) = struc.order(ii) + jj;
	end
end
end