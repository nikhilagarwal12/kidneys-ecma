// This script takes the partially processed offers.dta primary file and adds donor and match OPO dsa and region id's

program donor_and_match_opo_dsa_ids

preserve

keep donor_id match_id_code donor_opo match_opo
duplicates drop
isid donor_id match_id_code // should be unique donor and match opo's for each match run

rename donor_opo opo_id
merge m:1 opo_id using ../../data_preparation/ctr_dsa_def/opo_ctr_id_bridge, keepusing(listing_ctr_dsa_id region_id)
keep if _merge == 3
drop _merge
drop opo_id
rename listing_ctr_dsa_id donor_opo_dsa_id
rename region_id donor_opo_region_id

rename match_opo opo_id
merge m:1 opo_id using ../../data_preparation/ctr_dsa_def/opo_ctr_id_bridge, keepusing(listing_ctr_dsa_id region_id)
keep if _merge == 3
drop _merge
drop opo_id
rename listing_ctr_dsa_id match_opo_dsa_id
rename region_id match_opo_region_id

keep donor_id match_id_code donor_opo_dsa_id donor_opo_region_id match_opo_dsa_id match_opo_region_id

tempfile donor_and_match_opo
save `donor_and_match_opo', replace

restore

merge m:1 donor_id match_id_code using `donor_and_match_opo'
drop _merge

end
