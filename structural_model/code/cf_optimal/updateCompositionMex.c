#include "mex.h"
#include "matrix.h"

#include "common.h"

#include <stdint.h>
#include <string.h>
#include <math.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  double *q = (double*)mxGetPr(prhs[0]);
  double slack;
  if (nrhs < 4) {
    slack = 0.0;
  } else {
    slack = mxGetScalar(prhs[3]);
  }
  double *sigma = (double*)mxGetPr(mxGetField(prhs[2], 0, "sigma"));
  double *arrivals = (double*)mxGetPr(mxGetField(prhs[2], 0, "arrivals"));
  double *mu = (double*)mxGetPr(mxGetField(prhs[2], 0, "mu"));
  double *prCompat = (double*)mxGetPr(mxGetField(prhs[2], 0, "pr_compat"));
  double *deltaX_h = (double*)mxGetPr(mxGetField(prhs[2], 0, "delta_x_h"));
  double *lhOverK = (double*)mxGetPr(mxGetField(prhs[2], 0, "lambdah_over_K"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[2], 0, "mu"));
  const unsigned long nPatients = sizeArray[0];
  const unsigned long nTime = sizeArray[1];
  const unsigned long nDonors = sizeArray[2];
  const unsigned long nPT = nPatients*nTime;

  bool firstBest = false;
  const mxArray* mx_field = mxGetField(prhs[2], 0, "first_best");
  if (mx_field != NULL) {
    if (((uint64_t)(mxGetScalar(mx_field)))) {
      firstBest = true;
    }
  }
  double *out;
  out = (double*)calloc(nPT, sizeof(double));
  if (mxIsEmpty(prhs[1]) || firstBest) {
    for (unsigned long i = 0, index4 = 0, index2 = 0; i < nDonors; ++i, index4 += nPatients) {
      for (unsigned long j = 0, index1 = 0; j < nTime; ++j) {
        for (unsigned long k = 0, index3 = index4; k < nPatients; ++k, ++index2, ++index1, ++index3) {
          out[index1] += q[index2]*prCompat[index3];
        }
      }
    }
    for (unsigned long j = 0; j < nPT; ++j) {
      out[j] = exp(-deltaX_h[j]-lhOverK[j]*out[j]);
    }
  } else {
    double *V = (double*)mxGetPr(prhs[1]);
    for (unsigned long i = 0, index4 = 0, index2 = 0; i < nDonors; ++i, index4 += nPatients) {
      for (unsigned long j = 0, index1 = 0; j < nTime; ++j) {
        for (unsigned long k = 0, index3 = index4; k < nPatients; ++k, ++index2, ++index1, ++index3) {
          const double z = (mu[index2] - V[index1])/sigma[k];
          out[index1] += q[index2]*normCdf(z)*prCompat[index3];
        }
      }
    }
    for (unsigned long j = 0; j < nPT; ++j) {
      out[j] = exp(-deltaX_h[j]-lhOverK[j]*out[j]);
    }
  }
  // Create and compute p
  plhs[0] = mxCreateDoubleMatrix(nPatients, nTime, mxREAL);
  double *p = (double*)mxGetPr(plhs[0]);
  memcpy(p, arrivals, nPatients*sizeof(double));
  for (unsigned long i = nPatients, j = 0; i < nPT; ++i, ++j) {
    p[i] = arrivals[i] + p[j]*(out[j]+slack);
  }
  free(out);
}
