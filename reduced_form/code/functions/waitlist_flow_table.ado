// Waiting list inflows and outflows (Table 1, Panel B)
// The only arguments are the values of local variables

program waitlist_flow_table

args make_waitlist_flow_table waitlist_flow_file sample_start_date sample_end_date kidpan_w_vars flow_vars sample_name output_folder

if `make_waitlist_flow_table' == 1 {
    
    
    // Outflows by year and type
    use `kidpan_w_vars', clear
    replace end_year = year(end_date)
    keep if end_year != . & remove_reason~="" & end_date <= `sample_end_date' & end_date >= `sample_start_date'
    sort end_year remove_reason
    collapse (count) wl_id_code , by(end_year remove_reason)
    assert remove_reason~=""
    rename remove_reason flows
    rename end_year year
    rename wl_id_code count
    tempfile outflow_year_reason
    save `outflow_year_reason', replace

    // Inflows by year
    use `kidpan_w_vars', clear
    replace init_year = year(init_date)
    keep if init_year != . & init_date <= `sample_end_date' & init_date >= `sample_start_date'
    collapse (count) wl_id_code , by(init_year)
    rename init_year year
    gen flows = "Inflow"
    rename wl_id_code count
    append using `outflow_year_reason'
    save `outflow_year_reason', replace

    sort flows year
    order flows year count

    save `waitlist_flow_file', replace
}

use `waitlist_flow_file', replace
gen sam_order = 1 if flows == "Inflow"
replace sam_order = 2 if flows == "Deceased Donor Transplant"
replace sam_order = 3 if flows == "Received Live Donor Transplant"
replace sam_order = 4 if flows == "Died or Too Sick to Transplant"
replace sam_order = 5 if flows == "Departed for Other Reason"
sort sam_order year
drop sam_order

export excel using "`output_folder'summary_tables_`sample_name'.xls", sheet("Waitlist Flows") firstrow(varlabels) sheetreplace

end
