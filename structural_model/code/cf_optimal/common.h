#include <math.h>
#include <string.h>

#define GAUSS_PRE (M_2_SQRTPI/M_SQRT2/2.0)
#define MH (-0.5)
double normPdf(const double p) {
  return GAUSS_PRE*exp(MH*p*p);
}

#define M_1_SQRT2PI (M_2_SQRTPI * M_SQRT1_2 / 2.0)
#define SQRT32 (4.0 * M_SQRT2)
#define GSL_DBL_EPSILON 2.2204460492503131e-16
#define GAUSS_EPSILON  (GSL_DBL_EPSILON / 2)
#define GAUSS_XUPPER (8.572)
#define GAUSS_XLOWER (-37.519)
#define GAUSS_SCALE (16.0)
static double get_del (double x, double rational) {
  double xsq = 0.0;
  double del = 0.0;
  double result = 0.0;

  xsq = floor (x * GAUSS_SCALE) / GAUSS_SCALE;
  del = (x - xsq) * (x + xsq);
  del *= 0.5;
  result = exp (-0.5 * xsq * xsq) * exp (-1.0 * del) * rational;
  return result;
}
static double gauss_small (const double x) {
  unsigned int i;
  double result = 0.0;
  double xsq;
  double xnum;
  double xden;

  const double a[5] = {
    2.2352520354606839287,
    161.02823106855587881,
    1067.6894854603709582,
    18154.981253343561249,
    0.065682337918207449113 };
  const double b[4] = {
    47.20258190468824187,
    976.09855173777669322,
    10260.932208618978205,
    45507.789335026729956 };

  xsq = x * x;
  xnum = a[4] * xsq;
  xden = xsq;
  for (i = 0; i < 3; i++) {
    xnum = (xnum + a[i]) * xsq;
    xden = (xden + b[i]) * xsq;
  }
  result = x * (xnum + a[3]) / (xden + b[3]);
  return result;
}

/*
 * Normal cdf for 0.66291 < fabs(x) < sqrt(32).
 */
static double gauss_medium (const double x) {
  unsigned int i;
  double temp = 0.0;
  double result = 0.0;
  double xnum;
  double xden;
  double absx;

  const double c[9] = {
    0.39894151208813466764,
    8.8831497943883759412,
    93.506656132177855979,
    597.27027639480026226,
    2494.5375852903726711,
    6848.1904505362823326,
    11602.651437647350124,
    9842.7148383839780218,
    1.0765576773720192317e-8 };
  const double d[8] = {
    22.266688044328115691,
    235.38790178262499861,
    1519.377599407554805,
    6485.558298266760755,
    18615.571640885098091,
    34900.952721145977266,
    38912.003286093271411,
    19685.429676859990727 };

  absx = fabs (x);
  xnum = c[8] * absx;
  xden = absx;
  for (i = 0; i < 7; i++) {
    xnum = (xnum + c[i]) * absx;
    xden = (xden + d[i]) * absx;
  }
  temp = (xnum + c[7]) / (xden + d[7]);
  result = get_del (x, temp);
  return result;
}

/*
 * Normal cdf for 
 * {sqrt(32) < x < GAUSS_XUPPER} union { GAUSS_XLOWER < x < -sqrt(32) }.
 */
static double gauss_large (const double x) {
  int i;
  double result;
  double xsq;
  double temp;
  double xnum;
  double xden;
  double absx;

  const double p[6] = {
    0.21589853405795699,
    0.1274011611602473639,
    0.022235277870649807,
    0.001421619193227893466,
    2.9112874951168792e-5,
    0.02307344176494017303 };
  const double q[5] = {
    1.28426009614491121,
    0.468238212480865118,
    0.0659881378689285515,
    0.00378239633202758244,
    7.29751555083966205e-5 };

  absx = fabs (x);
  xsq = 1.0 / (x * x);
  xnum = p[5] * xsq;
  xden = xsq;
  for (i = 0; i < 4; i++) {
    xnum = (xnum + p[i]) * xsq;
    xden = (xden + q[i]) * xsq;
  }
  temp = xsq * (xnum + p[4]) / (xden + q[4]);
  temp = (M_1_SQRT2PI - temp) / absx;
  result = get_del (x, temp);
  return result;
}
double normCdf(const double x) {
  double result;
  double absx = fabs (x);
  if (absx < GAUSS_EPSILON) {
    result = 0.5;
    return result;
  } else {
    if (absx < 0.66291) {
      result = 0.5 + gauss_small(x);
      return result;
    } else { 
      if (absx < SQRT32) {
        result = gauss_medium(x);
        if (x > 0.0) {
          result = 1.0 - result;
        }
        return result;
      } else {
        if (x > GAUSS_XUPPER) {
          result = 1.0;
          return result;
        } else { 
          if (x < GAUSS_XLOWER) {
            result = 0.0;
            return result;
          } else {
            result = gauss_large(x);
            if (x > 0.0) {
              result = 1.0 - result;
            }
          }
        }
      }
    }
  }
  return result;
}
void COO_toCSR(const long nRow, const long nnz, \
    const long* const rowIndex, const long* const colIndex, \
    const double* const value, size_t* rowP, size_t* colIndices, \
    double *nonZeros) {
  for (unsigned long i = 0; i < nRow; ++i) {
   rowP[i] = 0;
  }
  for (unsigned long i = 0; i < nnz; ++i) {
    rowP[rowIndex[i]]++;
  }
  for (long i = 0, cumsum = 0; i < nRow; ++i) {
    long temp = rowP[i];
    rowP[i] = cumsum;
    cumsum += temp;
  }
  rowP[nRow] = nnz;
  for (unsigned long i = 0; i < nnz; ++i) {
    const long row = rowIndex[i];
    const long dest = rowP[row];
    colIndices[dest] = colIndex[i];
    nonZeros[dest] = value[i];
    rowP[row]++;
  }
  for (long i = 0, last = 0; i <= nRow; ++i) {
    long temp = rowP[i];
    rowP[i] = last;
    last = temp;
  }
}
void COO_toCSC(const long nRow, const long nnz, \
    const long* const rowIndex, const long* const colIndex, \
    const double* const value, size_t* colP, size_t* rowIndices, \
    double *nonZeros) {
  COO_toCSR(nRow, nnz, colIndex, rowIndex, value, colP, rowIndices, nonZeros);
}

/* Following code from gsl repo (https://github.com/ampl/gsl) */
static double rat_eval (const double a[], const size_t na,
          const double b[], const size_t nb, const double x) {
  size_t i, j;
  double u, v, r;
  u = a[na - 1];
  for (i = na - 1; i > 0; i--) {
    u = x * u + a[i - 1];
  }
  v = b[nb - 1];
  for (j = nb - 1; j > 0; j--) {
   v = x * v + b[j - 1];
  }
  r = u / v;
  return r;
}

static double smallInv (double q) {
  const double a[8] = { 3.387132872796366608, 133.14166789178437745,
    1971.5909503065514427, 13731.693765509461125,
    45921.953931549871457, 67265.770927008700853,
    33430.575583588128105, 2509.0809287301226727
  };
  const double b[8] = { 1.0, 42.313330701600911252,
    687.1870074920579083, 5394.1960214247511077,
    21213.794301586595867, 39307.89580009271061,
    28729.085735721942674, 5226.495278852854561
  };
  double r = 0.180625 - q * q;
  double x = q * rat_eval(a, 8, b, 8, r);
  return x;
}

static double intermediateInv (double r) {
  const double a[] = { 1.42343711074968357734, 4.6303378461565452959,
    5.7694972214606914055, 3.64784832476320460504,
    1.27045825245236838258, 0.24178072517745061177,
    0.0227238449892691845833, 7.7454501427834140764e-4
  };
  const double b[] = { 1.0, 2.05319162663775882187,
    1.6763848301838038494, 0.68976733498510000455,
    0.14810397642748007459, 0.0151986665636164571966,
    5.475938084995344946e-4, 1.05075007164441684324e-9
  };
  double x = rat_eval(a, 8, b, 8, (r - 1.6));
  return x;
}

static double tailInv (double r) {
  const double a[] = { 6.6579046435011037772, 5.4637849111641143699,
    1.7848265399172913358, 0.29656057182850489123,
    0.026532189526576123093, 0.0012426609473880784386,
    2.71155556874348757815e-5, 2.01033439929228813265e-7
  };
  const double b[] = { 1.0, 0.59983220655588793769,
    0.13692988092273580531, 0.0148753612908506148525,
    7.868691311456132591e-4, 1.8463183175100546818e-5,
    1.4215117583164458887e-7, 2.04426310338993978564e-15
  };
  double x = rat_eval(a, 8, b, 8, (r - 5.0));
  return x;
}

double invCDF(const double P) {
  double r, x, pp;
  double dP = P - 0.5;
  if (P == 1.0) {
    return (1.0/0.0); // Returns positive infinity
  } else if (P == 0.0) {
    return (-1.0/0.0); // Returns negative infinity
  }
  if (fabs(dP) <= 0.425) {
    x = smallInv(dP);
    return x;
  }
  pp = (P < 0.5) ? P : 1.0 - P;
  r = sqrt(-log(pp));
  if (r <= 5.0) {
    x = intermediateInv(r);
  } else {
    x = tailInv(r);
  }
  if (P < 0.5) {
    return -x;
  } else {
    return x;
  }
}
