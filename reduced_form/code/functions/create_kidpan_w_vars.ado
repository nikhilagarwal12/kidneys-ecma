// This function creates a patient table with additional variables for summary statistics


program create_kidpan_w_vars

args sample_start_date sample_end_date kidpan_w_vars primary_folder sim_offers_basic


// ************************************************ //
// Merge with Kidpan
use "`primary_folder'kidpan", clear

keep if end_date >= `sample_start_date' & init_date <= `sample_end_date'
drop if init_stat > 5000 & init_stat < 9000  // drop any patients waiting for pancreas (should be omitted by primary files code)

// Permanent characteristics
gen abo_pt = abo
replace abo_pt = subinstr(abo_pt,"1","",.)
replace abo_pt = subinstr(abo_pt,"2","",.)

gen race_pt = "White" if ethcat == 1
replace race_pt = "Black" if ethcat == 2
replace race_pt = "Hispanic" if ethcat == 4
replace race_pt = "Asian" if ethcat == 5
replace race_pt = "Other" if ethcat > 5

// primary diagnosis
gen pdiag_glomerular_sclerosis = dgn_tcr == 3006
gen pdiag_polycystic_kidneys = dgn_tcr == 3008
gen pdiag_graft_failure = dgn_tcr == 3037
gen pdiag_hypertens_nephroscler = dgn_tcr == 3040
gen pdiag_type1_diabetes = dgn_tcr == 3069
gen pdiag_type2_diabetes = dgn_tcr == 3070
gen pdiag_other = 1 - pdiag_glomer - pdiag_polycyst - pdiag_type1 - pdiag_type2 - pdiag_graft - pdiag_hypertens

// other illnesses
gen has_perip_vasc_disease = perip_vasc == "Y"
gen has_prev_malignancy = malig_tcr_ki == "Y"

// diabetes
gen diab_pt = diab>1 & diab<6     // diabetic patient, any type
gen diab_type1_pt = diab==2     // type 1 diabetic
gen diab_type2_pt = diab==3     // type 2 diabetic
gen diab_typeoth_pt = diab_pt - diab_type1_pt - diab_type2_pt     // type other/unknown diabetic

// gender
gen male_pt = gender=="M"

// citizenship status
gen us_citizen = citizenship == 1
gen us_resident = citizenship == 2 | citizenship == 3

// education is available for everyone!
gen educ_some_high_school = education >= 3 & education <= 6  // some high school education
gen educ_some_college = education >= 4 & education <= 6  // some college
gen educ_college_degree = education >= 5 & education <= 6  // college degree. Associate or bachelor's
gen educ_unknown_or_na = education > 6

// So is projected primary payment source. Need coding for this and education!
gen payment_private = pri_payment_tcr_ki == 1
gen payment_public = (pri_payment_tcr_ki > 1 & pri_payment_tcr_ki < 8) | pri_payment_tcr_ki == 13 | pri_payment_tcr_ki == 14
gen payment_other = 1 - payment_private - payment_public

// use on_expand_donor, on_iexpand_donor from wlhistory. on_dialysis from kidpan. num_prev_transplants gives whether prior transplants!
gen prior_transplant = num_prev_tx > 0


// other patient variables are time-specific: compute at init_date and end_date (if patient exited)

// EPTS at listing - init_epts, init_on_dialysis, end_years_on_dialysis
sysdir set PERSONAL ../../tools/code/   // to call compute_epts .ado file
gen match_date = max(`sample_start_date',init_date)
compute_epts  // computing EPTS at the time the patient entered our sample, either at sample_start or when you joined the waiting list if after
rename epts_raw c_init_epts
rename years_on_dialysis init_years_on_dialysis   // will be used later
replace init_years_on_dialysis = . if init_years_on_dialysis < 0   // only those on dialysis should contribute to the average time
gen init_on_dialysis = init_years_on_dialysis > 0 & init_years_on_dialysis != .
drop age_guess age_from_25 diabetes epts_bottom_20 prior_organ_transplant match_date

// EPTS at end_date - end_epts, end_on_dialysis, end_years_on_dialysis
gen match_date = min(`sample_end_date',end_date)
compute_epts  // computing EPTS at the time the patient entered our sample, either at sample_start or when you joined the waiting list if after
rename epts_raw c_end_epts
rename years_on_dialysis end_years_on_dialysis   // will be used later
replace end_years_on_dialysis = . if end_years_on_dialysis < 0   // only those on dialysis should contribute to the average time
gen end_on_dialysis = end_years_on_dialysis > 0 & end_years_on_dialysis != .
drop age_guess age_from_25 diabetes epts_bottom_20 prior_organ_transplant match_date

sysdir set PERSONAL ../../reduced_form/code/functions/

gen end_age = init_age + 0.5 + floor(end_date - init_date)/365.25

// status at listing
gen init_active_ki = init_stat <= 4060
gen init_inactive_ki = init_stat == 4099 | init_stat == 4999

// status at end date
gen end_active_ki = end_stat <= 4060
gen end_inactive_ki = end_stat == 4099 | end_stat == 4999

// Total time on list
gen end_years_on_waitlist = (end_date - init_date)/365.25

// "functional status", i.e. how healthy the patient is
gen func_stat_listing = mod(func_stat_tcr-10,100)    // measured at LISTING: coding is 0-10-20-...-90, with 90 the healthiest
gen func_stat_tx = mod(func_stat_trr-10,100)    // measured at TRANSPLANT: same coding


// some variables are only available for those transplanted
// BMI at listing is only available for those transplanted - bmi_tcr.
// Whether working for income (transplanted patients only)
gen working_for_income = work_income_tcr == "Y"

// previous pregnancy
gen previous_pregnancy = prev_preg >= 1 & prev_preg <= 6

gen remove_reason = "Departed for Other Reason" if rem_cd ~=.
replace remove_reason = "Deceased Donor Transplant" if rem_cd == 4 | rem_cd ==18 | (rem_cd == 14 & don_ty == "C")
replace remove_reason = "Died or Too Sick to Transplant" if rem_cd == 8 | rem_cd == 13 | rem_cd == 21
replace remove_reason = "Received Live Donor Transplant" if rem_cd == 15 | (rem_cd == 14 & don_ty == "L")

    // patients who transferred to another center within NYRT have not departed
    gen other_center_date = end_date if rem_cd == 7
    bys pt_code: egen max_end_date = max(end_date)
    gen other_center_nyrt = max_end_date > other_center_date
    replace remove_reason = "" if rem_cd == 7 & other_center_nyrt==1
    drop max_end_date other_center_date


// Recode variables discrete <- continuous

// discrete versions of continuous variables
gen age_cat = "0-17" if init_age <= 17
replace age_cat = "18-34" if init_age > 17 & init_age <= 34
replace age_cat = "35-49" if init_age > 34 & init_age <= 49
replace age_cat = "50-64" if init_age > 49 & init_age <= 64
replace age_cat = "65+" if init_age > 64

gen cpra_cat = "0" if init_cpra == 0 | init_cpra == .      // there are some missing init_cpra's
replace cpra_cat = "1-79" if init_cpra > 0 & init_cpra <= 79
replace cpra_cat = "80-94" if init_cpra >= 80 & init_cpra < 95
replace cpra_cat = "95-98" if init_cpra >= 95 & init_cpra < 99
replace cpra_cat = "99-100" if init_cpra == 99 | init_cpra == 100

gen init_cpra_80_98 = init_cpra>= 80 & init_cpra <=98 if init_cpra ~=.
gen init_cpra_99_100 = init_cpra>= 99 if init_cpra ~=.

replace init_cpra = init_cpra / 100
replace init_cpra = 0 if init_cpra == .
replace end_cpra = end_cpra / 100
assert ~missing(end_cpra) & ~missing(init_cpra)

// Using 2015-12-31 cutoffs for EPTS category
gen epts_cat = "0_20p" if c_init_epts < 1.49962696783025
replace epts_cat = "20_40p" if c_init_epts >= 1.49962696783025 & c_init_epts < 2.02457437173598
replace epts_cat = "40_60p" if c_init_epts >= 2.02457437173598 & c_init_epts < 2.37525033942218
replace epts_cat = "60_80p" if c_init_epts >= 2.37525033942218 & c_init_epts < 2.70924029803836
replace epts_cat = "80_100p" if c_init_epts >= 2.70924029803836 & c_init_epts != .

// continuous versions of discrete variables
foreach type in A B AB O{
  gen abo_pt_`type' = (abo_pt=="`type'")
}
foreach race in White Black Hispanic Asian{
  gen race_pt_`race' = (race_pt == "`race'")
}

gen geo_tx = "National" if donor_id != .
replace geo_tx = "Regional" if donor_id != . & listing_ctr_region_id == opo_region_id
replace geo_tx = "Local" if donor_id != . & listing_ctr_dsa_id == opo_dsa_id

gen init_year = year(init_date)  // date problem still present
replace end_date = `sample_end_date' if init_date > end_date | end_date < `sample_start_date' // some end_dates are from previous transplants
gen end_year = year(end_date)
replace end_year = . if end_date == `sample_end_date'  // if you're still on the list after the sample period
gen tx_year = year(tx_date)
gen death_year = year(death_date)

tab init_year
tab end_year
tab tx_year
tab death_year

gen generic = "All"
gen all = 1
gen deceased_donor = (don_ty == "C")  // for sample filtering

// What happened to the patient by the first year
gen one_year_tx = (tx_date - init_date) < 365  // transplanted within a year
gen one_year_tx_live = one_year_tx*(don_ty == "L")
gen one_year_tx_dec = one_year_tx*(don_ty == "C")
gen one_year_died = (death_date - init_date) < 365
gen one_year_waiting = (end_date - init_date) > 365 | rem_cd == .

// What happened to the patient by the second year
gen two_year_tx = (tx_date - init_date) < 365*2  // transplanted within a year
gen two_year_tx_live = two_year_tx*(don_ty == "L")
gen two_year_tx_dec = two_year_tx*(don_ty == "C")
gen two_year_died = (death_date - init_date) < 365*2
gen two_year_waiting = (end_date - init_date) > 365*2 | rem_cd == .


********************* Offers ************************************
// Offers received
// Note that offers are automatically truncated at the end of the sample period, even if it precedes the end of the offer data
tempfile offer_count

preserve
merge 1:m wl_id_code using "`sim_offers_basic'", keepusing(screened match_date) keep(match master)

// Count offers
gen offer_count_all = _merge==3 & (match_date >= `sample_start_date' & match_date <= `sample_end_date') & match_date >= init_date  // all offers
drop _merge
gen offer_count = offer_count_all*(screened == 0)   // unscreened offers
gen one_year_offer_count = offer_count & (match_date - init_date) < 365
gen two_year_offer_count = offer_count & (match_date - init_date) < 365*2
gen one_year_all_offer_count = offer_count_all & (match_date - init_date) < 365
gen two_year_all_offer_count = offer_count_all & (match_date - init_date) < 365*2

collapse (sum) *offer_count, by(wl_id_code)

gen one_year_any_offer = one_year_offer_count > 0 
gen two_year_any_offer = two_year_offer_count > 0

save `offer_count', replace

restore

// Merge back
merge 1:1 wl_id_code using `offer_count', assert(match) nogen

********************* Active Days ************************************
// Active Days
tempfile active_days

preserve
merge 1:m wl_id_code using `primary_folder'patient_history, keepusing(chg_date chg_time chg_till active cpra) keep(match master) nogenerate

keep if chg_date <= `sample_end_date'
bys wl_id_code: egen peak_cpra = max(cpra)  // also generate peak CPRA here, used later
bys wl_id_code (chg_date chg_time): gen first_cpra = cpra[1]    // CPRA from first patient history record. For new arrivals, their CPRA at listing
    
// replace missing values of chg_till with end date of sample period to compute active days correctly
// this truncates active days computation at the end of our sample period, potentially earlier than the end of the patient history
replace chg_till = `sample_end_date' if chg_till > `sample_end_date'

// Count active days within the first and second years respectively
gen one_year_active_days = active*(max(min(chg_till,init_date + 365) - max(chg_date,init_date),0))
gen two_year_active_days = active*(max(min(chg_till,init_date + 365*2) - max(chg_date,init_date),0))
gen sample_time_active = active*(max(min(chg_till,`sample_end_date') - max(chg_date,`sample_start_date'),0))

// Create active days for a few time-varying subcategories: CPRA, waiting time
gen wait_time_cutoff = min(init_date + 365*3,`sample_end_date')
gen sample_time_active_wait_lt_3yr = active*(max(min(chg_till,wait_time_cutoff) - max(chg_date,`sample_start_date'),0))

collapse (firstnm) peak_cpra first_cpra (sum) one_year_active_days two_year_active_days sample_time_active*, by(wl_id_code)

gen sample_time_active_wait_ge_3yr = sample_time_active - sample_time_active_wait_lt_3yr
gen peak_cpra_cat = "Peak CPRA >= 80%" if peak_cpra >= .80
replace peak_cpra_cat = "Peak CPRA < 80%" if peak_cpra_cat == ""

save `active_days', replace
restore

// Merge Back
merge 1:1 wl_id_code using `active_days', assert(match) nogenerate

save `kidpan_w_vars', replace
end
