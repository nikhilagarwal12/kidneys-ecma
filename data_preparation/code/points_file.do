// Create a points file. One row per offer, columns give each points source and sub-reasons
// DO NOT run on own; it should be called within gen_primary_files.do


  // select sample offers
  append_points_files `ptr_start_year' `sample_end_year' "`points_data'"
  joinby match_id_code ptr_sequence_num using "`primary_files'sample_offers"
  keep match_id_code opo_alloc_audit_id ptr_sequence_num wlreg_audit_id_code ptr_sub_score_cd alloc_points_ki_id ptr_sub_score_points

  // reshape points file so one row per offer
  rename alloc_points_ki_id alloc_points_ki_id_
  rename ptr_sub_score_points ptr_sub_score_points_

  reshape wide alloc_points_ki_id_ ptr_sub_score_points_, i(match_id_code ptr_sequence_num) j(ptr_sub_score_cd)
  isid match_id_code ptr_sequence_num

  // to keep schema consistent across samples, adding additional points codes
  cap gen alloc_points_ki_id_8 = .
  cap gen ptr_sub_score_points_8 = 0
  cap gen alloc_points_ki_id_34 = .
  cap gen ptr_sub_score_points_34 = 0
  cap gen alloc_points_ki_id_35 = .
  cap gen ptr_sub_score_points_35 = 0

  compress

  if `full_sample'==1{
      save "`primary_files'points_wide", replace
  }

  if `test_sample'==1{
      merge 1:1 match_id_code ptr_sequence_num using `test_sample_files'sample_offers, keep(master match) keepusing() nogen
      save `test_sample_files'points_wide, replace
  }
