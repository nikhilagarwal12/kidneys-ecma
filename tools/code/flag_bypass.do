/* Code to identify non-standard match runs:

1. If refusal_id = 851, 852, 862, 863, flag the entire match run as non-standard
2. If refusal_id = 850, 860, 861, 880, flag the entire match run as non-standard
   if at least one of the following criteria is met:
   1. More than 70% of the potential offers in the match run are due to the same 
      refusal reason (i.e. if more than 70% are refusal_id 850 or if more than 
      70% are refusal_id 860)
   2. There exists a potential offer in the match run after which 90% of the 
      potential offers are due to the same refusal reason 
Drop donors if they have at least one non-standard match run
Drop patients if they have received a kidney from a dropped donor
*/

bys match_id_code (ptr_sequence_num) : egen tl_offers_per_match = count(donor_id)

foreach val in 851 852 862 863{
	di "`val'"
	bys match_id_code (ptr_sequence_num): egen bypass_`val' = max(refusal_id == `val')
}
foreach val in 850 860 861 880{
	di "`val'"
	bys match_id_code (ptr_sequence_num): egen num_refused_for_`val' = total(refusal_id == `val')
	bys match_id_code (ptr_sequence_num): gen cum_refused_for_`val' = sum(refusal_id == `val')
	gen frc_bypass_`val' = num_refused_for_`val'/tl_offers_per_match
	bys match_id_code (ptr_sequence_num): gen fwd_frc_bypass_`val' = (num_refused_for_`val' - cum_refused_for_`val' + (refusal_id == `val'))/(tl_offers_per_match - _n + 1)
	bys match_id_code (ptr_sequence_num): egen pk_fwd_frc_bypass_`val' =  max(fwd_frc_bypass_`val')
	gen bypass_`val' = (pk_fwd_frc_bypass_`val' > 0.9 | frc_bypass_`val' > 0.7)
}

// tag donors to be dropped
gen _tag_bypass = max(bypass_851, bypass_852, bypass_862, bypass_863, bypass_850, bypass_860, bypass_861, bypass_880)
bys donor_id: egen _tag_bypass_donor = max(_tag_bypass)
bys donor_id: gen _tag_first_donor_offer = _n==1
tab _tag_bypass_donor if _tag_first_donor_offer == 1

// keep the list of dropped donors
preserve
keep if _tag_bypass_donor == 1
keep donor_id
cap duplicates drop
tempfile dropped_donors
save `dropped_donors'
restore

// tag patients who received a bypassed kidney
merge m:1 donor_id wl_id_code using `primary_files'kidpan.dta, keepusing(donor_id wl_id_code wl_org) keep(master match)
gen true_transplant = _merge == 3
drop _merge
gen _tag_bypass_organ_placed = organ_placed ~="" & _tag_bypass_donor == 1 & true_transplant == 1 & wl_org == "KI"
drop wl_org
bys wl_id_code : egen _tag_received_bypassed_donor = max(_tag_bypass_organ_placed)
bys wl_id_code : gen _tag_first_wl_offer = _n==1
tab _tag_received_bypassed_donor if _tag_first_wl_offer == 1

// keep the list of dropped patients
preserve
keep if _tag_received_bypassed_donor == 1
keep wl_id_code donor_id
cap duplicates drop
tempfile dropped_pairs
save `dropped_pairs', replace
keep wl_id_code
cap duplicates drop
tempfile dropped_patients
save `dropped_patients', replace
restore

drop _tag_bypass cum_refused_for_* num_refused_for_* fwd_frc_bypass_* pk_fwd_frc_bypass_* bypass_* frc_bypass_* tl_offers_per_match true_transplant _tag_first_donor_offer _tag_first_wl_offer _tag_bypass_organ_placed

drop if _tag_bypass_donor == 1
drop if _tag_received_bypassed_donor == 1

/* Delete dropped donors and patients from corresponding datasets. */
preserve
// delete dropped donors from deceased_donors.dta
use `primary_files'deceased_donors, clear
cap drop _merge
merge 1:1 donor_id using `dropped_donors', keep(master)
drop _merge
save `primary_files'deceased_donors, replace

// delete dropped patients from patients.dta
use `primary_files'patients, clear
cap drop _merge
merge 1:1 wl_id_code using `dropped_patients', keep(master)
drop _merge
save `primary_files'patients, replace

// delete dropped patients from patient_history.dta
use `primary_files'patient_history, clear
cap drop _merge
merge m:1 wl_id_code using `dropped_patients', keep(master)
drop _merge
save `primary_files'patient_history, replace

// delete dropped patients and donors from kidpan.dta
use `primary_files'kidpan , clear 
cap drop _merge
merge m:1 donor_id wl_id_code using `dropped_pairs', keep(master)
drop _merge
save `primary_files'kidpan, replace


restore
