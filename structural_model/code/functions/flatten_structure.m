
function [obj,blue] = flatten_structure(object,blueprint)

    if nargin==1
        % if object is a structure with object.a.x = 3 and object.b.y =2
        % obj is a structure with obj.x=3 and obj.y =2 and blueprint is 
        % another structure with blue.a = {'x'} and blue.b = {'y'};
        blue = struct();
        fldnames = {};
        for ff = fieldnames(object)',
            fn = fieldnames(object.(ff{1}));
            blue.(ff{1}) = fn;
            fldnames = [fldnames; fn];
            v2struct(object.(ff{1}));
        end
        fldnames{end+1} = 'fieldNames';
        obj = v2struct(fldnames);
    else
        % This reverses the process. If object.x=3 and object.y =2 and blueprint is 
        % another structure with blueprint.a = {'x'} and blueprint.b = {'y'};
        % then obj will be a structure with obj.a.x = 3 and obj.b.y =2
        % blueprint may also be a cell. In that case, obj will be a structure
        % with a subset of fields of object given by the fieldnames in blueprint.
        if isstruct(blueprint)
            fldnames = fieldnames(blueprint)';
        elseif iscell(blueprint)
            fldnames = blueprint(:)';
        elseif ischar(blueprint)
            fldnames = {blueprint};
        else
            error('The second argument has to be a structure, cell or char');
        end
        obj = struct();
        for ff = fldnames,
            if isstruct(object.(ff{1}))
                fn = fieldnames(object.(ff{1}));
                v2struct(object.(ff{1}));
                fn{end+1} = 'fieldNames';
                obj.(ff{1}) = v2struct(fn);
            else
                obj.(ff{1}) = object.(ff{1});
            end
        end
    end
        
