clear
use ~/organs-raw/raw-extract/CTR-DSA/KIDPAN_CTR_DSA_DATA.DTA
keep listing_ctr_dsa
duplicates drop
drop if listing_ctr_dsa=="" | listing_ctr_dsa=="Unknown Unknown"
gen listing_ctr_dsa_id = substr(listing_ctr_dsa,1,8)
isid listing_ctr_dsa_id, sort
outsheet using `auxiliary_files'listing_ctr_dsa_list.csv, comma replace

clear
use ~/organs-raw/raw-extract/CTR-DSA/KIDPAN_CTR_DSA_DATA.DTA
keep opo_ctr
duplicates drop
drop if opo_ctr=="UNKN-OP1 Unknown OPO" | opo_ctr==""
gen opo_id = substr(opo_ctr,1,8)
isid opo_id, sort
gen listing_ctr_dsa_id=""
gen region_id=.
outsheet using `auxiliary_files'opo_ctr_id_base.csv, comma replace
