function out=hazard_fn(distribution,param)
% Generates an structure with functions related to the hazard rate of "distribution" with
% parameters "param". 
% Distributions supported: weibull, gompertz
% Fields in out:
%   hazard: hazard rate
%   survival: survival function (1-CDF)
%   cond_survival: P(T>tau|T>t)


switch distribution
    case 'weibull'
        out.hazard = @(t,l) l.^param .*param.*t.^(param-1);
        out.survival = @(t,l) exp(-(l.*t).^param);
        out.cond_survival = @(tau,t,l) exp((l.*t).^param-(l.*tau).^param);
        out.quantile = @(p,l) l.^(-1).*(-log(1-p)).^(1./param);
    case 'gompertz'
        out.hazard = @(t,a) a.*exp(param*t);
        out.survival = @(t,a) exp(-(a./param).*(exp(t*param)-1));
        % out.cond_survival = @(tau,t,a) exp((a./param).*(exp(t*param)-exp(tau*param)));  
        % The expression below should be equivalent but it has better numerical properties:
        out.cond_survival = @(tau,t,a) exp(-(a./param).*exp(tau*param+log(1-exp((t-tau)*param))));  
        out.quantile =  @(p,a) log(1-(param./a).*log(1-p))./param;
    case 'exponential'
        out.hazard = @(t,rho) rho;
        out.survival = @(t,rho) exp(-rho.*t);
        out.cond_survival = @(tau,t,rho) exp(rho.*(t-tau));
        out.quantile = @(p,rho) -log(1-p)./rho;
    otherwise
        fprintf('Unrecognized distribuion'); poo;
end

