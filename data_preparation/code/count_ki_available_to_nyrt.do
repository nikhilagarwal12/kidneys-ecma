// File to compute # organs available to NYRT for each donor
// Takes # kidneys offered for transplant, and subtracts
//   - kidneys placed in an earlier geographic category than NYRT
//   - kidneys placed through national sharing (zero HLA mismatch)
// DO NOT run on own; it should be called within gen_primary_files.do


// number of kidneys offered for transplant (excludes KP offers/transplants)
use `data_files'deceased_donors, clear
gen n_ki_total = (kil_disposition == 5 | kil_disposition == 6) + (kir_disposition== 5 | kir_disposition == 6) + (kib_disposition==5 | kib_disposition==6)*2
keep donor_id n_ki_total
tempfile total_ki_available
save `total_ki_available', replace

// number of placements "before" NYRT
use `data_files'kidpan, clear
keep if don_ty=="C"
gen geo_nyrt = "Local" if opo_dsa_id == "NYRT-OP1"
replace geo_nyrt = "Regional" if opo_region_id == 9 & geo_nyrt == ""
replace geo_nyrt = "National" if geo_nyrt == ""

gen geo_tx = "Local" if opo_dsa_id == listing_ctr_dsa_id
replace geo_tx = "Regional" if opo_region_id == listing_ctr_region_id & geo_tx == ""
replace geo_tx = "National" if geo_tx == ""
replace geo_tx = "Payback" if payback == "Y"

// mark transplants that occurred before NYRT could be allocated
gen pre_nyrt = geo_nyrt == "National" & geo_tx != "National"
replace pre_nyrt = 1 if geo_nyrt=="Regional" & geo_tx == "Local" | geo_tx == "Payback"
keep donor_id wl_id_code pre_nyrt
keep if pre_nyrt==1
isid donor_id wl_id_code
tempfile pre_nyrt
save `pre_nyrt',replace

// add special (zero-mm, payback) allocations that precede the regular offer sequence
// also mark donor with some offer to NYRT
use donor_id wl_id_code final_decision donor_cat bin listing_ctr_dsa_id real_offer geo using `data_files'offers if real_offer==1, clear
gen nyrt_offer = listing_ctr_dsa_id == "NYRT-OP1"
bys donor_id: egen any_nyrt_offer = max(nyrt_offer)
preserve
keep donor_id any_nyrt_offer
duplicates drop
tempfile nyrt_offers
save `nyrt_offers', replace
restore

gen special_placement = bin < 40 & final_decision == 1
replace special_placement = bin <= 3 & final_decision == 1 if strpos(donor_cat,"dcd") > 0
keep if special_placement == 1
keep donor_id wl_id_code special_placement
merge m:1 donor_id wl_id_code using `pre_nyrt'

// combine info
bys donor_id: egen n_pre_nyrt = total(pre_nyrt | special_placement)
keep donor_id n_pre_nyrt
duplicates drop
merge 1:1 donor_id using `nyrt_offers', nogen
merge 1:1 donor_id using `total_ki_available', keep(match using) nogen
replace n_pre_nyrt = 0 if n_pre_nyrt == .
gen ki_available_nyrt = n_ki_total - n_pre_nyrt
replace ki_available_nyrt = 1 if ki_available_nyrt==0 & any_nyrt_offer==1
replace ki_available_nyrt = 2 if ki_available_nyrt > 2
keep donor_id ki_available_nyrt
isid donor_id
save `file_dir'ki_available_nyrt, replace
