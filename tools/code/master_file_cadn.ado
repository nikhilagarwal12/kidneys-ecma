// In CADN, some wlreg_audit_id's are "in master file" while others are "not in master file." The former are offered kidneys before the latter.
// The in/out of master file designation is constant within wlreg_audit_id, but not within wl_id_code.
// Some patients never appear as in or out of master file (based on descrip) hence cannot be classified

// Our goal is to "guess" in/out for each wlreg_audit_id in CADN
// Logic is to use last wlreg_audit_id where in/out was observed to impute for subsequent wlhistory updates


program master_file_cadn

args primary_files

// save current, unfinished offer file
tempfile offers_tmp
save `offers_tmp'

// Select offers made by CADN to CADN patients
keep if match_opo_dsa_id == "CADN-OP1" & listing_ctr_dsa_id == "CADN-OP1"

// mark "in" and "not in" master file
gen in_master = strpos(descrip,", Master")>0
gen not_in_master = strpos(descrip,"Not in Master")>0
tab in_master not_in_master

// classify wlreg_audit_id's in offer data by in / not in master file
bys wlreg_audit_id_code: egen wlreg_in_master = max(in_master)
bys wlreg_audit_id_code: egen wlreg_not_in_master = max(not_in_master)
tab wlreg_in_master wlreg_not_in_master

// save classifications as temporary file
keep wlreg_audit_id_code wlreg_in_master wlreg_not_in_master
duplicates drop
tempfile wlreg_in_master
save `wlreg_in_master'


// look at the universe of wlhistory updates for CADN, merge with classifications
use "`primary_files'patient_history.dta", clear
merge m:1 wl_id_code using "`primary_files'patients.dta", keepusing(listing_ctr_dsa_id) nogen
keep if listing_ctr_dsa_id == "CADN-OP1"

// create relevant wlhistory sample
sort wl_id_code chg_date chg_time
by wl_id_code: gen sample_relevant = year(chg_date[_n+1])>=2010
by wl_id_code: replace sample_relevant = 1 if _n==_N
replace sample_relevant = 0 if chg_ty == "D"
keep if sample_relevant == 1

// merge with classificaitons
drop if wlreg_audit_id_code == .
merge 1:m wlreg_audit_id_code using `wlreg_in_master', keepusing(wlreg_in_master wlreg_not_in_master) keep(master match)
keep wl_id_code wlreg_audit_id_code chg_date chg_time chg_ty wlreg_in_master wlreg_not_in_master

sort wl_id_code chg_date chg_time

// mark wlreg_audit_id's that weren't classified (AND didn't appear in the offer data)
replace wlreg_in_master = 99 if wlreg_in_master == .
replace wlreg_not_in_master = 99 if wlreg_not_in_master == .
tab wlreg_in_master wlreg_not_in_master

sort wl_id_code chg_date chg_time

// replace in_master and not_in_master with most recent non-99 value
by wl_id_code: replace wlreg_in_master = wlreg_in_master[_n-1] if wlreg_in_master == 99
by wl_id_code: replace wlreg_not_in_master = wlreg_not_in_master[_n-1] if wlreg_not_in_master == 99

// For wlreg_audit_id's that were before the first offer, set to value at first offer
by wl_id_code: gen index_offer = _n if wlreg_in_master != 99 & wlreg_in_master != .
by wl_id_code: egen first_index = min(index_offer)
by wl_id_code: gen select_wlreg_in_master = wlreg_in_master if _n==first_index
by wl_id_code: egen first_wlreg_in_master = max(select_wlreg_in_master)
by wl_id_code: gen select_wlreg_not_in_master = wlreg_not_in_master if _n==first_index
by wl_id_code: egen first_wlreg_not_in_master = max(select_wlreg_not_in_master)

replace wlreg_in_master = first_wlreg_in_master if wlreg_in_master == 99 | wlreg_in_master == .
replace wlreg_not_in_master = first_wlreg_not_in_master if wlreg_not_in_master == 99 | wlreg_not_in_master == .

replace wlreg_in_master = 99 if wlreg_in_master == .
replace wlreg_not_in_master = 99 if wlreg_not_in_master == .

keep wlreg_audit_id_code wlreg_in_master wlreg_not_in_master

tempfile master_cadn_tmp
save `master_cadn_tmp'


// update patient history
use "`primary_files'patient_history.dta", clear
cap drop wlreg_in_master wlreg_not_in_master
merge 1:1 wlreg_audit_id_code using `master_cadn_tmp', keep(master match) nogen
save "`primary_files'patient_history.dta", replace

// update offer file, after which gen_primary_files will continue
use `offers_tmp', clear
merge m:1 wlreg_audit_id_code using `master_cadn_tmp', keep(master match) nogen


end
