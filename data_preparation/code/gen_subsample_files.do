/* Subsample primary files for NYRT or other subsamples
   `data_files' -- nationwide sample directory
   `file_dir' -- subsample directory

   Called from within gen_primary_files.do; not to be run on its own
*/

// Donors
use "`data_files'deceased_donors", clear
merge 1:1 donor_id using "`file_dir'sample_donor_ids", assert(match master) keep(match) nogenerate
merge 1:1 donor_id using "`file_dir'ki_available_nyrt", assert(match using) keep(match) nogenerate
isid donor_id, sort
save "`file_dir'deceased_donors", replace

use "`data_files'donations", clear
merge m:1 donor_id using "`file_dir'sample_donor_ids", keep(match) nogenerate
save "`file_dir'donations", replace

use "`data_files'kidpan_donors", clear
merge 1:1 donor_id using "`file_dir'sample_donor_ids", keep(match) nogenerate
isid donor_id, sort
save "`file_dir'kidpan_donors", replace

// Patients
use "`data_files'patients", clear
merge 1:1 wl_id_code using "`file_dir'sample_wl_id_codes", keep(match) nogenerate
isid wl_id_code, sort
save "`file_dir'patients", replace

use "`data_files'kidpan", clear
merge 1:1 wl_id_code using "`file_dir'sample_wl_id_codes", keep(match) nogenerate
isid wl_id_code, sort
save "`file_dir'kidpan", replace

use "`data_files'patient_history", clear
merge m:1 wl_id_code using "`file_dir'sample_wl_id_codes", keep(match) nogenerate
isid wlreg_audit_id_code, sort
save "`file_dir'patient_history", replace
keep wlreg_audit_id_code
save "`file_dir'sample_wlreg_audit_id_codes", replace

// Offers
use "`data_files'offers", clear
merge 1:1 match_id_code ptr_sequence_num pb_offer_seq using "`file_dir'sample_offers", assert(match master) keep(match) nogenerate
isid match_id_code  ptr_row_order  pb_offer_seq, m sort
save "`file_dir'offers", replace

use "`data_files'points_wide", clear
merge 1:m match_id_code ptr_sequence_num using "`file_dir'sample_offers", assert(match master) keep(match) nogenerate keepusing()
isid match_id_code ptr_sequence_num, sort
save "`file_dir'points_wide", replace

/* Save the remaining files, and run format_text_files.do */
include ../../data_preparation/code/format_text_files.do
