function time_till_temp = import_wait_times(folder)

% This script loads all the data exported by python. Right now the python code
% exports data in many different ways. This code loads each piece of data 
% using the appropriate method and organizes it in a structure that contains
% all sparse matrices of the same size and two vectors with the ids of rows and 
% columns.


blood_types = {'O','A','B','AB'};
% spcv is similar to spconvert but it allows for trailing zero columns and rows
spcv = @(mat,m,n) sparse(mat(:,1),mat(:,2),mat(:,3),m,n);
spcv2 = @(mat,m,n) sparse(mat(:,1),mat(:,2),1,m,n);

for ii = 1:4
    fprintf('\\')
    % import data for csv files
    time_till_temp(ii).pt_hist_id = importdata([folder 'wlreg_audit_id_list_' blood_types{ii} '.csv']);
    time_till_temp(ii).donor_id = importdata([folder 'donor_id_list_' blood_types{ii} '.csv']);
    wait_data = importdata([folder 'time_till_' blood_types{ii} '.csv']);
    [nrows,ncols]=size(wait_data); wait_data(isnan(wait_data))=0;
    m = numel(time_till_temp(ii).pt_hist_id);n = numel(time_till_temp(ii).donor_id); 
    time_till_temp(ii).screened = spcv(wait_data(:,[1 2 3]),m,n);
    for k=4:ncols
        time_till_temp(ii).(['days' num2str(k-3)]) = spcv(wait_data(:,[1 2 k]),m,n);
    end
    fprintf('\b|')
    % chars;
    chars_data = importdata([folder 'chars_' blood_types{ii} '.csv']);
    for k = 3:numel(chars_data.colheaders)
        time_till_temp(ii).(chars_data.colheaders{k}) =  spcv(chars_data.data(:,[1 2 k]),m,n);
    end
    unacc = spcv2(importdata([folder 'unacc_' blood_types{ii} '.csv']),m,n);
    if isempty(unacc), unacc = sparse(m,n); end
    time_till_temp(ii).unacc = unacc;
    fprintf('\b/')
    
    mechs = {'preKAS','KAS'};

    for ll = 1:numel(mechs)
        % Files prefix
        switch mechs{ll},
            case 'preKAS', prefix = '';  
            case 'KAS', prefix = 'kas_';
        end
        % Can I find these files?
        checkfile = [folder prefix 'points_' blood_types{ii} '.bin']; 
        if ~exist(checkfile,'file')
            fprintf('Could not find %s. Skipping %s.\n',checkfile,mechs{ll});
            continue
        end
        % Import them!
        sz = size(unacc');
        fillup = find(unacc'==0); num_pts = numel(fillup);
        time_till_temp(ii).([prefix 'points']) = VecToMat(sz,fillup,readbinaryfile([folder prefix 'points_' blood_types{ii} '.bin'],num_pts))';
        time_till_temp(ii).([prefix 'bins'])   = VecToMat(sz,fillup,readbinaryfile([folder prefix 'bins_' blood_types{ii} '.bin'],num_pts))';
        
        % Deal with pediatric patients;
        fprintf('\b-')
        fillup = find(time_till_temp(ii).bins'); num_pts = numel(fillup);
        % Same pediatric file applies to all mechanisms, that's why there is no prefix in the next line
        time_till_temp(ii).pediatric = VecToMat(sz,fillup,readbinaryfile([folder 'pediatric_' blood_types{ii} '.bin'],num_pts))';
        fillup = find(time_till_temp(ii).pediatric'==1); num_ped = numel(fillup);
        ped_points = readbinaryfile([folder prefix 'pediatric_points_' blood_types{ii} '.bin'],[2 num_ped]);
        ped_bins = readbinaryfile([folder prefix 'pediatric_bins_' blood_types{ii} '.bin'],[2 num_ped]);
        fprintf('\b\\')
        time_till_temp(ii).([prefix 'points10']) = VecToMat(sz,fillup,ped_points(1,:))';
        time_till_temp(ii).([prefix 'points12']) = VecToMat(sz,fillup,ped_points(2,:))';
        time_till_temp(ii).([prefix 'bins10'])   = VecToMat(sz,fillup,ped_bins(1,:))';
        time_till_temp(ii).([prefix 'bins12'])   = VecToMat(sz,fillup,ped_bins(1,:))';
        fprintf('\b.')
        time_till_temp(ii).blood_type = blood_types{ii};
   end

end
fprintf('\n')

function A=readbinaryfile(filename,n)
    if nargin<1, n=inf; end
    % Read first byte
    fid = fopen(filename);
    bits = fread(fid,1,'ubit8','b');
    A = fread(fid,n,['ubit' num2str(bits)],'b');
    fclose(fid);

function B=VecToMat(sz,pos,dat,sp);
    % Check sizes:
    if numel(dat) ~= numel(pos) && numel(dat) ~= sum(pos), keyboard; end
    zz = sum(dat==0);
    if nargin<4, sp = numel(pos)-zz < prod(sz-1)/3; end   % It's not only memory, also speed of access.
    if ~sp
        B = zeros(sz);
        B(pos)=dat;
    else
        if zz>0.1*numel(pos), pos=pos(dat~=0); dat=dat(dat~=0); end 
        [rows,cols]  = ind2sub(sz,pos);
        B = sparse(rows,cols,dat,sz(1),sz(2),numel(pos));
    end
    
    
