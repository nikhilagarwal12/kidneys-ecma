// Creates the deceased_donors primary file from DECEASED_DONOR_DATA.DTA
// DO NOT run on own; it should be called within gen_primary_files.do


  use "`deceased_donor_data'DECEASED_DONOR_DATA.DTA", clear
  isid donor_id
  merge 1:1 donor_id using "`primary_files'sample_donor_ids"
  keep if _merge == 3
  drop _merge
  gen don_ty = "C"

  tempfile temp_base
  save `temp_base'

  // drop donors who donate to patients who wait for a pancreas
  // also drop patients who received kidneys from these donors from corresponding patient datasets
  include ../../tools/code/drop_pancreas_waitlist.do
  
  // type of donor, used for ranking candidates
  generate_donor_categories
  tab donor_cat

  // Calculate donor KDRI
  compute_kdri `pr_hypertension' `pr_diabetes'
  codebook donor_id
  summarize kdri, detail
  kdri_kas_categories `kdri_norm' `kdri_norm_p20' `kdri_norm_p34' `kdri_norm_p85'
  tab kdri_cat

  // merge in donor OPO's since the deceased file has some donors not in KIDPAN
  merge 1:1 donor_id using "`location_data'donor_NON_STD.DTA", keep(master match) nogen

  // Get opo region
  merge m:1 opo_ctr using "`ctr_dsa_def'opo_ctr_id_bridge"
  drop if _merge==2
  drop _merge
  rename region_id opo_region_id
  rename listing_ctr_dsa_id opo_dsa_id

  // finally, we need non- A and B antigens, which are needed for matching but not stored in DECEASED_DONOR_DATA
  // take from KIDPAN and from pre-created file for donors not in KIDPAN (we get about 2/3 of these)
  merge 1:1 donor_id using "`primary_files'kidpan_donors.dta", keepusing(dbw* dc* ddr* ddq* ddp*) keep(master match) nogen
  foreach var in `hla_vars'{
    rename `var' `var'_kidpan
  }
  merge 1:1 donor_id using "`auxiliary_files'donor_antigens_notin_kidpan.dta", keepusing(dbw* dc* ddr* ddq* ddp*) keep(master match) nogen
  foreach var in `hla_vars'{
    replace `var' = `var'_kidpan if `var'_kidpan != .
  }
  drop *_kidpan

  if `full_sample'==1{
      save "`primary_files'deceased_donors.dta", replace
  }
      
  if `test_sample'==1{
    merge 1:1 donor_id using `test_sample_files'sample_donor_ids, keep(match) nogen
    save `test_sample_files'deceased_donors, replace
  }
