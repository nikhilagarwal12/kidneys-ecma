function score = score_xtile(x,num_bin)
    % Get the third dimension
    N = size(x,3);

    % Initialize
    score = zeros(size(x));
    x_ii  = zeros(size(x,1),size(x,2));

    % Loop through N
    for ii = 1:N
        x_ii          = x(:,:,ii);
        ptiles        = prctile(x_ii(~(isinf(x_ii) & x_ii<0)),0:100/num_bin:100); % Excludes -inf in the computation of ptiles;
        % Get the score category
        score_ii      = sum(bsxfun(@ge,x_ii(:),ptiles(1:end-1)),2);  % -inf gets score = 0 
                                                                     % the minimum finite element gets score = 1
                                                                     % the maximum element gets score = num_bin
        score(:,:,ii) = reshape(score_ii,size(x,1),size(x,2));
    end