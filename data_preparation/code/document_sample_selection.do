// Code to document patient, donor, etc sample restrictions
// DO NOT run on own; it should be called within gen_primary_files.do

set more off

* input folder
local raw_kidpan = "`kidpan_data'KIDPAN_DATA.DTA"
local raw_location_data = "`location_data'kidpan_non_std.DTA"
local raw_wlhistory_data = "`wlhistory_data'KIDPAN_WLHISTORY_DATA.DTA"
local full_ptr = "`primary_files'ptr_primary.dta"
local mech_offers = "`primary_files'nyrt/mech_offers_processed.dta"

* output folder
local output_folder = "/proj/organs/common_output/reduced_form/"

* switches for which sample selection to work on
* set all to 1 for a full rerun
local ptr_offers = 1
local patients = 1
local all_offers = 1

local kidpan_vars = "wl_id_code init_date end_date wl_org rem_cd"

*************************
**** OFFER SELECTION ****
*************************
if `ptr_offers'==1{

  // preliminary: save a set of NYRT patients registered within the sample period 
  use wl_id_code using "`primary_files'nyrt/sample_wl_id_codes.dta", clear
  desc
  tempfile nyrt_wl_ids
  save `nyrt_wl_ids'

  // preliminary: save set of ptr offers made between 2010 and 2013
  use "`full_ptr'", clear
  rename match_submit_dt match_date
  rename prime_opo_refusal_id refusal_id
  drop if match_date < `nyrt_start_date' | (match_date > `nyrt_end_date' & match_date ~= .)
  tempfile offers_2010_2013
  save `offers_2010_2013'

  // take ptr_offers as the universe of all offers 
  gen offer_num = _n  // unique offer identifier
  tempfile offer_counts // to hold counts
  tempfile donor_counts // holds counts of donors in PTR data
  local row_order = 0
  local row_order_don = 0
  local row_order = `row_order'+1
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "All PTR offers in U.S., 2010-2013"
  gen row_order = `row_order'
  save `offer_counts'

  // NYRT patients
  use donor_id match_id_code wl_id_code ptr_row_order ptr_sequence_num pb_offer_seq match_date pt_code c_accept_810 ro_* real_offer using `primary_files'ptr_offers, clear
  drop if match_date < `nyrt_start_date' | (match_date > `nyrt_end_date' & match_date ~= .)
  merge m:1 wl_id_code using `nyrt_wl_ids', keep(match) nogen
  gen offer_num = 1
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Offers made to NYRT patient sample"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // Excluding patients and donors allocated using non-standard rules
  keep if ro_matchrun == 1
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Excluding patients and donors receiving non-standard allocations"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // exclude duplicated offers from the same donor to the same patient 
  keep if ro_duplicates == 1
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Excluding duplicated offers to the same patient"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // excluding offers past the cutoff
  keep if ro_cutoff == 1
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Excluding offers after the donor's cutoff"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // excluding offers that were automatically screened out
  drop if c_accept_810 == "Screen"
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Excluding offers rejected by pre-set criteria"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // excluding offers where patient was bypassed
  drop if c_accept_810 == "Bypass"
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Excluding offers where patient was bypassed"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // excluding offers to patients who could not accept (Inactive, technological, unacceptable antigens)
  drop if c_accept_810 == "Unacceptable" | c_accept_810 == "Technological" | c_accept_810 == "Inactive"
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Excluding offers to inactive or incompatible patients"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // excluding offers refused due to hospital/doctor constraints or negligence
  drop if c_accept_810 == "Negligence"
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Excluding offers where surgeon or hospital unavailable"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // excluding offers to patients who did not respond
  drop if c_accept_810 == "Non-Response"
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Excluding offers with no recorded response"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count

  // De-duplicating at pt_code level
  merge 1:1 donor_id match_id_code ptr_sequence_num pb_offer_seq using `primary_files'nyrt/sample_offers, assert(master match) keep(match) nogen
  local row_order = `row_order'+1
  preserve
  egen num_donors = tag(donor_id)
  collapse (count) offer_num (sum) num_donors
  gen label = "Dropping offers to duplicate patient listings"
  gen row_order = `row_order'
  append using `offer_counts'
  save `offer_counts', replace
  restore
  count


  use `offer_counts', clear
  sort row_order
  rename offer_num offer_count
  rename num_donors donor_count
  label var offer_count "Number of Offers"
  label var donor_count "Number of Donors"
  order label offer_count donor_count
      
  export excel using `output_folder'sample_restrictions.xls, sheet("offers") firstrow(varlabels) sheetreplace

}



***************************
**** PATIENT SELECTION ****
***************************
if `patients'==1{

  // KIDPAN data
  use "`raw_kidpan'", clear
  drop if wl_id_code == .
  isid wl_id_code
  keep `kidpan_vars'

  // Location data (NYRT)
  merge 1:m wl_id_code using `raw_location_data', keep(match) keepusing(listing_ctr_dsa) nogen
  gen listing_ctr_dsa_id = substr(listing_ctr_dsa,1,8)
  drop listing_ctr_dsa
  keep if listing_ctr_dsa_id == "NYRT-OP1"
  tab wl_org, m

  // Waitlist History Data
  merge 1:m wl_id_code using "`raw_wlhistory_data'", keep(master match) keepusing(chg_date chg_time chg_ty unos_cand_stat_cd wlreg_audit_id_code)
  bys wl_id_code: egen first_listed = min(chg_date)
  by wl_id_code: egen last_listed = max(chg_date*chg_ty=="D")
  replace last_listed = . if last_listed == 0
  keep if first_listed <= `nyrt_end_date' & last_listed >= `nyrt_start_date'

  sort wl_id_code chg_date chg_time chg_ty wlreg_audit_id_code
  by wl_id_code: gen chg_till = chg_date[_n+1]
  by wl_id_code: egen active_during_sample = max((unos_cand_stat_cd < 4099)*(chg_date <= `nyrt_end_date')*(chg_till >= `nyrt_start_date')*(chg_ty!="D"))

  keep wl_id_code active_during_sample wl_org rem_cd init_date end_date
  duplicates drop

  tab wl_org active_during_sample, m
  tab rem_cd

  // who is in the sample
  gen in_sample = init_date <= `nyrt_end_date' & end_date >= `nyrt_start_date'
  keep if in_sample

  tempfile patients_left
  save `patients_left', replace
  tempfile pt_output

  local row_order = 1

  // Total Patients
  preserve
    collapse (count) wl_id_code
    gen label = "Patients Registered in NYRT Between 2010 and 2013"
    gen row_order = `row_order'
    save `pt_output', replace
  restore

  // disqualified because waiting for pancreas
  local row_order = `row_order'+1
  gen registered_ki = wl_org == "KI"
    keep if registered_ki == 1
    save `patients_left', replace
    collapse (count) wl_id_code
    gen label = "Kidney candidates registered in NYRT, 2010-2013"
    gen row_order = `row_order'
    append using `pt_output'
    save `pt_output', replace

  // disqualified because did not need/want transplant
  local row_order = `row_order'+1
  use `patients_left', clear
    gen refused_tx = rem_cd==6|rem_cd==12|rem_cd==22|rem_cd==24
    keep if refused_tx==0
    save `patients_left', replace
    collapse (count) wl_id_code
    gen label = "Excluding candidates who were not interested in a transplant"
    gen row_order = `row_order'
    append using `pt_output'  
    save `pt_output', replace

  // disqualified because inactive
  local row_order = `row_order'+1
  use `patients_left', clear
    keep if active_during_sample==1
    save `patients_left', replace
    collapse (count) wl_id_code
    gen label = "Excluding inactive candidates"
    gen row_order = `row_order'
    append using `pt_output'  
    save `pt_output', replace

  // disqualified because received transplant through non-standard allocation procedures
  local row_order = `row_order'+1
  use `primary_files'nyrt/sample_wl_id_codes, clear
    collapse (count) wl_id_code
    gen label = "Excluding candidates who received non-standard allocations"
    gen row_order = `row_order'
    append using `pt_output'  
    save `pt_output', replace

  sort row_order
  rename wl_id_code patient_count
  label var patient_count "Number of Patients Registered"
  label var label ""
  order label patient_count row_order
  drop row_order

  export excel using `output_folder'sample_restrictions.xls, sheet("patients") firstrow(varlabels) sheetreplace

}


************************************
**** SIMULATED OFFERS SELECTION ****
************************************
if `all_offers'==1{

  use `mech_offers', clear
  local row_order = 1
  tempfile offer_count
  gen num_offers = 1
  
  // All possible patient-donor pairs
  preserve
  collapse (count) num_offers
  gen label = "All possible patient-donor pairs"
  gen row_order = `row_order'
  save `offer_count'
  local row_order = `row_order'+1
  restore
  count

  gen int neg_match_run = - match_run_num
  bys donor_id wl_id_code (screened  neg_match_run): gen tag_for_del = _n > 1
  drop if tag_for_del
  drop tag_for_del neg_match_run
  count

  // Exclude blood or tissue-type incompatible pairs
  drop if (~abo_abdr_ok | unacceptable_eq | unacceptable) & offered_dat==0
  preserve
  collapse (count) num_offers
  gen label = "Excluding blood and tissue-type incompatible pairs"
  gen row_order = `row_order'
  append using `offer_count'
  save `offer_count', replace
  local row_order = `row_order'+1
  restore
  count

  // Exclude offers below donor's cutoff
  drop if (~offered_points | ~offered_runtype) & offered_dat==0
  preserve
  collapse (count) num_offers
  gen label = "Excluding offers below donor's cutoff"
  gen row_order = `row_order'
  append using `offer_count'
  save `offer_count', replace
  local row_order = `row_order'+1
  restore
  count 
  
  // Exclude offers where patient was inactive
  drop if ~offered_status & offered_dat==0
  preserve
  collapse (count) num_offers
  gen label = "Excluding offers to patients not actively waiting"
  gen row_order = `row_order'
  append using `offer_count'
  save `offer_count', replace
  local row_order = `row_order'+1
  restore
  count
 
  // interlude to save tabulation of predicted vs actual offers meeting screening criteria
  tempfile sim_offers_left
  save `sim_offers_left'
  tab offered_dat offered_sim, matcell(x)
  matrix list x
  clear    
  svmat x
  l if _n==1
  export excel using `output_folder'mechanism_code_fit_`filter'.xls, sheet("raw", replace)

  // check whether we can find the relevant (donor_id, wl_id_code) pairs in the full PTR dataset
  use `sim_offers_left', clear
  keep donor_id wl_id_code offered_dat offered_sim
  isid donor_id wl_id_code
  sort donor_id wl_id_code  
  merge 1:m donor_id wl_id_code using `primary_files'ptr_offers, keepusing(ro_*)
  gen offered_dat_exp = _merge==3 | offered_dat
  gsort donor_id wl_id_code -offered_dat_exp -offered_sim
  by donor_id wl_id_code: keep if _n==1

  tab offered_dat_exp offered_sim, matcell(x)
  matrix list x
  clear    
  svmat x
  l if _n==1
  export excel using `output_folder'mechanism_code_fit_`filter'.xls, sheet("raw, full PTR", replace)

  // Add back offers in PTR data that were not predicted by simulation
  use `sim_offers_left', clear
  keep if ~offered_sim | offered_dat
  preserve
  collapse (count) num_offers
  gen label = "Excluding offers predicted to appear in PTR that did not"
  gen row_order = `row_order'
  append using `offer_count'
  save `offer_count', replace
  local row_order = `row_order'+1
  restore
  count

  // de-duplicating at the donor_id, pt_code level
  merge m:1 wl_id_code using `primary_files'nyrt/patients, assert(match using) keep(match) keepusing(pt_code) nogen
  keep donor_id pt_code num_offers
  duplicates drop
  isid donor_id pt_code
  preserve
  collapse (count) num_offers
  gen label = "De-duplicating across match runs and patient listings"
  gen row_order = `row_order'
  append using `offer_count'
  save `offer_count', replace
  local row_order = `row_order'+1
  restore

  use `offer_count', clear
  sort row_order
  rename num_offers offer_count
  label var offer_count "Number of Offers"
  order label offer_count
      
  export excel using `output_folder'sample_restrictions.xls, sheet("offers_mech") firstrow(varlabels) sheetreplace

}
