function out = diagnostics_gibbs(results, type_diag, nresults)

    n_diag = length(type_diag);
    
     
    
    fn_diag = cellfun(@str2func, type_diag, 'UniformOutput', false);
    for ii=1:n_diag
       fieldname =  func2str(fn_diag{ii});
       %assigning number of outputs
       ind=find(ismember(type_diag,fieldname));
       [result{1:nresults(ind)} ]= fn_diag{ii}(results);
       out.(fieldname)=result;
    end
    
    
end
