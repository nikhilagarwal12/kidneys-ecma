// This script computes summary statistics about the kidney system using our preKAS primary files (2010-2013)
// User needs to set sample_name and user locals, and also decide which parts of the code to run
// This script generates:
//    - Table 1: Patient Characteristics
//    - Table 2: Donor Characteristics
//    - Table 3: Rates of Receiving and Accepting Offers
//    - Table 4: Transplant Rates by Age and Dialysis Status

clear
set more off


************************************************************
* SETUP
************************************************************

local sample_name = "nyrt"  
local sample_start_date = date("01/01/2010","MDY")
local sample_end_date = date("12/31/2013","MDY")
prog drop _allado
sysdir set PERSONAL ../../reduced_form/code/functions/


local subsample_donor_code = `" opo_dsa_id == 12 "'  // Defines when a donor was recovered in the relevant subsample (think time frame, geographic region)
local subsample_tx_offers = `" final_decision == 1 & listing_ctr_dsa_id == "NYRT-OP1" "'  // identifies a transplant to subsample patient in offer file
local subsample_tx_donors = `" n_nyrt_tx > 0 & n_nyrt_tx != . "'  // identifies donor transplanted to subsample patient in deceased_donors_w_vars.dta
local subsample_offer_offers = `" listing_ctr_dsa_id == "NYRT-OP1" "'  // identifies an offer to a subsample patient in offer file
local subsample_offer_donors = `" n_nyrt_offers > 0 & n_nyrt_offers != . "'  // identifies a donor offered to some subsample patient in deceased_donors_w_vars.dta

************************************************************
* INPUT DATA LOCATION
************************************************************
local primary_full = "~/organs-star/primary_files/"
local primary_files = "~/organs-star/primary_files/"
if "`sample_name'" != ""{
  local primary_files = "`primary_files'`sample_name'/"    // if full sample, use main primary files folder
}
local auxiliary_table_path = "~/organs-star/processed_files/summary_statistics/"
if "`sample_name'" != ""{
    local auxiliary_table_path = "`auxiliary_table_path'`sample_name'/"
}


************************************************************
* INTERMEDIATE DATA LOCATION
************************************************************
local kidpan_w_vars = "`auxiliary_table_path'kidpan_w_vars.dta"
local deceased_donors_w_vars = "`auxiliary_table_path'deceased_donors_w_vars.dta"
local match_variables_file = "`auxiliary_table_path'match_variables_for_summary_stats.dta"
local sim_offers_basic = "`auxiliary_table_path'sim_offers_basic.dta"
local transplants = "`auxiliary_table_path'transplants.dta"

************************************************************
* OUTPUT PATHS
************************************************************
local excel_output = "~/organs-proj/common_output/reduced_form/"  // folder in which to write excel tables



************************************************************
* SET WHICH ANALYSIS TO RUN
************************************************************
// Whether to generate baseline files
local new_sim_offers = 1                // if == 1, re-processes predicted offers table from our simulations
local create_transplants_table = 1      // if == 1, creates a table with all transplants in KIDPAN (just the wl_id X donor_id pairs)
local create_kidpan_w_vars = 1          // if == 1, creates patient table with additional variables
local create_deceased_donors_w_vars = 1 // if == 1, creates donor table with additional variables
local new_match_variables_file = 1      // if == 1, prepares match_variables_file
    
// Which tables to generate
local make_donor_table = 1
local make_waitlist_snapshot_table = 1
local make_waitlist_flow_table = 1
local make_match_patient_experience = 1
local make_match_characteristics = 1

// Which tables to format
local format_donor_table = 1
local format_waitlist_snapshot_table = 1
local format_waitlist_flow_table = 1
local format_match_patient_experience = 1




************************************************************
* PREPARE INTERMEDIATE DATASETS
************************************************************

// If needed, create new file(s) with simulated offers and match variables
if `new_sim_offers'==1{
  create_sim_offers_file "`sim_offers_basic'" "`primary_files'"
}   
    
// Table with transplants from KIDPAN
if `create_transplants_table'==1{
  create_transplants_table "`transplants'" `sample_start_date' `sample_end_date' "`primary_files'"
}

// Prepare patient variables to cut on / summarize based on KIDPAN data
if `create_kidpan_w_vars'==1{
  create_kidpan_w_vars `sample_start_date' `sample_end_date' "`kidpan_w_vars'" "`primary_files'" "`sim_offers_basic'"
}

// Create separate file of donor characteristics using deceased_donors, since some donors are not in KIDPAN
if `create_deceased_donors_w_vars'==1{
  create_deceased_donors_w_vars "`deceased_donors_w_vars'" `sample_start_date' `sample_end_date' "`primary_files'" "`primary_full'" "`sample_name'" `" `subsample_tx_offers' "' `" `subsample_offer_offers' "'
}

if `new_match_variables_file'==1 {
  create_match_variables_file "`kidpan_w_vars'" "`deceased_donors_w_vars'" "`transplants'" "`sim_offers_basic'" "`match_variables_file'" "`primary_files'" "`sample_name'"
}



************************************************************
* GENERATE TABLES
************************************************************
*-------------------------------------------------
* Table 1: Patient Characteristics
*-------------------------------------------------
if `make_waitlist_snapshot_table'==1 | `format_waitlist_snapshot_table'==1 {
  local snapshot_vars = "curr_age curr_years_on_list curr_cpra diab_pt init_on_dialysis years_on_dialysis prior_transplant init_bmi_calc tot_serum_album"
  local snapshot_dates = "18263 19359"
  waitlist_snapshot_table `make_waitlist_snapshot_table' "`auxiliary_table_path'/waitlist_snapshot_table" "`kidpan_w_vars'" "`snapshot_dates'" "`snapshot_vars'" "`primary_files'" "`sample_name'" "`excel_output'"
}

*-------------------------------------------------
* Table 1: Patient Characteristics
*-------------------------------------------------
if `make_waitlist_flow_table'==1 | `format_waitlist_flow_table'==1 {
  local flow_vars = "end_age male_pt race_pt_White race_pt_Black race_pt_Hispanic race_pt_Asian abo_pt_A abo_pt_O abo_pt_AB abo_pt_B end_cpra c_end_epts diab_pt end_on_dialysis end_years_on_dialysis prior_transplant end_years_on_waitlist end_bmi_calc"
  waitlist_flow_table `make_waitlist_flow_table' "`auxiliary_table_path'/waitlist_flow_table" `sample_start_date' `sample_end_date' "`kidpan_w_vars'" "`flow_vars'" "`sample_name'" "`excel_output'"
}

*-------------------------------------------------
* Table 2: Donor Characteristics
*-------------------------------------------------
if `make_donor_table'== 1 | `format_donor_table' ==1 {
  local don_chars = "n_offers num_ki_tx age_don cause_head_trauma cause_stroke diabetes_don hypertension_don creat_don ecd_donor non_heart_beating"
  donor_table `make_donor_table' "`auxiliary_table_path'/donor_table" "`deceased_donors_w_vars'" "`don_chars'" "`sample_name'" `"`subsample_donor_code'"'  `"`subsample_tx_donors'"' `"`subsample_offer_donors'"' "`excel_output'" `sample_start_date' `sample_end_date'
}


*-------------------------------------------------
* Table 3: Rates of Receiving and Accepting Offers
*-------------------------------------------------
if `make_match_patient_experience'==1 | `format_match_patient_experience'==1{
  local results = "../../reduced_form/output/match_patient_experience.dta"
  local outcomes = "match local zero_abdr"
  match_patient_experience `make_match_patient_experience' "`match_variables_file'" "`sample_name'" "`results'" "`outcomes'" "`excel_output'"
}

*------------------------------------------------------
* Table 4: Transplant Rates by Age and Dialysis Status
*------------------------------------------------------
if `make_match_characteristics'==1{
  match_characteristics `kidpan_w_vars' `deceased_donors_w_vars' `excel_output' `sample_start_date' `sample_end_date' `sample_name'
}
