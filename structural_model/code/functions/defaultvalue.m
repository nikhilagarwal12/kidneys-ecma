function [structure,using_default] = defaultvalue(structure,field,default_value)

% Sets structure.field = default_value if field is not a field in structure.
% using_default = true if the condition above is met.
using_default = ~isfield(structure,field);
if using_default, structure.(field) = default_value; end
