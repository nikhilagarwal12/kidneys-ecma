% Compute r-squared of predicted (x) and actual (y) values

function rs = r_squared(y,x)

    ey = mean(y);
    tss = sum((y-ey).*(y-ey));
    rss = sum((y-x).*(y-x));
    rs = 1 - rss/tss;

end