# Main code to determine which patients would have received an offer in each match run
# Primary data inputs are a set of donors (match runs) and a set of patient history records

import sys
import csv
import re
import datetime
import math
import pdb
import calculate_offers_function_library
import calculate_offers_variances_library
import os

selective_write = 0
default_light = 0
default_encoded = 0
default_folderstring = '/processed_files/mech_'

light_version = 1 if len(sys.argv)>1 and sys.argv[1]=='1' else default_light
encoded = 1 if len(sys.argv)>2 and sys.argv[2]=='1' else default_encoded
folderstring = sys.argv[3].split('organs-star')[1] if len(sys.argv)>3 else default_folderstring


donor_file = file("/local-data/organs-"+os.getlogin()+folderstring+"donor_extract.csv",'rb')                                                                   
offer_file = file("/local-data/organs-"+os.getlogin()+folderstring+"offer_write.csv",'w') 
log_file = file("/local-data/organs-"+os.getlogin()+folderstring+"log_write.csv",'w') 

donor_extract_reader = csv.DictReader(donor_file, delimiter = ',', quotechar = '"')

if not light_version:
    fnames = ['a_mm','dr_mm', 'b_mm', 'offered_runtype','offered_status','offered_points','match_run_num', 'last_tie_break', 
          'unos_cand_stat_cd','p_hla', 'cpra', 'c_tie_break', 'listing_ctr_dsa_id', 
          'p_ped11', 'donor_id', 'init_date', 'Ped_List', 'screened', 'unacceptable', 'p_ped35', 'p_pld',
          'hlamis', 'Ped_Alloc', 'c_bin', 'imported', 'p_wait_wl', 'abo', 'c_abo','c_cpra',
          'match_date', 'cmis', 'wl_id_code', 'prior_living_donor','wlreg_audit_id_code',
          'p_pra', 'last_bin', 'c_kidney_points','c_addtl_pts','geo', 'last_kidney_points','abo_abdr_ok',
          'unacceptable_eq', 'reason_unacc','reason_unacc_eq','reason_screen','ever_offered','always_offered','needed_wait_time',
          'predicted_mechanism','predicted_offer']
else: # Rewrite fnames for light version 
    fnames = ('donor_id match_run_num wl_id_code wlreg_audit_id_code reason_screen offered_points offered_status' +
         ' unacceptable unacceptable_eq offered_runtype abo_abdr_ok c_bin c_kidney_points c_tie_break a_mm  dr_mm b_mm' + 
         ' abo_compat local national screened').split()

print fnames

fields_don =['donor_id','match_run_num','last_bin','last_kidney_points','last_tie_break']
fields_pat = ['wl_id_code','wlreg_audit_id_code','prior_living_donor','listing_ctr_dsa_id','unos_cand_stat_cd','abo','cpra']

          
offer_writer = csv.DictWriter(offer_file,delimiter = ',', quotechar = '"', fieldnames = fnames, extrasaction='ignore')
offer_writer.writerow(dict([(nn,nn) for nn in fnames]))
log_writer = csv.DictWriter(log_file,delimiter = ',', fieldnames = ['donor_id','wlreg_audit_id_code','c_abo'], extrasaction='ignore')

# Read various donor types
old_healthy = [];
with open('../../mechanism/input/rank_categories_old_healthy.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        old_healthy.append(row)        


young_healthy = [];
with open('../../mechanism/input/rank_categories_young_healthy.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        young_healthy.append(row)        

young_dcd = [];
with open('../../mechanism/input/rank_categories_young_dcd.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        young_dcd.append(row)        

old_dcd = [];
with open('../../mechanism/input/rank_categories_old_dcd.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        old_dcd.append(row)        

ecd = [];
with open('../../mechanism/input/rank_categories_ecd.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        ecd.append(row)        

ecd_dcd = [];
with open('../../mechanism/input/rank_categories_ecd_dcd.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        ecd_dcd.append(row)        

# CREG groups
creg_groups = [];
with open('../../mechanism/input/creg_groups_handbook.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        creg_groups.append(row)

# Split antigen equivalences
antigens = ['a','b','dr']
split_equiv_pre = dict([(x,{}) for x in antigens])
with open('../../mechanism/input/split_equiv_pre.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        for a in antigens:
            split_equiv_pre[a][row['pat_'+a]] = set([int(x) for x in row['don_'+a].split(',') if len(x)>0])


split_equiv_pos = dict([(x,{}) for x in antigens])
with open('../../mechanism/input/split_equiv_pos.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        for a in antigens:
            split_equiv_pos[a][row['pat_'+a]] = set([int(x) for x in row['don_'+a].split(',') if len(x)>0])


# Unacceptable antigen equivalences
antigens = ['a','b','c','dq','dr']
unacc_equiv_pre = dict([(x,{}) for x in antigens])
with open('../../mechanism/input/unacc_equiv_pre.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        for a in antigens:
            unacc_equiv_pre[a][row['pat_'+a]] = set([int(x) for x in row['don_'+a].split(',') if len(x)>0])

unacc_equiv_pos =  dict([(x,{}) for x in antigens])
with open('../../mechanism/input/unacc_equiv_pos.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        for a in antigens:
            unacc_equiv_pos[a][row['pat_'+a]] = set([int(x) for x in row['don_'+a].split(',') if len(x)>0])
        

# Create label dictionary
filename0 = "../../data_preparation/code/label_dictionary.do"
label_dict = dict()
if encoded:    
    with open(filename0,'rb') as label_file:                                                                   
        cleanfile = [line.strip() for line in label_file.readlines() if len(line.strip())>0 and line.strip()[0] not in set(['/','#'])]
        instructions = ' '.join(cleanfile).split('label')
        for instruction in instructions:
            inst=instruction.strip().split(' ',2)
            if inst[0] =='define':
                a = iter(inst[2].split('"')[0:-1])
                label_dict[inst[1]] = dict()
                for var in a:
                    label_dict[inst[1]][int(var)] = next(a)         
            if inst[0] == 'copy':
                label_dict[inst[2]] = label_dict[inst[1]]
        
# Load Patient history and define an iterator.
patient_file = open("/local-data/organs-"+os.getlogin()+folderstring+"pt_hist_extract.csv",'rb')
pt_extract_reader = csv.DictReader(patient_file, delimiter = ',', quotechar = '"')
pt_keys = ['til_date']+[k for k in pt_extract_reader.fieldnames if k != 'til_date']
active_list = []
last_match_date = datetime.datetime.strptime('1jan1900', "%d%b%Y")
pre_deadline = datetime.datetime.strptime('17mar2011', "%d%b%Y")
pt_dict = pt_extract_reader.next()
pt_dict['chg_date'] = datetime.datetime.strptime(pt_dict['chg_date'], "%d%b%Y")
pt_dict['til_date'] = datetime.datetime.strptime(pt_dict['til_date'], "%d%b%Y")
pt_dict=calculate_offers_function_library.label_decode(pt_dict,label_dict)

# Loop over donor types
countdown_donors = 10**5

for donor in donor_extract_reader:
    donor=calculate_offers_function_library.label_decode(donor,label_dict)
    countdown_donors = countdown_donors - 1
    if countdown_donors < 0:
        break
    
    print('Date:' + donor['match_date'] + ' donor:' + donor['donor_id'] + ' match run:' + donor['match_run_num'] + ' run_opo_id:' + donor['run_opo_id'] + ' at:' + datetime.datetime.now().strftime('%c'))
    counter = 0
    counter_rntype = 0
    counter_status = 0
    counter_uaccpt = 0
    counter_abdrok = 0
    counter_screen = 0
    counter_ok = 0
    
    match_date = datetime.datetime.strptime(donor['match_date'], "%d%b%Y")
    if match_date > last_match_date: 
        while pt_dict['chg_date'] < match_date:
            pt_list = [pt_dict[k] for k in pt_keys]
            active_list.append(pt_list)
            pt_dict = pt_extract_reader.next()
            pt_dict['chg_date'] = datetime.datetime.strptime(pt_dict['chg_date'], "%d%b%Y")
            pt_dict['til_date'] = datetime.datetime.strptime(pt_dict['til_date'], "%d%b%Y")
            pt_dict=calculate_offers_function_library.label_decode(pt_dict,label_dict)
        
        active_list = [pt for pt in active_list if pt[0] >= match_date] 
        last_match_date = match_date
        print('Active Patients: ' + str(len(active_list)))
        split_equiv = split_equiv_pre if match_date < pre_deadline else split_equiv_pos
        unacc_equiv = unacc_equiv_pre if match_date < pre_deadline else unacc_equiv_pos
        
        
# First Patient
    for active_pt in active_list:
        pt = dict([(pt_keys[e],v) for e,v in enumerate(active_pt)] )
        match = dict([(k,pt[k]) for k in fields_pat]+ [(k,donor[k]) for k in fields_don])
        match['match_date'] = match_date
        # Blood Type Compatability
        match=calculate_offers_function_library.abo_compatibility(donor,pt,match)
        match=calculate_offers_variances_library.abo_compatibility(donor,pt,match)
        
        # write donor-patient pair to output file if blood types are compatible
        write_entry = match['c_abo']!="Incompatible"
        log_writer.writerow(match)
        if write_entry:
            
            # Offer geography
            match=calculate_offers_function_library.geo_fun(donor,pt,match)
            match['imported'] = donor['opo_dsa_id'] != pt['listing_ctr_dsa_id']
            
            # Pediatric, age, waiting time at allocation
            match=calculate_offers_function_library.time_vars_fun(donor,pt,match)
            match=calculate_offers_variances_library.time_vars_fun(donor,pt,match)
            
            # CPRA
            match=calculate_offers_function_library.cpra_fun(pt,match)
            
            # HLA Mismatches
            match=calculate_offers_function_library.hla_fun_with_eq(donor,pt,creg_groups,match,split_equiv)
            # Calculate whether patient is eligible given the O/B policy: O,B donors to O,B patients are 0ABRDmm
            match=calculate_offers_function_library.abo_abdr_ok(donor,pt,match)
            
            # Compute kidney points variables and tie break
            match=calculate_offers_function_library.point_vars_fun(donor,pt,match)
            
            
            # Calculate whether there are unacceptable antigens (unacceptable=True/False)
            match=calculate_offers_function_library.unacc_antigen_fun(donor,pt,match)
            match=calculate_offers_function_library.unacc_antigen_fun_with_eq(donor,pt,match,unacc_equiv)
            
            # Check screening criterion (screened=True/False)
            match=calculate_offers_function_library.screening_fun(donor,pt,match)
            # Variance screening: They often appear in the listing as 883 (min crit not met) 
            match=calculate_offers_variances_library.screening_fun(donor,pt,match)
            
            # Compute bin and final kidney points
            match=calculate_offers_function_library.points_and_bins_fun(donor,pt,match,young_healthy,old_healthy,young_dcd,old_dcd,ecd,ecd_dcd)
            match=calculate_offers_variances_library.points_and_bins_fun(donor,pt,match)
            
            # Compute the number of days needed to wait to be offered the kidney
            match=calculate_offers_function_library.min_wait_fun(donor,pt,match)
            match=calculate_offers_variances_library.min_wait_fun(donor,pt,match)
            
            # Calculate if the kidney should be offered based on bin/points/tiebreak cutoff and run type
            match=calculate_offers_function_library.offer_fun(donor,pt,match)

            if light_version:
                match['abo_compat'] = not(match['c_abo']=="Identical")
                match['local'] = match['geo']=="Local" or match['geo']=="OPO"
                match['national'] = match['geo']=="National"
                
            match=calculate_offers_function_library.fixboolean(match,fnames)
            
            write_if_selective = (match['offered_runtype'] and match['offered_status'] and (not match['unacceptable']) and (not match['unacceptable_eq']) and match['abo_abdr_ok'] and (match['screened']==0 or match['screened']==50) )
            if  (not selective_write) | write_if_selective: 
                offer_writer.writerow(match)
                counter = counter + 1
                if (not selective_write):
                    if (not match['offered_runtype']):
                        counter_rntype = counter_rntype + 1
                    elif (not match['offered_status']):
                        counter_status = counter_status + 1
                    elif match['unacceptable'] or match['unacceptable_eq']:
                        counter_uaccpt = counter_uaccpt + 1
                    elif (not match['abo_abdr_ok']):
                        counter_abdrok = counter_abdrok + 1
                    elif  not (match['screened']==0 or match['screened']==50):
                        counter_screen = counter_screen + 1
                    else:
                        counter_ok = counter_ok + 1
    if (not selective_write):
        print('Offers Written: ' + str(counter) + '; rntype: ' + str(counter_rntype) + '; status: ' + str(counter_status) + '; uaccpt: ' + str(counter_uaccpt) + '; abdrok: ' + str(counter_abdrok) +  '; screen: ' + str(counter_screen) + '; ok: ' + str(counter_ok))
    else:
        print('Offers Written: ' + str(counter))
                
donor_file.close()
offer_file.close()
log_file.close()
