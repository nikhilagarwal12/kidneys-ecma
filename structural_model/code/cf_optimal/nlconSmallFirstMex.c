#include "common.h"

#include "mex.h"

#include <math.h>
#include <stdint.h>
#include <string.h>

static int notInitialized = 1;
static long *iIndex, *jIndex;
static double *value;
static double *expTerm, *invCdf, *q, *bac, *pc, *aK;

void cleanup(void) {
  mexPrintf("MEX-file is terminating, destroying allocations in analytic small first Jacobian\n");
  free(expTerm);
  free(q);
  free(bac);
  free(invCdf);
  free(pc);
  free(aK);
  free(iIndex);
  free(jIndex);
  free(value);
  notInitialized = 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  double *discountV1 = (double*)mxGetPr(mxGetField(prhs[1], 0, "discount_V1"));
  double *arrivals = (double*)mxGetPr(mxGetField(prhs[1], 0, "arrivals"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[1], 0, "mu"));
  const unsigned long nPatients = sizeArray[0];
  const unsigned long nTime = sizeArray[1];
  const unsigned long nDonors = sizeArray[2];
  
  const unsigned long nQ = nPatients*nTime*nDonors;
  const unsigned long nP = nPatients*nTime;
  const double *qTemp = (double*)mxGetPr(prhs[0]);

  /* Part to filter for smaller problem */
  double *sigmaFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "sigmaFiltered"));
  double *deltaBy_hFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "delta_x_hFiltered"));
  double *muFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "muFiltered"));
  double *lambda_hOverKFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "lambdah_over_KFiltered"));
  double *endowmentFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "endowmentFiltered"));
  double *discountW_Filtered = (double*)mxGetPr(mxGetField(prhs[1], 0, "discount_wFiltered"));
  double *cArrayRepeatFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "pr_compat_repeatFiltered"));
  double *hFiltered = (double*)mxGetPr(mxGetField(prhs[1], 0, "hFiltered"));
  const uint64_t qCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "qCount")));
  const uint64_t pCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "pCount")));
  const uint64_t pM1Count =  *((uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "pM1Count")));
  const uint64_t donorCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "donorCount")));
  const double nDonorsInv =  (double)mxGetScalar(mxGetField(prhs[1], 0, "nDonorsInv"));
  const uint64_t *npIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "npIndex"));
  const uint64_t *mapEq =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "mapEq"));
  const uint64_t *map =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "map"));
  const uint64_t *mapF =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "mapF"));
  const uint64_t *donorIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[1], 0, "donorIndex"));

  uint64_t *filterPattern = (uint64_t*)mxGetPr(prhs[2]);
  uint64_t *filterPaTi = filterPattern + nQ;
  uint64_t *filterDonors = (uint64_t*)mxGetPr(prhs[3]);

  const double *p = qTemp+qCount;
  const double *v = p+pCount;
  const unsigned long nTm1 = nTime-1;
  const unsigned long nTm1nP = nP - nPatients;

  if (notInitialized) {
    const unsigned long nnz = 2l*qCount+4l*pCount;
    iIndex = (long*)malloc(nnz*sizeof(long));
    jIndex = (long*)malloc(nnz*sizeof(long));
    value = (double*)malloc(nnz*sizeof(double));
    expTerm = (double*)malloc(pCount*sizeof(double));
    q = (double*)malloc(qCount*sizeof(double));
    bac = (double*)malloc(nP*sizeof(double));
    invCdf = (double*)malloc(qCount*sizeof(double));
    aK = (double*)malloc(pCount*sizeof(double));
    pc = (double*)malloc(qCount*sizeof(double));
    mexAtExit(cleanup);
    notInitialized = 0;
  }
  memset(expTerm, 0, pCount*sizeof(double));

  // Precompute common sub expressions
  for (unsigned long i = 0; i < qCount; ++i) {
    const double local = qTemp[i];
    const double cLocal = cArrayRepeatFiltered[i];
    const uint64_t pIndex = npIndex[i];
    q[i] = local*cLocal;
    expTerm[pIndex] += q[i]; 
    invCdf[i] = invCDF(1.0-local);
    pc[i] = cLocal*p[pIndex]; 
  }
  for (unsigned long j = 0; j < nP; ++j) {
    if (filterPaTi[j]) {
      const unsigned long i = mapEq[j];
      bac[j] = discountW_Filtered[i]*(expTerm[i]*nDonorsInv)+discountV1[j];
    }
  }
  for (unsigned long i = 0; i < pCount; ++i) {
    aK[i] = discountW_Filtered[i]*nDonorsInv;
  }
  for (unsigned long i = 0; i < pM1Count; ++i) {
    expTerm[i] = exp(deltaBy_hFiltered[i]-lambda_hOverKFiltered[i]*expTerm[i]);
  }
  // matrix c
  plhs[0] = mxCreateDoubleMatrix(donorCount, 1, mxREAL);
  double *c = (double*)mxGetPr(plhs[0]);
  /* Contribution from feasibility */
  for (unsigned long i = 0; i < nDonors; ++i) {
    if (filterDonors[i]) {
      double sum = 0.0;
      for (unsigned long j = 0, index1 = i*nP; j < nP; ++j, ++index1) {
        if (filterPattern[index1]) {
          const unsigned long index = mapEq[j];
          sum += p[index]*hFiltered[index]*q[map[index1]];
        }
      }
      const unsigned long index3 = mapF[i];
      c[index3] = sum - endowmentFiltered[index3];
    }
  }
  // matrix ceq
  plhs[1] = mxCreateDoubleMatrix(pCount+pM1Count, 1, mxREAL);
  double *ceq = (double*)mxGetPr(plhs[1]);
  /* Contribution from steady state */
  for (unsigned long i = 0; i < nPatients; ++i) {
    for (unsigned long j = 0, index = i; j < nTm1; ++j, index += nPatients) {
      if (filterPaTi[index]) {
        const unsigned long index1 = nPatients+index;
        const unsigned long index2 = mapEq[index];
        double pIndex1 = 0.0;
        if (filterPaTi[index1]) {
          pIndex1 = p[mapEq[index1]];
        }
        ceq[index2] = -p[index2]*expTerm[index2]+pIndex1-arrivals[index1];
      }
    }
  }
  ceq = ceq + pM1Count;
  for (unsigned long i = 0, index5 = nTm1nP; i < nPatients; ++i, ++index5) {
    for (unsigned long j = 0, index = i; j < nTm1; ++j, index += nPatients) {
      if (filterPaTi[index]) {
        const unsigned long index1 = index+nPatients;
        const unsigned long index4 = mapEq[index];
        double sumW = 0.0;
        for (unsigned long k = 0, index2 = index; k < nDonors; ++k, index2 += nP) {
          if (filterPattern[index2]) {
            const unsigned long index3 = map[index2];
            sumW += q[index3]*muFiltered[index3]+normPdf(invCdf[index3])*cArrayRepeatFiltered[index3]*sigmaFiltered[index4];
          }
        }
        double previous = 0.0;
        if (filterPaTi[index1]) {
          previous = v[mapEq[index1]];
        }
        ceq[index4] = bac[index]*v[index4]-aK[index4]*sumW-previous;
      }
    }
    if (filterPaTi[index5]) {
      const unsigned long index4 = mapEq[index5];
      double sumW = 0.0;
      for (unsigned long k = 0, index2 = index5; k < nDonors; ++k, index2 += nP) {
        if (filterPattern[index2]) {
          const unsigned long index3 = map[index2];
          sumW += q[index3]*muFiltered[index3]+normPdf(invCdf[index3])*cArrayRepeatFiltered[index3]*sigmaFiltered[index4];
        }
      }
      const unsigned long index3 = mapEq[index5];
      ceq[index3] = bac[index5]*v[index3]-aK[index3]*sumW;
    }
  }
  if (nlhs > 2) {
    // matrix gc
    unsigned long int currentIndex = 0;
    for (unsigned long i = 0; i < qCount; ++i) {
      const double pcL = hFiltered[npIndex[i]]*pc[i];
      if (pcL != 0.0) {
        iIndex[currentIndex] = i;
        jIndex[currentIndex] = donorIndex[i];
        value[currentIndex++] = pcL;
      }
    }
    for (unsigned long i = 0; i < qCount; ++i) {
      const double qh = hFiltered[npIndex[i]]*q[i];
      if (qh != 0.0) {
        iIndex[currentIndex] = npIndex[i]+qCount;
        jIndex[currentIndex] = donorIndex[i];
        value[currentIndex++] = qh;
      }
    }
    plhs[2] = mxCreateSparse(qCount+2*pCount, donorCount, currentIndex, mxREAL);
    double *sr  = (double*)mxGetPr(plhs[2]);
    size_t* irs = (size_t*)mxGetIr(plhs[2]);
    size_t* jcs = (size_t*)mxGetJc(plhs[2]);
    COO_toCSC(donorCount, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
    // matrix gceq
    currentIndex = 0l;
    /* // Part from steady state composition */
    for (unsigned long i = 0; i < qCount; ++i) {
      const unsigned long index = npIndex[i];
      if (index < pM1Count) {
        const double localValue = pc[i]*expTerm[index]*lambda_hOverKFiltered[index];
        if (localValue != 0.0) {
          iIndex[currentIndex] = i;
          jIndex[currentIndex] = index;
          value[currentIndex++] = localValue;
        }
      }
    }
    for (unsigned long j = 0; j < pM1Count; ++j) {
      if (expTerm[j] != 0.0) {
        iIndex[currentIndex] = j+qCount;
        jIndex[currentIndex] = j;
        value[currentIndex++] = -expTerm[j];
      }
    }
    for (unsigned long j = 0; j < nTm1nP; ++j) {
      const unsigned long index = j+nPatients;
      if (filterPaTi[index] && filterPaTi[j]) {
        iIndex[currentIndex] = qCount+mapEq[index];
        jIndex[currentIndex] = mapEq[j];
        value[currentIndex++] = 1.0;
      }
    }
    /* Value */
    for (unsigned long i = 0, index = 0; i < nDonors; ++i) {
      for (unsigned long j = 0; j < nP; ++j, ++index) {
        if (filterPattern[index] && filterPaTi[j]) {
          const unsigned long index2 = mapEq[j];
          const unsigned long index1 = map[index];
          double localValue = v[index2]-(muFiltered[index1]+sigmaFiltered[index2]*invCdf[index1]);
          localValue *= aK[index2]*cArrayRepeatFiltered[index1];
          if (localValue != 0.0) {
            iIndex[currentIndex] = index1;
            jIndex[currentIndex] = index2 + pM1Count;
            value[currentIndex++] = localValue;
          }
        }
      }
    }
    const unsigned long offset = pCount+qCount;
    for (unsigned long i = 0; i < nP; ++i) {
      if (filterPaTi[i]) {
        const double localValue = bac[i];
        if (localValue != 0.0) {
          iIndex[currentIndex] = mapEq[i]+offset;
          jIndex[currentIndex] = mapEq[i] + pM1Count;
          value[currentIndex++] = localValue;
        }
      }
    }
    for (unsigned long i = 0; i < nTm1nP; ++i) {
      if (filterPaTi[i] && filterPaTi[i+nPatients]) {
        iIndex[currentIndex] = mapEq[i+nPatients]+offset;
        jIndex[currentIndex] = mapEq[i] + pM1Count;
        value[currentIndex++] = -1.0;
      }
    }
    plhs[3] = mxCreateSparse(qCount+2*pCount, pCount+pM1Count, currentIndex, mxREAL);
    double *sr1  = (double*)mxGetPr(plhs[3]);
    size_t* irs1 = (size_t*)mxGetIr(plhs[3]);
    size_t* jcs1 = (size_t*)mxGetJc(plhs[3]);
    COO_toCSC(pCount+pM1Count, currentIndex, iIndex, jIndex, value, jcs1, irs1, sr1);
  }
}
