function [V, match_init_date_problem] = continuation_value(waittime,don_pat_pairs,model,donors,patients,pt_history,chain,eta,beta_i,options)

%% Check that all pt_histories are in don_pat_pairs:
foundit=false(size(pt_history.wlreg_audit_id_code));
for k=1:numel(don_pat_pairs), foundit=foundit | ismember(pt_history.wlreg_audit_id_code,don_pat_pairs{k}.pt_hist_id); end
if ~all(foundit), missing_pt=unique(~foundit), keyboard; end

%% A. Inline functions
% 1 "A" 11 "A1" 12 "A2" 2 "B" 0 "O" 3 "AB" 31 "A1B" 32 "A2B" -> {'O','A','B','AB'}
abo_fn = @(x) (x==0)+2*(x==1 | x == 11 | x ==12)+3*(x==2)+4*(x==3 | x==31 | x==32); 
flat = @(x) x(:);
%% B. Defaults
if isfield(model,'T_age'), T_age = model.T_age; else T_age=100; end
if ~exist('options','var'), options =struct(); end
if model.donor_uh
    if ~isfield(options, 'edraws'), options.edraws=size(eta.draws,2); end
    eta.draws = eta.draws(:,round([1:options.edraws]*size(eta.draws,2)/options.edraws));
end
if ~isfield(options, 'tdraws'), options.tdraws=40; end

% Patient specific hazard
pt_spec_hz = isfield(model,'q_leave') && ischar(model.q_leave);
if pt_spec_hz, s = strsplit(model.q_leave,'_'); haz_fn = hazard_fn(s{end},model.hazard.param); end

% Approaches to numerical integration
if ~isfield(options, 'approach'), options.approach='numeric_integration'; end
switch options.approach
  case 'montecarlo'
    wait_grid = rand(1,options.tdraws);
  case 'numeric_integration'
    wait_grid = linspace(0,1,options.tdraws+1); % these points split the unit interval in draws bins.
end

%% Loop
[unique_hist,where_first_hist,hist_map] = unique(pt_history.wlreg_audit_id_code);
N_iter = numel(unique_hist);
V = zeros(numel(unique_hist),numel(waittime)); 
match_init_date_problem = 0;
model.offer_sort ='match_date';

for nn = 1:N_iter
    %% Get abo and get the right don_pat_pairs
    this_pt_hist_id = unique_hist(nn);
    this_pt_hist = pt_history(where_first_hist(nn),:);
    pt  = this_pt_hist.wl_id_code;
    this_patient = patients(patients.wl_id_code==pt,:);
    if model.pt_uh, beta_i_nn = beta_i(patients.wl_id_code==pt); else beta_i_nn = []; end
    abo = abo_fn(this_patient.abo); % abo file
    pat_rho = model.rho;
    
    if min(waittime) > 1900 *365, % waittime is a date
        match_dates = waittime;
    else   % waittime is a waittime;
        match_dates = this_patient.init_date + waittime;
    end
    
    % Create offerset
    off = offerset(don_pat_pairs,this_pt_hist,this_patient,donors,[]);
    % Exclude offers
    off = off.excludeUnacceptables.excludeBin0.excludeNever;
    frac_of_donors_available = numel(unique(off.indices.donors))/size(off.donors,1);
    % Add match dates
    exponential_options.rho = pat_rho;
    exponential_options.min_match_date = min(match_dates);
    exponential_options.T_age = T_age;
    exponential_options.wait_grid = wait_grid;
    [off,dates] = matchDates(off,'exponential_distribution',exponential_options);
    dates.init_date = this_patient.init_date;
    
    % Generate a data structure with the same offers in off.
    this_data = gen_data_structure(model,[],off.subsetvars,patients,donors,pt_history,'value_function');

    pr_pos_xmatch = predict_pos_xmatch(off.data,this_pt_hist,patients,model.pos_xmatch.coefs);
    %% D.4 Compute \psi(p)
    if model.pt_uh
        chi = chi_param(this_data,model,beta_i_nn); 
    else
        chi = chi_param(this_data,model);
    end
    mu = - chi*chain.mu_theta;
    if model.donor_uh
        % Generate Eta mat  
        this_data.eta_mat = create_dummies(this_data.donor_id,eta.donor_id); % connect the list of donors in this_data with that in eta.draws;
        mu = bsxfun(@plus,mu,-this_data.eta_mat*eta.draws);        
    end
    % normcdf(mu) is the probability of NOT accepting a kidney.
    % cpp_inversion takes mu See lines 87-91 in gibbs_mixture.  
    [phi] = ccp_inversion(mu);
    if model.donor_uh, phi = mean(phi,2); end
    phi(in_eligibility_gap(off)) = 0;

    %% D.4 Compute V for each match_date
    if pt_spec_hz,
        surv.csf= @(tau,t) haz_fn.cond_survival(tau,t,this_patient.([model.q_leave '_dep_rate'])); 
        surv.hazard= @(tau) haz_fn.hazard(tau,this_patient.([model.q_leave '_dep_rate'])); 
    else, surv=[]; 
    end
    
    T = max(dates.days_till_death + dates.date_iz);
    [match_dates,~,where_dates]  = unique(match_dates);
    this_V = zeros(size(match_dates));
    for md = 1:numel(match_dates)
        if match_dates(md) > T, break; end
        [r,oops] = numeric_integration_V(match_dates(md),phi,dates,pat_rho,surv,pr_pos_xmatch);
        match_init_date_problem = match_init_date_problem + oops;
        this_V(md)=frac_of_donors_available*r;
    end
    V(nn,:) = this_V(where_dates);
end
% model.lambda is set to (days/#donors) i.e. the unconditional
% donor arrival rate irrespective of compatibility
% Therefore, V should be scaled by the fraction of donors overall
% that the patient is compatible with
V  = V * model.lambda./model.rho;  % Change so that we can use the pt_specific rho.
V  = V(hist_map,:);
end  % Function end



function [this_V,match_init_date_problem] = numeric_integration_V(t,phi,dates,pat_rho,surv,pr_xmatch)
        % This function computes the value function at t
        % averaging over phi (which is a vector or a 'donors x times' matrix).
        % The idea is that each observation in phi has the same probability of occuring so the 
        % average is consistent for the expectation (e.g. in value_function the times are drawn from an 
        % exponential so the average is a numerical integration). 
        % The code includes features that allows for evaluation of the value function at different t using
        % the same draws and updating the average accordingly:
        % 1. future_offer: it determines whether a draw is in the future and only uses future draws to compute the average
        % 2. surv: allows for a multiplicative factor that accounts for the patient specific departure hazard rate.
        % 3. Use a different date_iz for each donor. 
        % dates is a structure with the following fields:
        %     offer_dates ('donors x times', same number of elements as phi)
        %     init_date (scalar, init_date for our patient)
        %     date_iz   ('donors x 1', t_iz = max(min_t_iz, offer_dates(1))) 
        %     days_till_death ('donors x 1',  T - t_iz ) 
        % This code does NOT correct by lambda/rho.
        if nargin<6 || isempty(pr_xmatch); pr_xmatch = 1; end
        pr_xmatch = double(pr_xmatch);
        
        % 1. Time variables
        waittime_at_t = t-dates.init_date; % what's the waittime at t?
        match_init_date_problem = waittime_at_t<0; %Oh! We saw an offer before init_date!
        waittime_at_t = max(waittime_at_t,0);
        T = dates.days_till_death + dates.date_iz;
        % 2. patient specific hazard rate
        if nargin<5 || isempty(surv), 
            surv_fact=1; 
        else 
            tau = max(dates.offer_dates - t,0) + waittime_at_t;
            % Add extra term to account for the integration over the departure rate
            hazard   = surv.hazard(tau); 
            h        = [tau(:,2:end) T] - tau;
            haz_term = pat_rho./(pat_rho+hazard).*(1-exp(-(pat_rho+hazard).* h)) ./ (1-exp(-pat_rho.* h));
            haz_term(h==0)=1; % This is the limit as h->0 (L'Hopital).
            surv_fact=surv.csf(tau,waittime_at_t); 
            surv_fact= haz_term.*surv.csf(tau,waittime_at_t); 
        end
        surv_fact = double(surv_fact);
        phi_surv = reshape(phi.*pr_xmatch,size(dates.offer_dates)).*surv_fact;
        
        
        % 3. Compute the donor factor and donor average (see model_and_estimation)
        [donor_factor,donor_average]   = method_5(pat_rho,dates.date_iz,t,T,phi_surv,dates.offer_dates);
        this_V = mean(donor_average.*donor_factor);
        if any(imag(this_V)), keyboard; end;
        if any(isinf(this_V)), keyboard; end;
        if any(isnan(this_V)), keyboard; end;

end
            
function [donor_factor,donor_average] = method_5(rho,tiz,t,T,phi,date)
    f             = max([date T],t);
    f             = bsxfun(@plus,f,-min(f,[],2));
    f             = exp(-rho*double(f));
    w             = bsxfun(@times,f(:,1:end-1)-f(:,2:end),1./(f(:,1)-f(:,end)));
    donor_average = sum(phi.*w,2);
    donor_factor  = double(exp(-rho*(max(t,tiz)-t))-exp(-rho*(T-t)));
end
