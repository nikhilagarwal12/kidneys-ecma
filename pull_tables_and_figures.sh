#!/bin/sh

# Main Text: Tables 1 through 4
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/reduced_form/summary_tables_nyrt.xls
# Main Text: Table 5; Table A.3
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/structural/output/results/Estimates.xls
# Main Text: Tables 6 throug 7; Figure 3
cp /proj/organs/common_output/structural/output/results/nyrt_fix_time_uh/sample0_mech_compare_formatted.xls /proj/organs/common_output/assets/tables/
cp /proj/organs/common_output/structural/output/results/nyrt_fix_time_uh/sample0_mech_compare_formatted.xls /proj/organs/common_output/assets/figures/
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/structural/output/results/nyrt_fix_time_uh/sample0_mech_compare_formatted.xls
# Main Text: Figure 1 (a)
cp /proj/organs/common_output/structural/output/results/ccp_position_fit.png /proj/organs/common_output/assets/figures
# Main Text: Figure 1 (b)
cp /proj/organs/common_output/structural/output/results/ccp_waittime_fit.png /proj/organs/common_output/assets/figures
# Main Text: Figure 2
cp /proj/organs/common_output/structural/output/results/nyrt_fix_time_uh/figs/gamma_plots.xls /proj/organs/common_output/assets/figures/gamma_plots_nyrt_fix_time_uh.xls

# Data Appendix: Tables 1 through 3
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/reduced_form/sample_restrictions.xls
# data Appendix: Table 4
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/reduced_form/mechanism_code_fit_NYRT.xls

# Table A.1
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/reduced_form/positive_crossmatch_coefs.xls
# Table A.2
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables/ /proj/organs/common_output/reduced_form/hazard/hazard_regs.xlsx
# Table A.4
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/structural/output/results/ccp_model_validation.xls
# Figure C.1 (a)
cp /proj/organs/common_output/reduced_form/dynamic_incentives/figures/offer_rate_by_cpra_bin.eps /proj/organs/common_output/assets/figures
# Figure C.1 (b)
cp /proj/organs/common_output/reduced_form/dynamic_incentives/figures/acceptance_rate_by_cpra.eps /proj/organs/common_output/assets/figures
# Table C.5
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/structural/output/results/nyrt_fix_time_uh/sample0_mech_compare_formatted_robustness.xls
# Table C.6
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/reduced_form/dynamic_incentives/evidence_of_dynamic_incentives.xls
# Table E.7
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/reduced_form/dynamic_incentives/past_offer_analysis.xlsx
# Table E.8
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/reduced_form/rank_autocorrelation_tests.xls
# Table E.9
libreoffice --headless --convert-to pdf --outdir /proj/organs/common_output/assets/tables /proj/organs/common_output/reduced_form/patient_ordering_statistics.xls


	




