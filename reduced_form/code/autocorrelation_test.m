% Script to take a sequence of donors, construct rank autocorrelation statistics, 
% and bootstrap the distribution under a null of cutoffs not correlated over time
% This script has to be run after past_offers.do
% This script generates Table E.8: Tests for Rank Autocorrelation in Cutoff Scores

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table E.8: Tests for Rank Autocorrelation in Cutoff Scores
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
rng(2530928675)

% ------------------------------
% Data location
% ------------------------------
addpath('./functions/');
data = '~/organs-star/processed_files/summary_statistics/nyrt/';
output = '~/organs-proj/common_output/reduced_form/';
donors = readtable([data 'donors_for_rank_test.txt'],'delimiter','|','ReadVariableNames',true);

% ------------------------------
% Analaysis
% ------------------------------
donors = sortrows(donors,{'match_date','cat','donor_id'},{'ascend','ascend','ascend'});

N = length(donors.donor_id);
B = 1000;  % # bootstrap iterations
score = (100-donors.last_bin)*100 + donors.last_kidney_points + donors.last_tie_break/10000;   % no one waited more than 10,000 days!
tiebreak = rand(N,1);

donor_cat_labels = {'Healthy, Age < 35','Healthy, Age >= 35','Cardiac Death, Age < 35','Cardiac Death, Age >= 35','Low-Quality','Low-Quality or Cardiac Death'};
nyrt_labels = {'from NYRT','not from NYRT'};
abo_labels = {'Type O','Type B','Type AB','Type A'};

% baseline: grouping by donor category (determining KAS bins) only
group = donors.cat;   G = length(unique(group));
labels = donor_cat_labels;
[stat,dist,p,group_ids] = rank_bootstrap(group,score,tiebreak,B);
[stat; p; group_ids]'
result_table = make_table(group_ids,labels,group,stat,p,G)
writetable(result_table,[output 'rank_autocorrelation_tests.xls'],'Sheet','Cat');

% interact donor category with NYRT/non-NYRT
group = donors.cat*2 - donors.nyrt;   G = length(unique(group));
labels = cell(1,G);
for g=1:G
    labels{g} = [donor_cat_labels{floor((g-1)/2)+1} '; ' nyrt_labels{mod(g+1,2)+1}];
end
[stat,dist,p,group_ids] = rank_bootstrap(group,score,tiebreak,B);
[stat; p; group_ids]'
result_table = make_table(group_ids,labels,group,stat,p,G)
writetable(result_table,[output 'rank_autocorrelation_tests.xls'],'Sheet','Cat,NYRT');
   
% By NYRT/ABO
group = donors.cat*8 - 4*donors.nyrt - donors.abo + 1;   G = length(unique(group));
[stat,dist,p,group_ids] = rank_bootstrap(group,score,tiebreak,B);
[stat; p; group_ids]'
labels = cell(1,G);
for gg=1:G
        g = group_ids(gg);
	labels{gg} = [donor_cat_labels{floor((g-1)/8)+1} '; ' nyrt_labels{floor(((mod(g-1,8)+1)-1)/4) + 1} '; ' abo_labels{mod(g-1,4)+1}];
end
result_table = make_table(group_ids,labels,group,stat,p,G)
writetable(result_table,[output 'rank_autocorrelation_tests.xls'],'Sheet','Cat,NYRT,ABO');