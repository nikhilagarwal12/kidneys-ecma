function [data,model,priors] = gen_data_structure(model,priors,offers,patients,donors,pt_history,type,model_data_folder,output_stub)
%% Preliminary: set up data and output folders
    % Create directory to store base dataset
    % offers is a database: it requires
    %       wlreg_audit_id_code,match_date,wl_id_code,donor_id,match_characteristics;
        
    if strcmp(type,'master')
        data_folder = [model_data_folder model.model_name];
        if ~isdir(data_folder)
            mkdir(data_folder);
            fprintf('Created folder %s\n', data_folder);
        else
            fprintf('Folder %s already exists.\n', data_folder);
        end
        
        % Create directory to store Gibbs output
        output_folder = [output_stub model.model_name];
        if ~isdir(output_folder)
            mkdir(output_folder);
            fprintf('Created folder %s\n', output_folder);
        else
            fprintf('Folder %s already exists.\n', output_folder);
        end
        
        %% Select rows, first from offer file, then from donors/patients/pt_history
        
        % filter offers
        which_offers = eval(model.offer_def);
        offers = offers(which_offers,:);       % filter to sample offers
        offers = offers(~isnan(offers.wlreg_audit_id_code),:);  % Kill NaN rows in offers
        if ~isempty(model.match_date_range)                     % filter on date
            which_offers = all([datenum(offers.match_date) <= datenum(model.match_date_range{2}) datenum(offers.match_date) >= datenum(model.match_date_range{1})],2);
            offers = offers(which_offers,:);
        end
        
    end
    
    if ~strcmp(type,'value_function')
        % Sort the offers, once
        offers = sortrows(offers,cat(2,{'donor_id'},model.offer_sort));
    end
        
    offers.index = [1:size(offers,1)]';

    % filter donors, patients, pt_history
    donors = select_rows(donors,offers,'donor_id');
    patients = select_rows(patients,offers,'wl_id_code');
    pt_history = select_rows(pt_history,offers,'wlreg_audit_id_code');

    %% Set up data structure
    data.patients = offers.wl_id_code;
    data.donor_id = offers.donor_id; % Heterogeneity at donor level
    data.patient_list = unique(offers.wl_id_code,'sorted'); 
    data.patient_offers = histc(data.patients,data.patient_list);
    data.donor_list = donors.donor_id; 
    data.pt_abo = patients.abo;
    data.pt_history_id = offers.wlreg_audit_id_code;
    data.N_histories = length(unique(data.pt_history_id));
    data.N_donors = size(donors,1);
    data.N_patients = size(patients,1);
    data.N_offers = size(offers,1);
    data.offer_index = offers.index;  % Keep track of the original offer number.
    
    % Compute probability of positive crossmatch for each donor-patient pair, store
    data.pr_pos_xmatch = predict_pos_xmatch(offers,pt_history,patients,model.pos_xmatch.coefs);
    
    % Generate an offer number
    if ~strcmp(type,'value_function')
        data.offer_num = zeros(data.N_offers,1);
        for ii=1:data.N_donors
            where_ii = offers.donor_id == donors.donor_id(ii);
            data.offer_num(where_ii) = 1:sum(where_ii);
        end
    end        


    %% Populate Variables
    all_variables = cat(2,model.fixed.linear,model.flex.linear);

    modes = {'fixed','flex'}; 
    similarFields = {model.newvars,model.fixed.polyvar,model.flex.polyvar};
    for hh=1:length(similarFields) % loop over each set of variables
    for ii = 1:length(similarFields{hh})  % loop over variables within each set
            vars = similarFields{hh}{ii}{1};  % extract field name(s)
            if ~iscell(vars), vars = {vars}; end
            all_variables=cat(2,all_variables,vars);
        end
    end
    similarFields = {model.fixed.splines,model.flex.splines};
    for hh=1:length(similarFields)  % first fixed then flexible splines
        for ii = 1:length(similarFields{hh})  % loop over each spline
            for jj =1:length(similarFields{hh}{ii}) % extract variable name from each spline element (splines can be interactions)
                all_variables{end+1} = similarFields{hh}{ii}{jj}{1};
            end
        end
    end

    %% Set up donor and patient chars corresponding to each offer. Also match-specific vars from offer file
    offer_chars = offers(:,model.offer_chars);

    [~,where_donors] = ismember(data.donor_id, data.donor_list);
    donor_chars = donors(where_donors,model.donor_chars);
    assert(isequal(donors.donor_id,data.donor_list));

    [~,where_patients] = ismember(data.patients, data.patient_list);
    patient_chars = patients(where_patients,model.patient_chars);
    assert(isequal(patients.wl_id_code,data.patient_list));

    [~,where_history] = ismember(data.pt_history_id,pt_history.wlreg_audit_id_code);
    pt_history_chars = pt_history(where_history,model.pt_history);

    observables = [donor_chars, patient_chars, pt_history_chars, offer_chars];
    vf_project  = [model.patient_chars model.pt_history model.time_var];
    dn_project  = [model.donor_chars];
    % Add geo variables to dn_project if we're dealing with nyrt or other particular opo;
    sub_folder = fileparts(model.filenames.offers); [~,sub_folder] = fileparts(sub_folder); 
    if ismember(sub_folder,{'nyrt','other_opo_name'})
        dn_project = [dn_project 'geo'];
    end
 
    % Deal with missing values
    if strcmp(model.missing{1},'yes')  % if we keep and model missing values
       replacemissing = @(x) x + 0./(x~=model.missing{2});    % annonymous function that resolves to x or nan.
       observables = replacedata(observables,replacemissing); % replace missing values with nan values
    end
    
    % Construct newvars
    if ~isempty(model.newvars)
        for ii = 1:length(model.newvars)
            which_term = model.newvars{ii};

            % Distinguish based on whether one or more variables
            % are specified
            if ~iscell(which_term{1})
                if ismember(which_term{1},vf_project), vf_project = [vf_project which_term{3}]; end
                if ismember(which_term{1},dn_project), dn_project = [dn_project which_term{3}]; end
                which_term{1}=observables.(which_term{1});
            else
                if all(ismember(which_term{1}(:),vf_project)), vf_project = [vf_project which_term{3}]; end
                if all(ismember(which_term{1}(:),dn_project)), dn_project = [dn_project which_term{3}]; end
                for jj = 1:length(which_term{1}), which_term{1}{jj} = observables.(which_term{1}{jj}); end
            end
            % If only one variable is specified, the input to the
            % functions is a matrix
            observables.(which_term{3}) = which_term{2}(which_term{1});
        end
    end
    
    %% Create design matrix
    for mm = 1:length(modes)
        modelmm = model.(modes{mm}); K = 0; colheaders ={};
        % Pre-allocate design_matrix memory for speed;
        switch type
            case 'master', design_matrix = zeros(data.N_offers,0);
            otherwise, design_matrix = zeros(data.N_offers,numel(model.(modes{mm}).colheaders));
        end
        
        % Constant: add if option specified for this model
        if isfield(model.(modes{mm}),'constant')  % annoying case before updating model files without pt-uh
            if model.(modes{mm}).constant
                vf_project=['constant' vf_project];
                dn_project=['constant' dn_project];
                design_matrix(:,1) = 1; K = 1;
                colheaders ={'constant'}; 
            end
        elseif model.constant & mm==1
            vf_project=['constant' vf_project];
            dn_project=['constant' dn_project];
            design_matrix(:,1) = 1; K = 1;
            colheaders ={'constant'}; 
        end
        
        % Add linear variables
	if ~isempty(modelmm.linear)
            design_matrix(:,K+1:K+numel(modelmm.linear)) = double(observables(:,modelmm.linear)); K=K+numel(modelmm.linear);
            colheaders = cat(2,colheaders,modelmm.linear); % linear variable names
	end

        % Set up splines
        if ~isempty(modelmm.splines)
            for ii = 1:length(modelmm.splines) % loop over spline objects
                this_spline.Pn = 1;
                this_spline.colheaders = {'sp_'};
                include_vf = true;  % will include in value function projection if all input variables are included
                include_kd = true;
                for jj = 1:length(modelmm.splines{ii}) % loop over variables entering this spline (can be multiple)
                    which_term = modelmm.splines{ii}{jj}; % this spline element
                    include_vf = include_vf && ismember(which_term{1},vf_project);
                    include_kd = include_kd && ismember(which_term{1},dn_project);
                    if length(which_term)<4, which_term{4}=0;which_term{5}=1; end % fill in spline parameters if missing
                    this_vector = (observables.(which_term{1}) - which_term{4})/(which_term{5}-which_term{4}); % scale normalization
                    Pn=splinePn(which_term{2},this_vector,which_term{3}); % constructs the actual spline elements
                    % Aggregate with previous terms
                    [this_spline,num_cols] = tensorProd(this_spline,Pn,which_term{1});
                end
                design_matrix(:,K+1:K+size(this_spline.Pn,2)) = this_spline.Pn; K=K+size(this_spline.Pn,2);  % add spline to design matrix
                colheaders = cat(2,colheaders,this_spline.colheaders);  % add column names
                if include_vf, vf_project = [vf_project this_spline.colheaders]; end
                if include_kd, dn_project = [dn_project this_spline.colheaders]; end
            end
        end

        % Set up Polyvar
        if ~isempty(modelmm.polyvar)  % modelmm.polyvar = {poly1,poly2,...} A "poly" is a tuple {{vars},order}, so one "poly" per order
            for ii = 1:length(modelmm.polyvar) % loop over "polys" (polynomials) can be multiple 2nd-degree polynomials, for example)
                this_poly.Pn = ones(size(observables,1),1);
                this_poly.colheaders = {''}; % initialize as a constant
                this_poly.order = [0];
                poly =  modelmm.polyvar{ii}; % object specifying polynomial (variable name, order)
                include_vf = true;  % will include in value function projection if all input variables are included
                include_kd = true;
                if ~iscell(poly{1}), poly{1} = {poly{1}}; end
                for jj = 1:length(poly{1}) % loop over variables
                    this_vector = observables.(poly{1}{jj});
                    include_vf = include_vf && ismember(poly{1}{jj},vf_project);
                    include_kd = include_kd && ismember(poly{1}{jj},dn_project);
                    Pn=[]; 
                    for kk = 1:poly{2}            % reach the desired order of polynomial;
                        Pn(:,kk) = this_vector.^kk;
                    end
                    [this_poly,num_cols] = tensorProd(this_poly,Pn,[poly{1}{jj} '^'],'poly');
                end
                design_matrix(:,K+1:K+sum(this_poly.order>0))=[design_matrix this_poly.Pn(:,this_poly.order>0)]; K=K+sum(this_poly.order>0);  % add polynomial to design matrix
                colheaders = cat(2,colheaders,this_poly.colheaders(this_poly.order>0));  % add columns
                if include_vf, vf_project = [vf_project this_poly.colheaders(this_poly.order>0)]; end
                if include_kd, dn_project = [dn_project this_poly.colheaders(this_poly.order>0)]; end
            end
        end
        
        % Set up Piecewise Linear Splines
        if ~isempty(modelmm.pwlin)
            % loop over pwlin components
            for ii = 1:length(modelmm.pwlin)
                % Append to the design matrix
                pwlin_spec = modelmm.pwlin{ii};
                this_varname = pwlin_spec{1};
                this_observables = observables.(this_varname);
                this_knots = pwlin_spec{2};
                % Get the pwline columns
                [pwlin_data, pwlin_colhead,this_knots] = pwlin(this_knots,this_observables,this_varname,strcmp(type,'master'));
                model.(modes{mm}).pwlin{ii}{2} = this_knots;  %% Adds new knots when necessary.
                % Append
                design_matrix(:,K+1:K+size(pwlin_data,2)) = pwlin_data; K=K+size(pwlin_data,2);
                colheaders = cat(2,colheaders, pwlin_colhead);
                if ismember(this_varname,vf_project), vf_project = [vf_project pwlin_colhead]; end
                if ismember(this_varname,dn_project), dn_project = [dn_project pwlin_colhead]; end
            end        
        end
        
        % Set up interactions
        if ~isempty(modelmm.interactions)
            % Loop over interactions
            for ii = 1:length(modelmm.interactions)
                % Note: The current code assumes that the
                % interactions exist in levels through the linear
                % terms or through other definitions

                % Get the location of interaction terms in the
                % design matrix

                this_interaction = modelmm.interactions{ii};
                this_column = ones(size(design_matrix,1),1);
                varname = '';
                include_vf = true;
                include_kd = true;
                for jj = 1:length(this_interaction)
                    % First look in the "design matrix"
                    [ismem,loc] = ismember(this_interaction{jj},colheaders);
                    assert(length(loc)==1); % assert that you found the
                                            % variable at most once
                    % If not, look in the observables
                    if ismem
                        this_var = design_matrix(:,loc);
                    else
                        try
                            assert(ismember(this_interaction{jj},observables.Properties.VarNames)); 
                        catch me
                            display(['Could not find variable: ' this_interaction{jj}]);
                        end    

                        % Variable not found in either place if
                        % this assertion fails
                        % this assertion fails

                        this_var = observables.(this_interaction{jj});    
                    end
                    include_vf = include_vf && ismember(this_interaction{jj},vf_project);
                    include_kd = include_kd && ismember(this_interaction{jj},dn_project);
                    % Write this column
                    this_column = this_column.*this_var;
                    varname = [varname '_' this_interaction{jj}];
                end

                % Append to design matrix
                design_matrix(:,K+1) = this_column; K=K+1;

                % Append to column headers
                colheaders = cat(2, colheaders, varname);
                if include_vf, vf_project = [vf_project varname]; end
                if include_kd, dn_project = [dn_project varname]; end
            end
        end
        
        % Missing Values
        if strcmp(model.missing{3},'yes')  % deal with missing values if required    
            % Treat master differently because the design of
            % dummies depends only on the variables indeed used in
            % the estimation
            if strcmp(type,'master')
                % filters design matrix columns for any missing values
                which_vars = find(any(isnan(design_matrix),1)); 
                % Remove duplicate dummies
                [missing_cols,unique_cols] = unique(isnan(design_matrix(:,which_vars))','rows');
                which_vars = which_vars(unique_cols);
                model.(modes{mm}).missing_vars = colheaders(which_vars);
            else % model.missing_vars should be given
                which_vars = ismember(colheaders,model.(modes{mm}).missing_vars);
                missing_cols = isnan(design_matrix(:,which_vars))';
            end
            % Append
            missing_colnames = strcat(colheaders(which_vars),'_dum'); 
            if ~isempty(missing_colnames)
                colheaders = cat(2,colheaders,missing_colnames); % add missing dummy names to design matrix
                design_matrix(:,K+1:K+size(missing_cols,1)) = missing_cols'; K = K+size(missing_cols,1); % append the dummies
            end
            design_matrix(isnan(design_matrix))=0;  % set nan values to zero now that they're dealt with
            vf_project = [vf_project strcat(colheaders(which_vars(ismember(colheaders(which_vars),vf_project))),'_dum')];
            dn_project = [dn_project strcat(colheaders(which_vars(ismember(colheaders(which_vars),dn_project))),'_dum')];
        end
        design_matrix(isnan(design_matrix))=0;  % set nan values to zero, whether or not they're dealt with
        
        % Renormalize design matrix, but DON'T renormalize the constant if there is one!
        data.(modes{mm}).mu_dm = zeros(1,size(design_matrix,2)); % Default renormalization (mean 0 sd 1);
        data.(modes{mm}).sigma_dm = ones(1,size(design_matrix,2));  
        if model.renormalize % Overwrite default normalization 
            if strcmp(type,'master') 
		if model.pt_uh
		    if model.(modes{mm}).constant  % with a constant term, only renormalize other variables
                        [design_matrix(:,2:end), data.(modes{mm}).mu_dm(2:end), data.(modes{mm}).sigma_dm(2:end)] = zscore(design_matrix(:,2:end)); 
                    else                           % with no constant, re-nomalize everything   
                        [design_matrix, data.(modes{mm}).mu_dm, data.(modes{mm}).sigma_dm] = zscore(design_matrix); 
                    end
	        else
		    assert(model.constant);
		    [design_matrix(:,2:end), data.(modes{mm}).mu_dm(2:end), data.(modes{mm}).sigma_dm(2:end)] = zscore(design_matrix(:,2:end)); 
		end
            end
        end
        data.(modes{mm}).obs = design_matrix; % populate data object (data.fixed and data.flex)
        data.(modes{mm}).colheaders = colheaders;
    end
    
    % Check the rank of the observables
    fixed_K = length(data.fixed.colheaders);
    colheaders = cat(2,data.fixed.colheaders,data.flex.colheaders);
    % keep track of included and excluded regressors (use index)
    
    if ~ (strcmp(type,'counterfactuals') | strcmp(type,'value_function'))
        regressors = [data.fixed.obs data.flex.obs];
        % Check the rank conditions
        if (~(rank(regressors'*regressors)==size(regressors,2))) & isempty(strfind(model.model_name,'validation')) & isempty(strfind(model.model_name,'comparison'))
            display(['Warning! Rank Condition on Design Matrix not ' ...
                     'met. Size: ' num2str(size(regressors,2)) ' ' ...
                     'Rank: ' num2str(rank(regressors'*regressors))]);
            keyboard % moved from inside decol. It gives us more control in case rank fails.
            [regressors,index] = decol(regressors,colheaders); 
            clear regressors
        else, index = true;
        end
    else, index = ismember(colheaders,cat(2,model.fixed.colheaders,model.flex.colheaders));
    end
    if ~all(index)
        % Output the variables
        fixed_ind = index(1:fixed_K); flex_ind = index((fixed_K+1):end);

        data.fixed.obs = data.fixed.obs(:,fixed_ind);
        data.fixed.colheaders = data.fixed.colheaders(fixed_ind);
        
        data.flex.colheaders = data.flex.colheaders(flex_ind);
        data.flex.obs = data.flex.obs(:,flex_ind);
        if strcmp(type,'master')
            data.fixed.mu_dm = data.fixed.mu_dm(fixed_ind);
            data.fixed.sigma_dm = data.fixed.sigma_dm(fixed_ind);
            data.flex.mu_dm = data.flex.mu_dm(flex_ind);
            data.flex.sigma_dm = data.flex.sigma_dm(flex_ind);
        end
    end

    % Additional model parameters: lambda, and pt_arrival_rate
    if strcmp(type,'master')
        date_range = max(offers.match_date) - min(offers.match_date);
        model.lambda = data.N_donors/date_range; % Number of donors per day
        model.pt_arrival_rate = sum(all([patients.(model.time_var{1})>=min(offers.match_date) patients.(model.time_var{1})<=max(offers.match_date)],2))/date_range;
        model.fixed.K = size(data.fixed.obs,2);
        model.flex.K = size(data.flex.obs,2);   
        model.K_theta = size(data.fixed.colheaders,2)+model.K_types*size(data.flex.colheaders,2); 
        model.N_donors = data.N_donors;
        model.N_patients = data.N_patients;
        model.N_offers = data.N_offers;
        for mm = 1:length(modes)
            model.(modes{mm}).colheaders = data.(modes{mm}).colheaders;
            model.(modes{mm}).mu_dm = data.(modes{mm}).mu_dm;
            model.(modes{mm}).sigma_dm = data.(modes{mm}).sigma_dm;
        end
    end

    % Set up choices
    if strcmp(type,'master')
        data.cont_choice = offers.(model.choice_def{1});
        if strcmp(model.choice_def{2},'accept')  % if variable is coded as 1 for acceptance, flip so "no" is a 1 (model convention)
            data.cont_choice = ~data.cont_choice;
        end
    end

    if nargout>2
        % Last: expand CCP parameter prior (mean,variance) based on model dimensionality
        priors.V_theta = priors.V_theta*eye(model.K_theta); % V_theta*I, where I is the identity matrix
        priors.mu_theta = priors.mu_theta*ones(model.K_theta,1);
    end
    % Actually last: some pieces of info about the data structure
    data.model_info.gen_date = model.gen_date;  % model gen date, script, and name
    data.model_info.gen_script = model.gen_script;
    data.model_info.model_name = model.model_name;
    data.gen_date = datetime('today'); % date this data structure was generated

    if model.renormalize && ~strcmp(type,'master')
        for mm = 1:length(modes)
            data.(modes{mm}).mu_dm = model.(modes{mm}).mu_dm;
            data.(modes{mm}).sigma_dm = model.(modes{mm}).sigma_dm;
            data.(modes{mm}).obs = bsxfun(@minus,data.(modes{mm}).obs,data.(modes{mm}).mu_dm);
            data.(modes{mm}).obs = bsxfun(@times,data.(modes{mm}).obs,1./data.(modes{mm}).sigma_dm);
        end
    end
    
    if strcmp(type,'master') && ~isfield(model,'vf_projection')
        model.vf_projection = vf_project;
        model.dn_projection = dn_project;
    end
    
    if strcmp(type,'master') && ~isfield(model,'vf_projection')
        model.vf_projection = vf_project;
        model.dn_projection = dn_project;
    end
    % Load hazard parameters: 
    if strcmp(type,'master') && isfield(model,'q_leave') && ischar(model.q_leave)
        model.hazard.param = load([fileparts(model.filenames.donors) '/hazard']);
        model.hazard.param = model.hazard.param.(model.q_leave);
        model.hazard.distribution = strsplit(model.q_leave,'_');
        model.hazard.distribution = model.hazard.distribution{2};
    end
 
    
end
