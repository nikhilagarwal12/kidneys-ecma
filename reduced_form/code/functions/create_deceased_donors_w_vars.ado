// Create donor-level file with additional variables for summary statistics

// - deceased_donors_w_vars: filepath for output table
// - sample_start/end_date: pretty clear
// - primary_folder: folder for SUBSAMPLE primary files (same as main primary files folder if subsample == "")
// - primary_full: primary files folder for full sample
// - sample_name: subsample identifier ("" if full sample)
// - subsample_tx_code: identifies donors transplanted to patients in subsample
// - subsample_offer_code: identifies donors offered to patients in subsample

program create_deceased_donors_w_vars

args deceased_donors_w_vars sample_start_date sample_end_date primary_folder primary_full sample_name subsample_tx_code subsample_offer_code

// Get number of offers and the lowest offer
use donor_id zero_abdr_t geo_t c_accept final_decision descrip match_date offer_cat real_offer listing_ctr_id listing_ctr_dsa_id using "`primary_full'offers", clear
gen match_year = yofd(match_date)
tab offer_cat

* Fraction of offers of each type
gen local_offer = offer_cat <= 3
gen regional_offer = offer_cat==4|offer_cat==5

* Number of listing centers offered the kidney
bysort donor_id listing_ctr_id: gen n_listing_centers_offered = 1 if _n==1

* If geographic subsample, count offers to patients in subsample. Otherwise, do not
local collapse_string = ""

if `"`subsample_tx_code'"' != ""{
    gen n_`sample_name'_tx = `subsample_tx_code'
    local collapse_string = "`collapse_string' n_`sample_name'_tx"
}
if `"`subsample_offer_code'"' != ""{
    gen n_`sample_name'_offers = `subsample_offer_code'
    local collapse_string = "`collapse_string' n_`sample_name'_offers"
}

collapse (max) offer_cat max_match_date = match_date (min) min_match_date = match_date (count) n_offers = match_year (mean) local_offer regional_offer (sum) n_listing_centers_offered `collapse_string', by(donor_id)

// Keep only the sample period
keep if max_match_date >= `sample_start_date' & min_match_date <= `sample_end_date'

isid donor_id

// Merge with donor_file
if "`sample_name'"==""{
    merge 1:1 donor_id using "`primary_folder'deceased_donors_encoded.dta", keep(match using) nogen
}
else{
    merge 1:1 donor_id using "`primary_folder'deceased_donors_in_sample.dta", keep(match using) nogen
}
    
gen race_don = "White" if ethcat_don == 1
replace race_don = "Black" if ethcat_don == 2
replace race_don = "Hispanic" if ethcat_don == 4
replace race_don = "Asian" if ethcat_don == 5
replace race_don = "Other" if ethcat_don > 5

replace abo_don = 1 if abo_don==11|abo_don==12  // A1,A2 -> A
replace abo_don = 3 if abo_don==31|abo_don==32  // A1B,A2B -> AB
gen abo_don_A = abo_don==1
gen abo_don_B = abo_don==2
gen abo_don_AB = abo_don==3
gen abo_don_O = abo_don==0

// make abo_don string again
gen abo_don_num = abo_don
drop abo_don
gen abo_don = "O" if abo_don_num==0
replace abo_don = "A" if abo_don_num==1
replace abo_don = "B" if abo_don_num==2
replace abo_don = "AB" if abo_don_num==3
drop abo_don_num

foreach race in White Black Hispanic Asian{
  gen race_don_`race' = (race_don == "`race'")
  replace race_don_`race' = . if race_don == ""
}

// donor KDPI quintile
gen kdpi_bin = 0 if kdpi==.
replace kdpi_bin = 1 if kdpi <= .20
replace kdpi_bin = 2 if kdpi <= .40 & kdpi > .20
replace kdpi_bin = 3 if kdpi <= .60 & kdpi > .40
replace kdpi_bin = 4 if kdpi <= .80 & kdpi > .60
replace kdpi_bin = 5 if kdpi > .80 & kdpi != .

label define kdpi_bin 0 "Missing" 1 "0-20%" 2 "20-40%" 3 "40-60%" 4 "60-80%" 5 "80-100%"
label values kdpi_bin kdpi_bin

// donor cause of death
gen cause_anoxia = cod_cad_don == 1
gen cause_stroke = cod_cad_don == 2
gen cause_head_trauma = cod_cad_don == 3
gen cause_cns_tumor = cod_cad_don == 4
gen cause_other = cod_cad_don == 999

// donor medical conditions
gen hep_c_antibody = hep_c_anti_don == 5
rename blood_inf_don blood_infection
gen clinical_infection = clin_infect_don == 1  // blood infections are a subset
rename pulm_inf_don pulmonary_infection
rename urine_inf_don urine_infection
gen terminal_transfusions_don = transfus_term_don > 0 & transfus_term_don < 998
gen num_transfusions_don = transfus_term_don if transfus_term_don != 998

// other health factors
gen heavy_alcohol_use = alcohol_heavy_don == 1
gen recent_cigarette_use = contin_cig_don == 1
gen cocaine_use = hist_cocaine_don == 1
gen other_drug_use = hist_oth_drug_don == 1
gen diabetes_don = hist_diabetes_don >= 2 & hist_diabetes_don <= 5
gen hypertension_don = hist_hypertens_don == 1
rename hypertens_dur_don hypertension_duration
gen tattoos = tattoos_don == 1
gen high_hiv_risk = cdc_risk_hiv_don == 1

// how the donor died
gen death_circumst_mva = death_circum_don == 1
gen death_circumst_suicide = death_circum_don == 2
gen death_circumst_homicide = death_circum_don == 3
gen death_circumst_childabuse = death_circum_don == 4
gen death_circumst_non_mva = death_circum_don == 5
gen death_circumst_other = death_circum_don >= 6

gen death_mechan_drown = death_mech_don == 1
gen death_mechan_seizure = death_mech_don == 2
gen death_mechan_drugs = death_mech_don == 3
gen death_mechan_asphyx = death_mech_don == 4
gen death_mechan_cardio = death_mech_don == 5
gen death_mechan_electr = death_mech_don == 6
gen death_mechan_gunshot = death_mech_don == 7
gen death_mechan_stab = death_mech_don == 8
gen death_mechan_blunt_inj = death_mech_don == 9
gen death_mechan_sids = death_mech_don == 10
gen death_mechan_stroke = death_mech_don == 11
gen death_mechan_natural = death_mech_don == 12
gen death_mechan_other = death_mech_don >= 997

gen non_heart_beating = non_hrt_don == 1
gen card_arrest_post_death = cardarrest_postneuro_don == 1  // these two vars are not the same

save `deceased_donors_w_vars', replace


end
