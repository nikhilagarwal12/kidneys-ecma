
function [offer_value,rep_donor,rep_offer] = value_representative_offer(don_pat_pairs,model,all_donors,patients,pt_history,data,chain,eta_draws_struct,V_init)
    
    % Compute the representative donor
    subset_donors = all_donors.opo_dsa_id==model.local_opo & all_donors.age_don>18 & all_donors.age_don<35;
    rep_donor = representative_donor(all_donors(subset_donors,:));
    rep_offer = representative_offer(pt_history,patients);
    % Generate the data structure corresponding to off
    
    cf_data_hh = gen_data_structure(model,[],rep_offer,patients,rep_donor,pt_history,'value_function');
    % Compute Value function
    if isfield(V_init,'pt_history')
        [~,where] = ismember(cf_data_hh.pt_history_id,V_init.pt_history);
        V0 = V_init.V(where);
    else
        V0 = value_function(don_pat_pairs,cf_data_hh,model,all_donors,patients,pt_history,data,chain,eta_draws_struct,V_init); 
    end
    % Compute Gamma
    Gamma_rep_0 = V0 - cf_data_hh.fixed.obs*chain.mu_theta;
    % Compute value of a tioli offer
    offer_value=ccp_inversion(Gamma_rep_0);
    

function rep_donor=representative_donor(all_donors)
    rep_donor = all_donors(1,:);
    for kk = 1:size(all_donors,2)
        % Matlab function median does not work with datasets. So I need to take a somewhat indirect approach
        x = zeros(size(all_donors,1),1);  
        x(:) = all_donors(:,kk);    % To ensure that x is not a dataset
        rep_donor(:,kk) = dataset(median(x));% To ensure that rep_donor is a dataset and variable kk has the right variable type
    end
    % This donor doesn't exist in the data
    rep_donor.donor_id = 0;
    
    % Make it a local donor
    
function rep_offer = representative_offer(pt_history,patients)
    rep_offer.donor_id            = 0;
    rep_offer.geo                 = 2; % Local
    rep_offer.a_mm                = 2;
    rep_offer.b_mm                = 2;
    rep_offer.dr_mm               = 2; 
    rep_offer.c_abo               = 2; % Identical
    rep_offer = struct2dataset(rep_offer);
    rep_offer                     = repmat(rep_offer,numel(pt_history.wlreg_audit_id_code),1);
    rep_offer.wlreg_audit_id_code = pt_history.wlreg_audit_id_code;
    rep_offer.wl_id_code          = pt_history.wl_id_code;
    [~,where]=ismember(rep_offer.wl_id_code,patients.wl_id_code);
    rep_offer.match_date          = patients.init_date(where);
    
    
    
    

