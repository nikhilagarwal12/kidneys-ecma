#include "common.h"

#include "mex.h"

#include <math.h>
#include <string.h>
#include <stdint.h>

// A bunch of static variables to remove reallocation of arrays
static int notInitialized = 1;
static double *lambdaFeasibilityMod;
static long *iIndex, *jIndex;
static double *value;
static double *z, *fz, *Fz, *expTerm, *pExpTerm, *aK, *FzLh, *sumQfz, *sumQfzLh;

void cleanup(void) {
  mexPrintf("MEX-file is terminating, destroying allocations in analytic small second Hessian\n");
  free(iIndex);
  free(jIndex);
  free(value);
  free(lambdaFeasibilityMod);
  free(z);
  free(fz);
  free(Fz);
  free(expTerm);
  free(pExpTerm);
  free(aK);
  free(FzLh);
  free(sumQfz);
  free(sumQfzLh);
  notInitialized = 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[2], 0, "mu"));
  const unsigned long nDonorsP1 = sizeArray[2]+1;

  /* Input parameters constructed for smaller problem */
  double *muFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "muFiltered"));
  double *sigmaFilteredInv = (double*)mxGetPr(mxGetField(prhs[2], 0, "sigmaFilteredInv"));
  double *deltaBy_hFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "delta_x_hFiltered"));
  double *lambda_hOverKFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "lambdah_over_KFiltered"));
  double *discountWFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "discount_wFiltered"));
  double *cArrayRepeatFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "pr_compat_repeatFiltered"));
  double *paretoWeightsRepeatFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "pareto_weights_repeatFiltered"));
  double *hFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "hFiltered"));
  double *timeOutProbFiltered =  (double*)mxGetPr(mxGetField(prhs[2], 0, "timeout_probFiltered"));
  const double nDonorsInv =  (double)mxGetScalar(mxGetField(prhs[2], 0, "nDonorsInv"));
  const uint32_t maxTransplant = *((uint32_t*)mxGetPr(mxGetField(prhs[2], 0, "maximize_transplants")));

  const double *q = (double*)mxGetPr(prhs[0]);
  const double *lambdaFeasibility = (double*)mxGetPr(mxGetField(prhs[1], 0, "ineqnonlin"));
  const double *lambdaSteadyState = (double*)mxGetPr(mxGetField(prhs[1], 0, "eqnonlin"));

  /* Part to filter for smaller problem */
  const uint64_t qCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "qCount")));
  const uint64_t pCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "pCount")));
  const uint64_t pM1Count =  *((uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "pM1Count")));
  const uint64_t donorCount =  *((uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "donorCount")));
  const uint64_t *npIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "npIndex"));
  const uint64_t *donorIndex =  (uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "donorIndex"));
  uint64_t *donorList1D =  (uint64_t*)mxGetPr(mxGetField(prhs[2], 0, "donorList"));

  const double *p = q+qCount;
  const double *v = p+pCount;
  const double *lambdaValue = lambdaSteadyState + pM1Count;

  const size_t pDouble = sizeof(double)*pCount;

  if (notInitialized) {
    lambdaFeasibilityMod = (double*)malloc(donorCount*sizeof(double));
    const long nnz = pCount*(donorCount*donorCount+3)+qCount*4;
    iIndex = (long*)malloc(nnz*sizeof(long));
    jIndex = (long*)malloc(nnz*sizeof(long));
    value = (double*)malloc(nnz*sizeof(double));
    z = (double*)malloc(qCount*sizeof(double));
    fz = (double*)malloc(qCount*sizeof(double));
    Fz = (double*)malloc(qCount*sizeof(double));
    FzLh = (double*)malloc(qCount*sizeof(double));
    expTerm = (double*)malloc(pDouble);
    pExpTerm = (double*)malloc(pDouble);
    aK = (double*)malloc(pDouble);
    sumQfz = (double*)malloc(pDouble);
    sumQfzLh = (double*)malloc(pDouble);
    mexAtExit(cleanup);
    notInitialized = 0;
  }
  memcpy(lambdaFeasibilityMod, lambdaFeasibility, donorCount*sizeof(double));
  if (maxTransplant) {
    double *slackCostFiltered = (double*)mxGetPr(mxGetField(prhs[2], 0, "slack_costFiltered"));
    for (unsigned long j = 0; j < donorCount; ++j) {
      lambdaFeasibilityMod[j] -= slackCostFiltered[j];
    }
  }
  memset(expTerm, 0, pDouble);
  memset(pExpTerm, 0, pDouble);
  memset(sumQfz, 0, pDouble);

  unsigned long int currentIndex = 0;
  // Precompute common sub expressions
  for (unsigned long i = 0; i < qCount; ++i) {
    const unsigned long nIndex = npIndex[i];
    const double sigmaInv = sigmaFilteredInv[nIndex];
    const double zLocal = (muFiltered[i]-v[nIndex])*sigmaInv;
    fz[i] = normPdf(zLocal)*cArrayRepeatFiltered[i]*sigmaInv;
    Fz[i] = normCdf(zLocal)*cArrayRepeatFiltered[i];
    if (nIndex < pM1Count) {
      expTerm[nIndex] += q[i]*Fz[i]; 
    } 
    const double qfz = q[i]*fz[i];
    z[i] = zLocal*qfz*sigmaInv;
    FzLh[i] = Fz[i]*lambda_hOverKFiltered[nIndex];
    sumQfz[nIndex] += qfz;
  }
  for (unsigned long i = 0; i < pCount; ++i) {
    aK[i] = lambdaValue[i]*discountWFiltered[i]*nDonorsInv; 
    sumQfzLh[i] = sumQfz[i]*lambda_hOverKFiltered[i];
  }
  for (unsigned long i = 0; i < pM1Count; ++i) {
    expTerm[i] = -lambdaSteadyState[i]*exp(deltaBy_hFiltered[i]-lambda_hOverKFiltered[i]*expTerm[i]);
    pExpTerm[i] = p[i]*expTerm[i];
  }
  // Add qq Values
  uint64_t* dList = donorList1D;
  for (unsigned long k = 0; k < pCount; ++k, dList += nDonorsP1) {
    const double localValue = pExpTerm[k];
    const unsigned long donorCount = dList[0];
    for (unsigned long i = 1; i < donorCount; ++i) {
      const unsigned long index1 = dList[i];
      const double FzLhLocal = localValue*FzLh[index1];
      for (unsigned long j = 1; j < donorCount; ++j) {
        const unsigned long index2 = dList[j];
        const double localValue1 = FzLh[index2]*FzLhLocal;
        if (localValue1 != 0.0) {
          iIndex[currentIndex] = index1;
          jIndex[currentIndex] = index2;
          value[currentIndex++] = localValue1;
        }
      }
    }
  }
  // Add qp and pq Values
  dList = donorList1D;
  for (unsigned long j = 0; j < pCount; ++j, dList += nDonorsP1) {
    const unsigned long donorCount = dList[0];
    const unsigned long index2 = j+qCount;
    for (unsigned long i = 1; i < donorCount; ++i) {
      const unsigned long index1 = dList[i];
      const unsigned long index3 = donorIndex[index1];
      const double localValue = hFiltered[j]*(Fz[index1]+timeOutProbFiltered[index3])*lambdaFeasibilityMod[index3]-FzLh[index1]*expTerm[j];
      if (localValue != 0.0) {
        iIndex[currentIndex] = index1;
        jIndex[currentIndex] = index2;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = index2;
        jIndex[currentIndex] = index1;
        value[currentIndex++] = localValue;
      }
    }
  }
  // Add qv and vq values
  dList = donorList1D;
  const unsigned long offset = qCount+pCount;
  for (unsigned long j = 0; j < pCount; ++j, dList += nDonorsP1) {
    const unsigned long donorCount = dList[0];
    const unsigned long index2 = j+offset;
    for (unsigned long i = 1; i < donorCount; ++i) {
      const unsigned long index1 = dList[i];
      const double localValue1 = -fz[index1]*lambdaFeasibilityMod[donorIndex[index1]]*p[j]*hFiltered[j];
      const double localValue2 = (fz[index1]*lambda_hOverKFiltered[j]-FzLh[index1]*sumQfzLh[j])*pExpTerm[j];
      const double localValue = localValue1+localValue2+Fz[index1]*aK[j];
      if (localValue != 0.0) {
        iIndex[currentIndex] = index1;
        jIndex[currentIndex] = index2;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = index2;
        jIndex[currentIndex] = index1;
        value[currentIndex++] = localValue;
      }
    }
  }
  // Add pv and vp values
  dList = donorList1D;
  if (maxTransplant) {
    for (unsigned long i = 0; i < pCount; ++i, dList += nDonorsP1) {
      const unsigned long index1 = i+qCount;
      const unsigned long index2 = i+offset;
      double localValue1 = 0.0;
      const double localValue2 = sumQfzLh[i]*expTerm[i];
      const unsigned long donorCount = dList[0];
      for (unsigned long j = 1; j < donorCount; ++j) {
        const unsigned long index = dList[j];
        localValue1 -= q[index]*fz[index]*lambdaFeasibilityMod[donorIndex[index]];
      }
      const double localValue = localValue1*hFiltered[i] + localValue2;
      if (localValue != 0.0) {
        iIndex[currentIndex] = index1;
        jIndex[currentIndex] = index2;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = index2;
        jIndex[currentIndex] = index1;
        value[currentIndex++] = localValue;
      }
    }
  } else {
    for (unsigned long i = 0; i < pCount; ++i, dList += nDonorsP1) {
      const unsigned long index1 = i+qCount;
      const unsigned long index2 = i+offset;
      double localValue1 = 0.0;
      const double localValue2 = sumQfzLh[i]*expTerm[i];
      const unsigned long donorCount = dList[0];
      for (unsigned long j = 1; j < donorCount; ++j) {
        const unsigned long index = dList[j];
        localValue1 -= q[index]*fz[index]*lambdaFeasibilityMod[donorIndex[index]];
      }
      localValue1 -= paretoWeightsRepeatFiltered[i]; // Contribution from objective
      localValue1 *= hFiltered[i];
      const double localValue = localValue1 + localValue2;
      if (localValue != 0.0) {
        iIndex[currentIndex] = index1;
        jIndex[currentIndex] = index2;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = index2;
        jIndex[currentIndex] = index1;
        value[currentIndex++] = localValue;
      }
    }
  }
  // Add vv values
  dList = donorList1D;
  for (unsigned long i = 0; i < pCount; ++i, dList += nDonorsP1) {
    const unsigned long index = i+offset;
    double localValue1 = 0.0;
    double localValue2 = 0.0;
    const unsigned long donorCount = dList[0];
    for (unsigned long j = 1; j < donorCount; ++j) {
      const unsigned long index1 = dList[j];
      localValue1 -= z[index1]*lambdaFeasibilityMod[donorIndex[index1]];
      localValue2 += z[index1];
    }
    localValue1 *= p[i]*hFiltered[i];
    localValue2 = (localValue2*lambda_hOverKFiltered[i]+sumQfzLh[i]*sumQfzLh[i])*pExpTerm[i];
    const double localValue3 = -aK[i]*sumQfz[i];
    const double localValue = localValue1+localValue2+localValue3;
    if (localValue != 0.0) {
      iIndex[currentIndex] = index;
      jIndex[currentIndex] = index;
      value[currentIndex++] = localValue;
    }
  }
  // Create output sparse matrix
  const long matrixSize = qCount+2*pCount;
  plhs[0] = mxCreateSparse(matrixSize, matrixSize, currentIndex, mxREAL);
  double *sr  = (double*)mxGetPr(plhs[0]);
  size_t* irs = (size_t*)mxGetIr(plhs[0]);
  size_t* jcs = (size_t*)mxGetJc(plhs[0]);
  COO_toCSC(matrixSize, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
}
