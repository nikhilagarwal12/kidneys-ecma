#!/use/bin/python

# Script to calculate offer categories
import sys
import csv
import re
import datetime
import math
import pdb
import calculate_offers_function_library
import calculate_offers_variances_library
import shutil
import os
import calculate_kas_library as kas


default_folderstring = '/primary_files/nyrt/mech_'


folderstring = sys.argv[1].split('organs-star')[1] if len(sys.argv)>1 else default_folderstring

folder_read = "/local-data/organs-" + os.getlogin() + folderstring
folder_write = folder_read

pt_file = file(folder_read + "pt_unique_hist.csv",'rb')

# Separate set of files by patient blood type -- to define potential donors
pt_id_file={}
don_id_file={}
time_file={}
time_till_writer={}
char_file = {}
unacc_file = {}
char_writer = {}
unacc_writer = {}
# Points and bins
points_file = {}
bins_file = {}
points_writer = {}
bins_writer = {}
# Pediatric
pediatric_file = {}
pediatric_writer = {}
pediatric_points_file = {}
pediatric_bins_file = {}
pediatric_points_writer = {}
pediatric_bins_writer = {}
# Same thing for KAS
kas_points_file = {}
kas_bins_file = {}
kas_points_writer = {}
kas_bins_writer = {}
# Pediatric
kas_pediatric_points_file = {}
kas_pediatric_bins_file = {}
kas_pediatric_points_writer = {}
kas_pediatric_bins_writer = {}


# Field Dictionaries
fields_don =['donor_id','match_date','match_run_num','last_bin','last_kidney_points','last_tie_break']
fields_pat = ['wl_id_code','wlreg_audit_id_code','prior_living_donor','listing_ctr_dsa_id','unos_cand_stat_cd','abo']
fields_char = ['a_mm','b_mm','dr_mm','geo','c_abo']
# Add the indices for the char file
fields_char = ['pt_index','donor_index'] + fields_char
fields_unacc = ['pt_index','donor_index'] 
# See fields_time order in line 282.
# #fields_time = ['pt_index', 'donor_index','screened','needed_wait_time'] 
# sometimes for pediatric patients: ['nwt_u2','nwt_u1','nwt_l2','nwt_l1']


# Not all blood-type compatible donors are offered. e.g. Blood-type compatible
for abo in ('O','A','B','AB'):
    # Files
    pt_id_file[abo] = open(folder_write + "wlreg_audit_id_list_" + abo + ".csv",'w')
    time_file[abo] = open(folder_write + "time_till_" + abo + ".csv", 'w')
    char_file[abo] = open(folder_write + "chars_" + abo + ".csv", 'w')
    unacc_file[abo] = open(folder_write + "unacc_" + abo + ".csv", 'w')
    points_file[abo] = open(folder_write + "points_" + abo + ".bin", 'wb')
    bins_file[abo] = open(folder_write + "bins_" + abo + ".bin", 'wb')
    kas_points_file[abo] = open(folder_write + "kas_points_" + abo + ".bin", 'wb')
    kas_bins_file[abo] = open(folder_write + "kas_bins_" + abo + ".bin", 'wb')
    pediatric_file[abo] = open(folder_write + "pediatric_" + abo + ".bin", 'wb')
    pediatric_points_file[abo] = open(folder_write + "pediatric_points_" + abo + ".bin", 'wb')
    pediatric_bins_file[abo] = open(folder_write + "pediatric_bins_" + abo + ".bin", 'wb')
    kas_pediatric_points_file[abo] = open(folder_write + "kas_pediatric_points_" + abo + ".bin", 'wb')
    kas_pediatric_bins_file[abo] = open(folder_write + "kas_pediatric_bins_" + abo + ".bin", 'wb')
    # Open a writer for each blood-type
    time_till_writer[abo] = csv.writer(time_file[abo],delimiter = ',', quotechar = '"')
    char_writer[abo] = csv.DictWriter(char_file[abo],delimiter = ',', quotechar = '"',extrasaction='ignore',fieldnames =fields_char)
    unacc_writer[abo] = csv.DictWriter(unacc_file[abo],delimiter = ',', quotechar = '"',extrasaction='ignore',fieldnames =fields_unacc)
    points_writer[abo]=calculate_offers_function_library.binwriter(points_file[abo],5)  # The first byte specifies the length of a datapoint.
    bins_writer[abo]=calculate_offers_function_library.binwriter(bins_file[abo],9)
    pediatric_writer[abo]=calculate_offers_function_library.binwriter(pediatric_file[abo],1) 
    kas_points_writer[abo]=calculate_offers_function_library.binwriter(kas_points_file[abo],8)  # The first byte specifies the length of a datapoint.
    kas_bins_writer[abo]=calculate_offers_function_library.binwriter(kas_bins_file[abo],7)
    pediatric_points_writer[abo]=calculate_offers_function_library.binwriter(pediatric_points_file[abo],5) 
    pediatric_bins_writer[abo]=calculate_offers_function_library.binwriter(pediatric_bins_file[abo],9)
    kas_pediatric_points_writer[abo]=calculate_offers_function_library.binwriter(kas_pediatric_points_file[abo],8) 
    kas_pediatric_bins_writer[abo]=calculate_offers_function_library.binwriter(kas_pediatric_bins_file[abo],7)
    char_writer[abo].writerow(dict([(k,k) for k in fields_char]))
    print('*************************************')
    print('*************************************')
    print('Writers for abo ' + abo + ' are ready')
    print('*************************************')
    print('*************************************')
    
    
# Open certain files
pt_extract_reader = csv.DictReader(pt_file, delimiter = ',', quotechar = '"')

# Read donors once, and store them in a double indexed dictionary donors[donor_id][match_run_num];
with open(folder_read + "donor_extract.csv",'rb') as donor_file:
    donor_extract_reader = csv.DictReader(donor_file, delimiter = ',', quotechar = '"')
    donors = {} # Empty dictionary. 
    #Now I will create a dictionary indexed by donor_id which contains another dictionary
    #indexed by match_run_num which contains donor (which is... you guessed, another dictionary!)
    for donor in donor_extract_reader:
        if int(donor['donor_id']) not in donors.keys():
            donors[int(donor['donor_id'])] = {donor['match_run_num']:donor}
        else:
            donors[int(donor['donor_id'])][donor['match_run_num']] = donor
    donor_keys = sorted(donors.keys())  # To ensure stability it is better to fix donor_keys as an ordered list
                                        # (recall: python dictionaries are not ordered)

# Read various donor types
old_healthy = []
with open('../../mechanism/input/rank_categories_old_healthy.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        old_healthy.append(row)        


young_healthy = []
with open('../../mechanism/input/rank_categories_young_healthy.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        young_healthy.append(row)        

young_dcd = []
with open('../../mechanism/input/rank_categories_young_dcd.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        young_dcd.append(row)        

old_dcd = []
with open('../../mechanism/input/rank_categories_old_dcd.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        old_dcd.append(row)        

ecd = []
with open('../../mechanism/input/rank_categories_ecd.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        ecd.append(row)        

ecd_dcd = []
with open('../../mechanism/input/rank_categories_ecd_dcd.csv','rb') as cat_file:
    cat_reader = csv.reader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        ecd_dcd.append(row)        

# CREG groups
creg_groups = [];
with open('../../mechanism/input/creg_groups_handbook.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        creg_groups.append(row)

# Split antigen equivalences
antigens = ['a','b','dr']
split_equiv_pre = dict([(x,{}) for x in antigens])
with open('../../mechanism/input/split_equiv_pre.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        for a in antigens:
            split_equiv_pre[a][row['pat_'+a]] = set([int(x) for x in row['don_'+a].split(',') if len(x)>0])


split_equiv_pos = dict([(x,{}) for x in antigens])
with open('../../mechanism/input/split_equiv_pos.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        for a in antigens:
            split_equiv_pre[a][row['pat_'+a]] = set([int(x) for x in row['don_'+a].split(',') if len(x)>0])


# Unacceptable antigen equivalences
antigens = ['a','b','c','dq','dr']
unacc_equiv_pre = dict([(x,{}) for x in antigens])
with open('../../mechanism/input/unacc_equiv_pre.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        for a in antigens:
            unacc_equiv_pre[a][row['pat_'+a]] = set([int(x) for x in row['don_'+a].split(',') if len(x)>0])

unacc_equiv_pos =  dict([(x,{}) for x in antigens])
with open('../../mechanism/input/unacc_equiv_pos.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        for a in antigens:
            unacc_equiv_pos[a][row['pat_'+a]] = set([int(x) for x in row['don_'+a].split(',') if len(x)>0])

# Create label dictionary and abo_dictionary
filename0 = "../../data_preparation/code/label_dictionary.do"
label_dict = dict()
with open(filename0,'rb') as label_file:                                                                   
    cleanfile = [line.strip() for line in label_file.readlines() if len(line.strip())>0 and line.strip()[0] not in set(['/','#'])]
    instructions = ' '.join(cleanfile).split('label')
    for instruction in instructions:
        inst=instruction.strip().split(' ',2)
        if inst[0] =='define':
            a = iter(inst[2].split('"')[0:-1])
            label_dict[inst[1]] = dict()
            for var in a:
                label_dict[inst[1]][int(var)] = next(a)         
        if inst[0] == 'copy':
            label_dict[inst[2]] = label_dict[inst[1]]
abo_dict = calculate_offers_function_library.abo_dictionary()

# KAS
kas_rules = [];
for f in ['KDPI_20','KDPI_20_35','KDPI_35_85','KDPI_85']:
    with open('../../mechanism/input/kas/'+f+'.csv','rb') as cat_file:
        cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
        kas_rules.append([row  for row in cat_reader])


# Loop over pt histories
write_donor = {}
pt_index = {}
donor_dict = dict()
first_line_in_file={}

for abo in ('O','A','B','AB'):
    write_donor[abo] = True
    pt_index[abo] = 0
    donor_dict[abo] = []
    first_line_in_file[abo] = True
    first_line_in_file[abo] = True
    

i=0

## Set the binary export

# A byte (length = 8) with the number of bits per datapoint (unit = 1)  
#unacc = BitArray(uint = 1, lenght = 8) 


##
for pt in pt_extract_reader:
    # Output
    i = i + 1
    if i % 1000 == 1:
        print('Iter: ' + str(i) + ' Patient: ' + pt['wlreg_audit_id_code'] + " ABO: " + re.sub("\d+", "",pt['abo']))
    # Profile/Debug flag
    if i>500000000000:
        break
    
    # File-stub -- Bloodtype
    file_stub = re.sub("\d+", "",pt['abo'])
    pt_abo_dict = calculate_offers_variances_library.abo_dictionary(pt,abo_dict[pt['abo']])
    
    # Write the wl_reg_audit_id
    pt_id_file[file_stub].write(pt['wlreg_audit_id_code'])
    pt_id_file[file_stub].write('\n')    

    # Add to the pt_index
    pt_index[file_stub] = pt_index[file_stub] + 1

    calculate_offers_function_library.eq_unacc_antigen_fun_expand(pt,unacc_equiv_pre)
    pt['first_tx_date'] = datetime.datetime.strptime('31dec2999', "%d%b%Y") if pt['first_tx_date']=='' else datetime.datetime.strptime(pt['first_tx_date'], "%d%b%Y")
    pt['dialysis_date'] = datetime.datetime.strptime('31dec2999', "%d%b%Y") if pt['dialysis_date']=='' else datetime.datetime.strptime(pt['dialysis_date'], "%d%b%Y")
    
    for d in donor_keys:
        # Check pt-donor ABO potential compatibility and creates a list of runs associated with the donor:     
        runs = [donors[d][run] for run in donors[d] if donors[d][run]['abo_don'] in pt_abo_dict]
        if len(runs)==0:  # if list of runs is empty go to the next donor
            continue # 1st continue: ABO incompatibility (Not recorded)
        # Add to the donor dictonary -- This is blood-group specific, and only compatible types
        donor = runs[0]  # For general computations lets look at the first donor;
        if int(donor['donor_id']) not in donor_dict[file_stub]:
            donor_dict[file_stub].append(int(donor['donor_id']))
        donor_index = donor_dict[file_stub].index(int(donor['donor_id']))+1 # +1 bcs python indexing sets the first element to 0
        # Create basic donor patient match
        match = dict([(k,pt[k]) for k in fields_pat]+ [(k,donor[k]) for k in fields_don])
        match['c_abo'] = pt_abo_dict[donor['abo_don']]
        match['pt_index'] = pt_index[file_stub]
        match['donor_index'] = donor_index
        match['match_date'] = datetime.datetime.strptime(match['match_date'], "%d%b%Y")
        # Calculate whether there are unacceptable antigens using equivalences
        calculate_offers_function_library.unacc_antigen_fun(donor,pt,match)
        calculate_offers_function_library.unacc_antigen_fun_with_eq(donor,pt,match,unacc_equiv_pre)
        if match['unacceptable_eq'] or match['unacceptable']:
            unacc_writer[file_stub].writerow(match)    
            continue  #2nd continue: Tissue incompatibility (Recorded by unacc)
        # Now we need to look inside each run
        pick_run = {'imp':True, 'geo':"National", 'run' : 0} # Worst case scenario: imported kidney
        for run,donor in enumerate(runs):
            this_run = match.copy()
            calculate_offers_function_library.geo_fun(donor,pt,this_run)
            skip_this = (donor['opo_dsa_id'] != pt['listing_ctr_dsa_id'] and not pick_run['imp']) or (this_run['geo']=="National" and not pick_run['geo']=="National") or (this_run['geo']=="Regional" and pick_run['geo'] == "Local")
            if not skip_this:
                pick_run['imp'] = donor['opo_dsa_id'] != pt['listing_ctr_dsa_id']
                pick_run['geo'] = this_run['geo']
                pick_run['run'] = run
        # Pick the match run
        donor = runs[pick_run['run']]
        # Continue business as usual
        calculate_offers_function_library.geo_fun(donor,pt,match)
        match['imported'] = donor['opo_dsa_id'] != pt['listing_ctr_dsa_id']
        
        # Check pt-donor ABO compatibility given OPO's rules:  
        if match['c_abo'] == "Variance" or (match['c_abo']=="Compatible when Local" and match['imported']):
            points_writer[file_stub].binwrite(0)
            bins_writer[file_stub].binwrite(0)
            kas_points_writer[file_stub].binwrite(0)
            kas_bins_writer[file_stub].binwrite(0)
            continue   # 3rd continue recorded as bin==0
        match['c_abo']="Compatible" if match['c_abo']=="Compatible when Local" else match['c_abo']
            
            
        # HLA Mismatches
        calculate_offers_function_library.hla_fun_with_eq_light(donor,pt,creg_groups,match,split_equiv_pre)
        # Calculate whether patient is eligible given the O/B policy: O,B donors to O,B patients are 0ABRDmm
        calculate_offers_function_library.abo_abdr_ok(donor,pt,match)
        if not match['abo_abdr_ok']:
            points_writer[file_stub].binwrite(0)
            bins_writer[file_stub].binwrite(0)
            kas_points_writer[file_stub].binwrite(0)
            kas_bins_writer[file_stub].binwrite(0)
            continue  # 4th continue  recorded as bin==0
        
        # Pediatric listing
        if int(pt['init_age']) < 18:
            Ped_List = "Ped At Listing"
        else:
            Ped_List = "Non-Ped At Listing"           
            match['Ped_List'] = Ped_List
       
        # CPRA
        calculate_offers_function_library.cpra_fun(pt,match)
              
        # Non-Pediatric Allocation
        match['Ped_Alloc'] = "Non-Ped At Allocation"
        match['c_age'] = 19
        match['p_wait_wl'] = 0
        calculate_offers_function_library.point_vars_fun(donor,pt,match)
          

        # Check screening criterion (screened=True/False)
        calculate_offers_function_library.screening_fun(donor,pt,match)
        # Variance screening: They often appear in the listing as 883 (min crit not met) 
        calculate_offers_variances_library.screening_fun(donor,pt,match)

        # Compute bin and final kidney points
        calculate_offers_function_library.points_and_bins_fun(donor,pt,match,young_healthy,old_healthy,young_dcd,old_dcd,ecd,ecd_dcd)
        
      
        ###################################################
        pediatric_writer[file_stub].binwrite(1*(match['Ped_List'] == "Ped At Listing"))
        if match['Ped_List'] == "Ped At Listing":

            match_10 = match.copy()
            match_10['Ped_Alloc'] = "Ped At Allocation"
        
            # Pediatric Allocation < 11
            match_10['c_age'] = 10
            calculate_offers_function_library.point_vars_fun(donor,pt,match_10)
            calculate_offers_function_library.points_and_bins_fun(donor,pt,match_10,young_healthy,old_healthy,young_dcd,old_dcd,ecd,ecd_dcd)
            # Put an init_age check
            # Pediatric Allocation > 11
            match_12 = match_10.copy()
            match_12['c_age'] = 12
            calculate_offers_function_library.point_vars_fun(donor,pt,match_12)       
            calculate_offers_function_library.points_and_bins_fun(donor,pt,match_12,young_healthy,old_healthy,young_dcd,old_dcd,ecd,ecd_dcd)

            # Need to calculate bin variances at the end
            calculate_offers_variances_library.points_and_bins_fun(donor,pt,match_10)
            calculate_offers_variances_library.points_and_bins_fun(donor,pt,match_12)
            pediatric_points_writer[file_stub].binwrite(match_10['c_addtl_pts'])
            pediatric_points_writer[file_stub].binwrite(match_12['c_addtl_pts'])
            pediatric_bins_writer[file_stub].binwrite(match_10['c_bin'])
            pediatric_bins_writer[file_stub].binwrite(match_12['c_bin'])
            # KAS
            kas.bin(donor,pt,match_10,kas_rules)
            kas.points(donor,pt,match_10)
            kas.bin(donor,pt,match_12,kas_rules)
            kas.points(donor,pt,match_12)


            kas_pediatric_points_writer[file_stub].binwrite(match_10['kas_addtl_pts'])
            kas_pediatric_points_writer[file_stub].binwrite(match_12['kas_addtl_pts'])
            kas_pediatric_bins_writer[file_stub].binwrite(match_10['kas_bin'])
            kas_pediatric_bins_writer[file_stub].binwrite(match_12['kas_bin'])
            
            # Calculate Needed Points/Wait-time
            calculate_offers_function_library.min_wait_fun(donor,pt,match_10)
            calculate_offers_function_library.min_wait_fun(donor,pt,match_12)

        # Need to calculate bin variances at the end
        calculate_offers_variances_library.points_and_bins_fun(donor,pt,match)                              
        calculate_offers_function_library.min_wait_fun(donor,pt,match)
        # KAS
        kas.bin(donor,pt,match,kas_rules)
        kas.points(donor,pt,match)
        
        points_writer[file_stub].binwrite(match['c_addtl_pts'])
        bins_writer[file_stub].binwrite(match['c_bin'])
        kas_points_writer[file_stub].binwrite(match['kas_addtl_pts'])
        kas_bins_writer[file_stub].binwrite(match['kas_bin'])
        
       # Assign the relevant needed_wait_time for pediatric patients and write them if they are relevant
       # (I can't do this for KAS because I don't know the location of the last accepted offer.)
        if match['Ped_List'] == "Ped At Listing":
            # I am working under the assumption that younger individuals receive more points.
            assert(match_10['needed_wait_time']<=match_12['needed_wait_time'] or match_12['needed_wait_time']==0)
            assert(match_12['needed_wait_time']<=match['needed_wait_time']  or match['needed_wait_time']==0)
            # if match_12['ever_offered'] then match_10['ever_offered']
            assert(match_10['ever_offered'] or not match_12['ever_offered'])
            assert(match_12['ever_offered'] or not match['ever_offered'])
            intervals = [[match_10['needed_wait_time'],(11 - int(pt['init_age']))* 365],
                [match_12['needed_wait_time'], (18 - int(pt['init_age']))* 365],
                [match['needed_wait_time']]]
            # Would it make sense to compute a c_date_of_birth? 
            #Simplify the intervals (the last value can be any value greater than the prev one)    
            smp_intervals=calculate_offers_function_library.time_till_intervals(intervals)
        else:
            smp_intervals = [match['needed_wait_time']]
        # Note: Bins were not assigned purposely
        # Write to the time file
        if match['ever_offered']:
            this_row = [pt_index[file_stub],donor_index,1*match['screened']] + smp_intervals[::-1]
            # Matlab needs the first line in each file to have the right number of columns (i.e., commas)
            if first_line_in_file[file_stub]:
                this_row = this_row + (8-len(this_row))*['']
                first_line_in_file[file_stub] = False
                            
            time_till_writer[file_stub].writerow(this_row) 
        
        calculate_offers_function_library.label_encode(match,label_dict)
        char_writer[file_stub].writerow(match)
            
              
            


for abo in ('O','A','B','AB'):
    pt_id_file[abo].close()
    time_file[abo].close()
    char_file[abo].close()
    unacc_file[abo].close()
    with open(folder_write + "donor_id_list_" + abo + ".csv",'w') as donor_id_file:
        donor_id_file.write('\n'.join([str(x) for x in donor_dict[abo]]))
    bins_writer[abo].close()
    points_writer[abo].close()
    pediatric_writer[abo].close()
    pediatric_bins_writer[abo].close()
    pediatric_points_writer[abo].close()
    kas_bins_writer[abo].close()
    kas_points_writer[abo].close()
    kas_pediatric_bins_writer[abo].close()
    kas_pediatric_points_writer[abo].close()
    
    
    
