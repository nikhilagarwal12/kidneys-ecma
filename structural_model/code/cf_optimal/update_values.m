function [V,pr_tx] = update_values(q,V,params,heu)
    % V: value function (N_patients x N_time)
    % pr_tx: probability of transplant (yes and compat) conditional on offer (N_patients x N_time x N_donors).
    % Not exactly backwards induction because I use previous V instead.
    % It will perform similarly, maybe reduces oscilations.
    [N_patients, N_time, N_donors] = size(q);
    a = params.discount_w;
    
    if (isfield(params,'first_best') && params.first_best)
        run_type = 'first_best';
    elseif isfield(params,'use_fact_V') && params.use_fact_V
        run_type = 'second_best_use_fact';
    elseif nargin==4 && ~isempty(heu)
        run_type = 'second_best_backwards_induction';
    else
        run_type = 'second_best';
    end

    qc  = bsxfun(@times,q,params.pr_compat);
    
    if strcmp(run_type,'first_best')
        z   = norminv(1-q); % cutoff for standard normal
        fz  = normpdf(z);
        w_kit = bsxfun(@times,q.*params.mu + bsxfun(@times,params.sigma,fz),params.pr_compat);% E(mu+eps|mu+eps>cutoff)P(mu+eps>cutoff)
        w   = mean(w_kit,3);
        b   = a.*mean(qc,3) + params.discount_V1; 
        if nargout>1, pr_tx = bsxfun(@times,ones(size(params.mu)),params.pr_compat); end
    elseif strcmp(run_type,'second_best') || strcmp(run_type,'second_best_use_fact')
        z   = bsxfun(@times,bsxfun(@plus,params.mu,-V),1./params.sigma); % cutoff for standard normal
        fz  = normpdf(z);
        Fz  = normcdf(z);                        % Prob of accepting the offer
        if strcmp(run_type,'second_best')
            Emax = bsxfun(@times,params.sigma,Fz .* z + fz); % Emax(0,mu-V+eps)
            w   = mean(qc.*Emax,3);
            b   = params.discount_V2;
        else   % use_V_fact
            Emax = Fz .* params.mu +bsxfun(@times,params.sigma,fz); % E(mu+eps|mu+eps>cutoff)P(mu+eps>cutoff)
            w   = mean(qc.*Emax,3);
            b   = a.*mean(Fz.*qc,3) + params.discount_V1;
        end
        if nargout>1, pr_tx = bsxfun(@times,Fz,params.pr_compat); end
    elseif strcmp(run_type,'second_best_backwards_induction')
        b   = params.discount_V2;
        V   =   [V zeros(N_patients,1)]; % Add one more column of zeros (will be removed later)
        Vdiff = 0;
        for k = N_time:-1:1
            Vlb = V(:,k+1) ./b(:,k);  % discount one period
            Vub = inf;
            VV  = max(Vlb,V(:,k));
            ii  = 0;
            while norm(Vub-Vlb,'inf')>heu.tol && ii<heu.maxiter
                z   = bsxfun(@times,bsxfun(@plus,params.mu(:,k,:),-VV),1./params.sigma); % cutoff for standard normal
                fz  = normpdf(z);
                Fz  = normcdf(z);                        % Prob of accepting the offer
                Emax = bsxfun(@times,params.sigma,Fz .* z + fz); % Emax(0,mu-V+eps)
                w    = mean(qc(:,k,:).*Emax,3); 
                TV = (a(:,k) .* w + V(:,k+1))./b(:,k);
                Vub  = min(max(VV,TV),Vub);  % Want to find V = T(V), where T(V) is decreasing in V
                Vlb  = max(min(VV,TV),Vlb);  
                VV    = (Vub+Vlb)/2;
                ii    = ii + 1;
                % if ii==heu.maxiter, fprintf(':%d:%e:',k,norm(Vub-Vlb,'inf'));end
            end
            Vdiff  = max(Vdiff,norm(Vub-Vlb,'inf'));
            V(:,k) = TV; 
            if nargout>1, pr_tx(:,k,:) = bsxfun(@times,Fz,params.pr_compat); end
        end
        if isfield(heu,'verbose') && heu.verbose, fprintf('Backwards induction norm: %e\n',Vdiff); end
        V(:,N_time+1) = [];  % Remove the extra column of zeros
    end
    
    
    % Update V
    if strcmp(run_type,'second_best') || strcmp(run_type,'second_best_use_fact') || strcmp(run_type,'first_best')
        V = zeros(size(params.arrivals));
        V(:,end) = a(:,end) .* w(:,end) ./ b(:,end);
        for k=2:size(V,2)
            V(:,end+1-k) = (a(:,end+1-k) .* w(:,end+1-k) + V(:,end+2-k))./b(:,end+1-k);
        end
    end
end
    

