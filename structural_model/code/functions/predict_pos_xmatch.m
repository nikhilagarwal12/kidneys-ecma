% Function to calculate the probability of a positive crossmatch for a set
% of offers, conditional on being willing to accept

% Linear probability model estimated in STATA using NYRT sample; variables
% re-created here

% Inputs:
%   offers - Offer table with fields 'wlreg_audit_id_code','a_mm','b_mm','dr_mm'
%   pt_history - patient history record (should have cpra)
%   coefs_path - filepath specifying location of parameter estimates

% Output:
%   pr_pos_xmatch - vector containing the probability of a positive
%                   crossmatch for each offer

function pr_pos_xmatch = predict_pos_xmatch(offers,pt_history,patients,coefs_path)

    [~,pt_hist_ind] = ismember(offers.wlreg_audit_id_code,pt_history.wlreg_audit_id_code);
    [~,pt_ind] = ismember(offers.wl_id_code,patients.wl_id_code);
    N = size(offers,1);
    hlamis = offers.a_mm + offers.b_mm + offers.dr_mm;
    cons = ones(N,1);
    X = table(cons);

    % baseline covariates
    X.cpra = pt_history.cpra(pt_hist_ind);
    X.hla01 = hlamis == 0 | hlamis == 1;
    X.hla23 = hlamis == 2 | hlamis == 3;
    X.dr0 = offers.dr_mm==0;
    X.cprahla01 = X.cpra .* X.hla01;
    X.cprahla23 = X.cpra .* X.hla23;

    % additional covariates

    % cpra spline
    X.cpra0 = X.cpra==0;
    X.cpraabovep8 = X.cpra > .795;
    X.cpragtp8 = (X.cpra - .795) .* X.cpraabovep8;

    % dialysis time
    X.logdialysistimeatinit = log(min(max(offers.match_date - patients.dialysis_date(pt_ind),1)/365, 5000/365));
    X.logdialysistimeatinitgt5 = (X.logdialysistimeatinit - log(5)).*(X.logdialysistimeatinit > log(5));

    % patient age
    X.initage = patients.init_age(pt_ind);
    X.agegt35 = (X.initage - 35).*(X.initage > 35);
    X.agegt50 = (X.initage - 50).*(X.initage > 50);
 
    coefs = readtable(coefs_path,'Delimiter',',','ReadVariableNames',true);
    ind_pos_xmatch = zeros(N,1);
    for ii = 1:size(coefs,1)
        var = coefs{ii,'varname'};
        val = coefs{ii,'coef'};
        ind_pos_xmatch = ind_pos_xmatch + X.(var{1}) * val;
    end
    pr_pos_xmatch = normcdf(ind_pos_xmatch);
