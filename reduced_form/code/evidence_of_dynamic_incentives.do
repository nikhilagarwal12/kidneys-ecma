// This script looks for evidence of dynamic incentives in the NYRT data
// Differences in acceptance rates by blood type, sensitization, and age
// This script generate:
//      - Figure C.1: Offer and Acceptance Rate by CPRA
//      - Table E.7: Acceptance Rates and Recent Offers
//      - Table C.6: Evidence of Response to Dynamic Incentives

clear
set more off

**************************
* data locations
**************************
local data = "~/organs-star/primary_files/nyrt/"
local output = "~/organs-proj/common_output/reduced_form/dynamic_incentives/"
local figures = "`output'figures/"
local smoking_gun_nyrt_screened = "~/organs-star/processed_files/smoking_gun_nyrt_incl_screened.dta"

**************************
* covariates
**************************
local pt_chars = "female age_lt_18 age_18_35 age_35_50 age_50_65 wait_1_3yr wait_3_5yr wait_gt_5yr wait_x_1_3yr wait_x_3_5yr wait_x_gt_5yr dialysis_1_3yr dialysis_3_5yr dialysis_5_10yr dialysis_gt_10yr dialysis_x_1_3yr dialysis_x_3_5yr dialysis_x_5_10yr dialysis_x_gt_10yr unhealthy abo_A abo_B abo_AB diabetes"
local don_chars = "age_don creat_don creat_gt_p6 creat_gt_1p8 ecd_donor non_hrt diabetes_don"
local match_chars = "age_dif_neg age_dif_pos local abo_identical hlamis zero_hla zero_dr one_dr year_2010-year_2012"
local interactions = " pt_lt_18_don_gt_40 pt_18_35_don_gt_55 pt_35_50_don_gt_60 pt_50_65_don_lt_60 zero_hla_local age_don_local"
local other_vars = "wgt_kg_don_calc inv_reject inv_reject_810 cpra_0 wait_time wait_lt_1yr age_gt_65 kdri kdpi init_cpra end_cpra dialysis_time health_init no_dialysis ln_position age_guess listing_ctr"
local cpra_simple = "cpra cpra_0"
local priority_vars = "cpra_20_80 cpra_gt_80 age_lt_18 wait_1_3yr wait_3_5yr wait_gt_5yr wait_x_1_3yr wait_x_3_5yr wait_x_gt_5yr"
local cpra_bins = "cpra_20_80 cpra_gt_80"
local cpra_bins_hlamis = "cpra_20_80 cpra_gt_80 cpra_20_80_x_hlamis cpra_gt_80_x_hlamis"

*************************************
* control which analyses to run
*************************************
local prepare_data = 1
local Table_C_6 = 1
local Figure_C_1 = 1
local Table_E_7 = 1


**************************
* Data preparation
**************************
if `prepare_data'==1{
    
    // create main dataset
    use `data'simulated_offers_matlab, clear
    gen ln_position = log(position_dat)
    
    // was offer accepted?
    gen final_decision = accept_yn
    gen ref_810 = refusal_id == 810
    gen accept_810 = final_decision == 1 | ref_810 == 1
    
    // match characteristics
    gen local = geo == 1
    gen abo_identical = c_abo == 2
    gen zero_hla = a_mm + b_mm + dr_mm == 0
    gen hlamis = a_mm + b_mm + dr_mm
    rename dr_mm drmis
    gen non_drmis = a_mm + b_mm
    gen zero_dr = drmis==0
    gen one_dr = drmis==1
    forvalues x = 2010/2013{
        gen year_`x' = year(match_date)==`x'
        label var year_`x' "Year `x'"
    }
    
    // proxies for donor quality
    bys donor_id: egen n_accept = total(final_decision)
    by donor_id: egen n_accept_810 = total(accept_810)
    by donor_id: gen n_reject = _N - n_accept
    by donor_id: gen n_reject_810 = _N - n_accept_810
    gen inv_reject = 1/n_reject
    gen inv_reject_810 = 1/n_reject_810
    replace inv_reject = 2 if n_reject==0
    replace inv_reject_810 = 2 if n_reject_810==0

    // merge in patient characteristics
    merge m:1 wl_id_code using `data'patients, keep(master match) keepusing(abo init_date init_age diab init_cpra end_cpra dialysis_date func_stat_tcr gender listing_ctr) nogen
    gen diabetes = diab>1 & diab<=5

    gen female = gender=="F"
    gen wait_time = (match_date - init_date) / 365
    gen wait_lt_1yr = wait_time < 1
    gen wait_1_3yr = wait_time >= 1 & wait_time < 3
    gen wait_3_5yr = wait_time >= 3 & wait_time < 5
    gen wait_gt_5yr = wait_time >= 5
    gen wait_x_1_3yr = (wait_time - 1) * wait_1_3yr
    gen wait_x_3_5yr = (wait_time - 3) * wait_3_5yr
    gen wait_x_gt_5yr = (wait_time - 5) * wait_gt_5yr

    gen dialysis_time = (match_date - dialysis_date) / 365
    replace dialysis_time = 0 if dialysis_time == .
    gen no_dialysis = dialysis_time == .
    gen dialysis_lt_1yr = dialysis_time < 1
    gen dialysis_1_3yr = dialysis_time >= 1 & dialysis_time < 3
    gen dialysis_3_5yr = dialysis_time >= 3 & dialysis_time < 5
    gen dialysis_5_10yr = dialysis_time >= 5 & dialysis_time < 10
    gen dialysis_gt_10yr = dialysis_time >= 10
    gen dialysis_x_1_3yr = (dialysis_time - 1) * dialysis_1_3yr
    gen dialysis_x_3_5yr = (dialysis_time - 3) * dialysis_3_5yr
    gen dialysis_x_5_10yr = (dialysis_time - 5) * dialysis_5_10yr
    gen dialysis_x_gt_10yr = (dialysis_time - 10) * dialysis_gt_10yr

    gen health_init = func_stat_tcr - 2000
    replace health_init = health_init - 2000 if health_init > 100
    replace health_init = health_init / 100
    replace health_init = . if health_init < 0
    gen unhealthy = health_init < .5
    
    gen age_guess = init_age + 0.5 + wait_time/365  // recomputing age_guess
    gen age_lt_18 = age_guess <= 18  // weak inequality because you retain pediatric priority after you turn 18
    gen age_18_35 = age_guess > 18 & age_guess < 35
    gen age_35_50 = age_guess >= 35 & age_guess < 50
    gen age_50_65 = age_guess >= 50 & age_guess < 65
    gen age_gt_65 = age_guess >= 65

    gen abo_A = abo == "A" | abo == "A1"
    gen abo_AB = abo == "AB" | abo == "A1B" | abo == "A2B"
    gen abo_B = abo == "B"

    
    // merge in patient CPRA
    merge m:1 wlreg_audit_id_code using `data'patient_history, assert(match using) keep(match) keepusing(cpra) nogen
    gen cpra_0 = cpra == 0
    gen cpra_lt_20 = cpra < .195
    gen cpra_20_80 = cpra >= .195 & cpra < .795
    gen cpra_gt_80 = cpra >= .795
    gen cpra_80_90 = cpra >= .795 & cpra < .895
    gen cpra_80_95 = cpra >= .795 & cpra < .945
    gen cpra_gt_90 = cpra >= .895
    gen cpra_gt_95 = cpra >= .945
    gen cpra_80_85 = cpra >= .795 & cpra < .845
    gen cpra_85_90 = cpra >= .845 & cpra < .895
    gen cpra_90_95 = cpra >= .895 & cpra < .945
    gen cpra_x_lt_20 = cpra * cpra_lt_20
    gen cpra_x_20_80 = (cpra - .195) * cpra_20_80
    gen cpra_x_80_90 = (cpra - .795) * cpra_80_90
    gen cpra_x_80_95 = (cpra - .795) * cpra_80_95
    gen cpra_x_gt_90 = (cpra - .895) * cpra_gt_90
    gen cpra_x_gt_95 = (cpra - .945) * cpra_gt_95

    // Interactions between CPRA and # HLA, DR, non-DR mismatches
    gen hlamis_3_4 = hlamis==3|hlamis==4
    gen hlamis_5_6 = hlamis==5|hlamis==6
    foreach var in hlamis hlamis_3_4 hlamis_5_6 drmis non_drmis{
        gen cpra_20_80_x_`var' = cpra_20_80 * `var'
        gen cpra_gt_80_x_`var' = cpra_gt_80 * `var'
    }    
    
    // merge in donor characteristics
    merge m:1 donor_id using `data'deceased_donors, keep(master match) keepusing(ecd_donor non_hrt_don kdri kdpi age_don hist_diabetes_don creat_don wgt_kg_don_calc hep_c_anti_don) nogen
    gen non_hrt = non_hrt_don == "Y"
    gen diabetes_don = hist_diabetes_don <= 5 & hist_diabetes_don >= 2

    // donor creatinine
    gen creat_gt_p6 = (creat_don - 0.6) * (creat_don > 0.6)
    gen creat_gt_1p8 = (creat_don - 1.8) * (creat_don > 1.8)
        
    // interactions between donor and patient age
    gen age_dif = age_don - age_guess
    gen age_dif_neg = age_dif * (age_dif < 0)
    gen age_dif_pos = age_dif * (age_dif >= 0)
    gen pt_lt_18_don_gt_40 = age_lt_18 * (age_don >= 40) * (age_don - 40)  // Pediatric candidates don't accept patients over 40
    gen pt_18_35_don_gt_55 = age_18_35 * (age_don >= 55) * (age_don - 55)
    gen pt_35_50_don_gt_60 = age_35_50 * (age_don >= 60) * (age_don - 60)
    gen pt_50_65_don_lt_60 = age_50_65 * (age_don <= 60) * (60 - age_don)  // For 50-65 patients, donor age only matters above 60

    gen age_don_local = local * age_don
    gen zero_hla_local = local * zero_hla
    
    // label variables
    label var female "Female Patient"
    label var age_guess "Patient Age"
    label var age_don "Donor Age"
    label var age_don_local "Donor Age * 1{Local}"
    label var age_lt_18 "Patient Pediatric"
    label var age_18_35 "Patient Age 19-34"
    label var age_35_50 "Patient Age 35-49"
    label var age_50_65 "Patient Age 50-64"
    label var age_gt_65 "Patient Over 65"
    label var pt_lt_18_don_gt_40 "Donor Age * 1{Donor >= 40 Pediatric Patient}"
    label var pt_18_35_don_gt_55 "Donor Age * 1{Donor >= 55 Patient 18-35}"
    label var pt_35_50_don_gt_60 "Donor Age * 1{Donor >= 60 Patient 35-50}"
    label var pt_50_65_don_lt_60 "Donor Age * 1{Donor <= 60 Patient 50-65}"
    label var age_dif_neg "Age Difference * 1{Donor Younger}"
    label var age_dif_pos "Age Difference * 1{Donor Older}"
    label var cpra_0 "CPRA 0%"
    label var cpra_lt_20 "CPRA < 20%"
    label var cpra_20_80 "CPRA 20-80%"
    label var cpra_gt_80 "CPRA >= 80%"
    label var cpra_80_90 "CPRA 80-90%"
    label var cpra_80_95 "CPRA 80-95%"
    label var cpra_gt_90 "CPRA >= 90%"
    label var cpra_gt_95 "CPRA >= 95%"
    label var cpra_80_85 "CPRA 80-85%"
    label var cpra_85_90 "CPRA 85-90%"
    label var cpra_90_95 "CPRA 90-95%"
    label var cpra_x_lt_20 "Linear CPRA < 20%"
    label var cpra_x_20_80 "Linear CPRA 20-80%"
    label var cpra_x_80_90 "Linear CPRA 80-90%"
    label var cpra_x_80_95 "Linear CPRA 80-95%"
    label var cpra_x_gt_90 "Linear CPRA >= 90%"
    label var cpra_x_gt_95 "Linear CPRA >= 95%"
    label var cpra_20_80_x_hlamis "CPRA 20-80% x HLAMIS"
    label var cpra_20_80_x_hlamis_3_4 "CPRA 20-80% x 3-4 HLAMIS"
    label var cpra_20_80_x_hlamis_5_6 "CPRA 20-80% x 5-6 HLAMIS"
    label var cpra_20_80_x_drmis "CPRA 20-80% x DRMIS"
    label var cpra_20_80_x_non_drmis "CPRA 20-80% x non-DRMIS"
    label var cpra_gt_80_x_hlamis "CPRA >= 80% x HLAMIS"
    label var cpra_gt_80_x_hlamis_3_4 "CPRA >= 80% x 3-4 HLAMIS"
    label var cpra_gt_80_x_hlamis_5_6 "CPRA >= 80% x 5-6 HLAMIS"
    label var cpra_gt_80_x_drmis "CPRA >= 80% x DRMIS"
    label var cpra_gt_80_x_non_drmis "CPRA >= 80% x non-DRMIS"
    label var abo_A "Type A"
    label var abo_B "Type B"
    label var abo_AB "Type AB"
    label var diabetes "Patient Diabetic"
    label var kdri "Donor KDRI"
    label var age_don "Donor Age"
    label var creat_don "Donor Creatinine"
    label var creat_gt_p6 "Creatinine * 1{Creatinine >= 0.6}"
    label var creat_gt_1p8 "Creatinine * 1{Creatinine >= 1.8}"
    label var ecd_donor "ECD Donor"
    label var non_hrt "Non-Heart Beating Donor"
    label var diabetes_don "Donor Diabetic"
    label var local "Local Offer"
    label var abo_identical "Identical Blood Type"
    label var zero_hla "0 ABDR Mismatch"
    label var zero_hla_local "1{0 ABDR Mismatch} * 1{Local}"
    label var hlamis "# HLA Mismatches"
    label var hlamis_3_4 "3-4 HLAMIS"
    label var hlamis_5_6 "5-6 HLAMIS"
    label var drmis "# DR Mismatches"
    label var non_drmis "# non-DR Mismatches"
    label var zero_dr "0 DR Mismatch"
    label var one_dr "1 DR Mismatch"
    label var wait_time "Waiting Time (Years)"
    label var wait_lt_1yr "Waited < 1 Year"
    label var wait_1_3yr "Waited 1-3 Years"
    label var wait_3_5yr "Waited 3-5 Years"
    label var wait_gt_5yr "Waited >= 5 Years"
    label var wait_x_1_3yr "Linear Waited 1-3 Years"
    label var wait_x_3_5yr "Linear Waited 3-5 Years"
    label var wait_x_gt_5yr "Linear Waited >= 5 Years"
    label var health_init "Health at Listing"
    label var unhealthy "Poor Health at Listing"
    label var no_dialysis "No Dialysis Date"
    label var dialysis_lt_1yr "On Dialysis < 1 Year"
    label var dialysis_1_3yr "On Dialysis 1-3 Years"
    label var dialysis_3_5yr "On Dialysis 3-5 Years"
    label var dialysis_5_10yr "On Dialysis 5-10 Years"
    label var dialysis_gt_10yr "On Dialysis > 10 Years"
    label var dialysis_x_1_3yr "Linear Dialysis 1-3 Years"
    label var dialysis_x_3_5yr "Linear Dialysis 3-5 Years"
    label var dialysis_x_5_10yr "Linear Dialysis 5-10 Years"
    label var dialysis_x_gt_10yr "Linear Dialysis > 10 Years"
    label var ln_position "Log(Offer Number)"
    label var inv_reject "1 / (# rejections)"
    label var inv_reject_810 "1 / (# rejections) excluding positive crossmatches"
    label var accept_810 "Offer Accepted, or Positive Crossmatch"
    label var ref_810 "Positive Crossmatch"
    
    keep donor_id match_date wl_id_code wlreg_audit_id_code position_dat final_decision accept_810 ref_810 cpra* `match_chars' `don_chars' `pt_chars' `interactions' `other_vars' screened hep_c_anti_don
    save `smoking_gun_nyrt_screened', replace
}


*********************************************************
* Figure C.1 (right): Offer and Acceptance Rate by CPRA
*********************************************************
if `Figure_C_1' {
    
    * ------------------
    * Right pane
    * ------------------
    use `smoking_gun_nyrt_screened', clear

    local bin_size = 10
    gen cpra_bin = `bin_size'*floor(cpra*100/`bin_size') + `bin_size'/2
    replace cpra_bin = `bin_size'*floor(99/`bin_size') + `bin_size'/2 if cpra > .995
    replace  cpra_bin = 0 if cpra == 0
    
    gen count = 1
    collapse (sum) count (mean) rate = accept_810, by(cpra_bin)
    
    gen ub = rate + 1.96*sqrt(rate*(1-rate)/count)
    gen lb = rate - 1.96*sqrt(rate*(1-rate)/count)
    
    replace rate = rate*100   // turn to %
    replace ub = ub*100
    replace lb = lb*100
    
    graph twoway (rarea ub lb cpra_bin, lwidth(none) fcolor(gs12)) (line rate cpra_bin, lcolor(black)), legend(off) title("") xtitle("Calculated Panel Reactive Antibody (CPRA)") ytitle("Offer Acceptance Rate (%)", height(10)) note("") graphregion(fcolor(white))
    graph export `figures'acceptance_rate_by_cpra.eps, replace


    * ------------------
    * Left pane
    * ------------------
    use ~/organs-star/primary_files/nyrt/patient_history, clear
    keep if chg_date <= mdy(12,31,2013)
    gen mark_2010 = chg_date < mdy(1,1,2010) & chg_till >= mdy(1,1,2010)
    gen mark_2011 = chg_date < mdy(1,1,2011) & chg_till >= mdy(1,1,2011)
    gen mark_2012 = chg_date < mdy(1,1,2012) & chg_till >= mdy(1,1,2012)
    gen mark_2013 = chg_date < mdy(1,1,2013) & chg_till >= mdy(1,1,2013)

    local bin_size = 10
    local nbins = 100/`bin_size'

    
    keep if mark_2012 & active==1
    gen cpra_bin = `bin_size'*floor(cpra*100/`bin_size') + `bin_size'/2
    replace cpra_bin = `bin_size'*floor(99/`bin_size') + `bin_size'/2 if cpra > .995
    replace cpra_bin = 0 if cpra == 0
    collapse (sum) pt_count = active, by(cpra_bin)
    tempfile cpra_dist_patient
    save `cpra_dist_patient', replace
    use `smoking_gun_nyrt_screened', clear
    keep if year_2012==1
    
    gen active = 1
    gen cpra_bin = `bin_size'*floor(cpra*100/`bin_size') + `bin_size'/2
    replace cpra_bin = `bin_size'*floor(99/`bin_size') + `bin_size'/2 if cpra > .995
    replace  cpra_bin = 0 if cpra == 0
    gen low_kdri = kdri <= 1.48
    gen low_kdri_local = low_kdri * local
    collapse (sum) offer_count = active (sum) local_count = local (sum) low_kdri_count = low_kdri (sum) low_kdri_local_count = low_kdri_local, by(cpra_bin)
    merge 1:1 cpra_bin using `cpra_dist_patient', assert(match) nogen

    gen offers_per_patient = offer_count / pt_count
    gen ub = offers_per_patient + 1.96*sqrt(offer_count) / pt_count
    gen lb = offers_per_patient - 1.96*sqrt(offer_count) / pt_count
    graph twoway (rarea ub lb cpra_bin, lwidth(none) fcolor(gs12)) (line offers_per_patient cpra_bin, lc(black)), legend(off) title("") xtitle("Sensitization (CPRA)") ytitle("# Offers per Patient") note("") graphregion(fcolor(white))
    graph export `figures'offer_rate_by_cpra_bin.eps, replace    
}


************************************************************
* Table C.6: Evidence of Response to Dynamic Incentives
************************************************************
if `Table_C_6' {
    eststo clear

    use `smoking_gun_nyrt_screened', clear
    tabulate listing_ctr, gen(listing_ctr_dummy)

    summ accept_810
    local sample_avg = r(mean)

    eststo clear
    * Just linear CPRA, no controls
    eststo: reg accept_810 `cpra_simple', cluster(donor_id)
    estadd local sample_average `sample_avg'

    * Controls for Patient's Priority
    eststo: reg accept_810 `cpra_simple' `priority_vars', cluster(donor_id)
    estadd local sample_average `sample_avg'

    * Just patient and priority controls
    eststo: reg accept_810 `cpra_simple' `cpra_bins' `pt_chars' listing_ctr_dummy*, cluster(donor_id)
    estadd local sample_average `sample_avg'

    * Full set of controls
    eststo: reg accept_810 `cpra_simple' `cpra_bins' `pt_chars' `don_chars' `match_chars' `interactions' inv_reject_810 listing_ctr_dummy*, cluster(donor_id)
    estadd local sample_average `sample_avg'

    * CPRA bins x Linear # HLA mismatches
    eststo: reg accept_810 `cpra_simple' `cpra_bins_hlamis' `pt_chars' `don_chars' `match_chars' `interactions' inv_reject_810 listing_ctr_dummy*, cluster(donor_id)
    estadd local sample_average `sample_avg'

    * export
    esttab using `output'acceptance_by_cpra.csv, se r2 replace label title("Acceptance by CPRA") mtitles("No Controls" "Patient Priority" "Patient Controls" "Full Controls" "CPRA bin x HLAMIS") scalars("sample_average Mean Acceptance Rate")
    * format and restore to xls
    import delimited using `output'acceptance_by_cpra.csv, delimiter(",") clear
    foreach var of varlist _all {
        replace `var' = subinstr(`var', "=", "",.)
        replace `var' = subinstr(`var',`"""',"",.)
        replace `var' = subinstr(`var',"_","",.)
    }
    export excel using `output'evidence_of_dynamic_incentives.xls, sheet("acceptance_raw") sheetreplace
    cap shell rm `output'acceptance_by_cpra.csv

}


******************************************************
* Table E.7: Acceptance Rates and Recent Offers
******************************************************
if `Table_E_7' {
    eststo clear

    use `smoking_gun_nyrt_screened', clear
    tabulate listing_ctr, gen(listing_ctr_dummy)

    // Create variables related to past offers
    sort wl_id_code match_date accept_810

    by wl_id_code: gen has_1_past_offers = _n>1  // should be in sample
    by wl_id_code: gen time_since_last_1 = (match_date - match_date[_n-1])/365.25
    by wl_id_code: gen last_offer_age_1 = age_don[_n-1]
    by wl_id_code: gen last_offer_diabetic_1 = diabetes_don[_n-1]
    by wl_id_code: gen last_offer_ecd_1 = ecd_donor[_n-1]
    by wl_id_code: gen last_offer_dcd_1 = non_hrt[_n-1]

    by wl_id_code: gen has_2_past_offers = _n>2  // should be in sample
    by wl_id_code: gen time_since_last_2 = (match_date - match_date[_n-2])/(365.25*2) 
    by wl_id_code: gen last_offer_age_2 = (age_don[_n-1] + age_don[_n-2])/2
    by wl_id_code: gen last_offer_diabetic_2 = (diabetes_don[_n-1] + diabetes_don[_n-2])/2
    by wl_id_code: gen last_offer_ecd_2 = (ecd_donor[_n-1] + ecd_donor[_n-2])/2
    by wl_id_code: gen last_offer_dcd_2 = (non_hrt[_n-1] + non_hrt[_n-2])/2

    by wl_id_code: gen has_5_past_offers = _n>5  // should be in sample
    by wl_id_code: gen time_since_last_5 = (match_date - match_date[_n-5])/(365.25*5) 
    by wl_id_code: gen last_offer_age_5 = (age_don[_n-1] + age_don[_n-2] + age_don[_n-3] + age_don[_n-4] + age_don[_n-5])/5
    by wl_id_code: gen last_offer_diabetic_5 = (diabetes_don[_n-1] + diabetes_don[_n-2] + diabetes_don[_n-3] + diabetes_don[_n-4] + diabetes_don[_n-5])/5
    by wl_id_code: gen last_offer_ecd_5 = (ecd_donor[_n-1] + ecd_donor[_n-2] + ecd_donor[_n-3] + ecd_donor[_n-4] + ecd_donor[_n-5])/5
    by wl_id_code: gen last_offer_dcd_5 = (non_hrt[_n-1] + non_hrt[_n-2] + non_hrt[_n-3] + non_hrt[_n-4] + non_hrt[_n-5])/5

    // subroutine to count active time since last N offers
    by wl_id_code: gen match_date_last = match_date[_n-1]
    by wl_id_code: gen match_date_last_2 = match_date[_n-2]
    by wl_id_code: gen match_date_last_5 = match_date[_n-5]

    isid donor_id wl_id_code
    tempfile offers_and_stuff
    save `offers_and_stuff', replace
    
    use wl_id_code wlreg_audit_id_code unos_cand_stat_cd chg_date chg_time chg_ty active using `data'patient_history, clear
    replace active = 0 if chg_ty=="D"
    
    bys wl_id_code (chg_date chg_time): gen drop_this = active==active[_n-1] if _n>1
    drop if drop_this == 1
    by wl_id_code: gen chg_till = chg_date[_n+1]
    replace chg_till = mdy(1,1,2016) if chg_till==.

    drop if chg_date > mdy(12,31,2013) | chg_till < mdy(1,1,2010)  // drop records outside sample window
    drop if active==1
    keep wl_id_code wlreg_audit_id_code chg_date chg_till
    desc
    tempfile inactive_periods
    save `inactive_periods', replace

    use `offers_and_stuff', clear
    joinby wl_id_code using `inactive_periods'

    gen overlap_last = max(min(chg_till,match_date) - max(chg_date,match_date_last),0)
    gen overlap_last_2 = max(min(chg_till,match_date) - max(chg_date,match_date_last_2),0)
    gen overlap_last_5 = max(min(chg_till,match_date) - max(chg_date,match_date_last_5),0)

    bys wl_id_code donor_id: egen inactive_time_last = total(overlap_last)
    bys wl_id_code donor_id: egen inactive_time_last_2 = total(overlap_last_2)
    bys wl_id_code donor_id: egen inactive_time_last_5 = total(overlap_last_5)
    
    keep wl_id_code donor_id inactive_time_last inactive_time_last_2 inactive_time_last_5
    duplicates drop
    tempfile inactive_time
    save `inactive_time', replace

    use `offers_and_stuff', clear
    merge 1:1 wl_id_code donor_id using `inactive_time', assert(master match)
    replace inactive_time_last = 0 if _merge == 1
    replace inactive_time_last_2 = 0 if _merge == 1
    replace inactive_time_last_5 = 0 if _merge == 1
    drop _merge

    gen time_since_last_1_active = time_since_last_1 - inactive_time_last/365.25
    gen time_since_last_2_active = time_since_last_2 - inactive_time_last_2/(365.25*2)
    gen time_since_last_5_active = time_since_last_5 - inactive_time_last_5/(365.25*5)


    label var time_since_last_1_active "Time Since Last Offer (Years)"
    label var time_since_last_1 "Time Since Last Offer Including Inactive Periods"
    label var last_offer_age_1 "Last Offer Donor Age"
    label var last_offer_diabetic_1 "Last Offer from Diabetic Donor"
    label var last_offer_ecd_1 "Last Offer from Expanded Criteria Donor"
    label var last_offer_dcd_1 "Last Offer from Donation after Cardiac Death"

    label var time_since_last_2_active "Time Since Last Two Offers (Years)"
    label var time_since_last_2 "Time Since Last Two Offers Including Inactive Periods"
    label var last_offer_age_2 "Last Two Offers Donor Age"
    label var last_offer_diabetic_2 "Last Two Offers from Diabetic Donor"
    label var last_offer_ecd_2 "Last Two Offers from Expanded Criteria Donor"
    label var last_offer_dcd_2 "Last Two Offers from Donation after Cardiac Death"

    label var time_since_last_5_active "Time Since Last Five Offers (Years)"
    label var time_since_last_5 "Time Since Last Five Offers Including Inactive Periods"
    label var last_offer_age_5 "Last Five Offers Donor Age"
    label var last_offer_diabetic_5 "Last Five Offers from Diabetic Donor"
    label var last_offer_ecd_5 "Last Five Offers from Expanded Criteria Donor"
    label var last_offer_dcd_5 "Last Five Offers from Donation after Cardiac Death"
    
    local past_vars = "time_since_last_1_active time_since_last_2_active time_since_last_5_active time_since_last_1 time_since_last_2 time_since_last_5 last_offer_age_1 last_offer_diabetic_1 last_offer_ecd_1 last_offer_dcd_1 last_offer_age_2 last_offer_diabetic_2 last_offer_ecd_2 last_offer_dcd_2 last_offer_age_5 last_offer_diabetic_5 last_offer_ecd_5 last_offer_dcd_5"

    local donor_vars_1 = "last_offer_age_1 last_offer_diabetic_1 last_offer_ecd_1 last_offer_dcd_1"
    local donor_vars_2 = "last_offer_age_2 last_offer_diabetic_2 last_offer_ecd_2 last_offer_dcd_2"
    local donor_vars_5 = "last_offer_age_5 last_offer_diabetic_5 last_offer_ecd_5 last_offer_dcd_5"

    summ accept_810 if has_1_past_offers
    local sample_avg_1 = r(mean)
    summ accept_810 if has_2_past_offers
    local sample_avg_2 = r(mean)
    summ accept_810 if has_5_past_offers
    local sample_avg_5 = r(mean)

    summ time_since_last_1_active if has_1_past_offers
    local mean_time_1 = r(mean)
    local sd_time_1 = r(sd)
    summ time_since_last_2_active if has_2_past_offers
    local mean_time_2 = r(mean)
    local sd_time_2 = r(sd)
    summ time_since_last_5_active if has_5_past_offers
    local mean_time_5 = r(mean)
    local sd_time_5 = r(sd)


    // (column to ignore) for formatting reason only
    eststo: reg accept_810 `past_vars' `cpra_simple' `cpra_bins_hlamis' `pt_chars' `don_chars' `match_chars' `interactions' inv_reject_810 listing_ctr_dummy* `weight_string', cluster(donor_id)

    // LAST OFFER （No controls）
    eststo: reg accept_810 time_since_last_1_active if has_1_past_offers, cluster(donor_id)
    estadd scalar sample_average = `sample_avg_1'
    estadd scalar mean_time = `mean_time_1'
    estadd scalar sd_time = `sd_time_1'
    local ii = `ii'+1

    // LAST OFFER (Full Controls)
    eststo: reg accept_810 time_since_last_1_active `cpra_simple' `pt_chars' `don_chars' `match_chars' `interactions' inv_reject_810 listing_ctr_dummy* if has_1_past_offers, cluster(donor_id)
    estadd scalar sample_average = `sample_avg_1'
    estadd scalar mean_time = `mean_time_1'
    estadd scalar sd_time = `sd_time_1'
    local ii = `ii'+1

    // LAST 2 OFFERS
    eststo: reg accept_810 time_since_last_2_active `cpra_simple' `pt_chars' `don_chars' `match_chars' `interactions' inv_reject_810 listing_ctr_dummy* if has_2_past_offers, cluster(donor_id)
    estadd scalar sample_average = `sample_avg_2'
    estadd scalar mean_time = `mean_time_2'
    estadd scalar sd_time = `sd_time_2'
    local ii = `ii'+1

    // LAST 5 OFFERS
    eststo: reg accept_810 time_since_last_5_active `cpra_simple' `pt_chars' `don_chars' `match_chars' `interactions' inv_reject_810 listing_ctr_dummy* if has_5_past_offers, cluster(donor_id)
    estadd scalar sample_average = `sample_avg_5'
    estadd scalar mean_time = `mean_time_5'
    estadd scalar sd_time = `sd_time_5'
    local ii = `ii'+1

    // LAST OFFER （Full Controls）
    eststo: reg accept_810 time_since_last_1_active `donor_vars_1' `cpra_simple' `pt_chars' `don_chars' `match_chars' `interactions' inv_reject_810 listing_ctr_dummy* if has_1_past_offers, cluster(donor_id)
    estadd scalar sample_average = `sample_avg_1'
    estadd scalar mean_time = `mean_time_1'
    estadd scalar sd_time = `sd_time_1'
    local ii = `ii'+1

    
    // Write to .csv file
    esttab using `output'past_offers.csv, se r2 replace label title("Acceptance with Previous Offer Characteristics") mtitles("Ignore" "No Controls" "Full Controls" "Last 2 Offers" "Last 5 Offers" "Full Controls") scalars("sample_average Mean Acceptance Rate" "mean_time Mean Years Since Last N Offers" "sd_time S.D. Years Since Last N Offers")
    // Process output for table
    import delimited using `output'past_offers.csv, delimiter(",") stringcols(_all) clear
    ds, alpha
    local vars = "`r(varlist)'"
    foreach var in `vars'{
        replace `var' = subinstr(`var',"_","",.)
        replace `var' = subinstr(`var',"*","",.)
        replace `var' = subinstr(`var',`"""',"",.)
        replace `var' = subinstr(`var',"=","",.)
    }
    export excel using `output'past_offer_analysis.xlsx, sheet("raw", replace)
    cap shell rm `output'past_offers.csv
}



