% Parsimonious CCP specification (sensitivity check)

model = struct();
priors = struct();

% Load model that we will simplify 
model_file_nyrt_fix_time_uh

% Filepath
model.model_name = 'nyrt_fix_time_uh_sparse';
model.gen_script = mfilename('fullpath'); % name of script that generated model file (i.e. name of this script)
model.gen_date = datetime('today'); % date model file was generated

% Model structure
model.donor_uh = true;


model.fixed.linear = {'cpra','cpra_gt80','cpra_g_80', ...
		      'don_age_ge_50','ecd_donor','dcd', ...
		      'nyrt_donor','zero_mm','abo_compatible',...
		      'on_dialysis_at_init_date','log_wait_time'};,


model.fixed.pwlin = {{'init_age',18}};

model.fixed.interactions ={{'zero_mm','cpra'},...
                           {'zero_mm','ecd_donor'},...
                           {'zero_mm','dcd'},...
                           {'nyrt_donor','don_age_ge_50'}};

% store model in /structural_model/input/ (../input/)
save([models_folder model.model_name],'model','priors');
