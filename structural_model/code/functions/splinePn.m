function [Pn,sp]=splinePn(knots,X,order);

% Define number
ns=length(knots)-2*order;
assert(ns>0);

% Construct the spline
sp= struct('form','B-','order',order,'number',ns,'dim',1,...
    'knots',knots);

Pn = zeros(numel(X),1);

len = 0;
for i=1:sp.number
        sp.coefs=zeros(1,sp.number);
        sp.coefs(i)=1;
        spl=fnval(sp,X);
        if any(spl>0)
            len=len+1;
            Pn(:,len) = spl;
        end
end
