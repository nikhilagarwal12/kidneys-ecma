% Split patients and donors in groups for analysis.

clear

% Filepath
group_folder = '/proj/organs/common_output/structural/input/';
group.name = 'patient_donor_age';
group.gen_script = mfilename('fullpath'); % name of script that generated model file (i.e. name of this script)
group.gen_date = datetime('today'); % date model file was generated

donor_age_groups = [0 11 18 35 50 65 100];
pt_age_groups =    [0 11 18 35 50 65 100];

group.groups = cell((numel(donor_age_groups)-1)*(numel(pt_age_groups)-1),1);
g = 0;
for d = 1:(numel(donor_age_groups)-1)
	for p = 1:(numel(pt_age_groups)-1)
		g = g+1;
		group.groups{g}.label = ['Age: Donor ' num2str(donor_age_groups(d)) '-' num2str(donor_age_groups(d+1)) ' Patient ' num2str(pt_age_groups(p)) '-' num2str(pt_age_groups(p+1)) ];
		group.groups{g}.conditions(1).data = 'patients';
		group.groups{g}.conditions(1).variables= {'init_age'};
		group.groups{g}.conditions(1).f = @(x) (pt_age_groups(p) <= x & x < pt_age_groups(p+1));
		
		group.groups{g}.conditions(2).data = 'donors';
		group.groups{g}.conditions(2).variables = {'age_don'};
		group.groups{g}.conditions(2).f = @(x) (donor_age_groups(d) <= x & x < donor_age_groups(d+1));
	end
end

save([group_folder group.name],'group');

