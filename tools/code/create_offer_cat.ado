program create_offer_cat

gen offer_cat = 1 if zero_abdr_t == "Zero"
replace offer_cat = 2 if offer_cat ==. & (geo_t == "Local" | geo_t == "OPO" | geo_t == "Statewide")
replace offer_cat = 3 if offer_cat ==. & (geo_t == "KAS Local")
replace offer_cat = 4 if offer_cat ==. & (geo_t == "Payback" | geo_t == "UNOS" | geo_t == "SEOPF")
replace offer_cat = 5 if offer_cat ==. & geo_t == "Regional"
replace offer_cat = 6 if offer_cat ==. & geo_t == "KAS Regional"
replace offer_cat = 7 if offer_cat ==. & geo_t == "National"
replace offer_cat = 8 if offer_cat ==. & geo_t == "KAS National"
replace offer_cat = 0 if offer_cat ==.

label define offer_cat_label 0 "Unknown" 1 "Zero" 2 "Local" 3 "KAS Local" 4 "Payback" 5 "Regional" 6 "KAS Regional" 7 "National" 8 "KAS National"
label values offer_cat offer_cat_label

end
