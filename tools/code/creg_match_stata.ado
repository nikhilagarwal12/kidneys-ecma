// Wrapper for python creg code, to be called from STATA
// Expects a dataset of the format of offers.dta in the primary files

program creg_match_stata

args python auxiliary_files

  display "`python'"

  tempfile temp_creg
  save `temp_creg'

if `python'==1{  // run this if creg matches need to be re-generated; otherwise take output from past runs

  keep donor_id wl_id_code organ_sequence_num a1 a2 b1 b2 da1 da2 db1 db2
  sort donor_id organ_sequence_num

  export delimited using "`auxiliary_files'antigens_from_primary_files.csv", delimiter(",") replace
  display "input saved"

  shell python ../../mechanism/code/creg_match_forstata.py

  display "python script completed"
}
  import delimited using "`auxiliary_files'creg_matches.csv", varnames(1) clear

  merge 1:1 donor_id organ_sequence_num using `temp_creg'
  drop _merge

end
