% Function to compute offer statistics for pairs of patients
% This script generates Table E.9: Patient Ordering Statistics

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table E.9: Patient Ordering Statistics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear

% ------------------------------
% Data location
% ------------------------------
addpath('../../structural_model/code/functions/');
output_folder = '/proj/organs/common_output/reduced_form/';
load('~/organs-star/primary_files/nyrt/simulated_offers_matlab.mat');

% ------------------------------
% Analysis
% ------------------------------

offers = offers(offers.match_date <= min(offers.match_date) + 4*365 + 1,:);
wl_id_code = sort(unique(offers.wl_id_code));
donor_w_date = unique([offers.match_date offers.donor_id],'rows');
[donor_id,ia,ic] = unique(donor_w_date(:,2),'stable');
N = size(offers,1);  % total # offers
II = length(wl_id_code);
JJ = length(donor_id);

% assign consecutive patient and donor numbers
pt_nums = zeros(N,1);
don_nums = zeros(N,1);

tic
display('Assigning Consecutive Patient IDs')
for ii=1:II
    pt_nums(wl_id_code(ii)==offers.wl_id_code) = ii;
end
toc

tic
display('Assigning Consecutive Donor IDs')
for jj=1:JJ
    don_nums(donor_id(jj)==offers.donor_id) = jj;
end
toc

offer_mat = double([don_nums pt_nums offers.position_sim]);
offer_mat = sortrows(offer_mat,[1 3]);

% construct pair statistics by looping through donors, and then offers within a donor
tic
display('Computing Pair Statistics')
pair_stats = zeros(II*II,2);   % one row for each (pt1,pt2) pair
for jj = 1:JJ
    offers_jj = offer_mat(offer_mat(:,1)==jj,:);
    for ii=1:size(offers_jj,1);
        pt_ind = offers_jj(ii,2);
        pair_ind = (pt_ind-1)*II + offers_jj(:,2);
        pair_stats(pair_ind,1) = pair_stats(pair_ind,1) + 1;
        pair_stats(pair_ind,2) = pair_stats(pair_ind,2) + ((1:size(offers_jj,1)) <= ii)';
    end  % over offers
end  % over donors
toc

tic
display('Trimming to Relevant Pairs')
% patient numbers of each pair
pair_nums = zeros(II*II,2);  
pair_nums(:,1) = ceil((1:(II*II))' / II);
pair_nums(:,2) = mod(1:(II*II),II);
pair_nums(pair_nums(:,2)==0,2) = II;

% de-duplicate pairs
pair_dups = pair_nums(:,1) <= pair_nums(:,2);
pair_nums(pair_dups,:) = [];
pair_stats(pair_dups,:) = [];

% also drop pairs with no offers from the same donor
pair_drop = pair_stats(:,1)==0;
pair_nums(pair_drop,:) = [];
pair_stats(pair_drop,:) = [];
toc

% How often is the patient currently ahead also ahead other times?
freq_ahead_given_ahead = ((pair_stats(:,1) - pair_stats(:,2)).^2 + pair_stats(:,2).^2) ./ pair_stats(:,1);
freq_ahead_stat = sum(freq_ahead_given_ahead)/sum(pair_stats(:,1));

% logical array of which patients were offered to which donors
offers_wide = false(II,JJ);
positions_wide = zeros(II,JJ);
for ii=1:II
    index_ii = offer_mat(:,2)==ii;
    offers_wide(ii,offer_mat(index_ii,1)) = true;
    positions_wide(ii,offer_mat(index_ii,1)) = offer_mat(index_ii,3);
end

% Loop over pairs, process
NP = size(pair_nums,1); % # pairs
future_ahead_sum = zeros(NP,1);
future_ahead_freq = zeros(NP,1);
ever_ahead_freq = zeros(NP,1);
pair_offers = zeros(NP,1);  % # offers for each pair, minus 1
tic
display('Compute # and Frequency Ahead in Future, Given Ahead Today')
for n=1:NP
    if mod(n,1e06)==0
        display(['Pair ' num2str(n)])
    end
    common_donors = offers_wide(pair_nums(n,1),:) & offers_wide(pair_nums(n,2),:);
    pair_offers(n) = sum(common_donors) - 1;
    ahead = positions_wide(pair_nums(n,1),common_donors) > positions_wide(pair_nums(n,2),common_donors);
    times_ahead = cumsum(ahead,'reverse');
    future_ahead_vec = ahead(1:(end-1)).*times_ahead(2:end) ...
                        + (~ahead(1:(end-1))).*((pair_offers(n):-1:1) - times_ahead(2:end));
    future_ahead_sum(n) = mean(future_ahead_vec);
    future_ahead_freq(n) = mean(future_ahead_vec ./ (pair_offers(n):-1:1));
    ever_ahead_freq(n) = mean(future_ahead_vec>0);
end
toc
useful_pairs = pair_offers > 0;
future_ahead_sum = future_ahead_sum(useful_pairs);
future_ahead_freq = future_ahead_freq(useful_pairs);
ever_ahead_freq = ever_ahead_freq(useful_pairs);
pair_offers = pair_offers(useful_pairs);

% Average for summary statistics
num_ahead_in_future = sum(future_ahead_sum .* pair_offers) / sum(pair_offers);
  frac_ahead_in_future = sum(future_ahead_freq .* pair_offers) / sum(pair_offers);
  ever_ahead_in_future = sum(ever_ahead_freq .* pair_offers) / sum(pair_offers);

table_cell = {'% Cases Ever Ahead given Ahead Today',100*freq_ahead_stat;...
	      '% Cases Ahead in Future given Ahead Today',100*frac_ahead_in_future;...
	      '% Ever Ahead in Future given Ahead Today',100*ever_ahead_in_future;
              '# Cases Ahead in Future given Ahead Today',num_ahead_in_future};
out_table = cell2table(table_cell,'VariableNames',{'Statistic','Value'})
writetable(out_table,[output_folder 'patient_ordering_statistics.xls'],'Sheet','ordering, raw');
