%%% Estimation for CCP validation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% READ ME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script needs to be run multiple times, each with different settings. 
% STEP 1:
%         Use the setting options as below; Run the baseline model
% STEP 2:
%         Set model_names = {'nyrt_validation_fix_time_uh_sparse',...
%                            'nyrt_validation_fix_time_uh',...
%                            'nyrt_validation_fix_time_uh_overfit',...
%                            'nyrt_validation_fix_time_uh_overfit3',...
%                            'nyrt_comparison_fix_time_uh_sparse',...
%                            'nyrt_comparison_fix_time_uh',...
%                            'nyrt_comparison_fix_time_uh_overfit',...
%                            'nyrt_comparison_fix_time_uh_overfit3'};
%          Set tasks.gen_data and tasks.init_model to true;
%          Set all other tasks to false
% Step 3:
%          Set model_names = {'nyrt_validation_fix_time_uh_sparse',...
%                             'nyrt_validation_fix_time_uh',...
%                             'nyrt_validation_fix_time_uh_overfit',...
%                             'nyrt_validation_fix_time_uh_overfit3'};
%          Set tasks.validate_ccps to true;
%          Set all other tasks to false
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% Clear workspace, add paths and define target folders;
clearvars; add_paths;

model_names = {'nyrt_fix_time_uh_sparse',...
               'nyrt_fix_time_uh',...
               'nyrt_fix_time_uh_overfit',...
               'nyrt_fix_time_uh_overfit3'};

%% Tasks
tasks.init_model            = true;
tasks.gen_data              = true;
tasks.gen_gibbs_chain       = true;
tasks.summarize_base_chains = true;
tasks.convergence           = true;
tasks.results               = true;
tasks.avg_ccp               = false;
tasks.gen_redraw_chain      = true;
tasks.ccp_fit               = false;
tasks.value_function        = true;
tasks.average_offer         = false;
tasks.projection_table      = false;
tasks.validate_ccps         = false;


% Specify Options here (see main_options.m for available options)
options = main_options();

%%% Print Tasks
tasks                                      

%%% Pass to main scripts
% Loop
for ii = 1:length(model_names)
    model_name = model_names{ii};
    main
end



