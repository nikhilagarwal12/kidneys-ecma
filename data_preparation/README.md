#Folder: data_preparation

This folder cleans up raw data files and generate primary files 
for further analysis.

code/gen_primary_files.do is the wrapper script that calls other 
data cleaning scripts. code/gen_primary_files.do generates the 
final primary datasets for analysis. 

code/document_sample_selection.do generates Tables 1 through 4 
in Data Appendix. code/document_sample_selection.do is called in
code/gen_primary_files.do

Scripts in this folder other than code/gen_primary_files.do should 
not be run independently, though some can be run independently for 
diagnostic purposes.