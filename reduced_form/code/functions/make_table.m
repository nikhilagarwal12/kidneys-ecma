% Helper function to create a table from rank autocorrelation test output

function result_table = make_table(group_ids,labels,group,stat,p,G)

result_cell = cell(G,5);
for g=1:G
    result_cell{g,1} = group_ids(g);
    result_cell{g,2} = labels(g);
    result_cell{g,3} = sum(group==group_ids(g));
    result_cell{g,4} = stat(g);
    result_cell{g,5} = p(g);
end
result_table = cell2table(result_cell,'VariableNames',{'group_id','group_label','num_obs','vnm_statistic','p_value'});

end
