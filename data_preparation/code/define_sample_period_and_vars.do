**** Create variable list locals

local pre_2007 "hosp_90_days work_no_stat* work_yes_stat* fin_flow_rate~x prod_urine24 tumor_tx tumor_ty physical_capa~r cereb_vasc dial_ty_tcr drugtrt_hyp most_rcnt_creat peptic_ulcer dial_tcr"

local pre_2007b "oth_grf_fail_~i rej_acute_ki infect_ki grf_thromb_ki infect_ki surg_compl_ki urol_compl_ki recur_disease_ki pa_revasc"

local donor_char "da? db? ddr? rda? rdb? rddr? don_retyp *_don don_* *_don_* pump_ki ecd_donor referral_date recovery_date protein_urine lipase amylase inotrop_* cardarrest_ne~o resuscit_dur tattoos lt_ki_biopsy lt_ki_glomerul rt_ki_biopsy rt_ki_glomerul admit_date_don donor_id ino_procure* opo* kdri donor_cat donor_hospital donor_hospital_zip"

local match_char "ra? rb? rdr? amis bmis drmis hlamis abo_mat distance cold_isch_ki warm_isch_tm payback data_transplant tx_ctr tx_ctr_dsa organ"

// writing out all donor vars in KIDPAN to loop over. Omitting "_old" vars because they don't have any obs
local donor_char_full "rda1 rda2 rdb1 rdb2 rddr1 rddr2 don_retyp da1 da2 db1 db2 ddr1 ddr2 haplo_ty_match_don age_don ddavp_don cmv_old_liv_don cmv_don cmv_test_don ebv_test_don hbv_test_don hcv_test_don cmv_nucleic_don cmv_igg_don cmv_igm_don ebv_dna_don ebv_igg_don ebv_igm_don hbv_core_don hbv_sur_antigen_don ethcat_don cod_cad_don death_circum_don death_mech_don citizenship_don hep_c_anti_don hcv_rna_don abo_don don_ty gender_don home_state_don warm_isch_tm_don hcv_riba_don hcv_antibody_don liv_don_ty citizen_country_don cod_ostxt_don controlled_don core_cool_don non_hrt_don anticonv_don antihype_don blood_inf_don blood_inf_conf_don bun_don creat_don htlv1_old_don htlv2_old_don other_inf_don other_inf_conf_don other_inf_ostxt_don pt_diuretics_don pt_steroids_don pt_t3_don pt_t4_don pt_oth2_ostxt_don pt_oth3_ostxt_don pt_oth4_ostxt_don pt_oth1_ostxt_don pulm_inf_don pulm_inf_conf_don sgot_don sgpt_don tbili_don urine_inf_don urine_inf_conf_don vasodil_don vdrl_don clin_infect_don hypertens_dur_don cancer_free_int_don cancer_oth_ostxt_don contin_alcohol_old_don contin_cig_don contin_iv_drug_old_don contin_cocaine_don contin_oth_drug_don diet_don diuretics_don extracranial_cancer_don hist_alcohol_old_don cancer_site_don hist_cig_don diabdur_don hist_cocaine_don hist_hypertens_don hist_iv_drug_old_don insulin_dep_don intracranial_cancer_don other_hypertens_med_don hist_cancer_don hist_insulin_dep_don insulin_dur_don hist_diabetes_don hist_oth_drug_don skin_cancer_don diabetes_don liv_don_ty_ostxt heparin_don arginine_don insulin_don hgt_cm_don_calc wgt_kg_don_calc bmi_don_calc pump_ki ecd_donor education_don pri_payment_don pri_payment_ctry_don medicare_don medicaid_don oth_govt_don priv_ins_don hmo_ppo_don self_don donation_don free_don protein_urine lipase amylase inotrop_agents cardarrest_neuro resuscit_dur inotrop_support_don tattoos lt_ki_biopsy lt_ki_glomerul rt_ki_biopsy rt_ki_glomerul referral_date recovery_date admit_date_don donor_id hbsab_don ebv_igg_cad_don ebv_igm_cad_don cdc_risk_hiv_don ino_procure_agent_1 ino_procure_agent_2 ino_procure_agent_3 ino_procure_ostxt_1 ino_procure_ostxt_2 ino_procure_ostxt_3 opo_id opo_ctr opo_dsa_id opo_region_id donor_cat kdri donor_hospital donor_hospital_zip"

display "`donor_char_full'"

// characteristics that do not vary within donor (between a donor's two kidneys). Includes additional antigens not in KIDPAN
local donor_level_chars "da1 da2 db1 db2 ddr1 ddr2 haplo_ty_match_don age_don ddavp_don cmv_old_liv_don cmv_don cmv_test_don ebv_test_don hbv_test_don hcv_test_don cmv_nucleic_don cmv_igg_don cmv_igm_don ebv_dna_don ebv_igg_don ebv_igm_don hbv_core_don hbv_sur_antigen_don ethcat_don cod_cad_don death_circum_don death_mech_don citizenship_don hep_c_anti_don hcv_rna_don abo_don don_ty gender_don home_state_don hcv_riba_don hcv_antibody_don liv_don_ty citizen_country_don cod_ostxt_don controlled_don core_cool_don non_hrt_don antihype_don blood_inf_don blood_inf_conf_don bun_don creat_don htlv1_old_don htlv2_old_don other_inf_don other_inf_conf_don other_inf_ostxt_don pt_diuretics_don pt_steroids_don pt_t3_don pt_t4_don pt_oth2_ostxt_don pt_oth3_ostxt_don pt_oth4_ostxt_don pt_oth1_ostxt_don pulm_inf_don pulm_inf_conf_don sgot_don sgpt_don tbili_don urine_inf_don urine_inf_conf_don vasodil_don vdrl_don clin_infect_don hypertens_dur_don cancer_free_int_don cancer_oth_ostxt_don contin_alcohol_old_don contin_cig_don contin_iv_drug_old_don contin_cocaine_don contin_oth_drug_don diet_don diuretics_don extracranial_cancer_don hist_alcohol_old_don cancer_site_don hist_cig_don diabdur_don hist_cocaine_don hist_hypertens_don hist_iv_drug_old_don insulin_dep_don intracranial_cancer_don other_hypertens_med_don hist_cancer_don hist_insulin_dep_don insulin_dur_don hist_diabetes_don hist_oth_drug_don skin_cancer_don diabetes_don liv_don_ty_ostxt heparin_don arginine_don insulin_don hgt_cm_don_calc wgt_kg_don_calc bmi_don_calc ecd_donor education_don pri_payment_don pri_payment_ctry_don medicare_don medicaid_don oth_govt_don priv_ins_don hmo_ppo_don self_don donation_don free_don protein_urine lipase amylase inotrop_agents cardarrest_neuro resuscit_dur inotrop_support_don tattoos lt_ki_biopsy lt_ki_glomerul rt_ki_biopsy rt_ki_glomerul referral_date recovery_date admit_date_don donor_id hbsab_don ebv_igg_cad_don ebv_igm_cad_don cdc_risk_hiv_don ino_procure_agent_1 ino_procure_agent_2 ino_procure_agent_3 ino_procure_ostxt_1 ino_procure_ostxt_2 ino_procure_ostxt_3 opo_id opo_ctr opo_dsa_id opo_region_id donor_cat dbw* dc* ddr5* ddq* ddp*  donor_hospital donor_hospital_zip"

display "`donor_level_chars'"

local hla_vars = "dbw4 dbw6 dc1 dc2 ddr1 ddr2 ddr51 ddr52 ddr53 ddp1 ddp2 ddq1 ddq2"

// sample years

if `sample_start_date'==.{
  local sample_start_year = 2000    // first year of offer data, if start_date unspecified
}
else{  
  local sample_start_year = year(`sample_start_date')
}
local ptr_start_year = max(`sample_start_year',2000)

if `sample_end_date'==.{
  local sample_end_year = 2015      // last year of offer data, if end_date unspecified
}
else{
  local sample_end_year = year(`sample_end_date')
}

display "`sample_start_year' - `sample_end_year' Sample"


// New section of KDRI options

local kdri_norm = 1.2175         // normalization constant
local pr_diabetes = 0.09175      // imputed probability of being diabetic if unknown (but not missing)
local pr_hypertension = 0.30299  // imputed probability of being hypertensive if unknown (but not missing)
local kdri_norm_p20 = 0.746443612984839  // max KDRI (normalized) in 0-20% range
local kdri_norm_p34 = 0.854812784054361  // max KDRI (normalized) in 0-34% range
local kdri_norm_p85 = 1.466165368988480  // max KDRI (normalized) in 0-85% range
