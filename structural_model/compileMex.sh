#!/bin/sh
mex COPTIMFLAGS='-O3 -std=c99 -DNDEBUG -Wall -march=native -ffast-math -ftree-vectorize -fopenmp' LDFLAGS='\$LDFLAGS -fopenmp' -largeArrayDims code/cf_functions/mScoreValHelperMex.c
#mv backwardsInductMex.mexa64 code/cf_functions/
