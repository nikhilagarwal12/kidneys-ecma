#Folder: mechanism

This folder contains scripts that simulate the offer assignment
mechanism.

code/simulate_offers.do is the wrapper script that calls other 
mechanism scripts. code/simulate_offers.do is called in 
../data_preparation/code/gen_primary_files.do to generate 
offers sequence for each patient.

Scripts in this folder should not be run independently, though 
code/simulate_offers.do can be run independently for diagnostic 
purposes.
