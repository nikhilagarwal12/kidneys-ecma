% Decompose value function into observed and unobserved components

function [V_obj,pr_tx,V_ref] = vf_decomposition_flow(q,p,V,params,V_ref)

    % V: value function (N_patients x N_time)
    % pr_tx: probability of transplant (yes and compat) conditional on offer (N_patients x N_time x N_donors).
    % Not exactly backwards induction because I use previous V instead.
    % It will perform similarly, maybe reduces oscilations.
    [N_patients, N_time, N_donors] = size(q);
    
    if (isfield(params,'first_best') && params.first_best)
        run_type = 'first_best';
    else
        run_type = 'second_best';
    end
    
    qc  = bsxfun(@times,q,params.pr_compat);
    if strcmp(run_type,'first_best')  % E(eps|tx) = phi(x) / (1-Phi(x)), x <-> normal cutoff
        pr = 1;
        Eeps = normpdf(norminv(1-q));   % E[eps|assigned]
    else
        z    = bsxfun(@times,bsxfun(@plus,params.mu,-V),1./params.sigma); % cutoff for standard normal
        pr   = normcdf(z);
        Eeps = normpdf(z);  % E[eps|eps>V-mu]Pr(eps>V-mu), using Phi(x) = 1-Phi(-x)
    end
    
    %% Backwards induction
    backwards_indution = true;
    if backwards_indution
        heu.tol = 1e-7;  heu.maxiter = 1000;
        a = params.discount_w;
        V_old = V;
        z_old = z;
        z   = zeros(size(z));
        b   = params.discount_V2;
        V   =   [V zeros(N_patients,1)]; % Add one more column of zeros (will be removed later)
        Vdiff = 0;
        for k = N_time:-1:1
            Vlb = V(:,k+1) ./b(:,k);  % discount one period
            Vub = inf;
            VV  = max(Vlb,V(:,k));
            ii  = 0;
            while norm(Vub-Vlb,'inf')>heu.tol && ii<heu.maxiter
                zz  = bsxfun(@times,bsxfun(@plus,params.mu(:,k,:),-VV),1./params.sigma); % cutoff for standard normal
                fz  = normpdf(zz);
                Fz  = normcdf(zz);                        % Prob of accepting the offer
                Emax = bsxfun(@times,params.sigma,Fz .* zz + fz); % Emax(0,mu-V+eps)
                w    = mean(qc(:,k,:).*Emax,3); 
                TV = (a(:,k) .* w + V(:,k+1))./b(:,k);
                Vub  = min(max(VV,TV),Vub);  % Want to find V = T(V), where T(V) is decreasing in V
                Vlb  = max(min(VV,TV),Vlb);  
                VV    = (Vub+Vlb)/2;
                ii    = ii + 1;
            end
            Vdiff  = max(Vdiff,norm(Vub-Vlb,'inf'));
            V(:,k) = TV; 
            z(:,k,:) = zz;
            if nargout>1, pr_tx(:,k,:) = bsxfun(@times,Fz,params.pr_compat); end
        end
        if isfield(heu,'verbose') && heu.verbose, fprintf('Backwards induction norm: %e\n',Vdiff); end
        V(:,N_time+1) = [];  % Remove the extra column of zeros
    end
    
    p  = max(p,0);
    pl = [p(:,2:end) zeros(size(p,1),1)];
    % Numerical Approximation
    % If p_{t+h} = p_{t} * exp(-eta h)
    % The integration of p_{\tau} for \tau between t and t+h is:
    % p_{t} h * [(p_{t+h}/p_{t} - 1) / log(p_{t+h}/p_{t})]
    % The next in-line function returns the term in square brackets: 
    % F(x) = [(x - 1) / log(x)] where x = p_{t+h}/p_{t};
    % L'Hopital rule says that F(x)=1 as x->1.
    % We only care for p_{t+h}/p_{t}<1. 
    % F(x) is a bijection on [0,1]. 
    approx = @(pt,pth) (min(pth./pt,1-1e-12) - 1)./log(min(pth./pt,1-1e-12));
    % Used min(pth./pt,1-1e-12) instead of pth/pt to avoid 0/0, which arises when p_{t+h}/p_{t}~1.
    
    
    V_obj.V_raw_obs    = sum(params.lambdah_over_K.*p.*approx(p,pl).*sum(qc.*pr.*params.mu,3),2)/params.rho;
    V_obj.V_raw_unobs  = sum(params.lambdah_over_K.*p.*approx(p,pl).*sum(qc.*Eeps,3),2)/params.rho;
    
    if nargin< 5 || isempty(V_ref)
        % calculate mu_ref and eps_ref if this is the reference mechanism
        q_sum = sum(params.lambdah_over_K.*p.*approx(p,pl).*sum(qc.*pr,3),2)/params.rho;
        V_ref.eps_ref = V_obj.V_raw_unobs ./ q_sum;
        V_ref.mu_ref = V_obj.V_raw_obs ./ q_sum;
    end
    
    mu_res = bsxfun(@plus,params.mu,-V_ref.mu_ref);
    eps_res = bsxfun(@plus,Eeps,bsxfun(@times,-V_ref.eps_ref,pr));
    
    V_obj.V_obs    = sum(params.lambdah_over_K.*p.*approx(p,pl).*sum(qc.*pr.*mu_res,3),2)/params.rho;
    V_obj.V_unobs  = sum(params.lambdah_over_K.*p.*approx(p,pl).*sum(qc.*eps_res,3),2)/params.rho;
    V_obj.V_fixed  = sum(params.lambdah_over_K.*p.*approx(p,pl).* ...
                     sum(qc.*pr.*repmat(V_ref.eps_ref + V_ref.mu_ref,1,N_time,N_donors),3),2)/params.rho;
    V_obj.V_full   = V_obj.V_obs + V_obj.V_unobs + V_obj.V_fixed;
    assert(all(~isnan(V_obj.V_full)));

end
