#include "common.h"

#include "mex.h"

#include <math.h>
#include <string.h>
#include <stdint.h>

static int notInitialized = 1;
static double *lambdaFeasibilityMod;
static long *iIndex, *jIndex;
static double *value;
static double *z, *fz, *Fz, *expTerm, *pExpTerm, *aK, *FzLh, *sumQfz, *sumQfzLh;

void cleanup(void) {
  mexPrintf("MEX-file is terminating, destroying allocations in analytic second Hessian\n");
  free(iIndex);
  free(jIndex);
  free(value);
  free(lambdaFeasibilityMod);
  free(z);
  free(fz);
  free(Fz);
  free(expTerm);
  free(pExpTerm);
  free(aK);
  free(FzLh);
  free(sumQfz);
  free(sumQfzLh);
  notInitialized = 1;
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  double *mu = (double*)mxGetPr(mxGetField(prhs[2], 0, "mu"));
  double *sigmaInvA = (double*)mxGetPr(mxGetField(prhs[2], 0, "sigma_inverse"));
  double *deltaBy_h = (double*)mxGetPr(mxGetField(prhs[2], 0, "delta_x_h"));
  double *lambda_hOverK = (double*)mxGetPr(mxGetField(prhs[2], 0, "lambdah_over_K"));
  double *discountW = (double*)mxGetPr(mxGetField(prhs[2], 0, "discount_w"));
  double *cArrayRepeat = (double*)mxGetPr(mxGetField(prhs[2], 0, "pr_compat_repeat"));
  double *h = (double*)mxGetPr(mxGetField(prhs[2], 0, "h"));
  double *timeOutProb =  (double*)mxGetPr(mxGetField(prhs[2], 0, "timeout_prob"));
  double *paretoWeightsRepeat = (double*)mxGetPr(mxGetField(prhs[2], 0, "pareto_weights_repeat"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[2], 0, "mu"));
  const long nPatients = sizeArray[0];
  const long nTime = sizeArray[1];
  const long nDonors = sizeArray[2];
  const double nDonorsInv = (1.0/(double)nDonors);
  
  const uint32_t maxTransplant = *((uint32_t*)mxGetPr(mxGetField(prhs[2], 0, "maximize_transplants")));

  const long nQ = nPatients*nTime*nDonors;
  const long nP = nPatients*nTime;
  const long nTm1nP = nP-nPatients;
  const double *q = (double*)mxGetPr(prhs[0]);
  const double *p = q+nQ;
  const double *v = p+nP;
  double *lambdaFeasibility = (double*)mxGetPr(mxGetField(prhs[1], 0, "ineqnonlin"));
  const double *lambdaSteadyState = (double*)mxGetPr(mxGetField(prhs[1], 0, "eqnonlin"));
  const double *lambdaValue = lambdaSteadyState+nTm1nP; 

  if (notInitialized) {
    lambdaFeasibilityMod = (double*)malloc(nDonors*sizeof(double));
    const long nnz = nQ*4l+nP*3l+nDonors*nDonors*nTm1nP;
    iIndex = (long*)malloc(nnz*sizeof(long));
    jIndex = (long*)malloc(nnz*sizeof(long));
    value = (double*)malloc(nnz*sizeof(double));
    z = (double*)malloc(nQ*sizeof(double));
    fz = (double*)malloc(nQ*sizeof(double));
    Fz = (double*)malloc(nQ*sizeof(double));
    FzLh = (double*)malloc(nQ*sizeof(double));
    expTerm = (double*)malloc(nTm1nP*sizeof(double));
    pExpTerm = (double*)malloc(nTm1nP*sizeof(double));
    aK = (double*)malloc(nP*sizeof(double));
    sumQfz = (double*)malloc(nP*sizeof(double));
    sumQfzLh = (double*)malloc(nP*sizeof(double));
    mexAtExit(cleanup);
    notInitialized = 0;
  }
  memset(expTerm, 0, sizeof(double)*nTm1nP);
  memset(sumQfz, 0, nP*sizeof(double));
  memcpy(lambdaFeasibilityMod, lambdaFeasibility, nDonors*sizeof(double));

  if (maxTransplant) {
    double *slackCost = (double*)mxGetPr(mxGetField(prhs[2], 0, "slack_cost"));
    for (unsigned long j = 0; j < nDonors; ++j) {
      lambdaFeasibilityMod[j] -= slackCost[j];
    }
  }

  unsigned long int currentIndex = 0;
  // Precompute common sub expressions
  for (unsigned long j = 0, index = 0; j < nDonors; ++j) {
    for (unsigned long i = 0; i < nP; ++i, ++index) {
      const double sigmaInv = sigmaInvA[i];
      const double zLocal = (mu[index]-v[i])*sigmaInv;
      fz[index] = normPdf(zLocal)*cArrayRepeat[index]*sigmaInv;
      Fz[index] = normCdf(zLocal)*cArrayRepeat[index];
      const double qfz = q[index]*fz[index];
      z[index] = zLocal*qfz*sigmaInv;
      FzLh[index] = Fz[index]*lambda_hOverK[i];
      sumQfz[i] += qfz;
    }
  }
  for (unsigned long i = 0; i < nP; ++i) {
    aK[i] = lambdaValue[i]*discountW[i]*nDonorsInv; 
    sumQfzLh[i] = sumQfz[i]*lambda_hOverK[i];
  }
  for (unsigned long j = 0, index = 0; j < nDonors; ++j, index += nPatients) {
    for (unsigned long i = 0; i < nTm1nP; ++i, ++index) {
      expTerm[i] += q[index]*Fz[index];
    }
  }
  for (unsigned long i = 0; i < nTm1nP; ++i) {
    expTerm[i] = -lambdaSteadyState[i]*exp(-deltaBy_h[i]-lambda_hOverK[i]*expTerm[i]);
    pExpTerm[i] = p[i]*expTerm[i];
  }
  // Add qq Values
  for (unsigned long k = 0; k < nTm1nP; ++k) {
    const double localValue = pExpTerm[k];
    for (unsigned long i = 0, indexI = k; i < nDonors; ++i, indexI += nP) {
      const double FzLhLocal = localValue*FzLh[indexI];
      for (unsigned long j = 0, indexJ = k; j < nDonors; ++j, indexJ += nP) {
        const double localValue1 = FzLh[indexJ]*FzLhLocal;
        if (localValue1 != 0.0) {
          iIndex[currentIndex] = indexI;
          jIndex[currentIndex] = indexJ;
          value[currentIndex++] = localValue1;
        }
      }
    }
  }
  // Add qp and pq Values
  for (unsigned long i = 0, index = 0; i < nDonors; ++i) {
    for (unsigned long j = 0; j < nTm1nP; ++j, ++index) {
      const double localValue = h[j]*(Fz[index]+timeOutProb[i])*lambdaFeasibilityMod[i]-FzLh[index]*expTerm[j];
      if (localValue != 0.0) {
        iIndex[currentIndex] = index;
        jIndex[currentIndex] = j+nQ;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = j+nQ;
        jIndex[currentIndex] = index;
        value[currentIndex++] = localValue;
      }
    }
    for (unsigned long j = nTm1nP; j < nP; ++j, ++index) {
      const double localValue = h[j]*(Fz[index]+timeOutProb[i])*lambdaFeasibilityMod[i];
      if (localValue != 0.0) {
        iIndex[currentIndex] = index;
        jIndex[currentIndex] = j+nQ;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = j+nQ;
        jIndex[currentIndex] = index;
        value[currentIndex++] = localValue;
      }
    }
  }
  // Add qv and vq values
  const unsigned long offset = nQ + nP;
  for (unsigned long i = 0, index = 0; i < nDonors; ++i) {
    for (unsigned long j = 0; j < nTm1nP; ++j, ++index) {
      const double localValue1 = -fz[index]*lambdaFeasibilityMod[i]*p[j]*h[j];
      const double localValue2 = (fz[index]*lambda_hOverK[j]-FzLh[index]*sumQfzLh[j])*pExpTerm[j];
      const double localValue3 = Fz[index]*aK[j];
      const double localValue = localValue1+localValue2+localValue3;
      if (localValue != 0.0) {
        iIndex[currentIndex] = index;
        jIndex[currentIndex] = j+offset;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = j+offset;
        jIndex[currentIndex] = index;
        value[currentIndex++] = localValue;
      }
    }
    for (unsigned long j = nTm1nP; j < nP; ++j, ++index) {
      const double localValue1 = -fz[index]*lambdaFeasibilityMod[i]*p[j]*h[j];
      const double localValue3 = Fz[index]*aK[j] + localValue1;
      if (localValue3 != 0.0) {
        iIndex[currentIndex] = index;
        jIndex[currentIndex] = j+offset;
        value[currentIndex++] = localValue3;
        iIndex[currentIndex] = j+offset;
        jIndex[currentIndex] = index;
        value[currentIndex++] = localValue3;
      }
    }
  }
  // Add pv and vp values
  if (maxTransplant) {
    for (unsigned long i = 0; i < nTm1nP; ++i) {
      double localValue1 = 0.0;
      const double localValue2 = sumQfzLh[i]*expTerm[i];
      for (unsigned long j = 0, index = i; j < nDonors; ++j, index += nP) {
        localValue1 -= q[index]*fz[index]*lambdaFeasibilityMod[j];
      }
      const double localValue = h[i]*localValue1 + localValue2;
      if (localValue != 0.0) {
        iIndex[currentIndex] = i+nQ;
        jIndex[currentIndex] = i+offset;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = i+offset;
        jIndex[currentIndex] = i+nQ;
        value[currentIndex++] = localValue;
      }
    }
    for (unsigned long i = nTm1nP; i < nP; ++i) {
      double localValue1 = 0.0;
      for (unsigned long j = 0, index = i; j < nDonors; ++j, index += nP) {
        localValue1 -= q[index]*fz[index]*lambdaFeasibilityMod[j];
      }
      localValue1 *= h[i];
      if (localValue1 != 0.0) {
        iIndex[currentIndex] = i+nQ;
        jIndex[currentIndex] = i+offset;
        value[currentIndex++] = localValue1;
        iIndex[currentIndex] = i+offset;
        jIndex[currentIndex] = i+nQ;
        value[currentIndex++] = localValue1;
      }
    }
  } else {
    for (unsigned long i = 0; i < nTm1nP; ++i) {
      double localValue1 = 0.0;
      const double localValue2 = sumQfzLh[i]*expTerm[i];
      for (unsigned long j = 0, index = i; j < nDonors; ++j, index += nP) {
        localValue1 -= q[index]*fz[index]*lambdaFeasibilityMod[j];
      }
      localValue1 -= paretoWeightsRepeat[i]; //Contribution from objective function
      localValue1 *= h[i];
      const double localValue = localValue1 + localValue2;
      if (localValue != 0.0) {
        iIndex[currentIndex] = i+nQ;
        jIndex[currentIndex] = i+offset;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = i+offset;
        jIndex[currentIndex] = i+nQ;
        value[currentIndex++] = localValue;
      }
    }
    for (unsigned long i = nTm1nP; i < nP; ++i) {
      double localValue1 = 0.0;
      for (unsigned long j = 0, index = i; j < nDonors; ++j, index += nP) {
        localValue1 -= q[index]*fz[index]*lambdaFeasibilityMod[j];
      }
      localValue1 -= paretoWeightsRepeat[i]; //Contribution from objective function
      localValue1 *= h[i];
      if (localValue1 != 0.0) {
        iIndex[currentIndex] = i+nQ;
        jIndex[currentIndex] = i+offset;
        value[currentIndex++] = localValue1;
        iIndex[currentIndex] = i+offset;
        jIndex[currentIndex] = i+nQ;
        value[currentIndex++] = localValue1;
      }
    }
  }
  // Add vv values
  for (unsigned long i = 0; i < nTm1nP; ++i) {
    double localValue1 = 0.0;
    double localValue2 = 0.0;
    for (unsigned long j = 0, index = i; j < nDonors; ++j, index += nP) {
      localValue1 -= z[index]*lambdaFeasibilityMod[j];
      localValue2 += z[index];
    }
    localValue1 *= p[i]*h[i];
    localValue2 = (localValue2*lambda_hOverK[i]+sumQfzLh[i]*sumQfzLh[i])*pExpTerm[i];
    const double localValue3 = -aK[i]*sumQfz[i];
    const double localValue = localValue1+localValue2+localValue3;
    if (localValue != 0.0) {
      iIndex[currentIndex] = i+offset;
      jIndex[currentIndex] = i+offset;
      value[currentIndex++] = localValue;
    }
  }
  for (unsigned long i = nTm1nP; i < nP; ++i) {
    double localValue1 = 0.0;
    for (unsigned long j = 0, index = i; j < nDonors; ++j, index += nP) {
      localValue1 -= z[index]*lambdaFeasibilityMod[j];
    }
    localValue1 *= p[i]*h[i];
    localValue1 -= aK[i]*sumQfz[i];
    if (localValue1 != 0.0) {
      iIndex[currentIndex] = i+offset;
      jIndex[currentIndex] = i+offset;
      value[currentIndex++] = localValue1;
    }
  }
  // Create output sparse matrix
  const long matrixSize = nQ+2*nP;
  plhs[0] = mxCreateSparse(matrixSize, matrixSize, currentIndex, mxREAL);
  double *sr  = (double*)mxGetPr(plhs[0]);
  size_t* irs = (size_t*)mxGetIr(plhs[0]);
  size_t* jcs = (size_t*)mxGetJc(plhs[0]);
  COO_toCSC(matrixSize, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
}
