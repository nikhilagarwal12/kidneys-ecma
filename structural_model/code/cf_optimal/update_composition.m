function  p = update_composition(q,V,params,slack)

    if nargin<4, slack = 0; end
    [N_patients, N_time, ~] = size(q);
    if isempty(V) || (isfield(params,'first_best') && params.first_best)  % First-best
        out = exp(-(params.delta_x_h+params.lambdah_over_K.*sum(bsxfun(@times,q,params.pr_compat),3)));
    else  % Second-best
        mli  = bsxfun(@plus,params.mu,-V);         % Mean of the Latent Index
        z   = bsxfun(@times,-mli,1./params.sigma); % cutoff for standard normal
        Fz  = normcdf(-z);
        qcz = bsxfun(@times,q.*Fz,params.pr_compat);
        out = exp(-(params.delta_x_h+params.lambdah_over_K.*sum(qcz,3)));
    end
    
    p     = zeros(size(q,1),size(q,2));
    p(:,1) = params.arrivals(:,1);
    for k = 2:size(p,2)
        p(:,k) = params.arrivals(:,k) + p(:,k-1).* (out(:,k-1)+ slack) ;
    end 
end
