# This script computes CREG matches for gen_primary_files (or anything linked with STATA)

import sys
import csv
import re
import datetime
import math
import pdb
import calculate_offers_function_library
import os

antigen_file = file("/local-data/organs-"+os.getlogin()+"/primary_files/auxiliary_files/antigens_from_primary_files.csv",'rb')                                                              
match_file = file("/local-data/organs-"+os.getlogin()+"/primary_files/auxiliary_files/creg_matches.csv",'w') 

antigen_extract_reader = csv.DictReader(antigen_file, delimiter = ',', quotechar = '"')
match_writer = csv.DictWriter(match_file,delimiter = ',', quotechar = '"', fieldnames = ['donor_id','organ_sequence_num','cmis'])

match_writer.writerow({'donor_id' : 'donor_id', 'organ_sequence_num' : 'organ_sequence_num', 'cmis' : 'cmis'})

# CREG groups
creg_groups = [];
with open('../../mechanism/input/creg_groups_handbook.csv','rb') as cat_file:
    cat_reader = csv.DictReader(cat_file, delimiter = ',', quotechar = '"')
    for row in cat_reader:
        creg_groups.append(row)

# Compute # CREG mismatches for each offer
for offer in antigen_extract_reader:
 
    # patient
    a1 = int(offer['a1'])   
    a2 = int(offer['a2'])
    a1 = a2 if a1==0 else a1
    a2 = a1 if a2==0 else a2
    b1 = int(offer['b1'])
    b2 = int(offer['b2'])
    b1 = b2 if b1==0 else b1
    b2 = b1 if b2==0 else b2
    
    # donor
    da1 = int(offer['da1']) if offer['da1']!='' else a1
    da2 = int(offer['da2']) if offer['da2']!='' else a2
    da2 = da1 if da2 == 98 else da2
    db1 = int(offer['db1']) if offer['db1']!='' else b1
    db2 = int(offer['db2']) if offer['db2']!='' else b2
    db2 = db1 if db2 == 98 else db2

    cmis = calculate_offers_function_library.creg_match_flat(str(a1),str(a2),str(b1),str(b2),str(da1),str(da2),str(db1),str(db2),creg_groups)

    if offer['organ_sequence_num']=="1":
        print(offer['donor_id'] + ": " + str(cmis))
    
    match_writer.writerow({'donor_id' : offer['donor_id'], 'organ_sequence_num' : offer['organ_sequence_num'], 'cmis' : str(cmis)})

antigen_file.close()
match_file.close()
