#!bin/sh

# Mount
set -e
encfs-mount organs
cd /local-data/organs-${USER}

set +e
mkdir /local-data/organs-raw-data-${USER}/raw-extract

echo "*** Enter Center Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/CTR-DSA
7za e "/local-data/organs-${USER}/cds/CTR DSA DATA.zip" -o/local-data/organs-raw-data-${USER}/raw-extract/CTR-DSA

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Heart-Lung-PTR
7za e /local-data/organs-${USER}/cds/HEART_HEARTLUNG_PTR_30JUN2013.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Heart-Lung-PTR

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Kidney-PTR
7za e /local-data/organs-${USER}/cds/KIDNEY_PTR_30JUN2013.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Kidney-PTR

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Kidney-Pancreas-PTR
7za e /local-data/organs-${USER}/cds/KIDNEY_PANCREAS_PTR_30JUN2013.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Kidney-Pancreas-PTR

echo "*** Extracting Kidney Unacceptable Antigens *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Kidpan-Unaccept-Antigens
7za e /local-data/organs-${USER}/cds/KIDPAN_UNACCEPT_ANTIGENS_20130630.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Kidpan-Unaccept-Antigens

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Liver-PTR
7za e /local-data/organs-${USER}/cds/LIVER_PTR_30JUN2013.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Liver-PTR

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Liver-Unaccept-Antigens
7za e /local-data/organs-${USER}/cds/LIVER_UNACCEPT_ANTIGENS_20130630.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Liver-Unaccept-Antigens

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Lung-PTR
7za e /local-data/organs-${USER}/cds/LUNG_PTR_30JUN2013.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Lung-PTR

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Pancreas-PTR
7za e /local-data/organs-${USER}/cds/PANCREAS_PTR_30JUN2013.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Pancreas-PTR

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/Thoracic-Unaccept-Antigens
7za e /local-data/organs-${USER}/cds/THORACIC_UNACCEPT_ANTIGENS_20130630.zip -o/local-data/organs-raw-data-${USER}/raw-extract/Thoracic-Unaccept-Antigens

echo "*** Enter PTR Password *** \n"
mkdir /local-data/organs-raw-data-${USER}/raw-extract/STAR
cp /local-data/organs-${USER}/cds/STAR/*.docx /local-data/organs-raw-data-${USER}/raw-extract/STAR
cp -R /local-data/organs-${USER}/cds/STAR/IMP* /local-data/organs-raw-data-${USER}/raw-extract/STAR
cp -R /local-data/organs-${USER}/cds/STAR/COD* /local-data/organs-raw-data-${USER}/raw-extract/STAR
7za e /local-data/organs-${USER}/cds/STAR/SAS*Export*.zip -o/local-data/organs-raw-data-${USER}/raw-extract/STAR

# Delete CPT and DAT files
find /local-data/organs-raw-data-${USER}/raw-extract -name \*.CPT -type f -delete  
find /local-data/organs-raw-data-${USER}/raw-extract -name \*.DAT -type f -delete  
find /local-data/organs-raw-data-${USER}/raw-extract -name \*.sas7bdat -type f -delete  
