// This script imports the correspondence between ranking variables and offer bins that applied pre-KAS

import delimited ABDR CPRA ABO Ped_List Ped_Alloc Geo Bin using `mechanism'rank_categories_young_healthy.csv, clear
sort *
save `mechanism'rank_categories_preKAS_young_healthy, replace

import delimited ABDR CPRA ABO Ped_List Ped_Alloc Geo Bin using `mechanism'rank_categories_old_healthy.csv, clear
sort *
save `mechanism'rank_categories_preKAS_old_healthy, replace

import delimited ABDR ABO Ped_List Geo Bin using `mechanism'rank_categories_young_dcd.csv, clear
sort *
save `mechanism'rank_categories_preKAS_young_dcd, replace

import delimited ABDR ABO Geo Bin using `mechanism'rank_categories_old_dcd.csv, clear
sort *
save `mechanism'rank_categories_preKAS_old_dcd, replace

import delimited ABDR CPRA ABO Ped_List Ped_Alloc Geo Bin using `mechanism'rank_categories_ecd.csv, clear
sort *
save `mechanism'rank_categories_preKAS_ecd, replace

import delimited ABDR ABO Geo Bin using `mechanism'rank_categories_ecd_dcd.csv, clear
sort *
save `mechanism'rank_categories_preKAS_ecd_dcd, replace
