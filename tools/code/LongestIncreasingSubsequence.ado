qui{
capture program drop longseq
program define longseq, sortpreserve
	version 10
	syntax varlist [if] [in], Generate(name) BY(varlist) Order(varlist) [DECreasing]
	marksample touse
	//di "Invert order if decreasing"
	tempvar X donor_idv 
	if ("`decreasing'" == "") {
		noi di "Increasing sequence"
		bysort `by' (`varlist' `order'): gen long `X' = _n
		}
	else {
		noi di "Decreasing sequence"
		tempvar negord
		gen long `negord' = -`order'
		bysort `by' (`varlist' `negord'): gen long `X' = _N - _n + 1
		drop `negord'
	}

	
	//di "Re-order the sample"
	bysort `by' (`order'): gen long `donor_idv' = (_n == 1)
	replace `donor_idv' = sum(`donor_idv')
	//di "Export the full sequence to a txt"
	local numobs = _N
	local donor_id = 0
	file open myfile using ../../tools/input/longseq.txt, write replace text
	forval i = 1/`numobs'{
		if `touse'[`i']{
		if `donor_idv'[`i']~=`donor_id' {
			file write myfile _n
			local donor_id = `donor_idv'[`i']
		}
		file write myfile %11.0g (`X'[`i'])
		}
		}
	file close myfile
	//di "Run longseq.py"
	shell python ../../tools/code/longseq.py
	//di "Build a stata file"
	shell stata -b ../../tools/code/longseqToStata.do
	//di "Merge results"
	gen long _mergeid = sum(`touse')
	replace _mergeid = - _n if ~`touse'
	merge 1:1 _mergeid using ../../tools/output/longseq.dta, assert(match master) 
	drop _mergeid _merge
	ren _in_seq `generate'
	//di "End of the program"
end


capture program drop genaz
program define genaz, sortpreserve
version 10
	syntax varlist [if] [in], FIX(varname) BY(varlist) Order(varlist)
	marksample touse
	tempvar aux aux2
	foreach var of varlist `varlist'{
		gen a_`var' = `var' if `touse' & `fix'
		bysort `by' (`order'): replace a_`var' = a_`var'[_n-1] if ~(`touse' & `fix') & _n>1
		gen `aux' = (`touse' & `fix')*1
		bysort `by' (`order'): gen `aux2' = sum(`aux') - `aux' 
		bysort `by' `aux2': gen z_`var' = `var'[_N] if `aux'[_N] 
		drop `aux' `aux2'		
	}
end
	

capture program drop grouptable
program define grouptable, sortpreserve
version 10
	syntax varlist [if] [in], Order(varlist) [BY(varlist)] [List(varlist)]
	marksample touse, novarlist
	qui{
	tempvar group numobs show
	gen `group' = 0
	foreach var of varlist `varlist'{
			bysort `touse' `by' (`order'): replace `group' = 1 if `var'~=`var'[_n-1] 
	}
	bysort `touse' `by' (`order'): replace `group' = sum(`group')
	bysort `touse' `by' `group': gen `numobs' = _N
	bysort `touse' `by' `group': gen `show' =_n == _N
	}
	label var `group' "Group"
	label var `numobs' "N"
	list `by' `group' `order' `varlist' `list' `numobs' if `show' & `touse'
end
}
// 
	
