% Fit of the ccps in the model and in the data
% For models with donor_uh, it is conditional on no acceptances
% prior to the considered offer

function ccps = ccp_position_fit(data,model,chain,options)

    if model.pt_uh
        poo  % not coded for pt_uh case
    end
    options.ccp_fit.results_folder = options.results_folder;
    options = options.ccp_fit;
    
    % initialize the random number generator
    if ~isfield(options,'rng_seed')
        options.rng_seed = 954637;  % Randomly generated seed on 11-17-16
    end
    rng(options.rng_seed);

    % predicted and actual ccp vectors, for first acceptance
    ccps.model = zeros(options.n_pos,1);
    ccps.data = zeros(options.n_pos,1);

    % also store indicators for included offers in original data structure, and eta draws themselves
    ccps.eta_redraws = zeros(size(data.fixed.obs,1),options.n_draws);
    ccps.index = false(size(data.fixed.obs,1),1);

    % Eta mat
    if model.donor_uh
        data.eta_mat = create_dummies(data.donor_id,data.donor_list);
    end
  
    % Find offer # of first placement for each actual donor. Also record max offer # recorded
    data.first_accept = zeros(size(data.donor_list));
    data.max_offer_num = zeros(size(data.donor_list));
    
    % Convention accept is zero
    assert(isequal(model.choice_def{2},'accept'));   
    for ii = 1:data.N_donors
        donor_offers = (data.donor_id==data.donor_list(ii));
        donor_pos = data.offer_num(donor_offers);
        donor_decisions = (data.cont_choice(donor_offers) == 0);
        data.max_offer_num(ii) = max(donor_pos);

        % Compute first accepted position
        if ~isempty(donor_pos(donor_decisions))
            data.first_accept(ii) = min(donor_pos(donor_decisions));
        else
            data.first_accept(ii) = 10^6; % no actual position number is this high
            assert(data.max_offer_num(ii)<10^6);
        end
    end

    % Without donor_uh, we may look at all offers, if we so desire
    if ~model.donor_uh & ~options.first_accept
        data.first_accept(:) = 10^6;  % Look at all offers
    end

    % Loop Over Position Number
    for m=1:options.n_pos
        % Which donors are in the risk-set        
        which_donors = logical((data.max_offer_num >= m).*(data.first_accept >= m));
        donors_m = data.donor_list(which_donors);

        % Find the m-th offers
        mth_offers = logical(ismember(data.donor_id,donors_m).*(data.offer_num == m));
        ccps.index(mth_offers) = true;
        chi_m = [data.fixed.obs(mth_offers,:) data.flex.obs(mth_offers,:)];
            % with patient UH, need to "repmat" the flex obs
        assert(isequal(size(chi_m,1),sum((data.max_offer_num >= m).*(data.first_accept >= m))),'Error: there should be one offer per donor in risk set');
            
        % if model has donor UH, prediction must integrate over
        % conditional distribution of donor UH
        % Chain must be an M-dimensionsional structure, since we
        % expect a conditional draw for each position
        
        % Parameter values
        init.theta = chain.mu_theta;
        if model.donor_uh
            % Parameters -- initial values
            init.sigma_eta = chain.mu_sigma_eta;   % sigma_eta is the VARIANCE of eta

            [eta_draws, mth_offer_draws] = cond_distr_eta(data,init,model,options,m);
            ccps.eta_redraws(mth_offers,:) = eta_draws(:,(size(eta_draws,2)-options.n_draws+1):end);
            assert(isequal(mth_offers, mth_offer_draws));

            % Compute the CCPs
            ccps.model(m) = 1 - mean(mean(normcdf(repmat(chi_m*chain.mu_theta,1,size(eta_draws,2)) + eta_draws)));
        else
            ccps.model(m) = 1 - mean(normcdf(chi_m*chain.mu_theta,2));
        end 
        
        % CCPs in the data
        ccps.data(m) = 1-mean(data.cont_choice(mth_offers));
    end

    % Save the output as a plot
    model_results_folder = [options.results_folder model.model_name];
    makedir2(model_results_folder);
    makedir2([model_results_folder '/figs']);
    
    % Generate the plot
    plot(1:size(ccps.model,1),[ccps.model,ccps.data],'-o');
    if ~options.first_accept
        title({'Conditional Choice Probability Fits'; '(All Offers)'});
    else
        title({'Conditional Choice Probability Fits'; '(Prior to First Acceptance)'});
    end

    xlabel('Offer Number');
    ylabel('Acceptance Probability');
    legend('Model','Data');

    saveas(gcf,[model_results_folder '/figs/ccp_fit.png']);       
    ccps.options = options;
    ccps.eta_redraws = ccps.eta_redraws(ccps.index,:);  % filter eta draws to relevant offers
end




% Given structural model output from a model with donor UH, estimate the conditional distribution of eta given that a donor was rejected m-1 times
% This is to get the correct conditional distribution of eta to compute Pr(mth offer accepted | first m-1 offers rejected)

% For donor UH model, need to re-run gibbs sampler as follows:
%   - For each offer # k
%       - Select donors whose first (k-1) offers were rejected
%       - Select first k-1 offers (all rejections) for those donors
%       - Re-generate/modify relevant data structures
%       - Re-run gibbs sampler holding theta fixed at its posterior mean, but re-drawing eta

% Arguments are:
  % The data structure to filter
  % chain, model and priors
  % first_accept gives the position of first acceptance for each donor. max_offer_num has the total # offers for each donor to filter donors correctly
  % m is the position for which we want to get the conditional distribution of eta

function [eta_draws,mth_offers] = cond_distr_eta(data,init,model,options,m)

    if model.pt_uh
        poo % throw up since this function doesn't work in this case
    end
   
    display(['Predicting acceptance for position ' num2str(m)])

    if m==1
        % To predict offer #1 CCPs, use unconditional distribution of donor UH (no
        % need to run the chain)        
        eta_draws = randn(data.N_donors,options.n_draws)*sqrt(init.sigma_eta);
        mth_offers = logical(data.offer_num==1);  % all donors included here
    else

        % which donors and offers should be considered
        which_donors = logical((data.max_offer_num >= m).*(data.first_accept >= m));
        donors_m = data.donor_list(which_donors);
        which_offers = logical(ismember(data.donor_id,donors_m) .* (data.offer_num < m));
        
        % populate data_m objects accordingly
        data_m = struct();    % reset to empty
        data_m.donor_list = donors_m;
        data_m.N_donors = length(data_m.donor_list);
        data_m.donor_id = data.donor_id(which_offers);

        data_m.patients = data.patients(which_offers);
        data_m.patient_list = sort(unique(data_m.patients));
        data_m.N_patients = length(data_m.patient_list);

        data_m.N_offers = sum(which_offers);
        assert(data_m.N_offers == (m-1)*data_m.N_donors,'There should be exactly k-1 offers for each donor')

        data_m.eta_mat = create_dummies(data_m.donor_id,data_m.donor_list);

        data_m.fixed.obs = data.fixed.obs(which_offers,:);
        data_m.flex.obs = data.flex.obs(which_offers,:);

        data_m.cont_choice = data.cont_choice(which_offers);
        assert(sum(data_m.cont_choice==0)==0,'All offers should be rejected');
       
        % The following variables could be created, as follows, but
        % are currently not needed
        %        data_m.pt_history_id = data.pt_history_id(which_offers);
        %        data_m.patient_offers = histc(data_m.patients,data_m.patient_list);
        %        data_m.pt_abo = data.pt_abo(ismember(data.patient_list,data_m.patient_list));
        %        data_m.N_histories = length(unique(data_m.pt_history_id));
        %        data_m.model_info = data.model_info;
        %        data_m.gen_date = datetime('today');       

        % run gibbs sampler on new data, model and parameter values
        priors = struct();
        chain_spec = struct();
        chain_spec.draw_params = false;
        chain_spec.thin = options.thin;
        chain_spec.NITER = options.n_draws*options.thin+options.burn_in;

        % Chain starting values
        init.Y = round(rand(data_m.N_offers,1));
        init.eta = randn(data_m.N_donors,1)*sqrt(init.sigma_eta);

        % Draw the chain
        chain = gibbs_mixture(data_m, model, priors, init, chain_spec);
        
        % get the posterior eta draws for each donor
        eta_draws = cat(2,chain.eta);
        eta_draws = eta_draws(:,end-options.n_draws:end);

        % get the m-th offers
        mth_offers = logical(ismember(data.donor_id,donors_m) .* (data.offer_num == m));
        
    end
end


function makedir2(dir)
    if ~isdir(dir)
        mkdir(dir);
        fprintf('Created folder %s\n', dir);
    else
        fprintf('Folder %s already exists.\n', dir);
    end
end
