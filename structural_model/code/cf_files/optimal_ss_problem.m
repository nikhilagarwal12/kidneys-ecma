classdef optimal_ss_problem
    properties
        pareto_weights = [];     % Welfare fun = average Equivalent Offers
        arrivals       = [];     % daily arrival of patients
        % Utility 
        mu             = [];
        sigma          = [];
        % Dynamic parameters
        lambda         = []; % Donor arrivals per-day
        rho            = []; % Daily Discount factor
        days_per_pd    = []; % length of time in each period. 
        % Endowment
        endowment      = []; 
        % hazard rate
        hazard         = [];
        feasible       = [];
        % Timeout probability
        timeout_prob = [];
        % Compatibility
        pr_compat  = [];
        % Simplification
        included_times = [];
        periods_from_arrival = [];
        status_quo_V   = [];
        
        % Mechanism
        score          = [];
        V_fact         = [];
        alloc          = [];
        use_fact_V     = [];
        heuristic      = [];
        vf_decomp      = [];
        V_ref_dec      = [];
        
    end  % properties
    
    
    methods 
            % optimal_ss_problem: constructor
            % simplify_times: Simplifies the problem (reduces the number of time periods)
            % reconstruct: expands the solution to match the more complex structure.
            % saveobj: how to save the object
            % loadobj: how to load the object
        
        function obj = optimal_ss_problem(arg1,cf_mech)   % Constructor Function
            if nargin>0 && isa(arg1,'struct')
                % If argument is a structure convert all fields to properties
                 for fn = fieldnames(arg1)'    %enumerate fields
                    try
                        obj.(fn{1}) = arg1.(fn{1});   %and copy
                    catch
                        warning('Could not copy field %s', fn{1});
                    end
                end
            elseif nargin>0 && isa(arg1,'cf_data');
                if ~isempty(arg1.V_ref_flow)
                    obj.pareto_weights = sum(arg1.pt_entry_weights,2)./arg1.V_ref_flow;
                else
                    obj.pareto_weights = [];
                end
                if ~isempty(arg1.V_ref_entry)
                    % It's -inf in all periods except when pt_entry_weights>0
                    obj.status_quo_V = log(1*(arg1.pt_entry_weights>0))+ repmat(arg1.V_ref_entry,1,size(arg1.pt_entry_weights,2)); 
                else
                    obj.status_quo_V = [];
                end
                obj.V_ref_dec      = arg1.V_ref_dec;
                obj.arrivals       = arg1.pt_entry_weights * arg1.options.pt_arrival_rate; % daily arrival of patients
                % Utility 
                obj.mu             = arg1.calc.Gamma_eta;
                obj.sigma          = ones(size(arg1.V_init));
                
                % Dynamic parameters

                obj.lambda         = arg1.options.lambda; % Donor arrivals per-day
                obj.rho            = arg1.options.rho;        % Daily Discount factor
                obj.days_per_pd    = repmat(arg1.options.t - [0 arg1.options.t(1:end-1)],size(obj.arrivals,1),1); % length of time in each period. 

                % Endowment
                obj.endowment      = arg1.donor_options.N_ki_don; 

                % hazard rate
                obj.hazard        = arg1.q_leave_exog;

                % Timeout Probability
                obj.timeout_prob  = 1-exp(-arg1.donor_options.offers.timeout_hazard);

                % Compatibility
                obj.pr_compat     = arg1.pr_compat;

                % make them double (KNITRO crashes with single);

                for s = fieldnames(obj)'
                    if ~isstruct(obj.(s{1}))
                        obj.(s{1}) = double(obj.(s{1}));
                    end
                end
                
                % Feasible Participation
                obj.feasible = arg1.feasible;
                
                % V_fact
                obj.V_fact   = arg1.V_fact;
            end
            if nargin >1  % Update V_fact and mu if cf_mech is given
                if cf_mech.use_proj, calc = 'proj'; else calc = 'calc'; end
                obj.V_fact  = arg1.(calc).V;   % Update V_fact
                obj.mu      = arg1.(calc).Gamma_eta;
            end
        end   % end cf_data (Constructor)
        
        
        function simple = simplify_times(full_obj,periods_from_arrival)
            simple                         = full_obj;
            [N_patients, N_time, N_donors] = size(full_obj.mu);
            new_N_time                     = size([periods_from_arrival],2);
            
            
            simple.pareto_weights          = full_obj.pareto_weights;
            simple.lambda                  = full_obj.lambda;  
            simple.rho                     = full_obj.rho;  
            simple.endowment               = full_obj.endowment;
            simple.timeout_prob            = full_obj.timeout_prob;
            simple.pr_compat               = full_obj.pr_compat;
            simple.arrivals                = zeros(N_patients,new_N_time);
            simple.included_times          = zeros(N_patients,new_N_time);
            simple.days_per_pd             = zeros(N_patients,new_N_time);
            cumdays                        = cumsum(full_obj.days_per_pd,2);
            for pt = 1:N_patients
                arrival_pd                  = min(find(full_obj.arrivals(pt,:)));
                these_periods               = sort([periods_from_arrival+arrival_pd]);
                overT                       = sum(these_periods  > size(full_obj.days_per_pd,2));
                simple.included_times(pt,:) = [these_periods(1)-(overT:-1:1) these_periods(1:end-overT)];
                simple.arrivals(pt,overT+1) = full_obj.arrivals(pt,arrival_pd);
                included_med_times          = floor(0.95*[simple.included_times(pt,2:end) N_time] + 0.05*simple.included_times(pt,:));

                for fn = {'mu','sigma','hazard','feasible','score','V_fact','status_quo_V'}    
                    fn = fn{1};
                    if strcmp(fn,'score') && size(full_obj.score,2)==numel(included_med_times), continue; end  % Greedy has score from previous cfs. No need to shrink it.
                    dim = sum(size(full_obj.(fn))>1);
                    switch dim
                        case 1,
                            if pt == 1, simple.(fn) = zeros(N_patients,1); end
                            simple.(fn)(pt)       = full_obj.(fn)(pt);
                        case 2, 
                            if pt == 1, simple.(fn) = zeros(N_patients,new_N_time); end
                            simple.(fn)(pt,:)       = full_obj.(fn)(pt,included_med_times);
                        case 3,
                            if pt == 1, simple.(fn) = zeros(N_patients,new_N_time,N_donors); end
                            simple.(fn)(pt,:,:)     = full_obj.(fn)(pt,included_med_times,:);
                    end
                end
                cumdays_pt = cumdays(pt,simple.included_times(pt,:));
                simple.days_per_pd(pt,:) = cumdays_pt - [0 cumdays_pt(1:end-1)];
            end
        end
        
        function [full_obj] = full_times(simple_obj,N_time)
        if isempty(simple_obj.included_times)
            full_obj = simple_obj;
        else
            if nargin < 2
                N_time               = max(simple_obj.periods_from_arrival);
            end
            [N_patients,~,N_donors]  = size(simple_obj.mu);
            full_obj                         = optimal_ss_problem();
            full_obj.pareto_weights          = simple_obj.pareto_weights;
            full_obj.lambda                  = simple_obj.lambda;  
            full_obj.rho                     = simple_obj.rho;  
            full_obj.endowment               = simple_obj.endowment;
            full_obj.timeout_prob            = simple_obj.timeout_prob;
            full_obj.pr_compat               = simple_obj.pr_compat;
            full_obj.included_times          = [];
            full_obj.arrivals                = zeros(N_patients,N_time);
            full_obj.days_per_pd             = zeros(N_patients,N_time);
            full_obj.alloc                       = simple_obj.alloc;
            full_obj.alloc.V                     = zeros(N_patients,N_time);
            full_obj.alloc.p                     = zeros(N_patients,N_time);
            full_obj.alloc.q                     = zeros(N_patients,N_time,N_donors);
            full_obj.vf_decomp                   = simple_obj.vf_decomp;
            full_obj.V_ref_dec                   = simple_obj.V_ref_dec;
            t                                = simple_obj.included_times;
            for pt = 1:N_patients
                % fill up with zeros
                full_obj.arrivals(pt,t(pt,:))= simple_obj.arrivals(pt,:);
                arrival_pd                   = min(find(full_obj.arrivals(pt,:)));
                tdiff                        = t(pt,:) - [0 t(pt,1:end-1)];
                % split evenly
                simple_obj.days_per_pd(pt,:)  = simple_obj.days_per_pd(pt,:)./tdiff;
                % constant in the interval
                for fn = {'mu','sigma','hazard','feasible','score','days_per_pd','V_fact','status_quo_V'}    
                    fn = fn{1};
                    dim = sum(size(simple_obj.(fn))>1);
                    switch dim
                        case 1,
                            if pt == 1, full_obj.(fn) = zeros(N_patients,1); end
                            full_obj.(fn)(pt)       = simple_obj.(fn)(pt);
                        case 2,
                            if pt == 1, full_obj.(fn) = zeros(N_patients,N_time); end
                            full_obj.(fn)(pt,t(pt,:)) = simple_obj.(fn)(pt,:)-[0 simple_obj.(fn)(pt,1:end-1)];
                            full_obj.(fn)(pt,:)       = cumsum(full_obj.(fn)(pt,:),2);
                        case 3, 
                            if pt == 1, full_obj.(fn) = zeros(N_patients,N_time,N_donors); end
                            full_obj.(fn)(pt,t(pt,:),:) = simple_obj.(fn)(pt,:,:)-[zeros(1,1,N_donors) simple_obj.(fn)(pt,1:end-1,:)];
                            full_obj.(fn)(pt,:,:)     = cumsum(full_obj.(fn)(pt,:,:),2);
                    end
                end
                if isempty(simple_obj.alloc), continue; end
                % constant in the interval
                for fn = {'q','V','pr_tx','p'}
                    fn = fn{1};
                    dim = sum(size(simple_obj.alloc.(fn))>1);
                    switch dim
                        case 1,
                            if pt == 1, full_obj.alloc.(fn) = zeros(N_patients,1); end
                            full_obj.alloc.(fn)(pt)       = simple_obj.alloc.(fn)(pt);
                        case 2,
                            if pt == 1, full_obj.alloc.(fn) = zeros(N_patients,N_time); end
                            full_obj.alloc.(fn)(pt,t(pt,:)) = simple_obj.alloc.(fn)(pt,:)-[0 simple_obj.alloc.(fn)(pt,1:end-1)];
                            full_obj.alloc.(fn)(pt,:)       = cumsum(full_obj.alloc.(fn)(pt,:),2);
                        case 3, 
                            if pt == 1, full_obj.alloc.(fn) = zeros(N_patients,N_time,N_donors); end
                            full_obj.alloc.(fn)(pt,t(pt,:),:) = simple_obj.alloc.(fn)(pt,:,:)-[zeros(1,1,N_donors) simple_obj.alloc.(fn)(pt,1:end-1,:)];
                            full_obj.alloc.(fn)(pt,:,:)     = cumsum(full_obj.alloc.(fn)(pt,:,:),2);
                    end
                end
            end
        end
        end
        
        function s = export2solver(obj)
                N_donors = size(obj.mu,3);
                list_of_fields = {'mu','sigma','arrivals','hazard',...
                                    'endowment','timeout_prob','pr_compat','pareto_weights','rho','status_quo_V'};
                for fn = list_of_fields    %enumerate fields
                    try
                        s.(fn{1}) = obj.(fn{1});   %and copy
                    catch
                        warning('Could not copy field %s', fn{1});
                    end
                end
                % Days per period        
                s.h                 = obj.days_per_pd;
                % Feasible;
                s.include_pattern   = obj.feasible;
                % Term that discounts w        
                s.discount_w        = obj.lambda.*s.h;
                % Term that discounts V in first-best and does not depend on q
                s.discount_V1  = (1 + (obj.hazard+obj.rho).*s.h);
                % Term that discounts V in second-best
                s.discount_V2  = (1 + (obj.hazard+obj.rho).*s.h);
                % Other constants that need to be computed just once:
                s.delta_x_h =  obj.hazard.*s.h;
                s.lambdah_over_K = obj.lambda.*s.h/N_donors;
        end
        
        function obj = mechanism(obj,cf_mech,alloc_init)
            obj.use_fact_V = cf_mech.use_fact_V;
            obj.periods_from_arrival = cf_mech.simplify_times;
            if isfield(cf_mech,'heuristic')
                obj.heuristic = cf_mech.heuristic;
            end
            if strcmp(cf_mech.type,'score'), 
                obj.score  = cf_mech.score;   % Add score info
            end
            if ~isempty(cf_mech.simplify_times)
                obj      = obj.simplify_times(cf_mech.simplify_times);  % Simplify times;
            end
            if nargin==3 && ~isempty(alloc_init) && isfield(alloc_init,'q') && isfield(alloc_init,'V')
                obj.alloc = alloc_init;  % Use alloc_init if provided
            end
            if obj.use_fact_V
                obj.alloc.V = obj.V_fact;  % Restore V to V_fact if use_V_fact;
            end
            if isfield(cf_mech,'bound_pareto_ratio')
                % Upper bound for the pareto weight: limits influence of patients with high weight
                obj.pareto_weights = min(obj.pareto_weights,min(obj.pareto_weights)*cf_mech.bound_pareto_ratio);
            end
            if isfield(cf_mech,'timeout') && ~ cf_mech.timeout
                obj.timeout_prob = obj.timeout_prob * 0;
            end
        end
        
        function [state,heuristic] = find_ss_allocation(obj);
            [state,heuristic] = score_ss_allocation(obj.export2solver,obj.score,obj.alloc,obj.use_fact_V,obj.heuristic);
        end
        
        
        function obj = set_default_alloc(obj,force)
            if nargin==2 && force,      obj.alloc = [];                          end
            if ~isfield(obj.alloc,'q'), obj.alloc.q = ones(size(obj.mu))*0.3;    end
            if ~isfield(obj.alloc,'V'), obj.alloc.V = obj.V_fact;                end
            if ~isfield(obj.alloc,'p'), obj.alloc.p = zeros(size(obj.V_fact));   end 
        end
        
        function obj = set_rand_alloc(obj,force)
            if nargin==2 && force,      obj.alloc = [];                          end
            if ~isfield(obj.alloc,'q'), obj.alloc.q = rand(size(obj.mu))*0.3;    end
            if ~isfield(obj.alloc,'V'), obj.alloc.V = rand(size(obj.V_fact));   end
            if ~isfield(obj.alloc,'p'), obj.alloc.p = rand(size(obj.V_fact));   end 
        end

        function output = export2main(obj,options);
            % Maps the new framework to the old one from (p,q,V) to (pt_weights,pool_size,pi_this). 
            [N_patients,N_time,N_donors] = size(obj.mu);
            if nargin<3 || isempty(options), options = struct(); end
            % data.options.donor_weight;
            options = defaultvalue(options,'donor_weight',ones(1,N_donors)/N_donors);
            donorweighted = @(q) bsxfun(@times,q,reshape(options.donor_weight,1,1,[]));
            output.p_tx       = donorweighted(obj.alloc.q.*obj.alloc.pr_tx);   % probability that a patient gets a transplant conditional on donor j arrival
            output.pi_this    = donorweighted(obj.alloc.q); % pi_this: probability that patient i receives
                                                    % an offer for kidney k given that a donor arrives today.
                                                    %q: probability of an offer conditional on donor j arrival
            pt_output.av_p_tx = sum(output.p_tx,3);   % probability that a patient gets a transplant conditional on a donor arrival
            output.q_tx       = obj.lambda*pt_output.av_p_tx;   % daily transplants in a bin
            output.V          = obj.alloc.V;                             % Value Function
            pt_mass           = obj.alloc.p.*obj.days_per_pd;            % total number of patients in each bin
            output.pool_size  = sum(sum(pt_mass));                   % pool size
            output.pt_weights = pt_mass/output.pool_size;            % composition of patients (output.pt_weights(:) adds up to 1)
            output.pt_output  = pt_output;
            output.total_tx_rate  = sum(sum(output.q_tx.*output.pt_weights))*output.pool_size;  % daily transplants in the system
            
            output.frac_transplanted = output.total_tx_rate/(options.donor_weight*obj.endowment*obj.lambda); % fraction of kidneys transplanted: daily transplants/daily arrivals;
            output.dom_types         = sum(output.pt_weights,2)>1/(N_patients+1);
            output.vf_decomp         = obj.vf_decomp;
            output.V_ref_dec         = obj.V_ref_dec;
            % How to recover (p,q,V) from the output (old) structure of a score mechanism
            % V = output.V;
            % p = output.pt_weights * output.pool_size ./obj.days_per_pd;
            % q =  pi_this * 1/donor_weight;
            if isfield(obj.alloc,'diff_norm'),
                output.diff_norm  = obj.alloc.diff_norm;                     % Norm difference
                output.iter       = obj.alloc.iter;                          % Number of iterations
            else
                output.diff_norm  = obj.alloc.exitflag;
                output.iter       = obj.alloc.iterations;
            end
        end

        function s = saveobj(obj)   % Saving behavior
            s = struct(obj);
        end
        
        
        function obj = fill_vf_decomp(obj)              %%% Decompose value function into observed and unobserved components
            p      = obj.alloc.p;
            q      = obj.alloc.q;
            V      = obj.alloc.V;
            [obj.vf_decomp,~,obj.V_ref_dec]  = vf_decomposition_flow(q,p,V,obj.export2solver,obj.V_ref_dec);
        end

    end % methods
        
   methods(Static)
       function obj = loadobj(s)   % Loading behavior
          if isstruct(s)
             obj = optimal_ss_problem(s);
          else
             obj = s;
          end
       end
    end % static methods
end

