// Creates donor categories from KIDPAN variables (should be run on KIDPAN data)

program generate_donor_categories

  gen donor_cat = ""
  replace donor_cat = "young_healthy" if age_don < 35 & ecd_donor == 0 & non_hrt_don == "N" & don_ty=="C"
  replace donor_cat = "old_healthy" if age_don >= 35 & ecd_donor == 0 & non_hrt_don == "N" & don_ty=="C"
  replace donor_cat = "young_dcd" if age_don < 35 & ecd_donor == 0 & non_hrt_don == "Y" & don_ty=="C"
  replace donor_cat = "old_dcd" if age_don >= 35 & ecd_donor == 0 & non_hrt_don == "Y" & don_ty=="C"
  replace donor_cat = "ecd" if ecd_donor == 1 & non_hrt_don == "N" & don_ty=="C"
  replace donor_cat = "ecd_dcd" if ecd_donor == 1 & non_hrt_don == "Y" & don_ty=="C"

end
