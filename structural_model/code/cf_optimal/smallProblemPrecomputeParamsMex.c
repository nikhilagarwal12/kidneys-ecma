#include "mex.h"

#include <math.h>
#include <string.h>
#include <stdint.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  double *sigma = (double*)mxGetPr(mxGetField(prhs[0], 0, "sigma"));
  double *mu = (double*)mxGetPr(mxGetField(prhs[0], 0, "mu"));
  double *deltaBy_h = (double*)mxGetPr(mxGetField(prhs[0], 0, "delta_x_h"));
  double *lambda_hOverK = (double*)mxGetPr(mxGetField(prhs[0], 0, "lambdah_over_K"));
  double *discountW = (double*)mxGetPr(mxGetField(prhs[0], 0, "discount_w"));
  double *endowment = (double*)mxGetPr(mxGetField(prhs[0], 0, "endowment"));
  double *prCompatRepeat = (double*)mxGetPr(mxGetField(prhs[0], 0, "pr_compat_repeat"));
  double *paretoWeightsRepeat = (double*)mxGetPr(mxGetField(prhs[0], 0, "pareto_weights_repeat"));
  double *timeOutProb = (double*)mxGetPr(mxGetField(prhs[0], 0, "timeout_prob"));
  double *h = (double*)mxGetPr(mxGetField(prhs[0], 0, "h"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[0], 0, "mu"));
  const long nPatients = sizeArray[0];
  const long nTime = sizeArray[1];
  const long nDonors = sizeArray[2];
  const long nQ = nPatients*nTime*nDonors;
  const long nP = nPatients*nTime;
  uint64_t *filterPattern = (uint64_t*)mxGetPr(prhs[1]);
  uint64_t *filterDonors = (uint64_t*)mxGetPr(prhs[2]);
  const uint32_t maxTransplant = *((uint32_t*)mxGetPr(mxGetField(prhs[0], 0, "maximize_transplants")));

  plhs[0] = mxDuplicateArray(prhs[0]);
  mxArray* params = plhs[0];
  /* Input parameters needed for smaller problem */
  mxAddField(params, "map");
  mxArray* mapM = mxCreateNumericMatrix(nQ+2*nP, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "map", mapM);
  uint64_t* map = (uint64_t*)mxGetPr(mxGetField(params, 0, "map"));
  unsigned long qCount = 0;
  unsigned long indexT = 0;
  unsigned long indexTemp = 0;
  for (unsigned long i = 0; i < (nQ+2*nP); ++i) {
    if (filterPattern[i]) {
      map[i] = indexT++;
    }
    if (i == (nQ-1)) {
      qCount = indexT;
    }
    if (i == (nQ-1+nP-nPatients)) {
      indexTemp = indexT;
    }
  }
  const unsigned long pCount = (indexT-qCount)/2;
  const unsigned long pM1Count = indexTemp-qCount;
  mxAddField(params, "qCount");
  mxArray* t1M = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "qCount", t1M);
  uint64_t* t1 = (uint64_t*)mxGetPr(mxGetField(params, 0, "qCount"));
  (*t1) = qCount;
  mxAddField(params, "pCount");
  t1M = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "pCount", t1M);
  t1 = (uint64_t*)mxGetPr(mxGetField(params, 0, "pCount"));
  (*t1) = pCount;
  mxAddField(params, "pM1Count");
  t1M = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "pM1Count", t1M);
  t1 = (uint64_t*)mxGetPr(mxGetField(params, 0, "pM1Count"));
  (*t1) = pM1Count;
  mxAddField(params, "nDonorsInv");
  t1M = mxCreateDoubleMatrix(1, 1, mxREAL);
  mxSetField(params, 0, "nDonorsInv", t1M);
  double* d1 = (double*)mxGetPr(mxGetField(params, 0, "nDonorsInv"));
  (*d1) = 1.0/((double)nDonors);

  mxAddField(params, "muFiltered");
  mxArray* muFilteredM = mxCreateDoubleMatrix(qCount, 1, mxREAL);
  mxSetField(params, 0, "muFiltered", muFilteredM);
  double* muFiltered = (double*)mxGetPr(mxGetField(params, 0, "muFiltered"));
  indexT = 0;
  for (unsigned long i = 0; i < nQ; ++i) {
    if (filterPattern[i]) {
      muFiltered[indexT++] = mu[i]; 
    }
  }
  mxAddField(params, "mapEq");
  mapM = mxCreateNumericMatrix(nP, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "mapEq", mapM);
  uint64_t* mapEq = (uint64_t*)mxGetPr(mxGetField(params, 0, "mapEq"));
  mxAddField(params, "sigmaFiltered");
  muFilteredM = mxCreateDoubleMatrix(pCount, 1, mxREAL);
  mxSetField(params, 0, "sigmaFiltered", muFilteredM);
  double* sigmaFiltered = (double*)mxGetPr(mxGetField(params, 0, "sigmaFiltered"));
  mxAddField(params, "sigmaFilteredInv");
  muFilteredM = mxCreateDoubleMatrix(pCount, 1, mxREAL);
  mxSetField(params, 0, "sigmaFilteredInv", muFilteredM);
  double* sigmaFilteredInv = (double*)mxGetPr(mxGetField(params, 0, "sigmaFilteredInv"));
  mxAddField(params, "delta_x_hFiltered");
  muFilteredM = mxCreateDoubleMatrix(pCount, 1, mxREAL);
  mxSetField(params, 0, "delta_x_hFiltered", muFilteredM);
  double* delta_x_hFiltered = (double*)mxGetPr(mxGetField(params, 0, "delta_x_hFiltered"));
  mxAddField(params, "lambdah_over_KFiltered");
  muFilteredM = mxCreateDoubleMatrix(pCount, 1, mxREAL);
  mxSetField(params, 0, "lambdah_over_KFiltered", muFilteredM);
  double* lambdah_over_KFiltered = (double*)mxGetPr(mxGetField(params, 0, "lambdah_over_KFiltered"));
  mxAddField(params, "discount_wFiltered");
  muFilteredM = mxCreateDoubleMatrix(pCount, 1, mxREAL);
  mxSetField(params, 0, "discount_wFiltered", muFilteredM);
  double* discount_wFiltered = (double*)mxGetPr(mxGetField(params, 0, "discount_wFiltered"));
  mxAddField(params, "hFiltered");
  muFilteredM = mxCreateDoubleMatrix(pCount, 1, mxREAL);
  mxSetField(params, 0, "hFiltered", muFilteredM);
  double* hFiltered = (double*)mxGetPr(mxGetField(params, 0, "hFiltered"));
  mxAddField(params, "pareto_weights_repeatFiltered");
  muFilteredM = mxCreateDoubleMatrix(pCount, 1, mxREAL);
  mxSetField(params, 0, "pareto_weights_repeatFiltered", muFilteredM);
  double* paretoWeightsRepeatFiltered = (double*)mxGetPr(mxGetField(params, 0, "pareto_weights_repeatFiltered"));

  unsigned long indexLambda = 0;
  uint64_t *filterPatternLambda = filterPattern + nQ;
  for (unsigned long i = 0; i < nP; ++i) {
    if (filterPatternLambda[i]) {
      sigmaFiltered[indexLambda] = sigma[i];
      sigmaFilteredInv[indexLambda] = 1.0/sigma[i];
      delta_x_hFiltered[indexLambda] = -deltaBy_h[i];
      lambdah_over_KFiltered[indexLambda] = lambda_hOverK[i];
      discount_wFiltered[indexLambda] = discountW[i];
      hFiltered[indexLambda] = h[i];
      paretoWeightsRepeatFiltered[indexLambda] = paretoWeightsRepeat[i];
      mapEq[i] = indexLambda++;
    }
  }
  indexLambda = 0;
  mxAddField(params, "mapF");
  mapM = mxCreateNumericMatrix(nDonors, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "mapF", mapM);
  uint64_t* mapF = (uint64_t*)mxGetPr(mxGetField(params, 0, "mapF"));
  for (unsigned long i = 0; i < nDonors; ++i) {
    if (filterDonors[i]) {
      mapF[i] = indexLambda++;
    }
  }
  const unsigned long donorCount = indexLambda;
  mxAddField(params, "donorCount");
  t1M = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "donorCount", t1M);
  t1 = (uint64_t*)mxGetPr(mxGetField(params, 0, "donorCount"));
  (*t1) = donorCount;
  mxAddField(params, "endowmentFiltered");
  muFilteredM = mxCreateDoubleMatrix(donorCount, 1, mxREAL);
  mxSetField(params, 0, "endowmentFiltered", muFilteredM);
  double* endowmentFiltered = (double*)mxGetPr(mxGetField(params, 0, "endowmentFiltered"));
  for (unsigned long i = 0; i < nDonors; ++i) {
    if (filterDonors[i]) {
      endowmentFiltered[mapF[i]] = endowment[i];
    }
  }
  if (maxTransplant) {
    double *slackCost = (double*)mxGetPr(mxGetField(prhs[0], 0, "slack_cost"));
    mxAddField(params, "slack_costFiltered");
    muFilteredM = mxCreateDoubleMatrix(donorCount, 1, mxREAL);
    mxSetField(params, 0, "slack_costFiltered", muFilteredM);
    double* slackCostFiltered = (double*)mxGetPr(mxGetField(params, 0, "slack_costFiltered"));
    for (unsigned long i = 0; i < nDonors; ++i) {
      if (filterDonors[i]) {
        slackCostFiltered[mapF[i]] = slackCost[i];
      }
    }
  }

  mxAddField(params, "npIndex");
  mapM = mxCreateNumericMatrix(qCount, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "npIndex", mapM);
  uint64_t* npIndex = (uint64_t*)mxGetPr(mxGetField(params, 0, "npIndex"));
  mxAddField(params, "donorIndex");
  mapM = mxCreateNumericMatrix(qCount, 1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "donorIndex", mapM);
  uint64_t* donorIndex = (uint64_t*)mxGetPr(mxGetField(params, 0, "donorIndex"));
  mxAddField(params, "donorList");
  mapM = mxCreateNumericMatrix(pCount, nDonors+1, mxUINT64_CLASS, mxREAL);
  mxSetField(params, 0, "donorList", mapM);
  uint64_t* donorList = (uint64_t*)mxGetPr(mxGetField(params, 0, "donorList"));

  for (unsigned long i = 0; i < pCount; ++i) {
    donorList[i*(nDonors+1)] = 1;
  }
  for (unsigned long j = 0, index = 0; j < nDonors; ++j) {
    for (unsigned long i = 0; i < nP; ++i, ++index) {
      if (filterPattern[index]) {
        npIndex[map[index]] = mapEq[i];
        donorIndex[map[index]] = mapF[j];
        const unsigned long index1 = mapEq[i]*(nDonors+1);
        const unsigned long index2 = index1+donorList[index1];
        donorList[index2] = map[index]; 
        ++donorList[index1];
      }
    }
  }
  mxAddField(params, "pr_compat_repeatFiltered");
  mapM = mxCreateDoubleMatrix(qCount, 1, mxREAL);
  mxSetField(params, 0, "pr_compat_repeatFiltered", mapM);
  double* prCompatRepeatFiltered = (double*)mxGetPr(mxGetField(params, 0, "pr_compat_repeatFiltered"));
  for (unsigned long i = 0; i < nQ; ++i) {
    if (filterPattern[i]) {
      prCompatRepeatFiltered[map[i]] = prCompatRepeat[i];
    }
  }
  mxAddField(params, "timeout_probFiltered");
  muFilteredM = mxCreateDoubleMatrix(donorCount, 1, mxREAL);
  mxSetField(params, 0, "timeout_probFiltered", muFilteredM);
  double* timeOutProbFiltered = (double*)mxGetPr(mxGetField(params, 0, "timeout_probFiltered"));
  for (unsigned long i = 0; i < nDonors; ++i) {
    if (filterDonors[i]) {
      timeOutProbFiltered[mapF[i]] = timeOutProb[i];
    }
  }
}
