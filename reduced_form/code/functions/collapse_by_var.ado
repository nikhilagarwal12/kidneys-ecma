// This function collapses a set of collapse variables by a grouping variable

program collapse_by_var

args collapse_string byvar countvar tempname sample_cut

preserve

collapse `collapse_string', by(`byvar')
rename `countvar' count
rename `byvar' group
gen sample = "`sample_cut'"
order sample group count
append using `tempname'
save `tempname', replace

restore

end
