% Perform convergence diagnostics and plot chain draws
% Performing the convergence whenever the results are generated
% forces the use of the most updated chains in the convergence diagnostics
function conv_and_results(chain_summary,data,model,options)

% list of variable names corresponding to CCP parameters
K = model.fixed.K;
Kf = model.flex.K;

coefnames = {};
coefnames_clean = {};

% Coefficients common across patient types (all variables if no patient UH)
for ii = 1:K
    coefnames{ii} = data.fixed.colheaders{ii}; 
end

% Flexible coefficients must denote corresponding patient type
if model.pt_uh
    for kk = 1:model.K_types
        for ii = 1:Kf
            coefnames{K + (kk-1)*Kf + ii} = [data.flex.colheaders{ii} ' (Type ' num2str(kk) ')'];
        end
    end
end

for ii=1:length(coefnames);    coefnames_clean{ii} = strrep(coefnames{ii},'_',' '); end

% compute mean and covariance matrix of chain
chain.mean = chain_summary.mu_theta;
chain.se = sqrt(diag(chain_summary.cov_theta));
if options.diagnostics, chain.draws = chain_summary.theta_draws_trim;, end

% Renormalize the chain_mean, if necessary
if model.renormalize

    if model.pt_uh
        % if renormalizing, constant term must be flexible with patient UH
        assert(model.flex.constant & ~model.fixed.constant);
        constants_ii = K + (0:(model.K_types-1))*Kf + 1; 
        % constants should be in first position for the flexible coefficients of each patient type
        for index = 1:length(constants_ii)
            assert(strfind(coefnames{constants_ii(index)},'constant') > 0);
        end
        % constant is not normalized, but we act as though it is
        assert(data.flex.sigma_dm(1) == 1);
        assert(data.flex.mu_dm(1) == 0);
        
        % transformation matrix
        sigma_vec = [data.fixed.sigma_dm repmat(data.flex.sigma_dm,1,model.K_types)];
        mu_vec = [data.fixed.mu_dm repmat(data.flex.mu_dm,1,model.K_types)];
        trans_mat = diag(1./sigma_vec);
        % Separate adjustment for the constant terms by patient type
        for kk = 1:model.K_types
            index_kk = 1:K;
            if Kf>1; index_kk = [1:K (K + (kk-1)*Kf + (2:Kf))]; end   % if variables other than constant term are flexible
            trans_mat(constants_ii(kk),index_kk) = -mu_vec(index_kk)./sigma_vec(index_kk);
        end
    else
        assert(isequal(coefnames{1},'constant'));
        assert(data.fixed.sigma_dm(1) == 1);
        assert(data.fixed.mu_dm(1) == 0);
        
        % Generate the transformation matrix
        trans_mat = diag(1./data.fixed.sigma_dm); % Scale 
        trans_mat(1,2:end) = - data.fixed.mu_dm(2:end)./data.fixed.sigma_dm(2:end); % Fix the constant
    end
    
    % Adjust point estimates and standard errors
    chain.mean = trans_mat*chain.mean;
    chain.se = sqrt(diag(trans_mat*chain_summary.cov_theta*trans_mat'));  % By bilinearity of covariance
end

% Add variables and coefficient names if donor UH is specified
if model.donor_uh
    K = K+model.K_types*Kf+1;
    coefnames{end+1}='sigma_eta';
    coefnames_clean{end+1}='sigmaeta';

    chain.mean = [chain.mean; chain_summary.mu_sigma_eta];
    chain.se = [chain.mean; chain_summary.se_sigma_eta];
    if options.diagnostics, chain.draws = [chain.draws chain_summary.sigma_eta_draws_trim]; end
end

% Same for patient UH
if model.pt_uh
    K = K + model.K_types;
    for kk = 1:model.K_types
        coefnames{end+1}=['p_type' num2str(kk)];
        coefnames_clean{end+1} = ['ptype' num2str(kk)];
    end
    chain.mean = [chain.mean; chain_summary.mu_beta];
    chain.se = [chain.se; sqrt(diag(chain_summary.V_beta))];
    if options.diagnostics, chain.draws = [chain.draws chain_summary.beta_draws_trim]; end
end

% compute PSRF statistics 
if options.diagnostics, [R,neff,Vh,W,B,tau,thin] = psrf(chain.draws);, end

% construct cell array of important diagnostics, output to text file
results = {};
for ii = 1:K       
    results{ii,1} = coefnames{ii};
    results{ii,2} = chain.mean(ii);
    results{ii,3} = chain.se(ii);
    if options.diagnostics
        results{ii,4} = R(ii);
        results{ii,5} = neff(ii);
    end
end

% Create the directory, if necessary
model_results_folder = [options.results_folder model.model_name];
if ~isdir(model_results_folder)
    mkdir(model_results_folder);
    mkdir([model_results_folder '/figs']);
    fprintf('Created folder %s\n', model_results_folder);
end    

   
% Generate the results table
if ~options.diagnostics
    results_table = cell2table(results,'VariableNames',{'variable','mean_chain','sd_chain'});

else
    results_table = cell2table(results,'VariableNames',{'variable','mean_chain','sd_chain','PSRF','num_eff_obs'});

    % make plots for path of each coefficient, and histograms
    for ii = 1:K
        plot(1:size(chain.draws,1),chain.draws(:,ii));
        title(['Draws of ' coefnames_clean{ii}]);
        saveas(gcf,[model_results_folder '/figs/chain_draws_' coefnames{ii} '.png']);    
    end

    for ii = 1:K
        histogram(chain.draws(:,ii));
        title(['Hist of ' coefnames_clean{ii}]);
        saveas(gcf,[model_results_folder '/figs/histogram_' coefnames{ii} '.png']);    
    end

end

% Write the table
writetable(results_table,[model_results_folder '/chain_diagnostics' model.model_name '.txt']);

