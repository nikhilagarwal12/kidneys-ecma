% Generates a piecewise linear function
function [Pn,coeffnames,knots] = pwlin(knots,X,varname,add_knots)
    
    if nargin <4, add_knots = 1; end
    % Append min max, if necessary
    if add_knots && min(X)<min(knots)
        knots(2:end+1) = knots;
        knots(1) = floor(min(X));
    end
    if add_knots && max(X)>max(knots) 
        knots(end+1) = ceil(max(X)+1);
    end

    % Number of intervals, and initialization of Pn
    nsegments = length(knots)-1;
    Pn = zeros(size(X,1),nsegments);
    coeffnames = cell(1,nsegments);   

    % Loop over intervals
    Pn(:,1) = X;
    coeffnames{1} = varname;

    for ii = 2:nsegments 
        % Define the ii-th component
        Pn(:,ii) = (X-knots(ii)).*all([knots(ii)<=X],2);

        % Varnames
        coeffnames{ii} = [varname '_gt_' num2str(knots(ii))];
    end
end