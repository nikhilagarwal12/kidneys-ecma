// DO NOT run on own; it should be called within gen_primary_files.do
// This file creates the primary files based on KIDPAN_DATA.DTA:
//  - kidpan
//  - patients
//  - kidpan_donors


****************************
********** KIDPAN **********
****************************
if `kidpan'==1{
  
  // our wl_id sample contains all kidpan observations we are interested in
  use "`kidpan_data'KIDPAN_DATA.DTA", clear

  // First init date by patient
  bys pt_code: egen first_init_date = min(init_date)
  replace first_init_date = . if pt_code == .
  by pt_code: gen n_listings = _N    // keeping track of number of separate wl_id_codes for diagnostics later

  // Merge with sample wl_id_code's
  merge m:1 wl_id_code using "`primary_files'sample_wl_id_codes.dta"
  keep if _merge == 3
  drop _merge
  isid wl_id_code
  
  // Merge with center/opo information
  merge 1:m wl_id_code using "`location_data'kidpan_non_std.DTA", keep(master match) nogen

  // get IDs for each location variable:
    // - listing_ctr: 8 char code
    // - tx_ctr: 8 char code
    // - donor_hospital: 6-7 digit code, but may not be unique, text seems to change sometimes. Not coding for now
  gen listing_ctr_id = substr(listing_ctr,1,8)
  gen tx_ctr_id = substr(tx_ctr,1,8)
      
  // Get opo region
  merge m:1 opo_ctr using "`ctr_dsa_def'opo_ctr_id_bridge.dta"

  drop if _merge==2
  drop _merge
  rename region_id opo_region_id
  rename listing_ctr_dsa_id opo_dsa_id

  * Get listing center region 
  merge m:1 listing_ctr_dsa using "`ctr_dsa_def'dsa_region_bridge", keep(master match) nogen
  rename region_id listing_ctr_region_id

  * Get tx center info, though it almost always equals listing center
  rename listing_ctr_dsa listing_ctr_dsa_2  // renaming for merge with id file
  rename listing_ctr_dsa_id listing_ctr_dsa_id_2
  rename tx_ctr_dsa listing_ctr_dsa
  merge m:1 listing_ctr_dsa using "`ctr_dsa_def'dsa_region_bridge", keep(master match) nogen
  rename listing_ctr_dsa tx_ctr_dsa
  rename listing_ctr_dsa_id tx_ctr_dsa_id
  rename region_id tx_ctr_region_id
  rename listing_ctr_dsa_2 listing_ctr_dsa
  rename listing_ctr_dsa_id_2 listing_ctr_dsa_id


  ***************************
  * patient characteristics *
  ***************************
    
  * We already have blood type (abo) and final ecd (on_expand_donor, on_iexpand_donor)

  // Get DOB, calculate 18th bday for pediatric candidates
  merge m:1 wl_id_code using "`non_std_4_data'ped_wl_dob_ki.DTA", keep(master match) nogen
  gen date_turn_18 = mdy(month(dob),day(dob),year(dob)+18)  // 18th birthday for pediatric candidates. Missing for other candidates  

  // Ped_list - whether under 18 at listing
  gen Ped_List = "Non-Ped At Listing"
  replace Ped_List = "Ped At Listing" if init_age < 18

  // Identify prior transplants
  bys pt_code: egen first_ki_tx_date = min(tx_date)
  bys pt_code: egen first_ki_prev_tx_date = min(prev_ki_date)  // for the event where a prior transplant doesn't have its own wl_id_code
  gen first_tx_date = min(first_ki_tx_date,first_ki_prev_tx_date)
  replace first_tx_date = . if pt_code == .  

  // recode prior_living_donor string -> 1/0
  gen prior_living_donor = donation=="Y"
  
  
  *************************
  * donor characteristics *
  *************************

  // type of donor, used for ranking candidates
  generate_donor_categories
  tab donor_cat
  
  // Calculate donor KDRI
  compute_kdri `pr_hypertension' `pr_diabetes'
  codebook donor_id
  summarize kdri, detail

  format init_age %9.0g
  compress

  rename Ped_List ped_list
  
  * Save a baseline KIDPAN file
  if `full_sample'==1{
    save "`primary_files'kidpan.dta", replace
  }
    
  if `test_sample'==1{
    merge 1:1 wl_id_code using `test_sample_files'sample_wl_id_codes, keep(match) nogen
    save `test_sample_files'kidpan, replace
  }
}

*  Patient characteristics from KIDPAN (wl_id_code should be a unique key)

if `patient'==1{
 
  use "`primary_files'kidpan", clear
  drop `donor_char'

  // mark patients also listed for a pancreas
  gen awaits_PA = wl_org=="KP" | (wl_org=="KI" & wlpa=="Y") | (wl_org=="PA" & wlki=="Y")

  tempfile temp0
  save `temp0'

  // generate prior_living_donor variable from offer data, merge back. There is no such variable in kidpan, so we must use 'descrip' in the offer file.
 
  use "`primary_files'ptr_primary", clear
  
  // incompatible paired donor. Record DATE of first "descrip" with that title. Missing if never appears for a patient
  gen incomp_live_donor = strpos(classification, "Incompatible") > 0
  gen incomp_date = match_submit_dt if incomp_live_donor == 1
  bys wl_id_code: egen incomp_live_donor_date = min(incomp_date)
  
  // merge back to other patient variables
  keep wl_id_code incomp_live_donor_date
  duplicates drop

  merge 1:1 wl_id_code using `temp0'
  keep if _merge != 1
  drop _merge

  compress
  sort wl_id_code  

  if `full_sample'==1{
    save "`primary_files'patients.dta", replace
  }
    
  if `test_sample'==1{
    merge 1:1 wl_id_code using `test_sample_files'sample_wl_id_codes, keep(match) nogen
    save `test_sample_files'patients, replace
  }
  
}


***************************************
* donor characteristics from KIDPAN 
* -- one kidney transplant level file, 
*    one donor level file
***************************************

if `kidpan_donor'==1{

  // first create a transplant-level file, so a donor can have multiple rows
  use "`primary_files'kidpan", clear
  keep `donor_char' `match_char' trr_id_code
  sort donor_id
  merge m:1 donor_id using "`primary_files'sample_donor_ids"
  keep if _merge == 3
  drop _merge

  // merge additional (non-ABDR) antigens from KIDPAN_ADDTL_HLA.DTA
  isid trr_id_code
  merge 1:1 trr_id_code using "`kidpan_data'HLA Additional/KIDPAN_ADDTL_HLA.DTA", keepusing(dbw* dc* ddr5* ddq* ddp*)
  keep if _merge != 2
  drop _merge

  if `full_sample'==1{
      save "`primary_files'donations", replace
  }
  if `test_sample'==1{
      merge m:1 donor_id using `test_sample_files'sample_donor_ids, keep(match) nogen
      save "`test_sample_files'donations", replace
  }
    
  // donor-level table (one row per donor_id)
  keep `donor_level_chars'

  duplicates drop
  codebook donor_id
  bys donor_id: gen n_obs = _N
  drop if n_obs > 1 & donor_hospital == ""
  isid donor_id

  if `full_sample'==1{
      save "`primary_files'kidpan_donors.dta", replace
  }
    
  if `test_sample'==1{
      merge 1:1 donor_id using `test_sample_files'sample_donor_ids, keep(match) nogen
      save `test_sample_files'kidpan_donors, replace
  }

}
