from math import ceil
import csv


def longseqComplete(X):
	prevelem=[-1 for x in X]
	maxlist =[-1 for x in X]
	L=0
	M=[-1]
	for i,x in enumerate(X):
		lo=0
		hi=L
		while lo<=hi:
			mid = (lo+hi+1)/2
			if  X[M[mid]] < x:
				lo = mid + 1
			else:
				hi = mid - 1
		newL = lo
		prevelem[i]=M[newL-1]
		maxlist[i]=newL
		if newL>L:
			L=newL
			M.append(-1)
		M[newL]=i
	return (M,prevelem,maxlist)

def longseqBare(X):
	prevelem=[-1 for x in X]
	L=0
	M=[-1]
	for i,x in enumerate(X):
		lo=1
		hi=L
		while lo<=hi:
			mid = (lo+hi+1)/2
			if  X[M[mid]] < x:
				lo = mid + 1
			else:
				hi = mid - 1
		newL = lo
		prevelem[i]=M[newL-1]
		# maxlist[i]=newL
		if newL>L:
			L=newL
			M.append(-1)
		M[newL]=i
	return (M[-1],prevelem)

	
def longseqIndices(k,prevelem):
	ix = [k]
	while prevelem[k]>=0:
		k=prevelem[k]
		ix.append(k)
	return(ix[::-1])	
		
		
def longseq(X):
	M,P= longseqBare(X)
	return longseqIndices(M,P)
	
def longseqIx(X):
	ix = [0]*len(X)
	for h in longseq(X):
		ix[h]=1
	return ix

filein = '../../tools/input/longseq.txt'
readfile = open(filein, 'rb')
lines = readfile.readlines()

fileout = '../../tools/output/longseq.txt'
writefile = open(fileout, 'wb')

for l in lines:
	X=[int(x) for x in l.split()]
	if len(X)>0:	
		writefile.write('\n'.join([str(z) for z in longseqIx(X)]))
		writefile.write('\n')	
	
	
writefile.close()

dofile = '../../tools/code/longseqToStata.do'
writedofile = open(dofile, 'wb')
dofilecode = """ 
insheet _in_seq using ../../tools/output/longseq.txt
gen long _mergeid = _n
save ../../tools/output/longseq.dta,replace

"""
writedofile.write(dofilecode)

