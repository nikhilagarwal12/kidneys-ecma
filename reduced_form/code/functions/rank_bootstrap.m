% Function to bootstrap the NM rank statistic for a collection of observations
% Re-draws from input set of donors iid, with replacement, generating a random order. Then computes NM rank stats
% Essential that donors, group, score ordered the same way
% Any decisions about restricting the set of donors, forming the groups, etc must be made outside

% Inputs:
%  - donors: list of donor_id's for keeping track 
%  - group: subsets within which rank statistic should be computed 
%  - score: orders donors within group (must draw tiebreaker)
%  - tiebr: tiebreaks drawn for actual sample
%  - B: number of bootstrap iterations

% Outputs:
%  - stats: the statistic in sample
%  - distributions: distribution of bootstrapped statistics under the null of iid donors and their cutoffs. Separated by group
%  - p: Pr(distribution < stat) for each group

function [stat,distribution,p,group_ids] = rank_bootstrap(group,score,tiebr,B)

% constants
N = length(group);
assert(length(score)==N & length(tiebr)==N)
group_ids = unique(group);
G = length(group_ids);

% draw donor sequences. First column is realization
perm = [((1:N)'/N) rand(N,B)];   % each column is ordered in time; each arriving donor is a random draw from original sample
[~,index] = sort(perm,2,'ascend');
tiebreak = [tiebr rand(N,B)];     % tie-breaker for ties in score
distribution = zeros(B+1,G);

% compute ranks for each sequence. Looping in case B or N are large
for b=1:(B+1)
  
    [~,index] = sort(perm(:,b),'ascend');

    % scores and groups for each sequence
    s_b = score(index);
    t_b = tiebreak(index);
    g_b = group(index);

    % ranks within each group, normalized by group size (so ~ uniformly distributed)
    for g=1:G
        g_id = group_ids(g);
        ii_gb = logical(g_b==g_id);
        Ig = sum(ii_gb);
        [~,I] = sortrows([s_b(ii_gb) t_b(ii_gb)],[1 2]);
        ranks = zeros(Ig,1);
        ranks(I) = ((1:Ig) - 0.5) / Ig;
        distribution(b,g) = sum((ranks(1:(end-1)) - ranks(2:end)).^2) / sum((ranks - 0.5).^2);  % mean(ranks) should be 0.5 mechanically
	assert(abs(mean(ranks)-0.5) < 0.001)
    end
    
end

% separate out sample statistic from bootstrapped distribution
stat = distribution(1,:);
distribution(1,:) = [];
p = zeros(1,G);
for g=1:G
	p(g) = mean(distribution(:,g) <= stat(g));
end
group_ids = group_ids';
