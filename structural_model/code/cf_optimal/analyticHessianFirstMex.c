#include "common.h"

#include "mex.h"

#include <math.h>
#include <string.h>
#include <stdint.h>

// A bunch of static variables to remove reallocation of arrays
static int notInitialized = 1;
static double *lambdaFeasibilityMod;
static long *iIndex, *jIndex;
static double *value;
static double *expTerm, *akc, *cLh;

void cleanup(void) {
  mexPrintf("MEX-file is terminating, destroying allocations in analytic first Hessian\n");
  free(iIndex);
  free(jIndex);
  free(value);
  free(lambdaFeasibilityMod);
  free(expTerm);
  free(akc);
  free(cLh);
  notInitialized = 1;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* Get input parameters */
  double *sigma = (double*)mxGetPr(mxGetField(prhs[2], 0, "sigma"));
  double *deltaBy_h = (double*)mxGetPr(mxGetField(prhs[2], 0, "delta_x_h"));
  double *lambda_hOverK = (double*)mxGetPr(mxGetField(prhs[2], 0, "lambdah_over_K"));
  double *discountW = (double*)mxGetPr(mxGetField(prhs[2], 0, "discount_w"));
  double *cArrayRepeat = (double*)mxGetPr(mxGetField(prhs[2], 0, "pr_compat_repeat"));
  double *h = (double*)mxGetPr(mxGetField(prhs[2], 0, "h"));
  double *paretoWeightsRepeat = (double*)mxGetPr(mxGetField(prhs[2], 0, "pareto_weights_repeat"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[2], 0, "mu"));
  const long nPatients = sizeArray[0];
  const long nTime = sizeArray[1];
  const long nDonors = sizeArray[2];
  const double nDonorsInv = (1.0/(double)nDonors);
  
  const uint32_t maxTransplant = *((uint32_t*)mxGetPr(mxGetField(prhs[2], 0, "maximize_transplants")));

  const long nQ = nPatients*nTime*nDonors;
  const long nP = nPatients*nTime;
  const long nTm1nP = nP-nPatients;
  const double *q = (double*)mxGetPr(prhs[0]);
  const double *p = q+nQ;
  const double *lambdaFeasibility = (double*)mxGetPr(mxGetField(prhs[1], 0, "ineqnonlin"));
  const double *lambdaSteadyState = (double*)mxGetPr(mxGetField(prhs[1], 0, "eqnonlin"));
  const double *lambdaValue = lambdaSteadyState+nTm1nP; 

  if (notInitialized) {
    lambdaFeasibilityMod = (double*)malloc(nDonors*sizeof(double));
    const long nnz = nQ*(nDonors+4l)+2l*nP;
    iIndex = (long*)malloc(nnz*sizeof(long));
    jIndex = (long*)malloc(nnz*sizeof(long));
    value = (double*)malloc(nnz*sizeof(double));
    expTerm = (double*)malloc(nP*sizeof(double));
    akc = (double*)malloc(nQ*sizeof(double));
    cLh = (double*)malloc(nQ*sizeof(double));
    mexAtExit(cleanup);
    notInitialized = 0;
  }
  memset(expTerm, 0, nP*sizeof(double));
  memcpy(lambdaFeasibilityMod, lambdaFeasibility, nDonors*sizeof(double));
  if (maxTransplant) {
    double *slackCost = (double*)mxGetPr(mxGetField(prhs[2], 0, "slack_cost"));
    for (unsigned long j = 0; j < nDonors; ++j) {
      lambdaFeasibilityMod[j] -= slackCost[j];
    }
  }

  unsigned long currentIndex = 0l;
  // Precompute common sub expressions
  for (unsigned long j = 0l, index = 0l; j < nDonors; ++j) {
    for (unsigned long i = 0l; i < nP; ++i, ++index) {
      akc[index] = lambdaValue[i]*discountW[i]*cArrayRepeat[index]*nDonorsInv;
      cLh[index] = cArrayRepeat[index]*lambda_hOverK[i];
    }
  }
  for (unsigned long j = 0l, index = 0l; j < nDonors; ++j, index += nPatients) {
    for (unsigned long i = 0l; i < nTm1nP; ++i, ++index) {
      expTerm[i] += q[index]*cArrayRepeat[index];
    }
  }
  for (unsigned long i = 0l; i < nTm1nP; ++i) {
    expTerm[i] = -lambdaSteadyState[i]*exp(-deltaBy_h[i]-lambda_hOverK[i]*expTerm[i]);
  } 
  /* Add qq Values */
  for (unsigned long k = 0l; k < nP; ++k) {
    const double localValue = p[k]*expTerm[k];
    for (unsigned long i = 0l, indexI = k; i < nDonors; ++i, indexI += nP) {
      const double cArrayLocal = localValue*cLh[indexI];
      for (unsigned long j = 0l, indexJ = k; j < nDonors; ++j, indexJ += nP) {
        double localValue1 = cLh[indexJ]*cArrayLocal;
        if (i == j) {
          localValue1 += akc[indexJ]*sigma[k]/normPdf(invCDF(1.0-q[indexJ]));
        }
        if (localValue1 != 0.0) {
          iIndex[currentIndex] = indexI;
          jIndex[currentIndex] = indexJ;
          value[currentIndex++] = localValue1;
        }
      }
    }
  }
  // Add qp and pq Values
  for (unsigned long i = 0l, index = 0l; i < nDonors; ++i) {
    for (unsigned long j = 0l; j < nTm1nP; ++j, ++index) {
      const double localValue = cArrayRepeat[index]*(h[j]*lambdaFeasibilityMod[i]-lambda_hOverK[j]*expTerm[j]);
      if (localValue != 0.0) {
        iIndex[currentIndex] = index;
        jIndex[currentIndex] = j+nQ;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = j+nQ;
        jIndex[currentIndex] = index;
        value[currentIndex++] = localValue;
      }
    }
    for (unsigned long j = nTm1nP; j < nP; ++j, ++index) {
      const double localValue = cArrayRepeat[index]*h[j]*lambdaFeasibilityMod[i];
      if (localValue != 0.0) {
        iIndex[currentIndex] = index;
        jIndex[currentIndex] = j+nQ;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = j+nQ;
        jIndex[currentIndex] = index;
        value[currentIndex++] = localValue;
      }
    }
  }
  // Add qv and vq values
  /* const unsigned long nPmnPa = nP - nPatients; */
  const unsigned long offset = nQ+nP;
  for (unsigned long i = 0l, index = 0l; i < nDonors; ++i, index += nP) {
    for (unsigned long j = 0l, index1 = index; j < nP; ++j, ++index1) {
      const double localValue = akc[index1];
      if (localValue != 0.0) {
        iIndex[currentIndex] = index1;
        jIndex[currentIndex] = j+offset;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = j+offset;
        jIndex[currentIndex] = index1;
        value[currentIndex++] = localValue;
      }
    }
  }
  // Add pv and vp values
  if (!maxTransplant) {
    for (unsigned long i = 0l; i < nP; ++i) {
      const double localValue = -h[i]*paretoWeightsRepeat[i]; // Contribution from new objective
      if (localValue != 0.0) {
        iIndex[currentIndex] = i+nQ;
        jIndex[currentIndex] = i+offset;
        value[currentIndex++] = localValue;
        iIndex[currentIndex] = i+offset;
        jIndex[currentIndex] = i+nQ;
        value[currentIndex++] = localValue;
      }
    }
  }
  // Create output sparse matrix
  const long matrixSize = nQ+2l*nP;
  plhs[0] = mxCreateSparse(matrixSize, matrixSize, currentIndex, mxREAL);
  double *sr  = (double*)mxGetPr(plhs[0]);
  size_t* irs = (size_t*)mxGetIr(plhs[0]);
  size_t* jcs = (size_t*)mxGetJc(plhs[0]);
  COO_toCSC(matrixSize, currentIndex, iIndex, jIndex, value, jcs, irs, sr);
}
