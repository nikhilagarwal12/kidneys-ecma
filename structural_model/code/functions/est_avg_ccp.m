% Computes the average CCP in sample and compares to what's predicted by the model '

function avg_ccp = est_avg_ccp(chain,data,model)
    
    % Avg choice in data
    avg_ccp.data = mean(data.cont_choice);
    
    % Preliminaries if patient and/or donor UH
    if model.pt_uh
        where_ii= cell(size(data.patient_list));
        for ii=1:data.N_patients
            where_ii{ii} = find(data.patients == data.patient_list(ii));           
        end
    end
    if model.donor_uh
     	data.eta_mat = create_dummies(data.donor_id,data.donor_list);
    end
    
    % Avg choice predicted by model; just use last 1000 draws
    chi = [data.fixed.obs repmat(data.flex.obs,1,model.K_types)];
    if model.donor_uh || model.pt_uh     % with donor and/or patient UH, we must average over individual type draws
        draws = size(chain.theta_draws_trim,1); draw_nos = 1:draws; avg_ccp.model = 0;
        if draws > 200      % limit to 200 draws
            gap = floor(draws/200);  draw_nos = draws:(-gap):(draws-199*gap);
        end
        for d = 1:length(draw_nos)
            if model.pt_uh  % with patient UH, update chi matrix for the current draw
                for kk = 1:model.K_types
                    where_kk = cat(1,where_ii{chain.beta_i_draws_trim(:,draw_nos(d))==kk});
                    chi(where_kk,(model.fixed.K + 1):model.K_theta) = chi_param_update(data,model,kk,where_kk);
                end
            end
            if model.donor_uh
                avg_ccp.model = avg_ccp.model + (1/length(draw_nos))*mean(normcdf(chi*chain.mu_theta + data.eta_mat*chain.eta_indiv_draws_trim(:,draw_nos(d))));
            else  % only patient UH
                avg_ccp.model = avg_ccp.model + (1/length(draw_nos))*mean(normcdf(chi*chain.mu_theta));
            end
        end
    else
        avg_ccp.model = mean(normcdf(chi*chain.mu_theta));
    end

    avg_ccp.gen_date = datetime('today');

end
