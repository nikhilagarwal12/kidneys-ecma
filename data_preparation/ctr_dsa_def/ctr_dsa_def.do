// Re-generate bridges between OPO/DSA and region
// Should be called in another script that defines the local `ctr_dsa_def', the folder where these files are stored

insheet using `ctr_dsa_def'opo_ctr_id_bridge.csv, clear
save `ctr_dsa_def'opo_ctr_id_bridge, replace

keep listing_ctr_dsa_id region
duplicates drop
drop if listing_ctr_dsa==""
isid listing_ctr_dsa_id
save `ctr_dsa_def'dsa_region_bridge, replace

clear
insheet using `ctr_dsa_def'listing_ctr_dsa_list.csv, names
merge 1:1 listing_ctr_dsa_id using `ctr_dsa_def'dsa_region_bridge
keep if _merge==3
drop _merge
save `ctr_dsa_def'dsa_region_bridge, replace

