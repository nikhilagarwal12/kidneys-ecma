function out = corr_gibbs(chain, type_diag, maxlag)

    n_diag = length(type_diag);
    
    
    fn_diag = cellfun(@str2func, type_diag, 'UniformOutput', false);
    for ii=1:n_diag
       fieldname =  func2str(fn_diag{ii});
       out.(fieldname) = fn_diag{ii}(chain, maxlag);
    end
    
    
end
