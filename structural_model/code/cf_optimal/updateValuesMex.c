#include "mex.h"
#include "matrix.h"

#include "common.h"

#include <math.h>
#include <stdint.h>
#include <string.h>

void mexFunction(int nlhs, mxArray *plhs[], int rhs, const mxArray *prhs[]) {
  double *q = (double*)mxGetPr(prhs[0]);

  double *a = (double*)mxGetPr(mxGetField(prhs[2], 0, "discount_w"));
  double *sigma = (double*)mxGetPr(mxGetField(prhs[2], 0, "sigma"));
  double *mu = (double*)mxGetPr(mxGetField(prhs[2], 0, "mu"));
  double *prCompat = (double*)mxGetPr(mxGetField(prhs[2], 0, "pr_compat"));
  double *discountV1 = (double*)mxGetPr(mxGetField(prhs[2], 0, "discount_V1"));
  const mwSize *sizeArray = mxGetDimensions(mxGetField(prhs[2], 0, "mu"));
  const unsigned long nPatients = sizeArray[0];
  const unsigned long nTime = sizeArray[1];
  const unsigned long nDonors = sizeArray[2];
  const unsigned long nPT = nPatients*nTime;
  const double nDonorInv = 1.0/nDonors;
  
  mwSize dims[3] = {nPatients, nTime, nDonors};
  plhs[1] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
  double *prTx = (double*)mxGetPr(plhs[1]);

  bool firstBest = false;
  bool useFactV = false;
  bool backwardsInduction = false;
  const mxArray* mx_field = mxGetField(prhs[2], 0, "first_best");
  if (mx_field != NULL) {
    if (((uint64_t)(mxGetScalar(mx_field)))) {
      firstBest = true;
    }
  } 
  if (!firstBest) {
    const mxArray* mx_field = mxGetField(prhs[2], 0, "use_fact_V");
    if (mx_field != NULL) {
      if (((uint64_t)(mxGetScalar(mx_field)))) {
        useFactV = true;
      }
    }
    if (!useFactV) {
      if (rhs == 4 && (mxGetM(prhs[3]) != 0 || mxGetN(prhs[3]) != 0)) {
        backwardsInduction = true;
      }
    }
  }

  double *w;
  w = (double*)calloc(nPT, sizeof(double));

  // Create V
  plhs[0] = mxCreateDoubleMatrix(nPatients, nTime, mxREAL);
  double *V = (double*)mxGetPr(plhs[0]);
  if (backwardsInduction) {
    double *Vold = (double*)mxGetPr(prhs[1]);
    const double positiveInfinity = INFINITY;
    double tol = mxGetScalar(mxGetField(prhs[3], 0, "tol"));
    long maxIter = (long)mxGetScalar(mxGetField(prhs[3], 0, "maxiter"));
    const mxArray* mx_field1 = mxGetField(prhs[3], 0, "verbose");
    bool verbose = false;
    if (mx_field1 != NULL) {
      if (((uint64_t)(mxGetScalar(mx_field1)))) {
        verbose = true;
      }
    }
    double *Vlb, *Vub, *VV, *TV, *Fz;
    Vlb = (double*)malloc(nPatients*sizeof(double));
    Vub = (double*)malloc(nPatients*sizeof(double));
    VV = (double*)malloc(nPatients*sizeof(double));
    TV = (double*)malloc(nPatients*sizeof(double));
    Fz = (double*)malloc(nPatients*nDonors*sizeof(double));
    double *discountV2 = (double*)mxGetPr(mxGetField(prhs[2], 0, "discount_V2"));
    double *b = discountV2; 
    double vDiff = 0.0;
    for (long k = nTime-1; k >= 0; --k) {
      const unsigned long offset2 = nPatients*k;
      const unsigned long offset1 = offset2 + nPatients;
      if (k == nTime-1) {
        memset(Vlb, 0, nPatients*sizeof(double));
      } else {
        for (unsigned long i = 0, index1 = offset1, index2 = offset2; i < nPatients; ++i, ++index1, ++index2) {
          Vlb[i] = V[index1]/b[index2];
        }
      }
      for (unsigned long i = 0, index2 = offset2; i < nPatients; ++i, ++index2) {
        Vub[i] = positiveInfinity;
        if (Vold[index2] > Vlb[i]) {
          VV[i] = Vold[index2];
        } else {
          VV[i] = Vlb[i];
        }
      }
      double max = 0.0;
      for (unsigned long iteration = 0; iteration < maxIter; ++iteration) {
        memset(w, 0, nPatients*sizeof(double));
        for (unsigned long j = 0, index2 = 0; j < nDonors; ++j) {
          const unsigned long offset3 = offset2 + j*nPT;
          for (unsigned long i = 0, index1 = offset3; i < nPatients; ++i, ++index2, ++index1) {
            const double z = (mu[index1]-VV[i])/sigma[i];
            Fz[index2] = normCdf(z);
            w[i] += q[index1]*prCompat[index2]*sigma[i]*(Fz[index2]*z+normPdf(z));
          }
        }
        max = 0.0;
        for (unsigned long i = 0, index1 = offset2, index2 = offset1; i < nPatients; ++i, ++index1, ++index2) {
          w[i] *= nDonorInv;
          if (k == (nTime-1)) {
            TV[i] = (a[index1]*w[i])/b[index1];
          } else {
            TV[i] = (a[index1]*w[i]+V[index2])/b[index1];
          }
          if (VV[i] > TV[i]) {
            if (VV[i] < Vub[i]) {
              Vub[i] = VV[i];
            }             
            if (TV[i] > Vlb[i]) {
              Vlb[i] = TV[i];
            }           
          } else {
            if (TV[i] < Vub[i]) {
              Vub[i] = TV[i];
            }
            if (VV[i] > Vlb[i]) {
              Vlb[i] = VV[i];
            }
          }
          VV[i] = 0.5*(Vub[i]+Vlb[i]);
          double diff = fabs(Vub[i]-Vlb[i]);
          if (diff > max) {
            max = diff;
          }
        }
        if (max < tol) {
          break;
        }
      }
      if (max > vDiff) {
        vDiff = max;
      }
      //Copy TV to V 
      memcpy(V+(k*nPatients), TV, nPatients*sizeof(double));
      //Copy to pr_tx
      for (unsigned long j = 0, index2 = 0; j < nDonors; ++j) {
        const unsigned long offset3 = offset2 + j*nPT;
        for (unsigned long i = 0, index1 = offset3; i < nPatients; ++i, ++index2, ++index1) {
          prTx[index1] = Fz[index2]*prCompat[index2];
        }
      }
    }
    if (verbose) {
      printf("Backwards induction norm : %e\n", vDiff);
    }
    free(Vlb);
    free(Vub);
    free(VV);
    free(TV);
    free(Fz);
  } else {
    double *b;
    b = (double*)calloc(nPT, sizeof(double));
    if (firstBest) {
      for (unsigned long i = 0, index4 = 0, index2 = 0; i < nDonors; ++i, index4 += nPatients) {
        for (unsigned long j = 0, index1 = 0; j < nTime; ++j) {
          for (unsigned long k = 0, index3 = index4; k < nPatients; ++k, ++index2, ++index1, ++index3) {
            const double fz = normPdf(invCDF(1.0-q[index2])); 
            w[index1] += (q[index2]*mu[index2]+sigma[k]*fz)*prCompat[index3];
            b[index1] += q[index2]*prCompat[index3];
            prTx[index2] = prCompat[index3];
          }
        }
      }
      for (unsigned long j = 0; j < nPT; ++j) {
        w[j] *= nDonorInv;
        b[j] = a[j]*b[j]*nDonorInv+discountV1[j];
      }
    } else {
      double *Vold = (double*)mxGetPr(prhs[1]);
      for (unsigned long i = 0, index4 = 0, index2 = 0; i < nDonors; ++i, index4 += nPatients) {
        for (unsigned long j = 0, index1 = 0; j < nTime; ++j) {
          for (unsigned long k = 0, index3 = index4; k < nPatients; ++k, ++index2, ++index1, ++index3) {
            const double z = (mu[index2] - Vold[index1])/sigma[k];
            const double fz = normPdf(z);
            const double Fz = normCdf(z);
            const double qc = q[index2]*prCompat[index3];
            double eMax;
            if (useFactV) {
              eMax = Fz*mu[index2] + fz*sigma[k]; 
              b[index1] += Fz*qc; 
            } else {
              eMax = (Fz * z + fz)*sigma[k]; 
            }
            w[index1] += qc*eMax;
            prTx[index2] = Fz*prCompat[index3];
          }
        }
      }
      if (useFactV) {
        for (unsigned long j = 0; j < nPT; ++j) {
          w[j] *= nDonorInv;
          b[j] = a[j]*b[j]*nDonorInv+discountV1[j];
        }
      } else {
        for (unsigned long j = 0; j < nPT; ++j) {
          w[j] *= nDonorInv;
        }
        double *discountV2 = (double*)mxGetPr(mxGetField(prhs[2], 0, "discount_V2"));
        memcpy(b, discountV2, nPT*sizeof(double));
      }
    }
    for (unsigned long i = 0, index = nPatients*(nTime-1); i < nPatients; ++i, ++index) {
      V[index] = a[index]*w[index]/b[index];
    }
    for (long j = nTime-2; j >= 0; --j) {
      unsigned long index1 = j*nPatients;
      unsigned long index2 = index1 + nPatients;
      for (unsigned long k = 0; k < nPatients; ++k, ++index1, ++index2) {
        V[index1] = (a[index1]*w[index1] + V[index2])/b[index1];
      }
    }
    free(b);
  }
  free(w);
}
