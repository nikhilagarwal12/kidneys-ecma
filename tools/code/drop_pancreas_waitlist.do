

***************************************************************************
* drop patients/donors related to pancreas waitlist from kidpan 
***************************************************************************
use "`primary_files'kidpan", clear
gen on_pa_waitlist = wlpa == "Y" | wl_org~="KI" 
// Tag donors who donate to these patients
bys donor_id : egen to_drop = max(on_pa_waitlist)
replace to_drop = 0 if donor_id == . & on_pa_waitlist == 0

preserve
keep if to_drop ==1 
keep donor_id
keep if donor_id != .
cap duplicates drop
tempfile pa_donors
save `pa_donors', replace
restore

preserve
keep if to_drop ==1 
keep wl_id_code
keep if wl_id_code != .
cap duplicates drop
tempfile pa_patient
save `pa_patient', replace
restore

drop if to_drop == 1
drop on_pa_waitlist to_drop
save "`primary_files'kidpan", replace

***************************************************************************
* drop patients waiting for pancreas waitlist from patients.dta 
***************************************************************************
use `primary_files'patients, clear 
cap drop _merge
merge 1:1 wl_id_code using `pa_patient', keep(master)
drop _merge
assert awaits_PA == 0
save `primary_files'patients, replace

***************************************************************************
* drop patients waiting for pancreas waitlist from patient_history.dta 
***************************************************************************
use `primary_files'patient_history, clear
cap drop _merge
merge m:1 wl_id_code using `pa_patient', keep(master)
drop _merge
save `primary_files'patient_history, replace


***************************************************************************
* drop deceased donors donating to these patients from deceased_donors.dta 
***************************************************************************
use `temp_base', clear
cap drop _merge
merge 1:1 donor_id using `pa_donors', keep(master)
drop _merge



