% Plots
function [output,data] =  collect_cf_output(data,data_filepath,rslt_filepath,mech_list,options)
% Collects outcomes of different mechanisms
% Options
if ~isfield(options,'run_splits') || ~any(options.run_splits), options.run_splits=1:numel(data); end
options.run_splits = unique([1 options.run_splits]); % For now it always includes the combined pool.

% Function that selects relevant fields
relevant_fields =@(x) flatten_structure(x,{'frac_transplanted','total_tx_rate','pt_weights','diff_norm','iter','V','pool_size','p_tx','vf_decomp'});

% Loop over mechanisms
jj = 0;
for alias = mech_list
    
    mech_filename = [data_filepath '_' alias{1}];
    rslt_filename = [rslt_filepath '_' alias{1} '_results'];
    temp_output   = [];
    
    % Loop over splits
    for ii = options.run_splits
        % split string: to save and load files
        if ii>1, split_string = num2str(ii); else split_string =''; end % First data is always the full data
        % Load results
        if exist([rslt_filename split_string '.mat'],'file')
            cf_mech              = load([mech_filename '.mat']);
            x                    = load([rslt_filename split_string]);
            if ~isfield(x.output,'vf_decomp')
                display('*** Populating output.vf_decomp because it is missing ***')
                x.output.vf_decomp.V_full = x.output.V;
                x.output.vf_decomp.V_obs = zeros(size(x.output.V));
                x.output.vf_decomp.V_unobs = zeros(size(x.output.V));
                x.output.vf_decomp.V_fixed = zeros(size(x.output.V));
            end
	    if ~isfield(x.output.vf_decomp,'V_full')
	        display('*** Populating output.vf_decomp.V_full because it is missing ***')
                x.output.vf_decomp.V_full = x.output.V;
	    end
	    this_split           = relevant_fields(x.output);
            data(ii).options     = defaultvalue(data(ii).options,'donor_weight',ones(1,numel(data(ii).donor_id))/numel(data(ii).donor_id));
            
            [this_split.disc,this_split.disc_loc] = discards(this_split,data(ii));
            fprintf('Discards are %3.5f\n',this_split.disc);
            fprintf('Local Discards are %3.5f\n',this_split.disc_loc);
            fprintf('Frac tx is  %3.5f \n',this_split.frac_transplanted);
            this_split.kid_arrival_rate = data(ii).options.donor_weight * data(ii).donor_options.N_ki_don * data(1).options.lambda;
            this_split.ii        = ii;
            fprintf('Found %s\n',[rslt_filename split_string])
            this_split.use_proj  = cf_mech.use_proj;
        else
            fprintf('Could not find %s\n',[rslt_filename split_string])
            continue
        end
        if ii==1, temp_output= this_split; else temp_output(ii)= this_split; end
    end
    if numel(temp_output)==0, continue; end
    
    % Write the nice names for the mechanism
    temp_output(1).mech_name = beautifulnames(alias{1});
    jj = jj +1;
    output(jj,1) = temp_output(1);
    % Merge multiple splits into one structure
    if ii>1
        jj = jj + 1; 
        output(jj,1) = merge_pools(data, temp_output(1), temp_output(2:end));
    end
end

% Subfunctions;
function output = merge_pools(data,combined,split)
    % These may become useful later on.
    output = struct();

    % Aggregate output measures

    output.frac_transplanted = 0;
    output.total_tx_rate     = sum([split.total_tx_rate]);
    output.pool_size         = sum([split.pool_size]);
    output.diff_norm         = max([split.diff_norm]);
    output.iter              = max([split.iter]);
    output.pt_weights        = zeros(size(combined.pt_weights));
    output.V                 = zeros(size(combined.V));
    output.mech_name         = [combined.mech_name ' split'];
    output.kid_arrival_rate  = sum([split.kid_arrival_rate]);
    output.ii                = 2;
    output.exog_dep          = sum([split.exog_dep]); 
    output.frac_transplanted = sum([split.kid_arrival_rate].*[split.frac_transplanted])/ output.kid_arrival_rate;
    output.discards          = sum([split.kid_arrival_rate].*[split.discards])/ output.kid_arrival_rate;
    N_patients               = numel(data.pt_hist_id); 
    N_donors                 = numel(data.donor_id); 
    
    % Arrange V and pt_weights
    for ii = 1:numel(split)
       
        % Where is the patient in the big pool?
        [istrue,where_pt] = ismember(data(split(ii).ii).pt_hist_id,data(1).pt_hist_id);
        assert(all(istrue));        
        output.V(where_pt,:)     = split(ii).V;

        if numel(size(output.pt_weights)) == 2
            output.pt_weights(where_pt,:)   = split(ii).pt_weights;
        else
            output.pt_weights(where_pt,:,:) = split(ii).pt_weights;
        end
    end

% Subfunctions;
function beautiful=beautifulnames(ugly)
    ugly = strsplit(ugly,'_');
    for x=1:numel(ugly)
        switch ugly{x}
            case 'fact',         ugly{x}   = '[Naive]';
            case 'proj',         ugly{x}   = ''; % old code '[Projection]';
            case 'locl',         ugly{x}   = ''; % old code '[Local]';
            case 'alt2',         ugly{x}   = ''; % Alternative Framework
            case 'ref',          ugly{x}   = ''; % Reference Mechanism
            case 'notimeout',    ugly{x}   = ''; % No Timeouts
            case 'r40',          ugly{x}   = ''; % Bound Pareto Weights
            case 'eta',          ugly{x}   = '[Donor UH]';
			case 'vref',         ugly{x}   = '[Pat V]';
            case '20bins',       ugly{x}   = '[20 Score Bins]';
            case '50bins',       ugly{x}   = '[50 Score Bins]';
            case 'logit',        ugly{x}   = '[Logit Transformation]';
            
        end
    end
    
    beautiful = ugly;
    switch ugly{1}
        case 'score'
            switch ugly{2}
                case 'binspoints',       beautiful = 'Pre-2014 Priorities';
                case 'kas',              beautiful = 'Post-2014 Priorities';
                case 'gammaeta',         beautiful = 'Greedy Priorities';
                case 'lifo',             beautiful = 'Last Come First Served';
                case 'fifo',             beautiful = 'First Come First Served';
            end
        case 'optim'
            switch ugly{2},
                case 'first',            beautiful = 'Optimal Assignment';
                case 'second',           beautiful = 'Optimal Offer Rates';
                case 'secondC',          beautiful = 'Opt. Pareto Improving Offer Rates';
            end
        case 'greedy',
            switch ugly{2},
                case 'second',   beautiful = 'Approximately Optimal Priorities'; 
                case 'secondC',  beautiful = 'Approx. Opt. Pareto Improving Priorities'; 
                otherwise,       beautiful = ['Approx. ' beautiful];
            end
        otherwise,            beautiful = ugly;
    end
    
    if numel(ugly)>2
        beautiful = [beautiful ' ' strjoin(ugly(3:end),' ')];
    end
    
    
function [frac_discards,frac_discards_local] = discards(state,data)
         N_ki_don      = data.donor_options.N_ki_don;
         p_discard     = data.donor_options.offers.p_discard;
         donor_weight  = data.options.donor_weight;
         p_tx          = state.p_tx;
         pt_weights    = state.pt_weights;
         pool_size     = state.pool_size;
         
         sum_prob      = squeeze(sum(sum(bsxfun(@times,p_tx,pt_weights),1),2))*pool_size;
         % Expected number of discarded kidneys conditional on donor arrival (any donor)
         E_discard     = sum((N_ki_don.*donor_weight' - sum_prob).*p_discard);
         % Expected number of available kidneys conditional on donor arrival (any donor)
         E_ki_don      = donor_weight*N_ki_don;
         % Fraction of discarded kidneys
         frac_discards = E_discard/E_ki_don;
         if nargout >1
            local      = data.donor_options.offers.local_donor; 
            E_discard  = sum((N_ki_don.*donor_weight' - sum_prob).*p_discard.*local);
            E_ki_don   = donor_weight*(N_ki_don.*local);            
            frac_discards_local = E_discard/E_ki_don; 
         end
            
            
            
            
            
            
         
