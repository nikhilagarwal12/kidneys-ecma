// Fixes init_date using the information in the waitlist ordering.
// We noticed that there was some discrepancy between the
// wait points awarded in the data and those simulated by us.
// This file finds init_dates that are consistent with the order
// in the data.

// The main calculations involve several steps. 
// 1. Data Cleaning
// 2. Calculate ub_init_date and lb_init_date implied by  ptr_sub_score_points_5 (wait points)
// 3. Obtain a set of trusted wlreg_audit_id: wlregs that have identical init_date and wt_qual_date which are aways consistent.
// with the points awarded  by  ptr_sub_score_points_5 (wait points). Use them and the observed order in ptr_sub_score_points_1
// to narrow the bounds on the untrusted ones (lb_narrow,ub_narrow).
// 4. Now instead of requiring a trusted wlreg, we require a trusted offer. Look for offers with init_date (or wt_qual_date)
// within the bounds constructed before. These are trusted offers. Use the observed init_date (or wt_qual_date) for those
// offers to narrow the bounds of the untrusted offers.
// 5. Apply the longest increasing subsequence to every series of consecutive untrusted offers with a non-zero gap. Fix the dates
// of the observations in the sequence and narrow the bounds of the observations not in the sequence. Save `auxiliary_files'c_init_date.dta

// generate_match_run_last: uses `auxiliary_files'c_init_date.dta to create the last trusted date in each match run. This is a trusted cutoff.
// saves `auxiliary_files'fix_last_points.dta
 
// generate_wlreg_files uses the trusted offers in `auxiliary_files'c_init_date.dta to pin down a sensible init_date for each wlreg_audit_id.
* A. c_init_date_A: wlreg_audit_id with one and only one date that is consistent with all the offers (gap=0).
* B. c_init_date_B: wlreg_audit_id with at least one date that is consistent with all the offers (gap>0).
* C. c_init_date_C: wlreg_audit_id with at least one date that is consistent with most offers (up to 10 exceptions).
* D. c_init_date_D: wlreg_audit_id with at least one date that is almost consistent with most of the offers (up to 5 day tolerance).
* E. c_init_date_jump: jumps in init_date that are consistent across OPOs
* F. c_init_date_opo: wlregs that appear to have OPO-specific init_dates
* Appends A,B,C,D and saves it as `auxiliary_files'c_init_date.dta
* Saves c_init_date_jump as `auxiliary_files'c_init_date_jump.dta
* Saves c_init_date_opo as  `auxiliary_files'c_init_date_opo.dta

// generate_wl_id_files: Generates a single file organized by wl_id_code instead of wlreg_audit_id
// saves it as fix_init_date. This file will be used by simulate_offers.do. It uses c_init_date.dta 
// and c_init_date_jump.dta.

pause off
set more off

// All these locals have to be set to one for a full re-run
local wlreg_audit_date_range = 1   // get the valid  date range for each wlreg_audit_id
local run_main_calculations = 1
local generate_match_run_last = 1
local generate_wlreg_files = 1
local generate_wl_id_files = 1

local offers_file = "~/organs-star/primary_files/offers"

if `wlreg_audit_date_range' | `run_main_calculations'{
	use `offers_file',clear
}


if `wlreg_audit_date_range' {
	bys wlreg_audit_id: egen from = min(match_date)
	bys wlreg_audit_id: egen till = max(match_date)
	bys wlreg_audit_id: gen is_wl_id = (_n==1)
	outsheet wlreg_audit_id from till if is_wl_id using `auxiliary_files'wlreg_audit_date_range.csv, comma replace 
	drop from till is_wl_id
} 

if `run_main_calculations' {

	local keepvars ="wl_id_code pre_KAS match_date donor_id match_id_code run_opo_id descrip wlreg_audit init_date wt_qual_date ptr_sub_score_points_1 ptr_sub_score_points_5"
	keep `keepvars'
	order `keepvars'

	//1. Data Cleaning
	* Drop post KAS
	keep if pre_KAS 
	drop pre_KAS
	* Drop opos with half point rule
	drop if run_opo_id == "CADN-OP1" | run_opo_id == "LAOP-OP1" | run_opo_id == "MWOB-OP1" | run_opo_id == "ORUO-IO1" | run_opo_id == "TXGC-OP1"
	* Drop opos with dialysis rule
	drop if run_opo_id == "IAOP-OP1" | run_opo_id == "CAOP-OP1" | run_opo_id == "MIOP-OP1" 
	* Drop Region 1 that uses an age-specific rule for waitpoints
	drop if run_opo_id == "MAOB-OP1" | run_opo_id == "CTOP-OP1"
	* Drop runs that do not award tie-break points and observations that are not sorted according to the tie-break
	drop if ptr_sub_score_points_1==0
	* Drop a few cases where matchruns and points are ordered in different ways. 
	* This may occur because match_id_code fails to distinguish two different match runs or some other anomaly
	gen days_waited = match_date - init_date
	bysort donor_id  match_id_code descrip (ptr_sub_score_points_1 ptr_sub_score_points_5 days_waited wlreg_audit_id_code): gen prev_pts = ptr_sub_score_points_5[_n-1] if _n>1
	replace prev_pts =0 if prev_pts==.
	gen ptsdiff = ptr_sub_score_points_5 - prev_pts
	gen ok_ptsdiff = ptsdiff>=0 
	replace ok_ptsdiff = ok_ptsdiff & ptsdiff[_n+1]>=0
	table ok_ptsdiff
	table donor_id if ~ok_ptsdiff
	keep if ok_ptsdiff
	gen long resort_order = _n
	* Perform some manual fixes 
	replace init_date = td(20jan2011) if wlreg_audit== 17845294
	replace wt_qual_date = td(20jan2011) if wlreg_audit== 17845294
	* This is the original ordering. The correct init_dates should be sorted in decreasing order!

	// 2. Calculate ub_init_date and lb_init_date implied by  ptr_sub_score_points_5
	gen ub_init_date = mdy(month(match_date),day(match_date),year(match_date)-ptr_sub_score_points_5)
	replace ub_init_date = mdy(month(match_date)+1,1,year(match_date)-ptr_sub_score_points_5) if month(match_date)==2 & day(match_date)==29 & ub_init_date==.
	gen lb_init_date = mdy(month(match_date),day(match_date),year(match_date)-ptr_sub_score_points_5-1) 
	replace lb_init_date = mdy(month(match_date),day(match_date)-1,year(match_date)-ptr_sub_score_points_5-1) if month(match_date)==2 & day(match_date)==29 & lb_init_date==.


	// 3. Obtain a set of trusted wlregs (ok_always): find wlregs that have consistent init_date and wt_qual_date
	gen ok_wait = lb_init_date < wt_qual_date &  wt_qual_date < ub_init_date
	gen ok_init = lb_init_date < init_date &  init_date < ub_init_date
	gen ok_both = init_date == wt_qual_date & ok_wait & ok_init

	bysort wlreg_audit_id  run_opo_id: gen wlreg_opo_N = _N
	bysort wlreg_audit_id  run_opo_id: egen ok_always = mean(ok_both)
	bysort wlreg_audit_id  run_opo_id: egen sd_init_date = sd(init_date)
	bysort wlreg_audit_id  run_opo_id: egen sd_wt_qual_date = sd(wt_qual_date)

	replace ok_always = ok_always == 1 & sd_init_date == 0
	table ok_always ok_wait ok_init

	* Infer a few bounds (gap) dates from our trusted wlregs for our untrusted wlregs.
	gen ub_inferred_date = init_date if ok_always & ok_ptsdiff & wlreg_opo_N >50 & ptr_sub_score_points_1 >0
	gen lb_inferred_date = init_date if ok_always & ok_ptsdiff & wlreg_opo_N >50 & ptr_sub_score_points_1 >0
	*      Sort in ascending order of wait points -> descending init_dates
	sort resort_order
	replace ub_inferred_date = ub_inferred_date[_n-1] if ub_inferred_date==. & match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	gen days_not_ok = ub_inferred_date~=. & ub_inferred_date[_n-1] ~=. & (ub_inferred_date - ub_inferred_date[_n-1])>0 if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	summ days_not_ok,d
	format *_date %td
	*      Sort in descending order of wait points -> ascending init_dates
	gsort -resort_order
	replace lb_inferred_date = lb_inferred_date[_n-1] if lb_inferred_date==. & match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	summ lb_inferred_date ok_init
	replace days_not_ok = lb_inferred_date~=. & lb_inferred_date[_n-1] ~=. & (lb_inferred_date - lb_inferred_date[_n-1])<0 if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	summ days_not_ok,d
	gen gap = ub_inferred_date - lb_inferred_date
	summ gap,d
	*  Pick a set of trusted_dates: those dates with zero gap
	gen trusted_date = ub_inferred_date if gap==0
	forval  num_wlreg_opo_N = 40(-5)30 { 
		capture drop lb ub
		replace trusted_date = init_date if trusted_date==. & ok_always & ok_ptsdiff & wlreg_opo_N >`num_wlreg_opo_N' & ptr_sub_score_points_1 >0 & init_date<=ub_inferred_date & init_date>=lb_inferred_date
		sort resort_order
		gen ub = trusted_date
		replace ub = ub[_n-1] if ub==. & match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
		gsort -resort_order
		gen lb = trusted_date
		replace lb = lb[_n-1] if lb==. & match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
		replace gap = ub - lb
		summ gap,d
		replace trusted_date = lb if gap==0
	}
	replace lb = td(1jan1900) if lb==.
	replace ub = td(1jan2020) if ub==.
	gen lb_narrow = max(lb,lb_init_date)
	gen ub_narrow = min(ub,ub_init_date)
	replace gap =ub_narrow-lb_narrow
	summ gap,d
	gen problem = gap<0
	summ problem, d
	di r(sum)
	drop if problem


	// 4. Now instead of requiring a trusted wlreg, we require a trusted offer. Look for offers with init_date (or wt_qual_date)
	// within the bounds constructed before. These are trusted offers. Use the observed init_date (or wt_qual_date) for those
	// offers to narrow the bounds of the untrusted offers.
	* Back up old bounds
	gen ub_date = ub_narrow
	gen lb_date = lb_narrow
	* Verify order
	sort donor_id  match_id_code descrip resort_order
	*      Sort in ascending order of wait points -> descending init_dates
	gen aux =  lb_date - lb_date[_n-1] if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	replace aux = 0 if aux==.
	assert aux<=0
	drop aux
	gen aux =  ub_date - ub_date[_n-1] if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	replace aux = 0 if aux==.
	assert aux<=0
	drop aux
	* Choose init_date or wt_qual_date depending on which is consistent with bounds
	gen post_init_date_ok = init_date <= ub_date & lb_date<=init_date
	gen post_wait_date_ok = wt_qual_date <= ub_date & lb_date<=wt_qual_date
	gen use_wait_time = post_wait_date_ok & ~post_init_date_ok
	table post_init_date_ok post_wait_date_ok
	gen wait_date = init_date if post_init_date_ok
	replace wait_date = wt_qual_date if use_wait_time
	gen post_date_ok = post_init_date_ok | post_wait_date_ok
	* Generate an id for each gap (i.e. each series of consecutive untrusted wlregs between two trusted wlregs)
	gen long gap_id = 0
	replace gap_id = gap_id[_n-1]+(lb_narrow~=lb_narrow[_n-1] | ub_narrow~=ub_narrow[_n-1])  if _n>1 
	*      Sort in ascending order of wait points -> descending init_dates
	sort donor_id  match_id_code descrip gap_id resort_order
	* (wait_date should be decreasing..., recall wait_date is non-empty if post_date_ok 
	* and by construction should be btw lb_date and ub_date)
	replace wait_date = wait_date[_n-1] if wait_date==. & gap_id == gap_id[_n-1]
	gen date_diff = wait_date - wait_date[_n-1] if gap_id == gap_id[_n-1]
	replace date_diff = 0 if date_diff==.
	by donor_id  match_id_code descrip gap_id: egen date_diff_negs = total(date_diff>0)
	replace ub_date = wait_date if post_date_ok  & date_diff_negs==0
	replace lb_date = wait_date if post_date_ok  & date_diff_negs==0
	replace gap = ub_date - lb_date
	summ gap

	replace ub_date = min(ub_date,ub_date[_n-1]) if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	gen aux =  ub_date - ub_date[_n-1] if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	replace aux = 0 if aux==.
	assert aux<=0
	drop aux


	gsort -donor_id  -match_id_code -descrip -gap_id -resort_order
	replace lb_date = max(lb_date,lb_date[_n-1]) if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	gen aux =  lb_date - lb_date[_n-1] if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	replace aux = 0 if aux==.
	assert aux>=0
	drop aux

	replace gap = ub_date - lb_date
	summ gap
	drop post_init_date_ok post_wait_date_ok use_wait_time wait_date gap_id date_diff date_diff_negs post_date_ok

	// 5. Apply the longest increasing subsequence to every series of consecutive untrusted offers with a non-zero gap.

	*************************************
	***  LongestIncreasingSubsequence ***
	*************************************

	sort donor_id  match_id_code descrip resort_order
	gen long gap_id = 0
	replace gap_id = gap_id[_n-1]+(lb_narrow~=lb_narrow[_n-1] | ub_narrow~=ub_narrow[_n-1])  if _n>1 
	gen post_init_date_ok = init_date <= ub_date & lb_date<=init_date
	gen post_wait_date_ok = wt_qual_date <= ub_date & lb_date<=wt_qual_date
	gen use_wait_time = post_wait_date_ok & ~post_init_date_ok
	gen wait_date = init_date if post_init_date_ok
	replace wait_date = wt_qual_date if use_wait_time
	gen post_date_ok = post_init_date_ok | post_wait_date_ok

	do ../../tools/code/LongestIncreasingSubsequence.ado
	longseq wait_date if post_date_ok & gap>0, g(inSeq) by(gap_id) order(resort_order) dec
	tab inSeq,m

	replace ub_date = wait_date if inSeq == 1
	replace lb_date = wait_date if inSeq == 1
	replace gap = ub_date - lb_date
	summ gap


	sort donor_id  match_id_code descrip gap_id resort_order
	replace ub_date = min(ub_date,ub_date[_n-1]) if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	gen aux =  ub_date - ub_date[_n-1] if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	replace aux = 0 if aux==.
	assert aux<=0
	drop aux
	gsort -donor_id  -match_id_code -descrip -gap_id -resort_order
	replace lb_date = max(lb_date,lb_date[_n-1]) if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	gen aux =  lb_date - lb_date[_n-1] if match_id_code==match_id_code[_n-1] & descrip==descrip[_n-1]
	replace aux = 0 if aux==.
	assert aux>=0
	drop aux
	replace gap = ub_date - lb_date
	summ gap
	drop post_init_date_ok post_wait_date_ok use_wait_time wait_date gap_id post_date_ok

	capture noi save `auxiliary_files'c_init_date.dta, replace 
}
** End: main_calculations

if `generate_match_run_last' {
	use `auxiliary_files'c_init_date.dta, clear
	isid donor_id match_id_code wlreg_audit_id_code descrip
    keep donor_id match_id_code wlreg_audit_id_code descrip ub_date lb_date
	local sortingvariable ="ptr_row_order  pb_offer_seq"
	merge 1:m donor_id match_id_code wlreg_audit_id_code descrip using `offers_file', keepusing(donor_id match_run_num match_id_code `sortingvariable' bin_t match_run_num ptr*) keep(match)
	drop if match_run_num==.
	bys donor_id match_id_code (`sortingvariable'): gen keepit = _n==_N
	keep if keepit
	local p="ptr_sub_score_points_"
	gen c_ptr_points = `p'2 + `p'3 + `p'4 + `p'5 + `p'7 + `p'8 + `p'16 + `p'23 + `p'34 +`p'35
	drop keepit `sortingvariable' ptr_*
	save `auxiliary_files'fix_last_points.dta, replace
}


if `generate_wlreg_files' {
use `auxiliary_files'c_init_date.dta, clear

*******************************************************
*******************************************************
*** Once the gaps are small enough, we look at each ***
*** wlreg_audit_id to compute a c_init_date that is ***
*** consistent with the computed gap and minimizes  ***
*** the number of jumps of init_date.               ***
*******************************************************
*******************************************************
* In a way, I need to account for the fact that I may
* have some error in my bounds.
* So I will not take seriously small deviations. 
* A. c_init_date_A: wlreg_audit_id with one and only one date that is consistent with all the offers (gap=0).
* B. c_init_date_B: wlreg_audit_id with at least one date that is consistent with all the offers (gap>0).
* C. c_init_date_C: wlreg_audit_id with at least one date that is consistent with most offers (up to 10 exceptions).
* D. c_init_date_D: wlreg_audit_id with at least one date that is almost consistent with most of the offers (up to 5 day tolerance).
* E. c_init_date_jump: jumps in init_date that are consistent across OPOs
* F. c_init_date_opo: wlregs that appear to have OPO-specific init_dates


sort wlreg_audit_id match_date donor_id
gen c_init_date = .

// Outsheet wlreg_audit_id with one date that is consistent with all the offers.
by wlreg_audit_id:  gen is_wlreg = _n==1
by wlreg_audit_id: egen max_lb_wlreg = max(lb_date)
by wlreg_audit_id: egen min_ub_wlreg = min(ub_date)
gen output1 =  (min_ub_wlreg==max_lb_wlreg) + (min_ub_wlreg>=max_lb_wlreg) 
tab output1 if is_wlreg
replace c_init_date = min_ub_wlreg if output1==2
format %8.0g *_date
outsheet wl_id_code wlreg_audit_id c_init_date using `auxiliary_files'c_init_date_A.csv if is_wlreg & output1==2, comma replace
drop if output1==2
by wlreg_audit_id:  gen offers_wlreg = _N
gen gap_wlreg = min_ub_wlreg-max_lb_wlreg
gen is_init_ok = init_date<=min_ub_wlreg & max_lb_wlreg<=init_date
gen is_wait_ok = wt_qual_date<=min_ub_wlreg & max_lb_wlreg<=wt_qual_date
replace c_init_date = min_ub_wlreg if output1==1
replace c_init_date = init_date if is_init_ok & output1
replace c_init_date = wt_qual_date if is_wait_ok & output1
format %8.0g *_date
outsheet wl_id_code wlreg_audit_id c_init_date using `auxiliary_files'c_init_date_B.csv if is_wlreg & output1==1, comma replace
drop if output1==1
// Outsheet apparent jumps in init_date that are consistent across OPOs
// Some preliminary work:
local lb = lb_date[1]
local ub = ub_date[1]
local Nobs = _N
gen has_jump = 1

forval i = 2/`Nobs' {
    if wlreg_audit_id[`i']==wlreg_audit_id[`i'-1]  {
        if min(`ub',ub_date[`i'])>=max(`lb',lb_date[`i']) {
	        qui replace has_jump = has_jump[`i'-1] in `i'
	        local ub = min(`ub',ub_date[`i'])
	        local lb = max(`lb',lb_date[`i'])
	    }
	    else {
	        local ub = ub_date[`i']
	        local lb = lb_date[`i']
	        qui replace has_jump = has_jump[`i'-1]+1 in `i'
	    }
    }
    else {
	    local lb = lb_date[`i']
	    local ub = ub_date[`i']
    }
} 


bysort wlreg_audit_id has_jump (match_date): egen max_lb_jump_wlreg = max(lb_date)
bysort wlreg_audit_id has_jump (match_date): egen min_ub_jump_wlreg = min(ub_date)
bysort wlreg_audit_id has_jump (match_date): egen from_date = min(match_date)
bysort wlreg_audit_id has_jump (match_date): egen till_date = max(match_date)
bysort wlreg_audit_id has_jump (match_date donor_id): gen is_jump = _n==1
bysort wlreg_audit_id has_jump: gen offers_in_jump = _N
bysort wlreg_audit_id: egen num_jumps_wlreg = max(has_jump)
bysort wlreg_audit_id: egen min_offers_in_jump = min(offers_in_jump)
replace output1 = (min_offers_in_jump>=8) |  (min_offers_in_jump>=3 & num_jumps_wlreg<4)
gen gap_jump_wlreg = min_ub_jump_wlreg-max_lb_jump_wlreg
replace c_init_date = min_ub_jump_wlreg if is_jump & output1

format %8.0g *_date
outsheet wl_id_code wlreg_audit_id from_date c_init_date using `auxiliary_files'c_init_date_jump.csv if is_jump & output1, comma replace
drop if output1
drop has_jump from_date

// See if there is some OPO specific variations...
local Nobs = _N
sort wlreg_audit_id run_opo_id match_date donor_id
local lb = lb_date[1]
local ub = ub_date[1]
gen has_jump=1
forval i = 1/`Nobs' {
    if wlreg_audit_id[`i']==wlreg_audit_id[`i'-1] & run_opo_id[`i']==run_opo_id[`i'-1] {
        if min(`ub',ub_date[`i'])>=max(`lb',lb_date[`i']) {
	        qui replace has_jump = has_jump[`i'-1] in `i'
	        local ub = min(`ub',ub_date[`i'])
	        local lb = max(`lb',lb_date[`i'])
	    }
	    else {
	        local ub = ub_date[`i']
	        local lb = lb_date[`i']
	        qui replace has_jump = has_jump[`i'-1]+1 in `i'
	    }
    }
    else {
	    local lb = lb_date[`i']
	    local ub = ub_date[`i']
	}
} 


capture drop lb ub
bysort wlreg_audit_id run_opo_id has_jump (match_date): egen lb = max(lb_date)
bysort wlreg_audit_id run_opo_id has_jump (match_date): egen ub = min(ub_date)
bysort wlreg_audit_id run_opo_id has_jump (match_date): egen from_date = min(match_date)
bysort wlreg_audit_id run_opo_id has_jump (match_date donor_id): gen is_jump_opo = _n==1
bysort wlreg_audit_id run_opo_id has_jump: gen N_off = _N
bysort wlreg_audit_id run_opo_id: egen jump_w_most_offers = max(N_off)
keep if is_jump_opo
local keepvars = "wl_id_code wlreg_audit_id run_opo_id has_jump from_date lb ub N_off jump_w_most_offers  init_date wt_qual_date" 
keep `keepvars'
order `keepvars'
format ub lb from_ %td



// Purge meaningless jumps
gen suspect = 0
forval v=1/10{
	bys wlreg_audit run_opo_id suspect (has_jump): gen jump_magnitude = lb - ub[_n-1] if _n>1
	replace suspect = suspect | abs(jump_magnitude)<10 | (N_off<3 & jump_w_most_offers>=3) 
	drop jump_magnitude
}
drop if suspect 
drop suspect
bys wlreg_audit (run_opo_id): egen max_lb = max(lb)
bys wlreg_audit (run_opo_id): egen min_ub = min(ub)
bys wlreg_audit: gen is_wlreg =_n==1
gen diff = min_ub - max_lb
bys wlreg_audit (run_opo_id): egen c_init_date = median(ub)
gen output1 = abs(diff)<=10
replace c_init_date = . if ~output1
format %8.0g *_date
outsheet wl_id_code wlreg_audit_id c_init_date using `auxiliary_files'c_init_date_C.csv if is_wlreg & output1, comma replace
drop if output1
drop output1 c_init_date
 
drop has_jump diff max_lb min_ub jump_w_most_offers
by wlreg_audit run_opo_id: gen has_jump = _n
by wlreg_audit run_opo_id: gen jumps = _N
table jumps if is_wlreg


//Get modal date and departures from it.
gen neg_gap = lb-ub
bys wlreg_audit neg_gap lb: egen N_off_date = total(N_off)
bys wlreg_audit (neg_gap N_off_date lb): gen is_last_obs=_n==_N
gen modal_date_aux = lb if is_last_obs
replace modal_date_aux = init_date if is_last_obs & neg_gap<0 & init_date<=ub+5 & lb<=init_date+5
replace modal_date_aux = wt_qual_date if is_last_obs & neg_gap<0 & wt_qual_date<=ub+5 & lb<=wt_qual_date+5
by wlreg_audit: egen modal_date =max(modal_date_aux)


gen generic = lb<=modal_date+5 & modal_date<=ub+5
gen may_be_a_fluke = N_off_date<2
bysort wlreg_audit_id run_opo_id (from_date is_wlreg is_last_obs N_off_date generic): gen till_date = from_date[_n+1] if _n<_N
replace from_date = . if jumps==1

keep if is_last_obs | ~(may_be_a_fluke | generic)
replace run_opo_id ="" if is_last_obs
ren modal_date_aux c_init_date
replace c_init_date  = ub if ~is_last_obs

local keepvars = "wl_id_code wlreg_audit_id run_opo_id from_date till_date c_init_date is_last_obs" 
keep `keepvars'
order `keepvars'
format %8.0g *_date
outsheet wl_id_code wlreg_audit_id c_init_date if is_last_obs using `auxiliary_files'c_init_date_D.csv, comma replace
outsheet wl_id_code wlreg_audit_id run_opo_id from_date till_date c_init_date if ~is_last_obs using `auxiliary_files'c_init_date_opo.csv, comma replace
clear


*******************************************************
*******************************************************
*** Generate Files in auxiliary_files  ***
*******************************************************
*******************************************************


// Generate three stata files:
//   a) c_init_date: calculated init_date for most wlreg_audit_id
//   b) c_init_date_jump: calculated init_date for wlreg_audit_id that exhibit a jump at some point in time
//   c) c_init_date_opo: calculated init_date for wlreg_audit_id that exhibit variation across opo's.

// a)
clear
insheet  using `auxiliary_files'c_init_date_A.csv,comma
save `auxiliary_files'c_init_date.dta, replace
clear
insheet  using `auxiliary_files'c_init_date_B.csv,comma
append using  `auxiliary_files'c_init_date.dta
save `auxiliary_files'c_init_date.dta, replace
clear
insheet  using `auxiliary_files'c_init_date_C.csv,comma
append using  `auxiliary_files'c_init_date.dta
save `auxiliary_files'c_init_date.dta, replace
clear
insheet  using `auxiliary_files'c_init_date_D.csv,comma
append using  `auxiliary_files'c_init_date.dta
save `auxiliary_files'c_init_date.dta, replace
// b)
clear
insheet  using `auxiliary_files'c_init_date_jump.csv,comma
save `auxiliary_files'c_init_date_jump.dta, replace
// c)
clear
insheet  using `auxiliary_files'c_init_date_opo.csv,comma
save `auxiliary_files'c_init_date_opo.dta, replace
}
******************************************************
***** Single File that is organized by wl_id_code ****
******************************************************
if `generate_wl_id_files' {
	clear
	insheet using `auxiliary_files'wlreg_audit_date_range.csv,comma
	save `auxiliary_files'wlreg_audit_date_range.dta, replace

	use `auxiliary_files'c_init_date.dta, replace
	gen source = 0
	append using `auxiliary_files'c_init_date_jump.dta
	replace source = 1 if source==.
	append using `auxiliary_files'c_init_date_opo.dta
	replace source = 2 if source==.

	bys wl_id_code: gen is_wl_id =_n==1
	by wl_id_code: egen max_c = max(c_init_date) 
	by wl_id_code: egen min_c = min(c_init_date) 
	gen unique_wl_id_date = max_c == min_c
	merge m:1 wlreg_audit_id using `auxiliary_files'wlreg_audit_date_range.dta

	keep if _merge==3 & source<2
	bys wl_id_code  wlreg_audit_id_code (from_date): gen num_jump=_n
	ren from_date c_date_from
	keep wl_id_code  wlreg_audit_id_code c_date_from num_jump c_init_date
	save  `auxiliary_files'fix_init_date.dta,replace
}
di "THE END"
