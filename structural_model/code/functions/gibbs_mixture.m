% Gibbs' sampler generates markov chain of draws from posterior distribution, including data augmentation steps
% Implements sampling procedure described in Appendix A.2

% Everything should be sparse storage
% In the sampler, 1 means y > 0 means reject, and y = xi*theta + eta + eps

% cont_choice --- Stacked vector of decisions
% Basis function choice --- chi_fcn
% Columns with characteristics, Xs, Zs etc
% Priors
% N decisions and such


function [chain,final_val,all_chain] = gibbs_mixture(data,model,priors,init,chain_spec) 

% Initialization and pre-processing

    if ~isfield(model,'weighted_gibbs')
        model.weighted_gibbs = false;            
    end
    % Weight each donor equally -- i.e. each offer gets a weight =
    % 1/n_d where n_d is the number of offers for donor d
    if model.weighted_gibbs
        w_don = 1./(data.eta_mat*sum(data.eta_mat,1)');
        W = spdiags(w_don,0,data.N_offers,data.N_offers);
    end

    % Eta matrix of dummies for which decision is about which
    % kidney -- D x J
    if model.donor_uh
        % data.eta_mat'*data.eta_mat is a diagonal matrix. Faster
        % to store only the diagonal terms
        if model.weighted_gibbs
            inner_eta_mat = data.eta_mat' * W * data.eta_mat;
        else
            inner_eta_mat = data.eta_mat' * data.eta_mat;
        end
        assert(all(all(inner_eta_mat-diag(diag(inner_eta_mat))==0)));
        inner_eta_mat = diag(inner_eta_mat);

        % Only need the following if sigma_eta is redrawn
        if chain_spec.draw_params
            V_eta = 1./(inner_eta_mat + 1/priors.V_eta); % precision_posterior = inv(X'X + precision_prior)
        else
            V_eta = 1./(inner_eta_mat + 1/init.sigma_eta); 
        end
    end

    % Generate a where_ii variable 
    where_ii= cell(size(data.patient_list));
    for ii=1:data.N_patients
        where_ii{ii} = find(data.patients == data.patient_list(ii));           
    end

    % Set up init chi
    chi = zeros(data.N_offers,model.K_theta);
    chi_beta = cell(model.K_types,1);
    if model.pt_uh
        for kk = 1:model.K_types
            where_kk = cat(1,where_ii{init.beta_i==kk});
            chi(where_kk,:) = chi_param(data,model,kk,where_kk);
            chi_beta{kk} = chi_param(data,model,kk);
        end   
    else
        where_kk = cat(1,where_ii{:});
        chi(where_kk,:) = chi_param(data,model,1,where_kk);
    end
    
    % Posterior variance for theta    
    if chain_spec.draw_params
        if model.weighted_gibbs
            V_theta = inv(chi' * W * chi + inv(priors.V_theta));    
            % Symmetrize -- mvrnd creates a problem
            V_theta = (V_theta + V_theta')/2;
        else
            V_theta = inv(chi' * chi + inv(priors.V_theta));    
        end
    end
        
    % Note: only values in the post structure will be stored
    post = struct();
    chain = cell(chain_spec.NITER/chain_spec.thin);
   
    % Read initial values
    Y = init.Y;
    post.theta = init.theta;
    if model.pt_uh
        beta_i = init.beta_i; 
        post.beta = init.beta;   
        % Set sizes
        dens_resid = zeros(data.N_offers,model.K_types);
        post_prop = zeros(data.N_patients,model.K_types);
    end
    if model.donor_uh
        post.eta = init.eta;    
        post.sigma_eta = init.sigma_eta;   
    end    

    
%% Chain
    % Gibbs chain starts here
    for bb = 1:chain_spec.NITER    

        % Sample \theta
        if chain_spec.draw_params
            if model.donor_uh
                resid = Y - data.eta_mat*post.eta;
            else
                resid = Y;
            end
            if model.weighted_gibbs
                resid = w_don.*resid;
            end
            theta_tilde = V_theta*(chi' * resid + priors.V_theta \ priors.mu_theta);
            post.theta = mvnrnd(theta_tilde,V_theta)'; % New draw of theta
        end

        % Sample Y
        if model.donor_uh
            mu = - chi*post.theta - data.eta_mat*post.eta;   % Cutoff for epsilon
        else
            mu = - chi*post.theta;
        end
        
        draws = rand(data.N_offers,1);
        Y = -mu + norminv(data.cont_choice.*(1-draws) + draws.*normcdf(mu));  % Y = chi*theta + eta + eps, where eps is drawn | choice
        
        if any(isnan(Y)), disp('nan'); bb, keyboard, end
        if any(Y(data.cont_choice)<0), disp('<0'); bb, keyboard; end
        if any(Y(~data.cont_choice)>0), disp('>0'); bb, keyboard; end
        
        if model.donor_uh

	    % sample \eta
            resid = Y - chi*post.theta;  % [y - chi*theta] for each offer
            if model.weighted_gibbs
                resid = w_don.*resid;
            end
            post.eta = (V_eta.^0.5).*randn(data.N_donors,1) + V_eta.*(data.eta_mat' * resid);

            % Update \eta parameters
            if chain_spec.draw_params
                post.sigma_eta = iwishrnd(post.eta'*post.eta + priors.V_eta, priors.V_nu + data.N_donors);
                V_eta = 1./(inner_eta_mat + 1/post.sigma_eta); % precision_posterior = inv(X'X + precision_prior)
            end
        end

        % Sample \beta_i    
        if model.pt_uh
           
            draws = rand(data.N_patients,1);  
            % Likelihood of each type for each patient
            for kk = 1:model.K_types
                if model.donor_uh
                    dens_resid(:,kk) =  normpdf(Y - data.eta_mat*post.eta - chi_beta{kk}*post.theta);
                else
                    dens_resid(:,kk) =  normpdf(Y - chi_beta{kk}*post.theta);
                end
            end         
            
            % Draw from beta_i posterior. Exp/log formula is for speed, to
            % exploit data.pt_mat
            norm = 1/mean(mean(dens_resid));
            post_prop = repmat(post.beta,data.N_patients,1) .* exp(data.pt_mat' * log(dens_resid*norm));         
            beta_i = sum(repmat(draws.*sum(post_prop,2),1,model.K_types)>cumsum(post_prop,2),2)+1;  
            post.beta_i = beta_i;              
        
            % Re-define chi
            for kk = 1:model.K_types
                where_kk = cat(1,where_ii{beta_i==kk});
                chi(where_kk,(model.fixed.K + 1):model.K_theta) = chi_param_update(data,model,kk,where_kk);
            end

            % Update the draw of p_beta
            if chain_spec.draw_params
                post.beta = dirichlet_sample(histc(beta_i,1:model.K_types)'+priors.beta);
            end

            % V_theta gets updated because of Chi if pt_uh is on
            if chain_spec.draw_params && model.pt_uh
                if model.weighted_gibbs
                    V_theta = inv(chi' * W * chi + inv(priors.V_theta));    
                    V_theta = (V_theta + V_theta')/2;
                else
                    V_theta = inv(chi' * chi + inv(priors.V_theta));    
                end
            end
        end

        % Display Progress, in 10ths
        if mod(bb,10)==0
            display(['Iteration: ' num2str(bb)]);
        end
        % Save only the model.chain_spec.thin'th version
        if mod(bb,chain_spec.thin)==0
            chain{floor(bb/chain_spec.thin)}=post;
        end

        % Save if all_chain is requested
        if nargout>2
            post.Y = Y;
            all_chain{bb}=post;            
        end
    end
    
    % Store final values
    final_val.Y = Y;
    final_val.theta = post.theta;
    final_val.rng_state = rng;
    if model.donor_uh
        final_val.eta = post.eta;
        final_val.sigma_eta = post.sigma_eta;
    end
    if model.pt_uh
        final_val.beta_i = beta_i;
        final_val.beta = post.beta;
    end

    % Cat
    chain = cat(1,chain{:});
    if nargout>2
       all_chain = cat(1,all_chain{:});        
    end    
end
