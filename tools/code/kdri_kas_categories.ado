// This program takes a variable called kdri and converts it into kdri_cat, the category relevant for KAS allocation
// Requires a normalization constant and cutoffs for the 20th, 34th, and 85th percentiles of normalized KDRI

program kdri_kas_categories

args normalization kdri_p20 kdri_p34 kdri_p85

gen kdri_norm = kdri / `normalization'

gen kdri_cat = 1 if kdri_norm <= `kdri_p20'
replace kdri_cat = 2 if kdri_cat == . & kdri_norm <= `kdri_p34'
replace kdri_cat = 3 if kdri_cat == . & kdri_norm <= `kdri_p85'
replace kdri_cat = 4 if kdri_cat == . & kdri_norm <= 999999999  // '999999999' is actually given as the upper bound in the KDRI -> KDPI mapping table
replace kdri_cat = 0 if kdri_cat == .

label define kdri_cat_label 0 "missing" 1 "0-20%" 2 "21-34%" 3 "35-85%" 4 "86-100%"
label values kdri_cat kdri_cat_label

drop kdri_norm

end
