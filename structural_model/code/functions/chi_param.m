
% These functions populate and/or update the design matrix \chi
% (see Step 2 of Section 4.2)

function chi = chi_param(data,model,kk,where_ii)
    switch nargin
        case 2  % Useful when there is no pt UH
            if model.pt_uh
                poo  % should always be supplied more than two arguments in pt UH spec
            else
                chi = data.fixed.obs;
            end
        case 3, chi = chi_param_aux(data,model,kk);           % update specific pt UH type
        case 4, chi = chi_param_aux(data,model,kk,where_ii);  % update a subset of rows
    end
end


% handles updating flexible part of design matrix in pt UH spec
function [chi_beta] = chi_param_aux(data,model,kk,where_ii)
    
    if nargin==4
        chi_beta = zeros(size(where_ii,1),model.K_theta);
    
        % Set up chi_beta for the requested patients
        chi_beta(:,1:model.fixed.K) = data.fixed.obs(where_ii,:);
        chi_beta(:,(model.fixed.K+(kk-1)*model.flex.K)+(1:model.flex.K)) = data.flex.obs(where_ii,:);
    else
        % Same value of kk for all patients
        chi_beta = zeros(data.N_offers,model.K_theta);        
        chi_beta(:,1:model.fixed.K) = data.fixed.obs;
        chi_beta(:,(model.fixed.K+(kk-1)*model.flex.K)+(1:model.flex.K)) = data.flex.obs;      
    end
end


