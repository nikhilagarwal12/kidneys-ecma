function ds = select_rows(ds,id_ds,id_name)   
   % Select rows
   which_rows = ismember(ds.(id_name),id_ds.(id_name));
   ds = ds(which_rows,:);
end